<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;

if ($infoRequest == 2) {
	$data_key = "data";
	$data = $model -> $data_key;
	SaveOrUpdate($m, $data);
} else if ($infoRequest == 4) {
	/*
	 * Cherche si une ligne avec la valeur ACTIF_HOPITAL = 1
	 * Si des lignes existes, il faut les remettre à 0
	 */
	$id = "idHopital";
	$idCnaps = $model -> $id;
	$centre = "idCentre";
	$idCentre = $model -> $centre;
	$checkedId = $m -> CheckExistActif('hopital', 'ID_HOPITAL', 'ACTIF_HOPITAL = 1 AND ID_CENTRE = '.$idCentre);
		
	ResetActifById($m, $checkedId, 0);

	echo ResetActifById($m, [$idCnaps], 1);
} else if ($infoRequest === 3) {
	$data_key = "data";
	$data = $model -> $data_key;
	Delete($m, $data);
}

/*
 * Save or update changed data from Centre grid in Tab Panel
 * 
 */
function SaveOrUpdate($dao, $data) {
	$val_actif = $dao -> IsNullOrEmptyString($data -> actifHopital) > 0 ? 0 : $data -> actifHopital;

	if ($val_actif >0){
		$checkedId = $dao -> CheckExistActif('hopital', 'ID_HOPITAL', 'ID_CENTRE = '.$data -> nomCentre);		
		ResetActifById($dao, $checkedId, 0);
	}

	$SoU = $dao -> saveOrUpdateHopital(
		$data -> idHopital,
		1,
		$data -> pourcentageEmployeHopital,
		$data -> pourcentageEmployeurHopital,
		$data -> datePriseEffetHopital,
		$val_actif,
		$data -> nomCentre
	);
	echo $SoU;
}

/*
 * @$checkedId est un tableau de IDs
 * utiliser la méthode 'implode' pour les convertir en string et séparées les valeurs par des virgules
 * Envoyer les ids vers la condition WHERE de la requête qui fait la mise à jour
 */
function ResetActifById($dao, $checkedId, $ActifValue) {
	$result = $dao -> array_flatten($checkedId);

	$mystring = implode(', ', $result);
	$R = $dao ->  ResetActifValue('hopital', 'ACTIF_HOPITAL', $ActifValue, 'ID_HOPITAL', $mystring);
	return $R;
}

/*
 * Supprimer une ligne dans table "hopital" où ID_HOPITAL = ($data -> idHopital)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'hopital',
		'ID_HOPITAL',
		$data -> idHopital
	);
	echo $SoU;
}
?>
