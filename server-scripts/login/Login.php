<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

$nomUtilisateur = $_POST['nomUtilisateur'];
$motDePasse = $_POST['motDePasse'];
$centreAffectation = $_POST['centreAffectation'];

$r=$m->verifLogin($nomUtilisateur, $motDePasse, $centreAffectation);
if (count($r) > 0) {
?>
	{
		'success': true,
		'nomUtilisateur': '<?php echo $r[0][1];?>',
		'centreAffectation': '<?php echo $r[0][3];?>',
		'typeUtilisateur': '<?php echo $r[0][4];?>',
		'avatar': '<?php echo $r[0][6];?>',
	}
<?php	
}
else {
?>
	{'failure':true}
<?php
}
?>
