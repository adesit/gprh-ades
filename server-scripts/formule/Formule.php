<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Table = 'formule'
 * Insertion si idFormule est 'null'
 * Modification si idFormule != null  
 */
function SaveOrUpdate($dao, $data) {
	$val_actif = ($dao -> IsNullOrEmptyString(isset($data -> actifFormule)) > 0 ? 0 : 1);

	$SoU = $dao -> saveOrUpdateFormule(
		$data -> idFormule,
		$data -> idAdes,
		$data -> resultat,
		$data -> formuleCalcul,
		$data -> dateApplication,
		$val_actif,
		$data -> descriptionFormule
		
	);
	echo $SoU;
}

/*
 * Table = 'formule'
 * Suppression si idFormule = valeur  
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'formules',
		'ID_FORMULE',
		$data -> idFormule
	);
	echo $SoU;
}
?>
