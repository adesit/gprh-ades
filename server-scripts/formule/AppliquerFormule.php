<?php
class AppliquerFormule
{
	function __construct()
	{}

	function getLastSalaireBase($dao, $h, $u, $annee_derniere, $mois_dernier) {
		$where = "WHERE p.ID_EMPLOYE = $h AND s.MOIS_SALAIRE = $mois_dernier AND s.ANNEE_SALAIRE = $annee_derniere";
		$historiqueSalBaseDernier = $dao -> getHistoriqueSalaire($where);
		if (empty($historiqueSalBaseDernier)) return $u;
		else return $historiqueSalBaseDernier[0][10];
	}

	function findSalaireBaseByAnciennete($dao, $poste, $indice, $grille_indice, $suspendre_augmentation) {
		$grille_salaire_base = ["SALAIRE_BASE_MINIMUM", "SALAIRE_BASE_3", "SALAIRE_BASE_4", "SALAIRE_BASE_5"];
		$indice_final = ($indice == 0) ? 0 : $indice - 1;
		$colonne_categorie = $grille_indice[$indice]; // le changement de catégorie de l'employé commence en début de janvier de l'année en cours, donc cette valeur pointe directement au jour de l'anniversaire du contrat

		$ligne_grille = $dao -> getCategorieByPosteAnciennete($poste, $colonne_categorie);
		$categorie = $ligne_grille[0][3];
		$tauxHoraire = $ligne_grille[0][4];
		$nouveauIdCategorie = $ligne_grille[0][5];
		$categorieReelle = $ligne_grille[0][6]; // Catégories réelles séparées par les point-virgules
		$grille_indice;
		//TODO réduire d'un an l'indice
		$indice = ($suspendre_augmentation > 0) ? $indice - 1 : $indice;
		if ($indice < 3) {
			$grille_indice = 0;
		} else if ($indice >= 3 && $indice < 4) {
			$grille_indice = 1;
		} else if ($indice >= 4 && $indice < 5) {
			$grille_indice = 2;
		} else if ($indice >= 5) {
			$grille_indice = 3;
		}
		$salaire_correspondant = $dao -> getSalaireByCategoriAnciennete($categorie, $grille_salaire_base[$grille_indice]);
		
		return [$categorie, $salaire_correspondant[0][0], $tauxHoraire, $nouveauIdCategorie, $categorieReelle];
	}
	
	function appliquerAugmentationGenerale($limit, $difference_pourcent, $SB_conclu) {
		$augmentation;
		if ($difference_pourcent <= 0) {
			$taux_augmentation_quinze = 0;
			$augmentation = 0;
		} else if ($difference_pourcent >0 && $difference_pourcent <= $limit) {
			$taux_augmentation_quinze = $difference_pourcent / 100;
			$augmentation = ($difference_pourcent / 100) * $SB_conclu;
		} else if ($difference_pourcent > $limit) {
			$taux_augmentation_quinze = $limit;
			$augmentation = ($limit / 100 ) * $SB_conclu;
		}
		return [$augmentation, $taux_augmentation_quinze];
	}
	
	function calculSalaireBaseGenerale($categorie, $SB_conclu, $anomalie_salaire, $augmentation, $salaire_debut_janvier) {
		$salaireBaseQuinze;
		if (strpos($categorie, 'HC') !== false) { // si catégorie_professionnelle = HC, prendre le max ($SB_conclu, $salaire_debut_janvier)
			$salaireBaseQuinze = max($salaire_debut_janvier, $SB_conclu);
		} else { // si non (autre que HC)
			if ($anomalie_salaire == true) $salaireBaseQuinze = $SB_conclu; // si mention '$anomalie_salaire = true' prendre son salaire $SB_conclu
			else if ($augmentation > 1) { // Une augmentation est attendue
				$salaireBaseQuinze = $SB_conclu + $augmentation;
			} else $salaireBaseQuinze = $salaire_debut_janvier;
		}
		return $salaireBaseQuinze;	
	}
	
	function anomalieSalaire($categorie, $difference_annee_derniere_actuelle) {
		$anomalieSalaire;
		// echo '<br>difference_annee_derniere_actuelle AB: '.$difference_annee_derniere_actuelle;
		if (strpos($categorie, 'HC') !== false) {
			$anomalieSalaire = false;
		} else {
			if ($difference_annee_derniere_actuelle < 0) $anomalieSalaire = true;
			else $anomalieSalaire = false;
		}
		return $anomalieSalaire;
	}
	
	function verifierAnomalieSalaire($anomalie_salaire, $salaireBaseQuinze, $salaire_debut_janvier) {
		$diff = $salaire_debut_janvier - $salaireBaseQuinze;
		$trop_percu = ($anomalie_salaire === "true") ? $diff : 0;
		return $trop_percu;
	}

	function promotion($dao, $id_employe, $id_poste_actuel, $salaireBaseQuinze, $base_conclu_actuel, $annee_derniere) {
		$where = "WHERE p.ID_EMPLOYE = $id_employe AND s.MOIS_SALAIRE = 12 AND s.ANNEE_SALAIRE = $annee_derniere";
		$salaireAnneeDerniereDao = $dao -> getHistoriqueSalaire($where);
		$id_ancien_poste = empty($salaireAnneeDerniereDao) ? 0 : $salaireAnneeDerniereDao[0][52];
		if (count($salaireAnneeDerniereDao) > 0 && $id_ancien_poste != $id_poste_actuel) { // promotion
			$nouveauSalaireBase = ($base_conclu_actuel > $salaireBaseQuinze) ? $base_conclu_actuel : $salaireBaseQuinze;
		}
		else {
			$nouveauSalaireBase = $salaireBaseQuinze;
		}
		return $nouveauSalaireBase;
	}
	
	function appliquerAugmentationInflation($annee_en_cours, $limit2, $date_prise_poste, $difference_annee_derniere_actuelle_pourcent_decimal, $salaireBaseQuinze, $date_embauche) {
		$taux_augmentation_cinq;
	
		if (($annee_en_cours == $date_prise_poste->format("Y") && $date_embauche->format("Y") >= $annee_en_cours) || ($difference_annee_derniere_actuelle_pourcent_decimal > $limit2)) { // si la date de prise de poste est au cours de l'année actuelle, OU si augmentation de 15% > limitInflation (=5%) => prendre 0%
			$taux_augmentation_cinq = 0;
		} else if ($annee_en_cours == $date_prise_poste->format("Y") && $date_embauche->format("Y") < $annee_en_cours) { // si la date de prise de poste est au cours de l'année actuelle, MAIS c'est une promotion en interne
			if ($difference_annee_derniere_actuelle_pourcent_decimal < $limit2 && $difference_annee_derniere_actuelle_pourcent_decimal >= 0) {
				$taux_augmentation_cinq = $limit2 - $difference_annee_derniere_actuelle_pourcent_decimal;
			} else if ($difference_annee_derniere_actuelle_pourcent_decimal < $limit2 && $difference_annee_derniere_actuelle_pourcent_decimal <= 0) {
				$taux_augmentation_cinq = $limit2;
			} else $taux_augmentation_cinq = 0;
		} else {
			if ($difference_annee_derniere_actuelle_pourcent_decimal <= 0) {
				$taux_augmentation_cinq = $limit2;
			} else if ($difference_annee_derniere_actuelle_pourcent_decimal > 0 && $difference_annee_derniere_actuelle_pourcent_decimal <= $limit2) {
				$taux_augmentation_cinq = $limit2 - $difference_annee_derniere_actuelle_pourcent_decimal;
			} 
		}
			// echo '<br>taux augmentation cinq: '. $taux_augmentation_cinq;
		$salaire_supplementaire = ($taux_augmentation_cinq / 100) * $salaireBaseQuinze;
		return round($salaire_supplementaire);
	}
	
	function calculSalaireBaseInflation($salaireBaseQuinze, $augmentation_salaire_cinq) {
		return ($salaireBaseQuinze + $augmentation_salaire_cinq);
	}
	
	function anciennete($date_debut, $date_fin, $inf_sup) {
		$anciennete = $date_debut -> diff($date_fin);
		$anciennete = $anciennete -> y . '.' . $anciennete -> m;
		// echo '<br> ancienneté: '. $anciennete;
		$anciennete_arrondi;
		if ($inf_sup < 0) {
			$anciennete_arrondi = floor($anciennete);
			// echo '<br> anciennete_arrondi_inf: '.$anciennete_arrondi;
		} else if ($inf_sup > 0) {
			$anciennete_arrondi = ceil($anciennete);
			// echo '<br> anciennete_arrondi_sup: '.$anciennete_arrondi;
		}
		return $anciennete_arrondi;
	}

	function calculSalaireSupplementaire($dao, $h, $annee_en_cours, $augmentation_salaire_cinq, $difference_annee_derniere_actuelle_pourcent_decimal, $limit15, $annee_derniere, $salaire_base, $d, $salaireBaseCinq, $taux_augmentation_salaire_quinze, $date_embauche, $mois_dernier, $tranche1, $taux1, $tranche2, $taux2, $taux3) {
		
		$where = "WHERE p.ID_EMPLOYE = $h AND s.MOIS_SALAIRE = $mois_dernier AND s.ANNEE_SALAIRE = $annee_derniere";
		$salaireAnneeDerniere = $dao -> getHistoriqueSalaire($where);
		$salaireBaseCinqAnneeDerniere = empty($salaireAnneeDerniere) ? 0 : $salaireAnneeDerniere[0][74];
		$id_ancien_poste = empty($salaireAnneeDerniere) ? 0 : $salaireAnneeDerniere[0][52];

		if ($annee_en_cours != '2018') {
			// =SI(Q3<250000;Q3*1,15;(SI(Q3<300000;Q3*1,08;Q3*1,05)))

			if ($salaireBaseCinqAnneeDerniere <= $tranche1) {
				$taux_2019 = $taux1;
				$salaireAvecAugmentation2019 = $salaireBaseCinqAnneeDerniere * (($taux_2019/100)+1);
			}
			else if ($salaireBaseCinqAnneeDerniere > $tranche1 && $salaireBaseCinqAnneeDerniere <= $tranche2) {
				$taux_2019 = $taux2;
				$salaireAvecAugmentation2019 = $salaireBaseCinqAnneeDerniere * (($taux_2019/100)+1);
			}
			else {
				$taux_2019 = $taux3;
				// echo '<br>salaireBaseCinqAnneeDerniere :'.$salaireBaseCinqAnneeDerniere;
				$salaireAvecAugmentation2019 = $salaireBaseCinqAnneeDerniere * (($taux_2019/100)+1);
			}

			// si salaire dernière année > nouveau salaire: pas d'inflation
			// si augmentation 15% actuel > limit : pas de d'augmentation 2019
			// si changement de poste mais aucun changement de salaire de base, appliquer l'augmentation inflation
			
			$augmentation2019 = $this -> regleChangementPoste($id_ancien_poste, $d, $salaireBaseCinqAnneeDerniere, $salaire_base, $augmentation_salaire_cinq, $difference_annee_derniere_actuelle_pourcent_decimal, $limit15, $salaireAvecAugmentation2019);
		} else $augmentation2019 = $augmentation_salaire_cinq;
		
		// echo '<br>augmentation2019: '.$augmentation2019;

		if (($date_embauche->format("Y") != $annee_en_cours)) {
			$sbrutFictif = $augmentation2019 + $salaireBaseCinq;
			// echo '<br>sbrutFictif: '.$sbrutFictif.'<br>';
			$differenceBrut = ($date_embauche->format("Y") == $annee_en_cours) ? 0 : (($sbrutFictif - $salaireBaseCinqAnneeDerniere)/$salaireBaseCinqAnneeDerniere)*100;
			$differenceBrut_pourcent = number_format($differenceBrut, 10);
			
			// ATTENTION  || $differenceBrut_pourcent < 0 ne devrait pas être là normalement, mais le changement de tranche de salaire va causer la dimunition du salaire supplémentaire
			$plafondAugmentation = ($differenceBrut_pourcent >= $taux_2019 || $differenceBrut_pourcent < 0) ? $taux_2019 : $differenceBrut_pourcent;
			// echo '<br>plafondAugmentation: '.$plafondAugmentation.'<br>';
			
			// echo '<br>taux_augmentation_salaire_quinze: '.number_format($taux_augmentation_salaire_quinze, 10).'<br>';
			$nouveau_taux_augmentation = ($date_embauche->format("Y") == $annee_en_cours) ? 0 : $plafondAugmentation - (number_format($taux_augmentation_salaire_quinze, 10) * 100);
			// echo '<br>nouveau_taux_augmentation: '.$nouveau_taux_augmentation.'<br>';
			$augmentation_reelle = ($nouveau_taux_augmentation <= 0) ? 0 : ($nouveau_taux_augmentation * $salaireBaseCinqAnneeDerniere) / 100 ;

			// Si augmentation de l'année dernière >= 15% OU année d'embauche = année dernière, donc seulement $augmentation_reelle = augmentation_inflation de cette annéee
			// Si non $augmentation_reelle + $augmentation_salaire_cinq;
			$ancien_salaire_base = $salaireAnneeDerniere[0][8];
			$ancien_salaire_base_grille = $salaireAnneeDerniere[0][9];
			$diff_base_annee_derniere = $ancien_salaire_base_grille - $ancien_salaire_base;
			$augmentation_annee_derniere = ($diff_base_annee_derniere <= 0 || $ancien_salaire_base <= 0) ? 0 : ($diff_base_annee_derniere / $ancien_salaire_base * 100);
			$taux_augmentation_annee_derniere = number_format($augmentation_annee_derniere, 10);
			// echo '<br>salaireBaseCinqAnneeDerniere: '.$salaireBaseCinqAnneeDerniere.'<br>';
			// echo '<br>nouveau_taux_augmentation: '.$nouveau_taux_augmentation.'<br>';

			if ($date_embauche->format("Y") == $annee_derniere || $taux_augmentation_annee_derniere > 15) {
				// echo '<br>miditra 1:';
				$supplementaire2019 = $augmentation_reelle;
			} else if ($date_embauche->format("Y") != $annee_derniere && $difference_annee_derniere_actuelle_pourcent_decimal == 0) {
				// echo '<br>miditra 2';
				$supplementaire2019 = $salaireBaseCinqAnneeDerniere * $nouveau_taux_augmentation / 100;
			} else if ($date_embauche->format("Y") != $annee_derniere && $id_ancien_poste != $d && ($salaireBaseCinqAnneeDerniere + $augmentation_reelle) < $salaire_base) { // Changement de poste mais son salaire de base = (son salaire brut qu'il aurait dû avoir) avec l'ancien poste
				// echo '<br>miditra 3';
				$supplementaire2019 = 0;
			} else {
				// echo '<br>miditra 4';
				$supplementaire2019 = $augmentation_reelle;
			}

			// echo '<br>supplementaire2019: '.round($supplementaire2019).'<br>';
		} else $supplementaire2019 = $salaire_base;
		
		
		return [round($supplementaire2019), $salaireBaseCinqAnneeDerniere];
	}

	function regleChangementPoste($id_ancien_poste, $id_nouveau_poste, $salaireBaseCinqAnneeDerniere, $salaire_base, $augmentation_salaire_cinq, $difference_annee_derniere_actuelle_pourcent_decimal, $limit15, $salaireAvecAugmentation2019) {
		$augmentation_inflation = ($id_ancien_poste != $id_nouveau_poste && $salaireBaseCinqAnneeDerniere < $salaire_base) ? 0 : $augmentation_salaire_cinq;
		if ($difference_annee_derniere_actuelle_pourcent_decimal > $limit15) {
			// echo 'miditra1<br>';
			$suppl = $augmentation_inflation;
		} else if ($id_ancien_poste != $id_nouveau_poste){
			/* echo 'miditra2<br>';
			echo $salaireBaseCinqAnneeDerniere.' salaireBaseCinqAnneeDerniere<br>';
			echo $salaire_base.' salaire_base<br>'; */
			if ($salaireBaseCinqAnneeDerniere == $salaire_base) {
				// echo 'miditra3<br>';
				$suppl = ($salaireAvecAugmentation2019 - $salaireBaseCinqAnneeDerniere) + $augmentation_salaire_cinq;
			} else {
				// echo 'miditra4<br>';
				$suppl = 0;
			}

		} else {
			// echo 'miditra5';
			$suppl = ($salaireAvecAugmentation2019 - $salaireBaseCinqAnneeDerniere) + $augmentation_inflation;
		}
		//echo '<br>suppl: '.$suppl;
		return $suppl;
	}
	
	function calulerSalaireInterim($date_debut_interim, $date_fin_interim, $date_debut_mois, $date_fin_mois, $dao, $salaire_base, $remarque_interim){
		// Le paiement peut être effectué si le nombre est de 14jours au moins
		$intStartPaiementInterim = date('Y-m-d', strtotime($date_debut_interim. ' + 14 days')); 
		$c1 = $date_debut_mois;
		$c2 = $date_fin_mois;
		$p = new DateTime($intStartPaiementInterim);
		$o = new DateTime($date_debut_interim);
		$r = new DateTime($date_fin_interim);
		
		$jours_mois_cours;
		if ($dao -> IsNullOrEmptyString($date_debut_interim) || $dao -> IsNullOrEmptyString($date_fin_interim) || $p > $c2 || $r < $c1) {
			$jours_mois_cours = 0;
		} else {
			if ($r < $c2) {
				if ($p < $c2 && $r <= $c2) { // Les jours début et fin appartient au même mois
					$jours_mois_cours = $p -> diff($r);
					$jours_mois_cours = $jours_mois_cours -> d;
				} else {
					$jours_mois_cours = date('d', date_timestamp_get($r));
				}
			} else {
				if ($p < $c1 && $r >= $c2) {
					$jours_mois_cours = date('d', date_timestamp_get($c2));
				} else{
					$jours_mois_cours = $p -> diff($c2);
					$jours_mois_cours = $jours_mois_cours -> d;
				}
			}
		}
	
		$montant = ($jours_mois_cours > 0) ? ($salaire_base/date('d', date_timestamp_get($c2))) * $jours_mois_cours * 30/100 : 0;
		$remarque_motif = ($jours_mois_cours > 0) ? $remarque_interim : '';
		return [$montant, $remarque_motif];
	}
	
	function calculHS($categorie_actuelle, $HSList, $tauxHs, $tauxHoraire, $d, $salaireBrutInclInflation, $salaire_supplementaire, $annee_en_cours) {
		$remarque = '';
		if ($annee_en_cours !== '2018') {
			if (strpos($categorie_actuelle, 'HC') !== false || (empty($HSList) && ($d !== '5')))  return [0, 0, 0, '']; // Test si la catégorie actuelle n'est pas HC
			else if ($d == '5') { // Test si le poste est 'Agent d'entretient et de sécurité', calculer un forfetaire de 3.7% de son salaire de base
				$totalHeure = empty($HSList) ? 0 : $HSList[0][2] + $HSList[0][3] + $HSList[0][4] + $HSList[0][5] + $HSList[0][6] + $HSList[0][7];
				
				$totalMontantHS = $salaireBrutInclInflation * 3.7 / 100;
				// echo $salaireBrutInclInflation;
				$idHS = empty($HSList) ? 0 : $HSList[0][0];
				$remarque .= empty($HSList) ? '' : ' '.$HSList[0][8];
				return [$totalMontantHS, $totalHeure, $idHS, $remarque];
			} else {
				/*
					* Dans cette méthode, on récupére toutes les majorations actives pour chaque type d'heure supplémentaire
					* data: [
					{ nomTaux: 'HSup < 8heures', id: 1 }, <=> $tauxHs[0][2]
					{ nomTaux: 'HSup > 8heures', id: 2 }, <=> $tauxHs[1][2]
					{ nomTaux: 'HSup pour des nuits habituelles', id: 3 }, <=> $tauxHs[2][2]
					{ nomTaux: 'HSup pour des nuits occasionnelles', id: 4 }, <=> $tauxHs[3][2]
					{ nomTaux: 'HSup pour des dimanches', id: 5 }, <=> $tauxHs[4][2]
					{ nomTaux: 'HSup pour des jours fériés', id: 6 }, <=> $tauxHs[5][2]
				],
					* Les données des HS pour un employé commence à l'indice 3 donc il faut lier les tableaux des tauxHS et le tableau des données par employé
					* array 
					0 => 
						array 
						2 => h.INF_HUIT_HEURES
						3 => h.SUP_HUIT_HEURES
						4 => h.NUIT_HABITUELLE
						5 => h.NUIT_OCCASIONNELLE
						6 => h.DIMANCHE
						7 => h.FERIE
					*/
				$infHuitHeures = empty($HSList) ? 0 : $HSList[0][2] * ($tauxHs[0][2] / 100);
				$supHuitHeures = empty($HSList) ? 0 : $HSList[0][3] * ($tauxHs[1][2] /100);
				$nuitHabituelle = empty($HSList) ? 0 : $HSList[0][4] * ($tauxHs[2][2] /100);
				$nuitOccasionnelle = empty($HSList) ? 0 : $HSList[0][5] * ($tauxHs[3][2] /100);
				$dimanche = empty($HSList) ? 0 : $HSList[0][6] * ($tauxHs[4][2] /100);
				$ferie = empty($HSList) ? 0 : $HSList[0][7] * ($tauxHs[5][2] /100);
		
				$totalMontantHS = ($infHuitHeures + $supHuitHeures + $nuitHabituelle + $nuitOccasionnelle + $dimanche + $ferie) * $tauxHoraire;

				$totalHeure = empty($HSList) ? 0 : $HSList[0][2] + $HSList[0][3] + $HSList[0][4] + $HSList[0][5] + $HSList[0][6] + $HSList[0][7];

				$idHS = empty($HSList) ? 0 : $HSList[0][0];

				$remarque .= empty($HSList) ? '' : ' '.$HSList[0][8];
		
				return [$totalMontantHS, $totalHeure, $idHS, $remarque];
			}
		}
		else {
			if (strpos($categorie_actuelle, 'HC') !== false || empty($HSList))  return [0, 0, 0, '']; // Test si la catégorie actuelle n'est pas HC
			else {
				/*
					* Dans cette méthode, on récupére toutes les majorations actives pour chaque type d'heure supplémentaire
					* data: [
					{ nomTaux: 'HSup < 8heures', id: 1 }, <=> $tauxHs[0][2]
					{ nomTaux: 'HSup > 8heures', id: 2 }, <=> $tauxHs[1][2]
					{ nomTaux: 'HSup pour des nuits habituelles', id: 3 }, <=> $tauxHs[2][2]
					{ nomTaux: 'HSup pour des nuits occasionnelles', id: 4 }, <=> $tauxHs[3][2]
					{ nomTaux: 'HSup pour des dimanches', id: 5 }, <=> $tauxHs[4][2]
					{ nomTaux: 'HSup pour des jours fériés', id: 6 }, <=> $tauxHs[5][2]
				],
					* Les données des HS pour un employé commence à l'indice 3 donc il faut lier les tableaux des tauxHS et le tableau des données par employé
					* array 
					0 => 
						array 
						2 => h.INF_HUIT_HEURES
						3 => h.SUP_HUIT_HEURES
						4 => h.NUIT_HABITUELLE
						5 => h.NUIT_OCCASIONNELLE
						6 => h.DIMANCHE
						7 => h.FERIE
					*/
				$infHuitHeures = empty($HSList) ? 0 : $HSList[0][2] * ($tauxHs[0][2] / 100);
				$supHuitHeures = empty($HSList) ? 0 : $HSList[0][3] * ($tauxHs[1][2] /100);
				$nuitHabituelle = empty($HSList) ? 0 : $HSList[0][4] * ($tauxHs[2][2] /100);
				$nuitOccasionnelle = empty($HSList) ? 0 : $HSList[0][5] * ($tauxHs[3][2] /100);
				$dimanche = empty($HSList) ? 0 : $HSList[0][6] * ($tauxHs[4][2] /100);
				$ferie = empty($HSList) ? 0 : $HSList[0][7] * ($tauxHs[5][2] /100);
		
				$totalMontantHS = ($infHuitHeures + $supHuitHeures + $nuitHabituelle + $nuitOccasionnelle + $dimanche + $ferie) * $tauxHoraire;

				$totalHeure = empty($HSList) ? 0 : $HSList[0][2] + $HSList[0][3] + $HSList[0][4] + $HSList[0][5] + $HSList[0][6] + $HSList[0][7];
				
				$idHS = empty($HSList) ? 0 : $HSList[0][0];

				$remarque .= empty($HSList) ? '' : ' '.$HSList[0][8];

				return [$totalMontantHS, $totalHeure, $idHS, $remarque];
			}
		}
	}
	
	function calculHMinus($AbsenceList, $tauxHoraire) {
		$remarque = '';
		if (empty($AbsenceList))  return [0, '', 0];
		else {
			$sum = 0;
			foreach ($AbsenceList as $item) {
				$sum += $item[6];
				$remarque .= ' '.$item[3];
			}
			$totalMontantHS = $sum * $tauxHoraire  * -1;
	
			return [$totalMontantHS, $remarque, $sum];
		}
	}
	
	function calculMinusCongeMaternite($congeMaterniteList, $date_fin_mois, $date_debut_mois, $mois_en_cours, $nb_jour_mois_en_cours) {
		if (empty($congeMaterniteList))  return 0;
		else {
			$date_debut_conge_maternite = strtotime($congeMaterniteList[0][4]);
			$date_fin_conge_maternite = strtotime($congeMaterniteList[0][5]);
			$jour_debut_conge = date('d', $date_debut_conge_maternite);
			$jour_fin_conge = date('d', $date_fin_conge_maternite);
			$mois_debut_conge = date('m', $date_debut_conge_maternite);
			$mois_fin_conge = date('m', $date_fin_conge_maternite);
			
			$nb_jour_payer;
	
			if ($mois_debut_conge === $mois_en_cours) $nb_jour_payer = $nb_jour_mois_en_cours - $jour_debut_conge;
			else if ($mois_fin_conge === $mois_en_cours) $nb_jour_payer = $jour_fin_conge;
			else if ($date_debut_conge_maternite < date_timestamp_get($date_debut_mois) && $date_fin_conge_maternite > date_timestamp_get($date_fin_mois)) $nb_jour_payer = $nb_jour_mois_en_cours;
			
			return $nb_jour_payer;
		}
	}
	
	function pceil($n, $pres) {
		$i = (int) ($n / $pres); // retourne la division entière de $n par $pres
		return $i * $pres; // multiplie le résultat par la précision
	}
	
	function calculIRSA($salaire_brut_apres_cnaps_smie, $irsa, $nbEnfant, $abattement) {
		$salaireBrutApresCnapsSmieArrondi = $this -> pceil($salaire_brut_apres_cnaps_smie, 1000); // 1!AV
		$montantIrsaBase = ($salaireBrutApresCnapsSmieArrondi > $irsa[0][2]) ? $irsa[0][2] : $salaireBrutApresCnapsSmieArrondi;
		$montantIrsaApresPlafond = $salaireBrutApresCnapsSmieArrondi - $montantIrsaBase; // 1!AX
		$montantIrsaTaux = $montantIrsaApresPlafond * $irsa[0][3] / 100; // 1!AY
		$montantIrsaTauxArrondi = $this -> pceil($montantIrsaTaux, 1000); // 1!AZ
	
		$abattementIrsa = $nbEnfant * $abattement; // 1!BB
		$irsaNet = $montantIrsaTauxArrondi - $abattementIrsa;
		$irsaNetAffiche = ($irsaNet <= $abattement) ? $abattement : $irsaNet;
		return [$salaireBrutApresCnapsSmieArrondi, $montantIrsaApresPlafond, $montantIrsaTauxArrondi, $abattementIrsa, $irsaNetAffiche];
	}
	
	function calculCaisseHospitalisation($hopital, $aj, $salaire_brut_final) {
		$montantCaisseHospitalisation = ($aj > 0) ? 0 : $salaire_brut_final * ($hopital / 100);
		return $montantCaisseHospitalisation;
	}
	
	function calculFraisMedicaux($idContrat, $idCentre, $FraisMedicauxData, $dao) {
		// Les employés de centres 'DN, DRU, Ejeda' participent à 10% de la totalité de leur frais médicaux non couverts par les centres d'affiliation médicale
		$idCentres = ["2", "6", "7"];
		$sum = 0;
		$remarque = '';
			foreach ($FraisMedicauxData as $item) {
				$sum += $item[4];
				$remarque .= ' '.$item[5];
			}
		$montantFacture = empty($FraisMedicauxData) ? 0 : $sum;
		$tauxParticipation;
		
		if (in_array($idCentre, $idCentres, TRUE)) {
			$whereFM = "WHERE h.ID_CENTRE = $idCentre AND h.ACTIF_HOPITAL = 1"; // Récupère le taux de participation pour les frais médicaux (eg. 10%)
			$tauxParticipationMedicale = $dao -> getHopitalList($whereFM);
			$tauxParticipation = $tauxParticipationMedicale[0][2];
			$montantParticipation = $montantFacture * $tauxParticipationMedicale[0][2] / 100;
			$idParticipation = $tauxParticipationMedicale[0][0];
		} else {
			$tauxParticipation = 0;
			$montantParticipation = 0;
			$idParticipation = 0;
		}
	
		return [$montantFacture, $tauxParticipation, $montantParticipation, $idParticipation, $remarque];
	}
	
	function calculDiversRetenus($DRList) {
		$sum = 0;
		$remarque = '';
			foreach ($DRList as $item) {
				$sum += $item[3];
				$remarque .= ' '.$item[5];
			}
		$montantFacture = empty($DRList) ? 0 : $sum;
		return [$montantFacture, $remarque];
	}
	
	function calculDepassement($DPSList) {
		$sum = 0;
		$remarque = '';
			foreach ($DPSList as $item) {
				$sum += $item[3];
				$remarque .= ' '.$item[4];
			}
		$montantFacture = empty($DPSList) ? 0 : $sum;
		return [$montantFacture, $remarque];
	}
	
	function calculAvance($AvList, $dao) {
		$sum = 0;
		$remarque = '';
		if (!empty($AvList)) {
			$idAvance = $AvList[0][0];
			$montantAvance = $AvList[0][4];
			$deductionMensuelle = $AvList[0][9];
			$remarque .= ' '.$AvList[0][10];
			$remboursementList = $dao -> getRemboursement($idAvance);
			foreach ($remboursementList as $item) {
				$sum += $item[3]; // Faites la somme de tous les remboursements déjà effectués
			}
			$attendu = $sum + $deductionMensuelle;
			if ($attendu >= $montantAvance) { // Est-ce que la totalité des remboursements + déduction mensuelle excède le montant d'avance demandée
				$montantAvanceDeCeMois = $montantAvance - $sum; // Si oui, payer le reste
			} else {
				$montantAvanceDeCeMois = $deductionMensuelle; // Si non, payer la déduction mensuelle
			}
		} else {
			$montantAvanceDeCeMois = 0;
			$remarque .= '';
			$sum = 0;
		}
		return [$montantAvanceDeCeMois, $remarque, $sum];
	}
	
	function calculSoldeConge($soldeCongeDao, $date_fin_mois, $droitConge, $totalCongePris) {
		// Cette fonction calcule le solde de congé par rapport au dernier mise à jour du reliquat
		$date_releve = $soldeCongeDao[0][3];
		$date_dernier_releve = new DateTime($date_releve);
		$diff_mois = $date_dernier_releve -> diff($date_fin_mois);
			
		$diff =  $date_dernier_releve->diff($date_fin_mois);

		$months = $diff->y * 12 + $diff->m + $diff->d / 30;

		// si la période travaillé dépasse la moitié mais ne complète pas 1 mois, lui accorder la moitié de son droit
		// $interval = (fmod($months, 1) >= 0.5 && fmod($months, 1) <= 0.7) ? 0.5 : (int) round($months);

		$interval = (int) round($months); //$diff_mois ->format("%R%m");
		
		$soldeConge = $soldeCongeDao[0][2] + ($droitConge * $interval) - $totalCongePris;

		$mois_en_cours = $date_fin_mois->format('m');

		// si le mois est JUIN, réduire à 45 tous les soldes >= 45 et ensuite, continuer le compteur avec le droit jusqu'au prochain mois de JUIN
		$soldeConge_juin = ($mois_en_cours == 6 && $soldeConge >= 45) ? 45 : $soldeConge;

		return $soldeConge_juin;
	}
	
	function calculCongePris($CongeList) {
		$sum = 0;
		if (!empty($CongeList)) {
			foreach ($CongeList as $item) {
				$sum += $item[7]; // Faites la somme de tous les congés pris pour le mois de salaire
			}
		} else {
			$sum = 0;
		}
		return $sum;
	}

	function recupererCategorieRelle($categorie_actuelle, $cat_poste_actuelle) {
		$cats = explode(";", $cat_poste_actuelle); // Explode les catégories réelles séparées par des ";" dans la table poste
		$categorieReelleArray = array();
		foreach($cats as $cat) {
			$cat = trim($cat);
			if (strpos($categorie_actuelle, $cat) !== false) {
				array_push($categorieReelleArray, $cat); // Ajoute dans le tableau categorieReelleArray la valeur trouvée
			}
		}
		$categorieReelle = array_filter($categorieReelleArray, function ($var) { return (strlen($var)>0); }); // filtre 
		
		return empty($categorieReelle) ? '' : $categorieReelle[0];
	}

	function calculSalaireBrutIncInflationProrata($salaireBrutInclInflation, $heure_reglementaire, $date_poste_actuel, $date_debut_mois, $date_fin_mois) {
		$diff_date = $date_poste_actuel->diff($date_fin_mois);
		// $prorata = $diff_date ->format("%d");
		$holidays = array();
		$prorata = $this -> getWorkingDays($date_poste_actuel,$date_fin_mois,$holidays);
		$salaire_journalier = ($salaireBrutInclInflation / $heure_reglementaire) * 8;
		return $prorata * $salaire_journalier;
	}

	function calculRappel($RappelList) {
		$sum = 0;
		$remarque = '';
			foreach ($RappelList as $item) {
				$sum += $item[3];
				$remarque .= ' '.$item[7];
			}
		$montantRappel = empty($RappelList) ? 0 : $sum;
		return [$montantRappel, $remarque];
	}

	/*
	 * Cette calcul le nombre de jours entre 2 dates
	 * Excluant les weekends
	 */
	function getWorkingDays($startDate,$endDate,$holidays) {
		$endDate = $endDate->getTimestamp();
		$startDate = $startDate->getTimestamp();

		// $diff = $endDate->diff($startDate);
		// Calcule ne nombre de jours entre 2 dates. 
		// Calcule le (nombre de secondes)/ 60*60*24
		$days = ($endDate - $startDate) / 86400 + 1; // +1 inclut les 2dates dans l'intervalle.
		$no_full_weeks = floor($days / 7);
		$no_remaining_days = fmod($days, 7);
		
		$the_first_day_of_week = date("N", $startDate); // Return 1 if si Lundi,.. ,7 pour Dimanche
		$the_last_day_of_week = date("N", $endDate);
		//---->Les deux peuvent être égaux les années bissextiles quand février a 29 jours, le signe égal est ajouté ici
		// Dans le premier cas, tout l'intervalle est inférieur à une semaine, dans le second, il tombe à deux semaines.
		if ($the_first_day_of_week <= $the_last_day_of_week) {
			if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
			if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
		}
		else {
			// (définit si le jour de départ était un dimanche
			// et le jour de la fin n'était PAS un samedi)
			// le jour de la semaine pour le début est postérieur au jour de la semaine pour la fin
			if ($the_first_day_of_week == 7) {
				// si la date de début est un dimanche, nous soustrayons 1 jour
				$no_remaining_days--;
				if ($the_last_day_of_week == 6) {
					// si la date de fin est un samedi, on soustrait un autre jour
					$no_remaining_days--;
				}
			}
			else {
				// la date de début était un samedi (ou avant) et la date de fin était (lundi..vendredi)
				// donc nous sautons un week-end entier et soustrayons 2 jours
				$no_remaining_days -= 2;
			}
		}
		// Le nombre de jours ouvrables est: (nombre de semaines entre les deux dates) * (5 jours ouvrables) + le reste
		//---->En février, aucune année bissextile n’a donné le reste mais 0 week-end calculé entre le premier et le dernier jour, c’est un moyen de remédier à la situation.
	   $workingDays = $no_full_weeks * 5;
		if ($no_remaining_days > 0 )
		{
		  $workingDays += $no_remaining_days;
		}
		// On soustrait les vacances
		foreach($holidays as $holiday){
			$time_stamp=strtotime($holiday);
			// Si les vacances ne tombent pas dans le week-end
			if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
				$workingDays--;
		}
		return $workingDays;
	}
	
	function calculSBrutTreizieme($salaire_brut_final_liste, $annee_en_cours, $date_prise_poste, $date_embauche) {
		$sum = 0;
		$remarque = '';
		foreach ($salaire_brut_final_liste as $item) {
			$sum += $item[20];
		}
		$montantSBrut = empty($salaire_brut_final_liste) ? 0 : $sum;

		if ($annee_en_cours == $date_embauche->format("Y")) { // Embauché au cours de l'année === Prorata
			$coefficient = count($salaire_brut_final_liste);
		} else {
			// $coefficient = 12;
			$coefficient = count($salaire_brut_final_liste);
		}
		// echo $montantSBrut.' '.$coefficient.'</br>';
		$coeff = ($coefficient <= 0) ? 1 : $coefficient;
		$montantMoeyenne = $montantSBrut / $coeff;
		$treizieme = $montantMoeyenne * $coeff / 12;
		
		return $treizieme;
	}
}
?>