<?php
require_once '../DataAccessObject.php';
require_once '../formule/AppliquerFormule.php';require '../vendor/autoload.php';

//include the classes needed to create and write .xlsx file
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Shared\StringHelper;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

$dao = new DataAccessObject();
$formule = new AppliquerFormule();

$model = $_REQUEST['model'];
$model = json_decode($model);
$moisSalarial = $model -> moisSalarial;
$tranche1 = $dao -> Montant($model -> trancheMax1);
$taux1 = $model -> taux1;
$tranche2 = $dao -> Montant($model -> trancheMax2);
$taux2 = $model -> taux2;
$taux3 = $model-> taux3;
$infoRequest = $_REQUEST['infoRequest'];
$action = $_REQUEST['action'];

if ($moisSalarial < 0) {
	$arr['data'] = array();
	$arr['total'] = 0;
	
	$arr = json_encode($arr);
	echo $arr;
} else {

	// ----------------------- Les paramètres dont j'aurais besoin
	/*
	* le mois salarial est paramétré par 
	* sa date début (e.g 01/11/2018),
	* sa date de fin (e.g 30/11/2018)
	* ses dates de traitement (e.g entre 20/11/2018 et au plus tard le 25/11/2018) ie entre le 19ème et le 24ème du mois
	* */

	$moisSalarialById = explode("-", $moisSalarial);
	$whereSalaire = "WHERE MOIS_SALAIRE = $moisSalarialById[1] AND ANNEE_SALAIRE = $moisSalarialById[0]";

	$moisSalarialID = $dao -> getSalaireList($whereSalaire);

	$date_du_jour = $moisSalarial."-24";
	// $date_du_jour = strtotime('2018-11-24'); // A changer par la date de traitement du salaire celui du 24ème du mois
	$date_du_jour = strtotime($date_du_jour);
	$annee_en_cours = date('Y', strtotime("this year", $date_du_jour));
	$mois_en_cours = date('m', strtotime("this month", $date_du_jour));
	$annee_derniere = ($mois_en_cours - 1) == 0 ? date('Y', strtotime("this year", $date_du_jour)) - 1 : date('Y', strtotime("this year", $date_du_jour)); // Normalement -1 parce que c'est l'année dernière
	$mois_dernier = ($mois_en_cours - 1) == 0 ? 12 : $mois_en_cours - 1;
	$date_debut_mois = new DateTime(date("Y-m-d",strtotime("first day of this month", $date_du_jour)));
	$date_fin_mois = new DateTime(date("Y-m-d",strtotime("last day of this month", $date_du_jour)));
	$nb_jour_mois_en_cours = date('d', date_timestamp_get($date_fin_mois));
	$date_debut_annee_derniere = "{$annee_derniere}-01-01";
	$date_fin_annee_derniere = new DateTime(date("Y-m-d",strtotime("last year December 31st", $date_du_jour)));
	$date_debut_annee_en_cours = new DateTime(date("Y-m-d",strtotime("this year January 1st", $date_du_jour)));
	// echo '<br>date_debut_annee_en_cours: '.$date_debut_annee_en_cours->format('Y-m-d');
	$date_fin_annee_en_cours = new DateTime(date("Y-m-d",strtotime("this year December 31st", $date_du_jour)));

	$grille_indice = ["UN", "DEUX", "TROIS", "QUATRE", "CINQ", "SIX", "SEPT", "HUIT", "NEUF", "DIX", "ONZE", "DOUZE", "TREIZE", "QUATORZE", "QUINZE", "SEIZE", "DIXSEPT", "DIXHUIT", "DIXNEUF", "VINGT"];

	if ($infoRequest == 1) {
		$where = "WHERE c.ACTIF_CONTRAT = 1 AND c.DATE_PRISE_POSTE <= '".$date_fin_mois->format('Y-m-d')."'";

		$date_limite_naissance = new DateTime(date("Y-m-d",strtotime("-21 year", $date_du_jour)));
		$date_limite_naissance = $date_limite_naissance->format('Y-m-d');
		$whereAgeEnfant = "WHERE DATE_NAISSANCE_ENFANT >= '$date_limite_naissance'";
	} else if ($infoRequest == 2) {
		$idContrat = $_REQUEST['idContrat']; // A envoyer lors du rowclic ou buton click event

		$where = "WHERE c.ACTIF_CONTRAT = 1 and c.ID_CONTRAT = $idContrat AND c.DATE_PRISE_POSTE <= '".$date_fin_mois->format('Y-m-d')."'";
		
		$date_limite_naissance = new DateTime(date("Y-m-d",strtotime("-21 year", $date_du_jour)));
		$date_limite_naissance = $date_limite_naissance->format('Y-m-d');
		$whereAgeEnfant = "WHERE DATE_NAISSANCE_ENFANT >= '$date_limite_naissance'";
	}

	$dataContrat = $dao -> getContratList($where, $whereAgeEnfant);
	$nb=count($dataContrat);

	$response = array();

	$whereTauxHS = "WHERE ACTIF_HS_BASE = 1";
	$tauxHs = $dao -> getTauxHSList($whereTauxHS);

	$taux_augmentation_generale = $model -> tauxAugmentation; // Taux de l'augmentation générale
	$taux_inflation = $model -> tauxInflation; // Taux de l'augmentation de l'inflation

	$taux_reduction_maternite = $dao -> getTauxReductionMaternite(); // Taux de réduction pour le congé de maternité

	$whereCnaps = 'WHERE ACTIF_CNAPS = 1';
	$cnaps = $dao -> getCnapsList2($whereCnaps);

	$whereIrsa = 'WHERE ACTIF_IRSA = 1';
	$irsa = $dao -> getIrsaList2($whereIrsa);

	$hopital = $dao -> getTauxCaisseHopital();

	$abattement = $dao -> getMontantAbattement(); // Montant pour l'abattement par enfant

	$decallageAvance = $dao -> getDecallagaAvance(); // Le décallage entre 2 avances

	/* BEGIN FOREACH CONTRAT */
	foreach ($dataContrat as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae, $af, $ag, $ah, $ai, $aj)) {

		// Poste actuel
			$poste = $d;
			$date_embauche = new DateTime($m); // K
			// Pour la simulation de la masse salariale de l'année prochaine ne pas compter les embauchés de cette année donc décaller leur date d'embauche d'une année
			$date_embauche = ($date_embauche->format("Y") >= $annee_en_cours) ? $date_embauche->modify('-1 year') : $date_embauche;
			// echo '<br> date_embauche: '. date_format($date_embauche, 'Y-m-d');
			$date_poste_actuel = new DateTime($n);
			// echo '<br> date_poste_actuel: '. date_format($date_poste_actuel, 'Y-m-d');

		// -------------------- Dernier salaire de base
			$SB_conclu = $formule -> getLastSalaireBase($dao, $h, $u, $annee_derniere, $mois_dernier); // P
			// echo '<br>SB_conclu 2017 P: '.$SB_conclu;
		
		// -------------------- Ancienneté du début au décembre dernier
			$anciennete_debut_dern_decembre = $formule -> anciennete($date_poste_actuel, $date_fin_annee_derniere, -1);
			// echo '<br>anciennete_debut_dern_decembre M: '.$anciennete_debut_dern_decembre;
			$salaire_debut_dern_decembre = $formule -> findSalaireBaseByAnciennete($dao, $poste, $anciennete_debut_dern_decembre, $grille_indice);
			// echo '<br>salaire_debut_dern_decembre devra être P: '. $salaire_debut_dern_decembre[1]; 
			// Disons que ce salaire est celui conclu ou son salaire de base de l'année dernière stocké dans l'historique, alors cette valeur est inutile
		
		// -------------------- Ancienneté du début jusqu'à présent
			$anciennete_debut_janvier = $formule -> anciennete($date_poste_actuel, $date_fin_annee_en_cours, -1);
			// echo '<br>anciennete_debut_janvier T: '.$anciennete_debut_janvier;
			$salaire_debut_jan = $formule -> findSalaireBaseByAnciennete($dao, $poste, $anciennete_debut_janvier, $grille_indice);
			// echo '<br>salaire_depuis_janvier AA: '. $salaire_debut_jan[1];
			$salaire_debut_janvier = $salaire_debut_jan[1];
			$categorie_actuelle = $salaire_debut_jan[0]; // Groupe de catégorie

			// Cherche la nouvelle catégorie réelle de l'employé
			$categorieReelle = $formule -> recupererCategorieRelle($categorie_actuelle, $salaire_debut_jan[4]);
			
			
		// Différences entre les salaires de base 2017 - 2018
			$difference_annee_derniere_actuelle = $salaire_debut_janvier - $SB_conclu;
			$difference_annee_derniere_actuelle_pourcent = ($SB_conclu <= 0) ? 5 : ($difference_annee_derniere_actuelle/$SB_conclu)*100;
			$difference_annee_derniere_actuelle_pourcent_decimal = number_format($difference_annee_derniere_actuelle_pourcent, 10);
			// echo '<br>difference_annee_derniere_actuelle AB et AC : '.$difference_annee_derniere_actuelle. ' ' . $difference_annee_derniere_actuelle_pourcent;

		// -------------------- Augmentation de salaire de 15 % au max (1ère augmentation)
			$limit15 = $taux_augmentation_generale;
			$augmentation_salaire_quinz = $formule -> appliquerAugmentationGenerale($limit15, $difference_annee_derniere_actuelle_pourcent, $SB_conclu);
			$augmentation_salaire_quinze = $augmentation_salaire_quinz[0];
			$taux_augmentation_quinze = $augmentation_salaire_quinz[1];
			// echo '<br>augmentation_salaire_quinze AE : '.$augmentation_salaire_quinze;

		// -------------------- 'Anomalie Salaire' en fonction de la catégorie et la différence du salaire qui devrait être perçue l'année dernière et celui du salaire qui devrait être perçu actuellement
			$anomalie_salaire = $formule -> anomalieSalaire($categorie_actuelle, $difference_annee_derniere_actuelle);
			//echo '<br>anomalie_salaire AH: '.$anomalie_salaire;

		// -------------------- Salaire de base après 15%
			$salaireBaseQuinze = $formule -> calculSalaireBaseGenerale($categorie_actuelle, $SB_conclu, $anomalie_salaire, $augmentation_salaire_quinze, $salaire_debut_janvier);
			$salaireBaseQuinze = $formule -> promotion($dao, $h, $d, $salaireBaseQuinze, $u, $annee_derniere);
			// echo '<br>salaireBaseQuinze AG : '. $salaireBaseQuinze;
		
		// Pour ceux qui ont une différence > 15%, le sur-plus du 15% n'est pas perçu, considérons qu'il est perdu. Il faut l'enregistrer
			$somme_perdue = ($difference_annee_derniere_actuelle_pourcent_decimal > $limit15) ? $difference_annee_derniere_actuelle - $augmentation_salaire_quinze : 0;
			// echo "<br>somme_perdue AF: ".$somme_perdue;

			$trop_percu = $formule -> verifierAnomalieSalaire($anomalie_salaire, $salaireBaseQuinze, $salaire_debut_janvier);
			// echo '<br>trop_percu AI : '. $trop_percu;

		// -------------------- Augmentation de salaire de 5 % au max (2ème augmentation)
			$limit5 = $taux_inflation;
			$augmentation_salaire_cinq = $formule -> appliquerAugmentationInflation($annee_en_cours, $limit5, $date_poste_actuel, $difference_annee_derniere_actuelle_pourcent_decimal, $salaireBaseQuinze, $date_embauche);
			// echo '<br>augmentation_salaire_cinq AK: '.$augmentation_salaire_cinq;

		// -------------------- Salaire de base après 5%
			$salaireBaseCinq = $formule -> calculSalaireBaseInflation($salaireBaseQuinze, $augmentation_salaire_cinq);
			// echo '<br>salaireBaseCinq AL: '.$salaireBaseCinq;

		// **************************** TRAITEMENT DE SALAIRE *******************************/
			// echo '<br>**************************** TRAITEMENT DE SALAIRE *******************************';
			$salaire_base = $salaireBaseQuinze;
			// echo '<br>salaire de base 2018 1!L: '.$salaire_base;
			$salaire_suppl = $formule -> calculSalaireSupplementaire($dao, $h, $annee_en_cours, $augmentation_salaire_cinq, $difference_annee_derniere_actuelle_pourcent_decimal, $limit15, $annee_derniere, $salaire_base, $d, $salaireBaseCinq, $taux_augmentation_quinze, $date_embauche, $mois_dernier, $tranche1, $taux1, $tranche2, $taux2, $taux3);

			$salaire_supplementaire = ($date_embauche->format("Y") >= $annee_en_cours) ? 0 : $salaire_suppl[0];

			$salaireBrutAnneeDerniere = $salaire_suppl[1];

			$salaireBrutInclInflation = ($salaire_base == $SB_conclu && $date_embauche->format("Y") < $annee_en_cours) ? ($salaireBrutAnneeDerniere + $salaire_supplementaire) : ($salaire_base + $salaire_supplementaire) ;
			// echo '<br>salaireBrutAnneeDerniere: '.$salaireBrutAnneeDerniere;
			// echo '<br>salaire_supplementaire: '.$salaire_supplementaire;
			// echo '<br>salaireBrutInclInflation: '.$salaireBrutInclInflation;
		// Primes exceptionnelles
			$prime_exceptionnel = $aa;
			// echo '<br>prime Prime exceptionnelle 1!N: '.$prime_exceptionnel;

			// Remarques
			$remarque = array();

		// Rappels
			$whereRappelBase = "WHERE h.ID_CONTRAT = $a AND (h.DATE_DEBUT_RAPPEL <= '".$date_fin_mois->format('Y-m-d')."' AND YEAR(h.DATE_FIN_RAPPEL) >= '".$date_fin_mois->format('Y')."' AND MONTH(h.DATE_FIN_RAPPEL) >= '".$date_fin_mois->format('m')."' AND h.TYPE_RAPPEL = 1)";
			$rappel_salaire_base = $formule -> calculRappel($dao -> getRappelList($whereRappelBase));

			$whereRappelNet = "WHERE h.ID_CONTRAT = $a AND (h.DATE_DEBUT_RAPPEL <= '".$date_fin_mois->format('Y-m-d')."' AND YEAR(h.DATE_FIN_RAPPEL) >= '".$date_fin_mois->format('Y')."' AND MONTH(h.DATE_FIN_RAPPEL) >= '".$date_fin_mois->format('m')."' AND h.TYPE_RAPPEL = 2)";
			$rappel_salaire_net = $formule -> calculRappel($dao -> getRappelList($whereRappelNet));

			if (strlen($rappel_salaire_base[1]) > 0) array_push($remarque, $rappel_salaire_base[1]);
			if (strlen($rappel_salaire_net[1]) > 0) array_push($remarque, $rappel_salaire_net[1]);

		// -------------------- Salaire Supplémentaire
		// ----------- Salaire intérim
			$whereInterim = "WHERE ID_CONTRAT = $a AND (DATE_DEBUT_INTERIM <= '".$date_fin_mois->format('Y-m-d')."' AND DATE_FIN_INTERIM >= '".$date_debut_mois->format('Y-m-d')."')";
			$interim = $dao -> getSalaireInterimList($whereInterim);
			
			$date_debut_interim = empty($interim) ? '' : $interim[0][2];
			$date_fin_interim = empty($interim) ? '' : $interim[0][3];
			$remarque_interim = empty($interim) ? '' : $interim[0][4];
			$salaire_interim = $formule -> calulerSalaireInterim($date_debut_interim, $date_fin_interim, $date_debut_mois, $date_fin_mois, $dao, $salaire_base, $remarque_interim);
			if (strlen($salaire_interim[1]) > 0) array_push($remarque, $salaire_interim[1]);

		// ----------- Salaire base par heure
			/*
			* Pour être sûr que le calcul est à jour par rapport à la grille, chercher l'heure réglementaire (173.33 ou 243)
			*/
			$tauxHoraire = $salaire_base/$salaire_debut_jan[2]; // salaire selon la grille / taux_horaire

		// ----------- Heures supplémentaires
			$whereHS = "WHERE h.ID_CONTRAT = $a AND h.MOIS_SALAIRE = ".$moisSalarialID[0][0];
			$HSList = $dao -> getHeuresSupplementairesList($whereHS);
			$montantHS = $formule -> calculHS($categorie_actuelle, $HSList, $tauxHs, $tauxHoraire, $d, $salaireBrutInclInflation, $salaire_supplementaire, $annee_en_cours);
			if (strlen($montantHS[3]) > 0) array_push($remarque, $montantHS[3]);
			
		
		// ----------- Salaire brut avant congé de maternité
		/*
		* Salaire brut avant le congé de matérnité = L + AD
		* L = Salaire de base incluant les augmentations générale et inflation
		* AD = AC + T
		* 		AC = Montant total des Heures supplémentaires
		* 		T = M + N + S
		* 			M = Salaire supplémentaire
		* 			N = Prime exceptionnel
		* 			S = Salaire interim
		*/
			$salaireBrutInclInflation2 = ($date_poste_actuel <= $date_debut_mois) ? $salaireBrutInclInflation : $formule -> calculSalaireBrutIncInflationProrata($salaireBrutInclInflation, $salaire_debut_jan[2], $date_poste_actuel, $date_debut_mois, $date_fin_mois);
			$salaire_brut_avant_conge_maternite = $salaireBrutInclInflation2 + $prime_exceptionnel + $salaire_interim[0] + round($montantHS[0]) + $rappel_salaire_base[0]; 
		
		// ----------- Réduction salaire brut
		// ----------- Heures Minus
			$whereAbs = "WHERE h.ID_CONTRAT = $a AND h.ID_SALAIRE = ".$moisSalarialID[0][0]." AND h.TYPE_ABSENCE = 1";
			$AbsenceList = $dao -> getAbsenceList($whereAbs);
			$montantHeureMinus = $formule -> calculHMinus($AbsenceList, $tauxHoraire);
			if (strlen($montantHeureMinus[1]) > 0) array_push($remarque, $montantHeureMinus[1]);

		// ----------- Congé de maternité
			$whereCongeMaternite = "WHERE h.ID_CONTRAT = $a AND (h.DATE_DEBUT_CONGE <= '".$date_fin_mois->format('Y-m-d')."' AND h.DATE_FIN_CONGE >= '".$date_debut_mois->format('Y-m-d')."' AND h.NATURE_CONGE = 1)";
			$congeMaterniteList = $dao -> getCongeList($whereCongeMaternite);
			$dureeCongeMaterniteCeMois = $formule -> calculMinusCongeMaternite($congeMaterniteList, $date_fin_mois, $date_debut_mois, $mois_en_cours, $nb_jour_mois_en_cours);
			$DeductionMaternite = -1 * ($salaire_brut_avant_conge_maternite / $nb_jour_mois_en_cours) * $dureeCongeMaterniteCeMois * ($taux_reduction_maternite[0][0] / 100);
		
		// ----------- Total déduction sur salaire brut 
			$total_deduction_sur_salaire_brut = $montantHeureMinus[0] + $DeductionMaternite;
		
		// ----------- Salaire brut final
			$salaire_brut_final = $salaire_brut_avant_conge_maternite + $total_deduction_sur_salaire_brut;

		// ----------- CNAPS
			$cnapsBase = ($salaire_brut_final >= $cnaps[0][2]) ? $cnaps[0][2] : $salaire_brut_final;
			$cnapsPrime = $cnapsBase * $cnaps[0][3] / 100;
		
		// ----------- Caisse médicale
			$whereSmie = "WHERE s.ID_CENTRE = $c";
			$smie = $dao -> getSmieList2($whereSmie);
			$nomCentreSmie = $smie[0][1];
			$plafondSmie = $smie[0][2];
			$partEmployeSmie = $smie[0][3];
			$montantSmie = ($plafondSmie > 0 && $salaire_brut_final >= $plafondSmie) ? $plafondSmie : $salaire_brut_final;
			$smiePrime = $montantSmie * $partEmployeSmie / 100;
		
		// ----------- Somme cnaps + caisse médicale
			$somme_cnaps_smie = $cnapsPrime + $smiePrime;
		
		// ----------- Brut après CNaPS et Caisse médicale
			$salaire_brut_apres_cnaps_smie = $salaire_brut_final - $somme_cnaps_smie;
		
		// ----------- IRSA
			$montantIRSA = $formule -> calculIRSA($salaire_brut_apres_cnaps_smie, $irsa, $ab, $abattement[0][0]);
		
		// ----------- RETENUS
		// ----------- Hopital
			$montantHopital = $formule -> calculCaisseHospitalisation($hopital[0][0], $aj, $salaire_brut_final);

		// ----------- AVANCES
			$whereAvance = "WHERE h.ID_CONTRAT = $a AND h.EN_COURS = 1 AND DATE_DEBUT_REMBOURSEMENT <= '".$date_fin_mois->format('Y-m-d')."'";
			$AvList = $dao -> getAvanceList($whereAvance);
			$montantAvance = $formule -> calculAvance($AvList, $dao);
			if (strlen($montantAvance[1]) > 0) array_push($remarque, $montantAvance[1]);


		// ----------- Frais médicaux participation
			$whereFM = "WHERE h.ID_CONTRAT = $a AND h.ID_SALAIRE = ".$moisSalarialID[0][0];
			$FMList = $dao -> getFraisMedicauxList($whereFM);
			$participationMedicale = $formule -> calculFraisMedicaux($a, $c, $FMList, $dao);
			if (strlen($participationMedicale[4]) > 0) array_push($remarque, $participationMedicale[4]);

		// ----------- Divers retenus
			$whereDR = "WHERE h.ID_CONTRAT = $a AND h.ID_SALAIRE = ".$moisSalarialID[0][0];
			$DRList = $dao -> getDiversRetenusList($whereDR);
			$montantDiversRetenus = $formule -> calculDiversRetenus($DRList);
			if (strlen($montantDiversRetenus[1]) > 0) array_push($remarque, $montantDiversRetenus[1]);
		
		// ----------- Dépassement téléphonique
			$whereDPS = "WHERE h.ID_CONTRAT = $a AND h.ID_SALAIRE = ".$moisSalarialID[0][0];
			$DPSList = $dao -> getDepassementList($whereDPS);
			$montantDepassement = $formule -> calculDepassement($DPSList);
			if (strlen($montantDepassement[1]) > 0) array_push($remarque, $montantDepassement[1]);

		// ----------- Congé
			$whereConge = "WHERE h.ID_CONTRAT = $a AND h.ID_SALAIRE = ".$moisSalarialID[0][0]." AND h.NATURE_CONGE = 2";
			$CongeList = $dao -> getCongeList($whereConge);
			$totalCongePris = $formule -> calculCongePris($CongeList);
			$whereCongeSolde = "WHERE h.ID_CONTRAT = $a";
			$droitConge = ($ah == 'CM' || $ah == 'CMc') ? 3.1 : 2.5; // Le droit de congé pour les centres mobiles est de 3.1 les autres est de 2.5
			
			$soldeCongeDao = $dao -> getSoldeConge($whereCongeSolde); // Solde avant la déduction des congés
			$soldeConge = $formule -> calculSoldeConge($soldeCongeDao, $date_fin_mois, $droitConge, $totalCongePris);
			$nouveauSoldeConge = $soldeConge;
			// Mettre à jour le nouveau solde de congé (si (mois == 6 && nouveauSolde >= 45) ? 45 : nouveauSolde)

		// ----------- Résumé
			$Charge = round($cnapsPrime) + round($smiePrime) + round($montantIRSA[4]) + round($montantHopital);
			$SalaireNet = round($salaire_brut_final) - $Charge + $rappel_salaire_net[0];
			$Retenus = round($participationMedicale[2]) + $montantAvance[0] + round($montantDepassement[0]) + round($montantDiversRetenus[0]);
			$NETaPayer = $SalaireNet - $Retenus;

		// -------------------- Affichage
			array_push($response, array(
				'idContrat' => $a,
				'matricule' => $z,
				'nomPrenom' => $ac.' '.$ad,
				'titrePoste' => $af,
				'categorieProfessionnelle' => $categorie_actuelle,
				'dateEmbauche' => $m,
				'datePrisePoste' => $n,
				// Données à afficher
				'SBConclu2017' => $SB_conclu, // salaire du dernière année (2017)
				'Bjanvier' => $salaire_debut_janvier, // salaire selon la grille
				'salaireBase' => $salaire_base, // salaire incluant les augmentations 15% et 5%
				'salaireSupplementaire' => round($salaire_supplementaire), // la différence du salaire de base réel et les augmentations
				'nonPercu' => $somme_perdue, // Ce montant est la différence excédentaire après les 15% et 5%. Ce montant n'est pas perçu par l'employé
				'primeExceptionnel' => $prime_exceptionnel,
				'anomalie' => $anomalie_salaire,
				'dateDebutInterim' => $date_debut_interim,
				'dateFinInterim' => $date_fin_interim,
				'montantInterim' => $salaire_interim[0],
				'baseParHeure' => round($tauxHoraire),
				'montantHS' => round($montantHS[0]),
				'sBrutAvantMaternite' => round($salaire_brut_avant_conge_maternite),
				'montantHMinus' => round($montantHeureMinus[0]), //1!AG
				'dureeCongeMaterniteCeMois' => $dureeCongeMaterniteCeMois, //1!AJ
				'deductionMaternite' => round($DeductionMaternite), // 1!AK
				'salaireBrutFinal' => round($salaire_brut_final), //1!AN
				'cnapsBase' => round($cnapsBase), //1!AO
				'cnapsPrime' => round($cnapsPrime), //1!AP
				'nomCentreSmie' => $nomCentreSmie, //1!AQ
				'smiePrime' => round($smiePrime), //1!AS
				'sommeCnapsSmie' => round($somme_cnaps_smie), //1!AT
				'salaireBrutApresCnapsSmie' => round($salaire_brut_apres_cnaps_smie), //1!AU
				'brutApresCnapsSmieArrondi' => round($montantIRSA[0]), // 1!AV
				'montantIrsaApresPlafond' => round($montantIRSA[1]), //1!AW
				'IrsaBrutArrondi' => round($montantIRSA[2]), //1!AZ
				'abattement' => round($montantIRSA[3]), //1!BB
				'IrsaNet' => round($montantIRSA[4]), //1!BC
				'caisseHospitalisation' => round($montantHopital), //1!BE
				'factureFraisMedicaux' => round($participationMedicale[0]), //1!BF
				'tauxFraisMedicaux' => round($participationMedicale[1]), //1!BG
				'participationFraisMedicaux' => round($participationMedicale[2]), //1!BH
				'deductionAvance' => $montantAvance[0], //1!BQ
				'diversRetenus' => round($montantDiversRetenus[0]), //1!BX
				'depassement' => round($montantDepassement[0]), //1!BW
				'Charge' => $Charge,
				'SalaireNet' => $SalaireNet,
				'Retenus' => $Retenus,
				'netAPayer' => $NETaPayer,
				'decallageAvance' => (count($decallageAvance) > 0) ? $decallageAvance[0][0] : 0,
				'congePris' => $totalCongePris,
				'soldeConge' => $nouveauSoldeConge,
				// Données supplémentaires pour l'insertion dans la table historique de salaire
				'idSalaire' => $moisSalarialID[0][0],
				'idAugmGenerale' => null,
				'idAugmInflation' => null,
				'dureeValideInterim' => 14,
				'idHsInfHuitHeures' => $tauxHs[0][0],
				'idHsSupHuitHeures' => $tauxHs[1][0],
				'idHsNuitHabituelle' => $tauxHs[2][0],
				'idHsNuitOccasionnelle' => $tauxHs[3][0],
				'idHsDimanche' => $tauxHs[4][0],
				'idHsFerie' => $tauxHs[5][0],
				'debutMaternite' => (empty($congeMaterniteList)) ? null: $congeMaterniteList[0][4],
				'finMaternite' => (empty($congeMaterniteList)) ? null: $congeMaterniteList[0][5],
				'tauxMaternite' => $taux_reduction_maternite[0][1],
				'idCnaps' => $cnaps[0][0],
				'idSmie' => $i,
				'idIrsa' => $irsa[0][0],
				'idParamAbattement' => $abattement[0][1],
				'idParamHospitalisation' => $hopital[0][1],
				'idHopital' => $participationMedicale[3],
				'idAvance' => (empty($AvList)) ? null: $AvList[0][0],
				'idPoste' => $d,
				'idCategorieProfessionnelle' => $salaire_debut_jan[3],
				'netAPayer' => $NETaPayer,
				'nbEnfant' => $ab,
				'ancienneteDebutJanvier' => $anciennete_debut_janvier,
				'totalHeureHS' => $montantHS[1],
				'salaireBrutInclInflation' => $salaireBrutInclInflation,
				'ancienneCategorie' => $ag,
				'abreviationCentre' => $ah,
				'sBrutAnneeDerniere' => $salaireBrutAnneeDerniere,
				'idHS' => ($montantHS[2] == 0) ? null : $montantHS[2],
				'categorieReelle' => $categorieReelle,
				'rappelBase' => $rappel_salaire_base[0],
				'rappelNet' => $rappel_salaire_net[0],
				'remarque' => implode(' ', $remarque),
				'infHuitHeures' => empty($HSList) ? 0 : $HSList[0][2],
				'supHuitHeures' => empty($HSList) ? 0 : $HSList[0][3],
				'nuitHabituelle' => empty($HSList) ? 0 : $HSList[0][4],
				'nuitOccasionnelle' => empty($HSList) ? 0 : $HSList[0][5],
				'dimanche' => empty($HSList) ? 0 : $HSList[0][6],
				'ferie' => empty($HSList) ? 0 : $HSList[0][7],
				'partEmployeSmie' => $partEmployeSmie,
				'montantAvance' => (!empty($AvList)) ? $AvList[0][4] : 0,
				'dateDemandeAvance' => (!empty($AvList)) ? $AvList[0][5] : '',
				'dateDebutAvance' => (!empty($AvList)) ? $AvList[0][6] : '',
				'sommeDeductionAvance' => $montantAvance[2],
				'idCentre' => $c,
				'nbHeuresMinus' => $montantHeureMinus[2]
			));
	}
	/* END FOREACH CONTRAT */
	$arr['data'] = array_values($response);
	$arr['total'] = $nb;

	$arr = json_encode($arr);

	if ($action == 1) { // Renvoyer les données pour l'affichage dans le tableau
		affichage($arr);
	} else if ($action == 2) { // Export sous Excel quelques valeurs
		// Récupérer les données du mois dernier clôturé
		$whereHistorique = "h.ID_SALAIRE =(
			SELECT
				MAX(ID_SALAIRE)
			FROM
				historique_salaire
		)";
		$spreadsheet = new Spreadsheet();
		$dernieresDonnees = $dao -> getHistoriqueSalaire($where);
		exportData($spreadsheet, $dernieresDonnees, array_values($response));
	}
}

/*
 * Renvoie les données de salaire calculer pour affichage dans le grid 'DetailsSalaire'
 */
function affichage($arr) {
	echo $arr;
}

/*
 * Function pour exporter les anciennes et nouvelles données
 */
function exportData($spreadsheet, $dernieresDonnees, $nouvelleDonnees) {
	$newData = array();

	foreach ($dernieresDonnees as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae, $af, $ag, $ah, $ai, $aj, $ak, $al, $am, $an, $ao, $ap, $aq, $ar, $as, $at, $au, $av, $aw, $ax, $ay, $az, $ba, $bb, $bc ,$bd, $be, $bf, $bg, $bh, $bi, $bj, $bk, $bl, $bm, $bn, $bo, $bp, $bq, $br, $bs, $bt, $bu, $bv, $bw, $bx, $by, $bz, $ca, $cb, $cc, $cd, $ce, $cf, $cg, $ch, $ci, $cj, $ck, $cl, $cm, $cn, $co, $cp)) {
		array_push($newData, array(
			'idContrat' => $a,
			'ancSalaireBrut' => $bw
		));
	}

	$keyed = array_column($newData, NULL, 'idContrat'); // replace indexes with ur_user_id values

	foreach ($nouvelleDonnees as &$row) {       // write directly to $array1 while iterating
		if (isset($keyed[$row['idContrat']])) { // check if shared key exists
			$row += $keyed[$row['idContrat']]; // append associative elements
		}
	}

	// var_export($nouvelleDonnees);
	
	$filename = 'Simulation';
	
	$spreadsheets = spreadSheetData(0, $filename, $nouvelleDonnees, $spreadsheet);
		
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'. utf8_decode($filename) .'.xlsx"'); /*-- $filename is  xsl filename ---*/
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	header('downloadToken: true');
	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0
	
	$writer = new Xlsx($spreadsheets);
	$writer->save('php://output');
}

function spreadSheetData($sheetIndex, $filename, $res, $spreadsheet) {
	// Create a new worksheet called "My Data"
	$myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $filename);
	// Attach the "My Data" worksheet as the first worksheet in the Spreadsheet object
	$spreadsheet->addSheet($myWorkSheet, $sheetIndex);

	$cell_st =[
		'font' =>['bold' => true],
		'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER]
		];
	$cell_horizontal_center = [
		'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER]
		];
	$border_style = [
		'allBorders' => [
			'borderStyle' => Border::BORDER_THIN,
			'color' => [
				'rgb' => '808080'
			]
		]
	];

	$spreadsheet->setActiveSheetIndex($sheetIndex)
		->setCellValue('A1', 'Centre')
		->setCellValue('B1', 'Nom & prénoms')
		->setCellValue('C1', 'Poste occupé')
		->setCellValue('D1', 'Catégorie professionnelle')
		->setCellValue('E1', 'Ancien S.brut')
		->setCellValue('F1', 'Nouveau S.brut')
		->setCellValue('G1', 'Augmentation(%)');
	$spreadsheet->getActiveSheet()
		->getStyle('A1:G1')
			->applyFromArray($cell_st);
	
	// Les lignes
	$count = $debut_ligne = 1;
	$index = 1;
	$nbLigne = count($res) + $count - 1;
	// Insertion des lignes

	ini_set('memory_limit', '-1'); // It will take unlimited memory usage of server
	
	foreach ($res as $row) {
		$count += 1;
		//add some data in excel cells
		$spreadsheet->setActiveSheetIndex($sheetIndex)
		->setCellValue('A'.$count, $row['abreviationCentre'])
		->setCellValue('B'.$count, $row['nomPrenom'])
		->setCellValue('C'.$count, $row['titrePoste'])
		->setCellValue('D'.$count, $row['categorieReelle'])
		->setCellValue('E'.$count, $row['ancSalaireBrut'])
		->setCellValue('F'.$count, $row['salaireBrutInclInflation'])
		->setCellValue('G'.$count, '=(F'.$count.'-E'.$count.')/E'.$count.'*100');

		$index ++;
	}

	$spreadsheet->getActiveSheet()->getStyle('A1:G'.($nbLigne + 1))->getBorders()->applyFromArray($border_style);
	$spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$spreadsheet->getActiveSheet()
			->getStyle('E'.$debut_ligne.':F'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode('#,##0');
	$spreadsheet->getActiveSheet()
		->getStyle('G'.$debut_ligne.':G'.($nbLigne + 1))
			->getNumberFormat()
				->setFormatCode('#,##0.0');


	$spreadsheet->getActiveSheet()->setTitle($filename); //set a title for Worksheet

	return $spreadsheet;
}
?>
