<?php
require_once '../DataAccessObject.php';
require_once '../formule/AppliquerFormule.php';
require '../vendor/autoload.php';

//include the classes needed to create and write .xlsx file
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Shared\StringHelper;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

$dao = new DataAccessObject();
$formule = new AppliquerFormule();

$moisSalarial = $_REQUEST['moisSalarial'];
$infoRequest = $_REQUEST['infoRequest'];
$action = $_REQUEST['action'];

if ($moisSalarial < 0) {
	$arr['data'] = array();
	$arr['total'] = 0;

	$arr = json_encode($arr);
	echo $arr;
} else {

	$page = json_decode($_REQUEST['page']);
	$start = json_decode($_REQUEST['start']);
	$lim = json_decode($_REQUEST['limit']);
	$limit = $lim;

	// ----------------------- Les paramètres dont j'aurais besoin
	/*
	* le mois salarial est paramétré par 
	* sa date début (e.g 01/11/2018),
	* sa date de fin (e.g 30/11/2018)
	* ses dates de traitement (e.g entre 20/11/2018 et au plus tard le 25/11/2018) ie entre le 19ème et le 24ème du mois
	* */

	$moisSalarialById = explode("-", $moisSalarial);
	$whereSalaire = "WHERE MOIS_SALAIRE = $moisSalarialById[1] AND ANNEE_SALAIRE = $moisSalarialById[0]";

	$moisSalarialID = $dao -> getSalaireList($whereSalaire);

	$date_du_jour = $moisSalarial."-24";
	// $date_du_jour = strtotime('2018-11-24'); // A changer par la date de traitement du salaire celui du 24ème du mois
	$date_du_jour = strtotime($date_du_jour);
	$annee_en_cours = date('Y', strtotime("this year", $date_du_jour));
	$mois_en_cours = date('m', strtotime("this month", $date_du_jour));
	$annee_derniere = date('Y', strtotime("this year", $date_du_jour)) - 1;
	$date_debut_mois = new DateTime(date("Y-m-d",strtotime("first day of this month", $date_du_jour)));
	$date_fin_mois = new DateTime(date("Y-m-d",strtotime("last day of this month", $date_du_jour)));
	$nb_jour_mois_en_cours = date('d', date_timestamp_get($date_fin_mois));
	$date_debut_annee_derniere = "{$annee_derniere}-01-01";
	$date_fin_annee_derniere = new DateTime(date("Y-m-d",strtotime("last year December 31st", $date_du_jour)));
	$date_debut_annee_en_cours = new DateTime(date("Y-m-d",strtotime("this year January 1st", $date_du_jour)));
	// echo '<br>date_debut_annee_en_cours: '.$date_debut_annee_en_cours->format('Y-m-d');
	$date_fin_annee_en_cours = new DateTime(date("Y-m-d",strtotime("this year December 31st", $date_du_jour)));

	$grille_indice = ["UN", "DEUX", "TROIS", "QUATRE", "CINQ", "SIX", "SEPT", "HUIT", "NEUF", "DIX", "ONZE", "DOUZE", "TREIZE", "QUATORZE", "QUINZE", "SEIZE", "DIXSEPT", "DIXHUIT", "DIXNEUF", "VINGT"];

	if ($infoRequest == 1) {
		$where = "WHERE c.ACTIF_CONTRAT = 1 AND c.DATE_PRISE_POSTE <= '".$date_fin_mois->format('Y-m-d')."'";

		$date_limite_naissance = new DateTime(date("Y-m-d",strtotime("-21 year", $date_du_jour)));
		$date_limite_naissance = $date_limite_naissance->format('Y-m-d');
		$whereAgeEnfant = "WHERE DATE_NAISSANCE_ENFANT >= '$date_limite_naissance'";
	} else if ($infoRequest == 2) {
		$idContrat = $_REQUEST['idContrat']; // A envoyer lors du rowclic ou buton click event

		$where = "WHERE c.ACTIF_CONTRAT = 1 and c.ID_CONTRAT = $idContrat AND c.DATE_PRISE_POSTE <= '".$date_fin_mois->format('Y-m-d')."'";
		
		$date_limite_naissance = new DateTime(date("Y-m-d",strtotime("-21 year", $date_du_jour)));
		$date_limite_naissance = $date_limite_naissance->format('Y-m-d');
		$whereAgeEnfant = "WHERE DATE_NAISSANCE_ENFANT >= '$date_limite_naissance'";
	}

	$dataContrat = $dao -> getContratList($where, $whereAgeEnfant);
	$nb=count($dataContrat);

	$res1 = ($action == 1) ? array_slice($dataContrat, $start, $limit, true) : $dataContrat;

	$response = array();

	$whereTauxHS = "WHERE ACTIF_HS_BASE = 1";
	$tauxHs = $dao -> getTauxHSList($whereTauxHS);

	// Les taux d'augmentation
	$taux_augmentation_generale = $dao -> getTaux(1); // Taux de l'augmentation générale
	$taux_inflation = $dao -> getTaux(2); // Taux de l'augmentation de l'inflation
	$tranche_1 = $dao -> getTaux(7);
	$taux_1 = $dao -> getTaux(8);
	$tranche_2 = $dao -> getTaux(9);
	$taux_2 = $dao -> getTaux(10);
	$taux_3 = $dao -> getTaux(11);


	$taux_reduction_maternite = $dao -> getTauxReductionMaternite(); // Taux de réduction pour le congé de maternité

	$whereCnaps = 'WHERE ACTIF_CNAPS = 1';
	$cnaps = $dao -> getCnapsList2($whereCnaps);

	$whereIrsa = 'WHERE ACTIF_IRSA = 1';
	$irsa = $dao -> getIrsaList2($whereIrsa);

	$hopital = $dao -> getTaux(5);

	$abattement = $dao -> getTaux(3); // Montant pour l'abattement par enfant

	$decallageAvance = $dao -> getTaux(6); // Le décallage entre 2 avances

	/* BEGIN FOREACH CONTRAT */
	foreach ($dataContrat as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae, $af, $ag, $ah, $ai, $aj)) {

		// Poste actuel
			$poste = $d;
			$date_embauche = new DateTime($m); // K
			// echo '<br> date_embauche: '. date_format($date_embauche, 'Y-m-d');
			$date_poste_actuel = new DateTime($n);
			// echo '<br> date_poste_actuel: '. date_format($date_poste_actuel, 'Y-m-d');

		// -------------------- Dernier salaire de base
			$SB_conclu = $formule -> getLastSalaireBase($dao, $h, $u, $annee_derniere, 12); // P
			// echo '<br>SB_conclu 2017 P: '.$SB_conclu;
		
		// -------------------- Ancienneté du début au décembre dernier
			$anciennete_debut_dern_decembre = $formule -> anciennete($date_poste_actuel, $date_fin_annee_derniere, -1);
			// echo '<br>anciennete_debut_dern_decembre M: '.$anciennete_debut_dern_decembre;
			$salaire_debut_dern_decembre = $formule -> findSalaireBaseByAnciennete($dao, $poste, $anciennete_debut_dern_decembre, $grille_indice);
			// echo '<br>salaire_debut_dern_decembre devra être P: '. $salaire_debut_dern_decembre[1]; 
			// Disons que ce salaire est celui conclu ou son salaire de base de l'année dernière stocké dans l'historique, alors cette valeur est inutile
		
		// -------------------- Ancienneté du début jusqu'à présent
			$anciennete_debut_janvier = $formule -> anciennete($date_poste_actuel, $date_fin_annee_en_cours, -1);
			// echo '<br>anciennete_debut_janvier T: '.$anciennete_debut_janvier;
			$salaire_debut_jan = $formule -> findSalaireBaseByAnciennete($dao, $poste, $anciennete_debut_janvier, $grille_indice);
			// echo '<br>salaire_depuis_janvier AA: '. $salaire_debut_jan[1];
			$salaire_debut_janvier = $salaire_debut_jan[1];
			$categorie_actuelle = $salaire_debut_jan[0]; // Groupe de catégorie

			// Cherche la nouvelle catégorie réelle de l'employé
			$categorieReelle = $formule -> recupererCategorieRelle($categorie_actuelle, $salaire_debut_jan[4]);
			
			
		// Différences entre les salaires de base 2017 - 2018
			// $difference_annee_derniere_actuelle = $salaire_debut_janvier - $SB_conclu;
			$difference_annee_derniere_actuelle = $salaire_debut_janvier - $SB_conclu;
			$difference_annee_derniere_actuelle_pourcent = ($SB_conclu <= 0) ? 5 : ($difference_annee_derniere_actuelle/$SB_conclu)*100;
			$difference_annee_derniere_actuelle_pourcent_decimal = number_format($difference_annee_derniere_actuelle_pourcent, 10);
			// echo '<br>difference_annee_derniere_actuelle AB et AC : '.$difference_annee_derniere_actuelle. ' ' . $difference_annee_derniere_actuelle_pourcent;

		// -------------------- Augmentation de salaire de 15 % au max (1ère augmentation)
			$limit15 = $taux_augmentation_generale[0][0];
			$augmentation_salaire_quinz = $formule -> appliquerAugmentationGenerale($limit15, $difference_annee_derniere_actuelle_pourcent, $SB_conclu);
			// TODO
			$augmentation_salaire_quinze = 0;
			// $augmentation_salaire_quinze = $augmentation_salaire_quinz[0];
			$taux_augmentation_quinze = $augmentation_salaire_quinz[1];
			// echo '<br>augmentation_salaire_quinze AE : '.$augmentation_salaire_quinze;

		// -------------------- 'Anomalie Salaire' en fonction de la catégorie et la différence du salaire qui devrait être perçue l'année dernière et celui du salaire qui devrait être perçu actuellement
			$anomalie_salaire = $formule -> anomalieSalaire($categorie_actuelle, $difference_annee_derniere_actuelle);
			//echo '<br>anomalie_salaire AH: '.$anomalie_salaire;

		// -------------------- Salaire de base après 15%
			// TODO $salaire_debut_janvier = $SB_conclu
			$salaireBaseQuinze = $formule -> calculSalaireBaseGenerale($categorie_actuelle, $SB_conclu, $anomalie_salaire, $augmentation_salaire_quinze, $salaire_debut_janvier);
			echo '<br>salaireBaseQuinze : '. $salaire_debut_janvier;
			$salaireBaseQuinze = $formule -> promotion($dao, $h, $d, $salaireBaseQuinze, $u, $annee_derniere);
			// echo '<br>salaireBaseQuinze AG : '. $salaireBaseQuinze;
		
		// Pour ceux qui ont une différence > 15%, le sur-plus du 15% n'est pas perçu, considérons qu'il est perdu. Il faut l'enregistrer
			$somme_perdue = ($difference_annee_derniere_actuelle_pourcent_decimal > $limit15) ? $difference_annee_derniere_actuelle - $augmentation_salaire_quinze : 0;
			// echo "<br>somme_perdue AF: ".$somme_perdue;

			$trop_percu = $formule -> verifierAnomalieSalaire($anomalie_salaire, $salaireBaseQuinze, $salaire_debut_janvier);
			// echo '<br>trop_percu AI : '. $trop_percu;

		// -------------------- Augmentation de salaire de 5 % au max (2ème augmentation)
			$limit5 = $taux_inflation[0][0];
			$augmentation_salaire_cinq = $formule -> appliquerAugmentationInflation($annee_en_cours, $limit5, $date_poste_actuel, $difference_annee_derniere_actuelle_pourcent_decimal, $salaireBaseQuinze, $date_embauche);
			// echo '<br>augmentation_salaire_cinq AK: '.$augmentation_salaire_cinq;

		// -------------------- Salaire de base après 5%
			// $augmentation_salaire_cinq = 0 // TODO JANVIER 2020
			//$salaireBaseCinq = $formule -> calculSalaireBaseInflation($salaireBaseQuinze, $salaireBaseCinq);
			$salaireBaseCinq = $formule -> calculSalaireBaseInflation($salaireBaseQuinze, 0);
			// echo '<br>salaireBaseCinq AL: '.$salaireBaseCinq;

		// **************************** TRAITEMENT DE SALAIRE *******************************/
			// echo '<br>**************************** TRAITEMENT DE SALAIRE *******************************';
			$salaire_base = $salaireBaseQuinze;
			// echo '<br>salaire de base 2018 1!L: '.$salaire_base;
			// $augmentation_salaire_cinq = 0 // TODO JANVIER 2020
			// $salaire_suppl = $formule -> calculSalaireSupplementaire($dao, $h, $annee_en_cours, $augmentation_salaire_cinq, $difference_annee_derniere_actuelle_pourcent_decimal, $limit15, $annee_derniere, $salaire_base, $d, $salaireBaseCinq, $taux_augmentation_quinze, $date_embauche, 12, $tranche_1[0][0], $taux_1[0][0], $tranche_2[0][0], $taux_2[0][0], $taux_3[0][0]);
			$salaire_suppl = $formule -> calculSalaireSupplementaire($dao, $h, $annee_en_cours, 0, $difference_annee_derniere_actuelle_pourcent_decimal, $limit15, $annee_derniere, $salaire_base, $d, $salaireBaseCinq, $taux_augmentation_quinze, $date_embauche, 12, $tranche_1[0][0], $taux_1[0][0], $tranche_2[0][0], $taux_2[0][0], $taux_3[0][0]);

			$salaire_supplementaire = ($date_embauche->format("Y") >= $annee_en_cours) ? 0 : $salaire_suppl[0];
			$salaireBrutAnneeDerniere = $salaire_suppl[1];
			
			$salaireBrutInclInflation = ($salaire_base == $SB_conclu && $date_embauche->format("Y") < $annee_en_cours) ? ($salaireBrutAnneeDerniere + $salaire_supplementaire) : ($salaire_base + $salaire_supplementaire) ;
			echo '<br>salaire_supplementaire: '.($salaire_base + $salaire_supplementaire);
			
		// Primes exceptionnelles
			$prime_exceptionnel = $aa;
			// echo '<br>prime Prime exceptionnelle 1!N: '.$prime_exceptionnel;

			// Remarques
			$remarque = array();

		// Rappels
			$whereRappelBase = "WHERE h.ID_CONTRAT = $a AND (h.DATE_DEBUT_RAPPEL <= '".$date_fin_mois->format('Y-m-d')."' AND YEAR(h.DATE_FIN_RAPPEL) >= '".$date_fin_mois->format('Y')."' AND MONTH(h.DATE_FIN_RAPPEL) >= '".$date_fin_mois->format('m')."' AND h.TYPE_RAPPEL = 1)";
			$rappel_salaire_base = $formule -> calculRappel($dao -> getRappelList($whereRappelBase));

			$whereRappelNet = "WHERE h.ID_CONTRAT = $a AND (h.DATE_DEBUT_RAPPEL <= '".$date_fin_mois->format('Y-m-d')."' AND YEAR(h.DATE_FIN_RAPPEL) >= '".$date_fin_mois->format('Y')."' AND MONTH(h.DATE_FIN_RAPPEL) >= '".$date_fin_mois->format('m')."' AND h.TYPE_RAPPEL = 2)";
			$rappel_salaire_net = $formule -> calculRappel($dao -> getRappelList($whereRappelNet));

			if (strlen($rappel_salaire_base[1]) > 0) array_push($remarque, $rappel_salaire_base[1]);
			if (strlen($rappel_salaire_net[1]) > 0) array_push($remarque, $rappel_salaire_net[1]);

		// -------------------- Salaire Supplémentaire
		// ----------- Salaire intérim
			$whereInterim = "WHERE ID_CONTRAT = $a AND (DATE_DEBUT_INTERIM <= '".$date_fin_mois->format('Y-m-d')."' AND DATE_FIN_INTERIM >= '".$date_debut_mois->format('Y-m-d')."')";
			$interim = $dao -> getSalaireInterimList($whereInterim);
			
			$date_debut_interim = empty($interim) ? '' : $interim[0][2];
			$date_fin_interim = empty($interim) ? '' : $interim[0][3];
			$remarque_interim = empty($interim) ? '' : $interim[0][4];
			$salaire_interim = $formule -> calulerSalaireInterim($date_debut_interim, $date_fin_interim, $date_debut_mois, $date_fin_mois, $dao, $salaire_base, $remarque_interim);
			if (strlen($salaire_interim[1]) > 0) array_push($remarque, $salaire_interim[1]);

		// ----------- Salaire base par heure
			/*
			* Pour être sûr que le calcul est à jour par rapport à la grille, chercher l'heure réglementaire (173.33 ou 243)
			*/
			$tauxHoraire = $salaire_base/$salaire_debut_jan[2]; // salaire selon la grille / taux_horaire

		// ----------- Heures supplémentaires
			$whereHS = "WHERE h.ID_CONTRAT = $a AND h.MOIS_SALAIRE = ".$moisSalarialID[0][0];
			$HSList = $dao -> getHeuresSupplementairesList($whereHS);
			$montantHS = $formule -> calculHS($categorie_actuelle, $HSList, $tauxHs, $tauxHoraire, $d, $salaireBrutInclInflation, $salaire_supplementaire, $annee_en_cours);
			if (strlen($montantHS[3]) > 0) array_push($remarque, $montantHS[3]);
			
		
		// ----------- Salaire brut avant congé de maternité
		/*
		* Salaire brut avant le congé de matérnité = L + AD
		* L = Salaire de base incluant les augmentations générale et inflation
		* AD = AC + T
		* 		AC = Montant total des Heures supplémentaires
		* 		T = M + N + S
		* 			M = Salaire supplémentaire
		* 			N = Prime exceptionnel
		* 			S = Salaire interim
		*/
			$salaireBrutInclInflation2 = ($date_poste_actuel <= $date_debut_mois) ? $salaireBrutInclInflation : $formule -> calculSalaireBrutIncInflationProrata($salaireBrutInclInflation, $salaire_debut_jan[2], $date_poste_actuel, $date_debut_mois, $date_fin_mois);
			$salaire_brut_avant_conge_maternite = $salaireBrutInclInflation2 + $prime_exceptionnel + $salaire_interim[0] + round($montantHS[0]) + $rappel_salaire_base[0]; 
		
		// ----------- Réduction salaire brut
		// ----------- Heures Minus
			$whereAbs = "WHERE h.ID_CONTRAT = $a AND h.ID_SALAIRE = ".$moisSalarialID[0][0]." AND h.TYPE_ABSENCE = 1";
			$AbsenceList = $dao -> getAbsenceList($whereAbs);
			/*
			* Décision du 22/11/2019
			* $tauxHoraireMinus = $SalaireBrut/taux_horaire
			*/
			// $montantHeureMinus = $formule -> calculHMinus($AbsenceList, $tauxHoraire);
			$tauxHoraireMinus = ($salaireBrutInclInflation2 + $prime_exceptionnel)/$salaire_debut_jan[2]; // salaire selon la grille / taux_horaire
			$montantHeureMinus = $formule -> calculHMinus($AbsenceList, $tauxHoraireMinus);
			if (strlen($montantHeureMinus[1]) > 0) array_push($remarque, $montantHeureMinus[1]);

		// ----------- Congé de maternité
			$whereCongeMaternite = "WHERE h.ID_CONTRAT = $a AND (h.DATE_DEBUT_CONGE <= '".$date_fin_mois->format('Y-m-d')."' AND h.DATE_FIN_CONGE >= '".$date_debut_mois->format('Y-m-d')."' AND h.NATURE_CONGE = 1)";
			$congeMaterniteList = $dao -> getCongeList($whereCongeMaternite);
			$dureeCongeMaterniteCeMois = $formule -> calculMinusCongeMaternite($congeMaterniteList, $date_fin_mois, $date_debut_mois, $mois_en_cours, $nb_jour_mois_en_cours);
			$DeductionMaternite = -1 * ($salaire_brut_avant_conge_maternite / $nb_jour_mois_en_cours) * $dureeCongeMaterniteCeMois * ($taux_reduction_maternite[0][0] / 100);
		
		// ----------- Total déduction sur salaire brut 
			$total_deduction_sur_salaire_brut = $montantHeureMinus[0] + $DeductionMaternite;
		
		// ----------- Salaire brut final
			$salaire_brut_final = $salaire_brut_avant_conge_maternite + $total_deduction_sur_salaire_brut;

		// ----------- CNAPS
			$cnapsBase = ($salaire_brut_final >= $cnaps[0][2]) ? $cnaps[0][2] : $salaire_brut_final;
			$cnapsPrime = $cnapsBase * $cnaps[0][3] / 100;
		
		// ----------- Caisse médicale
			$whereSmie = "WHERE s.ID_CENTRE = $c";
			$smie = $dao -> getSmieList2($whereSmie);
			$nomCentreSmie = $smie[0][1];
			$plafondSmie = $smie[0][2];
			$partEmployeSmie = $smie[0][3];
			$montantSmie = ($plafondSmie > 0 && $salaire_brut_final >= $plafondSmie) ? $plafondSmie : $salaire_brut_final;
			$smiePrime = $montantSmie * $partEmployeSmie / 100;
		
		// ----------- Somme cnaps + caisse médicale
			$somme_cnaps_smie = $cnapsPrime + $smiePrime;
		
		// ----------- Brut après CNaPS et Caisse médicale
			$salaire_brut_apres_cnaps_smie = $salaire_brut_final - $somme_cnaps_smie;
		
		// ----------- IRSA
			$montantIRSA = $formule -> calculIRSA($salaire_brut_apres_cnaps_smie, $irsa, $ab, $abattement[0][0]);
		
		// ----------- RETENUS
		// ----------- Hopital
			$montantHopital = $formule -> calculCaisseHospitalisation($hopital[0][0], $aj, $salaire_brut_final);

		// ----------- AVANCES
			$whereAvance = "WHERE h.ID_CONTRAT = $a AND h.EN_COURS = 1 AND DATE_DEBUT_REMBOURSEMENT <= '".$date_fin_mois->format('Y-m-d')."'";
			$AvList = $dao -> getAvanceList($whereAvance);
			$montantAvance = $formule -> calculAvance($AvList, $dao);
			if (strlen($montantAvance[1]) > 0) array_push($remarque, $montantAvance[1]);


		// ----------- Frais médicaux participation
			$whereFM = "WHERE h.ID_CONTRAT = $a AND h.ID_SALAIRE = ".$moisSalarialID[0][0];
			$FMList = $dao -> getFraisMedicauxList($whereFM);
			$participationMedicale = $formule -> calculFraisMedicaux($a, $c, $FMList, $dao);
			if (strlen($participationMedicale[4]) > 0) array_push($remarque, $participationMedicale[4]);

		// ----------- Divers retenus
			$whereDR = "WHERE h.ID_CONTRAT = $a AND h.ID_SALAIRE = ".$moisSalarialID[0][0];
			$DRList = $dao -> getDiversRetenusList($whereDR);
			$montantDiversRetenus = $formule -> calculDiversRetenus($DRList);
			if (strlen($montantDiversRetenus[1]) > 0) array_push($remarque, $montantDiversRetenus[1]);
		
		// ----------- Dépassement téléphonique
			$whereDPS = "WHERE h.ID_CONTRAT = $a AND h.ID_SALAIRE = ".$moisSalarialID[0][0];
			$DPSList = $dao -> getDepassementList($whereDPS);
			$montantDepassement = $formule -> calculDepassement($DPSList);
			if (strlen($montantDepassement[1]) > 0) array_push($remarque, $montantDepassement[1]);

		// ----------- Congé
			$whereConge = "WHERE h.ID_CONTRAT = $a AND h.ID_SALAIRE = ".$moisSalarialID[0][0]." AND h.NATURE_CONGE = 2";
			$CongeList = $dao -> getCongeList($whereConge);
			$totalCongePris = $formule -> calculCongePris($CongeList);
			$whereCongeSolde = "WHERE h.ID_CONTRAT = $a";
			$droitConge = ($ah == 'CM' || $ah == 'CMc') ? 3.1 : 2.5; // Le droit de congé pour les centres mobiles est de 3.1 les autres est de 2.5
			
			$soldeCongeDao = $dao -> getSoldeConge($whereCongeSolde); // Solde avant la déduction des congés
			$soldeConge = $formule -> calculSoldeConge($soldeCongeDao, $date_fin_mois, $droitConge, $totalCongePris);
			$nouveauSoldeConge = $soldeConge;
			// Mettre à jour le nouveau solde de congé (si (mois == 6 && nouveauSolde >= 45) ? 45 : nouveauSolde)

		// ----------- Résumé
			$Charge = round($cnapsPrime) + round($smiePrime) + round($montantIRSA[4]) + round($montantHopital);
			$SalaireNet = round($salaire_brut_final) - $Charge + $rappel_salaire_net[0];
			$Retenus = round($participationMedicale[2]) + $montantAvance[0] + round($montantDepassement[0]) + round($montantDiversRetenus[0]);
			$NETaPayer = $SalaireNet - $Retenus;

		// -------------------- Affichage
			array_push($response, array(
				'idContrat' => $a,
				'matricule' => $z,
				'nomPrenom' => $ac.' '.$ad,
				'titrePoste' => $af,
				'categorieProfessionnelle' => $categorie_actuelle,
				'dateEmbauche' => $m,
				'datePrisePoste' => $n,
				// Données à afficher
				'SBConclu2017' => $SB_conclu, // salaire du dernière année (2017)
				'Bjanvier' => $salaire_debut_janvier, // salaire selon la grille
				'salaireBase' => $salaire_base, // salaire incluant les augmentations 15% et 5%
				'salaireSupplementaire' => round($salaire_supplementaire), // la différence du salaire de base réel et les augmentations
				'nonPercu' => $somme_perdue, // Ce montant est la différence excédentaire après les 15% et 5%. Ce montant n'est pas perçu par l'employé
				'primeExceptionnel' => $prime_exceptionnel,
				'anomalie' => $anomalie_salaire,
				'dateDebutInterim' => $date_debut_interim,
				'dateFinInterim' => $date_fin_interim,
				'montantInterim' => $salaire_interim[0],
				'baseParHeure' => round($tauxHoraire),
				'montantHS' => round($montantHS[0]),
				'sBrutAvantMaternite' => round($salaire_brut_avant_conge_maternite),
				'montantHMinus' => round($montantHeureMinus[0]), //1!AG
				'dureeCongeMaterniteCeMois' => $dureeCongeMaterniteCeMois, //1!AJ
				'deductionMaternite' => round($DeductionMaternite), // 1!AK
				'salaireBrutFinal' => round($salaire_brut_final), //1!AN
				'cnapsBase' => round($cnapsBase), //1!AO
				'cnapsPrime' => round($cnapsPrime), //1!AP
				'nomCentreSmie' => $nomCentreSmie, //1!AQ
				'smiePrime' => round($smiePrime), //1!AS
				'sommeCnapsSmie' => round($somme_cnaps_smie), //1!AT
				'salaireBrutApresCnapsSmie' => round($salaire_brut_apres_cnaps_smie), //1!AU
				'brutApresCnapsSmieArrondi' => round($montantIRSA[0]), // 1!AV
				'montantIrsaApresPlafond' => round($montantIRSA[1]), //1!AW
				'IrsaBrutArrondi' => round($montantIRSA[2]), //1!AZ
				'abattement' => round($montantIRSA[3]), //1!BB
				'IrsaNet' => round($montantIRSA[4]), //1!BC
				'caisseHospitalisation' => round($montantHopital), //1!BE
				'factureFraisMedicaux' => round($participationMedicale[0]), //1!BF
				'tauxFraisMedicaux' => round($participationMedicale[1]), //1!BG
				'participationFraisMedicaux' => round($participationMedicale[2]), //1!BH
				'deductionAvance' => $montantAvance[0], //1!BQ
				'diversRetenus' => round($montantDiversRetenus[0]), //1!BX
				'depassement' => round($montantDepassement[0]), //1!BW
				'Charge' => $Charge,
				'SalaireNet' => $SalaireNet,
				'Retenus' => $Retenus,
				'netAPayer' => $NETaPayer,
				'decallageAvance' => (count($decallageAvance) > 0) ? $decallageAvance[0][0] : 0,
				'congePris' => $totalCongePris,
				'soldeConge' => $nouveauSoldeConge,
				// Données supplémentaires pour l'insertion dans la table historique de salaire
				'idSalaire' => $moisSalarialID[0][0],
				'idAugmGenerale' => $taux_augmentation_generale[0][1],
				'idAugmInflation' => $taux_inflation[0][1],
				'dureeValideInterim' => 14,
				'idHsInfHuitHeures' => $tauxHs[0][0],
				'idHsSupHuitHeures' => $tauxHs[1][0],
				'idHsNuitHabituelle' => $tauxHs[2][0],
				'idHsNuitOccasionnelle' => $tauxHs[3][0],
				'idHsDimanche' => $tauxHs[4][0],
				'idHsFerie' => $tauxHs[5][0],
				'debutMaternite' => (empty($congeMaterniteList)) ? null: $congeMaterniteList[0][4],
				'finMaternite' => (empty($congeMaterniteList)) ? null: $congeMaterniteList[0][5],
				'tauxMaternite' => $taux_reduction_maternite[0][1],
				'idCnaps' => $cnaps[0][0],
				'idSmie' => $i,
				'idIrsa' => $irsa[0][0],
				'idParamAbattement' => $abattement[0][1],
				'idParamHospitalisation' => $hopital[0][1],
				'idHopital' => $participationMedicale[3],
				'idAvance' => (empty($AvList)) ? null: $AvList[0][0],
				'idPoste' => $d,
				'idCategorieProfessionnelle' => $salaire_debut_jan[3],
				'netAPayer' => $NETaPayer,
				'nbEnfant' => $ab,
				'ancienneteDebutJanvier' => $anciennete_debut_janvier,
				'totalHeureHS' => $montantHS[1],
				'salaireBrutInclInflation' => $salaireBrutInclInflation,
				'ancienneCategorie' => $ag,
				'abreviationCentre' => $ah,
				'sBrutAnneeDerniere' => $salaireBrutAnneeDerniere,
				'idHS' => ($montantHS[2] == 0) ? null : $montantHS[2],
				'categorieReelle' => $categorieReelle,
				'rappelBase' => $rappel_salaire_base[0],
				'rappelNet' => $rappel_salaire_net[0],
				'remarque' => implode(' ', $remarque),
				'infHuitHeures' => empty($HSList) ? 0 : $HSList[0][2],
				'supHuitHeures' => empty($HSList) ? 0 : $HSList[0][3],
				'nuitHabituelle' => empty($HSList) ? 0 : $HSList[0][4],
				'nuitOccasionnelle' => empty($HSList) ? 0 : $HSList[0][5],
				'dimanche' => empty($HSList) ? 0 : $HSList[0][6],
				'ferie' => empty($HSList) ? 0 : $HSList[0][7],
				'partEmployeSmie' => $partEmployeSmie,
				'montantAvance' => (!empty($AvList)) ? $AvList[0][4] : 0,
				'dateDemandeAvance' => (!empty($AvList)) ? $AvList[0][5] : '',
				'dateDebutAvance' => (!empty($AvList)) ? $AvList[0][6] : '',
				'sommeDeductionAvance' => $montantAvance[2],
				'idCentre' => $c,
				'nbHeuresMinus' => $montantHeureMinus[2]
			));
	}
	/* END FOREACH CONTRAT */
	$arr['data'] = array_values($response);
	$arr['total'] = $nb;

	$arr = json_encode($arr);

	if ($action == 1) { // Renvoyer les données pour l'affichage dans le tableau
		affichage($arr);
	} else if ($action == 2) { // Faire le sauvegarde des données dans des tables
		sauvegarde(array_values($response), $dao, $annee_en_cours, $mois_en_cours, $moisSalarialID[0][0], $date_fin_mois);
	} else if ($action == 3) { // Faire le back up de la DDB
		$dao -> exportDataBase();
		$arrayValue = array();
		array_push($arrayValue, array('response' => 1));
		$result['data'] = array_values($arrayValue);
					
		$result = json_encode($result);
		echo $result;
	} else if ($action == 4) { // Faire les différences des NAP entre le mois dernier et le mois en cours et l'exporte en Excel
		$spreadsheet = new Spreadsheet();
		$mois = $_REQUEST['mois'];
		exportDifference(array_values($response), $spreadsheet, $dao, $mois);
	} else if ($action == 5) { // Faire un prototype d'Etat de paie et l'exporter
		$spreadsheet = new Spreadsheet();
		$mois = $_REQUEST['mois'];
		$typeUtilisateur = json_decode($_REQUEST['typeUtilisateur']);
		exportPreEtatDePaie(array_values($response), $spreadsheet, $mois, $typeUtilisateur);
	}
}

/*
 * Renvoie les données de salaire calculer pour affichage dans le grid 'DetailsSalaire'
 */
function affichage($arr) {
	// echo $arr;
}

/*
 * Enregistre les données après clôture dans les tables historique_salaire, met à jour le réliquat de congé
 */
function sauvegarde($arr, $dao, $annee_en_cours, $mois_en_cours, $idSalaire, $date_fin_mois) {
	$arrayValue = array();
	try {
		foreach( $arr as $row ) 
		{
			$idSalaire = $row['idSalaire'];
			$idContrat = $row['idContrat'];
			$matricule = $row['matricule'];
			$matricule = $dao -> specialCharacters($matricule);
			$idPoste = $row['idPoste'];
			$idCategorieProfessionnelle = $row['idCategorieProfessionnelle'];
			$dateEmbauche = $row['dateEmbauche'];
			$datePrisePoste = $row['datePrisePoste'];
			$sbConclu = $row['SBConclu2017'];
			$sbGrille = $row['Bjanvier'];
			$idAugmGenerale = $row['idAugmGenerale'];
			$idAugmInflation = $row['idAugmInflation'];
			$salaireBase = $row['salaireBase'];
			$salaireSupplementaire = $row['salaireSupplementaire'];
			$sNonPercu = $row['nonPercu'];
			$primeExceptionnel = $row['primeExceptionnel'];
			$anomalie = empty($row['anomalie']) ? 0 : 1;
			$dateDebutInterim = empty($row['dateDebutInterim']) ? "(SELECT null FROM dual)" : "'".$row['dateDebutInterim']."'";
			$dateFinInterim = empty($row['dateFinInterim']) ? "(SELECT null FROM dual)" : "'".$row['dateFinInterim']."'";
			$montantInterim = $row['montantInterim'];
			$dureeValideInterim = $row['dureeValideInterim'];
			$baseParHeure = $row['baseParHeure'];
			$idHsInfHuitHeures = $row['idHsInfHuitHeures'];
			$idHsSupHuitHeures = $row['idHsSupHuitHeures'];
			$idHsNuitHabituelle = $row['idHsNuitHabituelle'];
			$idHsNuitOccasionnelle = $row['idHsNuitOccasionnelle'];
			$idHsDimanche = $row['idHsDimanche'];
			$idHsFerie = $row['idHsFerie'];
			$montantHs = $row['montantHS'];
			$sbrutAvMaternite = $row['sBrutAvantMaternite'];
			$montantHMinus = $row['montantHMinus'];
			$debutMaternite = empty($row['debutMaternite']) ? "(SELECT null FROM dual)" : "'".$row['debutMaternite']."'";
			$finMaternite = empty($row['finMaternite']) ? "(SELECT null FROM dual)" : "'".$row['finMaternite']."'";
			$dureeMaterniteCeMois = $row['dureeCongeMaterniteCeMois'];
			$tauxMaternite = $row['tauxMaternite'];
			$deductionMaternite = $row['deductionMaternite'];
			$sbrutFinal = $row['salaireBrutFinal'];
			$idCnaps = $row['idCnaps'];
			$cnapsBase = $row['cnapsBase'];
			$cnapsPrime = $row['cnapsPrime'];
			$idSmie = $row['idSmie'];
			$nomSmie = $row['nomCentreSmie'];
			$smiePrime = $row['smiePrime'];
			$sommeCnapsSmie = $row['sommeCnapsSmie'];
			$salaireBrutApresCnapsSmie = $row['salaireBrutApresCnapsSmie'];
			$brutApresCnapsSmieArrondi = $row['brutApresCnapsSmieArrondi'];
			$montantIrsaApresPlafond = $row['montantIrsaApresPlafond'];
			$sImposable = $row['IrsaBrutArrondi'];
			$idIrsa = $row['idIrsa'];
			$idParamAbattement = $row['idParamAbattement'];
			$abattement = $row['abattement'];
			$irsaNet = $row['IrsaNet'];
			$idParamHospitalisation = $row['idParamHospitalisation'];
			$caisseHospitalisation = $row['caisseHospitalisation'];
			$fraisMedicaux = $row['factureFraisMedicaux'];
			$idHopital = $row['idHopital'];
			$participationMedicale = $row['participationFraisMedicaux'];
			$idAvance = empty($row['idAvance']) ? "(SELECT null FROM dual)" : $row['idAvance'];
			$deductionAvance = $row['deductionAvance'];
			$diversRetenus = $row['diversRetenus'];
			$depassement = $row['depassement'];
			$congePris = $row['congePris'];
			$soldeConge = $row['soldeConge'];
			$netAPayer = $row['netAPayer'];
			$nbEnfant = empty($row['nbEnfant']) ? 0 : $row['nbEnfant'];
			$ancienneteDebutJanvier = empty($row['ancienneteDebutJanvier']) ? 0 : $row['ancienneteDebutJanvier'];
			$categorieProfessionnelle = empty($row['categorieProfessionnelle']) ? 0 : $row['categorieProfessionnelle'];
			$totalHeureHS = $row['totalHeureHS'];
			$salaireBrutInclInflation = $row['salaireBrutInclInflation'];
			$ancienneCategorie = $row['ancienneCategorie'];
			$sBrutAnneeDerniere = empty($row['sBrutAnneeDerniere']) ? 0 : $row['sBrutAnneeDerniere'];
			$idHS = empty($row['idHS']) ? "(SELECT null FROM dual)" : "'".$row['idHS']."'";
			$categorieReelle = $row['categorieReelle'];
			$rappelBase = $row['rappelBase'];
			$rappelNet = $row['rappelNet'];
			$remarque = $dao -> specialCharacters($row['remarque']);
			
			$historique[] = "($idSalaire,$idContrat,'$matricule',$idPoste,$idCategorieProfessionnelle,'$dateEmbauche','$datePrisePoste','$sbConclu','$sbGrille',$idAugmGenerale,$idAugmInflation,'$salaireBase','$salaireSupplementaire','$sNonPercu','$primeExceptionnel','$anomalie',$dateDebutInterim,$dateFinInterim,'$montantInterim','$dureeValideInterim','$baseParHeure',$idHsInfHuitHeures,$idHsSupHuitHeures,$idHsNuitHabituelle,$idHsNuitOccasionnelle,$idHsDimanche,$idHsFerie,'$montantHs','$sbrutAvMaternite','$montantHMinus',$debutMaternite,$finMaternite,'$dureeMaterniteCeMois',$tauxMaternite,'$deductionMaternite','$sbrutFinal',$idCnaps,'$cnapsBase','$cnapsPrime',$idSmie,'$nomSmie','$smiePrime','$sommeCnapsSmie','$salaireBrutApresCnapsSmie','$brutApresCnapsSmieArrondi', '$montantIrsaApresPlafond','$sImposable',$idIrsa,$idParamAbattement,'$abattement','$irsaNet',$idParamHospitalisation,'$caisseHospitalisation','$fraisMedicaux',$idHopital,'$participationMedicale',$idAvance,'$deductionAvance','$diversRetenus','$depassement','$congePris','$soldeConge', '$netAPayer', $nbEnfant, '$totalHeureHS', '$salaireBrutInclInflation', '$sBrutAnneeDerniere', $idHS, '$categorieReelle', '$rappelBase', '$rappelNet', '$remarque')";

			if ($deductionAvance > 0) $detailsAvance[] = "($idAvance,$idSalaire,'$deductionAvance')";
			
			// mise à jour du solde de congé
				$dao -> updateSoldeConge($idContrat, $soldeConge, $date_fin_mois->format('Y-m-d'));
			
			if ($ancienneCategorie != $categorieProfessionnelle) {
				// mise à jour de la catégorie professionnelle
					$dao -> updateCategorieProfessionnelle($idContrat, $idCategorieProfessionnelle, $ancienneteDebutJanvier, $categorieProfessionnelle);
			
				// enregistre dans la table change_categorie
					$dao -> insertIntoChangeCategorie($idSalaire, $idContrat, $ancienneteDebutJanvier, $idCategorieProfessionnelle, $categorieProfessionnelle);
			}
		}
		// Sauvegarde dans historique
			$insertHistorique = $dao -> insertIntoHistoriqueSalaire($historique);
			// echo '<br> insertHistorique: '. $insertHistorique . ' count historique='.count($historique);

		// Insertion dans details_avance
			if (isset($detailsAvance)) {
				$insertDetailsAvance = $dao -> saveOrUpdateDetailsAvance($detailsAvance);	
				// echo '<br> insertDetailsAvance: '. $insertDetailsAvance . ' count detailsAvance='.count($detailsAvance);
				$insertAvance = $insertDetailsAvance;
			} else $insertAvance = 0;

		// Mise à jour du statut en "Clôturé"
			$dateClotureSalaire = date('Y-m-d');
			$typeSalaire = 1; // 1=Mensuel, 2=journalier, 3=contractuel
			$clotureSalaire = 1; // 1=Clôturée; 0=En cours

			$updateSalaire = $dao -> saveOrUpdateSalaire(
				$idSalaire,
				$mois_en_cours,
				$annee_en_cours,
				$typeSalaire,
				$dateClotureSalaire,
				$clotureSalaire
			);
			// echo '<br> updateSalaire: '. $updateSalaire;

			array_push($arrayValue, array('response' => 1));
			$result['data'] = array_values($arrayValue);
						
			$result = json_encode($result);
			echo $result;
	} catch (Exception $e) {
		array_push($arrayValue, array('response' => $e->getMessage()));
		$result['data'] = $arrayValue;
		$result = json_encode($result);
		echo $result;
	}
}

/* 
 * Cette fonction sauvegarde les Net à Payer du mois en cours dans une table temporaire "netapayer"
 * Fait une jointure entre le Net à Payer du mois dernier et la table "netapayer" 
 * Exporte les données dans un fichier Excel qui contient une formule de différence
 */
function exportDifference($arr, $spreadsheet, $dao, $mois) {
	try {
		// Save Net à Paye dans table temporaire "netapayer"
		foreach( $arr as $row ) 
		{
			$idContrat = $row['idContrat'];
			$netAPayer = $row['netAPayer'];
			
			$historique[] = "($idContrat, $netAPayer)";
		}
		$resultat = $dao -> go($historique);

		ini_set('memory_limit', '-1'); // It will take unlimited memory usage of server
		$filename = 'Différence-'.$mois;
		$spreadsheet = spreadSheetData(0, $filename, $mois, $resultat, $spreadsheet);
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. utf8_decode($filename) .'.xlsx"'); /*-- $filename is  xsl filename ---*/
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		header('downloadToken: true');
		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		
		$writer = new Xlsx($spreadsheet);
		$writer->save('php://output');
	} catch (Exception $e) {
		array_push($arrayValue, array('response' => $e->getMessage()));
		$result['data'] = $arrayValue;
		$result = json_encode($result);
		echo $result;
	}
}

/*
 * Créer une Feuille de calcul dans un classeur Excel
 * Utilisé dans l'export des différences
 */
function spreadSheetData($sheetIndex, $filename, $mois, $res, $spreadsheet) {
	// Create a new worksheet called "My Data"
	$myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $filename);
	// Attach the "My Data" worksheet as the first worksheet in the Spreadsheet object
	$spreadsheet->addSheet($myWorkSheet, $sheetIndex);

	$cell_st =[
		'font' =>['bold' => true],
		'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER]
		];
	$cell_horizontal_center = [
		'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER]
		];
	$border_style = [
		'allBorders' => [
			'borderStyle' => Border::BORDER_THIN,
			'color' => [
				'rgb' => '808080'
			]
		]
	];

	$spreadsheet->setActiveSheetIndex($sheetIndex)
		->setCellValue('A1', 'Centre')
		->setCellValue('B1', 'Matricule')
		->setCellValue('C1', 'Nom & prénoms')
		->setCellValue('D1', 'Poste occupé')
		->setCellValue('E1', 'Net à Payer dernier')
		->setCellValue('F1', 'Net à Payer actuel')
		->setCellValue('G1', 'Différence');
	$spreadsheet->getActiveSheet()
		->getStyle('A1:G1')
			->applyFromArray($cell_st);
	
	// Les lignes
	$count = $debut_ligne = 1;
	$index = 1;
	$nbLigne = count($res) + $count - 1;
	// Insertion des lignes
	foreach ($res as list($a, $b, $c, $d, $e, $f, $g, $h, $i)) {
		$count += 1;
		//add some data in excel cells
		$spreadsheet->setActiveSheetIndex($sheetIndex)
		->setCellValue('A'.$count, $a)
		->setCellValue('B'.$count, $b)
		->setCellValue('C'.$count, $c)
		->setCellValue('D'.$count, $d)
		->setCellValue('E'.$count, $f)
		->setCellValue('F'.$count, $h)
		->setCellValue('G'.$count, $i);

		$index ++;
	}

	$spreadsheet->getActiveSheet()->getStyle('A1:G'.($nbLigne + 1))->getBorders()->applyFromArray($border_style);
	$spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$spreadsheet->getActiveSheet()
			->getStyle('E'.$debut_ligne.':G'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode('#,##0');


	$spreadsheet->getActiveSheet()->setTitle($filename); //set a title for Worksheet

	return $spreadsheet;
}

/*
 * Cette fonction exporte en Excel un Pré-Etat de paie pour la vérification des données avant la clôture de salaire
 */

function exportPreEtatDePaie($resultat, $spreadsheet, $mois, $typeUtilisateur) {
	try {
	
		ini_set('memory_limit', '-1'); // It will take unlimited memory usage of server
		$res = $resultat;
	
		//object of the Spreadsheet class to create the excel data
		// $spreadsheet = new Spreadsheet();
	
		//make object of the Xlsx class to save the excel file
		$filename = 'Pré-Etat de paie-'.$mois;
		$spreadsheet = spreadSheetDataPre(0, $filename, $mois, $res, $spreadsheet);
	
		if ($typeUtilisateur == 1) {
			$resByCentre = _group_by($res, 'idCentre'); // Groupe les résultats par id_centre
			$i = 1;
			foreach ($resByCentre as $res1) {
				$spreadsheet = spreadSheetDataPre($i, 'Etat-'.$res1[0]['abreviationCentre'].'-'.$mois, $mois, $res1, $spreadsheet);
				$i ++;
			}
		}
	
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. utf8_decode($filename) .'.xlsx"'); /*-- $filename is  xsl filename ---*/
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		header('downloadToken: true');
		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		
		$writer = new Xlsx($spreadsheet);
		$writer->save('php://output');
	} catch (Exception $e) {
		array_push($arrayValue, array('response' => $e->getMessage()));
		$result['data'] = $arrayValue;
		$result = json_encode($result);
		echo $result;
	}
}

/*
 * Fonction pour créer l'Excel de Pré-Etat de paie
 */
function spreadSheetDataPre($sheetIndex, $filename, $mois, $res, $spreadsheet) {
	// Create a new worksheet called "My Data"
	$myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $filename);

	// Attach the "My Data" worksheet as the first worksheet in the Spreadsheet object
		$spreadsheet->addSheet($myWorkSheet, $sheetIndex);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('E1', 'ETAT DE PAIE');
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('E2', $mois);
		$spreadsheet->getActiveSheet()->getStyle('E1:E2')
			->getFont()
			->setName('Arial')
			->setSize(14)
			->setBold(true);
		$spreadsheet->getDefaultStyle()
			->getFont()
			->setName('Arial')
			->setSize(8);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('I1', 'ASSOCIATION POUR LE DEVELOPPEMENT DE L\'ENERGIE SOLAIRE');
		$spreadsheet->getActiveSheet()->mergeCells('I1:L1');
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('I2', 'ROUTE BETANIMENA');
		$spreadsheet->getActiveSheet()->mergeCells('I2:J2');
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('I3', 'TOLIARA');
		$spreadsheet->getActiveSheet()->mergeCells('I3:J3');
	
	// Les en-têtes des colonnes
		$ligne_entete = 4;
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('A'.$ligne_entete, 'Centre');
		$spreadsheet->getActiveSheet()->mergeCells('A4:A7');
		$spreadsheet->getActiveSheet()->getStyle('A4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('B'.$ligne_entete, 'Calcul centre');
		$spreadsheet->getActiveSheet()->mergeCells('B4:B7');
		$spreadsheet->getActiveSheet()->getStyle('B4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('C'.$ligne_entete, 'N°');
		$spreadsheet->getActiveSheet()->mergeCells('C4:C7');
		$spreadsheet->getActiveSheet()->getStyle('C4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('D'.$ligne_entete, 'Calcul nombre d\'employé');
		$spreadsheet->getActiveSheet()->mergeCells('D4:D7');
		$spreadsheet->getActiveSheet()->getStyle('D4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('E'.$ligne_entete, 'Nom et prénom');
		$spreadsheet->getActiveSheet()->mergeCells('E4:E7');
		$spreadsheet->getActiveSheet()->getStyle('E4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('F'.$ligne_entete, 'Fonction');
		$spreadsheet->getActiveSheet()->mergeCells('F4:F7');
		$spreadsheet->getActiveSheet()->getStyle('F4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('G'.$ligne_entete, 'Date d\'embauche');
		$spreadsheet->getActiveSheet()->mergeCells('G4:G7');
		$spreadsheet->getActiveSheet()->getStyle('G4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('H'.$ligne_entete, 'Date poste actuel');
		$spreadsheet->getActiveSheet()->mergeCells('H4:H7');
		$spreadsheet->getActiveSheet()->getStyle('H4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('I'.$ligne_entete, 'Date débauche');
		$spreadsheet->getActiveSheet()->mergeCells('I4:I7');
		$spreadsheet->getActiveSheet()->getStyle('I4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('J'.$ligne_entete, 'Catégorie');
		$spreadsheet->getActiveSheet()->mergeCells('J4:J7');
		$spreadsheet->getActiveSheet()->getStyle('J4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('K'.$ligne_entete, 'Nombre d\'enfant');
		$spreadsheet->getActiveSheet()->mergeCells('K4:K7');
		$spreadsheet->getActiveSheet()->getStyle('K4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
	
	// SALAIRE
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('L'.($ligne_entete - 2), 'SALAIRE');
		$spreadsheet->getActiveSheet()->mergeCells('L'.($ligne_entete - 2).':AH'.($ligne_entete - 2));
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete - 2))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete - 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
	
	// Salaire de base
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('L'.($ligne_entete - 1), 'Salaire de base');
		$spreadsheet->getActiveSheet()->mergeCells('L'.($ligne_entete - 1).':L7');
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete - 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
	
	// Rappel
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('M'.($ligne_entete - 1), 'Rappel imposable');
		$spreadsheet->getActiveSheet()->mergeCells('M'.($ligne_entete - 1).':M7');
		$spreadsheet->getActiveSheet()->getStyle('M'.($ligne_entete - 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
	
	// Salaire supplémentaire
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('N'.($ligne_entete - 1), 'Salaire supplémentaire');
		$spreadsheet->getActiveSheet()->mergeCells('N'.($ligne_entete - 1).':AG'.($ligne_entete - 1));
		$spreadsheet->getActiveSheet()->getStyle('N'.($ligne_entete - 1))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('N'.($ligne_entete), 'Prime exceptionnelle');
		$spreadsheet->getActiveSheet()->mergeCells('N'.($ligne_entete).':N7');
		$spreadsheet->getActiveSheet()->getStyle('N'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Salaire intérim
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('O'.($ligne_entete), 'Salaire intérim');
		$spreadsheet->getActiveSheet()->mergeCells('O'.($ligne_entete).':Q'.($ligne_entete));
		$spreadsheet->getActiveSheet()->getStyle('O'.($ligne_entete + 1))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('O'.($ligne_entete + 1), 'Début');
		$spreadsheet->getActiveSheet()->mergeCells('O'.($ligne_entete + 1).':O7');
		$spreadsheet->getActiveSheet()->getStyle('O'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('P'.($ligne_entete + 1), 'Fin');
		$spreadsheet->getActiveSheet()->mergeCells('P'.($ligne_entete + 1).':P7');
		$spreadsheet->getActiveSheet()->getStyle('P'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('Q'.($ligne_entete + 1), 'Montant');
		$spreadsheet->getActiveSheet()->mergeCells('Q'.($ligne_entete + 1).':Q7');
		$spreadsheet->getActiveSheet()->getStyle('Q'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Heures supplémentaires
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('R'.($ligne_entete), 'Heures supplémentaires > 40 heures par semaine');
		$spreadsheet->getActiveSheet()->mergeCells('R'.($ligne_entete).':Z'.($ligne_entete));
		$spreadsheet->getActiveSheet()->getStyle('R'.($ligne_entete))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('R'.($ligne_entete + 1), 'Heures');
		$spreadsheet->getActiveSheet()->mergeCells('R'.($ligne_entete + 1).':R7');
		$spreadsheet->getActiveSheet()->getStyle('R'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('S'.($ligne_entete + 1), 'Salaire base par heure');
		$spreadsheet->getActiveSheet()->mergeCells('S'.($ligne_entete + 1).':S7');
		$spreadsheet->getActiveSheet()->getStyle('S'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('T'.($ligne_entete + 1), '< 8heures');
		$spreadsheet->getActiveSheet()->mergeCells('T'.($ligne_entete + 1).':T7');
		$spreadsheet->getActiveSheet()->getStyle('T'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('U'.($ligne_entete + 1), '> 8heures');
		$spreadsheet->getActiveSheet()->mergeCells('U'.($ligne_entete + 1).':U7');
		$spreadsheet->getActiveSheet()->getStyle('U'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('V'.($ligne_entete + 1), 'Nuit habituelle');
		$spreadsheet->getActiveSheet()->mergeCells('V'.($ligne_entete + 1).':V7');
		$spreadsheet->getActiveSheet()->getStyle('V'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('W'.($ligne_entete + 1), 'Nuit occasionnelle');
		$spreadsheet->getActiveSheet()->mergeCells('w'.($ligne_entete + 1).':W7');
		$spreadsheet->getActiveSheet()->getStyle('w'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('X'.($ligne_entete + 1), 'Dimanche');
		$spreadsheet->getActiveSheet()->mergeCells('X'.($ligne_entete + 1).':X7');
		$spreadsheet->getActiveSheet()->getStyle('X'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('Y'.($ligne_entete + 1), 'Férié');
		$spreadsheet->getActiveSheet()->mergeCells('Y'.($ligne_entete + 1).':Y7');
		$spreadsheet->getActiveSheet()->getStyle('Y'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('Z'.($ligne_entete + 1), 'Montant heures supp');
		$spreadsheet->getActiveSheet()->mergeCells('Z'.($ligne_entete + 1).':Z7');
		$spreadsheet->getActiveSheet()->getStyle('Z'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Réduction heures minus et congé de maternité
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AA'.($ligne_entete), 'Réduction salaire brut');
		$spreadsheet->getActiveSheet()->mergeCells('AA'.($ligne_entete).':AF'.($ligne_entete));
		$spreadsheet->getActiveSheet()->getStyle('AA'.($ligne_entete))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

	// Heures minus
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AA'.($ligne_entete + 1), 'Heures minus');
		$spreadsheet->getActiveSheet()->mergeCells('AA'.($ligne_entete + 1).':AB'.($ligne_entete + 1));
		$spreadsheet->getActiveSheet()->getStyle('AA'.($ligne_entete + 1))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AA'.($ligne_entete + 2), 'Nombre d\'heures');
		$spreadsheet->getActiveSheet()->mergeCells('AA'.($ligne_entete + 2).':AA7');
		$spreadsheet->getActiveSheet()->getStyle('AA'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AB'.($ligne_entete + 2), 'Réduction salaire');
		$spreadsheet->getActiveSheet()->mergeCells('AB'.($ligne_entete + 2).':AB7');
		$spreadsheet->getActiveSheet()->getStyle('AB'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Congé de maternité
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AC'.($ligne_entete + 1), 'Congé de maternité');
		$spreadsheet->getActiveSheet()->mergeCells('AC'.($ligne_entete + 1).':AF'.($ligne_entete + 1));
		$spreadsheet->getActiveSheet()->getStyle('AC'.($ligne_entete + 1))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AC'.($ligne_entete + 2), 'Début maternité');
		$spreadsheet->getActiveSheet()->mergeCells('AC'.($ligne_entete + 2).':AC7');
		$spreadsheet->getActiveSheet()->getStyle('AC'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AD'.($ligne_entete + 2), 'Fin maternité');
		$spreadsheet->getActiveSheet()->mergeCells('AD'.($ligne_entete + 2).':AD7');
		$spreadsheet->getActiveSheet()->getStyle('AD'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AE'.($ligne_entete + 2), 'Jours du mois en cours');
		$spreadsheet->getActiveSheet()->mergeCells('AE'.($ligne_entete + 2).':AE7');
		$spreadsheet->getActiveSheet()->getStyle('AE'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AF'.($ligne_entete + 2), 'Réduction salaire');
		$spreadsheet->getActiveSheet()->mergeCells('AF'.($ligne_entete + 2).':AF7');
		$spreadsheet->getActiveSheet()->getStyle('AF'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Total salaire supplémentaire
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AG'.($ligne_entete), 'Total salaire supplémentaire');
		$spreadsheet->getActiveSheet()->mergeCells('AG'.($ligne_entete).':AG7');
		$spreadsheet->getActiveSheet()->getStyle('AG'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Salaire BRUT
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AH'.($ligne_entete - 1), 'Salaire brut');
		$spreadsheet->getActiveSheet()->mergeCells('AH'.($ligne_entete - 1).':AH7');
		$spreadsheet->getActiveSheet()->getStyle('AH'.($ligne_entete - 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// SALAIRE
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AI'.($ligne_entete - 1), 'RETENUS');
		$spreadsheet->getActiveSheet()->mergeCells('AI'.($ligne_entete - 1).':BC'.($ligne_entete - 1));
		$spreadsheet->getActiveSheet()->getStyle('AI'.($ligne_entete - 1))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('AI'.($ligne_entete - 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Charges sociales
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AI'.($ligne_entete), 'Chages sociales');
		$spreadsheet->getActiveSheet()->mergeCells('AI'.($ligne_entete).':AL'.($ligne_entete));

		$spreadsheet->getActiveSheet()->getStyle('AC'.($ligne_entete))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AI'.($ligne_entete + 1), 'Prime CNaPS');
		$spreadsheet->getActiveSheet()->mergeCells('AI'.($ligne_entete + 1).':AI7');
		$spreadsheet->getActiveSheet()->getStyle('AI'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Caisse médicale
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AJ'.($ligne_entete + 1), 'Caisse médicale');
		$spreadsheet->getActiveSheet()->mergeCells('AJ'.($ligne_entete +1).':AL'.($ligne_entete +1));

		$spreadsheet->getActiveSheet()->getStyle('AJ'.($ligne_entete +1))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AJ'.($ligne_entete + 2), 'Caisse');
		$spreadsheet->getActiveSheet()->mergeCells('AJ'.($ligne_entete + 2).':AJ7');
		$spreadsheet->getActiveSheet()->getStyle('AJ'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AK'.($ligne_entete + 2), '%');
		$spreadsheet->getActiveSheet()->mergeCells('AK'.($ligne_entete + 2).':AK7');
		$spreadsheet->getActiveSheet()->getStyle('AK'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AL'.($ligne_entete + 2), 'Prime');
		$spreadsheet->getActiveSheet()->mergeCells('AL'.($ligne_entete + 2).':AL7');
		$spreadsheet->getActiveSheet()->getStyle('AL'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
	
	// IRSA
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AM'.($ligne_entete), 'IRSA');
		$spreadsheet->getActiveSheet()->mergeCells('AM'.($ligne_entete).':AQ'.($ligne_entete +1));
		$spreadsheet->getActiveSheet()->getStyle('AM'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AM'.($ligne_entete + 2), 'Salaire base IRSA');
		$spreadsheet->getActiveSheet()->mergeCells('AM'.($ligne_entete + 2).':AM7');
		$spreadsheet->getActiveSheet()->getStyle('AM'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AN'.($ligne_entete + 2), 'Salaire base IRSA arrondi');
		$spreadsheet->getActiveSheet()->mergeCells('AN'.($ligne_entete + 2).':AN7');
		$spreadsheet->getActiveSheet()->getStyle('AN'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AO'.($ligne_entete + 2), 'SBase IRSA > 350 000Ar');
		$spreadsheet->getActiveSheet()->mergeCells('AO'.($ligne_entete + 2).':AO7');
		$spreadsheet->getActiveSheet()->getStyle('AO'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AP'.($ligne_entete + 2), 'IRSA Brut');
		$spreadsheet->getActiveSheet()->mergeCells('AP'.($ligne_entete + 2).':AP7');
		$spreadsheet->getActiveSheet()->getStyle('AP'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AQ'.($ligne_entete + 2), 'IRSA Net');
		$spreadsheet->getActiveSheet()->mergeCells('AQ'.($ligne_entete + 2).':AQ7');
		$spreadsheet->getActiveSheet()->getStyle('AQ'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Caisse Hopital
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AR'.($ligne_entete), 'Caisse hopital');
		$spreadsheet->getActiveSheet()->mergeCells('AR'.($ligne_entete).':AR7');
		$spreadsheet->getActiveSheet()->getStyle('AR'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Frais médicaux
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AS'.($ligne_entete), 'Frais médicaux');
		$spreadsheet->getActiveSheet()->mergeCells('AS'.($ligne_entete).':AU'.($ligne_entete +1));
		$spreadsheet->getActiveSheet()->getStyle('AS'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AS'.($ligne_entete + 2), 'Frais brut');
		$spreadsheet->getActiveSheet()->mergeCells('AS'.($ligne_entete +2).':AS7');
		$spreadsheet->getActiveSheet()->getStyle('AS'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AT'.($ligne_entete + 2), '%');
		$spreadsheet->getActiveSheet()->mergeCells('AT'.($ligne_entete +2).':AT7');
		$spreadsheet->getActiveSheet()->getStyle('AT'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AU'.($ligne_entete + 2), 'Participation');
		$spreadsheet->getActiveSheet()->mergeCells('AU'.($ligne_entete +2).':AU7');
		$spreadsheet->getActiveSheet()->getStyle('AU'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Avances
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AV'.($ligne_entete), 'Avance');
		$spreadsheet->getActiveSheet()->mergeCells('AV'.($ligne_entete).':AZ'.($ligne_entete +1));
		$spreadsheet->getActiveSheet()->getStyle('AV'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AV'.($ligne_entete + 2), 'Date');
		$spreadsheet->getActiveSheet()->mergeCells('AV'.($ligne_entete +2).':AV7');
		$spreadsheet->getActiveSheet()->getStyle('AV'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AW'.($ligne_entete + 2), 'Montant brut');
		$spreadsheet->getActiveSheet()->mergeCells('AW'.($ligne_entete +2).':AW7');
		$spreadsheet->getActiveSheet()->getStyle('AW'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AX'.($ligne_entete + 2), '1er remboursement');
		$spreadsheet->getActiveSheet()->mergeCells('AX'.($ligne_entete +2).':AX7');
		$spreadsheet->getActiveSheet()->getStyle('AX'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AY'.($ligne_entete + 2), 'Remboursement');
		$spreadsheet->getActiveSheet()->mergeCells('AY'.($ligne_entete +2).':AY7');
		$spreadsheet->getActiveSheet()->getStyle('AY'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AZ'.($ligne_entete + 2), 'Solde');
		$spreadsheet->getActiveSheet()->mergeCells('AZ'.($ligne_entete +2).':AZ7');
		$spreadsheet->getActiveSheet()->getStyle('AZ'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Dépassement téléphonique
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BA'.($ligne_entete), 'Dépassement téléphonique');
		$spreadsheet->getActiveSheet()->mergeCells('BA'.($ligne_entete).':BA7');
		$spreadsheet->getActiveSheet()->getStyle('BA'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Divers retenus
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BB'.($ligne_entete), 'Divers retenus');
		$spreadsheet->getActiveSheet()->mergeCells('BB'.($ligne_entete).':BC'.($ligne_entete +1));
		$spreadsheet->getActiveSheet()->getStyle('BB'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BB'.($ligne_entete + 2), 'Montant');
		$spreadsheet->getActiveSheet()->mergeCells('BB'.($ligne_entete +2).':BB7');
		$spreadsheet->getActiveSheet()->getStyle('BB'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BC'.($ligne_entete + 2), 'Description divers retenus');
		$spreadsheet->getActiveSheet()->mergeCells('BC'.($ligne_entete +2).':BC7');
		$spreadsheet->getActiveSheet()->getStyle('BC'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Rappel sur salaire Net non imposable
		$spreadsheet->setActiveSheetIndex($sheetIndex)
		->setCellValue('BD'.($ligne_entete - 1), 'Rappel non imposable');
		$spreadsheet->getActiveSheet()->mergeCells('BD'.($ligne_entete -1).':BD7');
		$spreadsheet->getActiveSheet()->getStyle('BD'.($ligne_entete -1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('BD'.($ligne_entete -1))
		->getAlignment()->setWrapText(true);

	// Net à payer
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BE'.($ligne_entete - 1), 'NET A PAYER');
		$spreadsheet->getActiveSheet()->mergeCells('BE'.($ligne_entete -1).':BE7');
		$spreadsheet->getActiveSheet()->getStyle('BE'.($ligne_entete -1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
	
	// Congé
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BF'.($ligne_entete +1), 'CONGE');
		$spreadsheet->getActiveSheet()->mergeCells('BF'.($ligne_entete +1).':BH'.($ligne_entete +2));
		$spreadsheet->getActiveSheet()->getStyle('BF'.($ligne_entete +1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BF'.($ligne_entete + 3), 'Droit');
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BG'.($ligne_entete + 3), 'Pris');
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BH'.($ligne_entete + 3), 'Solde');

	// Les lignes
		$count = $debut_ligne = 8;
		$index = 1;
		$nbLigne = count($res) + $count - 1;
		// var_dump($res);
		// Insertion des lignes
		foreach ($res as $row) {
			$count += 1;
			//add some data in excel cells
			$date_embauche = new DateTime($row['dateEmbauche']);
			$date_prise_poste = new DateTime($row['datePrisePoste']);
			$debut_interim = empty($row['dateDebutInterim']) ? '' : new DateTime($row['dateDebutInterim']);
			$fin_interim = empty ($row['dateFinInterim']) ? '' : new DateTime($row['dateFinInterim']);
			$debut_maternite = empty ($row['debutMaternite']) ? '' : new DateTime($row['debutMaternite']);
			$fin_maternite = empty ($row['finMaternite']) ? '' : new DateTime($row['finMaternite']);
			$demande_avance = empty($row['dateDemandeAvance']) ? '' : new DateTime($row['dateDemandeAvance']);
			$premier_remboursement = empty($row['dateDebutAvance']) ? '' : new DateTime($row['dateDebutAvance']);
			$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('A'.$count, $row['abreviationCentre'])
			->setCellValue('B'.$count, $row['idCentre'])
			->setCellValue('C'.$count, $index)
			->setCellValue('E'.$count, $row['nomPrenom'])
			->setCellValue('F'.$count, $row['titrePoste'])
			->setCellValue('G'.$count, $date_embauche->format('d/m/Y'))
			->setCellValue('H'.$count, $date_prise_poste->format('d/m/Y'))
			->setCellValue('J'.$count, $row['categorieReelle'])
			->setCellValue('K'.$count, $row['nbEnfant'])
			->setCellValue('L'.$count, $row['salaireBase'])
			->setCellValue('M'.$count, $row['rappelBase'])
			->setCellValue('N'.$count, $row['primeExceptionnel'])
			->setCellValue('O'.$count, empty($debut_interim) ? '' : $debut_interim->format('d/m/Y'))
			->setCellValue('P'.$count, empty($fin_interim) ? '' : $fin_interim->format('d/m/Y'))
			->setCellValue('Q'.$count, $row['montantInterim'])
			->setCellValue('R'.$count, $row['totalHeureHS'])
			->setCellValue('S'.$count, $row['baseParHeure'])
			->setCellValue('T'.$count, $row['infHuitHeures'])
			->setCellValue('U'.$count, $row['supHuitHeures'])
			->setCellValue('V'.$count, $row['nuitHabituelle' ])
			->setCellValue('w'.$count, $row['nuitOccasionnelle'])
			->setCellValue('X'.$count, $row['dimanche'])
			->setCellValue('Y'.$count, $row['ferie'])
			->setCellValue('Z'.$count, $row['montantHS'])
			->setCellValue('AA'.$count, $row['nbHeuresMinus'])
			->setCellValue('AB'.$count, $row['montantHMinus'])
			->setCellValue('AC'.$count, empty($debut_maternite) ? '' : $debut_maternite->format('d/m/Y'))
			->setCellValue('AD'.$count, empty($fin_maternite) ? '' : $fin_maternite->format('d/m/Y'))
			->setCellValue('AE'.$count, $row['dureeCongeMaterniteCeMois'])
			->setCellValue('AF'.$count, $row['deductionMaternite'])
			->setCellValue('AG'.$count, ($row['primeExceptionnel'] + $row['salaireSupplementaire'] + $row['montantHS'] + $row['montantInterim']))
			->setCellValue('AH'.$count, $row['salaireBrutFinal'])
			->setCellValue('AI'.$count, $row['cnapsPrime'])
			->setCellValue('AJ'.$count, $row['nomCentreSmie'])
			->setCellValue('AK'.$count, $row['partEmployeSmie'].'%')
			->setCellValue('AL'.$count, $row['smiePrime'])
			->setCellValue('AM'.$count, $row['salaireBrutApresCnapsSmie'])
			->setCellValue('AN'.$count, $row['brutApresCnapsSmieArrondi'])
			->setCellValue('AO'.$count, $row['montantIrsaApresPlafond'])
			->setCellValue('AP'.$count, $row['IrsaBrutArrondi'])
			->setCellValue('AQ'.$count, $row['IrsaNet'])
			->setCellValue('AR'.$count, $row['caisseHospitalisation'])
			->setCellValue('AS'.$count, $row['factureFraisMedicaux'])
			->setCellValue('AT'.$count, empty($row['tauxFraisMedicaux'])? '' : ($row['tauxFraisMedicaux'].'%'))
			->setCellValue('AU'.$count, $row['participationFraisMedicaux'])
			->setCellValue('AV'.$count, empty($demande_avance) ? '' : $demande_avance->format('d/m/Y'))
			->setCellValue('AW'.$count, $row['montantAvance'])
			->setCellValue('AX'.$count, empty($premier_remboursement) ? '' : $premier_remboursement->format('d/m/Y'))
			->setCellValue('AY'.$count, $row['deductionAvance'])
			->setCellValue('AZ'.$count, ($row['montantAvance'] - ($row['sommeDeductionAvance'] + $row['deductionAvance'])))
			->setCellValue('BA'.$count, $row['depassement'])
			->setCellValue('BB'.$count, $row['diversRetenus'])
			->setCellValue('BD'.$count, $row['rappelNet'])
			->setCellValue('BE'.$count, $row['netAPayer'])
			->setCellValue('BF'.$count, ($row['congePris'] + $row['soldeConge']))
			->setCellValue('BG'.$count, $row['congePris'])
			->setCellValue('BH'.$count, $row['soldeConge']);

			$index ++;
		}
	
	//Mise en forme et Styles
		$cell_st =[
			'font' =>['bold' => true],
			'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER]
			];
		$cell_horizontal_center = [
			'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER]
			];
		$spreadsheet->getActiveSheet()
			->getStyle('A1:BH8')
				->applyFromArray($cell_st);
		$spreadsheet->getActiveSheet()->getStyle('I1:I3')
			->getFont()
			->setBold(false);
		$spreadsheet->getActiveSheet()->getStyle('I1:I3')
			->getAlignment()
			->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

		$spreadsheet->getActiveSheet()
			->getStyle('L'.$debut_ligne.':AI'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode('#,##0');
		$spreadsheet->getActiveSheet()
			->getStyle('AL'.$debut_ligne.':AS'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode('#,##0');
		$spreadsheet->getActiveSheet()
			->getStyle('AW'.$debut_ligne.':BE'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode('#,##0');
		$spreadsheet->getActiveSheet()
			->getStyle('BF'.$debut_ligne.':BH'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode('#,##0.0');

		$spreadsheet->getActiveSheet()
			->getStyle('AK'.$debut_ligne.':AK'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
		$spreadsheet->getActiveSheet()
			->getStyle('AK'.$debut_ligne.':AK'.($nbLigne + 1))
				->getAlignment()
					->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->getActiveSheet()
			->getStyle('AT'.$debut_ligne.':AT'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
		$spreadsheet->getActiveSheet()
			->getStyle('AT'.$debut_ligne.':AT'.($nbLigne + 1))
				->getAlignment()
					->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->getActiveSheet()->getStyle('A'.$ligne_entete.':K'.$ligne_entete)->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete -1).':M'.($ligne_entete -1))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('N'.($ligne_entete))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('O'.($ligne_entete +1).':Z'.($ligne_entete +1))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('AA'.($ligne_entete +2).':AF'.($ligne_entete +2))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('AG'.($ligne_entete))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('AH'.($ligne_entete -1))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('AM'.($ligne_entete +2).':AQ'.($ligne_entete +2))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('AR'.($ligne_entete))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('AL'.($ligne_entete +2).':AY'.($ligne_entete +2))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('BA'.($ligne_entete))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('BB'.($ligne_entete +2).':BC'.($ligne_entete +2))->getAlignment()->setWrapText(true);

		$border_style = [
			'allBorders' => [
				'borderStyle' => Border::BORDER_THIN,
				'color' => [
					'rgb' => '808080'
				]
			]
		];

		$spreadsheet->getActiveSheet()->getStyle('A'.$ligne_entete.':K'.($nbLigne + 1))->getBorders()->applyFromArray($border_style);
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete - 1).':BD'.($nbLigne + 1))->getBorders()->applyFromArray($border_style);
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete - 1).':BE'.($nbLigne + 1))->getBorders()->applyFromArray($border_style);
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete - 2).':AH'.($ligne_entete - 2))->getBorders()->applyFromArray($border_style);
		$spreadsheet->getActiveSheet()->getStyle('BE'.($ligne_entete + 1).':BH'.($nbLigne + 1))->getBorders()->applyFromArray($border_style);
					
		$cols = $spreadsheet->getActiveSheet()->getColumnIterator('G', 'BH');
		while ($cols->valid()) {
			$col = $cols->current();
			$nomCol = $col->getColumnIndex();

			$spreadsheet->getActiveSheet()->getColumnDimension($nomCol)->setWidth(16);

			$cols->next();
		}

		$cols2 = $spreadsheet->getActiveSheet()->getColumnIterator('E', 'F');
		while ($cols2->valid()) {
			$col2 = $cols2->current();
			$nomCol2 = $col2->getColumnIndex();

			$spreadsheet->getActiveSheet()->getColumnDimension($nomCol2)->setAutoSize(true);

			$cols2->next();
		}

		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(8);
		$spreadsheet->getActiveSheet()->getStyle('B'.($debut_ligne).':B'.($nbLigne + 1))->applyFromArray($cell_horizontal_center);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getStyle('C'.($debut_ligne).':C'.($nbLigne + 1))->applyFromArray($cell_horizontal_center);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(11);
		$spreadsheet->getActiveSheet()->getStyle('K'.($debut_ligne).':K'.($nbLigne + 1))->applyFromArray($cell_horizontal_center);

	// Formules de SOUS TOTAUX
		$spreadsheet->getActiveSheet()
			->setCellValue('D'.$debut_ligne, '=SUBTOTAL(2,C'.($debut_ligne + 1).':C'.($nbLigne + 1).')');
		$formulas_column = array(
			array('K', 'N'),
			array('Q', 'AB'),
			array('AE', 'AJ'),
			array('AL', 'AS'),
			array('AU', 'AU'),
			array('AU', 'AU'),
			array('AW', 'AW'),
			array('AY', 'BB'),
			array('BD', 'BH')
		);

		foreach ($formulas_column as list($start, $end)) {
			$cols3 = $spreadsheet->getActiveSheet()->getColumnIterator($start, $end);
			while ($cols3->valid()) {
				$col3 = $cols3->current();
				$nomCol3 = $col3->getColumnIndex();

				$spreadsheet->getActiveSheet()
				->setCellValue($nomCol3.$debut_ligne, '=SUBTOTAL(9,'.$nomCol3.($debut_ligne + 1).':'.$nomCol3.($nbLigne + 1).')');
				$spreadsheet->getActiveSheet()->getStyle($nomCol3.$debut_ligne)->getFill()
				->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
				->getStartColor()->setARGB('DCDCDC');

				$cols3->next();
			}
		}	
	
		$spreadsheet->getActiveSheet()->setTitle($filename); //set a title for Worksheet

		return $spreadsheet;
}

/*
 * Groupe les données par centre
 */
function _group_by($array, $key) {
    $return = array();
    foreach($array as $val) {
        $return[$val[$key]][] = $val;
    }
    return $return;
}
	
?>
