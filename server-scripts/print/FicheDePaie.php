<?php
$arrayValue = array();
// Load autoloader (using Composer)
require '../vendor/autoload.php';
require_once '../DataAccessObject.php';

class MYPDF extends TCPDF {

	// Page footer
	public function Footer() {
		$interligne = 5;
		// Position at 40 mm from bottom
		$this->SetY(-28);
		// Set font
		$this->SetFont('helvetica', 'I', 10);
		// #Cell(w, h = 0, txt = '', border = 0, ln = 0, align = '', fill = 0, link = nil, stretch = 0, ignore_min_height = false, calign = 'T', valign = 'M') ⇒ Object
		$this->Cell(43, $interligne, 'Pour acquit,', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		$this->Cell(43, $interligne, 'L\'employé(e)', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		$this->Cell(43, $interligne, 'La direction', 0, false, 'C', 0, '', 0, false, 'T', 'M');

		$this->SetFont('helvetica', '', 5.5);
		$this->Ln(16);
		$this->Cell(0, $interligne, 'Siège social: im Grindel 6 - CH-8932 Mettmentesstten - SUISSE', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		$this->Ln(2);
		// $this->Cell(0, $interligne, 'email: regulaochsner@adesolaire.org', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		// $this->Ln(2);
		$this->Cell(0, $interligne, 'Représentation Madagascar: Mitsinjo Betanimena - 601 Toliara - BP 637, tél. (020) 94 444 36 - directeurs.national@adesolaire.org', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		$this->Ln(2);
		$this->Cell(0, $interligne, 'www.adesolaire.org', 0, false, 'C', 0, 'http://www.adesolaire.org', 0, false, 'T', 'M');
	}
}

try {
	$dao = new DataAccessObject();

	// $infoRequest détermine le genre de requête à faire
	$infoRequest = json_decode($_POST['infoRequest']);
	
	if ($infoRequest == 1) { // Toutes les historiques de salaire
		$typeUtilisateur = json_decode($_POST['typeUtilisateur']);
		$idCentre = json_decode($_POST['idCentre']);
		if ($typeUtilisateur == 1) $where = "WHERE 1";
		else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE c.ID_CENTRE = $idCentre";

		ini_set('memory_limit', '-1'); // It will take unlimited memory usage of server
		$res = $dao -> getHistoriqueSalaire($where);
	} else if ($infoRequest == 3) { // Toutes les historiques d'un mois de salaire
		$moisSalaire = $_POST['idSalaire'];
		$mois = $_POST['mois'];
		$typeUtilisateur = json_decode($_POST['typeUtilisateur']);
		$idCentre = json_decode($_POST['idCentre']);
		if ($typeUtilisateur == 1) $where = "WHERE h.ID_SALAIRE = $moisSalaire";
		else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE h.ID_SALAIRE = $moisSalaire AND c.ID_CENTRE = $idCentre";

		ini_set('memory_limit', '-1'); // It will take unlimited memory usage of server
		$res = $dao -> getHistoriqueSalaire($where);
	} else if ($infoRequest == 4) { // Toutes les historiques du 13ème mois
		$moisSalaire = $_REQUEST['idSalaire'];
		$mois = $_REQUEST['mois'];
		$typeUtilisateur = json_decode($_POST['typeUtilisateur']);
		$idCentre = json_decode($_POST['idCentre']);
		if ($typeUtilisateur == 1) $where = "WHERE h.ID_SALAIRE = $moisSalaire";
		else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE h.ID_SALAIRE = $moisSalaire AND c.ID_CENTRE = $idCentre";

		ini_set('memory_limit', '-1'); // It will take unlimited memory usage of server
		$res = $dao -> getHistoriqueTreizieme($where);
	}

	

	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false); // create TCPDF object with default constructor args

	$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');
	// remove default header/footer
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(true);

	// Création des PDFs
	foreach ($res as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae, $af, $ag, $ah, $ai, $aj, $ak, $al, $am, $an, $ao, $ap, $aq, $ar, $as, $at, $au, $av, $aw, $ax, $ay, $az, $ba, $bb, $bc ,$bd, $be, $bf, $bg, $bh, $bi, $bj, $bk, $bl, $bm, $bn, $bo, $bp, $bq, $br, $bs, $bt, $bu, $bv, $bw, $bx, $by, $bz, $ca, $cb, $cc, $cd, $ce, $cf, $cg, $ch, $ci, $cj, $ck, $cl, $cm, $cn, $co, $cp)) {
		createPDF($pdf, $mois, $a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae, $af, $ag, $ah, $ai, $aj, $ak, $al, $am, $an, $ao, $ap, $aq, $ar, $as, $at, $au, $av, $aw, $ax, $ay, $az, $ba, $bb, $bc ,$bd, $be, $bf, $bg, $bh, $bi, $bj, $bk, $bl, $bm, $bn, $bo, $bp, $bq, $br, $bs, $bt, $bu, $bv, $bw, $bx, $by, $bz, $ca, $cb, $cc, $cd, $ce, $cf, $cg, $ch, $ci, $cj, $ck, $cl, $cm, $cn, $co, $cp, $infoRequest);
		
		createPDF($pdf, $mois, $a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae, $af, $ag, $ah, $ai, $aj, $ak, $al, $am, $an, $ao, $ap, $aq, $ar, $as, $at, $au, $av, $aw, $ax, $ay, $az, $ba, $bb, $bc ,$bd, $be, $bf, $bg, $bh, $bi, $bj, $bk, $bl, $bm, $bn, $bo, $bp, $bq, $br, $bs, $bt, $bu, $bv, $bw, $bx, $by, $bz, $ca, $cb, $cc, $cd, $ce, $cf, $cg, $ch, $ci, $cj, $ck, $cl, $cm, $cn, $co, $cp, $infoRequest);
	}
	/*
	 * I : send the file inline to the browser (default). The plug-in is used if available. The name given by name is used when one selects the "Save as" option on the link generating the PDF.
	 * D : send to the browser and force a file download with the name given by name.
	 * F : save to a local server file with the name given by name.
	 * S : return the document as a string (name is ignored).
	 * FI : equivalent to F + I option
	 * FD : equivalent to F + D option
	 * E : return the document as base64 mime multi-part email attachment (RFC 2045)
	 */
	
	$filename = 'Fiches de paie-'.utf8_decode($mois).'.pdf';

	header('Content-Type: application/pdf');
	header('Content-Disposition: attachment;filename="'. $filename.'"'); /*-- $filename is  xsl filename ---*/
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	
	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	// save the folder into TemplatePDF folder
	// $templateFolder = __DIR__. '\templatePDF\Fiche de paie-'.$mois.'.pdf';
	// $pdf->Output($templateFolder, 'D');

	$pdf->Output($filename, 'I');
} catch (Exception $e) {
	array_push($arrayValue, array('response' => $e->getMessage()));
	$result['data'] = $arrayValue;
	$result = json_encode($result);
	echo $result;
}

function createPDF($pdf, $mois, $a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae, $af, $ag, $ah, $ai, $aj, $ak, $al, $am, $an, $ao, $ap, $aq, $ar, $as, $at, $au, $av, $aw, $ax, $ay, $az, $ba, $bb, $bc ,$bd, $be, $bf, $bg, $bh, $bi, $bj, $bk, $bl, $bm, $bn, $bo, $bp, $bq, $br, $bs, $bt, $bu, $bv, $bw, $bx, $by, $bz, $ca, $cb, $cc, $cd, $ce, $cf, $cg, $ch, $ci, $cj, $ck, $cl, $cm, $cn, $co, $cp, $infoRequest) {
	$interligne = 5;
	$date_embauche = new DateTime($g);
	$date_prise_poste = new DateTime($h);
	$debut_interim = empty($p) ? '' : new DateTime($p);
	$fin_interim = empty ($q) ? '' : new DateTime($q);
	$heure_minus = ($v > 0)? ($v / $s) : '';
	$debut_maternite = empty ($bl) ? '' : new DateTime($bl);
	$fin_maternite = empty ($bm) ? '' : new DateTime($bm);
	$demande_avance = empty($cj) ? '' : new DateTime($cj);
	$premier_remboursement = empty($ck) ? '' : new DateTime($ck);

	$pdf->AddPage('P', 'A5');
	$pdf->SetFont('helvetica', 'BU', 9);
	
	$pdf->Image('../../resources/images/user-profile/logo2.jpg', 115, $interligne, 25, 15, '', 'http://www.adesolaire.org', '', false, 300, '');
	
	$titre = ($infoRequest != 4) ? $mois : str_replace('-12', ' - 13ème mois', $mois);
	$pdf->Cell(50, $interligne, $titre);

	$pdf->SetFont('helvetica', 'B', 12);

	$pdf->Cell(30, $interligne, 'FICHE DE PAIE');
	$pdf->Ln(10);

	$pdf->SetFont('helvetica', '', 10);


	$pdf->Cell(30, $interligne, 'Matricule:');
	$pdf->Cell(30, $interligne, $b);
	$pdf->Ln($interligne);
	$pdf->Cell(30, $interligne, 'Nom - prénom:');
	$pdf->Cell(30, $interligne, $c.' '.$d);
	$pdf->Ln($interligne);
	$pdf->Cell(30, $interligne, 'Fonction:');
	$pdf->Cell(30, $interligne, $e);
	$pdf->Ln($interligne);
	$pdf->Cell(30, $interligne, 'Catégorie:');
	$pdf->Cell(30, $interligne, $cm);
	$pdf->Ln($interligne);
	$pdf->Cell(30, $interligne, 'Date d\'embauche:');
	$pdf->Cell(30, $interligne, $date_embauche->format('d/m/Y'));
	$pdf->Ln($interligne);
	$pdf->Cell(30, $interligne, 'Bureau/Centre:');
	$pdf->Cell(30, $interligne, $bx);
	$pdf->Ln($interligne);

	$h_marge = 10;
	$v_marge = 50;

	$style = array('width' => 0.5);
	$pdf->Line($h_marge, $v_marge, 140, $v_marge); // Horizontal Line(x1, y1, x2, y2) => y1 = y2
	
	$ln_height = array(); // Pour les lignes
	
	if ($infoRequest != 4) {
		// Salaire de base
			$pdf->Ln($interligne);
			$pdf->Cell(30, $interligne, 'Salaire de Base:');
			$salaire_base = ($o == 0) ? '' : "Anomalie salaire          " ;
			$pdf->Cell(100, $interligne, $salaire_base.number_format($k, 1, '.', ' ').'Ar', 0, 0, 'R');
			$pdf->Ln($interligne);
			array_push($ln_height, 2);
			$line_height_supplementaire = array_sum($ln_height);
			$pdf->Line($h_marge, $v_marge + ($interligne * $line_height_supplementaire), 140, $v_marge + ($interligne * $line_height_supplementaire));
		
	
		// Salaire supplémentaire
			$sommeSupplementaire = $n + $l + $t + $r + $cn;
			$sSupplementaireAnneeDerniere = ($y > $sommeSupplementaire) ? $y - ($sommeSupplementaire + $k) : 0;
			if (($sommeSupplementaire) > 0) {
				$pdf->Cell(30, $interligne, 'Supplémentaires:');
				$pdf->Ln($interligne);
				
				array_push($ln_height, 1);
				if ($n > 0) { // Prime exceptionnelle
					$pdf->Cell(30, $interligne, "     - Prime exceptionnelle:");
					$pdf->Cell(100, $interligne, number_format($n, 1, '.', ' ').'Ar', 0, 0, 'R');
					$pdf->Ln($interligne);
					array_push($ln_height, 1);
				}
				if ($cn > 0) { // Rappel sur le salaire de base
					$pdf->Cell(30, $interligne, "     - Rappel sur salaire de base:");
					$pdf->Cell(100, $interligne, number_format($cn, 1, '.', ' ').'Ar', 0, 0, 'R');
					$pdf->Ln($interligne);
					array_push($ln_height, 1);
				}
				if ($r > 0) { // Salaire intérim
					$pdf->Cell(30, $interligne, "     - Salaire intérim:");
					$pdf->Cell(100, $interligne, number_format($r, 1, '.', ' ').'Ar', 0, 0, 'R');
					$pdf->Ln($interligne);
					array_push($ln_height, 1);
				}
				if ($sSupplementaireAnneeDerniere > 0) { // Salaire supplémentaire (année dernière)
					$pdf->Cell(30, $interligne, "     - S.supplémentaire année dernière:");
					$pdf->Cell(100, $interligne, number_format($sSupplementaireAnneeDerniere, 1, '.', ' ').'Ar', 0, 0, 'R');
					$pdf->Ln($interligne);
					array_push($ln_height, 1);
				}
				if ($l > 0) { // Salaire supplémentaire (augmentation inflation)
					$pdf->Cell(30, $interligne, "     - Salaire supplémentaire:");
					$pdf->Cell(100, $interligne, number_format($l, 1, '.', ' ').'Ar', 0, 0, 'R');
					$pdf->Ln($interligne);
					array_push($ln_height, 1);
				}
				if ($t > 0) {
					if ($ba == '5') { // Si c'est un agent d'entretien, afficher le montant HS forfetaire
						$pdf->Cell(30, $interligne, "     - Heures supplémentaires:");
						$pdf->Cell(100, $interligne, 'Montant forfetaire:          '.number_format($t, 1, '.', ' ').'Ar', 0, 0, 'R');
						$pdf->Ln($interligne);
					} else {
						$pdf->Cell(30, $interligne, "     - Heures supplémentaires:");
						$pdf->Cell(100, $interligne, 'Total des HS: '.$by.'                    '.number_format($t, 1, '.', ' ').'Ar', 0, 0, 'R');
						$pdf->Ln($interligne);
					}
					array_push($ln_height, 1);
				}
				$line_height_supplementaire = array_sum($ln_height);
				$pdf->Line($h_marge, $v_marge + ($interligne * $line_height_supplementaire), 140, $v_marge + ($interligne * $line_height_supplementaire));
			}
	}

	// Salaire brut
		if ($infoRequest != 4) {
			$pdf->Cell(30, $interligne, 'Salaire Brut:');
			$line_height = 1;
		}
		else {
			$pdf->Ln($interligne);
			$pdf->Cell(30, $interligne, 'Moyenne du salaire Brut:');
			$line_height = 2;
		}
		$pdf->Cell(100, $interligne, number_format($y, 1, '.', ' ').'Ar', 0, 0, 'R');
		$pdf->Ln($interligne);
		array_push($ln_height, $line_height);

		$line_height_supplementaire = array_sum($ln_height);
		$pdf->Line($h_marge, $v_marge + ($interligne * $line_height_supplementaire), 140, $v_marge + ($interligne * $line_height_supplementaire));

	// Charges sociales
	if ($infoRequest != 4) {
		$pdf->Cell(30, $interligne, 'Charges sociales:');
		$pdf->Ln($interligne);
		array_push($ln_height, 1);
			$pdf->Cell(30, $interligne, "     - CNaPS:");
			$pdf->Cell(60, $interligne, number_format($aa, 1, '.', ' ').'Ar', 0, 0, 'R');
			$pdf->Ln($interligne);
			array_push($ln_height, 1);

			$pdf->Cell(30, $interligne, "     - ".$ab.':');
			$pdf->Cell(60, $interligne, number_format($ac, 1, '.', ' ').'Ar', 0, 0, 'R');
			$pdf->Ln($interligne);
			array_push($ln_height, 1);

		$line_height_supplementaire = array_sum($ln_height);
		$pdf->Line($h_marge, $v_marge + ($interligne * $line_height_supplementaire), 140, $v_marge + ($interligne * $line_height_supplementaire));
	}

	// IRSA
		$pdf->Cell(30, $interligne, 'IRSA:');
		$pdf->Ln($interligne);
		array_push($ln_height, 1);
			$pdf->Cell(30, $interligne, "     - Base imposable:");
			$pdf->Cell(60, $interligne, number_format($ae, 1, '.', ' ').'Ar', 0, 0, 'R');
			$pdf->Ln($interligne);
			array_push($ln_height, 1);

			$pdf->Cell(30, $interligne, "     - IRSA Net: ");
			$pdf->Cell(60, $interligne, number_format($aj, 1, '.', ' ').'Ar', 0, 0, 'R');
			$pdf->Ln($interligne);
			array_push($ln_height, 1);

		$line_height_supplementaire = array_sum($ln_height);
		$pdf->Line($h_marge, $v_marge + ($interligne * $line_height_supplementaire), 140, $v_marge + ($interligne * $line_height_supplementaire));

	// Caisse Hopital
	if ($infoRequest != 4) {
		$pdf->Cell(30, $interligne, "Caisse hospitalisation:");
		$pdf->Cell(60, $interligne, number_format($ak, 1, '.', ' ').'Ar', 0, 0, 'R');
		$pdf->Ln($interligne);
		array_push($ln_height, 1);

		$line_height_supplementaire = array_sum($ln_height);
		$pdf->Line($h_marge, $v_marge + ($interligne * $line_height_supplementaire), 140, $v_marge + ($interligne * $line_height_supplementaire));
	}

	// Total de CHARGES
		$pdf->Cell(30, $interligne, "CHARGES:");
		$Charge = $aa + $ac + $aj + $ak;
	
	// Salaire Net
		$pdf->Cell(60, $interligne, number_format($Charge, 1, '.', ' ').'Ar', 0, 0, 'R');
		$pdf->Ln($interligne);
		array_push($ln_height, 1);
		if ($co > 0) { // Rappel sur salaire Net
			$pdf->Cell(30, $interligne, "Rappel sur salaire Net:");
			$pdf->Cell(100, $interligne, number_format($co, 1, '.', ' ').'Ar', 0, 0, 'R');
			$pdf->Ln($interligne);
			array_push($ln_height, 1);
		}
		$pdf->Cell(30, $interligne, "Salaire Net:");
		$salaire_net = $y - $Charge;
		$pdf->Cell(100, $interligne, number_format($salaire_net, 1, '.', ' ').'Ar', 0, 0, 'R');
		$pdf->Ln($interligne);
		array_push($ln_height, 1);

		$line_height_supplementaire = array_sum($ln_height);
		$pdf->Line($h_marge, $v_marge + ($interligne * $line_height_supplementaire), 140, $v_marge + ($interligne * $line_height_supplementaire));
	
	// Retenus
	if ($infoRequest != 4) {
		if (($an + $ao + $aq + $ap) > 0) {
			$pdf->Cell(30, $interligne, 'Retenus:');
			$pdf->Ln($interligne);
			
			array_push($ln_height, 1);
			if ($an > 0) { // Participation aux frais médicaux
				$pdf->Cell(30, $interligne, "     - Frais médicaux:");
				$pdf->Cell(60, $interligne, number_format($an, 1, '.', ' ').'Ar', 0, 0, 'R');
				$pdf->Ln($interligne);
				array_push($ln_height, 1);
			}
			if ($ao > 0) { // Remboursement avance
				$pdf->Cell(30, $interligne, "     - Remboursement avance:");
				$pdf->Cell(60, $interligne, number_format($ao, 1, '.', ' ').'Ar', 0, 0, 'R');
				$pdf->Ln($interligne);
				array_push($ln_height, 1);
			}
			if ($aq > 0) { // Dépassement téléphonique
				$pdf->Cell(30, $interligne, "     - Dépassement téléphonique:");
				$pdf->Cell(60, $interligne, number_format($aq, 1, '.', ' ').'Ar', 0, 0, 'R');
				$pdf->Ln($interligne);
				array_push($ln_height, 1);
			}
			if ($ap > 0) { // Divers retenus
				$pdf->Cell(30, $interligne, "     - Divers retenus:");
				$pdf->Cell(60, $interligne, number_format($ap, 1, '.', ' ').'Ar', 0, 0, 'R');
				$pdf->Ln($interligne);
				array_push($ln_height, 1);
			}
			$line_height_supplementaire = array_sum($ln_height);
			$pdf->Line($h_marge, $v_marge + ($interligne * $line_height_supplementaire), 140, $v_marge + ($interligne * $line_height_supplementaire));
		}
	}

	// NET A PAYER
		// $pdf->Ln($interligne);
		$pdf->Cell(100, $interligne, "NET A PAYER:", 0, 0, 'R');
		$pdf->Cell(30, $interligne, number_format($au, 1, '.', ' ').'Ar', 1, 0, 'R');
		$pdf->Ln($interligne);

	// Congés
	if ($infoRequest != 4) {
		$pdf->Cell(30, $interligne, 'Congés:');
		$pdf->Ln($interligne);
		array_push($ln_height, 3);
			$droitConge = ($bx == 'CM' || $bx == 'CMc') ? 3.1 : 2.5; // Le droit de congé pour les centres mobiles est de 3.1 les autres est de 2.5

			$pdf->Cell(42, $interligne, "Reliquat (solde antérieur)", 1, 0, 'C');
			$pdf->Cell(30, $interligne, "Droit ce mois", 1, 0, 'C');
			$pdf->Cell(30, $interligne, "Pris ce mois", 1, 0, 'C');
			$pdf->Cell(30, $interligne, "Solde restant", 1, 0, 'C');
			$pdf->Ln($interligne);
			array_push($ln_height, 1);
			
			
			$pdf->Cell(42, $interligne, number_format(($ax - $droitConge + $aw), 1, '.', ' ').'Jours', 1, 0, 'R');
			$pdf->Cell(30, $interligne, number_format($droitConge, 1, '.', ' ').'Jours', 1, 0, 'R');
			$pdf->Cell(30, $interligne, number_format($aw, 1, '.', ' ').'Jours', 1, 0, 'R');
			$pdf->Cell(30, $interligne, number_format($ax, 1, '.', ' ').'Jours', 1, 0, 'R');
			$pdf->Ln($interligne);
			array_push($ln_height, 1);
	
	// Remarques
		if (strlen($cp) > 0) {
			$pdf->Cell(20, $interligne, 'Remarques:');
			$pdf->MultiCell(112, $interligne, $cp, 0, 'L', 0, 0, '', '', true);
			$pdf->Ln($interligne);
			array_push($ln_height, 1);
		}
	}
			
	// Pied de page
	// Signatures = Footer()
		
}
?>