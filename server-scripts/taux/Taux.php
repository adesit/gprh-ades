<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;

if ($infoRequest == 2) {
	$data_key = "data";
	$data = $model -> $data_key;
	SaveOrUpdate($m, $data);
} else if ($infoRequest == 4) {
	/*
	 * Cherche si une ligne avec la valeur ACTIF_TAUX = 1
	 * Si des lignes existes, il faut les remettre à 0
	 */
	$nom = "nomTaux";
	$id = "idTaux";
	$nomTaux = $model -> $nom;
	$idTaux = $model -> $id;
	$checkedId = $m -> CheckExistActif('taux', 'ID_TAUX', "NOM_TAUX ='$nomTaux' AND ACTIF_TAUX = 1");

	ResetActifById($m, $checkedId, 0);

	echo ResetActifById($m, [$idTaux], 1);
} else if ($infoRequest === 3) {
	$data_key = "data";
	$data = $model -> $data_key;
	Delete($m, $data);
}

/*
 * Enregistrer les informations de la Taux d'augmentation
 */
function SaveOrUpdate($dao, $data) {
	$val_actif = $dao -> IsNullOrEmptyString($data -> actifTaux) > 0 ? 0 : $data -> actifTaux;

	if ($val_actif >0){
		$checkedId = $dao -> CheckExistActif('taux', 'ID_TAUX', 'NOM_TAUX ='.$data -> nomTaux);		
		ResetActifById($dao, $checkedId, 0);
	}
	
	$SoU = $dao -> saveOrUpdateTaux(
		$data -> idTaux,
		$data -> nomTaux,
		$dao -> Montant($data -> valeurPourcent),
		$data -> datePriseEffetTaux,
		$val_actif
	);
	echo $SoU;
}

/*
 * @$checkedId est un tableau de IDs
 * utiliser la méthode 'implode' pour les convertir en string et séparées les valeurs par des virgules
 * Envoyer les ids vers la condition WHERE de la requête qui fait la mise à jour
 */
function ResetActifById($dao, $checkedId, $ActifValue) {
	$result = $dao -> array_flatten($checkedId);

	$mystring = implode(', ', $result);
	$R = $dao ->  ResetActifValue('taux', 'ACTIF_TAUX', $ActifValue, 'ID_TAUX', $mystring);
	return $R;
}

/*
 * Supprimer une ligne dans table "taux" où ID_TAUX = ($data -> idTaux)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'taux',
		'ID_TAUX',
		$data -> idTaux
	);
	echo $SoU;
}
?>
