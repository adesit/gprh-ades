<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;

if ($infoRequest == 2) {
	$data_key = "data";
	$data = $model -> $data_key;
	SaveOrUpdate($m, $data);
} else if ($infoRequest == 4) {
	/*
	 * Cherche si une ligne avec la valeur ACTIF_HS_BASE = 1
	 * Si des lignes existes, il faut les remettre à 0
	 */
	$nom = "natureHsBase";
	$id = "idHsBase";
	$nomTaux = $model -> $nom;
	$idTaux = $model -> $id;
	$checkedId = $m -> CheckExistActif('hs_base', 'ID_HS_BASE', "NATURE_HS_BASE ='$nomTaux' AND ACTIF_HS_BASE = 1");

	ResetActifById($m, $checkedId, 0);

	echo ResetActifById($m, [$idTaux], 1);
} else if ($infoRequest === 3) {
	$data_key = "data";
	$data = $model -> $data_key;
	Delete($m, $data);
}

/*
 * Enregistrer les informations de la Taux d'augmentation
 */
function SaveOrUpdate($dao, $data) {
	$val_actif = $dao -> IsNullOrEmptyString($data -> actifHsBase) > 0 ? 0 : $data -> actifHsBase;

	if ($val_actif >0){
		$checkedId = $dao -> CheckExistActif('hs_base', 'ID_HS_BASE', 'NATURE_HS_BASE ='.$data -> natureHsBase);		
		ResetActifById($dao, $checkedId, 0);
	}
	
	$SoU = $dao -> saveOrUpdateTauxHS(
		$data -> idHsBase,
		$data -> natureHsBase,
		$dao -> Montant($data -> formuleCalcul),
		$data -> dateEffetHs,
		$val_actif
	);
	echo $SoU;
}

/*
 * @$checkedId est un tableau de IDs
 * utiliser la méthode 'implode' pour les convertir en string et séparées les valeurs par des virgules
 * Envoyer les ids vers la condition WHERE de la requête qui fait la mise à jour
 */
function ResetActifById($dao, $checkedId, $ActifValue) {
	$result = $dao -> array_flatten($checkedId);

	$mystring = implode(', ', $result);
	$R = $dao ->  ResetActifValue('hs_base', 'ACTIF_HS_BASE', $ActifValue, 'ID_HS_BASE', $mystring);
	return $R;
}

/*
 * Supprimer une ligne dans table "hs_base" où ID_HS_BASE = ($data -> idHsBase)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'hs_base',
		'ID_HS_BASE',
		$data -> idHsBase
	);
	echo $SoU;
}
?>
