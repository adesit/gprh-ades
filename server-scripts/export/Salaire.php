<?php
$arrayValue = array();
// Load autoloader (using Composer)
require '../vendor/autoload.php';
require_once '../DataAccessObject.php';

//include the classes needed to create and write .xlsx file
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Shared\StringHelper;

try {
	$dao = new DataAccessObject();

	// $infoRequest détermine le genre de requête à faire
	$infoRequest = json_decode($_POST['infoRequest']);
	
	if ($infoRequest == 1) { // Toutes les historiques de salaire
		$typeUtilisateur = json_decode($_POST['typeUtilisateur']);
		$idCentre = json_decode($_POST['idCentre']);
		if ($typeUtilisateur == 1) $where = "WHERE 1";
		else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE c.ID_CENTRE = $idCentre";
	}else if ($infoRequest == 3) { // Toutes les historiques d'un mois de salaire
		$moisSalaire = $_POST['idSalaire'];
		$mois = $_POST['mois'];
		$typeUtilisateur = json_decode($_POST['typeUtilisateur']);
		$idCentre = json_decode($_POST['idCentre']);
		if ($typeUtilisateur == 1) $where = "WHERE h.ID_SALAIRE = $moisSalaire";
		else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE h.ID_SALAIRE = $moisSalaire AND c.ID_CENTRE = $idCentre";
	}

	ini_set('memory_limit', '-1'); // It will take unlimited memory usage of server
	$res=$dao->getHistoriqueSalaire($where);

	//object of the Spreadsheet class to create the excel data
	$spreadsheet = new Spreadsheet();
	$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A1', 'Centre')
		->setCellValue('B1', 'Matricule')
		->setCellValue('C1', 'Nom & prénoms')
		->setCellValue('D1', 'Poste occupé')
		->setCellValue('E1', 'Catégorie')
		->setCellValue('F1', 'Embauché(e) le')
		->setCellValue('G1', 'Prise de poste')
		->setCellValue('H1', 'S.Base année dernière')
		->setCellValue('I1', 'S.Base grille')
		->setCellValue('J1', 'S.Base (5% et 15%)')
		->setCellValue('K1', 'S. Supplémentaire')
		->setCellValue('L1', 'S. brut année dernière')
		->setCellValue('M1', 'Relicat non perçu')
		->setCellValue('N1', 'Prime exceptionnel')
		->setCellValue('O1', 'Rappel sur S.Base')
		->setCellValue('P1', 'Anomalie salaire')
		->setCellValue('Q1', 'Début intérim')
		->setCellValue('R1', 'Fin intérim')
		->setCellValue('S1', 'Salaire interim')
		->setCellValue('T1', 'Salaire par heure')
		->setCellValue('U1', 'Totale heure HS')
		->setCellValue('V1', 'Montant H. Sup')
		->setCellValue('W1', 'SBrut avant maternité')
		->setCellValue('X1', 'Montant H. Minus')
		->setCellValue('Y1', 'Maternité')
		->setCellValue('Z1', 'Déduction maternité')
		->setCellValue('AA1', 'Salaire avant CNaPS')
		->setCellValue('AB1', 'Cnaps base')
		->setCellValue('AC1', 'Cnaps prime')
		->setCellValue('AD1', 'Nom SMIE')
		->setCellValue('AE1', 'Caisse médicale')
		->setCellValue('AF1', 'CNaPS + SMIE')
		->setCellValue('AG1', 'S.brut après CNaPS + SMIE')
		->setCellValue('AH1', 'Brut après CNaPS+IRSA arrondi')
		->setCellValue('AI1', 'IRSA après plafond')
		->setCellValue('AJ1', 'IRSA brut arrondi')
		->setCellValue('AK1', 'Nb enfants')
		->setCellValue('AL1', 'Abattement')
		->setCellValue('AM1', 'IRSA Net')
		->setCellValue('AN1', 'Caisse hospitalière')
		->setCellValue('AO1', 'Rappel sur S.Net')
		->setCellValue('AP1', 'Coût médical')
		->setCellValue('AQ1', 'Participation %')
		->setCellValue('AR1', 'Déduction médicale')
		->setCellValue('AS1', 'Déduction avance')
		->setCellValue('AT1', 'Divers retenus')
		->setCellValue('AU1', 'Dépassement téléphonique')
		->setCellValue('AV1', 'NET à payer')
		->setCellValue('AW1', 'Congé pris')
		->setCellValue('AX1', 'Solde congé')
		->setCellValue('AY1', 'Remarques');

	$count = 1;
	$nbLigne = count($res);
	// Insertion des lignes
	foreach ($res as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae, $af, $ag, $ah, $ai, $aj, $ak, $al, $am, $an, $ao, $ap, $aq, $ar, $as, $at, $au, $av, $aw, $ax, $ay, $az, $ba, $bb, $bc ,$bd, $be, $bf, $bg, $bh, $bi, $bj, $bk, $bl, $bm, $bn, $bo, $bp, $bq, $br, $bs, $bt, $bu, $bv, $bw, $bx, $by, $bz, $ca, $cb, $cc, $cd, $ce, $cf, $cg, $ch, $ci, $cj, $ck, $cl, $cm, $cn, $co, $cp)) {
		$count += 1;
		//add some data in excel cells
		$date_embauche = new DateTime($g);
		$date_prise_poste = new DateTime($h);
		$debut_interim = empty($p)? '' : (new DateTime($p))->format('d/m/Y');
		$fin_interim = empty($q)? '' : (new DateTime($q))->format('d/m/Y');
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$count, $bx)
		->setCellValue('B'.$count, $b)
		->setCellValue('C'.$count, $c.' '.$d)
		->setCellValue('D'.$count, $e)
		->setCellValue('E'.$count, $cm) // $f
		->setCellValue('F'.$count, $date_embauche->format('d/m/Y'))
		->setCellValue('G'.$count, $date_prise_poste->format('d/m/Y'))
		->setCellValue('H'.$count, $i)
		->setCellValue('I'.$count, $j)
		->setCellValue('J'.$count, $k)
		->setCellValue('K'.$count, $l)
		->setCellValue('L'.$count, $bz)
		->setCellValue('M'.$count, $m)
		->setCellValue('N'.$count, $n)
		->setCellValue('O'.$count, $cn)
		->setCellValue('P'.$count, ($o == 0) ? '' : 'Oui' )
		->setCellValue('Q'.$count, $debut_interim)
		->setCellValue('R'.$count, $fin_interim)
		->setCellValue('S'.$count, $r)
		->setCellValue('T'.$count, $s)
		->setCellValue('U'.$count, $by)
		->setCellValue('V'.$count, $t)
		->setCellValue('W'.$count, $u)
		->setCellValue('X'.$count, $v)
		->setCellValue('Y'.$count, $w)
		->setCellValue('Z'.$count, $x)
		->setCellValue('AA'.$count, $y)
		->setCellValue('AB'.$count, $z)
		->setCellValue('AC'.$count, $aa)
		->setCellValue('AD'.$count, $ab)
		->setCellValue('AE'.$count, $ac)
		->setCellValue('AF'.$count, $ad)
		->setCellValue('AG'.$count, $ae)
		->setCellValue('AH'.$count, $af)
		->setCellValue('AI'.$count, $ag)
		->setCellValue('AJ'.$count, $ah)
		->setCellValue('AK'.$count, $ay)
		->setCellValue('AL'.$count, $ai)
		->setCellValue('AM'.$count, $aj)
		->setCellValue('AN'.$count, $ak)
		->setCellValue('AO'.$count, $co)
		->setCellValue('AP'.$count, $al)
		->setCellValue('AQ'.$count, $am)
		->setCellValue('AR'.$count, $an)
		->setCellValue('AS'.$count, $ao)
		->setCellValue('AT'.$count, $ap)
		->setCellValue('AU'.$count, $aq)
		->setCellValue('AV'.$count, $au)
		->setCellValue('AW'.$count, $aw)
		->setCellValue('AX'.$count, $ax)
		->setCellValue('AY'.$count, $cp);
	}
	
	//set style for A1,B1,C1 cells
	$cell_st =[
		'font' =>['bold' => true],
		'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER]
		];
	$spreadsheet->getActiveSheet()
		->getStyle('A1:AV1')
			->applyFromArray($cell_st);

	$spreadsheet->getActiveSheet()
		->getStyle('H2:M'.($nbLigne + 1))
			->getNumberFormat()
    			->setFormatCode('#,##0');
	$spreadsheet->getActiveSheet()
		->getStyle('Q2:AA'.($nbLigne + 1))
			->getNumberFormat()
    			->setFormatCode('#,##0');
	$spreadsheet->getActiveSheet()
		->getStyle('AC2:AT'.($nbLigne + 1))
			->getNumberFormat()
    			->setFormatCode('#,##0');
	
	$spreadsheet->getActiveSheet()->getStyle('A1:AV'.($nbLigne + 1))->getBorders()->applyFromArray(
		[
			'allBorders' => [
				'borderStyle' => Border::BORDER_THIN,
				'color' => [
					'rgb' => '808080'
					]
			]
		]
	);
				
	$spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

	$cols = $spreadsheet->getActiveSheet()->getColumnIterator('A', 'AV');
	while ($cols->valid()) {
		$col = $cols->current();
		$nomCol = $col->getColumnIndex();

		$spreadsheet->getActiveSheet()->getColumnDimension($nomCol)->setAutoSize(true);

		$cols->next();
	}

	$spreadsheet->getActiveSheet()
    ->getActiveCell();
				
	//make object of the Xlsx class to save the excel file
	$filename = 'Résumé du salaire-'.$mois;
	$spreadsheet->getActiveSheet()->setTitle($filename); //set a title for Worksheet

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'. utf8_decode($filename) .'.xlsx"'); /*-- $filename is  xsl filename ---*/
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	header('downloadToken: true');
	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0
	
	$writer = new Xlsx($spreadsheet);
	$writer->save('php://output');
} catch (Exception $e) {
	array_push($arrayValue, array('response' => $e->getMessage()));
	$result['data'] = $arrayValue;
	$result = json_encode($result);
	echo $result;
}
?>