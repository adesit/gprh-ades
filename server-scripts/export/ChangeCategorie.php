<?php
$arrayValue = array();
// Load autoloader (using Composer)
require '../vendor/autoload.php';
require_once '../DataAccessObject.php';
require_once '../formule/AppliquerFormule.php';

//include the classes needed to create and write .xlsx file
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;

try {
	$dao = new DataAccessObject();
	$formule = new AppliquerFormule();

	// $infoRequest détermine le genre de requête à faire
	$infoRequest = json_decode($_POST['infoRequest']);
	
	if ($infoRequest == 1) { // Toutes les historiques de salaire
		$typeUtilisateur = json_decode($_POST['typeUtilisateur']);
		$idCentre = json_decode($_POST['idCentre']);
		if ($typeUtilisateur == 1) $where = "WHERE 1";
		else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE c.ID_CENTRE = $idCentre";
	}else if ($infoRequest == 3) { // Toutes les historiques d'un mois de salaire
		$moisSalaire = $_POST['idSalaire'];
		$mois = $_POST['mois'];
		$typeUtilisateur = json_decode($_POST['typeUtilisateur']);
		$idCentre = json_decode($_POST['idCentre']);
		if ($typeUtilisateur == 1) $where = "WHERE h.ID_SALAIRE = $moisSalaire";
		else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE h.ID_SALAIRE = $moisSalaire AND c.ID_CENTRE = $idCentre";
	}

	ini_set('memory_limit', '-1'); // It will take unlimited memory usage of server
	$res = $dao -> getChangeCategorieListe($where);

	//object of the Spreadsheet class to create the excel data
	$spreadsheet = new Spreadsheet();
	$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A1', 'Matricule')
		->setCellValue('B1', 'Nom & prénoms')
		->setCellValue('C1', 'Ancienneté (Ans)')
		->setCellValue('D1', 'Nouvelle catégorie');

	$count = 1;
	$nbLigne = count($res);
	// Insertion des lignes
	foreach ($res as list($a, $b, $c, $d, $e, $f, $g, $h, $i)) {
		$count += 1;
		
		$where = "WHERE c.ID_CONTRAT = $b           
		AND c.CATEGORIE_ANCIENNETE LIKE '%$g%'";
		$categorieReelleDao = $dao -> getCategorieByPoste($where);
		$categorieReelle = $formule -> recupererCategorieRelle($g, $categorieReelleDao[0][3]);
		
		//add some data in excel cells
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue('A'.$count, $c)
		->setCellValue('B'.$count, $d.' '.$e)
		->setCellValue('C'.$count, $f)
		->setCellValue('D'.$count, $categorieReelle);
	}
	
	//set style
	$cell_st =[
		'font' =>['bold' => true],
		'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER],
		'borders'=>['bottom' =>['style'=> \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM]]
		];
	$spreadsheet->getActiveSheet()->getStyle('A1:D1')->applyFromArray($cell_st);

	$border_style = [
		'allBorders' => [
			'borderStyle' => Border::BORDER_THIN,
			'color' => [
				'rgb' => '808080'
			]
		]
	];
	$spreadsheet->getActiveSheet()->getStyle('A1:D'.($nbLigne + 1))->getBorders()->applyFromArray($border_style);
	$spreadsheet->getActiveSheet()->getStyle('C1:C'.($nbLigne + 1))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

	$cols = $spreadsheet->getActiveSheet()->getColumnIterator('A', 'D');
		while ($cols->valid()) {
			$col = $cols->current();
			$nomCol = $col->getColumnIndex();

			$spreadsheet->getActiveSheet()->getColumnDimension($nomCol)->setAutoSize(true);

			$cols->next();
		}
	
	$filename = 'N.lle catégorie-'.$mois;
	$spreadsheet->getActiveSheet()->setTitle($filename); //set a title for Worksheet

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'. utf8_decode($filename) .'.xlsx"'); /*-- $filename is  xsl filename ---*/
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	header('downloadToken: true');
	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0
	
	$writer = new Xlsx($spreadsheet);
	$writer->save('php://output');
} catch (Exception $e) {
	array_push($arrayValue, array('response' => $e->getMessage()));
	$result['data'] = $arrayValue;
	$result = json_encode($result);
	echo $result;
}
?>