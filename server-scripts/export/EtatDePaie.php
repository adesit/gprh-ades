<?php
$arrayValue = array();
// Load autoloader (using Composer)
require '../vendor/autoload.php';
require_once '../DataAccessObject.php';

//include the classes needed to create and write .xlsx file
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Shared\StringHelper;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

try {
	$dao = new DataAccessObject();

	// $infoRequest détermine le genre de requête à faire
	$infoRequest = json_decode($_POST['infoRequest']);
	
	if ($infoRequest == 3) { // Toutes les historiques d'un mois de salaire
		$moisSalaire = $_POST['idSalaire'];
		$mois = $_POST['mois'];
		$typeUtilisateur = json_decode($_POST['typeUtilisateur']);
		$idCentre = json_decode($_POST['idCentre']);
		if ($typeUtilisateur == 1) $where = "WHERE h.ID_SALAIRE = $moisSalaire";
		else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE h.ID_SALAIRE = $moisSalaire AND c.ID_CENTRE = $idCentre";
	}

	ini_set('memory_limit', '-1'); // It will take unlimited memory usage of server
	$res = $dao -> getHistoriqueSalaire($where);

	//object of the Spreadsheet class to create the excel data
	$spreadsheet = new Spreadsheet();

	//make object of the Xlsx class to save the excel file
	$filename = 'Etat de paie-'.$mois;
	$spreadsheet = spreadSheetData(0, $filename, $mois, $res, $spreadsheet);

	if ($typeUtilisateur == 1) {
		$resByCentre = _group_by($res, '78'); // Groupe les résultats par id_centre
		$i = 1;
		foreach ($resByCentre as $res1) {
			$spreadsheet = spreadSheetData($i, 'Etat-'.$res1[0][75].'-'.$mois, $mois, $res1, $spreadsheet);
			$i ++;
		}
	}

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'. utf8_decode($filename) .'.xlsx"'); /*-- $filename is  xsl filename ---*/
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	header('downloadToken: true');
	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0
	
	$writer = new Xlsx($spreadsheet);
	$writer->save('php://output');
} catch (Exception $e) {
	array_push($arrayValue, array('response' => $e->getMessage()));
	$result['data'] = $arrayValue;
	$result = json_encode($result);
	echo $result;
}

function spreadSheetData($sheetIndex, $filename, $mois, $res, $spreadsheet) {
	// Create a new worksheet called "My Data"
	$myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $filename);

	// Attach the "My Data" worksheet as the first worksheet in the Spreadsheet object
		$spreadsheet->addSheet($myWorkSheet, $sheetIndex);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('E1', 'ETAT DE PAIE');
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('E2', $mois);
		$spreadsheet->getActiveSheet()->getStyle('E1:E2')
			->getFont()
			->setName('Arial')
			->setSize(14)
			->setBold(true);
		$spreadsheet->getDefaultStyle()
			->getFont()
			->setName('Arial')
			->setSize(8);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('I1', 'ASSOCIATION POUR LE DEVELOPPEMENT DE L\'ENERGIE SOLAIRE');
		$spreadsheet->getActiveSheet()->mergeCells('I1:L1');
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('I2', 'ROUTE BETANIMENA');
		$spreadsheet->getActiveSheet()->mergeCells('I2:J2');
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('I3', 'TOLIARA');
		$spreadsheet->getActiveSheet()->mergeCells('I3:J3');
	
	// Les en-têtes des colonnes
		$ligne_entete = 4;
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('A'.$ligne_entete, 'Centre');
		$spreadsheet->getActiveSheet()->mergeCells('A4:A7');
		$spreadsheet->getActiveSheet()->getStyle('A4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('B'.$ligne_entete, 'Calcul centre');
		$spreadsheet->getActiveSheet()->mergeCells('B4:B7');
		$spreadsheet->getActiveSheet()->getStyle('B4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('C'.$ligne_entete, 'N°');
		$spreadsheet->getActiveSheet()->mergeCells('C4:C7');
		$spreadsheet->getActiveSheet()->getStyle('C4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('D'.$ligne_entete, 'Calcul nombre d\'employé');
		$spreadsheet->getActiveSheet()->mergeCells('D4:D7');
		$spreadsheet->getActiveSheet()->getStyle('D4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('E'.$ligne_entete, 'Nom et prénom');
		$spreadsheet->getActiveSheet()->mergeCells('E4:E7');
		$spreadsheet->getActiveSheet()->getStyle('E4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('F'.$ligne_entete, 'Fonction');
		$spreadsheet->getActiveSheet()->mergeCells('F4:F7');
		$spreadsheet->getActiveSheet()->getStyle('F4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('G'.$ligne_entete, 'Date d\'embauche');
		$spreadsheet->getActiveSheet()->mergeCells('G4:G7');
		$spreadsheet->getActiveSheet()->getStyle('G4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('H'.$ligne_entete, 'Date poste actuel');
		$spreadsheet->getActiveSheet()->mergeCells('H4:H7');
		$spreadsheet->getActiveSheet()->getStyle('H4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('I'.$ligne_entete, 'Date débauche');
		$spreadsheet->getActiveSheet()->mergeCells('I4:I7');
		$spreadsheet->getActiveSheet()->getStyle('I4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('J'.$ligne_entete, 'Catégorie');
		$spreadsheet->getActiveSheet()->mergeCells('J4:J7');
		$spreadsheet->getActiveSheet()->getStyle('J4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('K'.$ligne_entete, 'Nombre d\'enfant');
		$spreadsheet->getActiveSheet()->mergeCells('K4:K7');
		$spreadsheet->getActiveSheet()->getStyle('K4')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
	
	// SALAIRE
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('L'.($ligne_entete - 2), 'SALAIRE');
		$spreadsheet->getActiveSheet()->mergeCells('L'.($ligne_entete - 2).':AH'.($ligne_entete - 2));
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete - 2))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete - 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
	
	// Salaire de base
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('L'.($ligne_entete - 1), 'Salaire de base');
		$spreadsheet->getActiveSheet()->mergeCells('L'.($ligne_entete - 1).':L7');
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete - 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
	
	// Rappel
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('M'.($ligne_entete - 1), 'Rappel imposable');
		$spreadsheet->getActiveSheet()->mergeCells('M'.($ligne_entete - 1).':M7');
		$spreadsheet->getActiveSheet()->getStyle('M'.($ligne_entete - 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
	
	// Salaire supplémentaire
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('N'.($ligne_entete - 1), 'Salaire supplémentaire');
		$spreadsheet->getActiveSheet()->mergeCells('N'.($ligne_entete - 1).':AG'.($ligne_entete - 1));
		$spreadsheet->getActiveSheet()->getStyle('N'.($ligne_entete - 1))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('N'.($ligne_entete), 'Prime exceptionnelle');
		$spreadsheet->getActiveSheet()->mergeCells('N'.($ligne_entete).':N7');
		$spreadsheet->getActiveSheet()->getStyle('N'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Salaire intérim
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('O'.($ligne_entete), 'Salaire intérim');
		$spreadsheet->getActiveSheet()->mergeCells('O'.($ligne_entete).':Q'.($ligne_entete));
		$spreadsheet->getActiveSheet()->getStyle('O'.($ligne_entete + 1))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('O'.($ligne_entete + 1), 'Début');
		$spreadsheet->getActiveSheet()->mergeCells('O'.($ligne_entete + 1).':O7');
		$spreadsheet->getActiveSheet()->getStyle('O'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('P'.($ligne_entete + 1), 'Fin');
		$spreadsheet->getActiveSheet()->mergeCells('P'.($ligne_entete + 1).':P7');
		$spreadsheet->getActiveSheet()->getStyle('P'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('Q'.($ligne_entete + 1), 'Montant');
		$spreadsheet->getActiveSheet()->mergeCells('Q'.($ligne_entete + 1).':Q7');
		$spreadsheet->getActiveSheet()->getStyle('Q'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Heures supplémentaires
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('R'.($ligne_entete), 'Heures supplémentaires > 40 heures par semaine');
		$spreadsheet->getActiveSheet()->mergeCells('R'.($ligne_entete).':Z'.($ligne_entete));
		$spreadsheet->getActiveSheet()->getStyle('R'.($ligne_entete))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('R'.($ligne_entete + 1), 'Heures');
		$spreadsheet->getActiveSheet()->mergeCells('R'.($ligne_entete + 1).':R7');
		$spreadsheet->getActiveSheet()->getStyle('R'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('S'.($ligne_entete + 1), 'Salaire base par heure');
		$spreadsheet->getActiveSheet()->mergeCells('S'.($ligne_entete + 1).':S7');
		$spreadsheet->getActiveSheet()->getStyle('S'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('T'.($ligne_entete + 1), '< 8heures');
		$spreadsheet->getActiveSheet()->mergeCells('T'.($ligne_entete + 1).':T7');
		$spreadsheet->getActiveSheet()->getStyle('T'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('U'.($ligne_entete + 1), '> 8heures');
		$spreadsheet->getActiveSheet()->mergeCells('U'.($ligne_entete + 1).':U7');
		$spreadsheet->getActiveSheet()->getStyle('U'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('V'.($ligne_entete + 1), 'Nuit habituelle');
		$spreadsheet->getActiveSheet()->mergeCells('V'.($ligne_entete + 1).':V7');
		$spreadsheet->getActiveSheet()->getStyle('V'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('W'.($ligne_entete + 1), 'Nuit occasionnelle');
		$spreadsheet->getActiveSheet()->mergeCells('w'.($ligne_entete + 1).':W7');
		$spreadsheet->getActiveSheet()->getStyle('w'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('X'.($ligne_entete + 1), 'Dimanche');
		$spreadsheet->getActiveSheet()->mergeCells('X'.($ligne_entete + 1).':X7');
		$spreadsheet->getActiveSheet()->getStyle('X'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('Y'.($ligne_entete + 1), 'Férié');
		$spreadsheet->getActiveSheet()->mergeCells('Y'.($ligne_entete + 1).':Y7');
		$spreadsheet->getActiveSheet()->getStyle('Y'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('Z'.($ligne_entete + 1), 'Montant heures supp');
		$spreadsheet->getActiveSheet()->mergeCells('Z'.($ligne_entete + 1).':Z7');
		$spreadsheet->getActiveSheet()->getStyle('Z'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Réduction heures minus et congé de maternité
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AA'.($ligne_entete), 'Réduction salaire brut');
		$spreadsheet->getActiveSheet()->mergeCells('AA'.($ligne_entete).':AF'.($ligne_entete));
		$spreadsheet->getActiveSheet()->getStyle('AA'.($ligne_entete))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

	// Heures minus
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AA'.($ligne_entete + 1), 'Heures minus');
		$spreadsheet->getActiveSheet()->mergeCells('AA'.($ligne_entete + 1).':AB'.($ligne_entete + 1));
		$spreadsheet->getActiveSheet()->getStyle('AA'.($ligne_entete + 1))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AA'.($ligne_entete + 2), 'Nombre d\'heures');
		$spreadsheet->getActiveSheet()->mergeCells('AA'.($ligne_entete + 2).':AA7');
		$spreadsheet->getActiveSheet()->getStyle('AA'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AB'.($ligne_entete + 2), 'Réduction salaire');
		$spreadsheet->getActiveSheet()->mergeCells('AB'.($ligne_entete + 2).':AB7');
		$spreadsheet->getActiveSheet()->getStyle('AB'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Congé de maternité
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AC'.($ligne_entete + 1), 'Congé de maternité');
		$spreadsheet->getActiveSheet()->mergeCells('AC'.($ligne_entete + 1).':AF'.($ligne_entete + 1));
		$spreadsheet->getActiveSheet()->getStyle('AC'.($ligne_entete + 1))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AC'.($ligne_entete + 2), 'Début maternité');
		$spreadsheet->getActiveSheet()->mergeCells('AC'.($ligne_entete + 2).':AC7');
		$spreadsheet->getActiveSheet()->getStyle('AC'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AD'.($ligne_entete + 2), 'Fin maternité');
		$spreadsheet->getActiveSheet()->mergeCells('AD'.($ligne_entete + 2).':AD7');
		$spreadsheet->getActiveSheet()->getStyle('AD'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AE'.($ligne_entete + 2), 'Jours du mois en cours');
		$spreadsheet->getActiveSheet()->mergeCells('AE'.($ligne_entete + 2).':AE7');
		$spreadsheet->getActiveSheet()->getStyle('AE'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AF'.($ligne_entete + 2), 'Réduction salaire');
		$spreadsheet->getActiveSheet()->mergeCells('AF'.($ligne_entete + 2).':AF7');
		$spreadsheet->getActiveSheet()->getStyle('AF'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Total salaire supplémentaire
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AG'.($ligne_entete), 'Total salaire supplémentaire');
		$spreadsheet->getActiveSheet()->mergeCells('AG'.($ligne_entete).':AG7');
		$spreadsheet->getActiveSheet()->getStyle('AG'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Salaire BRUT
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AH'.($ligne_entete - 1), 'Salaire brut');
		$spreadsheet->getActiveSheet()->mergeCells('AH'.($ligne_entete - 1).':AH7');
		$spreadsheet->getActiveSheet()->getStyle('AH'.($ligne_entete - 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// SALAIRE
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AI'.($ligne_entete - 1), 'RETENUS');
		$spreadsheet->getActiveSheet()->mergeCells('AI'.($ligne_entete - 1).':BC'.($ligne_entete - 1));
		$spreadsheet->getActiveSheet()->getStyle('AI'.($ligne_entete - 1))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('AI'.($ligne_entete - 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Charges sociales
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AI'.($ligne_entete), 'Chages sociales');
		$spreadsheet->getActiveSheet()->mergeCells('AI'.($ligne_entete).':AL'.($ligne_entete));

		$spreadsheet->getActiveSheet()->getStyle('AC'.($ligne_entete))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AI'.($ligne_entete + 1), 'Prime CNaPS');
		$spreadsheet->getActiveSheet()->mergeCells('AI'.($ligne_entete + 1).':AI7');
		$spreadsheet->getActiveSheet()->getStyle('AI'.($ligne_entete + 1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Caisse médicale
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AJ'.($ligne_entete + 1), 'Caisse médicale');
		$spreadsheet->getActiveSheet()->mergeCells('AJ'.($ligne_entete +1).':AL'.($ligne_entete +1));

		$spreadsheet->getActiveSheet()->getStyle('AJ'.($ligne_entete +1))
			->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AJ'.($ligne_entete + 2), 'Caisse');
		$spreadsheet->getActiveSheet()->mergeCells('AJ'.($ligne_entete + 2).':AJ7');
		$spreadsheet->getActiveSheet()->getStyle('AJ'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AK'.($ligne_entete + 2), '%');
		$spreadsheet->getActiveSheet()->mergeCells('AK'.($ligne_entete + 2).':AK7');
		$spreadsheet->getActiveSheet()->getStyle('AK'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AL'.($ligne_entete + 2), 'Prime');
		$spreadsheet->getActiveSheet()->mergeCells('AL'.($ligne_entete + 2).':AL7');
		$spreadsheet->getActiveSheet()->getStyle('AL'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
	
	// IRSA
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AM'.($ligne_entete), 'IRSA');
		$spreadsheet->getActiveSheet()->mergeCells('AM'.($ligne_entete).':AQ'.($ligne_entete +1));
		$spreadsheet->getActiveSheet()->getStyle('AM'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AM'.($ligne_entete + 2), 'Salaire base IRSA');
		$spreadsheet->getActiveSheet()->mergeCells('AM'.($ligne_entete + 2).':AM7');
		$spreadsheet->getActiveSheet()->getStyle('AM'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AN'.($ligne_entete + 2), 'Salaire base IRSA arrondi');
		$spreadsheet->getActiveSheet()->mergeCells('AN'.($ligne_entete + 2).':AN7');
		$spreadsheet->getActiveSheet()->getStyle('AN'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AO'.($ligne_entete + 2), 'SBase IRSA > 350 000Ar');
		$spreadsheet->getActiveSheet()->mergeCells('AO'.($ligne_entete + 2).':AO7');
		$spreadsheet->getActiveSheet()->getStyle('AO'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AP'.($ligne_entete + 2), 'IRSA Brut');
		$spreadsheet->getActiveSheet()->mergeCells('AP'.($ligne_entete + 2).':AP7');
		$spreadsheet->getActiveSheet()->getStyle('AP'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AQ'.($ligne_entete + 2), 'IRSA Net');
		$spreadsheet->getActiveSheet()->mergeCells('AQ'.($ligne_entete + 2).':AQ7');
		$spreadsheet->getActiveSheet()->getStyle('AQ'.($ligne_entete + 2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Caisse Hopital
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AR'.($ligne_entete), 'Caisse hopital');
		$spreadsheet->getActiveSheet()->mergeCells('AR'.($ligne_entete).':AR7');
		$spreadsheet->getActiveSheet()->getStyle('AR'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Frais médicaux
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AS'.($ligne_entete), 'Frais médicaux');
		$spreadsheet->getActiveSheet()->mergeCells('AS'.($ligne_entete).':AU'.($ligne_entete +1));
		$spreadsheet->getActiveSheet()->getStyle('AS'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AS'.($ligne_entete + 2), 'Frais brut');
		$spreadsheet->getActiveSheet()->mergeCells('AS'.($ligne_entete +2).':AS7');
		$spreadsheet->getActiveSheet()->getStyle('AS'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AT'.($ligne_entete + 2), '%');
		$spreadsheet->getActiveSheet()->mergeCells('AT'.($ligne_entete +2).':AT7');
		$spreadsheet->getActiveSheet()->getStyle('AT'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AU'.($ligne_entete + 2), 'Participation');
		$spreadsheet->getActiveSheet()->mergeCells('AU'.($ligne_entete +2).':AU7');
		$spreadsheet->getActiveSheet()->getStyle('AU'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Avances
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AV'.($ligne_entete), 'Avance');
		$spreadsheet->getActiveSheet()->mergeCells('AV'.($ligne_entete).':AZ'.($ligne_entete +1));
		$spreadsheet->getActiveSheet()->getStyle('AV'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AV'.($ligne_entete + 2), 'Date');
		$spreadsheet->getActiveSheet()->mergeCells('AV'.($ligne_entete +2).':AV7');
		$spreadsheet->getActiveSheet()->getStyle('AV'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AW'.($ligne_entete + 2), 'Montant brut');
		$spreadsheet->getActiveSheet()->mergeCells('AW'.($ligne_entete +2).':AW7');
		$spreadsheet->getActiveSheet()->getStyle('AW'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AX'.($ligne_entete + 2), '1er remboursement');
		$spreadsheet->getActiveSheet()->mergeCells('AX'.($ligne_entete +2).':AX7');
		$spreadsheet->getActiveSheet()->getStyle('AX'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AY'.($ligne_entete + 2), 'Remboursement');
		$spreadsheet->getActiveSheet()->mergeCells('AY'.($ligne_entete +2).':AY7');
		$spreadsheet->getActiveSheet()->getStyle('AY'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('AZ'.($ligne_entete + 2), 'Solde');
		$spreadsheet->getActiveSheet()->mergeCells('AZ'.($ligne_entete +2).':AZ7');
		$spreadsheet->getActiveSheet()->getStyle('AZ'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Dépassement téléphonique
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BA'.($ligne_entete), 'Dépassement téléphonique');
		$spreadsheet->getActiveSheet()->mergeCells('BA'.($ligne_entete).':BA7');
		$spreadsheet->getActiveSheet()->getStyle('BA'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Divers retenus
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BB'.($ligne_entete), 'Divers retenus');
		$spreadsheet->getActiveSheet()->mergeCells('BB'.($ligne_entete).':BC'.($ligne_entete +1));
		$spreadsheet->getActiveSheet()->getStyle('BB'.($ligne_entete))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BB'.($ligne_entete + 2), 'Montant');
		$spreadsheet->getActiveSheet()->mergeCells('BB'.($ligne_entete +2).':BB7');
		$spreadsheet->getActiveSheet()->getStyle('BB'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BC'.($ligne_entete + 2), 'Description divers retenus');
		$spreadsheet->getActiveSheet()->mergeCells('BC'.($ligne_entete +2).':BC7');
		$spreadsheet->getActiveSheet()->getStyle('BC'.($ligne_entete +2))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

	// Rappel sur salaire Net non imposable
		$spreadsheet->setActiveSheetIndex($sheetIndex)
		->setCellValue('BD'.($ligne_entete - 1), 'Rappel non imposable');
		$spreadsheet->getActiveSheet()->mergeCells('BD'.($ligne_entete -1).':BD7');
		$spreadsheet->getActiveSheet()->getStyle('BD'.($ligne_entete -1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		$spreadsheet->getActiveSheet()->getStyle('BD'.($ligne_entete -1))
		->getAlignment()->setWrapText(true);

	// Net à payer
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BE'.($ligne_entete - 1), 'NET A PAYER');
		$spreadsheet->getActiveSheet()->mergeCells('BE'.($ligne_entete -1).':BE7');
		$spreadsheet->getActiveSheet()->getStyle('BE'.($ligne_entete -1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
	
	// Congé
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BF'.($ligne_entete +1), 'CONGE');
		$spreadsheet->getActiveSheet()->mergeCells('BF'.($ligne_entete +1).':BH'.($ligne_entete +2));
		$spreadsheet->getActiveSheet()->getStyle('BF'.($ligne_entete +1))
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BF'.($ligne_entete + 3), 'Droit');
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BG'.($ligne_entete + 3), 'Pris');
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('BH'.($ligne_entete + 3), 'Solde');

	// Les lignes
		$count = $debut_ligne = 8;
		$index = 1;
		$nbLigne = count($res) + $count - 1;
		// Insertion des lignes
		foreach ($res as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae, $af, $ag, $ah, $ai, $aj, $ak, $al, $am, $an, $ao, $ap, $aq, $ar, $as, $at, $au, $av, $aw, $ax, $ay, $az, $ba, $bb, $bc ,$bd, $be, $bf, $bg, $bh, $bi, $bj, $bk, $bl, $bm, $bn, $bo, $bp, $bq, $br, $bs, $bt, $bu, $bv, $bw, $bx, $by, $bz, $ca, $cb, $cc, $cd, $ce, $cf, $cg, $ch, $ci, $cj, $ck, $cl, $cm, $cn, $co, $cp)) {
			$count += 1;
			//add some data in excel cells
			$date_embauche = new DateTime($g);
			$date_prise_poste = new DateTime($h);
			$debut_interim = empty($p) ? '' : new DateTime($p);
			$fin_interim = empty ($q) ? '' : new DateTime($q);
			$heure_minus = ($v > 0)? ($v / $s) : '';
			$debut_maternite = empty ($bl) ? '' : new DateTime($bl);
			$fin_maternite = empty ($bm) ? '' : new DateTime($bm);
			$demande_avance = empty($cj) ? '' : new DateTime($cj);
			$premier_remboursement = empty($ck) ? '' : new DateTime($ck);
			$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('A'.$count, $bx)
			->setCellValue('B'.$count, $ca)
			->setCellValue('C'.$count, $index)
			->setCellValue('E'.$count, $c.' '.$d)
			->setCellValue('F'.$count, $e)
			->setCellValue('G'.$count, $date_embauche->format('d/m/Y'))
			->setCellValue('H'.$count, $date_prise_poste->format('d/m/Y'))
			->setCellValue('J'.$count, $cm) // $f
			->setCellValue('K'.$count, $ay)
			->setCellValue('L'.$count, $k)
			->setCellValue('M'.$count, $cn)
			->setCellValue('N'.$count, $n)
			->setCellValue('O'.$count, empty($debut_interim) ? '' : $debut_interim->format('d/m/Y'))
			->setCellValue('P'.$count, empty($fin_interim) ? '' : $fin_interim->format('d/m/Y'))
			->setCellValue('Q'.$count, $r)
			->setCellValue('R'.$count, $by)
			->setCellValue('S'.$count, $s)
			->setCellValue('T'.$count, $cb)
			->setCellValue('U'.$count, $cc)
			->setCellValue('V'.$count, $cd)
			->setCellValue('w'.$count, $ce)
			->setCellValue('X'.$count, $cf)
			->setCellValue('Y'.$count, $cg)
			->setCellValue('Z'.$count, $t)
			->setCellValue('AA'.$count, $heure_minus)
			->setCellValue('AB'.$count, $v)
			->setCellValue('AC'.$count, empty($debut_maternite) ? '' : $debut_maternite->format('d/m/Y'))
			->setCellValue('AD'.$count, empty($fin_maternite) ? '' : $fin_maternite->format('d/m/Y'))
			->setCellValue('AE'.$count, $w)
			->setCellValue('AF'.$count, $x)
			->setCellValue('AG'.$count, ($n + $l + $t + $r))
			->setCellValue('AH'.$count, $y)
			->setCellValue('AI'.$count, $aa)
			->setCellValue('AJ'.$count, $ab)
			->setCellValue('AK'.$count, $ch.'%')
			->setCellValue('AL'.$count, $ac)
			->setCellValue('AM'.$count, $ae)
			->setCellValue('AN'.$count, $af)
			->setCellValue('AO'.$count, $ag)
			->setCellValue('AP'.$count, $ah)
			->setCellValue('AQ'.$count, $aj)
			->setCellValue('AR'.$count, $ak)
			->setCellValue('AS'.$count, $al)
			->setCellValue('AT'.$count, empty($am)? '' : ($am.'%'))
			->setCellValue('AU'.$count, $an)
			->setCellValue('AV'.$count, empty($demande_avance) ? '' : $demande_avance->format('d/m/Y'))
			->setCellValue('AW'.$count, $ci)
			->setCellValue('AX'.$count, empty($premier_remboursement) ? '' : $premier_remboursement->format('d/m/Y'))
			->setCellValue('AY'.$count, $ao)
			->setCellValue('AZ'.$count, ($ci - $cl))
			->setCellValue('BA'.$count, $aq)
			->setCellValue('BB'.$count, $ap)
			->setCellValue('BD'.$count, $co)
			->setCellValue('BE'.$count, $au)
			->setCellValue('BF'.$count, ($aw + $ax))
			->setCellValue('BG'.$count, $aw)
			->setCellValue('BH'.$count, $ax);

			$index ++;
		}
	
	//Mise en forme et Styles
		$cell_st =[
			'font' =>['bold' => true],
			'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER]
			];
		$cell_horizontal_center = [
			'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER]
			];
		$spreadsheet->getActiveSheet()
			->getStyle('A1:BH8')
				->applyFromArray($cell_st);
		$spreadsheet->getActiveSheet()->getStyle('I1:I3')
			->getFont()
			->setBold(false);
		$spreadsheet->getActiveSheet()->getStyle('I1:I3')
			->getAlignment()
			->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

		$spreadsheet->getActiveSheet()
			->getStyle('L'.$debut_ligne.':AI'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode('#,##0');
		$spreadsheet->getActiveSheet()
			->getStyle('AL'.$debut_ligne.':AS'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode('#,##0');
		$spreadsheet->getActiveSheet()
			->getStyle('AW'.$debut_ligne.':BE'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode('#,##0');
		$spreadsheet->getActiveSheet()
			->getStyle('BF'.$debut_ligne.':BH'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode('#,##0.0');

		$spreadsheet->getActiveSheet()
			->getStyle('AK'.$debut_ligne.':AK'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
		$spreadsheet->getActiveSheet()
			->getStyle('AK'.$debut_ligne.':AK'.($nbLigne + 1))
				->getAlignment()
					->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->getActiveSheet()
			->getStyle('AT'.$debut_ligne.':AT'.($nbLigne + 1))
				->getNumberFormat()
					->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
		$spreadsheet->getActiveSheet()
			->getStyle('AT'.$debut_ligne.':AT'.($nbLigne + 1))
				->getAlignment()
					->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

		$spreadsheet->getActiveSheet()->getStyle('A'.$ligne_entete.':K'.$ligne_entete)->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete -1).':M'.($ligne_entete -1))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('N'.($ligne_entete))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('O'.($ligne_entete +1).':Z'.($ligne_entete +1))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('AA'.($ligne_entete +2).':AF'.($ligne_entete +2))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('AG'.($ligne_entete))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('AH'.($ligne_entete -1))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('AM'.($ligne_entete +2).':AQ'.($ligne_entete +2))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('AR'.($ligne_entete))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('AL'.($ligne_entete +2).':AY'.($ligne_entete +2))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('BA'.($ligne_entete))->getAlignment()->setWrapText(true);
		$spreadsheet->getActiveSheet()->getStyle('BB'.($ligne_entete +2).':BC'.($ligne_entete +2))->getAlignment()->setWrapText(true);

		$border_style = [
			'allBorders' => [
				'borderStyle' => Border::BORDER_THIN,
				'color' => [
					'rgb' => '808080'
				]
			]
		];

		$spreadsheet->getActiveSheet()->getStyle('A'.$ligne_entete.':K'.($nbLigne + 1))->getBorders()->applyFromArray($border_style);
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete - 1).':BD'.($nbLigne + 1))->getBorders()->applyFromArray($border_style);
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete - 1).':BE'.($nbLigne + 1))->getBorders()->applyFromArray($border_style);
		$spreadsheet->getActiveSheet()->getStyle('L'.($ligne_entete - 2).':AH'.($ligne_entete - 2))->getBorders()->applyFromArray($border_style);
		$spreadsheet->getActiveSheet()->getStyle('BE'.($ligne_entete + 1).':BH'.($nbLigne + 1))->getBorders()->applyFromArray($border_style);
					
		$cols = $spreadsheet->getActiveSheet()->getColumnIterator('G', 'BH');
		while ($cols->valid()) {
			$col = $cols->current();
			$nomCol = $col->getColumnIndex();

			$spreadsheet->getActiveSheet()->getColumnDimension($nomCol)->setWidth(16);

			$cols->next();
		}

		$cols2 = $spreadsheet->getActiveSheet()->getColumnIterator('E', 'F');
		while ($cols2->valid()) {
			$col2 = $cols2->current();
			$nomCol2 = $col2->getColumnIndex();

			$spreadsheet->getActiveSheet()->getColumnDimension($nomCol2)->setAutoSize(true);

			$cols2->next();
		}

		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(8);
		$spreadsheet->getActiveSheet()->getStyle('B'.($debut_ligne).':B'.($nbLigne + 1))->applyFromArray($cell_horizontal_center);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getStyle('C'.($debut_ligne).':C'.($nbLigne + 1))->applyFromArray($cell_horizontal_center);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(11);
		$spreadsheet->getActiveSheet()->getStyle('K'.($debut_ligne).':K'.($nbLigne + 1))->applyFromArray($cell_horizontal_center);

	// Formules de SOUS TOTAUX
		$spreadsheet->getActiveSheet()
			->setCellValue('D'.$debut_ligne, '=SUBTOTAL(2,C'.($debut_ligne + 1).':C'.($nbLigne + 1).')');
		$formulas_column = array(
			array('K', 'N'),
			array('Q', 'AB'),
			array('AE', 'AJ'),
			array('AL', 'AS'),
			array('AU', 'AU'),
			array('AU', 'AU'),
			array('AW', 'AW'),
			array('AY', 'BB'),
			array('BD', 'BH')
		);

		foreach ($formulas_column as list($start, $end)) {
			$cols3 = $spreadsheet->getActiveSheet()->getColumnIterator($start, $end);
			while ($cols3->valid()) {
				$col3 = $cols3->current();
				$nomCol3 = $col3->getColumnIndex();

				$spreadsheet->getActiveSheet()
				->setCellValue($nomCol3.$debut_ligne, '=SUBTOTAL(9,'.$nomCol3.($debut_ligne + 1).':'.$nomCol3.($nbLigne + 1).')');
				$spreadsheet->getActiveSheet()->getStyle($nomCol3.$debut_ligne)->getFill()
				->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
				->getStartColor()->setARGB('DCDCDC');

				$cols3->next();
			}
		}	
	
		$spreadsheet->getActiveSheet()->setTitle($filename); //set a title for Worksheet

		return $spreadsheet;
}

function _group_by($array, $key) {
    $return = array();
    foreach($array as $val) {
        $return[$val[$key]][] = $val;
    }
    return $return;
}

?>