<?php
$arrayValue = array();
// Load autoloader (using Composer)
require '../vendor/autoload.php';
require_once '../DataAccessObject.php';
require_once '../formule/AppliquerFormule.php';

//include the classes needed to create and write .xlsx file
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Shared\StringHelper;
use PhpOffice\PhpSpreadsheet\Calculation\MathTrig;

try {
	$dao = new DataAccessObject();

	// $infoRequest détermine le genre de requête à faire
	$infoRequest = json_decode($_POST['infoRequest']);
	$date_du_backup = new Datetime('now');
	
	if ($infoRequest == 3) { // Toutes les historiques d'un mois de salaire
		$typeUtilisateur = json_decode($_POST['typeUtilisateur']);
		$idCentre = json_decode($_POST['idCentre']);
		if ($typeUtilisateur == 1) $where = "WHERE c.ACTIF_CONTRAT = 1";
		else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE c.ID_CENTRE = $idCentre AND c.ACTIF_CONTRAT = 1";
	}

	ini_set('memory_limit', '-1'); // It will take unlimited memory usage of server
	$res = $dao -> getContratActuelList($where);

	//object of the Spreadsheet class to create the excel data
	$spreadsheet = new Spreadsheet();

	//make object of the Xlsx class to save the excel file
	$filename = 'Liste du personnel-'.$date_du_backup->format('m-Y');
	$spreadsheet = spreadSheetData(0, $filename, $res, $spreadsheet);

	if ($typeUtilisateur == 1) {
		$resByCentre = _group_by($res, '6'); // Groupe les résultats par id_centre
		$i = 1;
		foreach ($resByCentre as $res1) {
			$spreadsheet = spreadSheetData($i, 'Liste-'.$res1[0][0], $res1, $spreadsheet);
			$i ++;
		}
	}

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'. utf8_decode($filename) .'.xlsx"'); /*-- $filename is  xsl filename ---*/
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	header('downloadToken: true');
	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0
	
	$writer = new Xlsx($spreadsheet);
	$writer->save('php://output');
} catch (Exception $e) {
	array_push($arrayValue, array('response' => $e->getMessage()));
	$result['data'] = $arrayValue;
	$result = json_encode($result);
	echo $result;
}

function spreadSheetData($sheetIndex, $filename, $res, $spreadsheet) {
	// Create a new worksheet called "My Data"
	$myWorkSheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($spreadsheet, $filename);

	// Attach the "My Data" worksheet as the first worksheet in the Spreadsheet object
		$spreadsheet->addSheet($myWorkSheet, $sheetIndex);

		$spreadsheet->getDefaultStyle()
			->getFont()
			->setName('Arial')
			->setSize(8);

	// Les en-têtes des colonnes
		$ligne_entete = 1;
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('A'.$ligne_entete, 'Centre');
		$spreadsheet->getActiveSheet()->getStyle('A1')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('B'.$ligne_entete, 'Nom');
		$spreadsheet->getActiveSheet()->getStyle('B1')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('C'.$ligne_entete, 'Prénom');
		$spreadsheet->getActiveSheet()->getStyle('C1')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('D'.$ligne_entete, 'Sexe');
		$spreadsheet->getActiveSheet()->getStyle('D1')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('E'.$ligne_entete, 'Titre de poste');
		$spreadsheet->getActiveSheet()->getStyle('E1')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
			
		$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('F'.$ligne_entete, 'Catégorie professionnelle');
		$spreadsheet->getActiveSheet()->getStyle('F1')
			->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		
	// Les lignes
		$count = $debut_ligne = 1;
		$index = 1;
		$nbLigne = count($res) + $count - 1;
		$formule = new AppliquerFormule();
		// Insertion des lignes
		foreach ($res as list($a, $b, $c, $d, $e, $f, $g, $h)) {
			$categorie_reelle = $formule -> recupererCategorieRelle($h, $f);
			$count += 1;
			//add some data in excel cells
			$spreadsheet->setActiveSheetIndex($sheetIndex)
			->setCellValue('A'.$count, $a)
			->setCellValue('B'.$count, $b)
			->setCellValue('C'.$count, $c)
			->setCellValue('D'.$count, $d)
			->setCellValue('E'.$count, $e)
			->setCellValue('F'.$count, $categorie_reelle);

			$index ++;
		}
	
	//Mise en forme et Styles
		$cell_st =[
			'font' =>['bold' => true],
			'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER]
			];
		$cell_horizontal_center = [
			'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER]
			];

		$spreadsheet->getActiveSheet()
			->getStyle('A1:F1')
				->applyFromArray($cell_st);

		$border_style = [
			'allBorders' => [
				'borderStyle' => Border::BORDER_THIN,
				'color' => [
					'rgb' => '808080'
				]
			]
		];

		$cols2 = $spreadsheet->getActiveSheet()->getColumnIterator('A', 'F');
		while ($cols2->valid()) {
			$col2 = $cols2->current();
			$nomCol2 = $col2->getColumnIndex();

			$spreadsheet->getActiveSheet()->getColumnDimension($nomCol2)->setAutoSize(true);

			$cols2->next();
		}
		$spreadsheet->getActiveSheet()->getStyle('A'.$ligne_entete.':F'.($nbLigne + 1))->getBorders()->applyFromArray($border_style);
	
		$spreadsheet->getActiveSheet()->setTitle($filename); //set a title for Worksheet

		return $spreadsheet;
}

function _group_by($array, $key) {
    $return = array();
    foreach($array as $val) {
        $return[$val[$key]][] = $val;
    }
    return $return;
}

?>