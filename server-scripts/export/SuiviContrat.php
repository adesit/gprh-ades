<?php
$arrayValue = array();
// Load autoloader (using Composer)
require '../vendor/autoload.php';

//include the classes needed to create and write .xlsx file
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;

try {	
	ini_set('memory_limit', '-1'); // It will take unlimited memory usage of server
	$headersData = json_decode($_REQUEST['headersText']);
	$datas = $_REQUEST['datas'];
	$filtre = $_REQUEST['filtre'];

	//object of the Spreadsheet class to create the excel data
	$spreadsheet = new Spreadsheet();

	// STYLES GENERALES
	// Tous les borders
	$border_style = [
		'allBorders' => [
			'borderStyle' => Border::BORDER_THIN,
			'color' => [
				'rgb' => '808080'
			]
		]
	];
	// En-tête alignée au centre et en gras
	$cell_st =[
		'font' =>['bold' => true],
		'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER]
		];

	/*
	 * Les EN-TETES
	 */
	// On n'affiche pas les en-tête cachées (hidden = false)
	foreach($headersData as $key => $value){
		if($value -> hidden === true){
		unset($headersData[$key]);
		}
	}
	$compteurColumn = count($headersData);
	$compteurCol = 0;
	$colName = array();
	
	for( $x = "A"; $x !== num2alpha($compteurColumn) ; $x++) {
		// echo $x."\n";
		array_push($colName, $x);
	}

	foreach($headersData as $key => $value){
		$spreadsheet->setActiveSheetIndex(0)
		->setCellValue($colName[$compteurCol].'1', $value -> text);
		$spreadsheet->getActiveSheet()->getStyle($colName[$compteurCol].'1')->getBorders()->applyFromArray($border_style);
		$spreadsheet->getActiveSheet()->getStyle($colName[$compteurCol].'1')->applyFromArray($cell_st);
		cellColor($spreadsheet, $colName[$compteurCol].'1', 'C0C0C0');
		$compteurCol += 1;
	}
	/*
	 * Les LIGNES
	 */
	$datas = json_decode($datas);
	$nbLignes = count($datas);
	$compteurCol = 0;
	foreach($headersData as $key => $value) {
		$compteurLigne = 2;
		foreach($datas as $valeur) { // TO DO
			$cle = $value -> dataIndex;
			if ($cle === 'evaluationUnAn') {
				$spreadsheet->setActiveSheetIndex(0)->setCellValue($colName[$compteurCol].$compteurLigne, date("d/m/Y", strtotime($valeur -> $cle)));
				$fondCellule = str_replace("#", "", $valeur -> evaluationRendererUn);
				cellColor($spreadsheet, $colName[$compteurCol].$compteurLigne, $fondCellule);
			} else if ($cle === 'evaluationDeuxAn') {
				$spreadsheet->setActiveSheetIndex(0)->setCellValue($colName[$compteurCol].$compteurLigne, date("d/m/Y", strtotime($valeur -> $cle)));
				$fondCellule = isset($valeur -> evaluationRendererDeux) ? str_replace("#", "", $valeur -> evaluationRendererDeux) : 'FFFFFF';
				cellColor($spreadsheet, $colName[$compteurCol].$compteurLigne, $fondCellule);
			} else if ($cle === 'anciennetePlusCinqAn') {
				$spreadsheet->setActiveSheetIndex(0)->setCellValue($colName[$compteurCol].$compteurLigne, $valeur -> $cle);
				$fondCellule = str_replace("#", "", $valeur -> ancienneteRenderer);
				cellColor($spreadsheet, $colName[$compteurCol].$compteurLigne, $fondCellule);
			}  else if ($cle === 'anniversaire') {
				$spreadsheet->setActiveSheetIndex(0)->setCellValue($colName[$compteurCol].$compteurLigne, $valeur -> $cle);
				$fondCellule = str_replace("#", "",fromRGB($valeur -> anniversaireRenderer));
				$spreadsheet->getActiveSheet()->getStyle($colName[$compteurCol].$compteurLigne)->applyFromArray(array(
					'font'  => array(
						'color' => array('rgb' => 'FFFFFF')
					)));
				cellColor($spreadsheet, $colName[$compteurCol].$compteurLigne, $fondCellule);
			} else {
				$spreadsheet->setActiveSheetIndex(0)->setCellValue($colName[$compteurCol].$compteurLigne, $valeur -> $cle);
			}
			$spreadsheet->getActiveSheet()->getStyle($colName[$compteurCol].$compteurLigne)->getBorders()->applyFromArray($border_style);
			$compteurLigne += 1;
		}
		$compteurCol += 1;
	}	

	// All column Width set to Auto size
	$cols = $spreadsheet->getActiveSheet()->getColumnIterator('A', num2alpha($compteurColumn));
		while ($cols->valid()) {
			$col = $cols->current();
			$nomCol = $col->getColumnIndex();

			$spreadsheet->getActiveSheet()->getColumnDimension($nomCol)->setAutoSize(true);

			$cols->next();
	}
	
	$filename = $filtre;
	$spreadsheet->getActiveSheet()->setTitle($filename); //set a title for Worksheet

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'. utf8_decode($filename) .'.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	header('downloadToken: true');
	// If you're serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0
	
	$writer = new Xlsx($spreadsheet);
	$writer->save('php://output');
} catch (Exception $e) {
	array_push($arrayValue, array('response' => $e->getMessage()));
	$result['data'] = $arrayValue;
	$result = json_encode($result);
	echo $result;
}

function cellColor($spreadsheet, $cells, $color){
   $spreadsheet->getActiveSheet()->getStyle($cells)
    ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
	$spreadsheet->getActiveSheet()->getStyle($cells)
		->getFill()->getStartColor()->setARGB($color);
}

function num2alpha($n)
{
    for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
        $r = chr($n%26 + 0x41) . $r;
    return $r;
}

function fromRGB($sCSSString)
{
	if( preg_match( '!\(([^\)]+)\)!', $sCSSString, $match ) )
	$text = $match[1];
	list($R, $G, $B) = explode(',', $text);
	
    $R = dechex($R);
    if (strlen($R)<2)
    $R = '0'.$R;

    $G = dechex($G);
    if (strlen($G)<2)
    $G = '0'.$G;

    $B = dechex($B);
    if (strlen($B)<2)
    $B = '0'.$B;

    return '#' . $R . $G . $B;
}

?>