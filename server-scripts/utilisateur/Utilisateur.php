<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

if (isset($_POST['model'])) {
	$model = json_decode($_POST['model']);
	$key = "infoRequest";
	$infoRequest = $model -> $key;
	$data_key = "data";
	$data = $model -> $data_key;
} else {
	$infoRequest = json_decode($_POST['infoRequest']);
}

if ($infoRequest == 2) {
	$fileName;
	if(isset($_FILES)){
		$name = $_FILES['avatar']['name'];
		$tmp_name =  $_FILES['avatar']['tmp_name'];
		$location = __DIR__.'/../../resources/images/user-profile/';
		sleep(rand(1,5));
		$random = time()."-".rand(1000, 9999)."-".$name;
		$new_name = $location.$random;
		if (move_uploaded_file($tmp_name, $new_name)){
			$fileName = $random;
		}
		else{
			$fileName = '';
		}
	} else {
		$fileName = '';
	}
	SaveOrUpdate($m, $fileName);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer les informations d'authentification d'un utilisateur
 */
function SaveOrUpdate($dao, $avatar_file_name) {
	$majMotDePasse = (empty($_POST['idUtilisateur'])) ? 0 : 1 ; // si c'est un nouveau utilisateur, il devra réinitialiser son mot de passe à la prochaine connexion, si non c'est déjà un mot de passe valide
	$SoU = $dao -> saveOrUpdateUtilisateur(
		$_POST['idUtilisateur'],
		$_POST['nomUtilisateur'],
		$_POST['motDePasse'],
		$_POST['centreAffectation'],
		$_POST['typeUtilisateur'],
		$majMotDePasse,
		$avatar_file_name
	);
	echo '{"success": true, "msg": "'.$SoU.'"}';
}

/*
 * Supprimer une ligne dans table "utilisateur" où id_utilisateur = ($id_utilisateur)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'utilisateur',
		'id_utilisateur',
		$data -> idUtilisateur
	);
	echo $SoU;
}
?>
