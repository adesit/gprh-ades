<?php
/**
 * $grievToken='--X85s93QSlmqpoe236iA98DgserFmpclal--';
 * $grievAttach='grievance-attachments/';
 * $host='http://www./';
*/
require_once '../dumper.php';

class DataAccessObject
{
	private $_query;
	private $_mysqli;
	private $_usersession = "../usersession";
	private $_host = 'localhost';
	private $_root = 'root';
	private $_password = 'Mysql@2019$';
	private $_database = 'gprhades_prod';

	function __construct()
	{
		$this -> _mysqli = new mysqli($this -> _host, $this -> _root, $this -> _password, $this -> _database) or die('DB server could not be reached');
	}

	// Methode to escape special characters into database
	function specialCharacters($str) {
		return mysqli_real_escape_string($this -> _mysqli, $str);
	}

	// Method to select data from database. This will return an array of records.
	function select()
	{
		$r = mysqli_query($this -> _mysqli, $this -> _query) or die(mysqli_error($this -> _mysqli));
		$res = array();

		while($l = mysqli_fetch_row($r))
		{
			// echo json_encode($l);
			array_push($res, $l);

		}

		return $res;
	}

	/*
	 * Method to update row in database. This will return the updated row_id or the last inserted record.
	 * @param $id the row_id (e.g: ID_CNAPS)
	 */
	function update($id)
	{
		$arr = explode(',', $id);
		mysqli_query($this -> _mysqli, $this -> _query) or die('{"error":"'.mysqli_error($this -> _mysqli).'"}');
		return (($this -> IsNullOrEmptyString($id) > 0) ? $this -> lastInsertId()[0][0] : array_sum($arr));
		
	}

	// Method to update multiple rows in database. This will return the number of all update records.
	function updates()
	{
		return  mysqli_query($this -> _mysqli, $this -> _query) or die('{"error":"'.mysqli_error($this -> _mysqli).'"}');
	}

	// Method to execute multiple queries and return false when error on the 1st query
	function multipleQuery() {
		$r = mysqli_multi_query($this -> _mysqli, $this -> _query) or die('{"error":"'.mysqli_error($this -> _mysqli).'"}');
		$res = array();
		if ($r) {
			do {
				/* Stockage du premier résultat */
				if ($result = mysqli_store_result($this -> _mysqli)) {
					while ($row = mysqli_fetch_row($result)) {
						// printf("%s\n", $row[0]);
						array_push($res, $row);
					}
					mysqli_free_result($result);
				}
			} while (mysqli_more_results($this -> _mysqli) && mysqli_next_result($this -> _mysqli));
		}
		return $res;
	}

	// Function to close database connexion
	function close()
	{
		$this -> _mysqli -> close();
	}
	
	// Method to get last insert id
	function lastInsertId()
	{
		$this -> _query = "SELECT LAST_INSERT_ID()";

		return $this -> select();
	}

	// Function for basic field validation (present and neither empty nor only white space
	function IsNullOrEmptyString($str){
		return (!isset($str) || trim($str) === '');
	}

	/* Function for checking all actif parameters value if their exist, 
	 * they will be removed before inserting new one
	 * @param {$table} table name
	 * @param {$field} actif field name to check if it has 0 or 1 value
	 * @param {$conditions} WHERE CLAUSE to detect the actif field
	 * @return {object} the field value in an array where the row has the condition
	 */
	function CheckExistActif($table, $field, $conditions) {
		$this -> _query = "SELECT $field FROM $table WHERE $conditions";
		
		return $this -> select();
	}

	// Method to format number. It returns a number with space as thousand separator
	function Montant($str) {
		return str_replace(' ', '', $str);
	}

	/*
	 * Method to set actif value in a specified table
	 * @param $table the table name (e.g: cnaps)
	 * @param $actifFieldName the column name (e.g: actif_cnaps)
	 * @param $actifFieldValue it will be 0 or 1 with 1 means that line is the current value in use
	 * @param $fieldIdName in the WHERE clause to find the raw (e.g ID_CNAPS)
	 * @param $IdsValue all row ids when the actif field will be changed
	 */ 
	function ResetActifValue($table, $actifFieldName, $actifFieldValue, $fieldIdName, $IdsValue) {
		if (empty($IdsValue)) return 0;
		else {
			$this -> _query = "UPDATE $table SET $actifFieldName = $actifFieldValue where $fieldIdName IN ($IdsValue)";
			
			return $this -> update($IdsValue);
		}
	}

	/*
	 * Method to delete row by its ID
	 * @param $table the table name (e.g: cnaps)
	 * @param $whereField the field name containing the table primary key (e.g: ID_CNAPS)
	 * @param $whereValues the id value
	 */
	function DeleteById($table, $whereField, $whereValues) {
		$this -> _query = "DELETE FROM $table 
		WHERE $whereField IN ($whereValues)";
		
		return $this -> updates();
	}
	/*
	 * Aplatir un tablea multidimension et de récupérer les valeurs dans un seul tableau
	 */
	function array_flatten($array) {

		$return = array();
		foreach ($array as $key => $value) {
			if (is_array($value)){ $return = array_merge($return, $this -> array_flatten($value));}
			else {$return[$key] = $value;}
		}
		return $return;
	 
	 }
	/************************************************************************************************/
	function getCentreList()
	{
		$this -> _query = "SELECT
			ID_CENTRE,
			ABREVIATION_CENTRE,
			NOM_CENTRE,
			ADRESSE_CENTRE
		FROM
			centre
		ORDER BY
			NOM_CENTRE
		ASC";
		return $this -> select();
	}

	function verifLogin($nomUtilisateur, $motDePasse, $centreAffectation)
	{
		$this -> _query = "SELECT
			id_utilisateur,
			nom_utilisateur,
			mot_de_passe,
			centre_affectation,
			type_utilisateur,
			maj_mot_de_passe,
			avatar
		FROM
			utilisateur
		WHERE
			nom_utilisateur = '$nomUtilisateur' AND mot_de_passe = PASSWORD('$motDePasse') AND centre_affectation = '$centreAffectation'";
		
		return $this -> select();
	}

	function getAdesInformations() {
		$this -> _query = "SELECT
			ID_ADES,
			RAISON_SOCIALE,
			ADRESSE_COMPLETE,
			NIF,
			STAT,
			TELEPHONE,
			DESCRIPTION_ADES,
			SITE_WEB
		FROM
			ades
		WHERE
			1";

		return $this -> select(); 
	}

	function countEmployes() {
		$this -> _query = "SELECT
			COUNT(*)
		FROM
			personnel
		WHERE
			1";

		return $this -> select(); 
	}

	function countCentres() {
		$this -> _query = "SELECT
			COUNT(*)
		FROM
			centre
		WHERE
			1";

		return $this -> select();
	}

	function saveOrUpdateAdes($ID_ADES, $RAISON_SOCIALE, $ADRESSE_COMPLETE, $NIF, $STAT, $TELEPHONE, $DESCRIPTION_ADES, $SITE_WEB) {
		$DESCRIPTION_ADES = $this -> specialCharacters($DESCRIPTION_ADES);		
		$RAISON_SOCIALE = $this -> specialCharacters($RAISON_SOCIALE);
		$ADRESSE_COMPLETE = $this -> specialCharacters($ADRESSE_COMPLETE);

		if ($this -> IsNullOrEmptyString($ID_ADES) > 0) {
			// ID_ADES is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO ades(
				RAISON_SOCIALE,
				ADRESSE_COMPLETE,
				NIF,
				STAT,
				TELEPHONE,
				DESCRIPTION_ADES,
				SITE_WEB
			)
			VALUES(
				'$RAISON_SOCIALE',
				'$ADRESSE_COMPLETE',
				'$NIF',
				'$STAT',
				'$TELEPHONE',
				'$DESCRIPTION_ADES',
				'$SITE_WEB'
			)";
		} else {
			// UPDATE WHERE ID_ADES is
			$this -> _query = "UPDATE
				ades
			SET
				RAISON_SOCIALE = '$RAISON_SOCIALE',
				ADRESSE_COMPLETE = '$ADRESSE_COMPLETE',
				NIF = '$NIF',
				STAT = '$STAT',
				TELEPHONE = '$TELEPHONE',
				DESCRIPTION_ADES = '$DESCRIPTION_ADES',
				SITE_WEB = '$SITE_WEB'
			WHERE
				ID_ADES = '$ID_ADES'";
		}

		return $this -> update($ID_ADES);
	}

	function getDepartementList() {
		$this -> _query = "SELECT
			ID_DEPARTEMENT,
			NOM_DEPARTEMENT,
			ABREVIATION_DEPARTEMENT
		FROM
			departement
		ORDER BY
			NOM_DEPARTEMENT
		DESC";
		return $this -> select();
	}

	function saveOrUpdateCentre($idCentre, $nomCentre, $abreviationCentre, $adresseCentre) {
		$nomCentre = $this -> specialCharacters($nomCentre);
		$abreviationCentre = $this -> specialCharacters($abreviationCentre);
		$adresseCentre = $this -> specialCharacters($adresseCentre);
		if ($this -> IsNullOrEmptyString($idCentre) > 0) {
			// ID_CENTRE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO centre(
				NOM_CENTRE,
				ABREVIATION_CENTRE,
				ADRESSE_CENTRE
			)
			VALUES(
				'$nomCentre',
				'$abreviationCentre',
				'$adresseCentre'
			)";
		} else {
			// UPDATE WHERE ID_CENTRE is
			$this -> _query = "UPDATE
				centre
			SET
				NOM_CENTRE = '$nomCentre',
				ABREVIATION_CENTRE = '$abreviationCentre',
				ADRESSE_CENTRE = '$adresseCentre'
			WHERE
				ID_CENTRE = '$idCentre'";
		}
		
		return $this -> update($idCentre);
	}

	function saveOrUpdateDepartement($idDepartement, $nomDepartement, $abreviationDepartement) {
		$nomDepartement = $this -> specialCharacters($nomDepartement);
		if ($this -> IsNullOrEmptyString($idDepartement) > 0) {
			// ID_DEPARTEMENT is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO 			departement(
				NOM_DEPARTEMENT,
				ABREVIATION_DEPARTEMENT
			)
			VALUES(
				'$nomDepartement',
				'$abreviationDepartement'
			)";
		} else {
			// UPDATE WHERE ID_DEPARTEMENT is
			$this -> _query = "UPDATE
				departement
			SET
				NOM_DEPARTEMENT = '$nomDepartement',
				ABREVIATION_DEPARTEMENT = '$abreviationDepartement'
			WHERE
				ID_DEPARTEMENT = '$idDepartement'";
		}
		
		return $this -> update($idDepartement);
	}

	function getCnapsList()	{
		$this -> _query = "SELECT
			ID_CNAPS,
			ID_ADES,
			PLAFOND_CNAPS,
			POURCENTAGE_EMPLOYE_CNAPS,
			POURCENTAGE_EMPLOYEUR_CNAPS,
			DATE_PRISE_EFFET_CNAPS,
			ACTIF_CNAPS
		FROM
			cnaps
		WHERE
			1
		ORDER BY
			DATE_PRISE_EFFET_CNAPS ASC";

		return $this -> select();
	}

	function saveOrUpdateCnaps($idCnaps, $idAdes, $plafondCnaps, $pourcentageEmployeCnaps, $pourcentageEmployeurCnaps, $datePriseEffetCnaps, $actifCnaps) {
		if ($this -> IsNullOrEmptyString($idCnaps) > 0) {
			// ID_CNAPS is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO cnaps(
				ID_ADES,
				PLAFOND_CNAPS,
				POURCENTAGE_EMPLOYE_CNAPS,
				POURCENTAGE_EMPLOYEUR_CNAPS,
				DATE_PRISE_EFFET_CNAPS,
				ACTIF_CNAPS
			)
			VALUES(
				$idAdes,
				'$plafondCnaps',
				'$pourcentageEmployeCnaps',
				'$pourcentageEmployeurCnaps',
				'$datePriseEffetCnaps',
				$actifCnaps
			)";
		} else {
			// UPDATE WHERE ID_CNAPS is
			$this -> _query = "UPDATE
				cnaps
			SET
				ID_ADES = $idAdes,
				PLAFOND_CNAPS = '$plafondCnaps',
				POURCENTAGE_EMPLOYE_CNAPS = '$pourcentageEmployeCnaps',
				POURCENTAGE_EMPLOYEUR_CNAPS = '$pourcentageEmployeurCnaps',
				DATE_PRISE_EFFET_CNAPS = '$datePriseEffetCnaps',
				ACTIF_CNAPS = $actifCnaps
			WHERE
				ID_CNAPS = $idCnaps";
		}
		
		return $this -> update($idCnaps);
	}

	function getIrsaList() {
		$this -> _query = "SELECT
			ID_IRSA,
			ID_ADES,
			PLAFOND,
			POURCENTAGE_EMPLOYE_IRSA,
			POURCENTAGE_EMPLOYEUR_IRSA,
			DEDUCTION_ENFANT,
			DATE_PRISE_EFFET_IRSA,
			ACTIF_IRSA
		FROM
			irsa
		WHERE
			1
		ORDER BY
			DATE_PRISE_EFFET_IRSA ASC";

		return $this -> select();
	}

	function saveOrUpdateIrsa($idIrsa, $idAdes, $plafond, $pourcentageEmployeIrsa, $pourcentageEmployeurIrsa, $deductionEnfant, $datePriseEffetIrsa, $actifIrsa) {
		if ($this -> IsNullOrEmptyString($idIrsa) > 0) {
			// ID_IRSA is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO irsa(
				ID_ADES,
				PLAFOND,
				POURCENTAGE_EMPLOYE_IRSA,
				POURCENTAGE_EMPLOYEUR_IRSA,
				DEDUCTION_ENFANT,
				DATE_PRISE_EFFET_IRSA,
				ACTIF_IRSA
			)
			VALUES(
				$idAdes,
				'$plafond',
				'$pourcentageEmployeIrsa',
				'$pourcentageEmployeurIrsa',
				'$deductionEnfant',
				'$datePriseEffetIrsa',
				$actifIrsa
			)";
		} else {
			// UPDATE WHERE ID_IRSA is
			$this -> _query = "UPDATE
				irsa
			SET
				ID_ADES = '$idAdes',
				PLAFOND = '$plafond',
				POURCENTAGE_EMPLOYE_IRSA = '$pourcentageEmployeIrsa',
				POURCENTAGE_EMPLOYEUR_IRSA = '$pourcentageEmployeurIrsa',
				DEDUCTION_ENFANT = '$deductionEnfant',
				DATE_PRISE_EFFET_IRSA = '$datePriseEffetIrsa',
				ACTIF_IRSA = $actifIrsa
			WHERE
				ID_IRSA = '$idIrsa'";
		}
		
		return $this -> update($idIrsa);
	}
	
	function getHopitalList($where)
	{
		$this -> _query = "SELECT
			h.ID_HOPITAL,
			h.ID_ADES,
			h.POURCENTAGE_EMPLOYE_HOPITAL,
			h.POURCENTAGE_EMPLOYEUR_HOPITAL,
			h.DATE_PRISE_EFFET_HOPITAL,
			h.ACTIF_HOPITAL,
			h.ID_CENTRE,
			c.NOM_CENTRE
		FROM
			hopital h
		INNER JOIN centre c 
			ON h.ID_CENTRE = c.ID_CENTRE
		$where
		ORDER BY
			h.DATE_PRISE_EFFET_HOPITAL ASC";

		return $this -> select();
	}

	function saveOrUpdateHopital($idHopital, $idAdes, $pourcentageEmployeHopital, $pourcentageEmployeurHopital, $datePriseEffetHopital, $actifHopital, $idCentre) {
		if ($this -> IsNullOrEmptyString($idHopital) > 0) {
			// ID_HOPITAL is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO hopital(
				ID_ADES,
				POURCENTAGE_EMPLOYE_HOPITAL,
				POURCENTAGE_EMPLOYEUR_HOPITAL,
				DATE_PRISE_EFFET_HOPITAL,
				ACTIF_HOPITAL,
				ID_CENTRE
			)
			VALUES(
				$idAdes,
				'$pourcentageEmployeHopital',
				'$pourcentageEmployeurHopital',
				'$datePriseEffetHopital',
				$actifHopital,
				$idCentre
			)";
		} else {
			// UPDATE WHERE ID_HOPITAL is
			$this -> _query = "UPDATE
				hopital
			SET
				ID_ADES = '$idAdes',
				POURCENTAGE_EMPLOYE_HOPITAL = '$pourcentageEmployeHopital',
				POURCENTAGE_EMPLOYEUR_HOPITAL = '$pourcentageEmployeurHopital',
				DATE_PRISE_EFFET_HOPITAL = '$datePriseEffetHopital',
				ACTIF_HOPITAL = $actifHopital
			WHERE
				ID_HOPITAL = '$idHopital'";
		}
		
		return $this -> update($idHopital);
	}

	function getGroupeList()
	{
		$this -> _query = "SELECT
			ID_GROUPE_CATEGORIE,
			NOM_GROUPE
		FROM
			groupe_categorie
		WHERE
			1
		ORDER BY
			NOM_GROUPE ASC";

		return $this -> select();
	}

	function saveOrUpdateGroupe($idGroupeCategorie, $nomGroupe) {
		$nomGroupe = $this -> specialCharacters($nomGroupe);
		if ($this -> IsNullOrEmptyString($idGroupeCategorie) > 0) {
			// ID_GROUPE_CATEGORIE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO groupe_categorie(NOM_GROUPE)
			VALUES('$nomGroupe')";
		} else {
			// UPDATE WHERE ID_GROUPE_CATEGORIE is
			$this -> _query = "UPDATE
				groupe_categorie
			SET
				NOM_GROUPE = '$nomGroupe'
			WHERE
				ID_GROUPE_CATEGORIE = '$idGroupeCategorie'";
		}
		
		return $this -> update($idGroupeCategorie);
	}

	function getCategorieList()	{
		$this -> _query = "SELECT
		tr.*,
		g.nb_lignes
		FROM
			(
			SELECT
				t.ID_CATEGORIE,
				t.ID_GROUPE_CATEGORIE,
				t.CATEGORIE_PROFESSIONNELLE,
				t.DESCRIPTION_CATEGORIE,
				t.NOM_GROUPE,
				h.nb_actif
			FROM
				(
				SELECT
					c.ID_CATEGORIE,
					c.ID_GROUPE_CATEGORIE,
					c.CATEGORIE_PROFESSIONNELLE,
					c.DESCRIPTION_CATEGORIE,
					g.NOM_GROUPE
				FROM
					categorie c
				JOIN groupe_categorie g ON
					c.ID_GROUPE_CATEGORIE = g.ID_GROUPE_CATEGORIE
				) t
			LEFT JOIN(
				SELECT
					ID_GROUPE_CATEGORIE,
					ID_CATEGORIE,
					ACTIF_LIGNE_GRILLE,
					COUNT(*) AS nb_actif
				FROM
					grille
				WHERE
					ACTIF_LIGNE_GRILLE = 1
				GROUP BY
					ID_GROUPE_CATEGORIE,
					ID_CATEGORIE
			) h
			ON
				t.ID_CATEGORIE = h.ID_CATEGORIE AND t.ID_GROUPE_CATEGORIE = h.ID_GROUPE_CATEGORIE
		) tr
		LEFT JOIN(
			SELECT
				ID_GROUPE_CATEGORIE,
				ID_CATEGORIE,
				ACTIF_LIGNE_GRILLE,
				COUNT(*) AS nb_lignes
			FROM
				grille
			GROUP BY
				ID_GROUPE_CATEGORIE,
				ID_CATEGORIE
		) g
		ON
			tr.ID_CATEGORIE = g.ID_CATEGORIE AND tr.ID_GROUPE_CATEGORIE = g.ID_GROUPE_CATEGORIE
		ORDER BY
			tr.ID_CATEGORIE ASC";

		return $this -> select();
	}

	function saveOrUpdateCategorie($idCategorie, $idGroupeCategorie, $categorieProfessionnelle, $descriptionCategorie) {
		$descriptionCategorie = $this -> specialCharacters($descriptionCategorie);
		if ($this -> IsNullOrEmptyString($idCategorie) > 0) {
			// ID_CATEGORIE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO categorie(
				ID_GROUPE_CATEGORIE,
				CATEGORIE_PROFESSIONNELLE,
				DESCRIPTION_CATEGORIE
			)
			VALUES(
				'$idGroupeCategorie',
				'$categorieProfessionnelle',
				'$descriptionCategorie'
			)";
		} else {
			// UPDATE WHERE ID_CATEGORIE is
			$this -> _query = "UPDATE
				categorie
			SET
				ID_GROUPE_CATEGORIE = '$idGroupeCategorie',
				CATEGORIE_PROFESSIONNELLE = '$categorieProfessionnelle',
				DESCRIPTION_CATEGORIE = '$descriptionCategorie'
			WHERE
				ID_CATEGORIE = '$idCategorie'";
		}
		
		return $this -> update($idCategorie);
	}

	function getSmieList() {
		$this -> _query = "SELECT
			s.ID_SMIE,
			s.NOM_SMIE,
			s.PLAFOND_SMIE,
			s.DEDUCTION_EMPLOYE,
			s.DEDUCTION_EMPLOYEUR,
			s.COUT_EXCEDENTAIRE,
			s.FRANCHISE_COUT_EXCENDENTAIRE,
			s.DATE_PRISE_EFFET_SMIE,
			s.ACTIF_SMIE,
			s.ID_CENTRE,
			c.NOM_CENTRE
		FROM
			smie s
		INNER JOIN centre c 
			ON s.ID_CENTRE = c.ID_CENTRE
		ORDER BY
			c.NOM_CENTRE ASC";

		return $this -> select();
	}

	function saveOrUpdateSmie($idSmie, $nomSmie, $plafondSmie, $deductionEmploye, $deductionEmployeur, $coutExcedentaire, $franchiseCoutExcendentaire, $datePriseEffetSmie, $actifSmie, $idCentre) {
		if ($this -> IsNullOrEmptyString($idSmie) > 0) {
			// ID_SMIE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO smie(
				NOM_SMIE,
				PLAFOND_SMIE,
				DEDUCTION_EMPLOYE,
				DEDUCTION_EMPLOYEUR,
				COUT_EXCEDENTAIRE,
				FRANCHISE_COUT_EXCENDENTAIRE,
				DATE_PRISE_EFFET_SMIE,
				ACTIF_SMIE,
				ID_CENTRE
			)
			VALUES(
				'$nomSmie',
				'$plafondSmie',
				'$deductionEmploye',
				'$deductionEmployeur',
				'$coutExcedentaire',
				'$franchiseCoutExcendentaire',
				'$datePriseEffetSmie',
				$actifSmie,
				$idCentre
			)";
		} else {
			// UPDATE WHERE ID_SMIE is
			$this -> _query = "UPDATE
				smie
			SET
				NOM_SMIE = '$nomSmie',
				PLAFOND_SMIE = '$plafondSmie',
				DEDUCTION_EMPLOYE = '$deductionEmploye',
				DEDUCTION_EMPLOYEUR = '$deductionEmployeur',
				COUT_EXCEDENTAIRE = '$coutExcedentaire',
				FRANCHISE_COUT_EXCENDENTAIRE = '$franchiseCoutExcendentaire',
				DATE_PRISE_EFFET_SMIE = '$datePriseEffetSmie',
				ACTIF_SMIE = $actifSmie,
				ID_CENTRE = $idCentre
			WHERE
				ID_SMIE = $idSmie";
		}
		
		return $this -> update($idSmie);
	}
	
	function getSalaireBaseList($whereActif)	{
		$this -> _query = "SELECT
			s.ID_LIGNE_GRILLE,
			s.ID_GROUPE_CATEGORIE,
			s.ID_CATEGORIE,
			s.SALAIRE_BASE_MINIMUM,
			s.SALAIRE_BASE_3,
			s.SALAIRE_BASE_4,
			s.SALAIRE_BASE_5,
			s.ACTIF_LIGNE_GRILLE,
			s.DATE_PRISE_EFFET_GRILLE,
			f.NOM_GROUPE,
			cat.CATEGORIE_PROFESSIONNELLE
		FROM
			grille s
		JOIN groupe_categorie f ON
			s.ID_GROUPE_CATEGORIE = f.ID_GROUPE_CATEGORIE
		JOIN categorie cat ON
			cat.ID_CATEGORIE = s.ID_CATEGORIE
		$whereActif";
		
		return $this -> select();
	}

	function saveOrUpdateSalaireBase($idLigneGrille, $idGroupe, $idCategorie, $salaireBaseMinimum, $salaireBase3, $salaireBase4, $salaireBase5, $actifLigneGrille, $datePriseEffetGrille) {
		if ($this -> IsNullOrEmptyString($idLigneGrille) > 0) {
			// ID_LIGNE_GRILLE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO grille(
				ID_GROUPE_CATEGORIE,
				ID_CATEGORIE,
				SALAIRE_BASE_MINIMUM,
				SALAIRE_BASE_3,
				SALAIRE_BASE_4,
				SALAIRE_BASE_5,
				ACTIF_LIGNE_GRILLE,
				DATE_PRISE_EFFET_GRILLE
			)
			VALUES(
				'$idGroupe',
				'$idCategorie',
				'$salaireBaseMinimum',
				'$salaireBase3',
				'$salaireBase4',
				'$salaireBase5',
				$actifLigneGrille,
				'$datePriseEffetGrille'
			)";
		} else {
			// UPDATE WHERE ID_LIGNE_GRILLE is
			$this -> _query = "UPDATE
				grille
			SET
				ID_GROUPE_CATEGORIE = '$idGroupe',
				ID_CATEGORIE = '$idCategorie',
				SALAIRE_BASE_MINIMUM = '$salaireBaseMinimum',
				SALAIRE_BASE_3 = '$salaireBase3',
				SALAIRE_BASE_4 = '$salaireBase4',
				SALAIRE_BASE_5 = '$salaireBase5',
				ACTIF_LIGNE_GRILLE = $actifLigneGrille,
				DATE_PRISE_EFFET_GRILLE = '$datePriseEffetGrille'
			WHERE
				ID_LIGNE_GRILLE = $idLigneGrille";
		}
		
		return $this -> update($idLigneGrille);
	}

	function getSectionList() {
		$this -> _query = "SELECT
			ID_SECTION,
			NOM_SECTION
		FROM
			section
		ORDER BY
			NOM_SECTION
		DESC";
		return $this -> select();
	}

	function saveOrUpdateSection($idSection, $nomSection) {
		$nomSection = $this -> specialCharacters($nomSection);
		if ($this -> IsNullOrEmptyString($idSection) > 0) {
			// ID_SECTION is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO 			section(
				NOM_SECTION
			)
			VALUES(
				'$nomSection'
			)";
		} else {
			// UPDATE WHERE ID_SECTION is
			$this -> _query = "UPDATE
				section
			SET
				NOM_SECTION = '$nomSection'
			WHERE
				ID_SECTION = '$idSection'";
		}
		
		return $this -> update($idSection);
	}

	function saveOrUpdatePoste($idPoste, $idGroupe, $idCategorie, $titrePoste, $descriptionPoste) {
		$titrePoste = $this -> specialCharacters($titrePoste);
		$descriptionPoste = $this -> specialCharacters($descriptionPoste);

		if ($this -> IsNullOrEmptyString($idPoste) > 0) {
			// ID_POSTE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO poste(
				ID_GROUPE_CATEGORIE,
				ID_CATEGORIE,
				TITRE_POSTE,
				DESCRIPTION_POSTE
			)
			VALUES(
				$idGroupe,
				$idCategorie,
				'$titrePoste',
				'$descriptionPoste'
			)";
		} else {
			// UPDATE WHERE ID_POSTE is
			$this -> _query = "UPDATE
				poste
			SET
				ID_GROUPE_CATEGORIE = $idGroupe,
				ID_CATEGORIE = $idCategorie,
				TITRE_POSTE = '$titrePoste',
				DESCRIPTION_POSTE = '$descriptionPoste'
			WHERE
				ID_POSTE = $idPoste";
		}
		
		return $this -> update($idPoste);
	}

	function getFormuleList()	{
		$this -> _query = "SELECT
			ID_FORMULE,
			ID_ADES,
			RESULTAT,
			FORMULE_CALCUL,
			DATE_APPLICATION,
			ACTIF_FORMULE,
			DESCRIPTION_FORMULE
		FROM
			formules
		WHERE
			1
		ORDER BY DATE_APPLICATION ASC";
		
		return $this -> select();
	}

	function saveOrUpdateFormule($idFormule, $idAdes, $resultat, $formuleCalcul, $dateApplication, $actifFormule, $descriptionFormule) {
		$resultat = $this -> specialCharacters($resultat);
		$descriptionFormule = $this -> specialCharacters($descriptionFormule);
		$formuleCalcul = $this -> specialCharacters($formuleCalcul);
		
		if ($this -> IsNullOrEmptyString($idFormule) > 0) {
			// ID_FORMULE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO formules(
				ID_ADES,
				RESULTAT,
				FORMULE_CALCUL,
				DATE_APPLICATION,
				ACTIF_FORMULE,
				DESCRIPTION_FORMULE
			 )
			 VALUES(
				$idAdes,
				'$resultat',
				'$formuleCalcul',
				'$dateApplication',
				'$actifFormule',
				'$descriptionFormule'
			 )";
		} else {
			// UPDATE WHERE ID_FORMULE is
			$this -> _query = "UPDATE
			formules
		SET
			ID_ADES = $idAdes,
			RESULTAT = '$resultat',
			FORMULE_CALCUL = '$formuleCalcul',
			DATE_APPLICATION = '$dateApplication',
			ACTIF_FORMULE = '$actifFormule',
			DESCRIPTION_FORMULE = '$descriptionFormule'
		WHERE
			ID_FORMULE = $idFormule";
		}
		
		return $this -> update($idFormule);
	}

	function getPersonnelList($where)	{
		$this -> _query = "SELECT
			p.ID_EMPLOYE,
			p.ID_FOKO,
			p.CIVILITE,
			p.NOM,
			p.PRENOM,
			p.SURNOM,
			p.INITIAL,
			p.SEXE,
			p.DATE_NAISSANCE,
			p.LIEU_NAISSANCE,
			p.NOM_PERE,
			p.NOM_MERE,
			p.NUMERO_CIN_PASSEPORT,
			p.DATE_DELIVRANCE_CIN,
			p.LIEU_DELIVRANCE_CIN_PASSEPORT,
			p.DATE_FIN_VALIDITE,
			p.NUM_CNAPS,
			p.PERMIS_A,
			p.PERMIS_A_P,
			p.PERMIS_B,
			p.PERMIS_C,
			p.PERMIS_D,
			p.PERMIS_E,
			p.PERMIS_F,
			p.TEL_MOBILE,
			p.TEL_FIXE,
			p.EMAIL,
			p.GROUPE_SANGUIN,
			g.nb_contrat,
			g.ID_CENTRE,
            e.nb_enfant,
			g.TITRE_POSTE
		FROM
			personnel p
		LEFT JOIN(
			SELECT
				c.ID_EMPLOYE,
				COUNT(*) AS nb_contrat,
				c.ID_CENTRE,
				c.ID_POSTE,
				pst.TITRE_POSTE
			FROM
				contrat c
			INNER JOIN poste pst ON
				c.ID_POSTE = pst.ID_POSTE
			WHERE
				c.ACTIF_CONTRAT = 1
			GROUP BY
				c.ID_EMPLOYE
		) g
		ON
			p.ID_EMPLOYE = g.ID_EMPLOYE
		LEFT JOIN(
			SELECT
				ID_EMPLOYE,
				COUNT(*) AS nb_enfant
			FROM
				enfant
			GROUP BY
				ID_EMPLOYE
		) e
		ON
			p.ID_EMPLOYE = e.ID_EMPLOYE
		$where
		ORDER BY p.nom, p.prenom ASC";
		
		return $this -> select();
	}

	function getFokoList()
	{
		$this -> _query = "SELECT
			ID_FOKO,
			NOM_FOKO
		FROM
			foko
		WHERE
			1
		ORDER BY
			NOM_FOKO
		ASC";
		return $this -> select();
	}

	function saveOrUpdatePersonnel($idEmploye, $idFoko, $civilite, $nom, $prenom, $surnom, $initial, $sexe, $dateNaissance, $lieuNaissance, $nomPere, $nomMere, $numeroCinPasseport, $dateDelivranceCin, $lieuDelivranceCinPasseport, $dateFinValidite, $numCnaps, $permisA, $permisAP, $permisB, $permisC, $permisD, $permisE, $permisF, $telMobile, $telFixe, $email, $groupeSanguin) {
		$dateFinValidite = $this -> IsNullOrEmptyString($dateFinValidite) > 0 ? "(select null from dual)" : "'".$dateFinValidite."'";
		$nom = $this -> specialCharacters($nom);
		$prenom = $this -> specialCharacters($prenom);
		$surnom = $this -> specialCharacters($surnom);
		$lieuNaissance = $this -> specialCharacters($lieuNaissance);
		$nomPere = $this -> specialCharacters($nomPere);
		$nomMere = $this -> specialCharacters($nomMere);
		$lieuDelivranceCinPasseport = $this -> specialCharacters($lieuDelivranceCinPasseport);

		if ($this -> IsNullOrEmptyString($idEmploye) > 0) {
			// ID_EMPLOYE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO personnel(
				ID_FOKO,
				CIVILITE,
				NOM,
				PRENOM,
				SURNOM,
				INITIAL,
				SEXE,
				DATE_NAISSANCE,
				LIEU_NAISSANCE,
				NOM_PERE,
				NOM_MERE,
				NUMERO_CIN_PASSEPORT,
				DATE_DELIVRANCE_CIN,
				LIEU_DELIVRANCE_CIN_PASSEPORT,
				DATE_FIN_VALIDITE,
				NUM_CNAPS,
				PERMIS_A,
				PERMIS_A_P,
				PERMIS_B,
				PERMIS_C,
				PERMIS_D,
				PERMIS_E,
				PERMIS_F,
				TEL_MOBILE,
				TEL_FIXE,
				EMAIL,
				GROUPE_SANGUIN
			)
			VALUES(
				$idFoko,
				'$civilite',
				'$nom',
				'$prenom',
				'$surnom',
				'$initial',
				'$sexe',
				'$dateNaissance',
				'$lieuNaissance',
				'$nomPere',
				'$nomMere',
				'$numeroCinPasseport',
				'$dateDelivranceCin',
				'$lieuDelivranceCinPasseport',
				".$dateFinValidite.",
				'$numCnaps',
				'$permisA',
				'$permisAP',
				'$permisB',
				'$permisC',
				'$permisD',
				'$permisE',
				'$permisF',
				'$telMobile',
				'$telFixe',
				'$email',
				'$groupeSanguin'
			)";
		} else {
			// UPDATE WHERE ID_EMPLOYE is
			$this -> _query = "UPDATE
			personnel
		SET
			ID_FOKO = $idFoko,
			CIVILITE = '$civilite',
			NOM = '$nom',
			PRENOM = '$prenom',
			SURNOM = '$surnom',
			INITIAL = '$initial',
			SEXE = '$sexe',
			DATE_NAISSANCE = '$dateNaissance',
			LIEU_NAISSANCE = '$lieuNaissance',
			NOM_PERE = '$nomPere',
			NOM_MERE = '$nomMere',
			NUMERO_CIN_PASSEPORT = '$numeroCinPasseport',
			DATE_DELIVRANCE_CIN = '$dateDelivranceCin',
			LIEU_DELIVRANCE_CIN_PASSEPORT = '$lieuDelivranceCinPasseport',
			DATE_FIN_VALIDITE = ".$dateFinValidite.",
			NUM_CNAPS = '$numCnaps',
			PERMIS_A = '$permisA',
			PERMIS_A_P = '$permisAP',
			PERMIS_B = '$permisB',
			PERMIS_C = '$permisC',
			PERMIS_D = '$permisD',
			PERMIS_E = '$permisE',
			PERMIS_F = '$permisF',
			TEL_MOBILE = '$telMobile',
			TEL_FIXE = '$telFixe',
			EMAIL = '$email',
			GROUPE_SANGUIN = '$groupeSanguin'
		WHERE
			ID_EMPLOYE = $idEmploye";
		}
		
		return $this -> update($idEmploye);
	}

	function getConjointList($whereActif)	{
		$this -> _query = "SELECT
			ID_CONJOINT,
			ID_EMPLOYE,
			NOM_CONJOINT,
			PRENOM_CONJOINT,
			DATE_NAISSANCE_CONJOINT,
			GENRE_CONJOINT,
			FONCTION_CONJOINT,
			TEL_CONJOINT
		FROM
			conjoint s
		$whereActif";
		
		return $this -> select();
	}

	function getCorrespondanceList($whereActif)	{
		$this -> _query = "SELECT
			ID_CORRESPONDANT,
			ID_EMPLOYE,
			NOM_PRENOM_CORRESPONDANT,
			TEL_CORRESPONDANT
		FROM
			correspondance s
		$whereActif";
		
		return $this -> select();
	}

	function saveOrUpdateConjoint($idConjoint, $idEmploye, $nomConjoint, $prenomConjoint, $dateNaissanceConjoint, $genreConjoint, $fonctionConjoint, $telConjoint) {
		$nomConjoint = $this -> specialCharacters($nomConjoint);
		$prenomConjoint = $this -> specialCharacters($prenomConjoint);
		$fonctionConjoint = $this -> specialCharacters($fonctionConjoint);

		if ($this -> IsNullOrEmptyString($idConjoint) > 0) {
			// ID_CONJOINT is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO conjoint(
				ID_EMPLOYE,
				NOM_CONJOINT,
				PRENOM_CONJOINT,
				DATE_NAISSANCE_CONJOINT,
				GENRE_CONJOINT,
				FONCTION_CONJOINT,
				TEL_CONJOINT
			)
			VALUES(
				$idEmploye,
				'$nomConjoint',
				'$prenomConjoint',
				'$dateNaissanceConjoint',
				'$genreConjoint',
				'$fonctionConjoint',
				'$telConjoint'
			)";
		} else {
			// UPDATE WHERE ID_CONJOINT is
			$this -> _query = "UPDATE
				conjoint
			SET
				ID_EMPLOYE = $idEmploye,
				NOM_CONJOINT = '$nomConjoint',
				PRENOM_CONJOINT = '$prenomConjoint',
				DATE_NAISSANCE_CONJOINT = '$dateNaissanceConjoint',
				GENRE_CONJOINT = '$genreConjoint',
				FONCTION_CONJOINT = '$fonctionConjoint',
				TEL_CONJOINT = '$telConjoint'
			WHERE
				ID_CONJOINT = $idConjoint";
		}
		
		return $this -> update($idConjoint);
	}

	function saveOrUpdateCorrespondant($idCorrespondant, $idEmploye, $nomPrenomCorrespondant, $telCorrespondant) {
		$nomPrenomCorrespondant = $this -> specialCharacters($nomPrenomCorrespondant);

		if ($this -> IsNullOrEmptyString($idCorrespondant) > 0) {
			// ID_CORRESPONDANT is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO correspondance(
				ID_EMPLOYE,
				NOM_PRENOM_CORRESPONDANT,
				TEL_CORRESPONDANT
			)
			VALUES(
				$idEmploye,
				'$nomPrenomCorrespondant',
				'$telCorrespondant'
			)";
		} else {
			// UPDATE WHERE ID_CORRESPONDANT is
			$this -> _query = "UPDATE
				correspondance
			SET
				ID_EMPLOYE = $idEmploye,
				NOM_PRENOM_CORRESPONDANT = '$nomPrenomCorrespondant',
				TEL_CORRESPONDANT = '$telCorrespondant'
			WHERE
				ID_CORRESPONDANT = $idCorrespondant";
		}
		
		return $this -> update($idCorrespondant);
	}

	function getEnfantList($whereActif)	{
		$this -> _query = "SELECT
			ID_ENFANT,
			ID_EMPLOYE,
			NOM_ENFANT,
			PRENOM_ENFANT,
			DATE_NAISSANCE_ENFANT,
			LIEU_NAISSANCE_ENFANT,
			GENRE_ENFANT
		FROM
			enfant s
		$whereActif";
		
		return $this -> select();
	}

	function getClasseList()
	{
		$this -> _query = "SELECT
			ID_EDUCATION,
			RANG_CLASSE,
			CLASSE
		FROM
			classe
		WHERE
			1
		ORDER BY
			RANG_CLASSE
		ASC";
		return $this -> select();
	}

	function saveOrUpdateEnfant($idEnfant, $idEmploye, $nomEnfant, $prenomEnfant, $dateNaissanceEnfant, $lieuNaissanceEnfant, $genreEnfant) {
		$nomEnfant = $this -> specialCharacters($nomEnfant);
		$prenomEnfant = $this -> specialCharacters($prenomEnfant);
		$lieuNaissanceEnfant = $this -> specialCharacters($lieuNaissanceEnfant);

		if ($this -> IsNullOrEmptyString($idEnfant) > 0) {
			// ID_ENFANT is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO enfant(
				ID_EMPLOYE,
				NOM_ENFANT,
				PRENOM_ENFANT,
				DATE_NAISSANCE_ENFANT,
				LIEU_NAISSANCE_ENFANT,
				GENRE_ENFANT
			)
			VALUES(
				$idEmploye,
				'$nomEnfant',
				'$prenomEnfant',
				'$dateNaissanceEnfant',
				'$lieuNaissanceEnfant',
				'$genreEnfant'
			)";
		} else {
			// UPDATE WHERE ID_ENFANT is
			$this -> _query = "UPDATE
				enfant
			SET
				ID_EMPLOYE = $idEmploye,
				NOM_ENFANT = '$nomEnfant',
				PRENOM_ENFANT = '$prenomEnfant',
				DATE_NAISSANCE_ENFANT = '$dateNaissanceEnfant',
				LIEU_NAISSANCE_ENFANT = '$lieuNaissanceEnfant',
				GENRE_ENFANT = '$genreEnfant'
			WHERE
				ID_ENFANT = $idEnfant";
		}
		
		return $this -> update($idEnfant);
	}

	function saveOrUpdateScolarite($idScolarite, $idEducation, $idEnfant, $nomEcole, $adresseEcole, $anneeScolaire, $fraisScolarite) {
		$nomEcole = $this -> specialCharacters($nomEcole);
		$adresseEcole = $this -> specialCharacters($adresseEcole);

		if ($this -> IsNullOrEmptyString($idScolarite) > 0) {
			// ID_SCOLARITE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO scolarite(
				ID_EDUCATION,
				ID_ENFANT,
				NOM_ECOLE,
				ADRESSE_ECOLE,
				ANNEE_SCOLAIRE,
				FRAIS_SCOLARITE
			)
			VALUES(
				$idEducation,
				$idEnfant,
				'$nomEcole',
				'$adresseEcole',
				'$anneeScolaire',
				'$fraisScolarite'
			)";
		} else {
			// UPDATE WHERE ID_SCOLARITE is
			$this -> _query = "UPDATE
				scolarite
			SET
				ID_EDUCATION = $idEducation,
				ID_ENFANT = $idEnfant,
				NOM_ECOLE = '$nomEcole',
				ADRESSE_ECOLE = '$adresseEcole',
				ANNEE_SCOLAIRE = '$anneeScolaire',
				FRAIS_SCOLARITE = '$fraisScolarite'
			WHERE
				ID_SCOLARITE = $idScolarite";
		}
		
		return $this -> update($idScolarite);
	}

	function getEnfantScolariteList($where) {
		$this -> _query = "SELECT
			e.ID_ENFANT,
			e.ID_EMPLOYE,
			e.NOM_ENFANT,
			e.PRENOM_ENFANT,
			e.DATE_NAISSANCE_ENFANT,
			e.LIEU_NAISSANCE_ENFANT,
			e.GENRE_ENFANT,
			s.ID_SCOLARITE,
			s.ID_EDUCATION,
			s.NOM_ECOLE,
			s.ADRESSE_ECOLE,
			s.ANNEE_SCOLAIRE,
			s.FRAIS_SCOLARITE,
			c.CLASSE

		FROM
			enfant e
		LEFT JOIN scolarite s ON
			e.ID_ENFANT = s.ID_ENFANT
		LEFT JOIN classe c ON
		s.ID_EDUCATION = c.ID_EDUCATION
		$where
		ORDER BY e.DATE_NAISSANCE_ENFANT ASC";
		return $this -> select();
	}

	function getBankList($where) {
		$this -> _query = "SELECT
			ID_BANCAIRE,
			ID_EMPLOYE,
			NOM_BANQUE,
			CODE_BANQUE,
			NUMERO_COMPTE,
			CODE_GUICHET,
			CLE_RIB,
			DATE_PRISE_EFFET_BANQUE,
			ACTIF_BANCAIRE
		FROM
			bancaire
		$where
		ORDER BY DATE_PRISE_EFFET_BANQUE ASC";
		return $this -> select();
	}

	function saveOrUpdateBancaire($idBancaire, $idEmploye, $nomBanque, $codeBanque, $numeroCompte, $codeGuichet, $cleRib, $datePriseEffetBanque, $actifBancaire) {
		if ($this -> IsNullOrEmptyString($idBancaire) > 0) {
			// ID_BANCAIRE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO bancaire(
				ID_EMPLOYE,
				NOM_BANQUE,
				CODE_BANQUE,
				NUMERO_COMPTE,
				CODE_GUICHET,
				CLE_RIB,
				DATE_PRISE_EFFET_BANQUE,
				ACTIF_BANCAIRE
			)
			VALUES(
				$idEmploye,
				'$nomBanque',
				'$codeBanque',
				'$numeroCompte',
				'$codeGuichet',
				'$cleRib',
				'$datePriseEffetBanque',
				$actifBancaire
			)";
		} else {
			// UPDATE WHERE ID_BANCAIRE is
			$this -> _query = "UPDATE
			bancaire
		SET
			ID_EMPLOYE = $idEmploye,
			NOM_BANQUE = '$nomBanque',
			CODE_BANQUE = '$codeBanque',
			NUMERO_COMPTE = '$numeroCompte',
			CODE_GUICHET = '$codeGuichet',
			CLE_RIB = '$cleRib',
			DATE_PRISE_EFFET_BANQUE = '$datePriseEffetBanque',
			ACTIF_BANCAIRE = $actifBancaire
		WHERE
			ID_BANCAIRE = $idBancaire";
		}
		
		return $this -> update($idBancaire);
	}

	function getProvinceList()
	{
		$this -> _query = "SELECT
			ID_PROVINCE,
			CODE_PROVINCE,
			NOM_PROVINCE
		FROM
			province
		WHERE
			1
		ORDER BY
			NOM_PROVINCE
		ASC";
		return $this -> select();
	}

	function getRegionList()
	{
		$this -> _query = "SELECT
			ID_REGION,
			ID_PROVINCE,
			CODE_REGION,
			NOM_REGION,
			CODE_PROVINCE
		FROM
			region
		WHERE
			1
		ORDER BY
			NOM_REGION
		ASC";
		return $this -> select();
	}

	function getDistrictList()
	{
		$this -> _query = "SELECT
			ID_DISTRICT,
			ID_REGION,
			CODE_DISTRICT,
			NOM_DISTRICT,
			CODE_REGION
		FROM
			district
		WHERE
			1
		ORDER BY
			NOM_DISTRICT
		ASC";
		return $this -> select();
	}

	function getCommuneList()
	{
		$this -> _query = "SELECT
			ID_COMMUNE,
			ID_DISTRICT,
			CODE_COMMUNE,
			NOM_COMMUNE,
			CODE_DISTRICT
		FROM
			commune
		WHERE
			1
		ORDER BY
			NOM_COMMUNE
		ASC";
		return $this -> select();
	}

	function getFokotanyList()
	{
		$this -> _query = "SELECT
			ID_FOKOTANY,
			ID_COMMUNE,
			CODE_FOKOTANY,
			NOM_FOKOTANY,
			CODE_COMMUNE
		FROM
			fokotany";
		return $this -> select();
	}

	function getAdresseList($where)
	{
		$this -> _query = "SELECT
			a.ID_FOKOTANY,
			a.ID_COMMUNE,
			a.ID_DISTRICT,
			a.ID_REGION,
			a.ID_PROVINCE,
			a.ID_EMPLOYE,
			a.ID_ADRESSE,
			a.ACTIF_ADRESSE,
			a.LOGEMENT_EMPLOYE,
			f.NOM_FOKOTANY,
			c.NOM_COMMUNE,
			d.NOM_DISTRICT,
			r.NOM_REGION,
			p.NOM_PROVINCE,
			u.NOM,
			u.PRENOM
		FROM
			adresse_employe a
		LEFT JOIN fokotany f ON
			a.ID_FOKOTANY = f.ID_FOKOTANY
		LEFT JOIN commune c ON
			f.ID_COMMUNE = c.ID_COMMUNE
		LEFT JOIN district d ON
			c.ID_DISTRICT = d.ID_DISTRICT
		LEFT JOIN region r ON
			d.ID_REGION = r.ID_REGION
		LEFT JOIN province p ON
			r.ID_PROVINCE = p.CODE_PROVINCE
		LEFT JOIN personnel u ON
			a.ID_EMPLOYE = u.ID_EMPLOYE
		$where";
		return $this -> select();
	}

	function saveOrUpdateAdresse($idFokotany, $idCommune, $idDistrict, $idRegion, $idProvince, $idEmploye, $idAdresse, $actifAdresse, $logementEmploye) {
		if ($this -> IsNullOrEmptyString($idAdresse) > 0) {
			$logementEmploye = $this -> specialCharacters($logementEmploye);
			// ID_ADRESSE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO adresse_employe(
				ID_FOKOTANY,
				ID_COMMUNE,
				ID_DISTRICT,
				ID_REGION,
				ID_PROVINCE,
				ID_EMPLOYE,
				ACTIF_ADRESSE,
				LOGEMENT_EMPLOYE
			)
			VALUES(
				$idFokotany,
				$idCommune,
				$idDistrict,
				$idRegion,
				$idProvince,
				$idEmploye,
				$actifAdresse,
				'$logementEmploye'
			)";
		} else {
			// UPDATE WHERE ID_ADRESSE is
			$this -> _query = "UPDATE
				adresse_employe
			SET
				ID_FOKOTANY = $idFokotany,
				ID_COMMUNE = $idCommune,
				ID_DISTRICT = $idDistrict,
				ID_REGION = $idRegion,
				ID_PROVINCE = $idProvince,
				ID_EMPLOYE = $idEmploye,
				ACTIF_ADRESSE = $actifAdresse,
				LOGEMENT_EMPLOYE = '$logementEmploye'
			WHERE
				ID_ADRESSE = $idAdresse";
		}
		
		return $this -> update($idAdresse);
	}

	function getQualificationList($where)
	{
		$this -> _query = "SELECT
			ID_QUALIFICATION,
			ID_EMPLOYE,
			ANNEE_EXPERIENCE,
			NIVEAU_FORMATION,
			INTITULE_DIPLOME,
			ECOLE_UNIVERSITE
		FROM
			qualification
		$where";
		return $this -> select();
	}

	function saveOrUpdateQualification($idQualification, $idEmploye, $anneeExperience, $niveauFormation, $intituleDiplome, $ecoleUniversite) {
		if ($this -> IsNullOrEmptyString($idQualification) > 0) {
			$anneeExperience = $this -> specialCharacters($anneeExperience);
			$intituleDiplome = $this -> specialCharacters($intituleDiplome);
			$ecoleUniversite = $this -> specialCharacters($ecoleUniversite);
			// ID_QUALIFICATION is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO qualification(
				ID_EMPLOYE,
				ANNEE_EXPERIENCE,
				NIVEAU_FORMATION,
				INTITULE_DIPLOME,
				ECOLE_UNIVERSITE
			)
			VALUES(
				$idEmploye,
				'$anneeExperience',
				$niveauFormation,
				'$intituleDiplome',
				'$ecoleUniversite'
			)";
		} else {
			// UPDATE WHERE ID_QUALIFICATION is
			$this -> _query = "UPDATE
				qualification
			SET
				ID_EMPLOYE = $idEmploye,
				ANNEE_EXPERIENCE = '$anneeExperience',
				NIVEAU_FORMATION = $niveauFormation,
				INTITULE_DIPLOME = '$intituleDiplome',
				ECOLE_UNIVERSITE = '$ecoleUniversite'
			WHERE
				ID_QUALIFICATION = $idQualification";
		}
		
		return $this -> update($idQualification);
	}

	function getJuridiqueList($where)
	{
		$this -> _query = "SELECT
			ID_JURIDIQUE,
			ID_EMPLOYE,
			NATURE_CONDAMNATION,
			DATE_CONDAMNATION,
			DUREE_CONDAMNATION
		FROM
			juridique
		$where";
		return $this -> select();
	}

	function saveOrUpdateJuridique($idJuridique, $idEmploye, $natureCondamnation, $dateCondamnation, $dureeCondamnation) {
		if ($this -> IsNullOrEmptyString($idJuridique) > 0) {
			$natureCondamnation = $this -> specialCharacters($natureCondamnation);
			$dureeCondamnation = $this -> specialCharacters($dureeCondamnation);
			// ID_JURIDIQUE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO juridique(
				ID_EMPLOYE,
				NATURE_CONDAMNATION,
				DATE_CONDAMNATION,
				DUREE_CONDAMNATION
			)
			VALUES(
				$idEmploye,
				'$natureCondamnation',
				'$dateCondamnation',
				'$dureeCondamnation'
			)";
		} else {
			// UPDATE WHERE ID_JURIDIQUE is
			$this -> _query = "UPDATE
				juridique
			SET
				ID_EMPLOYE = $idEmploye,
				NATURE_CONDAMNATION = '$natureCondamnation',
				DATE_CONDAMNATION = '$dateCondamnation',
				DUREE_CONDAMNATION = '$dureeCondamnation'
			WHERE
				ID_JURIDIQUE = $idJuridique";
		}
		
		return $this -> update($idJuridique);
	}

	function getContratList($where, $whereAgeEnfant)	{
		$this -> _query = "SELECT
			c.ID_CONTRAT,
			c.ID_ADES,
			c.ID_CENTRE,
			c.ID_POSTE,
			c.ID_SECTION,
			c.ID_DEPARTEMENT,
			c.ID_CATEGORIE,
			c.ID_EMPLOYE,
			c.ID_SMIE,
			c.TYPE_CONTRAT,
			c.NATURE_CONTRAT,
			c.INITIAL_AVENANT,
			c.DATE_EMBAUCHE,
			c.DATE_PRISE_POSTE,
			c.DATE_EXPIRATION_CONTRAT,
			c.HORAIRE,
			c.HEURE_REGLEMENTAIRE,
			c.ACTIF_CONTRAT,
			c.PERIODICITE_PAIEMENT,
			c.MODE_PAIEMENT,
			c.SALAIRE_BASE,
			c.TAUX_HORAIRE,
			c.MOTIF_RESILIATION_CONTRAT,
			c.DATE_DEBAUCHE,
			c.SALAIRE_BASE_GRILLE,
			c.MATRICULE,
			c.PRIME_EXCEPTIONNELLE,
			g.nb_enfant,
			p.NOM,
			p.PRENOM,
			p.SEXE,
			po.TITRE_POSTE,
			ca.CATEGORIE_PROFESSIONNELLE,
			ct.ABREVIATION_CENTRE,
			c.CATEGORIE_ANCIENNETE,
			c.APPLIQUER_HOPITAL,
			po.CATEGORIE_REELLE,
			h.NUMERO_SERIE,
			p.DATE_NAISSANCE
		FROM contrat c
		LEFT JOIN personnel p ON c.ID_EMPLOYE = p.ID_EMPLOYE
		LEFT JOIN poste po ON c.ID_POSTE = po.ID_POSTE
		LEFT JOIN categorie ca ON c.ID_CATEGORIE = ca.ID_CATEGORIE
		LEFT JOIN centre ct ON c.ID_CENTRE = ct.ID_CENTRE
		LEFT JOIN (
			SELECT ID_EMPLOYE,
				COUNT(*) AS nb_enfant
			FROM
				enfant
			$whereAgeEnfant
			GROUP BY
				ID_EMPLOYE
		) g ON c.ID_EMPLOYE = g.ID_EMPLOYE
		LEFT JOIN (
			SELECT ID_EMPLOYE,
				NUMERO_SERIE
			FROM equipement
			WHERE TYPE_EQUIPEMENT IN (9, 10)
			GROUP BY ID_EMPLOYE
		) h ON c.ID_EMPLOYE = h.ID_EMPLOYE
		$where
		ORDER BY p.NOM, p.PRENOM ASC";

		return $this -> select();
	}

	function saveOrUpdateContrat($idContrat, $idAdes, $idCentre, $idPoste, $idSection, $idDepartement, $idCategorie, $idEmploye, $idSmie, $typeContrat, $natureContrat, $initialAvenant, $dateEmbauche, $datePrisePoste, $dateExpirationContrat, $horaire, $heureReglementaire, $actifContrat, $periodicitePaiement, $modePaiement, $salaireBase, $tauxHoraire, $motifResiliationContrat, $dateDebauche, $salaireBaseGrille, $matricule, $salaireSupplementaire, $categorieAnciennete, $appliquerHopital) {
		$catProf = null; // initialize
		$matches = array();
		$categorie_regexp = '/^\\((.*)+\\)/';
		if (preg_match($categorie_regexp, $categorieAnciennete, $matches)) {
			$catProf = str_replace('(', '', $matches[0]);
			$catProf = str_replace(')', '', $catProf);
		}

		$date_expiration_contrat = $this -> IsNullOrEmptyString($dateExpirationContrat) > 0 ? "(select null from dual)" : "'".$dateExpirationContrat."'";

		$date_debauche = $this -> IsNullOrEmptyString($dateDebauche) > 0 ? "(select null from dual)" : "'".$dateDebauche."'";

		$matricule = $this -> specialCharacters($matricule);
		
		if ($this -> IsNullOrEmptyString($idContrat) > 0) {
			// ID_CONTRAT is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO contrat(
				ID_ADES,
				ID_CENTRE,
				ID_POSTE,
				ID_SECTION,
				ID_DEPARTEMENT,
				ID_CATEGORIE,
				ID_EMPLOYE,
				ID_SMIE,
				TYPE_CONTRAT,
				NATURE_CONTRAT,
				INITIAL_AVENANT,
				DATE_EMBAUCHE,
				DATE_PRISE_POSTE,
				DATE_EXPIRATION_CONTRAT,
				HORAIRE,
				HEURE_REGLEMENTAIRE,
				ACTIF_CONTRAT,
				PERIODICITE_PAIEMENT,
				MODE_PAIEMENT,
				SALAIRE_BASE,
				TAUX_HORAIRE,
				MOTIF_RESILIATION_CONTRAT,
				DATE_DEBAUCHE,
				SALAIRE_BASE_GRILLE,
				MATRICULE,
				PRIME_EXCEPTIONNELLE,
				CATEGORIE_ANCIENNETE,
				APPLIQUER_HOPITAL
			)
			VALUES(
				$idAdes,
				$idCentre,
				$idPoste,
				$idSection,
				$idDepartement,
				(select id_categorie from categorie where categorie_professionnelle like '%$catProf%'),
				$idEmploye,
				$idSmie,
				'$typeContrat',
				'$natureContrat',
				'$initialAvenant',
				'$dateEmbauche',
				'$datePrisePoste',
				".$date_expiration_contrat.",
				'$horaire',
				'$heureReglementaire',
				$actifContrat,
				'$periodicitePaiement',
				'$modePaiement',
				'$salaireBase',
				'$tauxHoraire',
				'$motifResiliationContrat',
				".$date_debauche.",
				'$salaireBaseGrille',
				'$matricule',
				'$salaireSupplementaire',
				'$categorieAnciennete',
				$appliquerHopital
			)";
		} else {
			// UPDATE WHERE ID_CONTRAT is
			$this -> _query = "UPDATE
				contrat
			SET
				ID_ADES = $idAdes,
				ID_CENTRE = $idCentre,
				ID_POSTE = $idPoste,
				ID_SECTION = $idSection,
				ID_DEPARTEMENT = $idDepartement,
				ID_CATEGORIE = (select id_categorie from categorie where categorie_professionnelle like '%$catProf%'),
				ID_EMPLOYE = $idEmploye,
				ID_SMIE = $idSmie,
				TYPE_CONTRAT = '$typeContrat',
				NATURE_CONTRAT = '$natureContrat',
				INITIAL_AVENANT = '$initialAvenant',
				DATE_EMBAUCHE = '$dateEmbauche',
				DATE_PRISE_POSTE = '$datePrisePoste',
				DATE_EXPIRATION_CONTRAT = ".$date_expiration_contrat.",
				HORAIRE = '$horaire',
				HEURE_REGLEMENTAIRE = '$heureReglementaire',
				ACTIF_CONTRAT = $actifContrat,
				PERIODICITE_PAIEMENT = '$periodicitePaiement',
				MODE_PAIEMENT = '$modePaiement',
				SALAIRE_BASE = '$salaireBase',
				TAUX_HORAIRE = '$tauxHoraire',
				MOTIF_RESILIATION_CONTRAT = '$motifResiliationContrat',
				DATE_DEBAUCHE = ".$date_debauche.",
				SALAIRE_BASE_GRILLE = '$salaireBaseGrille',
				MATRICULE = '$matricule',
				PRIME_EXCEPTIONNELLE = '$salaireSupplementaire',
				CATEGORIE_ANCIENNETE = '$categorieAnciennete',
				APPLIQUER_HOPITAL = $appliquerHopital
			WHERE
				ID_CONTRAT = $idContrat";
		}
		
		return $this -> update($idContrat);
	}

	function getPosteAncienneteList()	{
		$this -> _query = "SELECT 
		ID_POSTE,TITRE_POSTE,DESCRIPTION_POSTE,
		(select categorie_professionnelle from categorie where id_categorie = poste.UN) un,
		(select categorie_professionnelle from categorie where id_categorie = poste.DEUX) deux,
		(select categorie_professionnelle from categorie where id_categorie = poste.TROIS) trois,
		(select categorie_professionnelle from categorie where id_categorie = poste.QUATRE) quatre,
		(select categorie_professionnelle from categorie where id_categorie = poste.CINQ) cinq,
		(select categorie_professionnelle from categorie where id_categorie = poste.SIX) six,
		(select categorie_professionnelle from categorie where id_categorie = poste.SEPT) sept,
		(select categorie_professionnelle from categorie where id_categorie = poste.HUIT) huit,
		(select categorie_professionnelle from categorie where id_categorie = poste.NEUF) neuf,
		(select categorie_professionnelle from categorie where id_categorie = poste.DIX) dix,
		(select categorie_professionnelle from categorie where id_categorie = poste.ONZE) onze,
		(select categorie_professionnelle from categorie where id_categorie = poste.DOUZE) douze,
		(select categorie_professionnelle from categorie where id_categorie = poste.TREIZE) treize,
		(select categorie_professionnelle from categorie where id_categorie = poste.QUATORZE) quatorze,
		(select categorie_professionnelle from categorie where id_categorie = poste.QUINZE) quinze,
		(select categorie_professionnelle from categorie where id_categorie = poste.SEIZE) seize,
		(select categorie_professionnelle from categorie where id_categorie = poste.DIXSEPT) dixsept,
		(select categorie_professionnelle from categorie where id_categorie = poste.DIXHUIT) dixhuit,
		(select categorie_professionnelle from categorie where id_categorie = poste.DIXNEUF) dixneuf,
		(select categorie_professionnelle from categorie where id_categorie = poste.VINGT) vingt,
		poste.TAUX_HORAIRE,
		poste.CATEGORIE_REELLE
		
		FROM poste
		ORDER BY TITRE_POSTE ASC";
		
		return $this -> select();
	}

	function saveOrUpdatePosteV2($idPoste, $titrePoste, $descriptionPoste, $un, $deux, $trois, $quatre, $cinq, $six, $sept, $huit, $neuf, $dix, $vingt, $tauxHoraire, $categorieReelle) {
		$titrePoste = $this -> specialCharacters($titrePoste);
		$descriptionPoste = $this -> specialCharacters($descriptionPoste);

		if ($this -> IsNullOrEmptyString($idPoste) > 0) {
			// ID_POSTE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO poste(
				TITRE_POSTE,
				DESCRIPTION_POSTE,
				UN,
				DEUX,
				TROIS,
				QUATRE,
				CINQ,
				SIX,
				SEPT,
				HUIT,
				NEUF,
				DIX,
				ONZE,
				DOUZE,
				TREIZE,
				QUATORZE,
				QUINZE,
				SEIZE,
				DIXSEPT,
				DIXHUIT,
				DIXNEUF,
				VINGT,
				TAUX_HORAIRE,
				CATEGORIE_REELLE
			) 
			VALUES (
				'$titrePoste',
				'$descriptionPoste',
				(select id_categorie from categorie where categorie_professionnelle like '%$un%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$deux%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$trois%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$quatre%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$cinq%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$six%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$sept%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$huit%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$neuf%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$dix%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				(select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				$tauxHoraire,
				'$categorieReelle'
			)";
		} else {
			// UPDATE WHERE ID_POSTE is
			$this -> _query = "UPDATE
				poste
			SET
				TITRE_POSTE = '$titrePoste',
				DESCRIPTION_POSTE = '$descriptionPoste',
				UN = (select id_categorie from categorie where categorie_professionnelle like '%$un%'),
				DEUX = (select id_categorie from categorie where categorie_professionnelle like '%$deux%'),
				TROIS = (select id_categorie from categorie where categorie_professionnelle like '%$trois%'),
				QUATRE = (select id_categorie from categorie where categorie_professionnelle like '%$quatre%'),
				CINQ = (select id_categorie from categorie where categorie_professionnelle like '%$cinq%'),
				SIX = (select id_categorie from categorie where categorie_professionnelle like '%$six%'),
				SEPT = (select id_categorie from categorie where categorie_professionnelle like '%$sept%'),
				HUIT = (select id_categorie from categorie where categorie_professionnelle like '%$huit%'),
				NEUF = (select id_categorie from categorie where categorie_professionnelle like '%$neuf%'),
				DIX = (select id_categorie from categorie where categorie_professionnelle like '%$dix%'),
				ONZE = (select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				DOUZE = (select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				TREIZE = (select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				QUATORZE = (select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				QUINZE= (select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				SEIZE = (select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				DIXSEPT = (select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				DIXHUIT = (select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				DIXNEUF = (select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				VINGT = (select id_categorie from categorie where categorie_professionnelle like '%$vingt%'),
				TAUX_HORAIRE = $tauxHoraire,
				CATEGORIE_REELLE = '$categorieReelle'
			WHERE
				ID_POSTE = $idPoste";
		}
		
		return $this -> update($idPoste);
	}

	function saveOrUpdateInEffectif($idCentre, $effectif) {		
		if ($effectif == "1") {
			$this -> _query = "INSERT INTO effectif(ID_CENTRE, EFFECTIF)
			VALUES($idCentre, $effectif)";
		} else {
			$this -> _query = "UPDATE effectif SET EFFECTIF=$effectif WHERE ID_CENTRE=$idCentre";
		}
		
		return $this -> update($idCentre);
	}

	function getCentreEffectif() {
		$this -> _query = "SELECT
			ID_EFFECTIF,
			ID_CENTRE,
			EFFECTIF
		FROM
			effectif ";

		return $this -> select();		
	}

	function updateEffectifTotal() {
		$this -> _query = "UPDATE totaleffectif SET TOTAL=(TOTAL + 1)";
		return $this -> update(1);
	}

	function getTotalEffectif() {
		$this -> _query = "SELECT TOTAL FROM totaleffectif";

		return $this -> select();
	}

	function getCategorieByPosteAnciennete($poste, $anciennete)	{
		$this -> _query = "SELECT
			ID_POSTE,
			TITRE_POSTE,
			DESCRIPTION_POSTE,
			(
			SELECT
				CATEGORIE_PROFESSIONNELLE
			FROM
				categorie
			WHERE
				id_categorie = poste.$anciennete
			) as cat_pro,
			TAUX_HORAIRE,
			(
				SELECT
					ID_CATEGORIE
				FROM
					categorie
				WHERE
					id_categorie = poste.$anciennete
			) as cat_pro_id,
			CATEGORIE_REELLE
		FROM
			poste
		WHERE
			poste.ID_POSTE = $poste";
		
		return $this -> select();
	}

	function getCategorieByPoste($where)	{
		$this -> _query = "SELECT
			h.ID_POSTE,
			h.TITRE_POSTE,
			h.DESCRIPTION_POSTE,
			h.CATEGORIE_REELLE
		FROM
			poste h
		LEFT JOIN contrat c
			ON h.ID_POSTE = c.ID_POSTE
		$where";
		
		return $this -> select();
	}

	function getSalaireByCategoriAnciennete($categorie, $salaireCorrespondant)	{
		$this -> _query = "SELECT
			g.$salaireCorrespondant
		FROM
			grille g
		INNER JOIN categorie c ON
			g.ID_CATEGORIE = c.ID_CATEGORIE
		WHERE c.CATEGORIE_PROFESSIONNELLE = '$categorie'";
		
		return $this -> select();
	}

	function getSalaireList($where)	{
		$this -> _query = "SELECT
			ID_SALAIRE,
			MOIS_SALAIRE,
			ANNEE_SALAIRE,
			TYPE_SALAIRE,
			DATE_CLOTURE_SALAIRE,
			CLOTURE_SALAIRE
		FROM
			salaire
		$where
		ORDER BY STR_TO_DATE(concat('01/', MOIS_SALAIRE,'/', ANNEE_SALAIRE), '%m/%d/%Y') ASC";
		
		return $this -> select();
	}

	function saveOrUpdateSalaire($idSalaire, $moisSalaire, $anneeSalaire, $typeSalaire, $dateClotureSalaire, $clotureSalaire) {
		$dateClotureSalaire = $this -> IsNullOrEmptyString($dateClotureSalaire) > 0 ? "(select null from dual)" : "'".$dateClotureSalaire."'";

		if ($this -> IsNullOrEmptyString($idSalaire) > 0) {
			// ID_SALAIRE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO salaire(
				MOIS_SALAIRE,
				ANNEE_SALAIRE,
				TYPE_SALAIRE,
				DATE_CLOTURE_SALAIRE,
				CLOTURE_SALAIRE
			)
			VALUES(
				'$moisSalaire',
				'$anneeSalaire',
				'$typeSalaire',
				".$dateClotureSalaire.",
				'$clotureSalaire'
			)";
		} else {
			// UPDATE WHERE ID_SALAIRE is
			$this -> _query = "UPDATE
				salaire
			SET
				MOIS_SALAIRE = '$moisSalaire',
				ANNEE_SALAIRE = '$anneeSalaire',
				TYPE_SALAIRE = '$typeSalaire',
				DATE_CLOTURE_SALAIRE = ".$dateClotureSalaire.",
				CLOTURE_SALAIRE = '$clotureSalaire'
			WHERE
				ID_SALAIRE = $idSalaire";
		}
		
		return $this -> update($idSalaire);
	}

	function saveOrUpdateSalaireInterim($idPrimeIndemnite, $idContrat, $dateDebutInterim, $dateFinInterim, $remarquesInterim) {
		$remarquesInterim = $this -> specialCharacters($remarquesInterim);
		if ($this -> IsNullOrEmptyString($idPrimeIndemnite) > 0) {
			// ID_PRIME_INDEMNITE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO prime_indemnite(
				ID_CONTRAT,
				DATE_DEBUT_INTERIM,
				DATE_FIN_INTERIM,
				REMARQUES_INTERIM
			)
			VALUES(
				$idContrat,
				'$dateDebutInterim',
				'$dateFinInterim',
				'$remarquesInterim'
			)";
		} else {
			// UPDATE WHERE ID_PRIME_INDEMNITE is
			$this -> _query = "UPDATE
				prime_indemnite
			SET
				ID_CONTRAT = $idContrat,
				DATE_DEBUT_INTERIM = '$dateDebutInterim',
				DATE_FIN_INTERIM = '$dateFinInterim',
				REMARQUES_INTERIM = '$remarquesInterim'
			WHERE
				ID_PRIME_INDEMNITE = $idPrimeIndemnite";
		}
		
		return $this -> update($idPrimeIndemnite);
	}

	function getSalaireInterimList($where)	{
		$this -> _query = "SELECT
			ID_PRIME_INDEMNITE,
			ID_CONTRAT,
			DATE_DEBUT_INTERIM,
			DATE_FIN_INTERIM,
			REMARQUES_INTERIM
		FROM
			prime_indemnite
		$where";
		
		return $this -> select();
	}

	function saveOrUpdateHeuresSupplementaires($idHeureSupplementaire, $idContrat, $infHuitHeures, $supHuitHeures, $nuitHabituelle, $nuitOccasionnelle, $dimanche, $ferie, $remarqueMotifHs, $moisSalaire) {
		$remarqueMotifHs = $this -> specialCharacters($remarqueMotifHs);
		if ($this -> IsNullOrEmptyString($idHeureSupplementaire) > 0) {
			// ID_HEURE_SUPPLEMENTAIRE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO heures_supplementaires(
				ID_CONTRAT,
				INF_HUIT_HEURES,
				SUP_HUIT_HEURES,
				NUIT_HABITUELLE,
				NUIT_OCCASIONNELLE,
				DIMANCHE,
				FERIE,
				REMARQUE_MOTIF_HS,
				MOIS_SALAIRE
			)
			VALUES(
				$idContrat,
				'$infHuitHeures',
				'$supHuitHeures',
				'$nuitHabituelle',
				'$nuitOccasionnelle',
				'$dimanche',
				'$ferie',
				'$remarqueMotifHs',
				'$moisSalaire'
			)";
		} else {
			// UPDATE WHERE ID_HEURE_SUPPLEMENTAIRE is
			$this -> _query = "UPDATE
			heures_supplementaires
		SET
			ID_CONTRAT = $idContrat,
			INF_HUIT_HEURES = '$infHuitHeures',
			SUP_HUIT_HEURES = '$supHuitHeures',
			NUIT_HABITUELLE = '$nuitHabituelle',
			NUIT_OCCASIONNELLE = '$nuitOccasionnelle',
			DIMANCHE = '$dimanche',
			FERIE = '$ferie',
			REMARQUE_MOTIF_HS = '$remarqueMotifHs',
			MOIS_SALAIRE = '$moisSalaire'
		WHERE
			ID_HEURE_SUPPLEMENTAIRE = $idHeureSupplementaire";
		}
		
		return $this -> update($idHeureSupplementaire);
	}

	function getHeuresSupplementairesList($where)	{
		$this -> _query = "SELECT
			h.ID_HEURE_SUPPLEMENTAIRE,
			h.ID_CONTRAT,
			h.INF_HUIT_HEURES,
			h.SUP_HUIT_HEURES,
			h.NUIT_HABITUELLE,
			h.NUIT_OCCASIONNELLE,
			h.DIMANCHE,
			h.FERIE,
			h.REMARQUE_MOTIF_HS,
			h.MOIS_SALAIRE,
			c.MATRICULE,
			s.MOIS_SALAIRE,
			s.ANNEE_SALAIRE,
			p.NOM,
			p.PRENOM
		FROM
			heures_supplementaires h
		INNER JOIN contrat c ON
			h.ID_CONTRAT = c.ID_CONTRAT
		INNER JOIN salaire s ON
			h.MOIS_SALAIRE = s.ID_SALAIRE
		INNER JOIN personnel p ON c.ID_EMPLOYE = p.ID_EMPLOYE
		$where
		ORDER BY h.MOIS_SALAIRE DESC";
		
		return $this -> select();
	}

	function getTauxList($where)	{
		$this -> _query = "SELECT
			ID_TAUX,
			NOM_TAUX,
			VALEUR_POURCENT,
			DATE_PRISE_EFFET_TAUX,
			ACTIF_TAUX
		FROM
			taux
		$where
		ORDER BY
			DATE_PRISE_EFFET_TAUX ASC";

		return $this -> select();
	}

	function saveOrUpdateTaux($idTaux, $nomTaux, $valeurPourcent, $datePriseEffetTaux, $actifTaux) {
		if ($this -> IsNullOrEmptyString($idTaux) > 0) {
			// ID_TAUX is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO taux(
				NOM_TAUX,
				VALEUR_POURCENT,
				DATE_PRISE_EFFET_TAUX,
				ACTIF_TAUX
			)
			VALUES(
				'$nomTaux',
				'$valeurPourcent',
				'$datePriseEffetTaux',
				$actifTaux
			)";
		} else {
			// UPDATE WHERE ID_TAUX is
			$this -> _query = "UPDATE
				taux
			SET
				NOM_TAUX = '$nomTaux',
				VALEUR_POURCENT = '$valeurPourcent',
				DATE_PRISE_EFFET_TAUX = '$datePriseEffetTaux',
				ACTIF_TAUX = '$actifTaux'
			WHERE
				ID_TAUX = $idTaux";
		}
		
		return $this -> update($idTaux);
	}

	function getTauxHSList($where)	{
		$this -> _query = "SELECT
			ID_HS_BASE,
			NATURE_HS_BASE,
			FORMULE_CALCUL,
			DATE_EFFET_HS,
			ACTIF_HS_BASE
		FROM
			hs_base
		$where
		ORDER BY
			DATE_EFFET_HS ASC";
		
		return $this -> select();
	}

	function saveOrUpdateTauxHS($idHsBase, $natureHsBase, $formuleCalcul, $dateEffetHs, $actifHsBase) {
		if ($this -> IsNullOrEmptyString($idHsBase) > 0) {
			// ID_HS_BASE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO hs_base(
				NATURE_HS_BASE,
				FORMULE_CALCUL,
				DATE_EFFET_HS,
				ACTIF_HS_BASE
			)
			VALUES(
				'$natureHsBase',
				'$formuleCalcul',
				'$dateEffetHs',
				$actifHsBase
			)";
		} else {
			// UPDATE WHERE ID_HS_BASE is
			$this -> _query = "UPDATE
				hs_base
			SET
				NATURE_HS_BASE = '$natureHsBase',
				FORMULE_CALCUL = '$formuleCalcul',
				DATE_EFFET_HS = '$dateEffetHs',
				ACTIF_HS_BASE = '$actifHsBase'
			WHERE
				ID_HS_BASE = $idHsBase";
		}
		
		return $this -> update($idHsBase);
	}

	function saveOrUpdateAbsence($idAbsence, $idContrat, $idSalaire, $motifAbsence, $dateHeureDebutHs, $dateHeureFinHs, $dureeAbsence, $typeAbsence, $nbJours) {
		$motifAbsence = $this -> specialCharacters($motifAbsence);
		$date_Heure_DebutHs = $this -> IsNullOrEmptyString($dateHeureDebutHs) > 0 ? "(select null from dual)" : "'".$dateHeureDebutHs."'";
		$date_Heure_FinHs = $this -> IsNullOrEmptyString($dateHeureFinHs) > 0 ? "(select null from dual)" : "'".$dateHeureFinHs."'";
		$duree_Absence = $this -> IsNullOrEmptyString($dureeAbsence) > 0 ? 0 : $dureeAbsence;
		
		if ($this -> IsNullOrEmptyString($idAbsence) > 0) {
			// ID_ABSENCE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO absence(
				ID_CONTRAT,
				ID_SALAIRE,
				MOTIF_ABSENCE,
				DATE_HEURE_DEBUT_HS,
				DATE_HEURE_FIN_HS,
				DUREE_ABSENCE,
				TYPE_ABSENCE,
				NB_JOURS
			)
			VALUES(
				'$idContrat',
				'$idSalaire',
				'$motifAbsence',
				".$date_Heure_DebutHs.",
				".$date_Heure_FinHs.",
				'$duree_Absence',
				'$typeAbsence',
				'$nbJours'
			)";
		} else {
			// UPDATE WHERE ID_ABSENCE is
			$this -> _query = "UPDATE
				absence
			SET
				ID_CONTRAT = '$idContrat',
				ID_SALAIRE = '$idSalaire',
				MOTIF_ABSENCE = '$motifAbsence',
				DATE_HEURE_DEBUT_HS = ".$date_Heure_DebutHs.",
				DATE_HEURE_FIN_HS = ".$date_Heure_FinHs.",
				DUREE_ABSENCE = '$duree_Absence',
				TYPE_ABSENCE = '$typeAbsence',
				NB_JOURS = '$nbJours'
			WHERE
				ID_ABSENCE = $idAbsence";
		}
		
		return $this -> update($idAbsence);
	}

	function getAbsenceList($where)	{
		$this -> _query = "SELECT
			h.ID_ABSENCE,
			h.ID_CONTRAT,
			h.ID_SALAIRE,
			h.MOTIF_ABSENCE,
			h.DATE_HEURE_DEBUT_HS,
			h.DATE_HEURE_FIN_HS,
			h.DUREE_ABSENCE,
			h.TYPE_ABSENCE,
			c.MATRICULE,
			s.MOIS_SALAIRE,
			s.ANNEE_SALAIRE,
			p.NOM,
			p.PRENOM,
			h.NB_JOURS,
			ct.ABREVIATION_CENTRE
		FROM
			absence h
		INNER JOIN contrat c ON
			h.ID_CONTRAT = c.ID_CONTRAT
		INNER JOIN salaire s ON
			h.ID_SALAIRE = s.ID_SALAIRE
		INNER JOIN personnel p ON 
			c.ID_EMPLOYE = p.ID_EMPLOYE
		INNER JOIN centre ct ON
			c.ID_CENTRE = ct.ID_CENTRE		
		$where
		ORDER BY h.ID_SALAIRE DESC";
		
		return $this -> select();
	}

	function getCongeList($where)	{
		$this -> _query = "SELECT
			h.ID_CONGE,
			h.ID_CONTRAT,
			h.ID_SALAIRE,
			h.RELIQUAT,
			h.DATE_DEBUT_CONGE,
			h.DATE_FIN_CONGE,
			h.REMARQUE_CONGE,
			h.DUREE_CONGE_PRISE,
			h.NATURE_CONGE,
			c.MATRICULE,
			s.MOIS_SALAIRE,
			s.ANNEE_SALAIRE,
			c.ID_CENTRE,
			p.NOM,
			p.PRENOM,
			ct.ABREVIATION_CENTRE
		FROM
			conge h
		INNER JOIN contrat c ON
			h.ID_CONTRAT = c.ID_CONTRAT
		INNER JOIN salaire s ON
			h.ID_SALAIRE = s.ID_SALAIRE
		INNER JOIN personnel p ON 
			c.ID_EMPLOYE = p.ID_EMPLOYE
		INNER JOIN centre ct ON
			c.ID_CENTRE = ct.ID_CENTRE
		$where
		ORDER BY h.ID_CONGE DESC";
		
		return $this -> select();
	}

	function getAbsenceListDetails($where) {
		$this -> _query = "SELECT
			h.ID_ABSENCE,
			h.ID_CONTRAT,
			h.ID_SALAIRE,
			h.MOTIF_ABSENCE,
			h.DATE_HEURE_DEBUT_HS,
			h.DATE_HEURE_FIN_HS,
			h.DUREE_ABSENCE,
			h.TYPE_ABSENCE,
			c.MATRICULE,
			s.MOIS_SALAIRE,
			s.ANNEE_SALAIRE,
			p.NOM,
			p.PRENOM,
			h.NB_JOURS,
			ct.ABREVIATION_CENTRE,
			z.Date
		FROM
			absence h
		INNER JOIN contrat c ON
			h.ID_CONTRAT = c.ID_CONTRAT
		INNER JOIN salaire s ON
			h.ID_SALAIRE = s.ID_SALAIRE
		INNER JOIN personnel p ON 
			c.ID_EMPLOYE = p.ID_EMPLOYE
		INNER JOIN centre ct ON
			c.ID_CENTRE = ct.ID_CENTRE
		INNER JOIN (
			select a.Date, absence.ID_ABSENCE
			from (
				select curdate() - INTERVAL (a.a + (10 * b.a) + (100 * c.a) + (1000 * d.a) ) DAY as Date
				from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
				cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
				cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
				cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as d
			) a ,absence
			where a.Date between absence.DATE_HEURE_DEBUT_HS and absence.DATE_HEURE_FIN_HS
		) z ON
			h.ID_ABSENCE = z.ID_ABSENCE
		$where
		ORDER BY h.ID_SALAIRE DESC";
		
		return $this -> select();
	}

	function getCongeListDetails($where) {
		$this -> _query ="SELECT
			h.ID_CONGE,
			h.ID_CONTRAT,
			h.ID_SALAIRE,
			h.RELIQUAT,
			h.DATE_DEBUT_CONGE,
			h.DATE_FIN_CONGE,
			h.REMARQUE_CONGE,
			h.DUREE_CONGE_PRISE,
			h.NATURE_CONGE,
			c.MATRICULE,
			s.MOIS_SALAIRE,
			s.ANNEE_SALAIRE,
			c.ID_CENTRE,
			p.NOM,
			p.PRENOM,
			ct.ABREVIATION_CENTRE,
			z.Date
		FROM
			conge h
		INNER JOIN contrat c ON
			h.ID_CONTRAT = c.ID_CONTRAT
		INNER JOIN salaire s ON
			h.ID_SALAIRE = s.ID_SALAIRE
		INNER JOIN personnel p ON 
			c.ID_EMPLOYE = p.ID_EMPLOYE
		INNER JOIN centre ct ON
			c.ID_CENTRE = ct.ID_CENTRE
			INNER JOIN (
			select a.Date, conge.ID_CONGE
			from (
				select curdate() - INTERVAL (a.a + (10 * b.a) + (100 * c.a) + (1000 * d.a) ) DAY as Date
				from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
				cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
				cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
				cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as d
			) a ,conge
			where a.Date between conge.DATE_DEBUT_CONGE and conge.DATE_FIN_CONGE
		) z ON
			h.ID_CONGE = z.ID_CONGE
		$where
		ORDER BY h.ID_CONGE DESC";
		
		return $this -> select();
	}

	function saveOrUpdateConge($idConge, $idContrat, $idSalaire, $reliquat, $dateDebutConge, $dateFinConge, $remarqueConge, $dureeCongePrise, $natureConge) {
		$remarqueConge = $this -> specialCharacters($remarqueConge);
		$date_Debut_Conge = $this -> IsNullOrEmptyString($dateDebutConge) > 0 ? "(select null from dual)" : "'".$dateDebutConge."'";
		$date_Fin_Conge = $this -> IsNullOrEmptyString($dateFinConge) > 0 ? "(select null from dual)" : "'".$dateFinConge."'";
		$duree_Conge_Prise = $this -> IsNullOrEmptyString($dureeCongePrise) > 0 ? 0 : $dureeCongePrise;
		
		if ($this -> IsNullOrEmptyString($idConge) > 0) {
			// ID_CONGE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO conge(
				ID_CONTRAT,
				ID_SALAIRE,
				RELIQUAT,
				DATE_DEBUT_CONGE,
				DATE_FIN_CONGE,
				REMARQUE_CONGE,
				DUREE_CONGE_PRISE,
				NATURE_CONGE,
				DATE_SAISIE_CONGE
			)
			VALUES(
				$idContrat,
				$idSalaire,
				$reliquat,
				".$date_Debut_Conge.",
				".$date_Fin_Conge.",
				'$remarqueConge',
				'$duree_Conge_Prise',
				'$natureConge',
				SYSDATE()
			)";
		} else {
			// UPDATE WHERE ID_CONGE is
			$this -> _query = "UPDATE
				conge
			SET
				ID_CONTRAT = $idContrat,
				ID_SALAIRE = $idSalaire,
				RELIQUAT = $reliquat,
				DATE_DEBUT_CONGE = ".$date_Debut_Conge.",
				DATE_FIN_CONGE = ".$date_Fin_Conge.",
				REMARQUE_CONGE = '$remarqueConge',
				DUREE_CONGE_PRISE = ".$duree_Conge_Prise.",
				NATURE_CONGE = '$natureConge'
			WHERE
				ID_CONGE = $idConge";
		}
		
		return $this -> update($idConge);
	}

	function getTaux($nomTaux) {
		$this -> _query = "SELECT
			VALEUR_POURCENT,
			ID_TAUX
		FROM
			taux
		WHERE
			NOM_TAUX = $nomTaux AND ACTIF_TAUX = 1";
		
		return $this -> select();
	}

	function getTauxReductionMaternite() {
		$this -> _query = "SELECT
			VALEUR_POURCENT,
			ID_TAUX
		FROM
			taux
		WHERE
			NOM_TAUX = 3 AND ACTIF_TAUX = 1";
		
		return $this -> select();
	}

	function getMontantAbattement() {
		$this -> _query = "SELECT
			VALEUR_POURCENT,
			ID_TAUX
		FROM
			taux
		WHERE
			NOM_TAUX = 4 AND ACTIF_TAUX = 1";
		
		return $this -> select();
	}

	function getCnapsList2($where)	{
		$this -> _query = "SELECT
			ID_CNAPS,
			ID_ADES,
			PLAFOND_CNAPS,
			POURCENTAGE_EMPLOYE_CNAPS,
			POURCENTAGE_EMPLOYEUR_CNAPS,
			DATE_PRISE_EFFET_CNAPS,
			ACTIF_CNAPS
		FROM
			cnaps
		$where
		ORDER BY
			DATE_PRISE_EFFET_CNAPS ASC";

		return $this -> select();
	}

	function getIrsaList2($where) {
		$this -> _query = "SELECT
			ID_IRSA,
			ID_ADES,
			PLAFOND,
			POURCENTAGE_EMPLOYE_IRSA,
			POURCENTAGE_EMPLOYEUR_IRSA,
			DEDUCTION_ENFANT,
			DATE_PRISE_EFFET_IRSA,
			ACTIF_IRSA
		FROM
			irsa
		$where
		ORDER BY
			DATE_PRISE_EFFET_IRSA ASC";

		return $this -> select();
	}

	function getSmieList2($where) {
		$this -> _query = "SELECT
			s.ID_SMIE,
			s.NOM_SMIE,
			s.PLAFOND_SMIE,
			s.DEDUCTION_EMPLOYE,
			s.DEDUCTION_EMPLOYEUR,
			s.COUT_EXCEDENTAIRE,
			s.FRANCHISE_COUT_EXCENDENTAIRE,
			s.DATE_PRISE_EFFET_SMIE,
			s.ACTIF_SMIE,
			s.ID_CENTRE,
			c.NOM_CENTRE
		FROM
			smie s
		INNER JOIN centre c 
			ON s.ID_CENTRE = c.ID_CENTRE AND s.ACTIF_SMIE = 1
		$where
		ORDER BY
			c.NOM_CENTRE ASC";
		// echo $this ->_query;
		return $this -> select();
	}

	function getUtilisateurList($where) {
		$this -> _query = "SELECT
			u.id_utilisateur,
			u.nom_utilisateur,
			u.mot_de_passe,
			u.centre_affectation,
			u.type_utilisateur,
			u.maj_mot_de_passe,
			u.avatar,
			c.NOM_CENTRE
		FROM
			utilisateur u
		INNER JOIN centre c
			ON u.centre_affectation = c.ID_CENTRE
		$where
		ORDER BY
			nom_utilisateur ASC";

		return $this -> select();
	}

	function saveOrUpdateUtilisateur($idUtilisateur, $nomUtilisateur, $motDePasse, $centreAffectation, $typeUtilisateur, $majMotDePasse, $avatar) {
		$nomUtilisateur = $this -> specialCharacters($nomUtilisateur);
		if ($this -> IsNullOrEmptyString($idUtilisateur) > 0) {
			// idUtilisateur is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO utilisateur(
				nom_utilisateur,
				mot_de_passe,
				centre_affectation,
				type_utilisateur,
				maj_mot_de_passe,
				avatar
			)
			VALUES(
				'$nomUtilisateur',
				PASSWORD('$motDePasse'),
				$centreAffectation,
				$typeUtilisateur,
				$majMotDePasse,
				'$avatar'
			)";
		} else {
			// UPDATE WHERE idUtilisateur is
			$avatar2 = empty($avatar) ? '' : ",
			avatar = '$avatar'";
			$this -> _query = "UPDATE
				utilisateur
			SET
				nom_utilisateur = '$nomUtilisateur',
				mot_de_passe = PASSWORD('$motDePasse'),
				centre_affectation = $centreAffectation,
				type_utilisateur = $typeUtilisateur,
				maj_mot_de_passe = $majMotDePasse
				".$avatar2."
			WHERE
				id_utilisateur = $idUtilisateur";
		}
		
		return $this -> update($idUtilisateur);
	}

	function getFraisMedicauxList($where) {
		$this -> _query = "SELECT
			h.ID_MEDICAUX,
			h.ID_CONTRAT,
			h.ID_SALAIRE,
			h.DATE_MEDICAUX,
			h.FRAIS_MEDICAUX,
			h.REMARQUE_MOTIF_MEDICAL,
			c.MATRICULE,
			s.MOIS_SALAIRE,
			s.ANNEE_SALAIRE,
			p.NOM,
			p.PRENOM
		FROM
			medicaux h
		INNER JOIN contrat c ON
			h.ID_CONTRAT = c.ID_CONTRAT
		INNER JOIN salaire s ON
			h.ID_SALAIRE = s.ID_SALAIRE
		INNER JOIN personnel p ON 
			c.ID_EMPLOYE = p.ID_EMPLOYE
		$where
		ORDER BY h.ID_SALAIRE DESC";
		
		return $this -> select();
	}

	function saveOrUpdateMedicaux($idMedicaux, $idContrat, $idSalaire, $dateMedicaux, $fraisMedicaux, $remarqueMotifMedical) {
		$remarqueMotifMedical = $this -> specialCharacters($remarqueMotifMedical);
		$date_Medicaux = $this -> IsNullOrEmptyString($dateMedicaux) > 0 ? "(select null from dual)" : "'".$dateMedicaux."'";
		$frais_Medicaux = $this -> IsNullOrEmptyString($fraisMedicaux) > 0 ? 0 : $fraisMedicaux;
		
		if ($this -> IsNullOrEmptyString($idMedicaux) > 0) {
			// ID_MEDICAUX is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO medicaux(
				ID_CONTRAT,
				ID_SALAIRE,
				DATE_MEDICAUX,
				FRAIS_MEDICAUX,
				REMARQUE_MOTIF_MEDICAL
			)
			VALUES(
				$idContrat,
				$idSalaire,
				".$date_Medicaux.",
				'$fraisMedicaux',
				'$remarqueMotifMedical'
			)";
		} else {
			// UPDATE WHERE ID_MEDICAUX is
			$this -> _query = "UPDATE
				medicaux
			SET
				ID_CONTRAT = $idContrat,
				ID_SALAIRE = $idSalaire,
				DATE_MEDICAUX = ".$date_Medicaux.",
				FRAIS_MEDICAUX = '$fraisMedicaux',
				REMARQUE_MOTIF_MEDICAL = '$remarqueMotifMedical'
			WHERE
				ID_MEDICAUX = $idMedicaux";
		}
		
		return $this -> update($idMedicaux);
	}

	function getDiversRetenusList($where)	{
		$this -> _query = "SELECT
			h.ID_DIVERS_RETENUS,
			h.ID_CONTRAT,
			h.ID_SALAIRE,
			h.MONTANT_DIVERS_RETENUS,
			h.DATE_DIVERS_RETENUS,
			h.REMARQUE_DIVERS_RETENUS,
			c.MATRICULE,
			s.MOIS_SALAIRE,
			s.ANNEE_SALAIRE,
			p.NOM,
			p.PRENOM
		FROM
			divers_retenus h
		INNER JOIN contrat c ON
			h.ID_CONTRAT = c.ID_CONTRAT
		INNER JOIN salaire s ON
			h.ID_SALAIRE = s.ID_SALAIRE
		INNER JOIN personnel p ON 
			c.ID_EMPLOYE = p.ID_EMPLOYE
		$where
		ORDER BY h.ID_SALAIRE DESC";
		
		return $this -> select();
	}

	function saveOrUpdateDiversRetenus($idDiversRetenus, $idContrat, $idSalaire, $montantDiversRetenus, $dateDiversRetenus, $remarqueDiversRetenus) {
		$remarque_Divers_Retenus = $this -> specialCharacters($remarqueDiversRetenus);
		$date_Divers_Retenus = $this -> IsNullOrEmptyString($dateDiversRetenus) > 0 ? "(select null from dual)" : "'".$dateDiversRetenus."'";
		$montant_Divers_Retenus = $this -> IsNullOrEmptyString($montantDiversRetenus) > 0 ? 0 : $montantDiversRetenus;
		
		if ($this -> IsNullOrEmptyString($idDiversRetenus) > 0) {
			// ID_DIVERS_RETENUS is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO divers_retenus(
				ID_CONTRAT,
				ID_SALAIRE,
				MONTANT_DIVERS_RETENUS,
				DATE_DIVERS_RETENUS,
				REMARQUE_DIVERS_RETENUS
			)
			VALUES(
				$idContrat,
				$idSalaire,
				'$montant_Divers_Retenus',
				".$date_Divers_Retenus.",
				'$remarque_Divers_Retenus'
			)";
		} else {
			// UPDATE WHERE ID_DIVERS_RETENUS is
			$this -> _query = "UPDATE
				divers_retenus
			SET
				ID_CONTRAT = $idContrat,
				ID_SALAIRE = $idSalaire,
				MONTANT_DIVERS_RETENUS = '$montant_Divers_Retenus',
				DATE_DIVERS_RETENUS = ".$date_Divers_Retenus.",
				REMARQUE_DIVERS_RETENUS = '$remarque_Divers_Retenus'
			WHERE
				ID_DIVERS_RETENUS = $idDiversRetenus";
		}
		
		return $this -> update($idDiversRetenus);
	}

	function getDepassementList($where)	{
		$this -> _query = "SELECT
			h.ID_APPEL,
    		h.ID_CONTRAT,
    		h.ID_SALAIRE,
    		h.DEPASSEMENT,
    		h.REMARQUE_DEPASSEMENT,
			c.MATRICULE,
			s.MOIS_SALAIRE,
			s.ANNEE_SALAIRE,
			p.NOM,
			p.PRENOM
		FROM
			depassement h
		INNER JOIN contrat c ON
			h.ID_CONTRAT = c.ID_CONTRAT
		INNER JOIN salaire s ON
			h.ID_SALAIRE = s.ID_SALAIRE
		INNER JOIN personnel p ON 
			c.ID_EMPLOYE = p.ID_EMPLOYE
		$where
		ORDER BY h.ID_SALAIRE DESC";
		
		return $this -> select();
	}

	function saveOrDepassement($idAppel, $idContrat, $idSalaire, $depassement, $remarqueDepassement) {
		$remarque_Depassement = $this -> specialCharacters($remarqueDepassement);
		$depassements = $this -> IsNullOrEmptyString($depassement) > 0 ? 0 : $depassement;
		
		if ($this -> IsNullOrEmptyString($idAppel) > 0) {
			// ID_APPEL is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO depassement(
				ID_CONTRAT,
				ID_SALAIRE,
				DEPASSEMENT,
				REMARQUE_DEPASSEMENT
			)
			VALUES(
				$idContrat,
				$idSalaire,
				$depassements,
				'$remarque_Depassement'
			)";
		} else {
			// UPDATE WHERE ID_APPEL is
			$this -> _query = "UPDATE
				depassement
			SET
				ID_CONTRAT = $idContrat,
				ID_SALAIRE = $idSalaire,
				DEPASSEMENT = $depassement,
				REMARQUE_DEPASSEMENT = '$remarqueDepassement'
			WHERE
				ID_APPEL = $idAppel";
		}
		
		return $this -> update($idAppel);
	}

	function getAvanceList($where) {
		$this -> _query = "SELECT
			h.ID_AVANCE,
			h.ID_CONTRAT,
			h.MAX_AUTORISE,
			h.MAX_DEDUCTION_POSSIBLE,
			h.MONTANT_AVANCE,
			h.DATE_RECEPTION,
			h.DATE_DEBUT_REMBOURSEMENT,
			h.DATE_FIN_REMBOURSEMENT,
			h.NOMBRE_TRANCHE,
			h.DEDUCTION_MENSUELLE,
			h.RAISON,
			h.EN_COURS,
			c.MATRICULE,
			p.NOM,
			p.PRENOM
		FROM
			avance h
		INNER JOIN contrat c ON
			h.ID_CONTRAT = c.ID_CONTRAT
		INNER JOIN personnel p ON 
			c.ID_EMPLOYE = p.ID_EMPLOYE
		$where
		ORDER BY h.DATE_RECEPTION ASC";
		
		return $this -> select();
	}

	function getLastAvance($idContrat) {
		$this -> _query = "SELECT
			ID_AVANCE,
			ID_CONTRAT,
			DATE_FIN_REMBOURSEMENT
		FROM
			avance AS a
		WHERE
			DATE_FIN_REMBOURSEMENT =(
			SELECT
				MAX(DATE_FIN_REMBOURSEMENT)
			FROM
				avance AS b
			WHERE
				a.ID_CONTRAT = b.ID_CONTRAT
		) AND a.ID_CONTRAT = $idContrat";

		return $this -> select();
	}

	function saveOrUpdateAvance($idAvance, $idContrat, $maxAutorise, $maxDeductionPossible, $montantAvance, $dateReception, $dateDebutRemboursement, $dateFinRemboursement, $nombreTranche, $deductionMensuelle, $raison, $enCours) {
		$raison = $this -> specialCharacters($raison);
		
		if ($this -> IsNullOrEmptyString($idAvance) > 0) {
			// ID_AVANCE is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO avance(
				ID_CONTRAT,
				MAX_AUTORISE,
				MAX_DEDUCTION_POSSIBLE,
				MONTANT_AVANCE,
				DATE_RECEPTION,
				DATE_DEBUT_REMBOURSEMENT,
				DATE_FIN_REMBOURSEMENT,
				NOMBRE_TRANCHE,
				DEDUCTION_MENSUELLE,
				RAISON,
				EN_COURS
			)
			VALUES(
				$idContrat,
				'$maxAutorise',
				'$maxDeductionPossible',
				'$montantAvance',
				'$dateReception',
				'$dateDebutRemboursement',
				'$dateFinRemboursement',
				'$nombreTranche',
				'$deductionMensuelle',
				'$raison',
				$enCours
			)";
		} else {
			// UPDATE WHERE ID_AVANCE is
			$this -> _query = "UPDATE
				avance
			SET
				ID_CONTRAT = $idContrat,
				MAX_AUTORISE = '$maxAutorise',
				MAX_DEDUCTION_POSSIBLE = '$maxDeductionPossible',
				MONTANT_AVANCE = '$montantAvance',
				DATE_RECEPTION = '$dateReception',
				DATE_DEBUT_REMBOURSEMENT = '$dateDebutRemboursement',
				DATE_FIN_REMBOURSEMENT = '$dateFinRemboursement',
				NOMBRE_TRANCHE = '$nombreTranche',
				DEDUCTION_MENSUELLE = '$deductionMensuelle',
				RAISON = '$raison',
				EN_COURS = $enCours
			WHERE
				ID_AVANCE = '$idAvance'";
		}
		
		return $this -> update($idAvance);
	}

	function getRemboursement($idAvance) {
		$this -> _query = "SELECT
			ID_DETAILS_AVANCE,
			ID_AVANCE,
			ID_SALAIRE,
			MONTANT_REMBOURSEMENT
		FROM
			details_avance
		WHERE
			ID_AVANCE = $idAvance";
		
		return $this -> select();
	}

	function getSoldeConge($where) {
		$this -> _query = "SELECT
			h.ID_RELIQUAT,
			h.ID_CONTRAT,
			h.RELIQUAT,
			h.DATE_RELEVE
		FROM
			reliquat_conge h
		$where";

		return $this -> select();
	}

	function insertIntoHistoriqueSalaire($data) {
		$this -> _query = "INSERT INTO historique_salaire(
			ID_SALAIRE,
			ID_CONTRAT,
			MATRICULE,
			ID_POSTE,
			ID_CATEGORIE_PROFESSIONNELLE,
			DATE_EMBAUCHE,
			DATE_PRISE_POSTE,
			SB_CONCLU,
			SB_GRILLE,
			ID_AUGM_GENERALE,
			ID_AUGM_INFLATION,
			SALAIRE_BASE,
			SALAIRE_SUPPLEMENTAIRE,
			S_NON_PERCU,
			PRIME_EXCEPTIONNEL,
			ANOMALIE,
			DATE_DEBUT_INTERIM,
			DATE_FIN_INTERIM,
			MONTANT_INTERIM,
			DUREE_VALIDE_INTERIM,
			BASE_PAR_HEURE,
			ID_HS_INF_HUIT_HEURES,
			ID_HS_SUP_HUIT_HEURES,
			ID_HS_NUIT_HABITUELLE,
			ID_HS_NUIT_OCCASIONNELLE,
			ID_HS_DIMANCHE,
			ID_HS_FERIE,
			MONTANT_HS,
			SBRUT_AV_MATERNITE,
			MONTANT_H_MINUS,
			DEBUT_MATERNITE,
			FIN_MATERNITE,
			DUREE_MATERNITE_CE_MOIS,
			TAUX_MATERNITE,
			DEDUCTION_MATERNITE,
			SBRUT_FINAL,
			ID_CNAPS,
			CNAPS_BASE,
			CNAPS_PRIME,
			ID_SMIE,
			NOM_SMIE,
			SMIE_PRIME,
			SOMME_CNAPS_SMIE,
			S_BRUT_AP_CNAPS_SMIE,
			S_BRUT_AP_CNAPS_SMIE_ARR,
			IRSA_AP_PLAFOND,
			S_IMPOSABLE,
			ID_IRSA,
			ID_PARAM_ABATTEMENT,
			ABATTEMENT,
			IRSA_NET,
			ID_PARAM_HOSPITALISATION,
			CAISSE_HOSPITALISATION,
			FRAIS_MEDICAUX,
			ID_HOPITAL,
			PARTICIPATION_MEDICALE,
			ID_AVANCE,
			DEDUCTION_AVANCE,
			DIVERS_RETENUS,
			DEPASSEMENT,
			CONGE_PRIS,
			SOLDE_CONGE,
			NET_A_PAYER,
			NB_ENFANT,
			TOTAL_HEURE_HS,
			S_BRUT_AP_INFLATION,
			S_BRUT_ANNEE_DERNIERE,
			ID_HS,
			CATEGORIE_REELLE,
			RAPPEL_BASE,
			RAPPEL_NET,
			REMARQUES
		)
		VALUES ".implode(',', $data);
		
		return $this -> updates();
	}

	function saveOrUpdateDetailsAvance($data) {
		$this -> _query = "INSERT INTO details_avance(
			ID_AVANCE,
			ID_SALAIRE,
			MONTANT_REMBOURSEMENT
		)
		VALUES ".implode(',', $data);
		
		return $this -> updates();
	}

	function updateSoldeConge($idContrat, $soldeConge, $dateClotureSalaire) {
		$this -> _query = "UPDATE
			reliquat_conge
		SET
			RELIQUAT = '$soldeConge',
			DATE_RELEVE = '$dateClotureSalaire'
		WHERE
			ID_CONTRAT = $idContrat";
		
		return $this -> update($idContrat);
	}

	function saveSoldeConge($idContrat, $soldeConge, $dateClotureSalaire) {
		$this -> _query = "INSERT INTO reliquat_conge(
			ID_CONTRAT,
			RELIQUAT,
			DATE_RELEVE
		)
		VALUES(
			$idContrat,
			'$soldeConge',
			'$dateClotureSalaire'
		)";
		
		return $this -> update($idContrat);
	}

	function updateCategorieProfessionnelle($idContrat, $idCategorieProfessionnelle, $ancienneteDebutJanvier, $categorieProfessionnelle) {
		$this -> _query = "UPDATE
			contrat
		SET
			ID_CATEGORIE = $idCategorieProfessionnelle,
			CATEGORIE_ANCIENNETE = '(".$categorieProfessionnelle.") [".$ancienneteDebutJanvier."-".($ancienneteDebutJanvier + 1)."[ans'
		WHERE
			ID_CONTRAT = $idContrat";
		
		return $this -> update($idContrat);
	}

	function getHistoriqueSalaire($where) {
		$this -> _query = "SELECT
			h.ID_CONTRAT,
			h.MATRICULE,
			p.NOM,
			p.PRENOM,
			po.TITRE_POSTE,
			ca.CATEGORIE_PROFESSIONNELLE,
			h.DATE_EMBAUCHE,
			h.DATE_PRISE_POSTE,
			h.SB_CONCLU,
			h.SB_GRILLE,
			h.SALAIRE_BASE,
			h.SALAIRE_SUPPLEMENTAIRE,
			h.S_NON_PERCU,
			h.PRIME_EXCEPTIONNEL,
			h.ANOMALIE,
			h.DATE_DEBUT_INTERIM,
			h.DATE_FIN_INTERIM,
			h.MONTANT_INTERIM,
			h.BASE_PAR_HEURE,
			h.MONTANT_HS,
			h.SBRUT_AV_MATERNITE,
			h.MONTANT_H_MINUS,
			h.DUREE_MATERNITE_CE_MOIS,
			h.DEDUCTION_MATERNITE,
			h.SBRUT_FINAL,
			h.CNAPS_BASE,
			h.CNAPS_PRIME,
			h.NOM_SMIE,
			h.SMIE_PRIME,
			h.SOMME_CNAPS_SMIE,
			h.S_BRUT_AP_CNAPS_SMIE,
			h.S_BRUT_AP_CNAPS_SMIE_ARR,
			h.IRSA_AP_PLAFOND,
			h.S_IMPOSABLE,
			h.ABATTEMENT,
			h.IRSA_NET,
			h.CAISSE_HOSPITALISATION,
			h.FRAIS_MEDICAUX,
			t.POURCENTAGE_EMPLOYE_HOPITAL,
			h.PARTICIPATION_MEDICALE,
			h.DEDUCTION_AVANCE,
			h.DIVERS_RETENUS,
			h.DEPASSEMENT,
			null, /* Charge */
			null, /* SalaireNet */
			null, /* Retenus */
			h.NET_A_PAYER,
			null, /* decallageAvance */
			h.CONGE_PRIS,
			h.SOLDE_CONGE,
			h.NB_ENFANT,
			h.ID_SALAIRE,
			h.ID_POSTE,
			h.ID_CATEGORIE_PROFESSIONNELLE,
			h.ID_AUGM_GENERALE,
			h.ID_AUGM_INFLATION,
			h.DUREE_VALIDE_INTERIM,
			h.ID_HS_INF_HUIT_HEURES,
			h.ID_HS_SUP_HUIT_HEURES,
			h.ID_HS_NUIT_HABITUELLE,
			h.ID_HS_NUIT_OCCASIONNELLE,
			h.ID_HS_DIMANCHE,
			h.ID_HS_FERIE,
			h.DEBUT_MATERNITE,
			h.FIN_MATERNITE,
			h.TAUX_MATERNITE,
			h.ID_CNAPS,
			h.ID_SMIE,
			h.ID_IRSA,
			h.ID_PARAM_ABATTEMENT,
			h.ID_PARAM_HOSPITALISATION,
			h.ID_HOPITAL,
			h.ID_AVANCE,
			h.ID_HISTORIQUE_SALAIRE,
			h.S_BRUT_AP_INFLATION,
			ct.ABREVIATION_CENTRE,
			h.TOTAL_HEURE_HS,
			h.S_BRUT_ANNEE_DERNIERE,
			ct.ID_CENTRE,
			hs.INF_HUIT_HEURES,
			hs.SUP_HUIT_HEURES,
			hs.NUIT_HABITUELLE,
			hs.NUIT_OCCASIONNELLE,
			hs.DIMANCHE,
			hs.FERIE,
			sm.DEDUCTION_EMPLOYE,
			av.MONTANT_AVANCE,
			av.DATE_RECEPTION,
			av.DATE_DEBUT_REMBOURSEMENT,
            g.remboursement,
			h.CATEGORIE_REELLE,
			h.RAPPEL_BASE,
			h.RAPPEL_NET,
			h.REMARQUES,
			p.ID_EMPLOYE
		FROM
			historique_salaire h
		LEFT JOIN salaire s on h.ID_SALAIRE = s.ID_SALAIRE
		LEFT JOIN contrat c ON h.ID_CONTRAT = c.ID_CONTRAT
		LEFT JOIN poste po ON h.ID_POSTE = po.ID_POSTE
		LEFT JOIN categorie ca ON h.ID_CATEGORIE_PROFESSIONNELLE = ca.ID_CATEGORIE
		LEFT JOIN personnel p ON c.ID_EMPLOYE = p.ID_EMPLOYE
		LEFT JOIN hopital t ON h.ID_HOPITAL = t.ID_HOPITAL
		LEFT JOIN centre ct ON c.ID_CENTRE = ct.ID_CENTRE
		LEFT JOIN heures_supplementaires hs ON h.ID_HS = hs.ID_HEURE_SUPPLEMENTAIRE
		LEFT JOIN smie sm ON h.ID_SMIE = sm.ID_SMIE
		LEFT JOIN avance av ON h.ID_AVANCE = av.ID_AVANCE
		LEFT JOIN (
			SELECT ID_AVANCE,
				SUM(MONTANT_REMBOURSEMENT) AS remboursement
			FROM
				details_avance
			GROUP BY
				ID_AVANCE
		) g ON av.ID_AVANCE = g.ID_AVANCE
		$where
		ORDER BY p.NOM, p.PRENOM ASC";
		
		return $this -> select();
	}

	function insertIntoChangeCategorie($idSalaire, $idContrat, $ancienneteDebutJanvier, $idCategorieProfessionnelle, $categorieProfessionnelle) {
		$this -> _query = "INSERT INTO historique_categorie(
			ID_SALAIRE,
			ID_CONTRAT,
			ID_CATEGORIE,
			CATEGORIE,
			ANCIENNETE_DEBUT_JANVIER
		)
		VALUES(
			$idSalaire,
			$idContrat,
			$idCategorieProfessionnelle,
			'$categorieProfessionnelle',
			$ancienneteDebutJanvier
		)";
		
		return $this -> update($idContrat);
	}

	function getChangeCategorieListe($where) {
		$this -> _query = "SELECT 
			h.ID_SALAIRE,
			h.ID_CONTRAT,
			c.MATRICULE,
			p.NOM,
			p.PRENOM,
			h.ANCIENNETE_DEBUT_JANVIER,
			ca.CATEGORIE_PROFESSIONNELLE,
			s.MOIS_SALAIRE,
			s.ANNEE_SALAIRE
		FROM
			historique_categorie h
		LEFT JOIN salaire s on h.ID_SALAIRE = s.ID_SALAIRE
		LEFT JOIN contrat c ON h.ID_CONTRAT = c.ID_CONTRAT
		LEFT JOIN categorie ca ON h.ID_CATEGORIE = ca.ID_CATEGORIE
		LEFT JOIN personnel p ON c.ID_EMPLOYE = p.ID_EMPLOYE
		$where
		ORDER BY p.NOM, p.PRENOM ASC";
		
		return $this -> select();
	}

	function getRappelList($where) {
		$this -> _query = "SELECT
			h.ID_RAPPEL,
			h.ID_CONTRAT,
			h.ID_SALAIRE,
			h.MONTANT_RAPPEL,
			h.NOMBRE_MOIS,
			h.DATE_DEBUT_RAPPEL,
			h.DATE_FIN_RAPPEL,
			h.REMARQUE_RAPPEL,
			h.TYPE_RAPPEL,
			c.MATRICULE,
			p.NOM,
			p.PRENOM
		FROM
			rappel h
		INNER JOIN contrat c ON
			h.ID_CONTRAT = c.ID_CONTRAT
		INNER JOIN personnel p ON 
			c.ID_EMPLOYE = p.ID_EMPLOYE
		$where
		ORDER BY h.DATE_DEBUT_RAPPEL ASC";
		
		return $this -> select();
	}

	function saveOrUpdateRappel($idRappel, $idContrat, $idSalaire, $montantRappel, $nombreMois, $dateDebutRappel, $dateFinRappel, $remarqueRappel, $typeRappel) {
		$remarqueRappel = $this -> specialCharacters($remarqueRappel);
		
		if ($this -> IsNullOrEmptyString($idRappel) > 0) {
			// ID_RAPPEL is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO rappel(
				ID_CONTRAT,
				ID_SALAIRE,
				MONTANT_RAPPEL,
				NOMBRE_MOIS,
				DATE_DEBUT_RAPPEL,
				DATE_FIN_RAPPEL,
				REMARQUE_RAPPEL,
				TYPE_RAPPEL
			)
			VALUES(
				$idContrat,
				$idSalaire,
				'$montantRappel',
				$nombreMois,
				'$dateDebutRappel',
				'$dateFinRappel',
				'$remarqueRappel',
				$typeRappel
			)";
		} else {
			// UPDATE WHERE ID_RAPPEL is
			$this -> _query = "UPDATE
				rappel
			SET
				ID_CONTRAT = $idContrat,
				ID_SALAIRE = $idSalaire,
				MONTANT_RAPPEL = '$montantRappel',
				NOMBRE_MOIS = $nombreMois,
				DATE_DEBUT_RAPPEL = '$dateDebutRappel',
				DATE_FIN_RAPPEL = '$dateFinRappel',
				REMARQUE_RAPPEL = '$remarqueRappel',
				TYPE_RAPPEL = $typeRappel
			WHERE
				ID_RAPPEL = $idRappel";
		}
		
		return $this -> update($idRappel);
	}

	function getPermissionSolde($where) {
		$this -> _query = "SELECT
			h.ID_CONTRAT,
			SUM(h.NB_JOURS)
		FROM
			absence h
		$where
		GROUP BY
			h.ID_CONTRAT";
		
		return $this -> select();
	}

	function getFormateurList($where) {
		$this -> _query = "SELECT
			ID_FORMATEUR,
			NOM_FORMATEUR,
			PRENOM_FORMATEUR,
			TITRE_FONCTION_FORMATEUR,
			INSTITUTION,
			EMAIL_FORMATEUR,
			TELEPHONE_FORMATEUR,
			ADRESSE_COMPLETE_FORMATEUR
		FROM
			formateur
		$where
		ORDER BY
			NOM_FORMATEUR ASC";

		return $this -> select();
	}

	function saveOrUpdateFormateur($idFormateur, $nomFormateur, $prenomFormateur, $titreFonctionFormateur, $institution, $emailFormateur, $telephoneFormateur, $adresseCompleteFormateur) {
		$nomFormateur = $this -> specialCharacters($nomFormateur);
		$prenomFormateur = $this -> specialCharacters($prenomFormateur);
		$titreFonctionFormateur = $this -> specialCharacters($titreFonctionFormateur);
		$institution = $this -> specialCharacters($institution);
		$telephoneFormateur = $this -> specialCharacters($telephoneFormateur);
		$adresseCompleteFormateur = $this -> specialCharacters($adresseCompleteFormateur);
		
		if ($this -> IsNullOrEmptyString($idFormateur) > 0) {
			// ID_FORMATEUR is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO formateur(
				NOM_FORMATEUR,
				PRENOM_FORMATEUR,
				TITRE_FONCTION_FORMATEUR,
				INSTITUTION,
				EMAIL_FORMATEUR,
				TELEPHONE_FORMATEUR,
				ADRESSE_COMPLETE_FORMATEUR
			)
			VALUES(
				'$nomFormateur',
				'$prenomFormateur',
				'$titreFonctionFormateur',
				'$institution',
				'$emailFormateur',
				'$telephoneFormateur',
				'$adresseCompleteFormateur'
			)";
		} else {
			// UPDATE WHERE ID_FORMATEUR is
			$this -> _query = "UPDATE
				formateur
			SET
				NOM_FORMATEUR = '$nomFormateur',
				PRENOM_FORMATEUR = '$prenomFormateur',
				TITRE_FONCTION_FORMATEUR = '$titreFonctionFormateur',
				INSTITUTION = '$institution',
				EMAIL_FORMATEUR = '$emailFormateur',
				TELEPHONE_FORMATEUR = '$telephoneFormateur',
				ADRESSE_COMPLETE_FORMATEUR = '$adresseCompleteFormateur'
			WHERE
				ID_FORMATEUR = $idFormateur";
		}
		
		return $this -> update($idFormateur);
	}

	function getCatalogueList($where) {
		$this -> _query = "SELECT
			u.ID_SOUS_THEME,
			u.ID_FORMATEUR,
			u.TITRE_SOUS_THEME,
			u.OBJECTIFS_SOUS_THEME,
			c.NOM_FORMATEUR,
			c.PRENOM_FORMATEUR,
			c.TITRE_FONCTION_FORMATEUR,
			c.INSTITUTION,
			c.EMAIL_FORMATEUR,
			c.TELEPHONE_FORMATEUR,
			c.ADRESSE_COMPLETE_FORMATEUR
		FROM
			sous_theme u
		INNER JOIN formateur c ON
			u.ID_FORMATEUR = c.ID_FORMATEUR
		$where
		ORDER BY
			u.TITRE_SOUS_THEME ASC";

		return $this -> select();
	}

	function saveOrUpdateCatalogue($idSousTheme, $idFormateur, $titreSousTheme, $objectifsSousTheme) {
		$titreSousTheme = $this -> specialCharacters($titreSousTheme);
		$objectifsSousTheme = $this -> specialCharacters($objectifsSousTheme);
		
		if ($this -> IsNullOrEmptyString($idSousTheme) > 0) {
			// ID_SOUS_THEME is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO sous_theme(
				ID_FORMATEUR,
				TITRE_SOUS_THEME,
				OBJECTIFS_SOUS_THEME
			)
			VALUES(
				$idFormateur,
				'$titreSousTheme',
				'$objectifsSousTheme'
			)";
		} else {
			// UPDATE WHERE ID_SOUS_THEME is
			$this -> _query = "UPDATE
				sous_theme
			SET
				ID_FORMATEUR = $idFormateur,
				TITRE_SOUS_THEME = '$titreSousTheme',
				OBJECTIFS_SOUS_THEME = '$objectifsSousTheme'
			WHERE
				ID_SOUS_THEME = $idSousTheme";
		}
		
		return $this -> update($idSousTheme);
	}

	function getFormationListe($where) {
		$this -> _query = "SELECT
			u.ID_SESSION_FORMATION,
			u.THEME_FORMATION,
			u.OBJECTIFS_FORMATION,
			u.DATE_CREATION_SESSION,
			u.DATE_DEBUT_FORMATION,
			u.DATE_FIN_FORMATION,
			u.DOMAINE_FORMATION,
			u.ADRESSE_SEANCE,
			u.TYPE_FORMATION,
			u.NIVEAU_CIBLE,
			d.ID_SOUS_THEME,
			v.ID_SOUS_THEME,
			v.TITRE_SOUS_THEME,
			m.NOM_DOMAINE_FORMATION,
			p.NOM_TYPE_FORMATION
		FROM
			session_formation u
		INNER JOIN details_formation d ON
			u.ID_SESSION_FORMATION = d.ID_SESSION_FORMATION
		INNER JOIN (
			SELECT
				sous_theme.ID_SOUS_THEME,
				sous_theme.TITRE_SOUS_THEME,
				details_formation.ID_SESSION_FORMATION
			FROM
				sous_theme
			LEFT JOIN details_formation ON FIND_IN_SET(
					sous_theme.ID_SOUS_THEME,
					details_formation.ID_SOUS_THEME
				)
		) v ON
			d.ID_SESSION_FORMATION = v.ID_SESSION_FORMATION
		INNER JOIN domaine_formation m ON 
			u.DOMAINE_FORMATION = m.DOMAINE_FORMATION
		INNER JOIN type_formation p ON 
			u.TYPE_FORMATION = p.TYPE_FORMATION
		$where
		ORDER BY
			u.THEME_FORMATION ASC";

		return $this -> select();
	}

	function saveOrUpdateSessionFormation($idSessionFormation, $themeFormation, $objectifsFormation, $dateCreationSession, $dateDebutFormation, $dateFinFormation, $domaineFormation, $adresseSeance, $typeFormation, $niveauCible) {
		$themeFormation = $this -> specialCharacters($themeFormation);
		$objectifsFormation = $this -> specialCharacters($objectifsFormation);
		$adresseSeance = $this -> specialCharacters($adresseSeance);
		$adresseSeance = $this -> specialCharacters($adresseSeance);
		$niveauCible = $this -> specialCharacters($niveauCible);
		
		if ($this -> IsNullOrEmptyString($idSessionFormation) > 0) {
			// ID_SESSION_FORMATION is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO session_formation(
				THEME_FORMATION,
				OBJECTIFS_FORMATION,
				DATE_CREATION_SESSION,
				DATE_DEBUT_FORMATION,
				DATE_FIN_FORMATION,
				DOMAINE_FORMATION,
				ADRESSE_SEANCE,
				TYPE_FORMATION,
				NIVEAU_CIBLE
			)
			VALUES(
				'$themeFormation',
				'$objectifsFormation',
				'$dateCreationSession',
				'$dateDebutFormation',
				'$dateFinFormation',
				'$domaineFormation',
				'$adresseSeance',
				$typeFormation,
				'$niveauCible'
			)";
		} else {
			// UPDATE WHERE ID_SESSION_FORMATION is
			$this -> _query = "UPDATE
				session_formation
			SET
				THEME_FORMATION = '$themeFormation',
				OBJECTIFS_FORMATION = '$objectifsFormation',
				DATE_CREATION_SESSION = '$dateCreationSession',
				DATE_DEBUT_FORMATION = '$dateDebutFormation',
				DATE_FIN_FORMATION = '$dateFinFormation',
				DOMAINE_FORMATION = '$domaineFormation',
				ADRESSE_SEANCE = '$adresseSeance',
				TYPE_FORMATION = $typeFormation,
				NIVEAU_CIBLE = '$niveauCible'
			WHERE
				ID_SESSION_FORMATION = $idSessionFormation";
		}
		
		return $this -> update($idSessionFormation);
	}

	function getDetailsFormationListe($where) {
		$this -> _query = "SELECT
			ID_DETAILS_FORMATION,
			ID_SESSION_FORMATION,
			ID_SOUS_THEME
		FROM
			details_formation
			$where";

		return $this -> select();
	}

	function SaveOrUpdateDetailsFormation($idSessionFormation, $idSousTheme) {
		$whereFormationSessionId = "WHERE ID_SESSION_FORMATION = $idSessionFormation";
		$LastIdSessionFormation = $this -> getDetailsFormationListe($whereFormationSessionId);

		if (!empty($LastIdSessionFormation[0])) {
			// UPDATE WHERE ID_SESSION_FORMATION is
			$this -> _query = "UPDATE
				details_formation
			SET
				ID_SOUS_THEME = '$idSousTheme'
			WHERE
				ID_SESSION_FORMATION = $idSessionFormation";
		} else {
			// ID_SESSION_FORMATION is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO details_formation(
				ID_SESSION_FORMATION,
				ID_SOUS_THEME
			)
			VALUES(
				$idSessionFormation,
				'$idSousTheme'
			)";
		}
		
		return $this -> update($idSessionFormation);
	}

	function getTypeFormationList() {
		$this -> _query = "SELECT
			TYPE_FORMATION,
			NOM_TYPE_FORMATION
		FROM
			type_formation
		WHERE
			1";

		return $this -> select();
	}

	function getDomaineFormationList() {
		$this -> _query = "SELECT
			DOMAINE_FORMATION,
			NOM_DOMAINE_FORMATION
		FROM
			domaine_formation
		WHERE
			1";

		return $this -> select();
	}

	function getSimpleFormationListe($where) {
		$this -> _query = "SELECT
			u.ID_SESSION_FORMATION,
			u.THEME_FORMATION,
			u.OBJECTIFS_FORMATION,
			u.DATE_CREATION_SESSION,
			u.DATE_DEBUT_FORMATION,
			u.DATE_FIN_FORMATION,
			u.DOMAINE_FORMATION,
			u.ADRESSE_SEANCE,
			u.TYPE_FORMATION,
			u.NIVEAU_CIBLE,
			m.NOM_DOMAINE_FORMATION,
			p.NOM_TYPE_FORMATION,
			i.nbApprenant,
			e.nbEvaluation
		FROM
			session_formation u
		INNER JOIN domaine_formation m ON
			u.DOMAINE_FORMATION = m.DOMAINE_FORMATION
		INNER JOIN type_formation p ON
			u.TYPE_FORMATION = p.TYPE_FORMATION
		LEFT JOIN(
			SELECT
				ID_SESSION_FORMATION,
				COUNT(ID_APPRENANT) AS nbApprenant
			FROM
				inscription
			GROUP BY
				ID_SESSION_FORMATION
		) i
		ON
			u.ID_SESSION_FORMATION = i.ID_SESSION_FORMATION
		LEFT JOIN(
			SELECT
				ID_SESSION_FORMATION,
				COUNT(ID_APPRENANT) AS nbEvaluation
			FROM
				evaluation_formation
			GROUP BY
				ID_SESSION_FORMATION
		) e
		ON
			u.ID_SESSION_FORMATION = e.ID_SESSION_FORMATION
		$where
		ORDER BY
			u.ID_SESSION_FORMATION ASC";

		return $this -> select();
	}

	function saveOrUpdateApprenant($idApprenant, $nomApprenant, $prenomApprenant, $dateNaissanceApprenant, $sexeApprenant, $telephoneApprenant, $adresseCompleteApprenant, $niveau, $typeRelationAdes, $idEmploye) {
		$nomApprenant = $this -> specialCharacters($nomApprenant);
		$prenomApprenant = $this -> specialCharacters($prenomApprenant);
		$adresseCompleteApprenant = $this -> specialCharacters($adresseCompleteApprenant);
		$niveau = $this -> specialCharacters($niveau);
		$idEmploye = ($this -> IsNullOrEmptyString($idEmploye) > 0) ? 'NULL' : $idEmploye;

		// Vérifie dans la base s'il s'est déjà inscrit dans la DDB
		$CheckDoublonApprenant = $this -> verifyApprenant($nomApprenant, $prenomApprenant);

		if ($this -> IsNullOrEmptyString($idApprenant) > 0) {
			if (!empty($CheckDoublonApprenant[0]) > 0) {
				$idApprenantActuel = $CheckDoublonApprenant[0][0];
			} else $idApprenantActuel = null;
		} else {
			$idApprenantActuel = $idApprenant;
		}
		
		if (empty($idApprenantActuel) > 0) {
			// ID_APPRENANT is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO apprenant(
				NOM_APPRENANT,
				PRENOM_APPRENANT,
				DATE_NAISSANCE_APPRENANT,
				SEXE_APPRENANT,
				TELEPHONE_APPRENANT,
				ADRESSE_COMPLETE_APPRENANT,
				NIVEAU,
				TYPE_RELATION_ADES,
				ID_EMPLOYE
			)
			VALUES(
				'$nomApprenant',
				'$prenomApprenant',
				'$dateNaissanceApprenant',
				'$sexeApprenant',
				'$telephoneApprenant',
				'$adresseCompleteApprenant',
				'$niveau',
				$typeRelationAdes,
				$idEmploye
			)";
		} else {
			// UPDATE WHERE ID_APPRENANT is
			$this -> _query = "UPDATE
				apprenant
			SET
				NOM_APPRENANT = '$nomApprenant',
				PRENOM_APPRENANT = '$prenomApprenant',
				DATE_NAISSANCE_APPRENANT = '$dateNaissanceApprenant',
				SEXE_APPRENANT = '$sexeApprenant',
				TELEPHONE_APPRENANT = '$telephoneApprenant',
				ADRESSE_COMPLETE_APPRENANT = '$adresseCompleteApprenant',
				NIVEAU = '$niveau',
				TYPE_RELATION_ADES = $typeRelationAdes,
				ID_EMPLOYE = $idEmploye
			WHERE
				ID_APPRENANT = $idApprenantActuel";
		}
		
		return $this -> update($idApprenantActuel);
	}

	function saveOrUpdateInscription($data) {
		$this -> _query = "INSERT INTO inscription(
			ID_APPRENANT,
			ID_SESSION_FORMATION
		)
		VALUES ".implode(',', $data);
		
		return $this -> updates();
	}

	function checkDoublonByNamesApprenant() {
		$this -> _query = "SELECT
			MAX(ID_APPRENANT),
			NOM_APPRENANT,
			PRENOM_APPRENANT,
			COUNT(*) AS NumDuplicates
		FROM
			apprenant
		GROUP BY
		REPLACE(LOWER(NOM_APPRENANT), ' ',''),
		REPLACE(LOWER(PRENOM_APPRENANT), ' ', '')
		HAVING
			NumDuplicates > 1";

		return $this -> select();
	}

	function verifyApprenant($nomApprenant, $prenomApprenant) {
		$this -> _query = "SELECT
			ID_APPRENANT,
			NOM_APPRENANT,
			PRENOM_APPRENANT
		FROM
			apprenant
		WHERE
		REPLACE(LOWER(NOM_APPRENANT), ' ', '') = REPLACE(LOWER('$nomApprenant'), ' ', '') AND 
		REPLACE(LOWER(PRENOM_APPRENANT), ' ', '') =  REPLACE (LOWER('$prenomApprenant'), ' ', '')";

		return $this -> select();
	}

	function getApprenantListDistinct($where) {
		$this -> _query = "SELECT DISTINCT
			i.ID_INSCRIPTION,
			i.ID_APPRENANT,
			i.ID_SESSION_FORMATION,
			a.NOM_APPRENANT,
			a.PRENOM_APPRENANT,
			a.DATE_NAISSANCE_APPRENANT,
			a.SEXE_APPRENANT,
			a.TELEPHONE_APPRENANT,
			a.ADRESSE_COMPLETE_APPRENANT,
			a.NIVEAU,
			a.TYPE_RELATION_ADES,
			a.ID_EMPLOYE,
			u.THEME_FORMATION,
            p.NOM,
            p.PRENOM
		FROM
			inscription i
		INNER JOIN apprenant a ON
			i.ID_APPRENANT = a.ID_APPRENANT
		INNER JOIN session_formation u ON
			i.ID_SESSION_FORMATION = u.ID_SESSION_FORMATION
		LEFT JOIN personnel p ON
        	a.ID_EMPLOYE = p.ID_EMPLOYE
		$where
		GROUP BY
    		i.ID_APPRENANT
		ORDER BY
			i.ID_SESSION_FORMATION,
			a.NOM_APPRENANT ASC";

		return $this -> select();
	}

	function getApprenantList($where) {
		$this -> _query = "SELECT
			i.ID_INSCRIPTION,
			i.ID_APPRENANT,
			i.ID_SESSION_FORMATION,
			a.NOM_APPRENANT,
			a.PRENOM_APPRENANT,
			a.DATE_NAISSANCE_APPRENANT,
			a.SEXE_APPRENANT,
			a.TELEPHONE_APPRENANT,
			a.ADRESSE_COMPLETE_APPRENANT,
			a.NIVEAU,
			a.TYPE_RELATION_ADES,
			a.ID_EMPLOYE,
			u.THEME_FORMATION,
            p.NOM,
            p.PRENOM
		FROM
			inscription i
		INNER JOIN apprenant a ON
			i.ID_APPRENANT = a.ID_APPRENANT
		INNER JOIN session_formation u ON
			i.ID_SESSION_FORMATION = u.ID_SESSION_FORMATION
		LEFT JOIN personnel p ON
        	a.ID_EMPLOYE = p.ID_EMPLOYE
		$where
		ORDER BY
			i.ID_SESSION_FORMATION,
			a.NOM_APPRENANT ASC";
		
		return $this -> select();
	}

	function verifyInscription($saveApprenant, $idSession) {
		$this -> _query = "SELECT
			COUNT(*)
		FROM
			inscription
		WHERE
			ID_APPRENANT = $saveApprenant AND ID_SESSION_FORMATION = $idSession";

		return $this -> select();
	}

	function saveOrUpdateEvaluation($idEvaluationFormation, $idApprenant, $idSessionFormation, $reponses) {
		if ($this -> IsNullOrEmptyString($idEvaluationFormation) > 0) {
			// ID_SESSION_FORMATION is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO evaluation_formation(
				ID_APPRENANT,
				ID_SESSION_FORMATION,
				REPONSES
			)
			VALUES(
				$idApprenant,
				$idSessionFormation,
				'$reponses'
			)";
		} else {
			// UPDATE WHERE ID_SESSION_FORMATION is
			$this -> _query = "UPDATE
				evaluation_formation
			SET
				ID_APPRENANT = $idApprenant,
				ID_SESSION_FORMATION = $idSessionFormation,
				REPONSES = '$reponses'
			WHERE
				ID_EVALUATION_FORMATION = $idEvaluationFormation";
		}
		
		return $this -> update($idEvaluationFormation);
	}

	function updateEvaluationId() {
		$this -> _query = "UPDATE
			evaluation_formation
		SET
			REPONSES = REPLACE(REPONSES, '\"idEvaluationFormation\":\"\",', CONCAT('\"idEvaluationFormation\":\"', ID_EVALUATION_FORMATION, '\",'))";
		return $this -> update(1);
	}

	function getResultatsEvaluationList($where) {
		$this -> _query = "SELECT
			u.REPONSES
		FROM
			evaluation_formation u
		$where";
		
		return $this -> select();
	}

	function verfiThemeDansSession($idSousTheme) {
		$this -> _query = "SELECT
			*
		FROM
			details_formation
		WHERE
			ID_SOUS_THEME LIKE '$idSousTheme,%' OR ID_SOUS_THEME LIKE '%,$idSousTheme' OR ID_SOUS_THEME = '$idSousTheme'";
		return $this -> select();
	}

	function rapportNombrePersonnel($where) {
		$this -> _query = "SELECT
			COUNT(*),
			p.SEXE
		FROM
			personnel p
		LEFT JOIN contrat c ON
			p.ID_EMPLOYE = c.ID_EMPLOYE
		WHERE
			$where
		GROUP BY
			p.SEXE
		";

		return $this -> select();
	}

	function rapportAgePersonnel($where) {
		$this -> _query = "SELECT
			YEAR(CURDATE()) - YEAR(p.DATE_NAISSANCE) AS age,
			p.SEXE,
			count(*) as nbParSexe
		FROM
			personnel p
		LEFT JOIN contrat c ON
			p.ID_EMPLOYE = c.ID_EMPLOYE
		WHERE $where
		GROUP BY age, p.SEXE
		ORDER BY age ASC";

		return $this -> select();
	}

	function rapportEnfantPersonnel($where) {
		$this -> _query = "SELECT
			age,
			SUM(IF(GENRE_ENFANT = 1, nbParSexe, 0)) AS homme,
			SUM(IF(GENRE_ENFANT = 2, nbParSexe, 0)) AS femme,
			(
				SUM(IF(GENRE_ENFANT = 1, nbParSexe, 0))
				+
				SUM(IF(GENRE_ENFANT = 2, nbParSexe, 0))
			) AS total
		FROM
			(SELECT
				GROUP_CONCAT(c.ID_CONTRAT),
				YEAR(CURDATE()) - YEAR(p.DATE_NAISSANCE_ENFANT) AS age,
				p.GENRE_ENFANT,
				COUNT(*) AS nbParSexe
				FROM
					enfant p
				LEFT JOIN contrat c ON
					p.ID_EMPLOYE = c.ID_EMPLOYE
				WHERE
					$where
				GROUP BY
					age,
					p.GENRE_ENFANT
				ORDER BY
					age ASC) EnfantQuery
			GROUP BY
				age
			ORDER BY
				age ASC";

		return $this -> select();
	}

	function rapportFraisScolarite($where) {
		$this -> _query = "SELECT
			s.ANNEE_SCOLAIRE,
			SUM(s.FRAIS_SCOLARITE),
			count(s.ID_ENFANT)
		FROM
			enfant e
		LEFT JOIN scolarite s ON
			e.ID_ENFANT = s.ID_ENFANT
		LEFT JOIN personnel p ON
			e.ID_EMPLOYE = p.ID_EMPLOYE
		LEFT JOIN contrat c ON
			p.ID_EMPLOYE = c.ID_EMPLOYE
		WHERE $where AND s.ANNEE_SCOLAIRE IS NOT NULL
		GROUP BY
			s.ANNEE_SCOLAIRE
		ORDER BY s.ANNEE_SCOLAIRE ASC";

		return $this -> select();
	}

	function rapportContratParCentre($where) {
		$this -> _query = "SELECT
			centreAffectation,
			SUM(IF(GENRE_PERSONNEL = 1, NbContrat, 0)) AS homme,
			SUM(IF(GENRE_PERSONNEL = 2, NbContrat, 0)) AS femme
		FROM
			(SELECT
				ct.NOM_CENTRE AS centreAffectation,
				p.SEXE AS GENRE_PERSONNEL,
				count(*) as NbContrat
			FROM contrat c
			LEFT JOIN personnel p ON c.ID_EMPLOYE = p.ID_EMPLOYE
			LEFT JOIN categorie ca ON c.ID_CATEGORIE = ca.ID_CATEGORIE
			LEFT JOIN centre ct ON c.ID_CENTRE = ct.ID_CENTRE
			WHERE $where
			GROUP BY centreAffectation, GENRE_PERSONNEL
			ORDER BY centreAffectation, GENRE_PERSONNEL ASC) ContratQuery
			GROUP BY
				centreAffectation
			ORDER BY
				centreAffectation ASC";

		return $this -> select();
	}

	function rapportContratParPoste($where) {
		$this -> _query = "SELECT
			posteAffectation,
			SUM(IF(GENRE_PERSONNEL = 1, NbContrat, 0)) AS homme,
			SUM(IF(GENRE_PERSONNEL = 2, NbContrat, 0)) AS femme
		FROM
			(SELECT
				po.TITRE_POSTE AS posteAffectation,
				p.SEXE AS GENRE_PERSONNEL,
				count(*) as NbContrat
			FROM contrat c
			LEFT JOIN personnel p ON c.ID_EMPLOYE = p.ID_EMPLOYE
			LEFT JOIN poste po ON c.ID_POSTE = po.ID_POSTE
			WHERE $where
			GROUP BY posteAffectation, GENRE_PERSONNEL
			ORDER BY posteAffectation, GENRE_PERSONNEL ASC) ContratQuery
			GROUP BY
				posteAffectation
			ORDER BY
				posteAffectation ASC";

		return $this -> select();
	}

	function rapportContratParAnciennete($where) {
		$this -> _query = "SELECT
			age,
			SUM(IF(GENRE_PERSONNEL = 1, NbContrat, 0)) AS homme,
			SUM(IF(GENRE_PERSONNEL = 2, NbContrat, 0)) AS femme
		FROM
			(SELECT
				YEAR(CURDATE()) - YEAR(c.DATE_EMBAUCHE) AS age,
				p.SEXE AS GENRE_PERSONNEL,
				COUNT(*) AS NbContrat
				FROM contrat c
				LEFT JOIN personnel p ON c.ID_EMPLOYE = p.ID_EMPLOYE
				WHERE
					$where
				GROUP BY
					age,
					p.SEXE
				ORDER BY
					age ASC) EnfantQuery
			GROUP BY
				age
			ORDER BY
				age ASC";

		return $this -> select();
	}

	function rapportInspection($whereAnnee) {
		$this -> _query = "SELECT
			CAT_PRO,
			homme,
			femme,
			NET_A_PAYER,
			CNAPS,
			SMIE,
			hommeEmbauche,
			femmeEmbauche,
			hommeDemission,
			femmeDemission,
			hommeLicenciement,
			femmeLicenciement,
			hommeChTec,
			femmeChTec
		FROM
			( /*Nombre de contrat actuel*/
			SELECT
				CAT_PRO,
				SUM(IF(GENRE_PERSONNEL = 1, NbContrat, 0)) AS homme,
				SUM(IF(GENRE_PERSONNEL = 2, NbContrat, 0)) AS femme
			FROM
				(
				SELECT
					p.SEXE AS GENRE_PERSONNEL,
					COUNT(c.ID_CONTRAT) AS NbContrat,
					ca.CATEGORIE_PROFESSIONNELLE AS CAT_PRO,
					ca.ID_CATEGORIE AS ID_CAT
				FROM
					contrat c
				LEFT JOIN personnel p ON c.ID_EMPLOYE = p.ID_EMPLOYE
				RIGHT JOIN categorie ca ON c.ID_CATEGORIE = ca.ID_CATEGORIE
				AND (YEAR(c.DATE_EMBAUCHE) <= '$whereAnnee' AND (c.ACTIF_CONTRAT = 1  OR YEAR(c.DATE_DEBAUCHE) = '$whereAnnee'))
				GROUP BY
					CAT_PRO,
					GENRE_PERSONNEL
			) ContratQuery
			GROUP BY
				CAT_PRO
			ORDER BY
				ID_CAT ASC
			) req
		LEFT JOIN( /*Nombre d'embauche*/
			SELECT
				CAT_PRO0,
				SUM(IF(GENRE_PERSONNEL0 = 1, NbContrat0, 0)) AS hommeEmbauche,
				SUM(IF(GENRE_PERSONNEL0 = 2, NbContrat0, 0)) AS femmeEmbauche
			FROM
				(
				SELECT
					p.SEXE AS GENRE_PERSONNEL0,
					COUNT(c.ID_CONTRAT) AS NbContrat0,
					ca.CATEGORIE_PROFESSIONNELLE AS CAT_PRO0,
					ca.ID_CATEGORIE AS ID_CAT0
				FROM
					contrat c
				LEFT JOIN personnel p ON
					c.ID_EMPLOYE = p.ID_EMPLOYE
				RIGHT JOIN categorie ca ON
					c.ID_CATEGORIE = ca.ID_CATEGORIE AND YEAR(c.DATE_EMBAUCHE) = '$whereAnnee'
				GROUP BY
					CAT_PRO0,
					GENRE_PERSONNEL0
			) ContratQuery0
			GROUP BY
				CAT_PRO0
			ORDER BY
			ID_CAT0 ASC
		) req0 ON req.CAT_PRO = req0.CAT_PRO0
		LEFT JOIN( /*Nombre de démission*/
			SELECT
				CAT_PRO1,
				SUM(IF(GENRE_PERSONNEL1 = 1, NbContrat1, 0)) AS hommeDemission,
				SUM(IF(GENRE_PERSONNEL1 = 2, NbContrat1, 0)) AS femmeDemission
			FROM
				(
				SELECT
					p.SEXE AS GENRE_PERSONNEL1,
					COUNT(c.ID_CONTRAT) AS NbContrat1,
					ca.CATEGORIE_PROFESSIONNELLE AS CAT_PRO1,
					ca.ID_CATEGORIE AS ID_CAT1
				FROM
					contrat c
				LEFT JOIN personnel p ON
					c.ID_EMPLOYE = p.ID_EMPLOYE
				RIGHT JOIN categorie ca ON
					c.ID_CATEGORIE = ca.ID_CATEGORIE AND YEAR(c.DATE_DEBAUCHE) = '$whereAnnee' AND c.MOTIF_RESILIATION_CONTRAT = 1
				GROUP BY
					CAT_PRO1,
					GENRE_PERSONNEL1
			) ContratQuery1
			GROUP BY
				CAT_PRO1
			ORDER BY
			ID_CAT1 ASC
		) req1 ON req.CAT_PRO = req1.CAT_PRO1
		LEFT JOIN( /*Nombre de licenciement*/
			SELECT
				CAT_PRO2,
				SUM(IF(GENRE_PERSONNEL2 = 1, NbContrat2, 0)) AS hommeLicenciement,
				SUM(IF(GENRE_PERSONNEL2 = 2, NbContrat2, 0)) AS femmeLicenciement
			FROM
				(
				SELECT
					p.SEXE AS GENRE_PERSONNEL2,
					COUNT(c.ID_CONTRAT) AS NbContrat2,
					ca.CATEGORIE_PROFESSIONNELLE AS CAT_PRO2,
					ca.ID_CATEGORIE AS ID_CAT2
				FROM
					contrat c
				LEFT JOIN personnel p ON
					c.ID_EMPLOYE = p.ID_EMPLOYE
				RIGHT JOIN categorie ca ON
					c.ID_CATEGORIE = ca.ID_CATEGORIE AND YEAR(c.DATE_DEBAUCHE) = '$whereAnnee' AND c.MOTIF_RESILIATION_CONTRAT = 2
				GROUP BY
					CAT_PRO2,
					GENRE_PERSONNEL2
			) ContratQuery2
			GROUP BY
				CAT_PRO2
			ORDER BY
			ID_CAT2 ASC
		) req2 ON req.CAT_PRO = req2.CAT_PRO2
		LEFT JOIN( /*Nombre de chômage technique*/
			SELECT
				CAT_PRO3,
				SUM(IF(GENRE_PERSONNEL3 = 1, NbContrat3, 0)) AS hommeChTec,
				SUM(IF(GENRE_PERSONNEL3 = 2, NbContrat3, 0)) AS femmeChTec
			FROM
				(
				SELECT
					p.SEXE AS GENRE_PERSONNEL3,
					COUNT(c.ID_CONTRAT) AS NbContrat3,
					ca.CATEGORIE_PROFESSIONNELLE AS CAT_PRO3,
					ca.ID_CATEGORIE AS ID_CAT3
				FROM
					contrat c
				LEFT JOIN personnel p ON
					c.ID_EMPLOYE = p.ID_EMPLOYE
				RIGHT JOIN categorie ca ON
					c.ID_CATEGORIE = ca.ID_CATEGORIE AND YEAR(c.DATE_DEBAUCHE) = '$whereAnnee' AND c.MOTIF_RESILIATION_CONTRAT = 3
				GROUP BY
					CAT_PRO3,
					GENRE_PERSONNEL3
			) ContratQuery3
			GROUP BY
				CAT_PRO3
			ORDER BY
			ID_CAT3 ASC
		) req3 ON req.CAT_PRO = req3.CAT_PRO3
		LEFT JOIN ( /*Net à payer, CNaPS, SMIE*/
			SELECT
				cat.CATEGORIE_PROFESSIONNELLE AS CAT_PRO4a,
				cat.ID_CATEGORIE AS ID_CATa4,
				reqq4.NET_A_PAYER,
				reqq4.CNAPS,
				reqq4.SMIE
			FROM categorie cat
			LEFT JOIN (
				SELECT
					ca.CATEGORIE_PROFESSIONNELLE AS CAT_PRO4,
					ca.ID_CATEGORIE AS ID_CAT4,
					SUM(h.NET_A_PAYER) AS NET_A_PAYER,
					SUM(h.CNAPS_PRIME) AS CNAPS,
					SUM(h.SMIE_PRIME) AS SMIE
				FROM
					categorie ca
				LEFT JOIN historique_salaire h ON
					h.ID_CATEGORIE_PROFESSIONNELLE = ca.ID_CATEGORIE
				LEFT JOIN salaire s ON
					s.ID_SALAIRE = h.ID_SALAIRE 
				WHERE s.ANNEE_SALAIRE = '$whereAnnee'
				GROUP BY
					CAT_PRO4
				ORDER BY
					ID_CAT4 ASC) reqq4 ON 
					cat.ID_CATEGORIE = reqq4.ID_CAT4
		) req4 ON req.CAT_PRO = req4.CAT_PRO4a";

		return $this -> select();
	}

	function rapportSalaire($whereAnnee) {
		$this -> _query = "SELECT
			cat.CATEGORIE_PROFESSIONNELLE AS CAT_PRO4a,
			cat.ID_CATEGORIE AS ID_CATa4,
			reqq4.SALAIRE_BASE,
			reqq4.SALAIRE_BRUT,
			reqq4.SBRUT_AV_MATERNITE,
			reqq4.CNAPS,
			reqq4.SMIE,
			reqq4.IRSA_NET,
			reqq4.NET_A_PAYER
		FROM categorie cat
		LEFT JOIN (
			SELECT
				ca.CATEGORIE_PROFESSIONNELLE AS CAT_PRO4,
				ca.ID_CATEGORIE AS ID_CAT4,
				SUM(h.SALAIRE_BASE) AS SALAIRE_BASE,
				SUM(h.S_BRUT_AP_INFLATION) AS SALAIRE_BRUT, /*S.Brut = S.Base + augmentations*/
				SUM(h.SBRUT_AV_MATERNITE) AS SBRUT_AV_MATERNITE, /*S.Brut2 = S.Brut + Primes exceptionnelles + HS + S.intérim*/
				SUM(h.CNAPS_PRIME) AS CNAPS,
				SUM(h.SMIE_PRIME) AS SMIE,
				SUM(h.IRSA_NET) AS IRSA_NET,
				SUM(h.NET_A_PAYER) AS NET_A_PAYER
			FROM
				categorie ca
			LEFT JOIN historique_salaire h ON
				h.ID_CATEGORIE_PROFESSIONNELLE = ca.ID_CATEGORIE
			LEFT JOIN salaire s ON
				s.ID_SALAIRE = h.ID_SALAIRE 
			WHERE s.ANNEE_SALAIRE = '$whereAnnee'
			GROUP BY
				CAT_PRO4
			ORDER BY
				ID_CAT4 ASC) reqq4 ON 
				cat.ID_CATEGORIE = reqq4.ID_CAT4";

		return $this -> select();
	}

	function callFunctionEvolution() {
		$this -> _query = "SELECT
		get_effectif_ades()";

		return $this -> select();
	}

	function rapportEvolutionEffectif() {
		$this -> _query = "SELECT
			requeteEnsemble.ANNEE,
			SUM(IF (GENREENTRANT = 1, ENTRANT, 0)) AS HOMMEENTRANT,
			SUM(IF (GENREENTRANT = 2, ENTRANT, 0)) AS FEMMEENTRANT,
			SUM(IF (GENREDEBAUCHE = 1, DEBAUCHE, 0)) AS HOMMEDEBAUCHE,
			SUM(IF (GENREDEBAUCHE = 2, DEBAUCHE, 0)) AS FEMMEDEBAUCHE,
			c.totalJoining AS TOTALENTRANT,
			IF (d.totalResing is null, 0, d.totalResing) AS TOTALDEBAUCHE,
			(c.totalJoining - IF (d.totalResing is null, 0, d.totalResing)) AS TOTALEFFECTIF
		FROM (
			SELECT
				a.annee AS ANNEE,
				a.newJoining AS ENTRANT,
				a.genrePersonnel AS GENREENTRANT,
				b.newResing AS DEBAUCHE,
				b.genrePersonnelDebauche AS GENREDEBAUCHE
			FROM
				get_effectif_details a
			LEFT JOIN get_debauche_details b ON
				a.annee = b.anneeDebauche AND a.genrePersonnel = b.genrePersonnelDebauche
			GROUP BY
				a.annee, a.genrePersonnel
		) requeteEnsemble
		LEFT JOIN get_effectif_details_total c ON
				requeteEnsemble.ANNEE = c.annee
		LEFT JOIN get_debauche_details_total d ON
				requeteEnsemble.ANNEE = d.anneeDebauche
		GROUP BY requeteEnsemble.ANNEE
		ORDER BY requeteEnsemble.ANNEE ASC";

		return $this -> select();
	}

	function rapportEvolutionSalaire() {
		$this -> _query = "SELECT
			reqq4.ANNEE,
			reqq4.SALAIRE_BASE,
			reqq4.SALAIRE_BRUT,
			reqq4.SBRUT_AV_MATERNITE,
			reqq4.CNAPS,
			reqq4.SMIE,
			reqq4.IRSA_NET,
			reqq4.NET_A_PAYER
		FROM (
			SELECT
				s.ANNEE_SALAIRE AS ANNEE,
				SUM(h.SALAIRE_BASE) AS SALAIRE_BASE,
				SUM(h.S_BRUT_AP_INFLATION) AS SALAIRE_BRUT, /*S.Brut = S.Base + augmentations*/
				SUM(h.SBRUT_AV_MATERNITE) AS SBRUT_AV_MATERNITE, /*S.Brut2 = S.Brut + Primes exceptionnelles + HS + S.intérim*/
				SUM(h.CNAPS_PRIME) AS CNAPS,
				SUM(h.SMIE_PRIME) AS SMIE,
				SUM(h.IRSA_NET) AS IRSA_NET,
				SUM(h.NET_A_PAYER) AS NET_A_PAYER
			FROM
				historique_salaire h
			LEFT JOIN salaire s ON
				s.ID_SALAIRE = h.ID_SALAIRE 
			GROUP BY
				s.ANNEE_SALAIRE
			ORDER BY
				ANNEE ASC) reqq4";

		return $this -> select();
	}

	function nouveauEntrants($where) {
		$this -> _query = "SELECT
			COUNT(DISTINCT c.ID_EMPLOYE) AS NoOfEmployee
		FROM
			contrat c
		WHERE $where";

		return $this -> select();
	}

	function rapportFormation($where) {
		$this -> _query = "SELECT
			COUNT(DISTINCT a.ID_EMPLOYE),
			u.TYPE_FORMATION
		FROM
			inscription i
		INNER JOIN apprenant a ON
			i.ID_APPRENANT = a.ID_APPRENANT
		INNER JOIN session_formation u ON
			i.ID_SESSION_FORMATION = u.ID_SESSION_FORMATION
		LEFT JOIN personnel p ON
			a.ID_EMPLOYE = p.ID_EMPLOYE
		WHERE $where
		GROUP BY
			u.TYPE_FORMATION";

		return $this -> select();
	}

	function exportDataBase() {
	try {
			$world_dumper = Shuttle_Dumper::create(array(
				'host' => $this -> _host,
				'username' => $this -> _root,
				'password' => $this -> _password,
				'db_name' => $this -> _database
			));
			$date_du_backup = new Datetime('now');
			// dump the database to plain text file in GprhDDBSave folder with actual date (year-month-day-hour-min-sec)
			$world_dumper -> dump('../../../GprhDDBSave/'.$date_du_backup->format('Y-m-d-h-i-s').'.sql');
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	function saveOrUpdateEquipement($idEquipement, $idEmploye, $typeEquipement, $marque, $numeroSerie, $couleur, $autresCaracteristiques, $dateReception, $etatReception, $dateRestitution, $etatRestitution) {
		$marque = $this -> specialCharacters($marque);
		$numeroSerie = $this -> specialCharacters($numeroSerie);
		$autresCaracteristiques = $this -> specialCharacters($autresCaracteristiques);
		$dateRestitution = $this -> IsNullOrEmptyString($dateRestitution) > 0 ? "(select null from dual)" : "'".$dateRestitution."'";
		
		if ($this -> IsNullOrEmptyString($idEquipement) > 0) {
			// ID_EQUIPEMENT is empty or null then INSERT new raw
			$this -> _query = "INSERT INTO equipement(
				ID_EMPLOYE,
				TYPE_EQUIPEMENT,
				MARQUE,
				NUMERO_SERIE,
				COULEUR,
				AUTRES_CARACTERISTIQUES,
				DATE_RECEPTION,
				ETAT_RECEPTION,
				DATE_RESTITUTION,
				ETAT_RESTITUTION
			)
			VALUES(
				$idEmploye,
				'$typeEquipement',
				'$marque',
				'$numeroSerie',
				'$couleur',
				'$autresCaracteristiques',
				'$dateReception',
				'$etatReception',
				$dateRestitution,
				'$etatRestitution'
			)";
		} else {
			// UPDATE WHERE ID_EQUIPEMENT is
			$this -> _query = "UPDATE
				equipement
			SET
				ID_EMPLOYE = $idEmploye,
				TYPE_EQUIPEMENT = '$typeEquipement',
				MARQUE = '$marque',
				NUMERO_SERIE = '$numeroSerie',
				COULEUR = '$couleur',
				AUTRES_CARACTERISTIQUES = '$autresCaracteristiques',
				DATE_RECEPTION = '$dateReception',
				ETAT_RECEPTION = '$etatReception',
				DATE_RESTITUTION = $dateRestitution,
				ETAT_RESTITUTION = '$etatRestitution'
			WHERE
				ID_EQUIPEMENT = $idEquipement";
		}
		
		return $this -> update($idEquipement);
	}

	function getEquipementList($where) {
		$this -> _query = "SELECT
			ID_EQUIPEMENT,
			ID_EMPLOYE,
			TYPE_EQUIPEMENT,
			MARQUE,
			NUMERO_SERIE,
			COULEUR,
			AUTRES_CARACTERISTIQUES,
			DATE_RECEPTION,
			ETAT_RECEPTION,
			DATE_RESTITUTION,
			ETAT_RESTITUTION
		FROM
			equipement
		$where";

		return $this -> select();
	}

	function getContratActuelList($where) {
		$this -> _query = "SELECT
			ct.ABREVIATION_CENTRE,
			p.NOM,
			p.PRENOM,
			CASE WHEN p.SEXE = 1 THEN 'Masculin' WHEN p.SEXE = 2 THEN 'Féminin'
			END AS GENRE,
			po.TITRE_POSTE,
			po.CATEGORIE_REELLE,
			c.ID_CENTRE,
			ca.CATEGORIE_PROFESSIONNELLE
		FROM
			contrat c
		LEFT JOIN personnel p ON
			c.ID_EMPLOYE = p.ID_EMPLOYE
		LEFT JOIN poste po ON
			c.ID_POSTE = po.ID_POSTE
		LEFT JOIN categorie ca ON
			c.ID_CATEGORIE = ca.ID_CATEGORIE
		LEFT JOIN centre ct ON
			c.ID_CENTRE = ct.ID_CENTRE
		LEFT JOIN(
			SELECT
				ID_EMPLOYE,
				COUNT(*) AS nb_enfant
			FROM
				enfant
			WHERE
				1
			GROUP BY
				ID_EMPLOYE
		) g
		ON
			c.ID_EMPLOYE = g.ID_EMPLOYE
		$where
		ORDER BY
			p.NOM,
			p.PRENOM ASC";

		return $this -> select();
	}

	// Créer une table temporaire et insert les lignes
	function go($data) {
		$this -> _query = "CREATE TEMPORARY TABLE
		IF NOT EXISTS
			netapayer (idContrat INT (11), netApayer DECIMAL (20, 2));
		DELETE FROM netapayer;
		INSERT INTO netapayer (idContrat, netApayer)
		VALUES ".implode(',', $data).";
		SELECT
			ct.ABREVIATION_CENTRE,
			c.MATRICULE,
			CONCAT(p.NOM, ' ', p.PRENOM),
			po.TITRE_POSTE,
			h.ID_CONTRAT AS dernContrats,
			h.NET_A_PAYER AS dernNAP,
			n.idContrat AS nouvContrats,
			n.netApayer AS nouvNAP,
			(IFNULL(h.NET_A_PAYER, 0) - n.netApayer) AS diff
		FROM netapayer n 
		LEFT JOIN historique_salaire h ON h.ID_CONTRAT = n.idContrat
		AND h.ID_SALAIRE = (
			SELECT
				MAX(ID_SALAIRE) AS IDsALAIRE
			FROM
				salaire
			WHERE
				CLOTURE_SALAIRE = 1
		)
		LEFT JOIN contrat c ON n.idContrat = c.ID_CONTRAT
		LEFT JOIN poste po ON c.ID_POSTE = po.ID_POSTE
		LEFT JOIN personnel p ON c.ID_EMPLOYE = p.ID_EMPLOYE
		LEFT JOIN centre ct ON c.ID_CENTRE = ct.ID_CENTRE
		WHERE (IFNULL(h.NET_A_PAYER, 0) - n.netApayer) <> 0 or h.NET_A_PAYER IS NULL";
		// echo $this -> _query;
		return $this -> multipleQuery();
	}

	// Salaire brut par catégorie et par genre
	function rapportDagobert($whereAnnee) {
		$this -> _query = "SELECT
				cat.CATEGORIE_PROFESSIONNELLE AS CAT_PRO4a,
				cat.ID_CATEGORIE AS ID_CATa4,
				SUM(IF(req4.GENRE_PERSONNEL = 1, req4.S_BRUT, 0)) AS homme,
				SUM(IF(req4.GENRE_PERSONNEL = 2, req4.S_BRUT, 0)) AS femme
			FROM categorie cat
		LEFT JOIN (SELECT
		ca.CATEGORIE_PROFESSIONNELLE AS CAT_PRO4,
		SUM(h.S_BRUT_AP_INFLATION) AS S_BRUT,
		p.SEXE AS GENRE_PERSONNEL
		FROM
		categorie ca
		LEFT JOIN historique_salaire h ON
		h.ID_CATEGORIE_PROFESSIONNELLE = ca.ID_CATEGORIE
		LEFT JOIN contrat c ON h.ID_CONTRAT = c.ID_CONTRAT
		LEFT JOIN personnel p ON c.ID_EMPLOYE = p.ID_EMPLOYE
		LEFT JOIN salaire s ON
		s.ID_SALAIRE = h.ID_SALAIRE
		WHERE
		s.ANNEE_SALAIRE = '$whereAnnee'
		GROUP BY
		CAT_PRO4,
		p.SEXE) req4 ON req4.CAT_PRO4 = cat.CATEGORIE_PROFESSIONNELLE
		GROUP BY cat.CATEGORIE_PROFESSIONNELLE
		ORDER BY cat.ID_CATEGORIE";

		return $this -> select();
	}

	function getTreiziemeList($where) {
		$this -> _query = "SELECT
			ID_SALAIRE
		FROM
			historique_treizieme
		$where";

		return $this -> select();
	}

	function insertIntoHistoriqueTreizieme($data) {
		$this -> _query = "INSERT INTO historique_treizieme(
			ID_SALAIRE,
			ID_CONTRAT,
			MATRICULE,
			ID_POSTE,
			ID_CATEGORIE_PROFESSIONNELLE,
			DATE_EMBAUCHE,
			DATE_PRISE_POSTE,
			SB_CONCLU,
			SB_GRILLE,
			ID_AUGM_GENERALE,
			ID_AUGM_INFLATION,
			SALAIRE_BASE,
			SALAIRE_SUPPLEMENTAIRE,
			S_NON_PERCU,
			PRIME_EXCEPTIONNEL,
			ANOMALIE,
			DATE_DEBUT_INTERIM,
			DATE_FIN_INTERIM,
			MONTANT_INTERIM,
			DUREE_VALIDE_INTERIM,
			BASE_PAR_HEURE,
			ID_HS_INF_HUIT_HEURES,
			ID_HS_SUP_HUIT_HEURES,
			ID_HS_NUIT_HABITUELLE,
			ID_HS_NUIT_OCCASIONNELLE,
			ID_HS_DIMANCHE,
			ID_HS_FERIE,
			MONTANT_HS,
			SBRUT_AV_MATERNITE,
			MONTANT_H_MINUS,
			DEBUT_MATERNITE,
			FIN_MATERNITE,
			DUREE_MATERNITE_CE_MOIS,
			TAUX_MATERNITE,
			DEDUCTION_MATERNITE,
			SBRUT_FINAL,
			ID_CNAPS,
			CNAPS_BASE,
			CNAPS_PRIME,
			ID_SMIE,
			NOM_SMIE,
			SMIE_PRIME,
			SOMME_CNAPS_SMIE,
			S_BRUT_AP_CNAPS_SMIE,
			S_BRUT_AP_CNAPS_SMIE_ARR,
			IRSA_AP_PLAFOND,
			S_IMPOSABLE,
			ID_IRSA,
			ID_PARAM_ABATTEMENT,
			ABATTEMENT,
			IRSA_NET,
			ID_PARAM_HOSPITALISATION,
			CAISSE_HOSPITALISATION,
			FRAIS_MEDICAUX,
			ID_HOPITAL,
			PARTICIPATION_MEDICALE,
			ID_AVANCE,
			DEDUCTION_AVANCE,
			DIVERS_RETENUS,
			DEPASSEMENT,
			CONGE_PRIS,
			SOLDE_CONGE,
			NET_A_PAYER,
			NB_ENFANT,
			TOTAL_HEURE_HS,
			S_BRUT_AP_INFLATION,
			S_BRUT_ANNEE_DERNIERE,
			ID_HS,
			CATEGORIE_REELLE,
			RAPPEL_BASE,
			RAPPEL_NET,
			REMARQUES
		)
		VALUES ".implode(',', $data);
		
		return $this -> updates();
	}

	function getHistoriqueTreizieme($where) {
		$this -> _query = "SELECT
			h.ID_CONTRAT,
			h.MATRICULE,
			p.NOM,
			p.PRENOM,
			po.TITRE_POSTE,
			ca.CATEGORIE_PROFESSIONNELLE,
			h.DATE_EMBAUCHE,
			h.DATE_PRISE_POSTE,
			h.SB_CONCLU,
			h.SB_GRILLE,
			h.SALAIRE_BASE,
			h.SALAIRE_SUPPLEMENTAIRE,
			h.S_NON_PERCU,
			h.PRIME_EXCEPTIONNEL,
			h.ANOMALIE,
			h.DATE_DEBUT_INTERIM,
			h.DATE_FIN_INTERIM,
			h.MONTANT_INTERIM,
			h.BASE_PAR_HEURE,
			h.MONTANT_HS,
			h.SBRUT_AV_MATERNITE,
			h.MONTANT_H_MINUS,
			h.DUREE_MATERNITE_CE_MOIS,
			h.DEDUCTION_MATERNITE,
			h.SBRUT_FINAL,
			h.CNAPS_BASE,
			h.CNAPS_PRIME,
			h.NOM_SMIE,
			h.SMIE_PRIME,
			h.SOMME_CNAPS_SMIE,
			h.S_BRUT_AP_CNAPS_SMIE,
			h.S_BRUT_AP_CNAPS_SMIE_ARR,
			h.IRSA_AP_PLAFOND,
			h.S_IMPOSABLE,
			h.ABATTEMENT,
			h.IRSA_NET,
			h.CAISSE_HOSPITALISATION,
			h.FRAIS_MEDICAUX,
			t.POURCENTAGE_EMPLOYE_HOPITAL,
			h.PARTICIPATION_MEDICALE,
			h.DEDUCTION_AVANCE,
			h.DIVERS_RETENUS,
			h.DEPASSEMENT,
			null, /* Charge */
			null, /* SalaireNet */
			null, /* Retenus */
			h.NET_A_PAYER,
			null, /* decallageAvance */
			h.CONGE_PRIS,
			h.SOLDE_CONGE,
			h.NB_ENFANT,
			h.ID_SALAIRE,
			h.ID_POSTE,
			h.ID_CATEGORIE_PROFESSIONNELLE,
			h.ID_AUGM_GENERALE,
			h.ID_AUGM_INFLATION,
			h.DUREE_VALIDE_INTERIM,
			h.ID_HS_INF_HUIT_HEURES,
			h.ID_HS_SUP_HUIT_HEURES,
			h.ID_HS_NUIT_HABITUELLE,
			h.ID_HS_NUIT_OCCASIONNELLE,
			h.ID_HS_DIMANCHE,
			h.ID_HS_FERIE,
			h.DEBUT_MATERNITE,
			h.FIN_MATERNITE,
			h.TAUX_MATERNITE,
			h.ID_CNAPS,
			h.ID_SMIE,
			h.ID_IRSA,
			h.ID_PARAM_ABATTEMENT,
			h.ID_PARAM_HOSPITALISATION,
			h.ID_HOPITAL,
			h.ID_AVANCE,
			h.ID_HISTORIQUE_SALAIRE,
			h.S_BRUT_AP_INFLATION,
			ct.ABREVIATION_CENTRE,
			h.TOTAL_HEURE_HS,
			h.S_BRUT_ANNEE_DERNIERE,
			ct.ID_CENTRE,
			hs.INF_HUIT_HEURES,
			hs.SUP_HUIT_HEURES,
			hs.NUIT_HABITUELLE,
			hs.NUIT_OCCASIONNELLE,
			hs.DIMANCHE,
			hs.FERIE,
			sm.DEDUCTION_EMPLOYE,
			av.MONTANT_AVANCE,
			av.DATE_RECEPTION,
			av.DATE_DEBUT_REMBOURSEMENT,
            g.remboursement,
			h.CATEGORIE_REELLE,
			h.RAPPEL_BASE,
			h.RAPPEL_NET,
			h.REMARQUES,
			p.ID_EMPLOYE
		FROM
			historique_treizieme h
		LEFT JOIN contrat c ON h.ID_CONTRAT = c.ID_CONTRAT
		LEFT JOIN poste po ON h.ID_POSTE = po.ID_POSTE
		LEFT JOIN categorie ca ON h.ID_CATEGORIE_PROFESSIONNELLE = ca.ID_CATEGORIE
		LEFT JOIN personnel p ON c.ID_EMPLOYE = p.ID_EMPLOYE
		LEFT JOIN hopital t ON h.ID_HOPITAL = t.ID_HOPITAL
		LEFT JOIN centre ct ON c.ID_CENTRE = ct.ID_CENTRE
		LEFT JOIN heures_supplementaires hs ON h.ID_HS = hs.ID_HEURE_SUPPLEMENTAIRE
		LEFT JOIN smie sm ON h.ID_SMIE = sm.ID_SMIE
		LEFT JOIN avance av ON h.ID_AVANCE = av.ID_AVANCE
		LEFT JOIN (
			SELECT ID_AVANCE,
				SUM(MONTANT_REMBOURSEMENT) AS remboursement
			FROM
				details_avance
			GROUP BY
				ID_AVANCE
		) g ON av.ID_AVANCE = g.ID_AVANCE
		$where
		ORDER BY p.NOM, p.PRENOM ASC";
		
		return $this -> select();
	}
}
?>
