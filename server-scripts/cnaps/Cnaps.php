<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;

if ($infoRequest == 2) {
	$data_key = "data";
	$data = $model -> $data_key;
	SaveOrUpdate($m, $data);
} else if ($infoRequest == 4) {
	/*
	 * Cherche si une ligne avec la valeur ACTIF_CNAPS = 1
	 * Si des lignes existes, il faut les remettre à 0
	 */
	$id = "idCnaps";
	$idCnaps = $model -> $id;
	$checkedId = $m -> CheckExistActif('cnaps', 'ID_CNAPS', 'ACTIF_CNAPS = 1');
		
	ResetActifById($m, $checkedId, 0);

	echo ResetActifById($m, [$idCnaps], 1);
} else if ($infoRequest === 3) {
	$data_key = "data";
	$data = $model -> $data_key;
	Delete($m, $data);
}

/*
 * Enregistrer les informations de la CNaPS
 */
function SaveOrUpdate($dao, $data) {
	$val_actif = $dao -> IsNullOrEmptyString($data -> actifCnaps) > 0 ? 0 : $data -> actifCnaps;

	if ($val_actif >0){
		$checkedId = $dao -> CheckExistActif('cnaps', 'ID_CNAPS', '1');		
		ResetActifById($dao, $checkedId, 0);
	}
	
	$SoU = $dao -> saveOrUpdateCnaps(
		$data -> idCnaps,
		1,
		$dao -> Montant($data -> plafondCnaps),
		$data -> pourcentageEmployeCnaps,
		$data -> pourcentageEmployeurCnaps,
		$data -> datePriseEffetCnaps,
		$val_actif
	);
	echo $SoU;
}

/*
 * @$checkedId est un tableau de IDs
 * utiliser la méthode 'implode' pour les convertir en string et séparées les valeurs par des virgules
 * Envoyer les ids vers la condition WHERE de la requête qui fait la mise à jour
 */
function ResetActifById($dao, $checkedId, $ActifValue) {
	$result = $dao -> array_flatten($checkedId);

	$mystring = implode(', ', $result);
	$R = $dao ->  ResetActifValue('cnaps', 'ACTIF_CNAPS', $ActifValue, 'ID_CNAPS', $mystring);
	return $R;
}

/*
 * Supprimer une ligne dans table "cnaps" où ID_CNAPS = ($data -> idCnaps)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'cnaps',
		'ID_CNAPS',
		$data -> idCnaps
	);
	echo $SoU;
}
?>
