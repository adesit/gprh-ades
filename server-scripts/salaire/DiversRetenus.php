<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer les divers retenus d'un employé pour un mois donné
 */
function SaveOrUpdate($dao, $data) {
	$montantDiversRetenus = $dao -> Montant($data -> montantDiversRetenus);
	
	$SoU = $dao -> saveOrUpdateDiversRetenus(
		$data -> idDiversRetenus,
		$data -> idContrat,
		$data -> idSalaire,
		$montantDiversRetenus,
		$data -> dateDiversRetenus,
		$data -> remarqueDiversRetenus
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "divers_retenus" où ID_DIVERS_RETENUS = ($data -> idDiversRetenus)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'divers_retenus',
		'ID_DIVERS_RETENUS',
		$data -> idDiversRetenus
	);
	echo $SoU;
}
?>
