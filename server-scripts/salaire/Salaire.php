<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer les informations de Salaire
 */
function SaveOrUpdate($dao, $data) {
	$typeSalaire = 1; // 1=Mensuel, 2=journalier, 3=contractuel
	$dateClotureSalaire ='';
	$clotureSalaire = 0; // 1=Clôturée; 0=En cours

	$SoU = $dao -> saveOrUpdateSalaire(
		$data -> idSalaire,
		$data -> moisSeulement,
		$data -> anneeSalaire,
		$typeSalaire,
		$dateClotureSalaire,
		$clotureSalaire
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "salaire" où ID_SALAIRE = ($data -> idSalaire)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'salaire',
		'ID_SALAIRE',
		$data -> idSalaire
	);
	echo $SoU;
}
?>
