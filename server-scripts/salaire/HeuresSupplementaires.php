<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer les heures supplémentaiers d'un employé pour un mois donné
 */
function SaveOrUpdate($dao, $data) {
	$montantInfHuitHeures = $dao -> Montant((empty($data -> infHuitHeures) ? 0 : $data -> infHuitHeures));
	$montantSupHuitHeures = $dao -> Montant((empty($data -> supHuitHeures) ? 0 : $data -> supHuitHeures));
	$montantNuitHabituelle = $dao -> Montant((empty($data -> nuitHabituelle) ? 0 : $data -> nuitHabituelle));
	$montantNuitOccasionnelle = $dao -> Montant((empty($data -> nuitOccasionnelle) ? 0 : $data -> nuitOccasionnelle));
	$montantDimanche = $dao -> Montant((empty($data -> dimanche) ? 0 : $data -> dimanche));
	$montantFerie = $dao -> Montant((empty($data -> ferie) ? 0 : $data -> ferie));
	$SoU = $dao -> saveOrUpdateHeuresSupplementaires(
		$data -> idHeureSupplementaire,
		$data -> idContrat,
		$montantInfHuitHeures,
		$montantSupHuitHeures,
		$montantNuitHabituelle,
		$montantNuitOccasionnelle,
		$montantDimanche,
		$montantFerie,
		$data -> remarqueMotifHs,
		$data -> moisSalaire
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "heures_supplementaires" où ID_HEURE_SUPPLEMENTAIRE = ($data -> idHeureSupplementaire)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'heures_supplementaires',
		'ID_HEURE_SUPPLEMENTAIRE',
		$data -> idHeureSupplementaire
	);
	echo $SoU;
}
?>
