<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer les absences d'un employé pour un mois donné
 */
function SaveOrUpdate($dao, $data) {
	$montantDureeAbsence = $dao -> Montant($data -> dureeAbsence);
	
	$SoU = $dao -> saveOrUpdateAbsence(
		$data -> idAbsence,
		$data -> idContrat,
		$data -> idSalaire,
		$data -> motifAbsence,
		$data -> dateHeureDebutHs,
		$data -> dateHeureFinHs,
		$montantDureeAbsence,
		$data -> typeAbsence,
		$data -> nbJours
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "absence" où ID_ABSENCE = ($data -> idAbsence)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'absence',
		'ID_ABSENCE',
		$data -> idAbsence
	);
	echo $SoU;
}
?>
