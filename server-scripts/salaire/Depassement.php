<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer les dépassements d'un employé pour un mois donné
 */
function SaveOrUpdate($dao, $data) {
	$montantDepassement = $dao -> Montant($data -> depassement);
	
	$SoU = $dao -> saveOrDepassement(
		$data -> idAppel,
		$data -> idContrat,
		$data -> idSalaire,
		$montantDepassement,
		$data -> remarqueDepassement
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "depassement" où ID_APPEL = ($data -> idAppel)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'depassement',
		'ID_APPEL',
		$data -> idAppel
	);
	echo $SoU;
}
?>
