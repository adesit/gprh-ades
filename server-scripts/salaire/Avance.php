<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer l'avance d'un employé
 */
function SaveOrUpdate($dao, $data) {
	$montantMaxAutorise = $dao -> Montant($data -> maxAutorise);
	$montantMaxDeductionPossible = $dao -> Montant($data -> maxDeductionPossible);
	$montantMontantAvance = $dao -> Montant($data -> montantAvance);
	$nombreTranche = $dao -> Montant($data -> nombreTranche);
	$nombreDeductionMensuelle = $dao -> Montant($data -> deductionMensuelle);
	$val_actif = $dao -> IsNullOrEmptyString(isset($data -> enCours)) > 0 ? 0 : 1;
	
	$SoU = $dao -> saveOrUpdateAvance(
		$data -> idAvance,
		$data -> idContrat,
		$montantMaxAutorise,
		$montantMaxDeductionPossible,
		$montantMontantAvance,
		$data -> dateReception,
		$data -> dateDebutRemboursement,
		$data -> dateFinRemboursement,
		$nombreTranche,
		$nombreDeductionMensuelle,
		$data -> raison,
		$val_actif
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "avance" où ID_AVANCE = ($data -> idAvance)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'avance',
		'ID_AVANCE',
		$data -> idAvance
	);
	echo $SoU;
}
?>
