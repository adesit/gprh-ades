<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer le rappel d'un employé
 */
function SaveOrUpdate($dao, $data) {
	$montantRappel = $dao -> Montant($data -> montantRappel);
	$nombreMois = $dao -> Montant($data -> nombreMois);
	
	$SoU = $dao -> saveOrUpdateRappel(
		$data -> idRappel,
		$data -> idContrat,
		$data -> idSalaire,
		$montantRappel,
		$nombreMois,
		$data -> dateDebutRappel,
		$data -> dateFinRappel,
		$data -> remarqueRappel,
		$data -> typeRappel
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "rappel" où ID_RAPPEL = ($data -> idRappel)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'rappel',
		'ID_RAPPEL',
		$data -> idRappel
	);
	echo $SoU;
}
?>
