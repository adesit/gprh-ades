<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer les frais médicaux d'un employé pour un mois donné
 */
function SaveOrUpdate($dao, $data) {
	$montantFraisMedicaux = $dao -> Montant($data -> fraisMedicaux);
	
	$SoU = $dao -> saveOrUpdateMedicaux(
		$data -> idMedicaux,
		$data -> idContrat,
		$data -> idSalaire,
		$data -> dateMedicaux,
		$montantFraisMedicaux,
		$data -> remarqueMotifMedical
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "medicaux" où ID_MEDICAUX = ($data -> idMedicaux)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'medicaux',
		'ID_MEDICAUX',
		$data -> idMedicaux
	);
	echo $SoU;
}
?>
