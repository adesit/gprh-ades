<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer le congé d'un employé pour un mois donné
 */
function SaveOrUpdate($dao, $data) {
	$montantDureeConge = $dao -> Montant($data -> dureeCongePrise);
	
	$SoU = $dao -> saveOrUpdateConge(
		$data -> idConge,
		$data -> idContrat,
		$data -> idSalaire,
		0,
		$data -> dateDebutConge,
		$data -> dateFinConge,
		$data -> remarqueConge,
		$montantDureeConge,
		$data -> natureConge
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "conge" où ID_CONGE = ($data -> idConge)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'conge',
		'ID_CONGE',
		$data -> idConge
	);
	echo $SoU;
}
?>
