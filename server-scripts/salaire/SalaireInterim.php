<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer les informations la période Intérimaire
 */
function SaveOrUpdate($dao, $data) {
	$SoU = $dao -> saveOrUpdateSalaireInterim(
		$data -> idPrimeIndemnite,
		$data -> idContrat,
		$data -> dateDebutInterim,
		$data -> dateFinInterim,
		$data -> remarquesInterim
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "prime_indemnite" où ID_PRIME_INDEMNITE = ($data -> idPrimeIndemnite)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'prime_indemnite',
		'ID_PRIME_INDEMNITE',
		$data -> idPrimeIndemnite
	);
	echo $SoU;
}
?>
