<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer les thèmes de formation
 */
function SaveOrUpdate($dao, $data) {
	$SoU = $dao -> saveOrUpdateEvaluation(
		$data -> idEvaluationFormation,
		$data -> idApprenant,
		$data -> idSessionFormation,
		json_encode($data)
	);
	echo $SoU;
	$dao -> updateEvaluationId();
}

/*
 * Supprimer une ligne dans table "sous_theme" où ID_SOUS_THEME = ($data -> idSousTheme)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'sous_theme',
		'ID_SOUS_THEME',
		$data -> idSousTheme
	);
	echo $SoU;
}
?>
