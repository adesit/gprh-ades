<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer
 * 1. Une session de formation
 * 2. Liste des thèmes à traiter pendant la session de formation (idSessionFormation, idSousTheme(idFormateur, titreSousTheme))
 */
function SaveOrUpdate($dao, $data) {
	// 1. Enregistrer une session de formation
	$SoU = $dao -> saveOrUpdateSessionFormation(
		$data -> idSessionFormation,
		$data -> themeFormation,
		$data -> objectifsFormation,
		$data -> dateCreationSession,
		$data -> dateDebutFormation,
		$data -> dateFinFormation,
		$data -> domaineFormation,
		$data -> adresseSeance,
		$data -> typeFormation,
		$data -> niveauCible
	);

	// 2. Enregistrer les thèmes à traiter pendant la session de formation
	if ($SoU > 0) {
		$dao -> SaveOrUpdateDetailsFormation(
			$SoU,
			$data -> idSousTheme
		);
		echo $SoU;
	}
}

/*
 * Supprimer une ligne dans table "session_formation" où ID_SESSION_FORMATION = ($data -> idSessionFormation)
 * Supprimer les thèmes dans la tables "details_formation" ou ID_SESSION_FORMATION = ($data -> idSessionFormation)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'session_formation',
		'ID_SESSION_FORMATION',
		$data -> idSessionFormation
	);

	if ($SoU > 0) {
		$dao -> DeleteById(
			'details_formation',
			'ID_SESSION_FORMATION',
			$data -> idSessionFormation
		);
	}
	echo $SoU;
}
?>
