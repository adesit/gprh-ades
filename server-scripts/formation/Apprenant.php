<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 1) {
	$ids_key = "paramsIds";
	$ids = $model -> $ids_key;
	$idSession = $data -> idSessionFormation;
	$inscription = array();
	foreach ($ids as $apprenant) {
		$saveApprenant = SaveOrUpdateApprenantEmploye($m, $apprenant); // 1. Enregistrement des infos du stagiaires

		$verifSiDejaInscrit = $m -> verifyInscription($saveApprenant, $idSession);

		if ($verifSiDejaInscrit[0][0] == 0) {
			array_push($inscription, "($saveApprenant, $idSession)");			
		} else $arr["data"] = 1;
	}
	$arr["data"] = $m -> saveOrUpdateInscription($inscription); // 2. Inscription à la formation
	

	echo json_encode($arr);

} else if ($infoRequest === 2) { // Inscription à partir d'un formulaire d'un stagiaire non employé
	$saveApprenant = SaveOrUpdateApprenantNonEmploye($m, $data); // 1. Enregistrement des infos du stagiaires
	$idSession_key = "idSessionFormation";
	$idSession = $model -> $idSession_key;

	$verifSiDejaInscrit = $m -> verifyInscription($saveApprenant, $idSession);

	if ($verifSiDejaInscrit[0][0] == 0) {
		$inscription[] = "($saveApprenant, $idSession)";
		$arr["data"] = $m -> saveOrUpdateInscription($inscription); // 2. Inscription à la formation
	} else {
		$arr["data"] = 'Cette personne est déjà inscrite à cette formation';
	};
	
	echo json_encode($arr);

} else if ($infoRequest === 3) { // Inscription à partir de la liste des Stagiaires non Employés
	$ids_key = "paramsIds";
	$ids = $model -> $ids_key;
	$idSession = $data -> idSessionFormation;
	$inscription = array();
	foreach ($ids as $apprenant) {
		$saveApprenant = SaveOrUpdateApprenantNonEmploye($m, $apprenant); // 1. Enregistrement des infos du stagiaires
		array_push($inscription, "($saveApprenant, $idSession)");
	}
	
	$arr["data"] = $m -> saveOrUpdateInscription($inscription); // 2. Inscription à la formation

	echo json_encode($arr);
	
} else if ($infoRequest === 4) { // Supprimer un apprenant
	DeleteApprenant($m, $data);
} else if ($infoRequest === 5) { // Supprimer une inscription
	DeleteInscription($m, $data);
}

/*
 * Enregistrer les informations du Stagiaire
 */
function SaveOrUpdateApprenantEmploye($dao, $data) {
	$SoU = $dao -> saveOrUpdateApprenant(
		null,
		$data -> nom,
		$data -> prenom,
		$data -> dateNaissance,
		$data -> sexe,
		$data -> telMobile,
		'',
		'',
		1, // Type de relation avec ADES = Employé
		$data -> idEmploye
	);
	return $SoU;
}

/*
 * Enregistrer les informations du Stagiaire
 */
function SaveOrUpdateApprenantNonEmploye($dao, $data) {
	$SoU = $dao -> saveOrUpdateApprenant(
		$data -> idApprenant,
		$data -> nomApprenant,
		$data -> prenomApprenant,
		$data -> dateNaissanceApprenant,
		$data -> sexeApprenant,
		$data -> telephoneApprenant,
		$data -> adresseCompleteApprenant,
		$data -> niveau,
		$data -> typeRelationAdes,
		$data -> idEmploye
	);
	return $SoU;
}

/*
 * Supprimer une ligne dans table "apprenant" où ID_SOUS_THEME = ($data -> idApprenant)
 */
function DeleteApprenant($dao, $data) {
	$SoU = $dao -> DeleteById(
		'apprenant',
		'ID_APPRENANT',
		$data -> idApprenant
	);
	echo $SoU;
}
/*
 * Supprimer une ligne dans table "inscription" où ID_INSCRIPTION = ($data -> idInscription)
 */
function DeleteInscription($dao, $data) {
	$SoU = $dao -> DeleteById(
		'inscription',
		'ID_INSCRIPTION',
		$data -> idInscription
	);
	echo $SoU;
}
?>