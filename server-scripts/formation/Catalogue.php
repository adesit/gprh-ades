<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer les thèmes de formation
 */
function SaveOrUpdate($dao, $data) {
	$SoU = $dao -> saveOrUpdateCatalogue(
		$data -> idSousTheme,
		$data -> idFormateur,
		$data -> titreSousTheme,
		$data -> objectifsSousTheme
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "sous_theme" où ID_SOUS_THEME = ($data -> idSousTheme)
 */
function Delete($dao, $data) {
	$verifThemeExist = $dao -> verfiThemeDansSession($data -> idSousTheme);
	if (count($verifThemeExist) > 0) {
		echo '{"error": "ne pas supprimer"}';
	} else {
		$SoU = $dao -> DeleteById(
			'sous_theme',
			'ID_SOUS_THEME',
			$data -> idSousTheme
		);
		echo $SoU;
	}
}
?>
