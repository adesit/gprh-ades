<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer les informations du Formateur
 */
function SaveOrUpdate($dao, $data) {
	$SoU = $dao -> saveOrUpdateFormateur(
		$data -> idFormateur,
		$data -> nomFormateur,
		$data -> prenomFormateur,
		$data -> titreFonctionFormateur,
		$data -> institution,
		$data -> emailFormateur,
		$data -> telephoneFormateur,
		$data -> adresseCompleteFormateur
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "formateur" où ID_FORMATEUR = ($data -> idFormateur)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'formateur',
		'ID_FORMATEUR',
		$data -> idFormateur
	);
	echo $SoU;
}
?>
