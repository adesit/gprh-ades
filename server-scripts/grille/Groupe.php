<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;

if ($infoRequest == 2) {
	$data_key = "data";
	$data = $model -> $data_key;
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	$data_key = "data";
	$data = $model -> $data_key;
	Delete($m, $data);
}

/*
 * Save or update changed data from Centre grid in Tab Panel
 * 
 */
function SaveOrUpdate($dao, $data) {
	$SoU = $dao -> saveOrUpdateGroupe(
		$data -> idGroupeCategorie,
		$data -> nomGroupe
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "groupe_categorie" où ID_GROUPE_CATEGORIE = ($data -> idGroupeCategorie)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'groupe_categorie',
		'ID_GROUPE_CATEGORIE',
		$data -> idGroupeCategorie
	);
	echo $SoU;
}
?>
