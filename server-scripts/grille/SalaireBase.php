<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest == 3) {
/*
 * Cherche si une ligne avec la valeur ACTIF_LIGNE_GRILLE = 1 pour un groupe et catégorie données
 * Si des lignes existes, il faut les remettre à 0
 */
	$checkedId = $m -> CheckExistActif('grille', 'ID_LIGNE_GRILLE', 'ID_GROUPE_CATEGORIE = '.$data -> idGroupe.' AND ID_CATEGORIE = '. $data -> idCategorie.' AND ACTIF_LIGNE_GRILLE = 1');
	
	echo ResetActifById($m, $checkedId, 0);
} else if ($infoRequest == 4) {
	$checkedId = array();
	$IdValue = $data -> idLigneGrille;
	$checkedId[] = $IdValue;
	
	echo ResetActifById($m, $checkedId, 1);
} else if ($infoRequest === 5) {
	$data_key = "data";
	$data = $model -> $data_key;
	Delete($m, $data);
}

/*
 * Table = 'grille'
 * Insertion si idLigneGrille est 'null'
 * Modification si idLigneGrille != null  
 */
function SaveOrUpdate($dao, $entry) {
	$val_actif = ($dao -> IsNullOrEmptyString(isset($entry -> actifLigneGrille)) > 0 ? 0 : 1);

	if ($val_actif >0){
		$checkedId = $dao -> CheckExistActif('grille', 'ID_LIGNE_GRILLE', 'ID_GROUPE_CATEGORIE = '.$entry -> idGroupe.' AND ID_CATEGORIE = '. $entry -> idCategorie.' AND ACTIF_LIGNE_GRILLE = 1');
		ResetActifById($dao, $checkedId, 0);
	}

	$SoU = $dao -> saveOrUpdateSalaireBase(
		$entry -> idLigneGrille,
		$entry -> idGroupe,
		$entry -> idCategorie,
		$dao -> Montant($entry -> salaireBaseMinimum),
		$dao -> Montant($entry -> salaireBase3),
		$dao -> Montant($entry -> salaireBase4),
		$dao -> Montant($entry -> salaireBase5),
		$val_actif,
		$entry -> datePriseEffetGrille
	);
	echo $SoU;
}

/*
 * @$checkedId est un tableau de IDs
 * utiliser la méthode 'implode' pour les convertir en string et séparées les valeurs par des virgules
 * Envoyer les ids vers la condition WHERE de la requête qui fait la mise à jour
 */
function ResetActifById($dao, $checkedId, $ActifValue) {
	$result = $dao -> array_flatten($checkedId);

	$mystring = implode(', ', $result);
	$R = $dao ->  ResetActifValue('grille', 'ACTIF_LIGNE_GRILLE', $ActifValue, 'ID_LIGNE_GRILLE', $mystring);

	return $R;
}

/*
 * Supprimer une ligne dans table "grille" où ID_LIGNE_GRILLE = ($data -> idLigneGrille)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'grille',
		'ID_LIGNE_GRILLE',
		$data -> idLigneGrille
	);
	echo $SoU;
}
?>
