<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_REQUEST['infoRequest']);
$idSessionFormation = json_decode($_REQUEST['idSessionFormation']);

if ($infoRequest == 1) { // Evaluation de toutes les sessions de formation
	$where = "WHERE 1";
	$whereSession = "";
} else if ($infoRequest == 2) { // Evaluation d'une sessions de formation par son ID
	$where = "WHERE u.ID_SESSION_FORMATION = $idSessionFormation";
	$whereSession = "WHERE i.ID_SESSION_FORMATION = $idSessionFormation AND i.ID_APPRENANT NOT IN(
		SELECT
			ID_APPRENANT
		FROM
			evaluation_formation
		WHERE
			ID_SESSION_FORMATION = $idSessionFormation
	)";
}

$page = json_decode($_REQUEST['page']);
$start = json_decode($_REQUEST['start']);
$lim = json_decode($_REQUEST['limit']);
$limit = $lim;

$themes=$m->getFormationListe($where);

$themes1 = array_slice($themes, $start, $limit, true);

$nb=count($themes);
$response = array();
$persons = $m -> getApprenantList($whereSession);

$persons1 = array_slice($persons, $start, $limit, true);

// Column headers
	$idEvaluationFormation = array(
			'header' => '#idEvaluationFormation', 
			'dataIndex' => 'idEvaluationFormation', 
			'hidden' => true, 
			'editor' => false
		);
	$idInscription = array(
		'header' => '#idInscription', 
		'dataIndex' => 'idInscription', 
		'hidden' => true, 
		'editor' => false
	);
	$idApprenant = array(
		'header' => '#idApprenant', 
		'dataIndex' => 'idApprenant', 
		'hidden' => true, 
		'editor' => false
	);
	$idSessionFormation = array(
		'header' => '#idSessionFormation', 
		'dataIndex' => 'idSessionFormation', 
		'hidden' => true, 
		'editor' => false
	);
	$nomApprenant = array(
		'header' => 'Nom', 
		'dataIndex' => 'nomApprenant', 
		'width' => 200, 
		'editor' => false
	);
	$prenomApprenant = array(
		'header' => 'Prénom', 
		'dataIndex' => 'prenomApprenant', 
		'width' => 200, 
		'editor' => false
	);
	$arQ1 = array(
		'header' => "Q1", 
		'dataIndex' => "FLQ1", 
		'width' => 105
	);
	$arQ2 = array(
		'header' => "Q2", 
		'dataIndex' => "FLQ2", 
		'width' => 105
	);
	$arQ3 = [];
	$arQ4 = [];
	$arQ5 = [];
	$arQ6 = array(
		'header' => "Q6", 
		'dataIndex' => 
		"FLQ6", 
		'width' => 105
	);
	$arQ7 = array(
		'header' => "Q7", 
		'dataIndex' => "FLQ7", 
		'width' => 105
	);
	$remarqueEditor['xtype'] = "textfield";
	$remarqueEditor['width'] = 350;
	$remarques = array(
		'header' => 'Remarques et commentaires', 
		'dataIndex' => 'remarques', 
		'width' => 350, 
		'editor' => $remarqueEditor
	);

// Field data
	$FLidEvaluationFormation = array('name' => 'idEvaluationFormation');
	$FLidInscription = array('name' => 'idInscription');
	$FLidApprenant = array('name' => 'idApprenant');
	$FLidSessionFormation = array('name' => 'idSessionFormation');
	$FLnomApprenant = array('name' => 'nomApprenant');
	$FLprenomApprenant = array('name' => 'prenomApprenant');
	$flQ1 = array('name' => "FLQ1");
	$flQ2 = array('name' => "FLQ2");
	$flQ3 = [];
	$flQ4 = [];
	$flQ5 = [];
	$flQ6 = array('name' => "FLQ6");
	$flQ7 = array('name' => "FLQ7");
	$FLremarques = array('name' => 'remarques');

	$nbTheme = 0;
	foreach ($themes1 as list($q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae)) {
		$nbTheme += 1;
		array_push($arQ3, array(
			'header' => 'Q3TH'.$ab,
			'dataIndex' => 'FLQ3TH'.$ab,
			'width' => 105,
			'tooltip' => $ac,
		));
		array_push($flQ3, array(
			'name' => 'FLQ3TH'.$ab,
		));
		array_push($arQ4, array(
			'header' => 'Q4TH'.$ab,
			'dataIndex' => 'FLQ4TH'.$ab,
			'width' => 105,
			'tooltip' => $ac,
		));
		array_push($flQ4, array(
			'name' => 'FLQ4TH'.$ab,
		));
		array_push($arQ5, array(
			'header' => 'Q5TH'.$ab,
			'dataIndex' => 'FLQ5TH'.$ab,
			'width' => 105,
			'tooltip' => $ac,
		));
		array_push($flQ5, array(
			'name' => 'FLQ5TH'.$ab,
		));
	}

// Les titres des columns
	$headers = [];
	array_push($headers, $idEvaluationFormation);
	array_push($headers, $idInscription);
	array_push($headers, $idApprenant);
	array_push($headers, $idSessionFormation);
	array_push($headers, $nomApprenant);
	array_push($headers, $prenomApprenant);
	array_push($headers, $arQ1);
	array_push($headers, $arQ2);
	foreach ($arQ3 as $q3) {
		array_push($headers, $q3);
	}
	foreach ($arQ4 as $q4) {
		array_push($headers, $q4);
	}
	foreach ($arQ5 as $q5) {
		array_push($headers, $q5);
	}
	array_push($headers, $arQ6);
	array_push($headers, $arQ7);
	array_push($headers, $remarques);

	$arr['columndata'] = array_values($headers);

// Les fields
	$fields = [];
	array_push($fields, $FLidEvaluationFormation);
	array_push($fields, $FLidInscription);
	array_push($fields, $FLidApprenant);
	array_push($fields, $FLidSessionFormation);
	array_push($fields, $FLnomApprenant);
	array_push($fields, $FLprenomApprenant);
	array_push($fields, $flQ1);
	array_push($fields, $flQ2);
	foreach ($flQ3 as $f3) {
		array_push($fields, $f3);
	}
	foreach ($flQ4 as $f4) {
		array_push($fields, $f4);
	}
	foreach ($flQ5 as $f5) {
		array_push($fields, $f5);
	}
	array_push($fields, $flQ6);
	array_push($fields, $flQ7);
	array_push($fields, $FLremarques);

	$arr['fielddata'] = array_values($fields);

// Les values
	function arrayOfDynamicValues($a, $b, $c, $d, $e, $flQ3, $flQ4, $flQ5) {
		$expectedArray = array(
			"",
			$a,
			$b,
			$c,
			$d,
			$e
		);
		array_push($expectedArray, "");
		array_push($expectedArray, "");
		foreach ($flQ3 as $f3) {
			array_push($expectedArray, "");
		}
		foreach ($flQ4 as $f4) {
			array_push($expectedArray, "");
		}
		foreach ($flQ5 as $f5) {
			array_push($expectedArray, "");
		}
		array_push($expectedArray, "");
		array_push($expectedArray, "");
		array_push($expectedArray, "");

		return (array) $expectedArray;
	}

	$values = [];
	foreach ($persons1 as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $o, $p)) {
		array_push($values, arrayOfDynamicValues($a, $b, $c, $d, $e, $flQ3, $flQ4, $flQ5));
	}
	$arr['values'] = ($values);
	$arr['total'] = count($persons);

$arr = json_encode($arr);
echo $arr;
?>
