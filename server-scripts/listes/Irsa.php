<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getIrsaList();

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h)) {
	array_push($response, array(
		'idIrsa' => $a,
		'idAdes' => $b,
		'plafond' => $c,
		'pourcentageEmployeIrsa' => $d,
		'pourcentageEmployeurIrsa' => $e,
		'deductionEnfant' => $f,
		'datePriseEffetIrsa' => $g,
		'actifIrsa' => $h > 0 ? true : false,
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
