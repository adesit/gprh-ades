<?php
require_once '../DataAccessObject.php';
require_once '../formule/AppliquerFormule.php';

$dao=new DataAccessObject();

$taux_augmentation_generale = $dao -> getTauxAugmentationGenerale(); // Taux de l'augmentation générale
$taux_inflation = $dao -> getTauxAugmentationInflation(); // Taux de l'augmentation de l'inflation

$response = [
	'tauxAugmentation' => $taux_augmentation_generale[0][0],
	'tauxInflation' => $taux_inflation[0][0]
];

$arr['data'] = array_values($response);

$arr = json_encode($arr);
echo $arr;

?>
