<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Tous les conjoints
	$whereActif = "WHERE 1";
} else if ($infoRequest == 2) { // Le conjoint d'un employé 
	$id_employe = json_decode($_GET['idEmploye']);
	$whereActif = "WHERE s.ID_EMPLOYE = $id_employe";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getConjointList($whereActif);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h)) {
	array_push($response, array(
		'idConjoint' => $a,
		'idEmploye' => $b,
		'nomConjoint' => $c,
		'prenomConjoint' => $d,
		'dateNaissanceConjoint' => $e,
		'genreConjoint' => $f,
		'fonctionConjoint' => $g,
		'telConjoint' => $h
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
