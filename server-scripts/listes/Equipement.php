<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Tous les équipements
	$whereActif = "WHERE 1";
} else if ($infoRequest == 2) { // Liste des équipements par ID_EMPLOYE
	$id_employe = json_decode($_GET['idEmploye']);
	$whereActif = "WHERE ID_EMPLOYE = $id_employe";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getEquipementList($whereActif);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k)) {
	array_push($response, array(
		'idEquipement' => $a,
		'idEmploye' => $b,
		'typeEquipement' => $c,
		'marque' => $d,
		'numeroSerie' => $e,
		'couleur' => $f,
		'autresCaracteristiques' => $g,
		'dateReception' => $h,
		'etatReception' => $i,
		'dateRestitution' => $j,
		'etatRestitution' => $k
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
