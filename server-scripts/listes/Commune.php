<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();
$r=$m->getCommuneList();

$n=count($r);
$response = array();
foreach ($r as list($a, $b, $c, $d, $e)) {
	array_push($response, array(
		'idCommune' => $a,
		'idDistrict' => $b,
		'codeCommune' => $c,
		'nomCommune' => $d,
		'codeDistrict' => $e
	));
}

$arr['data'] = array_values($response);

$arr = json_encode($arr);
echo $arr;
?>
