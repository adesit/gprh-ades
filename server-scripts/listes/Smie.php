<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getSmieList();

$nb=count($res);
$res1 = array_slice($res, $start, $limit, true);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k)) {
	array_push($response, array(
		'idSmie' => $a,
		'nomSmie' => $b,
		'plafondSmie' => $c,
		'deductionEmploye' => $d,
		'deductionEmployeur' => $e,
		'coutExcedentaire' => $f,
		'franchiseCoutExcendentaire' => $g,
		'datePriseEffetSmie' => $h,
		'actifSmie' => $i > 0 ? true : false,
		'idCentre' => $j,
		'nomCentre' => $k
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
