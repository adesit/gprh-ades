<?php
require_once '../DataAccessObject.php';

$dao = new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Toutes les historiques de salaire
	$typeUtilisateur = json_decode($_GET['typeUtilisateur']);
	$idCentre = json_decode($_GET['idCentre']);
	if ($typeUtilisateur == 1) $where = "WHERE 1";
	else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE c.ID_CENTRE = $idCentre";
} else if ($infoRequest == 3) { // Toutes les historiques d'un mois de salaire
	$moisSalaire = json_decode($_GET['idSalaire']);
	$typeUtilisateur = json_decode($_GET['typeUtilisateur']);
	$idCentre = json_decode($_GET['idCentre']);
	if ($typeUtilisateur == 1) $where = "WHERE h.ID_SALAIRE = $moisSalaire";
	else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE h.ID_SALAIRE = $moisSalaire AND c.ID_CENTRE = $idCentre";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

ini_set('memory_limit', '-1'); // It will take unlimited memory usage of server
if ($infoRequest == 1 || $infoRequest == 3) { // Liste des historiques salaires
	$res = $dao -> getHistoriqueSalaire($where);
} else if ($infoRequest == 2) { // Liste des historique 13èmes MOIS
	$idSalaire = json_decode($_REQUEST['idSalaire']);
	$typeUtilisateur = json_decode($_REQUEST['typeUtilisateur']);
	$idCentre = json_decode($_REQUEST['idCentre']);
	if ($typeUtilisateur == 1) $where = "WHERE h.ID_SALAIRE = $idSalaire";
	else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE h.ID_SALAIRE = $idSalaire AND c.ID_CENTRE = $idCentre";
	$res = $dao -> getHistoriqueTreizieme($where);
}

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae, $af, $ag, $ah, $ai, $aj, $ak, $al, $am, $an, $ao, $ap, $aq, $ar, $as, $at, $au, $av, $aw, $ax, $ay, $az, $ba, $bb, $bc ,$bd, $be, $bf, $bg, $bh, $bi, $bj, $bk, $bl, $bm, $bn, $bo, $bp, $bq, $br, $bs, $bt, $bu, $bv, $bw, $bx, $by, $bz, $ca, $cb, $cc, $cd, $ce, $cf, $cg, $ch, $ci, $cj, $ck, $cl, $cm, $cn, $co, $cp)) {
	array_push($response, array(
		'idContrat' => $a,
		'matricule' => $b,
		'nomPrenom' => $c.' '.$d,
		'titrePoste' => $e,
		'categorieProfessionnelle' => $f,
		'dateEmbauche' => $g,
		'datePrisePoste' => $h,
		'SBConclu2017' => $i,
		'Bjanvier' => $j,
		'salaireBase' => $k,
		'salaireSupplementaire' => $l,
		'nonPercu' => $m,
		'primeExceptionnel' => $n,
		'anomalie' => ($o == 0) ? false : true ,
		'dateDebutInterim' => $p,
		'dateFinInterim' => $q,
		'montantInterim' => $r,
		'baseParHeure' => $s,
		'montantHS' => $t,
		'sBrutAvantMaternite' => $u,
		'montantHMinus' => $v,
		'dureeCongeMaterniteCeMois' => $w,
		'deductionMaternite' => $x,
		'salaireBrutFinal' => $y,
		'cnapsBase' => $z,
		'cnapsPrime' => $aa,
		'nomCentreSmie' => $ab,
		'smiePrime' => $ac,
		'sommeCnapsSmie' => $ad,
		'salaireBrutApresCnapsSmie' => $ae,
		'brutApresCnapsSmieArrondi' => $af,
		'montantIrsaApresPlafond' => $ag,
		'IrsaBrutArrondi' => $ah,
		'abattement' => $ai,
		'IrsaNet' => $aj,
		'caisseHospitalisation' => $ak,
		'factureFraisMedicaux' => $al,
		'tauxFraisMedicaux' => $am,
		'participationFraisMedicaux' => $an,
		'deductionAvance' => $ao,
		'diversRetenus' => $ap,
		'depassement' => $aq,
		'Charge' => $ar,
		'SalaireNet' => $as,
		'Retenus' => $at,
		'netAPayer' => $au,
		'decallageAvance' => $av,
		'congePris' => $aw,
		'soldeConge' => $ax,
		'nbEnfant' => $ay,
		'abreviationCentre' => $bx,
		'totalHeureHS' => $by,
		'salaireBrutInclInflation' => $bw,
		'sBrutAnneeDerniere' => $bz,
		'categorieReelle' => $cm,
		'rappelBase' => $cn,
		'rappelNet' => $co,
		'remarque' => $cp
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
