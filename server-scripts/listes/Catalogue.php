<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Toutes les thèmes de formation
	$where = "WHERE 1";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getCatalogueList($where);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
// var_dump($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k)) {
	array_push($response, array(
		'idSousTheme' => $a,
		'idFormateur' => $b,
		'titreSousTheme' => str_replace('"', "'", $c),
		'objectifsSousTheme' => str_replace('"', "'", $d),
		'nomFormateur' => str_replace('"', "'", $e),
		'prenomFormateur' => str_replace('"', "'", $f),
		'titreFonctionFormateur' => str_replace('"', "'", $g),
		'institution' => str_replace('"', "'", $h),
		'emailFormateur' => $i,
		'telephoneFormateur' => $j,
		'adresseCompleteFormateur' => $k
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
