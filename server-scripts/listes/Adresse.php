<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Toutes les adresses
	$whereActif = "WHERE 1";
} else if ($infoRequest == 2) { // Liste des adresses par ID_EMPLOYE
	$id_employe = json_decode($_GET['idEmploye']);
	$whereActif = "WHERE u.ID_EMPLOYE = $id_employe";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getAdresseList($whereActif);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p)) {
	array_push($response, array(
		'idFokotany' => $a,
		'idCommune' => $b,
		'idDistrict' => $c,
		'idRegion' => $d,
		'idProvince' => $e,
		'idEmploye' => $f,
		'idAdresse' => $g,
		'actifAdresse' => $h > 0 ? true : false,
		'logementEmploye' => $i,
		'nomFokotany' => $j,
		'nomCommune' => $k,
		'nomDistrict' => $l,
		'nomRegion' => $m,
		'nomProvince' => $n
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
