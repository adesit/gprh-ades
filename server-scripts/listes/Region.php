<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getRegionList();

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e)) {
	array_push($response, array(
		'idRegion' => $a,
		'idProvince' => $b,
		'codeRegion' => $c,
		'nomRegion' => $d,
		'codeProvince' => $e
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
