<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Tous les taux des H. Supp
	$where = "WHERE 1";
} else if ($infoRequest == 2) { // Le taux actif pour une augmentation précise
	$id_employe = json_decode($_GET['nomTaux']);
	$where = "WHERE NATURE_HS_BASE = '$nomTaux'". " AND ACTIF_HS_BASE = 1";
}

$res=$m->getTauxHSList($where);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e)) {
	array_push($response, array(
		'idHsBase' => $a,
		'natureHsBase' => $b,
		'formuleCalcul' => $c,
		'dateEffetHs' => $d,
		'actifHsBase' => $e > 0 ? true : false
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
