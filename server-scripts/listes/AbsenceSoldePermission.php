<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Toutes les PERMISSIONS d'un employé par son ID_CONTRAT dans une année
	$annee_en_cours = date("Y");
	$id_Contrat = json_decode($_GET['idContrat']);
	$where = "WHERE h.ID_CONTRAT = $id_Contrat AND h.TYPE_ABSENCE = 2 AND YEAR(h.DATE_HEURE_FIN_HS) = '$annee_en_cours'";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getPermissionSolde($where);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b)) {
	array_push($response, array(
		'idContrat' => $a,
		'soldePermission' => 10 - $b // au max le total des jours non travaillés à titre de permission pour évènements familiaux = 10
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>