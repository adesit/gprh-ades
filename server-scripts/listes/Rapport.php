<?php
require_once '../DataAccessObject.php';

$dao = new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_REQUEST['infoRequest']);
$typeUtilisateur = json_decode($_GET['typeUtilisateur']);
$idCentre = json_decode($_GET['idCentre']);

if ($infoRequest == 1) {
	$wherePersonnel = "c.ACTIF_CONTRAT = 1";
	$whereEmbauche = "c.DATE_DEBAUCHE is null";
	$whereDebauche = "c.DATE_DEBAUCHE is not null";
	$whereFormation = "1";
	$whereAnnee = date('Y');
	$whereAbsence = " WHERE 1";
	$whereConge = " WHERE 1";

	if ($typeUtilisateur == 1) $wherePersonnel .= "";
	else if ($typeUtilisateur == 2 && !empty($idCentre)) $wherePersonnel .= " AND c.ID_CENTRE = $idCentre";
} else if ($infoRequest == 2) {
	$dateDebut = $_REQUEST['dateDebut'];
	$dateFin = $_REQUEST['dateFin'];
	$wherePersonnel = "c.DATE_EMBAUCHE >= '$dateDebut' AND c.DATE_EMBAUCHE <= '$dateFin'";
	$whereEmbauche = "c.DATE_EMBAUCHE >= '$dateDebut' AND c.DATE_EMBAUCHE <= '$dateFin'";
	$whereDebauche = "c.DATE_DEBAUCHE >= '$dateDebut' AND c.DATE_DEBAUCHE <= '$dateFin'";
	$whereFormation = "u.DATE_DEBUT_FORMATION <= '$dateFin' AND u.DATE_FIN_FORMATION >= '$dateDebut'";
	$whereAnnee = date('Y', strtotime($dateFin));
	$whereAbsence = " WHERE h.DATE_HEURE_DEBUT_HS >= '$dateDebut' AND h.DATE_HEURE_FIN_HS <= '$dateFin'";
	$whereConge = " WHERE h.DATE_DEBUT_CONGE >= '$dateDebut' AND h.DATE_FIN_CONGE <= '$dateFin'";

	if ($typeUtilisateur == 1) $wherePersonnel .= "";
	else if ($typeUtilisateur == 2 && !empty($idCentre)) $wherePersonnel .= " AND c.ID_CENTRE = $idCentre";
} else if ($infoRequest == 3) {
	$annee = $_REQUEST['anneeRecherche'];
	$whereAnnee = $annee;
	goto inspection;
} else if ($infoRequest == 4) {
	$annee = $_REQUEST['anneeRecherche'];
	$whereAnnee = $annee;
	goto salaire;
	goto tout;
}

// PERSONNEL
// Nombre de personnel d'ADES actuel
	$nbPersonnel = $dao -> rapportNombrePersonnel($wherePersonnel);
	$personnelParSexe = ['homme' => (isset($nbPersonnel[0]) ? $nbPersonnel[0][0] : 0), 'femme' => (isset($nbPersonnel[1]) ? $nbPersonnel[1][0] : 0)];

	$arr['personnel'] = [$personnelParSexe];

// Nombre de personnel par âge et par sexe
	$nbPersonnelParAge = $dao -> rapportAgePersonnel($wherePersonnel);
	$lesAges = array_unique(array_column($nbPersonnelParAge, 0));
	$lesAges1 = array_values($lesAges);
	$nbPersonnelParAgeFormatted = array();
	$nbPersonnelParAgeForChart = array();
	
	foreach($lesAges1 as $age) {
		$hommeResult = multiSearch($nbPersonnelParAge, array(0 => $age, 1 => '1'));
		$homme = array_values($hommeResult);
		if (count($homme) != 0) {
			$nbHomme = $homme[0][2];
		} else {
			$nbHomme = 0;
		}

		$femmeResult = multiSearch($nbPersonnelParAge, array(0 => $age, 1 => '2'));
		$femme = array_values($femmeResult);
		if (count($femme) != 0) {
			$nbFemme = $femme[0][2];
		} else {
			$nbFemme = 0;
		}

		array_push($nbPersonnelParAgeFormatted, array(
			'age' => $age,
			'homme' => intval($nbHomme),
			'femme' => intval($nbFemme),
			'totalParSexe' => intval($nbHomme) + intval($nbFemme)
		));

		$nbPersonnelParAgeForChart[$age] =  intval($nbHomme) + intval($nbFemme);
	}
	$arr['agePersonnel'] = $nbPersonnelParAgeFormatted;

	$intervallesAge = array();
	$intervallesAge[] = array(0, 17, 30, 40, 50, 60);
	$intervallesAge[] = array(16, 29, 39, 49, 59, 80);
	$structureAge = array();

	for ($i = 0; $i <= 5; $i++) {
		array_push($structureAge, array(
			'age' => $intervallesAge[0][$i].'-'.$intervallesAge[1][$i].' ans',
			'data1' => sumArray($nbPersonnelParAgeForChart, $intervallesAge[0][$i], $intervallesAge[1][$i])
		));
	}
	$arr['structureAge'] = $structureAge;

// ------------------------------------------------- //
$arr['total'] = array_sum(array_column($nbPersonnel, 0));

// Nombre d'enfants du personnel avec contrat en cours, retranché par age et par sexe
	$nbEnfantPersonnel = $dao -> rapportEnfantPersonnel($wherePersonnel);
	$nbEnfantPersonnelParAge = array();
	foreach ($nbEnfantPersonnel as list($a, $b, $c, $d)) {
		array_push($nbEnfantPersonnelParAge, array(
			'age' => $a,
			'homme' => intval($b),
			'femme' => intval($c),
			'total' => intval($d)
		));
	}
	$arr['ageEnfantPersonnel'] = $nbEnfantPersonnelParAge;

// Totalité des frais de scolarité des enfants
	$fraisParAnneeScolariteDao = $dao -> rapportFraisScolarite($wherePersonnel);
	$fraisParAnneeScolariteArray = array();
	foreach ($fraisParAnneeScolariteDao as list($a, $b, $c)) {
		array_push($fraisParAnneeScolariteArray, array(
			'anneeScolaire' => $a,
			'fraisScolarite' => floatval($b),
			'nbEnfant' => intval($c)
		));
	}
	$arr['fraisScolariteEnfant'] = $fraisParAnneeScolariteArray;

// CONTRAT
// Nb de contrat par centre par sexe
	$contratParCentreDao = $dao -> rapportContratParCentre($wherePersonnel);
	$contratParCentreArray = array();
	foreach ($contratParCentreDao as list($a, $b, $c)) {
		array_push($contratParCentreArray, array(
			'centre' => $a,
			'homme' => intval($b),
			'femme' => intval($c)
		));
	}
	$arr['contratParCentre'] = $contratParCentreArray;

// Nb de contrat par poste par sexe
	$contratParPosteDao = $dao -> rapportContratParPoste($wherePersonnel);
	$contratParPosteArray = array();
	foreach ($contratParPosteDao as list($a, $c, $d)) {
		array_push($contratParPosteArray, array(
			'poste' => $a,
			'homme' => intval($c),
			'femme' => intval($d)
		));
	}
	$arr['contratParPoste'] = $contratParPosteArray;

// Nb de contrat par ancienneté par sexe
	$contratParAncienneteDao = $dao -> rapportContratParAnciennete($wherePersonnel);
	$contratParAncienneteArray = array();
	foreach ($contratParAncienneteDao as list($a, $c, $d)) {
		array_push($contratParAncienneteArray, array(
			'anneeAnciennete' => $a,
			'homme' => intval($c),
			'femme' => intval($d)
		));
	}
	$arr['contratParAnciennete'] = $contratParAncienneteArray;

// EVOLUTION 
// Effectif par année et par sexe
	$evolutionAnneeSexeDao = $dao -> callFunctionEvolution();
	$evolutionAnneeSexeDao = $dao -> rapportEvolutionEffectif();
	$evolutionAnneeSexeArray = array();
	foreach ($evolutionAnneeSexeDao as list($a, $b, $c, $d, $e, $f, $g, $h)) {
		array_push($evolutionAnneeSexeArray, array(
			'anneeEmbauche' => $a,
			'hommeEntrant' => intval($b),
			'femmeEntrant' => intval($c),
			'hommeDebauche' => intval($d),
			'femmeDebauche' => intval($e),
			'totalEntrant' => intval($f),
			'totalDebauche' => intval($g),
			'totalEffectif' => intval($h)
		));
	}
	$arr['effectifParAnnee'] = $evolutionAnneeSexeArray;

// RESUME
	$reqEntrantDao = $dao -> nouveauEntrants($whereEmbauche);
	$reqSortantDao = $dao -> nouveauEntrants($whereDebauche);
	$reqFormationDao = $dao -> rapportFormation($whereFormation);
	$formationIntern = 0;
	$formationExtern = 0;
	foreach($reqFormationDao as list($a, $b)) {
		if ($b == 1) $formationIntern += $a;
		else $formationExtern += $a;
	}
	$resumeArray = array(
		array('indicateur' => 'Personnel engagé à Madagascar', 'valeurIndicateur' => array_sum(array_column($nbPersonnelParAgeFormatted,'totalParSexe'))),
		array('indicateur' => 'Personnel féminin', 'valeurIndicateur' => array_sum(array_column($nbPersonnelParAgeFormatted,'femme'))),
		array('indicateur' => 'Personnel masculin', 'valeurIndicateur' => array_sum(array_column($nbPersonnelParAgeFormatted,'homme'))),
		array('indicateur' => 'Prestataires sous contrat', 'valeurIndicateur' => 'Données introuvables'),
		array('indicateur' => 'Arrivées durant la période du rapport', 'valeurIndicateur' => $reqEntrantDao[0][0]),
		array('indicateur' => 'Départs durant la période du rapport', 'valeurIndicateur' => $reqSortantDao[0][0]),
		array('indicateur' => 'Stagiaires formés', 'valeurIndicateur' => 'Données introuvables'),
		array('indicateur' => 'Stagiaires engagés', 'valeurIndicateur' => 'Données introuvables'),
		array('indicateur' => 'Employé(e)s formé(e)s par ADES', 'valeurIndicateur' => $formationIntern),
		array('indicateur' => 'Employé(e)s formé(e)s à l\'extérieur', 'valeurIndicateur' => $formationExtern),
		array('indicateur' => 'Enfants dont les frais d\'écolage sont pris en charge par ADES', 'valeurIndicateur' => array_sum(array_column($fraisParAnneeScolariteArray,'nbEnfant'))),
		array('indicateur' => 'Enfants de sexe masculin', 'valeurIndicateur' => array_sum(array_column($nbEnfantPersonnelParAge,'homme'))),
		array('indicateur' => 'Enfants de sexe féminin', 'valeurIndicateur' => array_sum(array_column($nbEnfantPersonnelParAge,'femme'))));

	$arr['resumeParametrable'] = $resumeArray;


// Salaire brut (sans HS, sans Prime exceptionnelle, sans Salaire intérim, sans Congé de maternité) = Nouveau Salaire brut
	$evolutionSalaireAnneeDao = $dao -> rapportEvolutionSalaire();
	$evolutionSalaireAnneeArray = array();
	foreach ($evolutionSalaireAnneeDao as list($a, $b, $c, $d, $e, $f, $g, $h)) {
		array_push($evolutionSalaireAnneeArray, array(
			'anneeSalaire' => $a,
			'salaireBase' => intval($b),
			'salaireBrut' => intval($c),
			'sBrutAvMaternite' => intval($d),
			'cnaps' => intval($e),
			'smie' => intval($f),
			'irsaNet' => intval($g),
			'netAPayer' => intval($h)
		));
	}
	$arr['salairesParAnnee'] = $evolutionSalaireAnneeArray;

// SALAIRE
salaire:
	$salaireRapportDao = $dao -> rapportSalaire($whereAnnee);
	$salaireRapportArray = array();
	foreach ($salaireRapportDao as list($a, $b, $c, $d, $e, $f, $g, $h, $i)) {
		array_push($salaireRapportArray, array(
			'categorieProfessionnelle' => $a,
			'salaireBase' => intval($c),
			'salaireBrut' => intval($d),
			'sBrutAvMaternite' => intval($e),
			'cnaps' => intval($f),
			'smie' => intval($g),
			'irsaNet' => intval($h),
			'netAPayer' => intval($i)
		));
	}
	$arr['salaireRapport'] = $salaireRapportArray;

// Rapport selon l'inspection de travail - catégorie professionnelle
inspection:
	$inspectionRapportDao = $dao -> rapportInspection($whereAnnee);
	$inspectionRapportArray= array();
	foreach ($inspectionRapportDao as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n)) {
		array_push($inspectionRapportArray, array(
			'categorieProfessionnelle' => $a,
			'homme' => intval($b),
			'femme' => intval($c),
			'netAPayer' => intval($d),
			'cnaps' => intval($e),
			'smie' => intval($f),
			'hommeEmbauche' => intval($g),
			'femmeEmbauche' => intval($h),
			'hommeDemission' => intval($i),
			'femmeDemission' => intval($j),
			'hommeLicenciement' => intval($j),
			'femmeLicenciement' => intval($l),
			'hommeChTec' => intval($m),
			'femmeChTec' => intval($n)
		));
	}
	$arr['inspectionRapport'] = $inspectionRapportArray;

// ABSENCE 
absence:
	$absenceRapportDao = $dao -> getAbsenceListDetails($whereAbsence);
	$absenceRapportArray = array();
	$congeRapportDao = $dao -> getCongeListDetails($whereConge);
	foreach ($absenceRapportDao as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p)) {
		array_push($absenceRapportArray, array(
			'Date début' => date("Y-m-d",strtotime($e)),
			'Date fin' => date("Y-m-d",strtotime($f)),
			'Motif' => $d,
			'typeAbs' => $h, // 1=Abs sans motif, 2=Permission, 3=Abs autorisée
			'Centre' => $o,
			'Date' => date("Y-m-d",strtotime($p))
		));
	}

	foreach ($congeRapportDao as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q)) {
		array_push($absenceRapportArray, array(
			'Date début' => $e,
			'Date fin' => $f,
			'Motif' => $g,
			'typeAbs' => ($i == '1') ? '4' : '5', // 4=Congé de maternité, 2=Congé
			'Centre' => $p,
			'Date' => date("Y-m-d",strtotime($q))
		));
	}

	$arr['absences'] = $absenceRapportArray;

// Mettre toutes les données dans un seul tableau	
tout:
$arr = json_encode($arr);
echo $arr;


// rechercher des pairs key => value dans un array
function multiSearch(array $array, array $pairs)
{
	$found = array();
	foreach ($array as $aKey => $aVal) {
		$coincidences = 0;
		foreach ($pairs as $pKey => $pVal) {
			if (array_key_exists($pKey, $aVal) && $aVal[$pKey] == $pVal) {
				$coincidences++;
			}
		}
		if ($coincidences == count($pairs)) {
			$found[$aKey] = $aVal;
		}
	}

	return $found;
}

// somme des valeurs dont les clés deviennent des intervalles
function sumArray($array, $min, $max) {
	$sum = 0;
	foreach ($array as $k => $a) {
	   if ($k >= $min && $k <= $max) {
		  $sum += $a;
	   }
	}
	return $sum;
 }
?>
