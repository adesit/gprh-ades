<?php
require_once '../DataAccessObject.php';
require_once '../formule/AppliquerFormule.php';

$dao = new DataAccessObject();
$formule = new AppliquerFormule();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Tous les congés
	$typeUtilisateur = json_decode($_GET['typeUtilisateur']);
	$idCentre = json_decode($_GET['idCentre']);
	if ($typeUtilisateur == 1) $where = "WHERE 1";
	else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE c.ID_CENTRE = $idCentre";
} else if ($infoRequest == 2) { // Tous les congés d'un employé par son ID_CONTRAT 
	$id_Contrat = json_decode($_GET['idContrat']);
	$where = "WHERE h.ID_CONTRAT = $id_Contrat";
} else if ($infoRequest == 3) { // Tous les congés d'un mois de salaire
	$moisSalaire = json_decode($_GET['moisSalaire']);
	$typeUtilisateur = json_decode($_GET['typeUtilisateur']);
	$idCentre = json_decode($_GET['idCentre']);
	if ($typeUtilisateur == 1) $where = "WHERE h.ID_SALAIRE = $moisSalaire";
	else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE h.ID_SALAIRE = $moisSalaire AND c.ID_CENTRE = $idCentre";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$dao->getCongeList($where);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();


foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o)) {
	$whereSalaire = "WHERE MOIS_SALAIRE = $k AND ANNEE_SALAIRE = $l";
	$moisSalarialID = $dao -> getSalaireList($whereSalaire);
	
	$mois_correct = (strlen($k) == 1) ? "0".$k : $k;
	$date_du_jour = $l."-".$mois_correct."-24";
	$date_du_jour = strtotime($date_du_jour);
	$date_fin_mois = new DateTime(date("Y-m-d",strtotime("last day of this month", $date_du_jour)));

	$whereConge = "WHERE h.ID_CONTRAT = $b AND h.ID_SALAIRE = ".$moisSalarialID[0][0]." AND h.NATURE_CONGE = 2";
	$CongeList = $dao -> getCongeList($whereConge);
	$totalCongePris = $formule -> calculCongePris($CongeList);
	$whereCongeSolde = "WHERE h.ID_CONTRAT = $b";
	$droitConge = ($m == 'CM' || $m == 'CMc') ? 3.1 : 2.5; // Le droit de congé pour les centres mobiles est de 3.1 les autres est de 2.5
	$soldeCongeDao = $dao -> getSoldeConge($whereCongeSolde); // Solde avant la déduction des congés
	
	$soldeConge = $formule -> calculSoldeConge($soldeCongeDao, $date_fin_mois, $droitConge, $totalCongePris);
	$nouveauSoldeConge = $soldeConge;

	array_push($response, array(
		'idConge' => $a,
		'idContrat' => $b,
		'idSalaire' => $c,
		'reliquat' => $nouveauSoldeConge,
		'dateDebutConge' => $e,
		'dateFinConge' => $f,
		'remarqueConge' => $g,
		'dureeCongePrise' => $h,
		'natureConge' => $i,
		'matricule' => $j,
		'mois' => $k,
		'annee' => $l,
		'nom' => $n,
		'prenom' => $o
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;

?>
