<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Toutes les informations bancaires
	$whereActif = "WHERE 1";
} else if ($infoRequest == 2) { // Toutes les informations bancaires d'un employé par son ID_EMPLOYE 
	$id_employe = json_decode($_GET['idEmploye']);
	$whereActif = "WHERE ID_EMPLOYE = $id_employe";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getBankList($whereActif);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h, $i)) {
	array_push($response, array(
		'idBancaire' => $a,
		'idEmploye' => $b,
		'nomBanque' => $c,
		'codeBanque' => $d,
		'numeroCompte' => $e,
		'codeGuichet' => $f,
		'cleRib' => $g,
		'datePriseEffetBanque' => $h,
		'actifBancaire' => $i > 0 ? true : false,
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
