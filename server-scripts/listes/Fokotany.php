<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();
$r=$m->getFokotanyList();

$n=count($r);
$response = array();
foreach ($r as list($a, $b, $c, $d, $e)) {
	array_push($response, array(
		'idFokotany' => $a,
		'idCommune' => $b,
		'codeFokotany' => $c,
		'nomFokotany' => $d,
		'codeCommune' => $e
	));
}

$arr['data'] = array_values($response);

$arr = json_encode($arr);
echo $arr;
?>
