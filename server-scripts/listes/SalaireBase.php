<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Toutes les lignes de (Groupe & Catégorie) dont la valeur ACTIF_LIGNE_GRILLE = 1
	$whereActif = 'WHERE s.ACTIF_LIGNE_GRILLE = 1';
} else if ($infoRequest == 2) { // Toutes les lignes de (Groupe & Catégorie)
	$id_groupe = json_decode($_GET['idGroupe']);
	$id_categorie = json_decode($_GET['idCategorie']);
	$whereActif = "WHERE s.ID_GROUPE_CATEGORIE = $id_groupe AND s.ID_CATEGORIE = $id_categorie";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getSalaireBaseList($whereActif);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k)) {
	array_push($response, array(
		'idLigneGrille' => $a,
		'idGroupe' => $b,
		'idCategorie' => $c,
		'salaireBaseMinimum' => $d,
		'salaireBase3' => $e,
		'salaireBase4' => $f,
		'salaireBase5' => $g,
		'actifLigneGrille' => $h > 0 ? true : false ,
		'datePriseEffetGrille' => $i,
		'nomGroupe' => $j,
		'categorieProfessionnelle' => $k

	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
