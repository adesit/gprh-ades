<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// Récupère la liste de toutes les adresses en fonction des conditions
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Liste de toutes les qualifications
	$whereActif = "WHERE 1";
} else if ($infoRequest == 2) { // Liste des qualification par ID_EMPLOYE
	$id_employe = json_decode($_GET['idEmploye']);
	$whereActif = "WHERE ID_EMPLOYE = $id_employe";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getJuridiqueList($whereActif);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e)) {
	array_push($response, array(
		'idJuridique' => $a,
		'idEmploye' => $b,
		'natureCondamnation' => $c,
		'dateCondamnation' => $d,
		'dureeCondamnation' => $e
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
