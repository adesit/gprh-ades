<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$resultats=$m->getPosteAncienneteList();

$res1;

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Tous les postes mais affichés par palier de 25
	$res1 = array_slice($resultats, $start, $limit, true);
} else if ($infoRequest == 2) { // Tous les postes
	$res1 = $resultats;
}

$nb=count($resultats);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $y, $z)) {
	array_push($response, array(
		'idPoste' => $a,
		'titrePoste' => $b,
		'descriptionPoste' => str_replace('"', "'",$c),
		'un' => $d,
		'deux' => $e,
		'trois' => $f,
		'quatre' => $g,
		'cinq' => $h,
		'six' => $i,
		'sept' => $j,
		'huit' => $k,
		'neuf' => $l,
		'dix' => $m,
		'onze' => $n,
		'douze' => $o,
		'treize' => $p,
		'quatorze' => $q,
		'quinze' => $r,
		'seize' => $s,
		'dixsept' => $t,
		'dixhuit' => $u,
		'dixneuf' => $v,
		'vingt' => $w,
		'tauxHoraire' => $y,
		'categorieReelle' => $z
	));
	
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
