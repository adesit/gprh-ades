<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_REQUEST['infoRequest']);
$idSessionFormation = json_decode($_REQUEST['idSessionFormation']);

if ($infoRequest == 1) { // Résultats de l'évaluation de toutes les sessions de formation
	$where = "WHERE 1";
	$whereSession = "";
} else if ($infoRequest == (2 || 3 || 4 || 5 || 6)) { // Résultats de l'évaluation d'une sessions de formation par son ID
	$where = "WHERE u.ID_SESSION_FORMATION = $idSessionFormation";
}

$resultats=$m->getResultatsEvaluationList($where);
$themes=$m->getFormationListe($where);

$nb=count($resultats);

$response = array();
$response2 = array();
$response3 = array();


foreach ($resultats as list($b)) {
	// Toutes les données
	$obj = json_decode($b);
	array_push($response, $obj);

	// Les fields FLQn
	$obj2 = json_decode($b, true);
	unset($obj2['idEvaluationFormation']);
	unset($obj2['idInscription']);
	unset($obj2['idApprenant']);
	unset($obj2['idSessionFormation']);
	unset($obj2['nomApprenant']);
	unset($obj2['prenomApprenant']);
	unset($obj2['remarques']);
	array_push($response2, $obj2);
	
	// Les valeurs des fields FLQn
	$nbKeys = count($obj2);
	$moyenneGenerale = array_sum(array_values($obj2)) / $nbKeys;
	array_push($response3, (object)['moyenneGenerale' => round($moyenneGenerale)]);
}

if ($infoRequest == 2) {
	if ($nb > 0) {
		$fields = array_keys(get_object_vars(json_decode($resultats[0][0])));
		$arr['fielddata'] = array_values($fields);
	}
	$arr['data'] = array_values($response);
	$arr['total'] = $nb;
	
	$arr = json_encode($arr);
	echo $arr;
} else if ($infoRequest == 3) {
	$arr['data'] = array_values($response2);
	$arr['total'] = $nb;
	
	$arr = json_encode($arr);
	echo $arr;
} else if ($infoRequest == 4) {
	$resultatsEvaluationGenerale = [];
	$note = array('5', '4', '3', '2', '1');
	foreach ($response3 as $object) {
		if (isset($object -> moyenneGenerale)) {
			$hole = $object -> moyenneGenerale;
			if (!isset($resultatsEvaluationGenerale[$hole])) {
				$resultatsEvaluationGenerale[$hole] = 0;
			}
			$resultatsEvaluationGenerale[$hole]++;
		}
	}

	$keys = array_keys($resultatsEvaluationGenerale);
	$noteGenerale = array();
	foreach ($note as $decision) {
		if (in_array($decision, $keys)) {
			$valeur = $resultatsEvaluationGenerale[$decision];
		} else $valeur = 0;
		
		switch ($decision) {
			case '1':
				$decisionFinale = "Médiocre";
				break;
			case '2':
				$decisionFinale = "Passable";
				break;
			case '3':
				$decisionFinale = "Moyen";
				break;
			case '4':
				$decisionFinale = "Très bien";
				break;
			case '5':
				$decisionFinale = "Excellent";
				break;
		}

		array_push($noteGenerale, array(
			'noteGenerale' => $decisionFinale,
			'nbVote' => $valeur
		));
	}

	$arr['generalite'] = array_values($noteGenerale);
	$arr['total'] = $nb;

	/*
	 * Le but est d'avoir un tableau d'array
	 * array (size=7)
      'FLQ1' => moyenne de FLQ1
      'FLQ2' => moyenne de FLQ2
      'FLQ3' => moyenne des FLQ3
      'FLQ4' => moyenne des FLQ4
      'FLQ5' => moyenne des FLQ5
      'FLQ6' => moyenne de FLQ6
	  'FLQ7' => moyenne de FLQ7
	  */
	  $refonteInsideArray = array();
	  $refonteAll = array();
	  foreach ($response2 as $list) {
		  $avgFLQ3 = 0;
		  $avgFLQ4 = 0;
		  $avgFLQ5 = 0;
		  $i = 0;
		  $j = 0;
		  $k = 0;
		  foreach($list as $key => $value) {
			  if (preg_match('/^FLQ3/i',$key)){
				  $i++;
				  $newKey = 'FLQ3';
				  $avgFLQ3 += $value;
				  $newValue = $avgFLQ3 / $i;
			  } else if (preg_match('/^FLQ4/i',$key)) {
				  $j++;
				  $newKey = 'FLQ4';
				  $avgFLQ4 += $value;
				  $newValue = $avgFLQ4 / $j;
			  } else if (preg_match('/^FLQ5/i',$key)) {
				  $k++;
				  $newKey = 'FLQ5';
				  $avgFLQ5 += $value;
				  $newValue = $avgFLQ5 / $k;
			  } else {
				  $newKey = $key;
				  $newValue = $value;
			  }
			  $refonteInsideArray[$newKey] = round($newValue);
		  }
		  array_push($refonteAll, $refonteInsideArray);
	  }
	  // var_dump($refonteAll);
	  /*
	   * Le but est d'avoir un tableau d'array de la forme
	   * array (size=7)
		'note' => 'mediocre'
		'FLQ1' => count FLQ1 where value = 1
		'FLQ2' => count FLQ2 where value = 1
		'FLQ3' => count FLQ3 where value = 1
		'FLQ4' => count FLQ4 where value = 1
		'FLQ5' => count FLQ5 where value = 1
		'FLQ6' => count FLQ6 where value = 1
		'FLQ7' => count FLQ7 where value = 1
	   */
	$notes = array(5 => 'Excellent', 4 => 'Très bien', 3 => 'Moyen', 2 => 'Passable', 1 => 'Médiocre');
	$arrayNote = array();
	foreach ($notes as $note => $critere) {
		$noteFLQ1 = 0;
		$noteFLQ2 = 0;
		$noteFLQ3 = 0;
		$noteFLQ4 = 0;
		$noteFLQ5 = 0;
		$noteFLQ6 = 0;
		$noteFLQ7 = 0;
		foreach($refonteAll as $evaluation) {
			if ($evaluation['FLQ1'] == $note) $noteFLQ1++;
			if ($evaluation['FLQ2'] == $note) $noteFLQ2++;
			if ($evaluation['FLQ3'] == $note) $noteFLQ3++;
			if ($evaluation['FLQ4'] == $note) $noteFLQ4++;
			if ($evaluation['FLQ5'] == $note) $noteFLQ5++;
			if ($evaluation['FLQ6'] == $note) $noteFLQ6++;
			if ($evaluation['FLQ7'] == $note) $noteFLQ7++;
		}
		array_push($arrayNote, array(
			'note'=> $critere,
			'FLQ1'=> $noteFLQ1,
			'FLQ2'=> $noteFLQ2,
			'FLQ3'=> $noteFLQ3,
			'FLQ4'=> $noteFLQ4,
			'FLQ5'=> $noteFLQ5,
			'FLQ6'=> $noteFLQ6,
			'FLQ7'=> $noteFLQ7
		));		
	}
	
	$arr['questionnaire'] = array_values($arrayNote);

	/*
	 * Le but est d'avoir un tableau d'array
	 * array (size=variable en fonction du nombre de thème)
      'question' => 'Qualité de la formation'
      'THi' => moyenne de FLQ1 pour le thème ii
      'THii' => moyenne de FLQ1 pour le thème ii
      'THiii' => moyenne de FLQ1 pour le thème iii
      '...' => moyenne de FLQ1 pour le thème ii...
	  'THn' => moyenne de FLQ1 pour le thème n
	  */
	$questionnairesArray = [
		['abreviation' => 'FLQ1', 'critere' => 'Qualité de la formation'],
		['abreviation' => 'FLQ2', 'critere' => 'Organisation générale'],
		['abreviation' => 'FLQ3', 'critere' => 'Rythme'],
		['abreviation' => 'FLQ4', 'critere' => 'Durée'],
		['abreviation' => 'FLQ5', 'critere' => 'L\'enseignement et/ou le formateur'],
		['abreviation' => 'FLQ6', 'critere' => 'Perfectionner vos connaissances'],
		['abreviation' => 'FLQ7', 'critere' => 'Suivi particulier à la suite'],
	];

	$arr['questions'] = $questionnairesArray;
	
	$recapByTheme = array();
	
	// crée le Store pour afficher les notes par thèmes en toile d'araignée
	foreach($questionnairesArray as $critere) {
		$arrayNoteByTheme = array();
		$series = array(); // crée les séries utilisées dans la graphe en toile d'araignée
		
		foreach ($themes as list($q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae)) {
			if (in_array($critere['abreviation'], array('FLQ1', 'FLQ2', 'FLQ6', 'FLQ7'), true)) {
				$mq = array_sum(array_column($response2, $critere['abreviation']));
			} else {
				$keyTH = $critere['abreviation'].'TH'.$ab;
				$mq = array_sum(array_column($response2, $keyTH));
			}
			
			$arrayNoteByTheme['TH'.$ab] = $mq;
			array_push($series, array(
				'type' => 'radar',
				'title' => $ac,
				'angleField' => 'question',
				'radiusField' => 'TH'.$ab,
				'style' => ['opacity' => 0.40],
				'marker' => true,
				'highlightCfg' => [
					'radius' => 6,
					'fillStyle' => 'yellow',
				],
				'tooltip' => [
					'trackMouse' => true,
					'renderer' => 'onSeriesLabelRender',
				]
			));
		}
		array_push($recapByTheme, array_merge(
			['question' => $critere['critere']],
			$arrayNoteByTheme
		));
	}
	
	$arr['byThemeStore'] = $recapByTheme;
	$arr['series'] = $series;

	$arr = json_encode($arr);
	echo $arr;
}

?>