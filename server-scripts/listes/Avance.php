<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Toutes les avances
	$typeUtilisateur = json_decode($_GET['typeUtilisateur']);
	$idCentre = json_decode($_GET['idCentre']);
	if ($typeUtilisateur == 1) $where = "WHERE 1";
	else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE c.ID_CENTRE = $idCentre";
} else if ($infoRequest == 2) { // Toutes les avances d'un employé par son ID_CONTRAT 
	$id_Contrat = json_decode($_GET['idContrat']);
	$where = "WHERE h.ID_CONTRAT = $id_Contrat";
} else if ($infoRequest == 3) { // Toutes les avances d'un employé par son ID_CONTRAT et qui sont en cours
	$id_Contrat = json_decode($_GET['idContrat']);
	$where = "WHERE h.ID_CONTRAT = $id_Contrat AND h.EN_COURS = 1";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getAvanceList($where);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o)) {
	array_push($response, array(
		'idAvance' => $a,
		'idContrat' => $b,
		'maxAutorise' => $c,
		'maxDeductionPossible' => $d,
		'montantAvance' => $e,
		'dateReception' => $f,
		'dateDebutRemboursement' => $g,
		'dateFinRemboursement' => $h,
		'nombreTranche' => $i,
		'deductionMensuelle' => $j,
		'raison' => $k,
		'enCours' => $l > 0 ? true : false,
		'matricule' => $m,
		'nom' => $n,
		'prenom' => $o
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
