<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Toutes les sessions de formation
	$where = "WHERE 1";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getFormationListe($where);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o)) {
	array_push($response, array(
		'idSessionFormation' => $a,
		'themeFormation' => str_replace('"', "'", $b),
		'objectifsFormation' => str_replace('"', "'", $c),
		'dateCreationSession' => $d,
		'dateDebutFormation' => $e,
		'dateFinFormation' => $f,
		'domaineFormation' => $g,
		'adresseSeance' => $h,
		'typeFormation' => $i,
		'niveauCible' => $j,
		'idSousTheme' => $k,
		'idSousThemeST' => $l,
		'titreSousTheme' => str_replace('"', "'", $m),
		'DomaineFormation' => str_replace('"', "'", $n),
		'TypeFormation' => str_replace('"', "'", $o)
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
