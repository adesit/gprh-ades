<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getCategorieList();

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g)) {
	array_push($response, array(
		'idCategorie' => $a,
		'idGroupeCategorie' => $b,
		'categorieProfessionnelle' => $c,
		'descriptionCategorie' => $d,
		'nomGroupe' => $e,
		'nbActifLigneGrille' => ($f > 0 ? $f : 0),
		'nbLigneGrilleByCategorie' => ($g > 0 ? $g : 0)
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
