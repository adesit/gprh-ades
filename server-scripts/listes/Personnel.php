<?php
require_once '../DataAccessObject.php';

$dao=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Tous les personnels
	$typeUtilisateur = json_decode($_GET['typeUtilisateur']);
	$idCentre = json_decode($_GET['idCentre']);
	if ($typeUtilisateur == 1 || $typeUtilisateur == 3) $where = "WHERE 1";
	else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE g.ID_CENTRE = $idCentre";
} else if ($infoRequest == 2) { // Un seul employé 
	$id_employe = json_decode($_GET['idEmploye']);
	$where = "WHERE p.ID_EMPLOYE = $id_employe";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$dao->getPersonnelList($where);

$res1 = array_slice($res, $start, $limit, true);
$response = array();
foreach ($res as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $pa, $pap, $pb, $pc, $pd, $pe, $pf, $r, $s, $t, $u, $v, $w, $x, $y)) {
	array_push($response, array(
		'idEmploye' => $a,
		'idFoko' => $b,
		'civilite' => $c,
		'nom' => $d,
		'prenom' => $e,
		'surnom' => $f,
		'initial' => $g,
		'sexe' => $h,
		'dateNaissance' => $i,
		'lieuNaissance' => $j,
		'nomPere' => $k,
		'nomMere' => $l,
		'numeroCinPasseport' => $m,
		'dateDelivranceCin' => $n,
		'lieuDelivranceCinPasseport' => $o,
		'dateFinValidite' => $p,
		'numCnaps' => $q,
		'permisA' => ($pa > 0 ? $pa : 0),
		'permisAP' => ($pap > 0 ? $pap : 0),
		'permisB' => ($pb > 0 ? $pb : 0),
		'permisC' => ($pc > 0 ? $pc : 0),
		'permisD' => ($pd > 0 ? $pd : 0),
		'permisE' => ($pe > 0 ? $pe : 0),
		'permisF' => ($pf > 0 ? $pf : 0),
		'telMobile' => $r,
		'telFixe' => $s,
		'email' => $t,
		'groupeSanguin' => $u,
		'nbContrat' => $v,
		'nbEnfant' => $x,
		'titrePoste' => $y
	));
}

$nb=count($res);
$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
