<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();
$r=$m->getDistrictList();

$n=count($r);
$response = array();
foreach ($r as list($a, $b, $c, $d, $e)) {
	array_push($response, array(
		'idDistrict' => $a,
		'idRegion' => $b,
		'codeDistrict' => $c,
		'nomDistrict' => $d,
		'codeRegion' => $e
	));
}

$arr['data'] = array_values($response);

$arr = json_encode($arr);
echo $arr;
?>
