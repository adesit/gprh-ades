<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Tous les inscrits 
	$typeUtilisateur = json_decode($_GET['typeUtilisateur']);
	$idCentre = json_decode($_GET['idCentre']);
	if ($typeUtilisateur == 1 || $typeUtilisateur == 3) $where = "WHERE 1";
	else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE c.ID_CENTRE = $idCentre";

	$res=$m->getApprenantList($where);

} else if ($infoRequest == 2) { // Les inscrits par ID_SESSION_FORMATION 
	$id_SessionFormation = json_decode($_GET['idSessionFormation']);
	$where = "WHERE i.ID_SESSION_FORMATION = $id_SessionFormation";

	$res=$m->getApprenantList($where);
	
} else if ($infoRequest == 3) { // Les inscrits qui ne sont pas des employés d'ADES et qui ne sont pas encore inscrits à la formation en question
	$id_SessionFormation = json_decode($_GET['idSessionFormation']);
	$where = "WHERE a.TYPE_RELATION_ADES <> 1 AND i.ID_APPRENANT NOT IN(
		SELECT
			ID_APPRENANT
		FROM
			inscription
		WHERE
			ID_SESSION_FORMATION = $id_SessionFormation
	)";

	$res=$m->getApprenantListDistinct($where);

}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $o, $p)) {
	array_push($response, array(
		'idInscription' => $a,
		'idApprenant' => $b,
		'idSessionFormation' => $c,
		'nomApprenant' => $d,
		'prenomApprenant' => $e,
		'dateNaissanceApprenant' => $f,
		'sexeApprenant' => $g,
		'telephoneApprenant' => $h,
		'adresseCompleteApprenant' => $i,
		'niveau' => $j,
		'typeRelationAdes' => $k,
		'idEmploye' => $l,
		'themeFormation' => $m,
		'nomPrenomEmploye' => $o.' '.$p
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>