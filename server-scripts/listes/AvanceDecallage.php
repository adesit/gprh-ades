<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

$id_Contrat = json_decode($_GET['idContrat']);
$res=$m->getLastAvance($id_Contrat);

$nb=count($res);
$response = array();
foreach ($res as list($a, $b, $c)) {
	array_push($response, array(
		'idAvance' => $a,
		'idContrat' => $b,
		'dateFinRemboursement' => $c
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
