<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Toutes les périodes intérimaires
	$where = "WHERE 1";
} else if ($infoRequest == 2) { // Toutes les périodes intérimaires d'un employé par son ID_CONTRAT 
	$id_Contrat = json_decode($_GET['idContrat']);
	$where = "WHERE ID_CONTRAT = $id_Contrat";
} else if ($infoRequest == 3) { // Toutes les périodes intérimaires d'un mois de salaire
	// TODO déterminer les intervalles de temps récupérer les intérim durant un mois de salaire
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getSalaireInterimList($where);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e)) {
	array_push($response, array(
		'idPrimeIndemnite' => $a,
		'idContrat' => $b,
		'dateDebutInterim' => $c,
		'dateFinInterim' => $d,
		'remarquesInterim' => $e
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
