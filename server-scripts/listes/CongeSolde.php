<?php
require_once '../DataAccessObject.php';
require_once '../formule/AppliquerFormule.php';

$dao=new DataAccessObject();
$formule = new AppliquerFormule();

$date_fin_mois = new DateTime(date("Y-m-d",strtotime("last day of this month")));

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Tous les reliquats de congé
	$typeUtilisateur = json_decode($_GET['typeUtilisateur']);
	$idCentre = json_decode($_GET['idCentre']);
	if ($typeUtilisateur == 1) $where = "WHERE c.ACTIF_CONTRAT = 1 AND c.DATE_PRISE_POSTE <= '".$date_fin_mois->format('Y-m-d')."'";
	else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE c.ACTIF_CONTRAT = 1 and c.ID_CONTRAT = $idContrat AND c.DATE_PRISE_POSTE <= '".$date_fin_mois->format('Y-m-d')."' AND c.ID_CENTRE = $idCentre";
} else if ($infoRequest == 2) { // Tous les reliquats de congé d'un employé par son ID_CONTRAT 
	$idContrat = json_decode($_GET['idContrat']);
	$where = "WHERE c.ACTIF_CONTRAT = 1 and c.ID_CONTRAT = $idContrat AND c.DATE_PRISE_POSTE <= '".$date_fin_mois->format('Y-m-d')."'";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$whereAgeEnfant = "";
$res=$dao->getContratList($where, $whereAgeEnfant);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();

$whereSalaire = "WHERE MOIS_SALAIRE = ".date("m")." AND ANNEE_SALAIRE = ".date("Y");
$moisSalarialID = $dao -> getSalaireList($whereSalaire);

foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae, $af, $ag, $ah, $ai, $aj)) {
	if ($moisSalarialID[0][5] == 1) { // Si le mois de salaire en cours est déjà clôturé, reprendre le solde restant sans rajouter le droit de congé
		$whereConge = "WHERE h.ID_CONTRAT = $a AND h.NATURE_CONGE = 2 AND h.DATE_SAISIE_CONGE > STR_TO_DATE('".$moisSalarialID[0][4]."','%Y-%m-%d')";
		$CongeList = $dao -> getCongeList($whereConge);
		$totalCongePris = $formule -> calculCongePris($CongeList);
		
		$whereCongeSolde = "WHERE h.ID_CONTRAT = $a";
		$soldeCongeDao = $dao -> getSoldeConge($whereCongeSolde); // Solde avant la déduction des congés
		// Solde après la déduction des congés pris après la date clôture du salaire
		$nouveauSoldeConge = $soldeCongeDao[0][2] - $totalCongePris;
	} else { // Si le mois de salaire en cours est encore en cours de traitement, faire le calcul normal de solde de congé
		$whereConge = "WHERE h.ID_CONTRAT = $a AND h.ID_SALAIRE = ".$moisSalarialID[0][0]." AND h.NATURE_CONGE = 2";
		$CongeList = $dao -> getCongeList($whereConge);
		$totalCongePris = $formule -> calculCongePris($CongeList);
		$whereCongeSolde = "WHERE h.ID_CONTRAT = $a";
		$droitConge = ($ah == 'CM' || $ah == 'CMc') ? 3.1 : 2.5; // Le droit de congé pour les centres mobiles est de 3.1 les autres est de 2.5
		
		$soldeCongeDao = $dao -> getSoldeConge($whereCongeSolde); // Solde avant la déduction des congés
		$soldeConge = $formule -> calculSoldeConge($soldeCongeDao, $date_fin_mois, $droitConge, $totalCongePris);
		$nouveauSoldeConge = $soldeConge;
		// Mettre à jour le nouveau solde de congé (si (mois == 6 && nouveauSolde >= 45) ? 45 : nouveauSolde)
	}

	array_push($response, array(
		'idContrat' => $a,
		'reliquat' => $nouveauSoldeConge
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;

?>
