<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$where = '';

$res=$m->getHopitalList($where);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h)) {
	array_push($response, array(
		'idHopital' => $a,
		'idAdes' => $b,
		'pourcentageEmployeHopital' => $c,
		'pourcentageEmployeurHopital' => $d,
		'datePriseEffetHopital' => $e,
		'actifHopital' => $f > 0 ? true : false,
		'idCentre' => $g,
		'nomCentre' => $h
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
