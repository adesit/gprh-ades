<?php
require_once '../DataAccessObject.php';
require_once '../formule/AppliquerFormule.php';

$dao = new DataAccessObject();
$formule = new AppliquerFormule();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Tous les contrats
	$typeUtilisateur = json_decode($_GET['typeUtilisateur']);
	$idCentre = json_decode($_GET['idCentre']);
	if ($typeUtilisateur == 1) $where = "WHERE 1";
	else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE c.ID_CENTRE = $idCentre";
} else if ($infoRequest == 2) { // Tous les contrats d'un employé par son ID_EMPLOYE 
	$id_employe = json_decode($_GET['idEmploye']);
	$where = "WHERE c.ID_EMPLOYE = $id_employe";
} else if ($infoRequest == 3) { // Tous les contrats actifs avec ACTIF_CONTRAT = 1 
	$typeUtilisateur = json_decode($_GET['typeUtilisateur']);
	$idCentre = json_decode($_GET['idCentre']);
	if ($typeUtilisateur == 1) $where = "WHERE c.ACTIF_CONTRAT = 1";
	else if ($typeUtilisateur == 2 && !empty($idCentre)) $where = "WHERE c.ID_CENTRE = $idCentre AND c.ACTIF_CONTRAT = 1";
} else if ($infoRequest == 4) { // Le contrat actif d'un employé par son ID_EMPLOYE 
	$id_employe = json_decode($_GET['idEmploye']);
	$where = "WHERE c.ID_EMPLOYE = $id_employe AND c.ACTIF_CONTRAT = 1";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$whereAgeEnfant = "";
$res=$dao->getContratList($where, $whereAgeEnfant);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n, $o, $p, $q, $r, $s, $t, $u, $v, $w, $x, $y, $z, $aa, $ab, $ac, $ad, $ae, $af, $ag, $ah, $ai, $aj, $ak, $al, $am)) {
	$categorie_reelle = $formule -> recupererCategorieRelle($ag, $ak);
	array_push($response, array(
		'idContrat' => $a,
		'idAdes' => $b,
		'idCentre' => $c,
		'idPoste' => $d,
		'idSection' => $e,
		'idDepartement' => $f,
		'idCategorie' => $g,
		'idEmploye' => $h,
		'idSmie' => $i,
		'typeContrat' => $j,
		'natureContrat' => $k,
		'initialAvenant' => $l,
		'dateEmbauche' => $m,
		'datePrisePoste' => $n,
		'dateExpirationContrat' => $o,
		'horaire' => $p,
		'heureReglementaire' => $q,
		'actifContrat' => $r > 0 ? true : false,
		'periodicitePaiement' => $s,
		'modePaiement' => $t,
		'salaireBase' => $u,
		'tauxHoraire' => $v,
		'motifResiliationContrat' => $w,
		'dateDebauche' => $x,
		'salaireBaseGrille' => $y,
		'matricule' => $z,
		'primeExceptionnelle' => $aa,
		'nbEnfant' => $ab,
		'nom' => $ac,
		'prenom' => $ad,
		'sexe' => $ae,
		'titrePoste' => $af,
		'categorieProfessionnelle' => $ag, // Groupe de catégorie (eg: OP2B-5A-C)
		'abreviationCentre' => $ah,
		'categorieAnciennete' => $ai,
		'appliquerHopital' => $aj,
		'categorieReelle' => $categorie_reelle,
		'flotte' => $al,
		'dateNaissance' => $am
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
