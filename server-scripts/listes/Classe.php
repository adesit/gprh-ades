<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getClasseList();

// $res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res as list($a, $b, $c)) {
	array_push($response, array(
		'idEducation' => $a,
		'rangClasse' => $b,
		'classe' => $c
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
