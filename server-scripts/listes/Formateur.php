<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Tous les formateurs
	$where = "WHERE 1";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getFormateurList($where);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h)) {
	array_push($response, array(
		'idFormateur' => $a,
		'nomFormateur' => $b,
		'prenomFormateur' => $c,
		'titreFonctionFormateur' => $d,
		'institution' => $e,
		'emailFormateur' => $f,
		'telephoneFormateur' => $g,
		'adresseCompleteFormateur' => $h
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
