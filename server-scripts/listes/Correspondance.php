<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) {  // Toutes les personnes contact en cas d'urgence
	$whereActif = "WHERE 1";
} else if ($infoRequest == 2) { // Tous les contacts d'un employé par son ID_EMPLOYE
	$id_employe = json_decode($_GET['idEmploye']);
	$whereActif = "WHERE s.ID_EMPLOYE = $id_employe";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getCorrespondanceList($whereActif);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d)) {
	array_push($response, array(
		'idCorrespondant' => $a,
		'idEmploye' => $b,
		'nomPrenomCorrespondant' => $c,
		'telCorrespondant' => $d
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
