<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();
$r=$m->getFokoList();

$n=count($r);
$response = array();
foreach ($r as list($a, $b)) {
	array_push($response, array(
		'idFoko' => $a,
		'nomFoko' => $b
	));
}

$arr['data'] = array_values($response);

$arr = json_encode($arr);
echo $arr;
?>
