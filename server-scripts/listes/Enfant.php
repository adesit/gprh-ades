<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Tous les enfants
	$whereActif = "WHERE 1";
} else if ($infoRequest == 2) { // Tous les enfants d'un employé par son ID_EMPLOYE 
	$id_employe = json_decode($_GET['idEmploye']);
	$whereActif = "WHERE s.ID_EMPLOYE = $id_employe";
} else if ($infoRequest == 3) { // Tous les enfants agés < 21ans d'un employé par son ID_EMPLOYE 
	$id_employe = json_decode($_GET['idEmploye']);
	$whereActif = "WHERE s.ID_EMPLOYE = $id_employe AND DATE_FORMAT(NOW(),'%Y') - DATE_FORMAT(s.DATE_NAISSANCE,'%Y') < 21";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getEnfantList($whereActif);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g)) {
	array_push($response, array(
		'idEnfant' => $a,
		'idEmploye' => $b,
		'nomEnfant' => $c,
		'prenomEnfant' => $d,
		'dateNaissanceEnfant' => $e,
		'lieuNaissanceEnfant' => $f,
		'genreEnfant' => $g
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
