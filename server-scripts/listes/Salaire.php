<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) {  // Toutes les mois de salaire
	$where = "WHERE 1";
} else if ($infoRequest == 2) { // Le mois de salaire choisi dans une année
	$id_employe = json_decode($_GET['idEmploye']);
	$where = "WHERE s.ID_EMPLOYE = $id_employe";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getSalaireList($where);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f)) {
	array_push($response, array(
		'idSalaire' => $a,
		'moisSalaire' => $b,
		'anneeSalaire' => $c,
		'typeSalaire' => $d,
		'dateClotureSalaire' => $e,
		'clotureSalaire' => $f
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
