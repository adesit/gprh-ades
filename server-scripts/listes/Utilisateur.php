<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// $infoRequest détermine le genre de requête à faire
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Toutes les utilisateurs
	$where = "WHERE 1 AND nom_utilisateur not in ('Narisoa', 'Toussaint')";
} else if ($infoRequest == 2) { // Liste des information de connextion par id_utilisateur
	$id_utilisateur = json_decode($_GET['idUtilisateur']);
	$where = "WHERE u.id_utilisateur = $id_utilisateur";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getUtilisateurList($where);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h)) {
	array_push($response, array(
		'idUtilisateur' => $a,
		'nomUtilisateur' => $b,
		'motDePasse' => $c,
		'centreAffectation' => $d,
		'typeUtilisateur' => $e,
		'majMotDePasse' => $f, // A ne pas afficher
		'avatar' => $g,
		'nomCentre' => $h
	));
}

$arr['data'] = array_values($response); // Le contenu de cet array sera utilisé dans le store
$arr['total'] = $nb; // Cette valeur est utilisée comme le nombre total d'enregistrement dans le grid

$arr = json_encode($arr);
echo $arr;
?>
