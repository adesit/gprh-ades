<?php
require_once '../DataAccessObject.php';

$m=new DataAccessObject();

// Récupère la liste de tous les enfants en fonction des conditions
$infoRequest = json_decode($_GET['infoRequest']);

if ($infoRequest == 1) { // Liste de tous les enfants
	$whereActif = "WHERE 1";
} else if ($infoRequest == 2) { // Liste des enfants par ID_EMPLOYE
	$id_employe = json_decode($_GET['idEmploye']);
	$whereActif = "WHERE e.ID_EMPLOYE = $id_employe";
}

$page = json_decode($_GET['page']);
$start = json_decode($_GET['start']);
$lim = json_decode($_GET['limit']);
$limit = $lim;

$res=$m->getEnfantScolariteList($whereActif);

$res1 = array_slice($res, $start, $limit, true);

$nb=count($res);
$response = array();
foreach ($res1 as list($a, $b, $c, $d, $e, $f, $g, $h, $i, $j, $k, $l, $m, $n)) {
	array_push($response, array(
		'idEnfant' => $a,
		'idEmploye' => $b,
		'nomEnfant' => $c,
		'prenomEnfant' => $d,
		'dateNaissanceEnfant' => $e,
		'lieuNaissanceEnfant' => $f,
		'genreEnfant' => $g,
		'idScolarite' => $h,
		'idEducation' => $i,
		'nomEcole' => $j,
		'adresseEcole' => $k,
		'anneeScolaire' => $l,
		'fraisScolarite' => $m,
		'classe' => $n
	));
}

$arr['data'] = array_values($response);
$arr['total'] = $nb;

$arr = json_encode($arr);
echo $arr;
?>
