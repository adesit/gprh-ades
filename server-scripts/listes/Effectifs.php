<?php
require_once '../DataAccessObject.php';

$dao=new DataAccessObject();

$res = $dao->getCentreEffectif();
$resTotal = $dao->getTotalEffectif();

$response = array();
foreach ($res as list($a, $b, $c)) {
	array_push($response, array(
		'idCentre' => $b,
		'effectif' => $c
	));
}

$responseTotal = array();
foreach ($resTotal as list($a)) {
	array_push($responseTotal, array(
		'totalEffectif' => $a
	));
}

$arr['effectif'] = array_values($response);
$arr['totalEffectif'] = array_values($responseTotal);

$arr = json_encode($arr);
echo $arr;
?>
