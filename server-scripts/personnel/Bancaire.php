<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest == 4) {
	/*
	 * Cherche si une ligne avec la valeur ACTIF_BANCAIRE = 1
	 * Si des lignes existes, il faut les remettre à 0
	 */
	$id = "idBancaire";
	$idBancaire = $model -> $id;
	$checkedId = $m -> CheckExistActif('bancaire', 'ID_BANCAIRE', 'ID_EMPLOYE = '.$data -> idEmploye);
	$arrayOfIds = array();
	foreach ($checkedId as list($a)) {
		array_push($arrayOfIds, $a);
	}
	ResetActifById($m, $arrayOfIds, 0);
		
	ResetActifById($m, [$idBancaire], 1);
}

/*
 * Save or update changed data from Centre grid row editor in Tab Panel
 * 
 */
function SaveOrUpdate($dao, $entry) {
	$val_actif = ($entry -> actifBancaire == false) ? 0 : 1;

	if ($val_actif > 0) {
		$checkedId = $dao -> CheckExistActif('bancaire', 'ID_BANCAIRE', 'ID_EMPLOYE = '.$entry -> idEmploye);
		$arrayOfIds = array();
		foreach ($checkedId as list($a)) {
			array_push($arrayOfIds, $a);
		}
		$checkedId = array_values($arrayOfIds);	
		// if many IDs, explode it in a string separate with coma and put condition with IN clause
		$mystring = implode(', ',$checkedId);
		
		$R = $dao ->  ResetActifValue('bancaire', 'ACTIF_BANCAIRE', 0, 'ID_BANCAIRE', $mystring);
	}

	$SoU = $dao -> saveOrUpdateBancaire(
		$entry -> idBancaire,
		$entry -> idEmploye,
		$entry -> nomBanque,
		$entry -> codeBanque,
		$entry -> numeroCompte,
		$entry -> codeGuichet,
		$entry -> cleRib,
		$entry -> datePriseEffetBanque,
		$val_actif
	);
	echo $SoU;
}


function ResetActifById($dao, $checkedId, $ActifValue) {
	$arrayOfIds = array_values($checkedId);	
	// if many IDs, explode it in a string separate with coma and put condition with IN clause
	$mystring = implode(', ',$arrayOfIds);
	
	$R = $dao ->  ResetActifValue('bancaire', 'ACTIF_BANCAIRE', $ActifValue, 'ID_BANCAIRE', $mystring);

	echo $R;
}
?>
