<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer les renseignements d'un employé
 */
function SaveOrUpdate($dao, $data) {
	$SoU = $dao -> saveOrUpdateEquipement(
		$data -> idEquipement,
		$data -> idEmploye,
		$data -> typeEquipement,
		$data -> marque,
		$data -> numeroSerie,
		$data -> couleur,
		$data -> autresCaracteristiques,
		$data -> dateReception,
		$data -> etatReception,
		$data -> dateRestitution,
		$data -> etatRestitution
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "personnel" où personnel = $data -> idEmploye
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'equipement',
		'ID_EQUIPEMENT',
		$data -> idEquipement
	);
	echo $SoU;
}
?>
