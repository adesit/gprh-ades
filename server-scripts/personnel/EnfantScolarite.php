<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	DeleteEnfant($m, $data);
} else if ($infoRequest === 4) {
	DeleteScolarite($m, $data);
} else if ($infoRequest === 5) {
	$response = array (
		'souScolarite' => SoUScolarite($m, $data, $data -> idEnfant)
	);
	$arr['data'] = array_values($response);

	$arr = json_encode($arr);
	echo $arr;
}

/*
 * Save or update changed data from Renseignement form Panel
 * 
 */
function SaveOrUpdate($dao, $entry) {
	$SoU_Enfant = 0;
	$SoU_Scolarite = 0;
	
	if($dao -> IsNullOrEmptyString($entry -> nomEnfant) !== 1) {
		$SoU_Enfant = SoUEnfant($dao, $entry);
	}
	if($SoU_Enfant > 0 && !empty($entry -> nomEcole)) {
		$SoU_Scolarite = SoUScolarite($dao, $entry, $SoU_Enfant);
	}
	
	$response = array (
		'souEnfant' => $SoU_Enfant,
		'souScolarite' => $SoU_Scolarite
	);
	$arr['data'] = array_values($response);

	$arr = json_encode($arr);
	echo $arr;
}

/*
 * Save or Update child information 
 */
function SoUEnfant($daoParam, $entryParam) {
	return $daoParam -> saveOrUpdateEnfant(
		$entryParam -> idEnfant,
		$entryParam -> idEmploye,
		$entryParam -> nomEnfant,
		$entryParam -> prenomEnfant,
		$entryParam -> dateNaissanceEnfant,
		$entryParam -> lieuNaissanceEnfant,
		$entryParam -> genreEnfant
	);
}

/*
 * Save or Update Scolarite of one child 
 */
function SoUScolarite($daoParam, $entryParam, $id_Enfant) {
	return $daoParam -> saveOrUpdateScolarite(
		$entryParam -> idScolarite,
		$entryParam -> idEducation,
		$id_Enfant,
		$entryParam -> nomEcole,
		$entryParam -> adresseEcole,
		$entryParam -> anneeScolaire,
		$daoParam -> Montant($entryParam -> fraisScolarite)
	);
}

/*
 * Delete record on table conjoint where ID_ENFANT equals to value
 * 
 */
function DeleteEnfant($dao, $data) {
	$SoU = $dao -> DeleteById(
		'enfant',
		'ID_ENFANT',
		$data -> idEnfant
	);
	echo $SoU;
}
/*
 * Delete record on table correspondance where ID_SCOLARITE equals to value
 * 
 */
function DeleteScolarite($dao, $data) {
	$SoU = $dao -> DeleteById(
		'scolarite',
		'ID_SCOLARITE',
		$data -> idScolarite
	);
	echo $SoU;
}
?>
