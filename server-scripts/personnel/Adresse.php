<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Save or update changed data from Centre grid row editor in Tab Panel
 * 
 */
function SaveOrUpdate($dao, $entry) {
	$val_actif = ($dao -> IsNullOrEmptyString(isset($entry -> actifAdresse)) > 0 ? 0 : 1);

	$SoU = $dao -> saveOrUpdateAdresse(
		$entry -> idFokotany,
		$entry -> idCommune,
		$entry -> idDistrict,
		$entry -> idRegion,
		$entry -> idProvince,
		$entry -> idEmploye,
		$entry -> idAdresse,
		$val_actif,
		$entry -> logementEmploye
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "adresse_employe" où ID_ADRESSE = ($data -> idAdresse)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'adresse_employe',
		'ID_ADRESSE',
		$data -> idAdresse
	);
	echo $SoU;
}
?>
