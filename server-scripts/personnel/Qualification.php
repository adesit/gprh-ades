<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
}
/*
 * Insertion ou mise à jour des données sur la qualification (résumé)  de l'employé
 */
function SaveOrUpdate($dao, $entry) {
	$SoU = $dao -> saveOrUpdateQualification(
		$entry -> idQualification,
		$entry -> idEmploye,
		$entry -> anneeExperience,
		$entry -> niveauFormation,
		$entry -> intituleDiplome,
		$entry -> ecoleUniversite
	);
	echo $SoU;
}
?>
