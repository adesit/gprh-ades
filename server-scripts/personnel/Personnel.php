<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Enregistrer les renseignements d'un employé
 */
function SaveOrUpdate($dao, $entry) {
	$val_a = ($dao -> IsNullOrEmptyString(isset($entry -> permisA)) > 0 ? 0 : 1);
	$val_ap = ($dao -> IsNullOrEmptyString(isset($entry -> permisAP)) > 0 ? 0 : 1);
	$val_b = ($dao -> IsNullOrEmptyString(isset($entry -> permisB)) > 0 ? 0 : 1);
	$val_c = ($dao -> IsNullOrEmptyString(isset($entry -> permisC)) > 0 ? 0 : 1);
	$val_d = ($dao -> IsNullOrEmptyString(isset($entry -> permisD)) > 0 ? 0 : 1);
	$val_e = ($dao -> IsNullOrEmptyString(isset($entry -> permisE)) > 0 ? 0 : 1);
	$val_f = ($dao -> IsNullOrEmptyString(isset($entry -> permisF)) > 0 ? 0 : 1);

	$SoU = $dao -> saveOrUpdatePersonnel(
		$entry -> idEmploye,
		$entry -> idFoko,
		$entry -> civilite,
		$entry -> nom,
		$entry -> prenom,
		$entry -> surnom,
		$entry -> initial,
		$entry -> sexe,
		$entry -> dateNaissance,
		$entry -> lieuNaissance,
		$entry -> nomPere,
		$entry -> nomMere,
		$entry -> numeroCinPasseport,
		$entry -> dateDelivranceCin,
		$entry -> lieuDelivranceCinPasseport,
		$entry -> dateFinValidite,
		$entry -> numCnaps,
		$val_a,
		$val_ap,
		$val_b,
		$val_c,
		$val_d,
		$val_e,
		$val_f,
		$entry -> telMobile,
		$entry -> telFixe,
		$entry -> email,
		$entry -> groupeSanguin
	);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "personnel" où personnel = $data -> idEmploye
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'personnel',
		'ID_EMPLOYE',
		$data -> idEmploye
	);
	echo $SoU;
}
?>
