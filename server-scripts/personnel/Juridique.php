<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
}
/*
 * Insertion ou mise à jour des données sur la qualification (résumé)  de l'employé
 */
function SaveOrUpdate($dao, $entry) {
	$SoU = $dao -> saveOrUpdateJuridique(
		$entry -> idJuridique,
		$entry -> idEmploye,
		$entry -> natureCondamnation,
		$entry -> dateCondamnation,
		$entry -> dureeCondamnation
	);
	echo $SoU;
}
?>
