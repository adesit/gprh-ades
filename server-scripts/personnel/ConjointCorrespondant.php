<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	DeleteConjoint($m, $data);
} else if ($infoRequest === 4) {
	DeleteCorrespondance($m, $data);
}

/*
 * Save or update changed data from Renseignement form Panel
 * 
 */
function SaveOrUpdate($dao, $entry) {
	$SoU_Conjoint = 0;
	$SoU_Correspondant = 0;
	
	if($dao -> IsNullOrEmptyString($entry -> nomConjoint) !== 1) {
		$SoU_Conjoint = $dao -> saveOrUpdateConjoint(
			$entry -> idConjoint,
			$entry -> idEmploye,
			$entry -> nomConjoint,
			$entry -> prenomConjoint,
			$entry -> dateNaissanceConjoint,
			$entry -> genreConjoint,
			$entry -> fonctionConjoint,
			$entry -> telConjoint
		);
	}
	if($dao -> IsNullOrEmptyString($entry -> nomPrenomCorrespondant) !== 1) {
		$SoU_Correspondant = $dao -> saveOrUpdateCorrespondant(
			$entry -> idCorrespondant,
			$entry -> idEmploye,
			$entry -> nomPrenomCorrespondant,
			$entry -> telCorrespondant
		);
	}
	
	$response = array (
		'souConjoint' => $SoU_Conjoint,
		'souCorrespondant' => $SoU_Correspondant
	);
	$arr['data'] = array_values($response);

	$arr = json_encode($arr);
	echo $arr;
}

/*
 * Delete record on table conjoint where ID_CONJOINT equals to value
 * 
 */
function DeleteConjoint($dao, $data) {
	$SoU = $dao -> DeleteById(
		'conjoint',
		'ID_CONJOINT',
		$data -> idConjoint
	);
	echo $SoU;
}
/*
 * Delete record on table correspondance where ID_CORRESPONDANT equals to value
 * 
 */
function DeleteCorrespondance($dao, $data) {
	$SoU = $dao -> DeleteById(
		'correspondance',
		'ID_CORRESPONDANT',
		$data -> idCorrespondant
	);
	echo $SoU;
}
?>
