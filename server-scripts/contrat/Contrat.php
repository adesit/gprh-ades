<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;

if ($infoRequest == 2) {
	$data_key = "data";
	$data = $model -> $data_key;
	SaveOrUpdate($m, $data);
} else if ($infoRequest == 4) {
	$data = $model -> data;
	/*
	* Cherche si une ligne avec la valeur ACTIF_CONTRAT = 1 pour un employé
	* Si des lignes existes, il faut les remettre à 0
	*/
	$checkedId = $m -> CheckExistActif('contrat', 'ID_CONTRAT', 'ID_EMPLOYE = '.$data -> idEmploye);
	
	ResetActifById($m, $checkedId, 0);
} else if ($infoRequest === 3) {
	$data_key = "data";
	$data = $model -> $data_key;
	Delete($m, $data);
}

/*
 * Table = 'contrat'
 * Insertion si idContrat est 'null'
 * Modification si idContrat != null  
 */
function SaveOrUpdate($dao, $data) {
	$val_actif = $dao -> IsNullOrEmptyString(isset($data -> actifContrat)) > 0 ? 0 : 1;
	$valActifHopital = $dao -> IsNullOrEmptyString(isset($data -> appliquerHopital)) > 0 ? 0 : 1;
	$montantSalaireBaseConclu = $dao -> Montant($data -> salaireBase);
	$montantSalaireBaseGrille = $dao -> Montant($data -> salaireBaseGrille);
	$tauxHoraire = $dao -> Montant($data -> tauxHoraire);
	$heureReglementaire = $dao -> Montant($data -> heureReglementaire);
	$montantprimeExceptionnelle = $dao -> Montant($data -> primeExceptionnelle);
	
	// Fonction pour mettre à jour ou non les effectifs
	$id_employe = $data -> idEmploye;
	$where = "WHERE c.ID_EMPLOYE = $id_employe";
	$whereAgeEnfant = "";
	$anciensContratDao = $dao -> getContratList($where, $whereAgeEnfant);
	$ancienContrat = empty($anciensContratDao) ? 0 : max(array_column($anciensContratDao, 0));
	
	$SoU = $dao -> saveOrUpdateContrat(
		$data -> idContrat,
		1, 
		$data -> idCentre, 
		$data -> idPoste, 
		$data -> idSection, 
		$data -> idDepartement, 
		$data -> categorieAnciennete, 
		$data -> idEmploye, 
		$data -> idSmie, 
		$data -> typeContrat, 
		$data -> natureContrat, 
		$data -> initialAvenant, 
		$data -> dateEmbauche, 
		$data -> datePrisePoste, 
		$data -> dateExpirationContrat, 
		$data -> horaire, 
		$heureReglementaire, 
		$val_actif, 
		$data -> periodicitePaiement, 
		$data -> modePaiement, 
		$montantSalaireBaseConclu, 
		$tauxHoraire, 
		$data -> motifResiliationContrat, 
		$data -> dateDebauche, 
		$montantSalaireBaseGrille, 
		$data -> matricule,
		$montantprimeExceptionnelle,
		$data -> categorieAnciennete,
		$valActifHopital
	);

	if (empty($data -> idContrat) && ($SoU > 0)) {
		$effectif = explode(".", $data -> matricule, 3);
		
		// Cette fonction peut mettre à jour l'effectif par centre et l'effectif total
		saveOrUpdateEffectifs($data -> idCentre, $effectif[1], $ancienContrat, $dao);

		// Attention cette fonction peut démarrer un nouveau solde de congé OU continuer le solde antérieur si c'est un ancien employé
		saveOrUpdateSoldeConge($data -> idContrat, $data -> idEmploye, $SoU, $dao, $data -> idCentre, $val_actif, $data -> datePrisePoste);
	}
	echo $SoU;
}

/*
 * @$checkedId est un tableau de IDs
 * utiliser la méthode 'implode' pour les convertir en string et séparées les valeurs par des virgules
 * Envoyer les ids vers la condition WHERE de la requête qui fait la mise à jour
 */
function ResetActifById($dao, $checkedId, $ActifValue) {
	$result = $dao -> array_flatten($checkedId);

	$mystring = implode(', ', $result);
	$R = $dao ->  ResetActifValue('contrat', 'ACTIF_CONTRAT', $ActifValue, 'ID_CONTRAT', $mystring);

	echo $R;
}

/*
 * Pour un nouveau contrat (Original ou Avenant), le solde de congé du contrat doit être initialisé
 * Si c'est un Avenant au contrat, le solde de congé de l'ancien contrat sera réutilisé pour cet avenant
 * Si c'est un Original, le solde de congé démarre à Zéro (0)
 */
function saveOrUpdateSoldeConge($idContrat, $id_employe, $SoU, $dao, $idCentre, $val_actif, $date_prise_poste) {
	$where = "WHERE c.ID_EMPLOYE = $id_employe AND c.ACTIF_CONTRAT = 0";
	$whereAgeEnfant = "";
	$anciensContratDao = ($val_actif > 0) ? $dao -> getContratList($where, $whereAgeEnfant) : array();
	$droitConge = ($idCentre == '8' || $idCentre == '14') ? 3.1 : 2.5; // Le droit de congé pour les centres mobiles est de 3.1 les autres est de 2.5
	if (empty($anciensContratDao)) {
		// Insérer une nouvelle ligne de solde de congé avec le nouveau contrat
		$dao -> saveSoldeConge($SoU, 0, $date_prise_poste);
	} else {
		// Récupérer le solde de congé de son dernier contrat
		$ancienContrat = max(array_column($anciensContratDao, 0));
		$whereSoldeConge = "WHERE h.ID_CONTRAT = $ancienContrat";
		$ancienSolde = $dao -> getSoldeConge($whereSoldeConge);
		$solde = empty($ancienSolde) ? 0 : $ancienSolde[0][2];
		$date_releve_reliquat = empty($ancienSolde) ? new DateTime('now') : $ancienSolde[0][3];

		// Insérer une nouvelle ligne de solde de congé avec cette valeur
		$dao -> saveSoldeConge($SoU, $solde, $date_releve_reliquat);
	}
}

/*
 * Pour un nouveau contrat (Original ou Avenant),
 * Si c'est un Avenant au contrat et que l'employé ne change pas de centre d'affectation aucune mise à jour des effectifs n'est utile
 * Si c'est un Original OU si c'est un avenant plus le changement de centre d'affectation, les mise à jour des effectifs sont obligatoires
 */
function saveOrUpdateEffectifs($idCentre, $effectif, $ancienContrat, $dao) {
	if ($ancienContrat > 0) { // C'est un contrat Avenant
		$whereIdContrat = "WHERE c.ID_CONTRAT = $ancienContrat";
		$whereAgeEnfant = "";
		$ancienCentre = $dao -> getContratList($whereIdContrat, $whereAgeEnfant);
		if ($ancienCentre[0][2] != $idCentre) {
			// Mettre à jour l'effectif du centre
			$dao -> saveOrUpdateInEffectif($idCentre, $effectif);
			
			//Mettre à jour l'effectif total d'ADES
			$dao -> updateEffectifTotal();
		}
	} else { // C'est un contrat Original
		// Mettre à jour l'effectif du centre
		$dao -> saveOrUpdateInEffectif($idCentre, $effectif);
			
		//Mettre à jour l'effectif total d'ADES
		$dao -> updateEffectifTotal();
	}
}

/*
 * Supprimer une ligne dans table "contrat" où ID_CONTRAT = ($data -> idContrat)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'contrat',
		'ID_CONTRAT',
		$data -> idContrat
	);
	echo $SoU;
}
?>