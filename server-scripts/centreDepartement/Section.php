<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Save or update changed data from Section grid in Tab Panel
 * 
 */
function SaveOrUpdate($dao, $data) {
	$SoU = $dao -> saveOrUpdateSection(
		$data -> idSection,
		$data -> nomSection
	);
	echo $SoU;
}

/*
 * Delete record on table departement where ID_SECTION equals to value
 * 
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'section',
		'ID_SECTION',
		$data -> idSection
	);
	echo $SoU;
}
?>
