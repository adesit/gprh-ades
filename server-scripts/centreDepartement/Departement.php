<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Save or update changed data from Departement grid in Tab Panel
 * 
 */
function SaveOrUpdate($dao, $data) {
	$SoU = $dao -> saveOrUpdateDepartement(
		$data -> idDepartement,
		$data -> nomDepartement,
		$data -> abreviationDepartement
	);
	echo $SoU;
}

/*
 * Delete record on table departement where ID_DEPARTEMENT equals to value
 * 
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'departement',
		'ID_DEPARTEMENT',
		$data -> idDepartement
	);
	echo $SoU;
}
?>
