<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;
/* $ids_key = "paramsIds";
$ids = $model -> $ids_key; */

if ($infoRequest == 2) {
	/* $results = [];
	foreach ($ids as list($a, $b)) {
		array_push($results, SaveOrUpdate($m, $data, $a, $b));
	}
	echo json_encode($results); */
	SaveOrUpdateV2($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Save or update changed data from Poste form and grille salariale grid in Tab Panel
 * 
 
function SaveOrUpdate($dao, $data, $idGroupe, $idCategorie) {
	$SoU = $dao -> saveOrUpdatePoste(
		$data -> idPoste,
		$idGroupe,
		$idCategorie,
		$data -> titrePoste,
		$data -> descriptionPoste
	);
	return $SoU;
} */

function SaveOrUpdateV2($dao, $data) {
	$SoU = $dao -> saveOrUpdatePosteV2(
		$data -> idPoste,
		$data -> titrePoste,
		$data -> descriptionPoste,
		$data -> un,
		$data -> deux,
		$data -> trois,
		$data -> quatre,
		$data -> cinq,
		$data -> six,
		$data -> sept,
		$data -> huit,
		$data -> neuf,
		$data -> dix,
		$data -> vingt,
		$data -> tauxHoraire,
		$data -> categorieReelle
	);
	echo $SoU;
}

/*
 * Delete record on table "poste" where ID_POSTE equals to value
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'poste',
		'ID_POSTE',
		$data -> idPoste
	);
	echo $SoU;
}
?>
