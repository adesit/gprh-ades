<?php
require_once '../DataAccessObject.php';

$m = new DataAccessObject();

$model = json_decode($_POST['model']);
$key = "infoRequest";
$infoRequest = $model -> $key;
$data_key = "data";
$data = $model -> $data_key;

if ($infoRequest == 2) {
	SaveOrUpdate($m, $data);
} else if ($infoRequest === 3) {
	Delete($m, $data);
}

/*
 * Insérer ou mettre à jour en enregistrement dans la table "centre"
 */
function SaveOrUpdate($dao, $data) {
	$SoU = $dao -> saveOrUpdateCentre(
		$data -> idCentre,
		$data -> nomCentre,
		$data -> abreviationCentre,
		$data -> adresseCentre
	);
	if (empty($data -> idCentre)) $dao -> saveOrUpdateInEffectif($SoU, 0);
	echo $SoU;
}

/*
 * Supprimer une ligne dans table "centre" où ID_CENTRE = ($data -> idCentre)
 */
function Delete($dao, $data) {
	$SoU = $dao -> DeleteById(
		'centre',
		'ID_CENTRE',
		$data -> idCentre
	);
	echo $SoU;
}
?>
