/**
 * @class Gprh.ades.util.Downloader
 * @singleton
 *
 * @example
 * <pre>
 * Gprh.ades.util.Downloader.get({
 *      url: 'http://example.com/download?filename=test.txt',
 *      params: {
 *          param1: 'value1'
 *      }
 * });
 * </pre>
 *
 */
Ext.define('Gprh.ades.util.Downloader', {

	/**
     * Singleton class
     * @type {Boolean}
     */
	singleton: true,

	downloadFrame: null,

	downloadForm: null,

	/**
     * Get/Download from url
     * @param config1
     */
	get(config) {
		let config1 = config || {};

		/**
         * Support for String config as url
         */
		if (Ext.isString(config)) {
			config1 = {
				url: config,
			};
		}

		const xhttp = new XMLHttpRequest();
		const url = Object.keys(config1.params).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(config1.params[k])}`).join('&');

		xhttp.onload = () => {
			const a = document.createElement('a');
			let filename;
			if (xhttp.readyState === 4 && xhttp.status === 200) {
				const disposition = xhttp.getResponseHeader('Content-Disposition');
				const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
				const matches = filenameRegex.exec(disposition);
				a.href = window.URL.createObjectURL(xhttp.response);
				if (matches != null && matches[1]) { 
					filename = matches[1].replace(/['"]/g, '');
				}
				a.download = filename;
				a.style.display = 'none';
				document.body.appendChild(a);
				a.click();
			}
		};
		xhttp.open('POST', config1.url);
		// xhttp.setRequestHeader('Connection', 'close');
		xhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		// For a word I must return a 'blob' because it's a binary. Maybe you should change it
		xhttp.responseType = 'blob';
		xhttp.send(url);
		xhttp.addEventListener('readystatechange', () => {
			if (xhttp.readyState === xhttp.DONE) { 
				// Hide your modal, or clean your innerhtml here
				Ext.getBody().unmask();
			}
		});
	},

	getPDF: (config1, targets) => {
		let config = config1 || {};

		/**
         * Support for String config as url
         */
		if (Ext.isString(config)) {
			config = {
				url: config,
			};
		}

		Ext.create('Ext.form.Panel', {
			renderTo: Ext.getBody(),
			standardSubmit: true,
			url: config.url,
		}).submit({
			method: 'POST',
			params: config.params,
			scope: this,
			target: targets,
			headers: {
				type: 'application/pdf',
			},
		});
	},
});
