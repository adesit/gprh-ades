/*
 * This file contains all global variable and methods
 * In your *.js, include the class Gprh.ades.util.Globals.
 * Call Gprh.ades.util.Globals.formatNumber() or AppName.util.Globals.version throug your Classes
 */

Ext.define('Gprh.ades.util.Globals', {
	singleton: true,
	alternateClassName: 'globalUtilities',

	version: '1.0',
	config: {
		author: 'RAKOTOVAO Narisoa',
	},

	// Message de succès
	successMsg: 'Enregistrement effectué avec succès.',

	// Message d'erreur
	errorMsg: 'Echec de l\'enregistrement. Vérifiez vos données ou demandez de l\'aide à votre amdinistrateur.',

	// Message de succès de la suppréssion
	successDelMsg: 'Enregistrement supprimé.',

	// Message d'erreur de la suppréssion
	errorDelMsg: 'Echec de la suppréssion. Cette donnée est déjà utilisée par d\'autres entités. Demandez de l\'aide à votre amdinistrateur.',

	// Message d'invitation à sélectionner une ligne
	selectRowMsg: 'Vous devez sélectionner une ligne dans la grille.',

	errorPrintMsg: 'Une erreur est survenue lors de l\'édition de votre document',

	// La rendue de "value" est un nombre de la forme "0 000.00", cette valeur sera rendue dans le "target", 
	// utile pour formater un chiffre dans une zone de texte
	formatNumber: (target) => {
		Ext.util.Format.thousandSeparator = ' ';

		const formattedNumber = Ext.util.Format.number(target.getValue(), '0,000.00');
		target.setValue(formattedNumber);
	},

	// La rendue de "value" est un nombre de la forme "0 000.00"
	formatRenderNumber: (valueToFormat) => {
		Ext.util.Format.thousandSeparator = ' ';
		return Ext.util.Format.number(valueToFormat, '0,000.00');
	},

	// Fontion qui retourne des montants sans séparateur de millier
	simpleNumber: valueFormatted => valueFormatted.replace(/\s/gi, ''),
	
	/*
	 * Set all configuration values to edit grid row
	 * @param 
	 */
	/**
	 * Set all configuration values to edit grid row when the user clicks Update or Cancel button.
	 * @param {string} url the proxy url to find data from server.
	 * @param {string} gridId the grid id to load after success result
	 * @return {object} this {@link Ext.grid.plugin.RowEditing}
	 * @private
	 */
	setRowEditingConfig(url, object) {
		return Ext.create('Ext.grid.plugin.RowEditing', {
			clicksToMoveEditor: 1,
			autoCancel: false,
			saveBtnText: 'Enregistrer',
			cancelBtnText: 'Annuler',
			listeners: {
				beforeedit: () => (Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
				validateedit: (editor, context) => {
					const model = {
						infoRequest: 2,
						data: context.newValues,
					};
					const saveOrUpdate = object.requestSaveOrUpdate(url, model);
					if (saveOrUpdate.request.responseText > 0) {
						context.grid.getStore().reload();
					}
				},
				canceledit: (editor, context) => {
					context.grid.getStore().reload();
					context.grid.getView().refresh();
				},
			},
		});
	},
	
	// Récupère une linge du store dans un Promise
	requestOneRecordSettings(storeClass, params) {
		return new Promise((resolve, reject) => {
		// Charger un store est asynchrone
			const Store = Ext.create(storeClass);
			Ext.apply(Store.getProxy(), params);
			Store.load({
				callback(records, operation, success) {
					if (success) {
						if (records.length > 0) {
						// tout va bien, une ligne est retournée
							resolve(records);
						} else {
						// tout va bien mais l'enregistrement n'existe pas
							resolve(false);
						}
					} else {
					// Un problème -> Vérifiez du côté serveur
						reject(operation);
					}
				},
			});
		});
	},

	// Récupère le store tout entier dans un Promise
	requestStoreSettings(storeClass, params) {
		return new Promise((resolve, reject) => {
			// tout va bien, une ligne est retournée
			const Store = Ext.create(storeClass);
			Ext.apply(Store.getProxy(), params);
			Store.load({
				callback(records, operation, success) {
					if (success) {
						// tout va bien, toutes les lignes sont retournées
						resolve(Store);
					} else {
						// Un problème -> Vérifiez du côté serveur
						reject(operation);
					}
				},
			});			
		});
	},

	// La rendue de "value" est une date de la forme "dd/jj/aaaa"
	rendererDate(value) {
		if (Ext.isEmpty(value)) return '';
		return Ext.Date.format(new Date(value), 'd/m/Y');
	},

	getPagingToolbar: (table) => {
		if (!table.pagingToolbar) { // Filtre - pagination + tous les enregistrements
			Ext.apply(table, {
				pagingToolbar: Ext.create('Gprh.ades.util.PagingWithSearch', {
					displayInfo: true,
					displayMsg: '{1} ligne(s) sur {2}',
					dock: 'bottom',
					enabledPagination: false,
					emptyMsg: 'Aucun enregistrement',
					refreshText: 'Rafraîchir',
				}),
			});
		}
		return table.pagingToolbar;
	},

	getPagingToolbarEnabled: (table) => { // Filtre + pagination + Enregistrements par palier
		if (!table.pagingToolbar) {
			Ext.apply(table, {
				pagingToolbar: Ext.create('Gprh.ades.util.PagingWithSearch', {
					displayInfo: true,
					displayMsg: '{0} - {1} ligne(s) sur {2}',
					dock: 'bottom',
					enabledPagination: true,
					emptyMsg: 'Aucun enregistrement',
					firstText: 'Premiere page',
					lastText: 'Dernière page',
					prevText: 'Page précédente',
					nextText: 'Page suivante',
					refreshText: 'Rafraîchir',
					afterPageText: 'sur {0}',
				}),
			});
		}
		return table.pagingToolbar;
	},

	getSimplePagingToolbar: (table) => { // Pagination par défaut - Filtre - Enregistrement
		if (!table.pagingToolbar) {
			Ext.apply(table, {
				pagingToolbar: Ext.create('Ext.toolbar.Paging', {
					displayInfo: false,
					displayMsg: '{1} ligne(s) sur {2}',
					dock: 'bottom',
					emptyMsg: 'Aucun enregistrement',
					firstText: 'Premiere page',
					lastText: 'Dernière page',
					prevText: 'Page précédente',
					nextText: 'Page suivante',
					refreshText: 'Rafraîchir',
					afterPageText: 'sur {0}',
				}),
			});
		}
		return table.pagingToolbar;
	},

	getDefaultPagingToolbar: (table) => { // Pagination par défaut - Filtre + Enregistrement
		if (!table.pagingToolbar) {
			Ext.apply(table, {
				pagingToolbar: Ext.create('Ext.toolbar.Paging', {
					displayInfo: true,
					displayMsg: '{1} ligne(s) sur {2}',
					dock: 'bottom',
					emptyMsg: 'Aucun enregistrement',
					firstText: 'Premiere page',
					lastText: 'Dernière page',
					prevText: 'Page précédente',
					nextText: 'Page suivante',
					refreshText: 'Rafraîchir',
					afterPageText: 'sur {0}',
				}),
			});
		}
		return table.pagingToolbar;
	},

	// Force format renderered Date to 'dd/mm/yyyy'
	forceDateFormat(inputFormat) {
		function pad(s) { return (s < 10) ? `0${s}` : s; }
		const d = new Date(inputFormat);
		const dateVal = [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
		console.log(dateVal);
		return dateVal;
	},

	// Convert date in string format 'dd/mm/yyyy' to Date object
	convertDateString(inputDateString) {
		const dateParts = inputDateString.split('/');
		// month is 0-based, that's why we need dataParts[1] - 1
		return (new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]));
	},

	constructor(config) {
		this.initConfig(config);
	},

});
