Ext.define('Gprh.ades.util.VTypes', {
	override: 'Ext.form.field.VTypes',
    
	// année scolaire de type "2018 - 2019"
	schoolYear(value) {
		return /^\d{4}-?\d{4}$/.test(value); // Vérifie la validité de la valeur
	},	
	schoolYearText: 'Le format attendu est de : xxxx-xxxx', // Message d'erreur
	schoolYearMask: /[\d-]/i, // N'autorise que les touches numériques

	// horaire de travail de type "07h30 : 12h00 - 14h30 : 18h00"
	horaire(value) {
		return /^\d{2}h?\d{2} : \d{2}h\d{2} - \d{2}h?\d{2} : \d{2}h\d{2}$/.test(value);
	},
	horaireText: 'Le format attendu est de : xxhxx : xxhxx - xxhxx : xxhxx',

	// année en quatre chiffres
	year(value) {
		return /^\d{4}$/.test(value);
	},	
	yearText: 'Le format attendu est de : xxxx',
	yearMask: /[\d-]/i,

	// nombre décimal avec un séparateur décimal "."
	decimal(value) {		
		return /^\d+(\.\d{1,2})?$/.test(value);
	},
	decimalText: 'Nombre invalide ou le séparateur décimal est un point.',
	decimalMask: /[\d\.]/, // N'autorise que les touches numériques et le "."

	// Confirmer le mot de passe
	passwordMatch(value, field) {
		const password = field.up('form').down('#motDePasseId');
		return (value === password.getValue());
	},
	passwordMatchText: 'Les mots de passe saisis ne sont pas identiques.',

	// Vérifier si la somme d'avance demandée ne dépasse pas le maximum autorisé
	avanceMatch(value, field) {
		const avanceDemandeField = field.up('form').down('#maxAutoriseId');
		const maxValue = avanceDemandeField.getValue().replace(/\s/gi, '');
		
		return (parseFloat(value.replace(/\s/gi, '')) <= parseFloat(maxValue));
	},
	avanceMatchText: 'La somme demandée ne devrait pas excédée le maximum autorisé.',

	// Vérifier si le remboursement mensuel ne dépasse pas la déduction maximale
	avanceRemboursementMatch(value, field) {
		const remboursementMaxField = field.up('form').down('#maxDeductionPossibleId');
		const maxValue = remboursementMaxField.getValue().replace(/\s/gi, '');
		
		return (parseFloat(value.replace(/\s/gi, '')) <= parseFloat(maxValue));
	},
	avanceRemboursementMatchText: 'Le montant de remboursement ne devrait pas excédé le maximum autorisé.',

	// numéro de mobile madagascar "+261 xx xx xxx xx"
	numeroTel(value) {
		return /^\+261 \d{2} \d{2} \d{3} \d{2}$/.test(value);
	},
	numeroTelText: 'Le format attendu est de : +261 xx xx xxx xx',

	// La somme de la 2ème tranche doit être supérieure à celle de la 1ère tranche
	trancheMatch(value, field) {
		const tranche1Field = field.up('form').down('#trancheMaxId1');
		const maxValue = tranche1Field.getValue().replace(/\s/gi, '');
		
		return (parseFloat(value.replace(/\s/gi, '')) > parseFloat(maxValue));
	},
	trancheMatchText: 'La 2ème tranche > 1ère tranche.',
});
