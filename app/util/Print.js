Ext.define('Gprh.ades.util.Print', {
	extend: 'Ext.data.Store',
	
	storeId: 'printStore',
	alias: 'store.printStore',

	loading: true,
	
	fields: [],
	proxy: {
		type: 'ajax',
		url: null,
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
