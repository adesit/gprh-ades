Ext.define('Gprh.ades.util.PagingWithSearch', {
	extend: 'Ext.toolbar.Paging',
	xtype: 'pagingtoolbarwithsearch',

	filterItemWidth: 200,
	filterOnType: true,
	filterSplitButtonGlyph: 'xf0b0@FontAwesome',
	filterSplitButtonText: 'Filtre',

	getPagingItems() {
		const me = this;
            
		const inputListeners = {
			scope: me,
			blur: me.onPagingBlur,
		};

		inputListeners[Ext.supports.SpecialKeyDownRepeat ? 'keydown' : 'keypress'] = me.onPagingKeyDown;

		if (!me.enabledPagination) {
			return [
				{
					itemId: 'refresh',
					tooltip: me.refreshText,
					overflowText: me.refreshText,
					iconCls: `${Ext.baseCSSPrefix}tbar-loading`,
					disabled: me.store.isLoading(),
					handler: me.doRefresh,
					scope: me,
				},
				'-',
				this.getFilterSplitButton(),
				this.getFilterTextField(),
			];
		}
		return [
			{
				itemId: 'first',
				tooltip: me.firstText,
				overflowText: me.firstText,
				iconCls: `${Ext.baseCSSPrefix}tbar-page-first`,
				disabled: true,
				handler: me.moveFirst,
				scope: me,
			}, {
				itemId: 'prev',
				tooltip: me.prevText,
				overflowText: me.prevText,
				iconCls: `${Ext.baseCSSPrefix}tbar-page-prev`,
				disabled: true,
				handler: me.movePrevious,
				scope: me,
			},
			'-',
			me.beforePageText, {
				xtype: 'numberfield',
				itemId: 'inputItem',
				name: 'inputItem',
				cls: `${Ext.baseCSSPrefix}tbar-page-number`,
				allowDecimals: false,
				minValue: 1,
				hideTrigger: true,
				enableKeyEvents: true,
				keyNavEnabled: false,
				selectOnFocus: true,
				submitValue: false,
				// mark it as not a field so the form will not catch it when getting fields
				isFormField: false,
				width: me.inputItemWidth,
				margin: '-1 2 3 2',
				listeners: inputListeners,
			}, {
				xtype: 'tbtext',
				itemId: 'afterTextItem',
				html: Ext.String.format(me.afterPageText, 1),
			},
			'-', {
				itemId: 'next',
				tooltip: me.nextText,
				overflowText: me.nextText,
				iconCls: `${Ext.baseCSSPrefix}tbar-page-next`,
				disabled: true,
				handler: me.moveNext,
				scope: me,
			}, {
				itemId: 'last',
				tooltip: me.lastText,
				overflowText: me.lastText,
				iconCls: `${Ext.baseCSSPrefix}tbar-page-last`,
				disabled: true,
				handler: me.moveLast,
				scope: me,
			},
			'-', {
				itemId: 'refresh',
				tooltip: me.refreshText,
				overflowText: me.refreshText,
				iconCls: `${Ext.baseCSSPrefix}tbar-loading`,
				disabled: me.store.isLoading(),
				handler: me.doRefresh,
				scope: me,
			},
			'-',
			this.getFilterSplitButton(),
			this.getFilterTextField(),
		];
	},

	getFilterSplitButton() {
		if (!this.filterSplitButton) {
			this.filterSplitButton = Ext.create('Ext.button.Split', {
				text: this.filterSplitButtonText,
				handler: this.doFilter,
				glyph: this.filterSplitButtonGlyph,
				scope: this,
			});
		}
		return this.filterSplitButton;
	},

	getFilterTextField() {
		if (!this.filterTextField) {
			this.filterTextField = Ext.create('Ext.form.field.Text', {
				submitValue: false,
				isFormField: false,
				width: this.filterItemWidth,
				margin: '-1 2 3 2',
				enableKeyEvents: true,
			});
			if (this.filterOnType) {
				this.filterTextField.on('change', this.doFilter, this);
			} else {
				this.filterTextField.on('specialkey', (e) => {
					if (e.getKey() === e.ENTER) {
						this.doFilter();
					}
				}, this);
			}
		}
		return this.filterTextField;
	},

	beforeRender() {
		this.callParent();

		this.updateBarInfo();
		this.updateSearchColumnsMenu();
	},

	getSelectAllColumnsMenuItem() {
		if (!this.selectAllColumnsMenuItem) {
			this.selectAllColumnsMenuItem = Ext.create('Ext.menu.CheckItem', {
				text: 'All Columns',
				checked: true,
				listeners: {
					checkchange: {
						fn(menuCheckItem, checked) {
							const menuArray = (this.getFilterColumnsMenu().items.items).filter(item => !Ext.isEmpty(item.dataIndex));
							menuArray.map(item => item.setChecked(checked));
							
							this.doFilter();
						},
						scope: this,
					},
				},
			});
		}
		return this.selectAllColumnsMenuItem;
	},

	getFilterColumnsMenu() {
		if (!this.filterColumnsMenu) {
			this.filterColumnsMenu = Ext.create('Ext.menu.Menu', {
				items: [
					this.getSelectAllColumnsMenuItem(),
					{
						xtype: 'menuseparator',
					},
				],
			});
		}
		return this.filterColumnsMenu;
	},

	updateSearchColumnsMenu() {
		const columns = this.up('grid').getColumns();
		Ext.Array.each(columns, (column) => {
			this.getFilterColumnsMenu().add({
				xtype: 'menucheckitem',
				text: column.text,
				dataIndex: column.dataIndex,
				checked: true,
				listeners: {},
			});
		}, this);
		this.getFilterSplitButton().setMenu(this.getFilterColumnsMenu());
	},

	doFilter() {
		const searchColumnIndexes = [];
            
		const searchValue = this.getFilterTextField().getValue();

		const arrayMenu = (this.getFilterSplitButton().getMenu().items.items).filter(item => (!Ext.isEmpty(item.dataIndex) && item.checked));
		arrayMenu.map(item => searchColumnIndexes.push(item.dataIndex));

		if (this.getStore().remoteFilter) {
			this.remoteFilter(searchColumnIndexes, searchValue);
		} else {
			this.localFilter(searchColumnIndexes, searchValue);
		}
	},

	localFilter(searchColumnIndexes, searchValue) {
		this.getStore().removeFilter(this.filter);
		this.filter = new Ext.util.Filter({
			filterFn(record) {
				if (searchColumnIndexes.length === 0 || Ext.isEmpty(searchValue)) {
					return true;
				}

				return searchColumnIndexes.some(dataIndex => (record.get(dataIndex) && record.get(dataIndex).toString().indexOf(searchValue) !== -1));
			},
		});
		this.getStore().addFilter(this.filter);
	},

	remoteFilter(searchColumnIndexes, searchValue) {
		const remoteFilters = [];
		Ext.Array.each(searchColumnIndexes, (columnIndex) => {
			remoteFilters.push({
				property: columnIndex,
				value: searchValue,
			});
		});
		this.getStore().clearFilter();
		this.getStore().filter(remoteFilters);
	},

	getStore() {
		if (!this.store) {
			this.store = this.getCmp().up('grid').getStore();
		}
		return this.store;
	},
});
