Ext.define('Gprh.ades.util.ColorField', {
	extend: 'Ext.form.field.Picker',
	xtype: 'colorPicker2',

	requires: [
		'Ext.picker.Color',
	],

	editable: false,
	colorPicker: null,

	createPicker() {
		const me = this;
		
		const picker = Ext.create('Ext.picker.Color', {
			value: me.getValue(),
			renderTo: document.body,
			floating: true,
			minWidth: 250,
			listeners: {
				select: {
					fn: me.onColorPickerChange,
					scope: me,
				},
			},
		});
		me.colorPicker = picker;
		return me.colorPicker;
	},

	onColorPickerChange(colorPicker, color) {
		this.setValue(color);
		this.inputEl.setStyle({
			backgroundColor: `#${color}`,
		});
		colorPicker.hide();
		this.inputEl.blur();
	},
});
