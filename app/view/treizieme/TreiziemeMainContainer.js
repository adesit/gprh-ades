Ext.define('Gprh.ades.view.treizieme.TreiziemeMainContainer', {
	extend: 'Ext.panel.Panel',

	xtype: 'treiziemeMainContainer',

	requires: [
		'Gprh.ades.view.treizieme.TreiziemeController',
		'Gprh.ades.view.treizieme.TreiziemeViewModel',
		'Gprh.ades.util.VTypes',
		'Gprh.ades.view.salaire.SalaireDetailsGrid',
		'Gprh.ades.util.Helper',
	],
	
	viewModel: {
		type: 'treiziemeViewModel',
	},

	controller: 'treiziemeController',

	itemId: 'treiziemeMainContainerId',

	layout: {
		type: 'vbox',
		pack: 'start',
		align: 'stretch',
	},

	bodyPadding: 1,

	items: [
		{
			layout: 'column',
			xtype: 'form',
			items: [
				{
					columnWidth: '0.20',
					items: [
						{
							xtype: 'textfield',
							itemId: 'anneeTreiziemeId',
							fieldLabel: 'Année',
							allowBlank: false,
							vtype: 'year',
							msgTarget: 'side',
							width: 200,
							labelAlign: 'left',
							labelSeparator: ':',
							submitEmptyText: false,
							listeners: {
								change: (el) => {
									el.up('form').down('#treiziemeMenu').setDisabled(false);									
								},
							},
						},
					],
				},
				{
					columnWidth: '0.35',
					items: [
						{
							xtype: 'toolbar',
							layout: 'hbox',
							itemId: 'treiziemeBottomToolbar',
							overflowHandler: 'menu',
							items: [
								{
									text: 'Options',
									iconCls: 'x-fa fa-th',
									formBind: true,
									menu: {
										itemId: 'treiziemeMenu',
										items: [
											{
												text: '<b>Calculer</b>',
												iconCls: 'x-fa fa-calculator',
												handler: 'onItemRunClick',
												itemId: 'calculerMenuid',
											},
											{
												text: 'Enregistrer',
												iconCls: 'x-fa fa-save',
												handler: 'onItemSaveClick',
												itemId: 'enregistrerMenuid',
											},
											{
												text: 'Etat de paie',
												iconCls: 'x-fa fa-file-excel-o greenIcon',
												handler: 'onItemExportClick',
												itemId: 'exporterMenuid',
											},
											{
												text: 'Fiche de paie',
												iconCls: 'x-fa fa-file-pdf-o redIcon',
												handler: 'onItemExportPdfClick',
												itemId: 'exporterPdfMenuid',
											},
											{
												text: 'Historique',
												iconCls: 'x-fa fa-book',
												handler: 'onItemHistoriqueClick',
												itemId: 'historiqueMenuid',
											},
										],
									},
									listeners: {
										click: (el) => {
											const anneeValue = parseInt(el.up('form').down('textfield').getValue(), 10);
											if (anneeValue < 2019) { // Les données ne sont pas disponibles avant 2018
												Gprh.ades.util.Helper.showError('GPRH calcule le 13ème mois depuis l\'année 2019.');
												el.setDisabled(true);
											} else if (anneeValue >= 2019) { // Vérifier si le 13ème mois pour l'année en question est déjà payé
												let exist;

												// cet appel permet la vérification d'un 13ème mois déjà clôturé
												Ext.Ajax.request({
													url: 'server-scripts/listes/Treizieme.php',
													params: {
														infoRequest: 1,
														idSalaire: anneeValue,
													},
													async: false,
													success: (response) => {
														const data = Ext.decode(response.responseText);
														exist = data.data;
													},
												});
												// si HISTORIQUE disponible ou NON
												// Désactiver certains boutons	
												if (exist > 0) {				
													el.up('form').down('#calculerMenuid').setHidden(true);
													el.up('form').down('#enregistrerMenuid').setHidden(true);
													el.up('form').down('#exporterMenuid').setHidden(false);
													el.up('form').down('#exporterPdfMenuid').setHidden(false);
													el.up('form').down('#historiqueMenuid').setHidden(false);
												} else { // Si non 
													const admin = Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') !== '1';
													el.up('form').down('#calculerMenuid').setHidden(admin);
													el.up('form').down('#enregistrerMenuid').setHidden(admin);
													el.up('form').down('#exporterMenuid').setHidden(true);
													el.up('form').down('#exporterPdfMenuid').setHidden(true);
													el.up('form').down('#historiqueMenuid').setHidden(true);
												}												
											}
										},
									},
								},
								{
									text: 'Annuler',
									iconCls: 'x-fas fa-redo-alt',
									ui: 'soft-blue',
									itemId: 'printSalaireGridBtnId',
									handler: 'onTreizeCancel',
								},
							],
						},
					],
				},
			],
		},
		{
			xtype: 'salaireDetailsGrid',
			itemId: 'treiziemeDetailsGrid',
			hideAllColumns: true,
		},
	],
		
});
