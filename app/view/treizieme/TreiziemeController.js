/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 */
Ext.define('Gprh.ades.view.treizieme.TreiziemeController', {
	extend: 'Ext.app.ViewController',

	alias: 'controller.treiziemeController',
	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.util.Downloader',
	],

	init() {},

	async onItemRunClick(el) {
		const anneeDecembre = el.up('form').down('textfield');
		const gridDetails = this.getView().down('salaireDetailsGrid');
		
		const extraParameters = {
			url: 'server-scripts/formule/Treizieme.php',
			extraParams: {
				moisSalarial: anneeDecembre.getValue() + '-' + 12,
				infoRequest: 1, // tous les employés d'ADES
				action: 1, // action == 1 retourne les données à afficher
			},
		};
		// Va faire les calculs et récupérer les résultats, puis afficher dans le grid treiziemeDetailsGrid le store ainsi obtenu
		Ext.getBody().mask("Veuillez patientez ...");
		const inputSalairesData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.SalaireDetails', extraParameters);
		
		Ext.getBody().unmask();
		gridDetails.setStore(inputSalairesData);
	},

	onItemExportClick(el) {
		const anneeDecembre = el.up('form').down('textfield');
		// const gridDetails = Ext.getCmp('treiziemeDetailsGrid');

		Ext.getBody().mask("Veuillez patientez ...");

		// Va exporter les données du 13ème clôturé sur Excel (2007 .xlsx) en format 13ème Mois
		Gprh.ades.util.Downloader.get({
			url: 'server-scripts/formule/Treizieme.php',
			params: {
				infoRequest: 1, // tous les employés d'ADES
				action: 5, // action == 5 exporte un Excel 
				start: 0,
				page: 1,
				limit: 25,
				mois: anneeDecembre.getValue(),
				moisSalarial: anneeDecembre.getValue() + '-' + 12,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			}
		});
	},

	async onItemSaveClick(el) {
		const anneeDecembre = el.up('form').down('textfield');
		
		const extraParameters = {
			url: 'server-scripts/formule/Treizieme.php',
			extraParams: {
				infoRequest: 1,
				moisSalarial: anneeDecembre.getValue() + '-' + 12,
				action: 2, // action == 2 sauvegarde dans la BDD et retourne 1 si aucune erreur
				start: 0,
				page: 1,
				limit: 25,
			},
		};
		// Va faire les calculs et récupérer les résultats, puis afficher dans le grid treiziemeDetailsGrid le store ainsi obtenu
		Ext.getBody().mask("Veuillez patientez ...");
		await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.SalaireDetails', extraParameters);
		
		Ext.getBody().unmask();
	},

	onTreizeCancel(el) {
		el.up('form').reset();
		this.getView().down('salaireDetailsGrid').setStore(null);
	},

	async onItemHistoriqueClick(el) {
		const anneeDecembre = el.up('form').down('textfield');
		const gridDetails = this.getView().down('salaireDetailsGrid');
		
		const extraParameters = {
			url: 'server-scripts/listes/HistoriqueSalaire.php',
			extraParams: {
				infoRequest: 2,
				idSalaire: anneeDecembre.getValue(),
				start: 0,
				page: 1,
				limit: 25,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		// Va faire les calculs et récupérer les résultats, puis afficher dans le grid treiziemeDetailsGrid le store ainsi obtenu
		Ext.getBody().mask("Veuillez patientez ...");
		const inputSalairesData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.SalaireDetails', extraParameters);
		
		Ext.getBody().unmask();
		gridDetails.setStore(inputSalairesData);
	},

	onItemExportPdfClick(el) {
		const anneeDecembre = el.up('form').down('textfield');
		// const gridDetails = Ext.getCmp('treiziemeDetailsGrid');

		Ext.getBody().mask("Veuillez patientez ...");

		// Va exporter les données du 13ème clôturé sur Excel (2007 .xlsx) en format 13ème Mois
		Gprh.ades.util.Downloader.get({
			url: 'server-scripts/print/FicheDePaie.php',
			params: {
				infoRequest: 4, // Export en pdf 13ème
				start: 0,
				page: 1,
				limit: 25,
				idSalaire: anneeDecembre.getValue(),
				mois: anneeDecembre.getValue() + '-' +  12,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			}
		});
	},

});
