Ext.define('Gprh.ades.view.rapport.RapportMainContainer', {
	extend: 'Ext.panel.Panel',
	xtype: 'rapportMainContainer',

	requires: [
		'Gprh.ades.store.Mois',
		/* 'Ext.chart.CartesianChart',
		'Ext.chart.axis.Numeric',
		'Ext.chart.axis.Category',
		'Ext.chart.series.Bar',
		'Ext.chart.interactions.ItemHighlight',
		'Ext.chart.PolarChart',
		'Ext.chart.series.Pie',
		'Ext.chart.interactions.Rotate',
		'Ext.chart.series.Area',
		'Ext.chart.series.Line',
		'Gprh.ades.controller.RapportController', */
	],
	
	bodyPadding: 2,

	itemId: 'rapportMainContainerId',
	id: 'rapportMainContainerid',

	controller: 'rapportController',

	initComponent() {
		// Store pour PERSONNEL par sexe
		const personnelStore = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'personnel',  
				},  
			},  
		});

		// Store pour PERSONNEL par age et par sexe
		const agePersonnelStore = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'agePersonnel',  
				},  
			},  
		});

		// Store pour PERSONNEL par age et par sexe pour la structure
		const agePersonnelStructureStore = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'structureAge',  
				},  
			},  
		});

		// Store pour ENFANT par age et par sexe
		const ageEnfantPersonnelStore = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'ageEnfantPersonnel',  
				},  
			},  
		});

		// Store pour FRAIS DE SCOLARITE des enfants
		const fraisScolariteStore = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'fraisScolariteEnfant',  
				},  
			},  
		});

		// Store pour Contrats par centre
		const contratCentreStore = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'contratParCentre',  
				},  
			},  
		});

		// Store pour Contrats par Poste
		const contratPosteStore = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'contratParPoste',  
				},  
			},  
		});

		// Store pour Contrats par Ancienneté
		const contratAncienneteStore = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'contratParAnciennete',  
				},  
			},  
		});

		// Store pour Rapport de SALAIRE
		const salaireRapportStore = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'salaireRapport',  
				},  
			},  
		});

		// Store pour Rapport de l'inspection de travail
		const inspectionRapportStore = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'inspectionRapport',  
				},  
			},  
		});

		// Store pour Evolution de l'effectif par annee et par sexe
		const evolutionEffectif1Store = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'effectifParAnnee',  
				},  
			},  
		});

		// Store pour Evolution de salaires par année
		const evolutionSalaireStore = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'salairesParAnnee',  
				},  
			},  
		});

		// Store pour les indicateurs demandés par Suisse
		const resumeRapportStore = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'salairesParAnnee',  
				},  
			},  
		});

		// Store pour les absences
		const absenceRapportStore = Ext.create('Ext.data.Store', { 
			fields: [
				'Date début',
				'Date fin',
				'Motif',
				'typeAbs',
				'Centre',
				{
					name: 'Type',
					convert(v, rec) {
						const types = ['Absence non autorisée', 'Permission', 'Absence autorisée', 'Congé de maternité', 'Congé régulier'];
						
						return types[parseInt(rec.get('typeAbs'), 10) - 1];
					},
				},
			],
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'absences',  
				},  
			},  
		});

		const globalStore = Ext.create('Ext.data.Store', {  
			proxy: {
				type: 'ajax',  
				url: 'server-scripts/listes/Rapport.php',
				async: false, 
				reader: {  
					type: 'json',
				},
				extraParams: {
					infoRequest: 1,
					typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
					idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
				},  
			},  
			autoLoad: true,  
			listeners: {  
				load: (store, records) => {
					personnelStore.loadRawData(records[0].store.data.items[0].get('personnel'));
					agePersonnelStore.loadRawData(records[0].store.data.items[0].get('agePersonnel'));
					agePersonnelStructureStore.loadRawData(records[0].store.data.items[0].get('structureAge'));  
					ageEnfantPersonnelStore.loadRawData(records[0].store.data.items[0].get('ageEnfantPersonnel'));
					fraisScolariteStore.loadRawData(records[0].store.data.items[0].get('fraisScolariteEnfant'));
					contratCentreStore.loadRawData(records[0].store.data.items[0].get('contratParCentre'));
					contratPosteStore.loadRawData(records[0].store.data.items[0].get('contratParPoste'));
					contratAncienneteStore.loadRawData(records[0].store.data.items[0].get('contratParAnciennete'));
					inspectionRapportStore.loadRawData(records[0].store.data.items[0].get('inspectionRapport'));
					salaireRapportStore.loadRawData(records[0].store.data.items[0].get('salaireRapport'));
					evolutionEffectif1Store.loadRawData(records[0].store.data.items[0].get('effectifParAnnee'));
					evolutionSalaireStore.loadRawData(records[0].store.data.items[0].get('salairesParAnnee'));
					resumeRapportStore.loadRawData(records[0].store.data.items[0].get('resumeParametrable'));
					absenceRapportStore.loadRawData(records[0].store.data.items[0].get('absences'));
				},
			},
		});

		const tabPanels = {
			xtype: 'tabpanel',
			itemsId: 'tabPanelRapportId',
			defaults: {
				margin: 5,
				maxHeight: Ext.Element.getViewportHeight() - 165,
				autoScroll: true,
				layout: {
					type: 'vbox',
					align: 'stretch',
				},
				bodyPadding: 5,
			},
			items: [
				{
					title: 'PERSONNEL',
					itemId: 'rapportPersonnelTabId',
					id: 'rapportPersonnelTabid',
					items: [
						{
							html: `<span class="x-fa fa-info-circle"></span> Cet onglet contient:
							<ul style="list-style-type: circle;">
							<li>L'effectif du personnel actuel par genre</li>
							<li>Le tableau de r&eacute;partition du personnel par sexe et par &acirc;ge</li>
							<li>Le graphe de l'effectif par sexe et par &acirc;ge</li>
							<li>Le graphe de r&eacute;partition du personnel par tranche d'&acirc;ge</li>
							<li>Le tableau de r&eacute;partion des enfants du personnel par sexe et par &acirc;ge</li>
							<li>Le graphe de l'effectif des enfants du personnel par sexe et par &acirc;ge</li>
							</ul>`,
							itemId: 'helperId',
							baseCls: 'x-toast-info',
							anchor: '100%',
							margin: '0 0 5 0',
						},
						{
							layout: 'column',
							items: [
								{
									columnWidth: '0.50',
									items: [
										{
											xtype: 'gridpanel',
											title: `Personnel de l'année ${Ext.Date.format(new Date(), 'Y')}`,
											itemId: 'rapportPersonnelGridId',
											id: 'rapportPersonnelGridid',
											columnLines: true,
											store: personnelStore,
											maxWidth: 200,
											columns: [
												{
													text: '<span class="fa fa-male"> </span> Masculin',
													sortable: false,
													dataIndex: 'homme',
												},
												{
													text: '<span class="fa fa-female"> </span> Féminin',
													sortable: false,
													dataIndex: 'femme',
												},
											],
										},
									],
								},
								{
									columnWidth: '0.50',
									items: [
										{
											xtype: 'gridpanel',
											title: 'Répartition par âge du personnel',
											itemId: 'rapportPersonnelAgeGridId',
											id: 'rapportPersonnelAgeGridid',
											columnLines: true,
											store: agePersonnelStore,
											maxWidth: 420,
											maxHeight: 250,
											plugins: 'gridfilters',
											features: {
												ftype: 'summary',
												dock: 'top',
												id: 'sum',
											},
											columns: [
												{
													text: 'Age (ans)',
													sortable: false,
													dataIndex: 'age',
													filter: {
														type: 'number',
														itemDefaults: {
															listeners: {
																afterrender: () => {
																	const els = Ext.select('div.x-form-trigger.x-form-trigger-default.x-form-trigger-spinner.x-form-trigger-spinner-default.x-unselectable', true);
																	els.removeCls('x-form-trigger-default');
																},
																change: 'setDetailsRemboursement',
															},
														},
													},
												},
												{
													text: '<span class="fa fa-male"> </span> Masculin',
													sortable: false,
													dataIndex: 'homme',
													summaryType: 'sum',
													summaryRenderer: value => `<b>${value}<b>`,
												},
												{
													text: '<span class="fa fa-female"> </span> Féminin',
													sortable: false,
													dataIndex: 'femme',
													summaryType: 'sum',
													summaryRenderer: value => `<b>${value}<b>`,
												},
												{
													text: 'Total',
													sortable: false,
													dataIndex: 'totalParSexe',
													style: {
														fontWeight: 'bold',
														background: '#EAA8A8',
													},
													renderer: (value, metaData) => {
														Ext.apply(metaData, {
															style: 'background-color:#EAA8A8',
														});
														return value;
													},
													summaryType: 'sum',
													summaryRenderer: value => `<b>${value}<b>`,
												},
											],
										},
									],	
								},
							],
						},
						{
							layout: 'column',
							margin: '10 0 0 0',
							items: [
								{
									columnWidth: '0.50',
									dockedItems: [{
										xtype: 'toolbar',
										items: [{
											xtype: 'button',
											ui: 'soft-green-small',
											iconCls: 'fa fa-download',
											handler: () => {
												Ext.getCmp('grapheColumnPersonnel').download({
													filename: 'Effectif du personnel d\'ADES actuel',
													format: 'png',
												});
											},
										}],
									}],
									items: [
										{ // Graphe en "stacked column" pour le personnel
											xtype: 'cartesian',
											reference: 'grapheColumnPersonnelRef',
											id: 'grapheColumnPersonnel',
											width: '100%',
											height: Ext.Element.getViewportHeight() - 100,
											store: agePersonnelStore,
											legend: {
												type: 'sprite',
												docked: 'bottom',
											},
											insetPadding: 50,
											innerPadding: 20,
											sprites: [
												{
													type: 'text',
													text: 'Effectif du personnel d\'ADES actuel',
													font: '14px Arial',
													width: 100,
													height: 30,
													x: 40, // the sprite x position
													y: 20, // the sprite y position
												},
											],
											axes: [{
												type: 'numeric',
												position: 'left',
												adjustByMajorUnit: true,
												grid: true,
												fields: ['homme'],
												renderer: 'onAxisLabelRender',
												minimum: 0,
												title: 'Effectif du personnel',
											}, {
												type: 'category',
												position: 'bottom',
												grid: true,
												fields: ['age'],
												title: 'Âge (ans)',
												label: {
													rotate: {
														degrees: -45,
													},
												},
											}],
											series: [{
												type: 'bar',
												title: ['Masculin', 'Féminin'],
												xField: 'age',
												yField: ['homme', 'femme'],
												stacked: true,
												style: {
													opacity: 0.80,
												},
												highlight: {
													fillStyle: 'gold',
													lineWidth: 0,
												},
												tooltip: {
													renderer: 'onBarTipRender',
												},
												colors: ['#a5c249', '#ff00ff'],
											}],
										},
									],
								},
								{
									columnWidth: '0.50',
									dockedItems: [{
										xtype: 'toolbar',
										items: [{
											xtype: 'button',
											ui: 'soft-green-small',
											iconCls: 'fa fa-download',
											handler: () => {
												Ext.getCmp('graphePiePersonnelid').download({
													filename: 'Répartition du personnel',
													format: 'png',
												});
											},
										}],
									}],
									items: [
										{ // Graphe en "pie" Répartition du personnel
											xtype: 'polar',
											reference: 'graphePiePersonnelRef',
											id: 'graphePiePersonnelid',
											width: '100%',
											height: 500,
											insetPadding: 50,
											innerPadding: 20,
											store: agePersonnelStructureStore,
											legend: {
												type: 'sprite',
												docked: 'right',
											},
											interactions: ['rotate'],
											sprites: [
												{
													type: 'text',
													text: 'Répartition du personnel',
													font: '14px Arial',
													width: 100,
													height: 30,
													x: 40, // the sprite x position
													y: 20, // the sprite y position
												},
											],
											series: [{
												type: 'pie',
												angleField: 'data1',
												label: {
													field: 'age',
													calloutLine: {
														length: 60,
														width: 3,
														// specifying 'color' is also possible here
													},
												},
												highlight: true,
												tooltip: {
													trackMouse: true,
													renderer: 'onSeriesTooltipRender',
												},
											}],
										},
									],	
								},
							],
						},
						{
							xtype: 'fieldset',
							title: 'Enfants du personnel',
							layout: 'column',
							margin: '10 0 0 0',
							items: [
								{
									columnWidth: '0.50',
									layout: {
										type: 'vbox',
										align: 'stretch',
									},
									items: [
										{
											xtype: 'gridpanel',
											title: 'Répartition par âge des enfants',
											itemId: 'rapportEnfantAgeGridId',
											id: 'rapportEnfantAgeGridid',
											userCls: 'shadow',
											cls: 'user-grid',
											columnLines: true,
											store: ageEnfantPersonnelStore,
											maxWidth: 420,
											maxHeight: 250,
											plugins: 'gridfilters',
											features: {
												ftype: 'summary',
												dock: 'top',
												id: 'sum1',
											},
											columns: [
												{
													text: 'Age (ans)',
													sortable: false,
													dataIndex: 'age',
													filter: {
														type: 'number',
														itemDefaults: {
															listeners: {
																afterrender: () => {
																	const els = Ext.select('div.x-form-trigger.x-form-trigger-default.x-form-trigger-spinner.x-form-trigger-spinner-default.x-unselectable', true);
																	els.removeCls('x-form-trigger-default');
																},
																change: 'setDetailsRemboursement',
															},
														},
													},
												},
												{
													text: '<span class="fa fa-male"> </span> Masculin',
													sortable: false,
													dataIndex: 'homme',
													summaryType: 'sum',
													summaryRenderer: value => `<b>${value}<b>`,
												},
												{
													text: '<span class="fa fa-female"> </span> Féminin',
													sortable: false,
													dataIndex: 'femme',
													summaryType: 'sum',
													summaryRenderer: value => `<b>${value}<b>`,
												},
												{
													text: 'Total',
													sortable: false,
													dataIndex: 'total',
													style: {
														fontWeight: 'bold',
														background: '#EAA8A8',
													},
													renderer: (value, metaData) => {
														Ext.apply(metaData, {
															style: 'background-color:#EAA8A8',
														});
														return value;
													},
													summaryType: 'sum',
													summaryRenderer: value => `<b>${value}<b>`,
												},
											],
										},
										{
											xtype: 'gridpanel',
											title: 'Frais de scolarité',
											itemId: 'scolariteGridId',
											id: 'scolariteGridid',
											cls: 'shadow',
											columnLines: true,
											store: fraisScolariteStore,
											maxWidth: 420,
											maxHeight: 270,
											margin: '50 0 0 0',
											columns: [
												{
													text: 'Année scolaire',
													sortable: false,
													dataIndex: 'anneeScolaire',
													flex: 1,
												},
												{
													text: 'Frais de scolarité (Ariary)',
													sortable: false,
													dataIndex: 'fraisScolarite',
													flex: 1,
													renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
												},
												{
													text: 'Nombre d\'enfant',
													sortable: false,
													dataIndex: 'nbEnfant',
													flex: 1,
													renderer: value => value,
												},
											],
										},
									],
								},
								{
									columnWidth: '0.50',
									dockedItems: [{
										xtype: 'toolbar',
										items: [{
											xtype: 'button',
											ui: 'soft-green-small',
											iconCls: 'fa fa-download',
											handler: () => {
												Ext.getCmp('grapheColumnEnfant').download({
													filename: 'Effectif des enfants du personnel actuel',
													format: 'png',
												});
											},
										}],
									}],
									items: [
										{ // Graphe en "stacked column" enfants du personnel
											xtype: 'cartesian',
											reference: 'grapheColumnEnfantRef',
											id: 'grapheColumnEnfant',
											width: '100%',
											height: Ext.Element.getViewportHeight() - 100,
											store: ageEnfantPersonnelStore,
											legend: {
												type: 'sprite',
												docked: 'bottom',
											},
											insetPadding: 50,
											innerPadding: 20,
											sprites: [
												{
													type: 'text',
													text: 'Effectif des enfants du personnel actuel',
													font: '14px Arial',
													width: 100,
													height: 30,
													x: 40, // the sprite x position
													y: 20, // the sprite y position
												},
											],
											axes: [{
												type: 'numeric',
												position: 'left',
												adjustByMajorUnit: true,
												grid: true,
												fields: ['homme'],
												renderer: 'onAxisLabelRender',
												minimum: 0,
												title: 'Effectif du personnel',
											}, {
												type: 'category',
												position: 'bottom',
												grid: true,
												fields: ['age'],
												title: 'Âge (ans)',
												label: {
													rotate: {
														degrees: -45,
													},
												},
											}],
											series: [{
												type: 'bar',
												title: ['Masculin', 'Féminin'],
												xField: 'age',
												yField: ['homme', 'femme'],
												stacked: true,
												style: {
													opacity: 0.80,
												},
												highlight: {
													fillStyle: 'gold',
													lineWidth: 0,
												},
												tooltip: {
													renderer: 'onBarTipRender',
												},
												colors: ['#E88712', '#21AE8A'],
											}],
										},
									],
								},
							],
						},
					],
				},
				{
					title: 'CONTRAT',
					itemId: 'rapportContratTabId',
					id: 'rapportContratTabid',
					items: [
						{
							html: `<span class="x-fa fa-info-circle"></span> Cet onglet contient:
							<ul>
							<li>Nombre de contrats par centre</li>
							<li>Le graphe de r&eacute;partition des contrats par centre</li>
							<li>Nombre de contrats par poste de travail</li>
							<li>Le graphe d'anciennet&eacute; du personnel</li>
							</ul>`,
							itemId: 'helperId',
							baseCls: 'x-toast-info',
							anchor: '100%',
							margin: '0 0 5 0',
						},
						{
							layout: 'column',
							items: [
								{
									columnWidth: '0.50',
									items: [
										{
											xtype: 'gridpanel',
											title: 'Personnel par centre',
											itemId: 'rapportContratCentreGridId',
											id: 'rapportContratCentreGridid',
											columnLines: true,
											store: contratCentreStore,
											maxWidth: 420,
											maxHeight: 450,
											plugins: 'gridfilters',
											features: {
												ftype: 'summary',
												dock: 'top',
												id: 'sum',
											},
											columns: [
												{
													text: 'Centre',
													sortable: false,
													dataIndex: 'centre',
													filter: {
														type: 'list',
													},
													flex: 1,
												},
												{
													text: '<span class="fa fa-male"> </span> Masculin',
													sortable: false,
													dataIndex: 'homme',
													summaryType: 'sum',
													summaryRenderer: value => `<b>${value}<b>`,
												},
												{
													text: '<span class="fa fa-female"> </span> Féminin',
													sortable: false,
													dataIndex: 'femme',
													summaryType: 'sum',
													summaryRenderer: value => `<b>${value}<b>`,
												},
											],
										},
									],
								},
								{
									columnWidth: '0.50',
									dockedItems: [{
										xtype: 'toolbar',
										items: [{
											xtype: 'button',
											ui: 'soft-green-small',
											iconCls: 'fa fa-download',
											handler: () => {
												Ext.getCmp('grapheColumnContratCentre').download({
													filename: 'Effectif du personnel par centre',
													format: 'png',
												});
											},
										}],
									}],
									items: [
										{ // Graphe en "stacked column" pour les contrats par centre
											xtype: 'cartesian',
											reference: 'grapheColumnContratCentreRef',
											id: 'grapheColumnContratCentre',
											width: '100%',
											height: 500,
											store: contratCentreStore,
											legend: {
												type: 'sprite',
												docked: 'bottom',
											},
											insetPadding: 50,
											innerPadding: 20,
											sprites: [
												{
													type: 'text',
													text: 'Effectif du personnel par centre',
													font: '14px Arial',
													width: 100,
													height: 30,
													x: 40,
													y: 20,
												},
											],
											axes: [{
												type: 'numeric',
												position: 'left',
												adjustByMajorUnit: true,
												grid: true,
												fields: ['homme'],
												renderer: 'onAxisLabelRender',
												minimum: 0,
												title: 'Effectif du personnel',
											}, {
												type: 'category',
												position: 'bottom',
												fields: ['centre'],
												title: 'Centre',
												label: {
													rotate: {
														degrees: -45,
													},
												},
											}],
											series: [{
												type: 'bar',
												title: ['Masculin', 'Féminin'],
												xField: 'centre',
												yField: ['homme', 'femme'],
												stacked: true,
												style: {
													opacity: 0.80,
												},
												highlight: {
													fillStyle: 'gold',
													lineWidth: 0,
												},
												tooltip: {
													renderer: 'onBarTipRenderCenter',
												},
												colors: ['#9D5FFA', '#ff00ff'],
											}],
										},
									],	
								},
							],
						},
						{
							layout: 'column',
							items: [
								{
									columnWidth: '0.50',
									items: [
										{
											xtype: 'gridpanel',
											title: 'Personnel par poste de travail',
											itemId: 'rapportContratPosteGridId',
											id: 'rapportContratPosteGridid',
											columnLines: true,
											store: contratPosteStore,
											maxWidth: 420,
											maxHeight: 450,
											plugins: 'gridfilters',
											features: {
												ftype: 'summary',
												dock: 'top',
												id: 'sum',
											},
											columns: [
												{
													text: 'Fonction/Poste',
													sortable: false,
													dataIndex: 'poste',
													filter: {
														type: 'list',
													},
													width: 200,
													renderer: (value, metaData) => {
														Ext.apply(metaData, {
															tdAttr: `data-qtip= "${value}" data-qclass="tipCls" `,
														});
														return value;
													},
												},
												{
													text: '<span class="fa fa-male"> </span> Masculin',
													sortable: false,
													dataIndex: 'homme',
													summaryType: 'sum',
													summaryRenderer: value => `<b>${value}<b>`,
												},
												{
													text: '<span class="fa fa-female"> </span> Féminin',
													sortable: false,
													dataIndex: 'femme',
													summaryType: 'sum',
													summaryRenderer: value => `<b>${value}<b>`,
												},
											],
										},
									],
								},
								{
									columnWidth: '0.50',
									dockedItems: [{
										xtype: 'toolbar',
										items: [{
											xtype: 'button',
											ui: 'soft-green-small',
											iconCls: 'fa fa-download',
											handler: () => {
												Ext.getCmp('grapheContratAnciennete').download({
													filename: 'Ancienneté du personnel',
													format: 'png',
												});
											},
										}],
									}],
									items: [
										{
											xtype: 'cartesian',
											reference: 'chartContratAnciennete',
											id: 'grapheContratAnciennete',
											width: '100%',
											height: 500,
											insetPadding: 50,
											innerPadding: 20,
											store: contratAncienneteStore,
											legend: {
												type: 'sprite',
												docked: 'bottom',
											},
											axes: [{
												type: 'numeric',
												position: 'left',
												fields: ['homme', 'femme'],
												title: 'Répartition par ancienneté',
												grid: true,
												minimum: 0,
												adjustByMajorUnit: true,
												renderer: 'onAxisLabelRender',
											}, {
												type: 'category',
												position: 'bottom',
												fields: 'anneeAnciennete',
												title: 'Année d\'ancienneté',
												label: {
													rotate: {
														degrees: -45,
													},
												},
											}],
											// No 'series' config here,
											// Les séries sont ajoutées dynamiquement dans le Controller.
											sprites: [
												{
													type: 'text',
													text: 'Ancienneté du personnel',
													font: '14px Arial',
													width: 100,
													height: 30,
													x: 40,
													y: 20,
												},
											],
											listeners: {
												afterrender: 'onAfterRender',
											},
										},
									],
								},
							],
						},
					],
				},
				{
					title: 'SALAIRE',
					itemId: 'salaireTabId',
					id: 'salaireTabid',
					items: [
						{
							html: `<span class="x-fa fa-info-circle"></span> Cet onglet contient:
							<ul>
							<li>Donn&eacute;es de salaire</li>
							<li>Le graphe de r&eacute;partition de salaire par cat&eacute;gorie professionnelle</li>
							</ul>`,
							itemId: 'helperId',
							baseCls: 'x-toast-info',
							anchor: '100%',
							margin: '0 0 5 0',
						},
						{
							xtype: 'form',
							itemId: 'paramsRapportSalaireFormId',
							id: 'paramsRapportSalaireFormid',
							layout: {
								type: 'hbox',
							},
							margin: '0 0 5 0',
							items: [
								{
									xtype: 'numberfield',
									fieldLabel: 'Année de recherche',
									name: 'anneeRechercheSalaire',
									reference: 'anneeRechercheSalaireRef',
									publishes: 'value',
									itemId: 'anneeRechercheSalaireId',
									id: 'anneeRechercheSalaireid',
									allowDecimal: false,
									allowBlank: false,
									minValue: 2018,
									labelWidth: 'auto',
									msgTarget: 'side',
									listeners: {
										afterrender: () => {
											const els = Ext.select('div.x-form-trigger.x-form-trigger-default.x-form-trigger-spinner.x-form-trigger-spinner-default.x-unselectable', true);
											els.removeCls('x-form-trigger-default');
										},
									},
									margin: '0 50 0 0',
								},
								{
									itemId: 'RapportSalaireParamsBtn',
									xtype: 'button',
									text: 'Rechercher',
									tooltip: 'Rapporter cette année',
									ui: 'soft-purple-small',
									iconCls: 'fas fa-search',
									listeners: {
										click: 'onRapportSalaireSearch',
									},
								},
							],
						},
						{
							xtype: 'gridpanel',
							title: 'Salaires',
							itemId: 'salairePeriodiquesGridId',
							id: 'salairePeriodiquesGridid',
							columnLines: true,
							store: salaireRapportStore,
							maxHeight: 450,
							plugins: 'gridfilters',
							features: {
								ftype: 'summary',
								dock: 'top',
								id: 'sum',
							},
							columns: [
								{
									text: 'Catégorie professionnelle',
									sortable: false,
									dataIndex: 'categorieProfessionnelle',
									filter: {
										type: 'list',
									},
									width: 150,
								},
								{
									text: 'Salaire de base',
									sortable: false,
									dataIndex: 'salaireBase',
									summaryType: 'sum',
									summaryRenderer: value => `<b>${Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value))}<b>`,
									renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
									width: 150,
									align: 'right',
								},
								{
									text: 'Salaire Brut',
									sortable: false,
									dataIndex: 'salaireBrut',
									summaryType: 'sum',
									summaryRenderer: value => `<b>${Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value))}<b>`,
									renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
									width: 150,
									align: 'right',
								},
								{
									text: 'Salaire Brut + primes + intérim + HS',
									sortable: false,
									dataIndex: 'sBrutAvMaternite',
									summaryType: 'sum',
									summaryRenderer: value => `<b>${Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value))}<b>`,
									renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
									width: 150,
									align: 'right',
								},
								{
									text: 'Cotisations annuelles CNaPS',
									sortable: false,
									dataIndex: 'cnaps',
									summaryType: 'sum',
									summaryRenderer: value => `<b>${Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value))}<b>`,
									renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
									width: 150,
									align: 'right',
								},
								{
									text: 'Cotisations annuelles OMIT',
									sortable: false,
									dataIndex: 'smie',
									summaryType: 'sum',
									summaryRenderer: value => `<b>${Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value))}<b>`,
									renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
									width: 150,
									align: 'right',
								},
								{
									text: 'IRSA',
									sortable: false,
									dataIndex: 'irsaNet',
									summaryType: 'sum',
									summaryRenderer: value => `<b>${Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value))}<b>`,
									renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
									width: 150,
									align: 'right',
								},
								{
									text: 'Salaires annuels vérsés',
									sortable: false,
									dataIndex: 'netAPayer',
									summaryType: 'sum',
									summaryRenderer: value => `<b>${Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value))}<b>`,
									renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
									width: 150,
									align: 'right',
								},
															
							],
						},
						{
							xtype: 'panel',
							itemId: 'salaireChartPanelId',
							id: 'salaireChartPanelid',
							margin: '50 0 0 0',
							tbar: [
								'->',
								{
									html: '<span class="x-fa fa-info-circle"> Choisissez ici=>></span>',
									itemId: 'helperId',
									baseCls: 'x-toast-info',
									anchor: '100%',
									margin: '0 0 5 0',
								},
								{
									text: 'Salaire de base',
									enableToggle: true,
									handler: (btn) => {
										Ext.getCmp('rapportMainContainerid').getController('rapportController').onChangeVariables('salaireBase', '#a5c249', btn);
									},
								},
								{
									text: 'Salaire brut',
									enableToggle: true,
									handler: (btn) => {
										Ext.getCmp('rapportMainContainerid').getController('rapportController').onChangeVariables('salaireBrut', '#ff00ff', btn);
									},
								},
								{
									text: 'Salaire brut avant maternité',
									enableToggle: true,
									handler: (btn) => {
										Ext.getCmp('rapportMainContainerid').getController('rapportController').onChangeVariables('sBrutAvMaternite', '#E88712', btn);
									},
								},
								{
									text: 'CNaPS',
									enableToggle: true,
									handler: (btn) => {
										Ext.getCmp('rapportMainContainerid').getController('rapportController').onChangeVariables('cnaps', '#9D5FFA', btn);
									},
								},
								{
									text: 'SMIE',
									enableToggle: true,
									handler: (btn) => {
										Ext.getCmp('rapportMainContainerid').getController('rapportController').onChangeVariables('smie', '#21AE8A', btn);
									},
								},
								{
									text: 'IRSA',
									enableToggle: true,
									handler: (btn) => {
										Ext.getCmp('rapportMainContainerid').getController('rapportController').onChangeVariables('irsaNet', '#ED7348', btn);
									},
								},
								{
									text: 'Net à payer',
									enableToggle: true,
									handler: (btn) => {
										Ext.getCmp('rapportMainContainerid').getController('rapportController').onChangeVariables('netAPayer', '#D7ED48', btn);
									},
								},
								{
									xtype: 'toolbar',
									items: [{
										xtype: 'button',
										ui: 'soft-green-small',
										iconCls: 'fa fa-download',
										handler: () => {
											Ext.getCmp('grapheColumnSalaire').download({
												filename: 'Répartition de salaire par catégorie professionnelle',
												format: 'png',
											});
										},
									}],
								},
							],
						
							items: { // Graphe en "bar" pour les salaires
								xtype: 'cartesian',
								reference: 'grapheColumnSalaireRef',
								id: 'grapheColumnSalaire',
								width: '100%',
								height: Ext.Element.getViewportHeight() - 100,
								store: salaireRapportStore,
								legend: {
									type: 'sprite',
									docked: 'top',
								},
								insetPadding: 50,
								innerPadding: 20,
								sprites: [
									{
										type: 'text',
										text: 'Répartition de salaire par catégorie professionnelle',
										font: '14px Arial',
										width: 100,
										height: 30,
										x: 40,
										y: 20,
									},
								],
								axes: [{
									type: 'numeric',
									position: 'left',
									adjustByMajorUnit: true,
									grid: true,
									title: 'Montant en Ariary',
									renderer: 'onAxisLabelRenderSalaire',
								}, {
									type: 'category',
									position: 'bottom',
									title: 'Catégorie professionnelle',
									label: {
										rotate: {
											degrees: -45,
										},
									},
								}],
								series: [{
									type: 'bar',
									xField: 'categorieProfessionnelle',
									yField: 'salaireBase',
									title: 'Salaire de base',
									style: {
										opacity: 0.80,
									},
									highlight: {
										fillStyle: 'gold',
										lineWidth: 0,
									},
									tooltip: {
										renderer: 'onBarTipRenderSalaire',
									},
									colors: ['#a5c249'],
								}],
							},
						},
					],
				},
				{
					title: 'EVOLUTIONS',
					itemId: 'evolutionsTabId',
					id: 'evolutionsTabid',
					items: [
						{
							html: `<span class="x-fa fa-info-circle"></span> Cet onglet contient:
							<ul>
							<li>Les renseignements p&eacute;riodiques demand&eacute;s par l'inspection du travail</li>
							<li>2 panels sur l'&eacute;volution de l'effectif
							<ul>
							<li>Tableau r&eacute;capitulatif</li>
							<li>Graphe des &eacute;volutions: Effectif total, Total entrant, Total sortant, Evolutions par genre</li>
							</ul>
							</li>
							<li>2 panels sur l'&eacute;volution des salaires
							<ul>
							<li>Tableau r&eacute;capitulatif</li>
							<li>Graphe des &eacute;volutions: Salaire de base, Salaire brut, Net &agrave; payer</li>
							</ul>
							</li>
							</ul>`,
							itemId: 'helperId',
							baseCls: 'x-toast-info',
							anchor: '100%',
							margin: '0 0 5 0',
						},
						{
							xtype: 'form',
							itemId: 'paramsRapportInspecFormId',
							id: 'paramsRapportInspecFormid',
							layout: {
								type: 'hbox',
							},
							margin: '0 0 5 0',
							items: [
								{
									xtype: 'numberfield',
									fieldLabel: 'Année de recherche',
									name: 'anneeRecherche',
									reference: 'anneeRechercheRef',
									publishes: 'value',
									itemId: 'anneeRechercheId',
									id: 'anneeRechercheid',
									allowDecimal: false,
									allowBlank: false,
									minValue: 2018,
									labelWidth: 'auto',
									msgTarget: 'side',
									listeners: {
										afterrender: () => {
											const els = Ext.select('div.x-form-trigger.x-form-trigger-default.x-form-trigger-spinner.x-form-trigger-spinner-default.x-unselectable', true);
											els.removeCls('x-form-trigger-default');
										},
									},
									margin: '0 50 0 0',
								},
								{
									itemId: 'RapportInspecParamsBtn',
									xtype: 'button',
									text: 'Rechercher',
									tooltip: 'Rapporter cette année',
									ui: 'soft-purple-small',
									iconCls: 'fas fa-search',
									listeners: {
										click: 'onRapportInspecSearch',
									},
								},
							],
						},
						{
							xtype: 'gridpanel',
							title: 'Renseignements périodiques annuels',
							itemId: 'evolutionsPeriodiquesGridId',
							id: 'evolutionsPeriodiquesGridid',
							columnLines: true,
							store: inspectionRapportStore,
							maxHeight: 450,
							plugins: 'gridfilters',
							features: {
								ftype: 'summary',
								dock: 'top',
								id: 'sum',
							},
							columns: [
								{
									text: 'Catégorie professionnelle',
									dataIndex: 'categorieProfessionnelle',
									filter: {
										type: 'list',
									},
									width: 150,
									summaryType: 'sum',
									summaryRenderer: () => '',
									// locked: true,
								},
								{
									text: '<span class="fa fa-male"> </span> Masculin',
									sortable: false,
									dataIndex: 'homme',
									summaryType: 'sum',
									summaryRenderer: value => `<b>${value}<b>`,
								},
								{
									text: '<span class="fa fa-female"> </span> Féminin',
									sortable: false,
									dataIndex: 'femme',
									summaryType: 'sum',
									summaryRenderer: value => `<b>${value}<b>`,
								},
								{
									text: 'Salaires annuels vérsés',
									sortable: false,
									dataIndex: 'netAPayer',
									summaryType: 'sum',
									summaryRenderer: value => `<b>${Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value))}<b>`,
									renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
									width: 150,
									align: 'right',
								},
								{
									text: 'Cotisations annuelles CNaPS',
									sortable: false,
									dataIndex: 'cnaps',
									summaryType: 'sum',
									summaryRenderer: value => `<b>${Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value))}<b>`,
									renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
									width: 150,
									align: 'right',
								},
								{
									text: 'Cotisations annuelles OMIT',
									sortable: false,
									dataIndex: 'smie',
									summaryType: 'sum',
									summaryRenderer: value => `<b>${Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value))}<b>`,
									renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
									width: 150,
									align: 'right',
								},
								{
									text: 'Embauche',
									columns: [
										{
											text: '<span class="fa fa-male"> </span> Masculin',
											sortable: false,
											dataIndex: 'hommeEmbauche',
											summaryType: 'sum',
											summaryRenderer: value => `<b>${value}<b>`,
										},
										{
											text: '<span class="fa fa-female"> </span> Féminin',
											sortable: false,
											dataIndex: 'femmeEmbauche',
											summaryType: 'sum',
											summaryRenderer: value => `<b>${value}<b>`,
										},
									],
								},
								{
									text: 'Démission',
									columns: [
										{
											text: '<span class="fa fa-male"> </span> Masculin',
											sortable: false,
											dataIndex: 'hommeDemission',
											summaryType: 'sum',
											summaryRenderer: value => `<b>${value}<b>`,
										},
										{
											text: '<span class="fa fa-female"> </span> Féminin',
											sortable: false,
											dataIndex: 'femmeDemission',
											summaryType: 'sum',
											summaryRenderer: value => `<b>${value}<b>`,
										},
									],
								},
								{
									text: 'Licenciement',
									columns: [
										{
											text: '<span class="fa fa-male"> </span> Masculin',
											sortable: false,
											dataIndex: 'hommeLicenciement',
											summaryType: 'sum',
											summaryRenderer: value => `<b>${value}<b>`,
										},
										{
											text: '<span class="fa fa-female"> </span> Féminin',
											sortable: false,
											dataIndex: 'femmeLicenciement',
											summaryType: 'sum',
											summaryRenderer: value => `<b>${value}<b>`,
										},
									],
								},
								{
									text: 'Chômage technique',
									columns: [
										{
											text: '<span class="fa fa-male"> </span> Masculin',
											sortable: false,
											dataIndex: 'hommeChTec',
											summaryType: 'sum',
											summaryRenderer: value => `<b>${value}<b>`,
										},
										{
											text: '<span class="fa fa-female"> </span> Féminin',
											sortable: false,
											dataIndex: 'femmeChTec',
											summaryType: 'sum',
											summaryRenderer: value => `<b>${value}<b>`,
										},
									],
								},
							],
						},
						{
							xtype: 'panel',
							itemsId: 'evolutionEffectifPanelId',
							id: 'evolutionEffectifPanelid',
							height: Ext.Element.getViewportHeight() - 65,
							layout: {
								type: 'hbox',
								pack: 'start',
								align: 'stretch',
							},
							margin: '5 0 0 0',
							items: [
								{
									title: 'Tableau de l\'évolution de l\'effectif',
									flex: 1,
									collapsed: true,
									collapsible: true,
									collapseDirection: 'left',
									collapseMode: 'header',
									margin: '0 1 0 0',
									itemId: 'effectifGridPanelId',
									items: [
										{
											xtype: 'gridpanel',
											itemId: 'evolutionEffectifGridId',
											id: 'evolutionEffectifGridid',
											columnLines: true,
											store: evolutionEffectif1Store,
											maxHeight: Ext.Element.getViewportHeight() - 80,
											width: '100%',
											columns: [
												{
													text: 'Année',
													sortable: false,
													dataIndex: 'anneeEmbauche',
													width: 75,
												},
												{
													text: 'Homme entrant',
													sortable: false,
													dataIndex: 'hommeEntrant',
													flex: 1,												
												},
												{
													text: 'Femme entrant',
													sortable: false,
													dataIndex: 'femmeEntrant',
													flex: 1,												
												},
												{
													text: 'Homme débauché',
													sortable: false,
													dataIndex: 'hommeDebauche',
													flex: 1,										
												},
												{
													text: 'Femme débauchée',
													sortable: false,
													dataIndex: 'femmeDebauche',
													flex: 1,
												},
												{
													text: 'Total entrant',
													sortable: false,
													dataIndex: 'totalEntrant',
													width: 100,
												},
												{
													text: 'Total sortant',
													sortable: false,
													dataIndex: 'totalDebauche',
													width: 100,
												},
												{
													text: 'Effectif total',
													sortable: false,
													dataIndex: 'totalEffectif',
													width: 100,
												},
											],
										},
									],
									listeners: {
										expand: (self) => {
											self.up().down('#effectifGraphePanelId').collapse(true);
										},
										collapse: (self) => {
											self.up().down('#effectifGraphePanelId').expand(true);
										},
									},
								},
								{
									title: 'Graphes des effectifs',
									flex: 1,
									collapsible: true,
									collapseDirection: 'left',
									collapseMode: 'header',
									margin: '0 1 0 0',
									itemId: 'effectifGraphePanelId',
									dockedItems: [{
										xtype: 'toolbar',
										items: [{
											xtype: 'button',
											ui: 'soft-green-small',
											iconCls: 'fa fa-download',
											handler: () => {
												Ext.getCmp('grapheLineEffectif').download({
													filename: 'Evolution de l\'effectif du personnel d\'ADES',
													format: 'png',
												});
											},
										}],
									}],
									items: [
										{ // Graphe en  pour l'évolution de l'effectif par année et par sexe
											xtype: 'cartesian',
											reference: 'grapheLineEffectifRef',
											id: 'grapheLineEffectif',
											width: '100%',
											height: Ext.Element.getViewportHeight() - 150,
											store: evolutionEffectif1Store,
											legend: {
												type: 'sprite',
												docked: 'bottom',
											},
											insetPadding: 50,
											innerPadding: 20,
											margin: '10 0 0 0',
											sprites: [
												{
													type: 'text',
													text: 'Evolution de l\'effectif du personnel d\'ADES',
													font: '14px Arial',
													width: 100,
													height: 30,
													x: 40, // the sprite x position
													y: 20, // the sprite y position
												},
											],
											axes: [{
												type: 'numeric',
												position: 'left',
												adjustByMajorUnit: true,
												grid: true,
												fields: ['hommeEntrant', 'femmeEntrant', 'hommeDebauche', 'femmeDebauche', 'totalEntrant', 'totalDebauche', 'totalEffectif'],
												renderer: 'onAxisLabelRender',
												minimum: 0,
												title: 'Effectif du personnel',
											}, {
												type: 'category',
												position: 'bottom',
												grid: true,
												fields: ['anneeEmbauche'],
												title: 'Année',
												label: {
													rotate: {
														degrees: -45,
													},
												},
											}],
											series: [{
												type: 'area',
												xField: 'anneeEmbauche',
												yField: 'totalEffectif',
												title: 'Effectif total',
												style: {
													lineWidth: 2,
													opacity: 0.40,
												},
											}, {
												type: 'area',
												xField: 'anneeEmbauche',
												yField: 'totalEntrant',
												title: 'Total entrant',
												style: {
													lineWidth: 2,
													opacity: 0.20,
												},
											}, {
												type: 'area',
												xField: 'anneeEmbauche',
												yField: 'totalDebauche',
												title: 'Total débauche',
												style: {
													lineWidth: 2,
													opacity: 0.20,
												},
											}, {
												type: 'line',
												title: 'Masculin entrant',
												xField: 'anneeEmbauche',
												yField: 'hommeEntrant',
												marker: {
													type: 'triangle',
													fx: {
														duration: 200,
														easing: 'backOut',
													},
												},
												highlightCfg: {
													scaling: 2,
												},
												tooltip: {
													trackMouse: true,
													renderer: (tooltip, record, item) => Ext.getCmp('rapportMainContainerid').getController('rapportController').onSeriesTooltipRenderEvolution(tooltip, record, item, 'anneeEmbauche', 'hommeEntrant'),
												},
											}, {
												type: 'line',
												title: 'Femme entrant',
												xField: 'anneeEmbauche',
												yField: 'femmeEntrant',
												marker: {
													type: 'triangle',
													fx: {
														duration: 200,
														easing: 'backOut',
													},
												},
												highlightCfg: {
													scaling: 2,
												},
												tooltip: {
													trackMouse: true,
													renderer: (tooltip, record, item) => Ext.getCmp('rapportMainContainerid').getController('rapportController').onSeriesTooltipRenderEvolution(tooltip, record, item, 'anneeEmbauche', 'femmeEntrant'),
												},
											}, {
												type: 'line',
												title: 'Homme débauche',
												xField: 'anneeEmbauche',
												yField: 'hommeDebauche',
												marker: {
													type: 'circle',
													fx: {
														duration: 200,
														easing: 'backOut',
													},
												},
												highlightCfg: {
													scaling: 2,
												},
												tooltip: {
													trackMouse: true,
													renderer: (tooltip, record, item) => Ext.getCmp('rapportMainContainerid').getController('rapportController').onSeriesTooltipRenderEvolution(tooltip, record, item, 'anneeEmbauche', 'hommeDebauche'),
												},
											}, {
												type: 'line',
												title: 'Femme débauche',
												xField: 'anneeEmbauche',
												yField: 'femmeDebauche',
												marker: {
													type: 'circle',
													fx: {
														duration: 200,
														easing: 'backOut',
													},
												},
												highlightCfg: {
													scaling: 2,
												},
												tooltip: {
													trackMouse: true,
													renderer: (tooltip, record, item) => Ext.getCmp('rapportMainContainerid').getController('rapportController').onSeriesTooltipRenderEvolution(tooltip, record, item, 'anneeEmbauche', 'femmeDebauche'),
												},
											}],
										},
									],
									listeners: {
										expand: (self) => {
											self.up().down('#effectifGridPanelId').collapse(true);
										},
										collapse: (self) => {
											self.up().down('#effectifGridPanelId').expand(true);
										},
									},
								},
							],
						},
						{
							xtype: 'panel',
							itemsId: 'evolutionSalairePanelId',
							id: 'evolutionSalairePanelid',
							height: Ext.Element.getViewportHeight() - 65,
							layout: {
								type: 'hbox',
								pack: 'start',
								align: 'stretch',
							},
							margin: '5 0 0 0',
							items: [
								{
									title: 'Tableau de l\'évolution des salaires',
									flex: 1,
									collapsed: true,
									collapsible: true,
									collapseDirection: 'left',
									collapseMode: 'header',
									margin: '0 1 0 0',
									itemId: 'salaireGridPanelId',
									items: [
										{
											xtype: 'gridpanel',
											itemId: 'evolutionsSalairesGridId',
											id: 'evolutionsSalairesGridid',
											columnLines: true,
											store: evolutionSalaireStore,
											maxHeight: 450,
											columns: [
												{
													text: 'Année',
													sortable: false,
													dataIndex: 'anneeSalaire',
													width: 150,
													locked: true,
												},
												{
													text: 'Salaire de base',
													sortable: false,
													dataIndex: 'salaireBase',
													summaryType: 'sum',
													renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
													width: 150,
													align: 'right',
												},
												{
													text: 'Salaire Brut',
													sortable: false,
													dataIndex: 'salaireBrut',
													summaryType: 'sum',
													renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
													width: 150,
													align: 'right',
												},
												{
													text: 'Salaire Brut + primes + intérim + HS',
													sortable: false,
													dataIndex: 'sBrutAvMaternite',
													summaryType: 'sum',
													renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
													width: 150,
													align: 'right',
												},
												{
													text: 'Cotisations annuelles CNaPS',
													sortable: false,
													dataIndex: 'cnaps',
													summaryType: 'sum',
													renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
													width: 150,
													align: 'right',
												},
												{
													text: 'Cotisations annuelles OMIT',
													sortable: false,
													dataIndex: 'smie',
													summaryType: 'sum',
													renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
													width: 150,
													align: 'right',
												},
												{
													text: 'IRSA',
													sortable: false,
													dataIndex: 'irsaNet',
													summaryType: 'sum',
													renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
													width: 150,
													align: 'right',
												},
												{
													text: 'Salaires annuels vérsés',
													sortable: false,
													dataIndex: 'netAPayer',
													summaryType: 'sum',
													renderer: value => Gprh.ades.util.Globals.formatRenderNumber(parseFloat(value)),
													width: 150,
													align: 'right',
												},
																			
											],
										},
									],
									listeners: {
										expand: (self) => {
											self.up().down('#salaireGraphePanelId').collapse(true);
										},
										collapse: (self) => {
											self.up().down('#salaireGraphePanelId').expand(true);
										},
									},
								},
								{
									title: 'Graphes des salaires',
									flex: 1,
									collapsible: true,
									collapseDirection: 'left',
									collapseMode: 'header',
									margin: '0 1 0 0',
									itemId: 'salaireGraphePanelId',
									dockedItems: [{
										xtype: 'toolbar',
										items: [{
											xtype: 'button',
											ui: 'soft-green-small',
											iconCls: 'fa fa-download',
											handler: () => {
												Ext.getCmp('grapheLineSalaire').download({
													filename: 'Evolution de salaires d\'ADES',
													format: 'png',
												});
											},
										}],
									}],
									items: [
										{ // Graphe en  pour l'évolution de salaire par année
											xtype: 'cartesian',
											reference: 'grapheLineSalaireRef',
											id: 'grapheLineSalaire',
											width: '100%',
											height: Ext.Element.getViewportHeight() - 150,
											store: evolutionSalaireStore,
											legend: {
												type: 'sprite',
												docked: 'bottom',
											},
											insetPadding: 50,
											innerPadding: 20,
											margin: '10 0 0 0',
											sprites: [
												{
													type: 'text',
													text: 'Evolution de salaires d\'ADES',
													font: '14px Arial',
													width: 100,
													height: 30,
													x: 40, // the sprite x position
													y: 20, // the sprite y position
												},
											],
											axes: [{
												type: 'numeric',
												position: 'left',
												adjustByMajorUnit: true,
												grid: true,
												fields: ['salaireBase'],
												renderer: 'onAxisLabelRenderSalaire',
												minimum: 0,
												title: 'Salaires en Ariary',
											}, {
												type: 'category',
												position: 'bottom',
												grid: true,
												fields: ['anneeSalaire'],
												title: 'Année',
												label: {
													rotate: {
														degrees: -45,
													},
												},
											}],
											series: [{
												type: 'bar',
												xField: 'anneeSalaire',
												yField: ['salaireBase', 'salaireBrut', 'netAPayer'],
												title: ['S.Base', 'S.Brut', 'Net à payer'],
												lineWidth: 10,
												stacked: false,
												style: {
													opacity: 0.40,
												},
											}],
										},
									],
									listeners: {
										expand: (self) => {
											self.up().down('#salaireGridPanelId').collapse(true);
										},
										collapse: (self) => {
											self.up().down('#salaireGridPanelId').expand(true);
										},
									},
								},
							],
						},
					],
				},
				{
					title: 'RESUME',
					itemId: 'resumeTabId',
					id: 'resumeTabid',
					items: [
						{
							html: '<span class="x-fa fa-info-circle"></span> Cet onglet contient les indicateurs numériques du personnel nécessaires au rapport semestriel.',
							itemId: 'helperId',
							baseCls: 'x-toast-info',
							anchor: '100%',
							margin: '0 0 5 0',
						},
						{
							xtype: 'gridpanel',
							itemId: 'resumeGridId',
							id: 'resumeGridid',
							hideHeaders: true,
							store: resumeRapportStore,
							columns: [
								{
									dataIndex: 'indicateur',
									sortable: false,
									menuDisabled: true,
									flex: 1,
								},
								{
									dataIndex: 'valeurIndicateur',
									sortable: false,
									menuDisabled: true,
									flex: 1,
								},
							],
						},
					],
				},
				{
					title: 'ABSENCES',
					itemId: 'absenceTabId',
					id: 'absenceTabid',
					items: [
						{
							xtype: 'button',
							ui: 'soft-green-small',
							iconCls: 'x-fa fa-file-excel-o',
							tooltip: 'Exporter la table sous Excel',
							maxWidth: 32,
							handler: () => {
								Ext.getCmp('rapportMainContainerid').getController('rapportController').exportTableToExcel('Liste');
							},
						},
						{
							xtype: 'panel',
							autoScroll: true,
							id: 'absenceContainerId',
							layout: {
								type: 'vbox',
								align: 'stretch',
							},
							height: Ext.Element.getViewportHeight() - 220,
						},
						
					],
					listeners: {
						afterrender: () => {
							const dataJSON = [];
							absenceRapportStore.each((rec) => {
								dataJSON.push(rec.data);
							});
							
							const nrecoPivotExt = new NRecoPivotTableExtensions({
								wrapWith: `<div class="pvtTableRendererHolder" style="max-width:${Ext.Element.getViewportWidth() - 600}px;max-height: ${Ext.Element.getViewportHeight() - 320}px;"></div>`, // special div is needed by fixed headers when used with pivotUI
								fixedHeaders: true,
								// sortByLabelEnabled: false,
							});
							
							const stdRendererNames = ['Table', 'Table Barchart', 'Heatmap', 'Row Heatmap', 'Col Heatmap'];
							const wrappedRenderers = $.extend({}, $.pivotUtilities.renderers);
							Ext.each(stdRendererNames, (rName) => {
								wrappedRenderers[rName] = nrecoPivotExt.wrapTableRenderer(wrappedRenderers[rName]);
							});

							// eslint-disable-next-line prefer-destructuring
							const dateFormat = $.pivotUtilities.derivers.dateFormat;

							const dateRange = (startDate, endDate) => {
								const start = startDate.split('-');
								const end = endDate.split('-');
								const startYear = parseInt(start[0], 10);
								const endYear = parseInt(end[0], 10);
								const dates = [];
								const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
								for (let i = startYear; i <= endYear; i += 1) {
									const endMonth = i !== endYear ? 11 : parseInt(end[1], 10) - 1;
									const startMon = i === startYear ? parseInt(start[1], 10) - 1 : 0;
									for (let j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j + 1) {
										const month = j + 1;
										// const displayMonth = month < 10 ? `0${month}` : month;
										const displayMonth = months[month - 1];
										dates.push([displayMonth, i].join('-'));
									}
								}
								return dates;
							};
							
							$('#absenceContainerId').pivotUI(dataJSON, {
								derivedAttributes: {
									Mensuel: dateFormat('Date', '%n-%y'),
									Mois: dateFormat('Date', '%n'),
									Année: dateFormat('Date', '%y'),
								},
								renderers: wrappedRenderers,
								rows: ['Type'],
								cols: ['Mensuel'],
								vals: ['Type'],
								aggregatorName: 'Count',
								rendererName: 'Heatmap',
								onRefresh: () => {
									// this is correct way to apply fixed headers with pivotUI
									nrecoPivotExt.initFixedHeaders($('#absenceContainerId table.pvtTable'));
								},
								hiddenAttributes: ['id', 'typeAbs', 'Date début', 'Date fin'],
								// unusedAttrsVertical: 250,
								sorters: {
									// Elargir l'intervalle de dates pour ordonner l'affichage des mois
									Mensuel: $.pivotUtilities.sortAs(dateRange('2018-12-01', '2050-12-31')),
								},
							});
						},
					},
				},
			],
		};
		
		this.items = [
			{
				xtype: 'form',
				itemId: 'paramsRapportFormId',
				id: 'paramsRapportFormid',
				layout: {
					type: 'hbox',
				},
				margin: '0 0 5 0',
				items: [
					{
						xtype: 'datefield',
						name: 'dateDebut',
						itemId: 'dateDebutId',
						reference: 'dateDebutRef',
						publishes: 'value',
						fieldLabel: 'Entre',
						format: 'd/m/Y',
						submitFormat: 'Y-m-d',
						labelWidth: 'auto',
						allowBlank: false,
					},
					{
						xtype: 'datefield',
						name: 'dateFin',
						itemId: 'dateFinId',
						reference: 'dateFinRef',
						publishes: 'value',
						fieldLabel: 'Et',
						format: 'd/m/Y',
						submitFormat: 'Y-m-d',
						labelWidth: 'auto',
						margin: '0 50 0 50',
						allowBlank: false,
					},
					{
						itemId: 'RapportParamsBtn',
						xtype: 'button',
						text: 'Rechercher',
						tooltip: 'Rapport entre les dates',
						ui: 'soft-purple-small',
						iconCls: 'fas fa-search',
						formBind: true,
						listeners: {
							click: () => {
								Ext.getCmp('rapportMainContainerid').getController('rapportController').onRapportSearch(globalStore);
							},
						},
					},
					{
						itemId: 'RapportClearParamsBtn',
						xtype: 'button',
						text: 'Réinitialiser',
						tooltip: 'Réinitialiser les dates',
						ui: 'soft-red-small',
						iconCls: 'fas fa-trash',
						formBind: true,
						listeners: {
							click: () => {
								Ext.getCmp('rapportMainContainerid').getController('rapportController').onRapportSearchClear(globalStore);
							},
						},
					},
				],
			},
			tabPanels,
		];
		this.callParent();
	},
});
