Ext.define('Gprh.ades.controller.RapportController', {
	extend: 'Ext.app.ViewController',
	
	alias: 'controller.rapportController',

	requires: [
		'Gprh.ades.util.Globals',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	init() {},

	onAxisLabelRender(axis, label) { // Axes des ordonnées des Graphes
		return `${label.toFixed(label < 10 ? 1 : 0)}`;
	},

	// Tooltip des Graphes dans Personnel et Contrat (Ancienneté du personnel)
	onBarTipRender(tooltip, record, item) { // Axe des abscisses des Graphes en Barres Personnel et Contrat
		const fieldIndex = Ext.Array.indexOf(item.series.getYField(), item.field);
		const browser = item.series.getTitle()[fieldIndex];

		tooltip.setHtml(`${browser} à ${record.get('age')} ans: ${record.get(item.field)}`);
	},

	// Tooltip Graphe en "pie" Répartition du personnel
	onSeriesTooltipRender(tooltip, record) { // Tooltip pour la graphe en pie
		tooltip.setHtml(`${record.get('age')}: ${record.get('data1')}`);
	},

	// Envoie les intervalles de recherche et récupère les stores
	onRapportSearch(globalStore) {
		const form = Ext.getCmp('paramsRapportFormid');
		const extraParamsRapport = {
			extraParams: {
				infoRequest: 2,
				dateDebut: form.getValues().dateDebut,
				dateFin: form.getValues().dateFin,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		Ext.apply(globalStore.getProxy(), extraParamsRapport);		
		globalStore.load();
	},

	// Réinitiliser les stores
	onRapportSearchClear(globalStore) {
		Ext.getCmp('paramsRapportFormid').reset();
		const extraParamsRapport = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		Ext.apply(globalStore.getProxy(), extraParamsRapport);		
		globalStore.load();
	},

	// Tooltip Graphe en "stacked column" pour les contrats par centre
	onBarTipRenderCenter(tooltip, record, item) { // Axe des abscisses
		const fieldIndex = Ext.Array.indexOf(item.series.getYField(), item.field);
		const sexe = item.series.getTitle()[fieldIndex];

		tooltip.setHtml(`${sexe} chez ${record.get('centre')}: ${record.get(item.field)}`);
	},

	// Graphe area des contrats par anciennete
	getSeriesConfig(field, titles) {
		return {
			type: 'area',
			title: titles,
			xField: 'anneeAnciennete',
			yField: field,
			style: {
				opacity: 0.60,
			},
			marker: {
				opacity: 0,
				scaling: 0.01,
				fx: {
					duration: 200,
					easing: 'easeOut',
				},
			},
			highlightCfg: {
				opacity: 1,
				scaling: 1.5,
			},
			tooltip: {
				trackMouse: true,
				renderer: (tooltip, record) => {
					tooltip.setHtml(`${titles} (${record.get('anneeAnciennete')} ans): ${record.get(field)}`);
				},
			},
		};
	},

	onAfterRender() {
		const me = this;
		const chart = me.lookupReference('chartContratAnciennete');
		chart.setSeries([
			me.getSeriesConfig('homme', 'Masculin'),
			me.getSeriesConfig('femme', 'Féminin'),
		]);
	},

	// Clic sur le bouton "Rechercher" dans le tab panel EVOLUTIONS
	onRapportInspecSearch() {
		const rapportInspecGrid = Ext.getCmp('evolutionsPeriodiquesGridid');
		const updatedStore = Ext.create('Ext.data.Store', {  
			proxy: {
				type: 'ajax',  
				url: 'server-scripts/listes/Rapport.php',
				async: false, 
				reader: {  
					type: 'json',
					rootProperty: 'inspectionRapport',
				},
				extraParams: {
					infoRequest: 3,
					anneeRecherche: Ext.getCmp('anneeRechercheid').getValue(),
					typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
					idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
				},  
			},  
			autoLoad: true,
		});
		rapportInspecGrid.reconfigure(updatedStore);
	},

	// Clic sur le bouton "Rechercher" dans le tab panel SALAIRE
	onRapportSalaireSearch() {
		const rapportInspecGrid = Ext.getCmp('salairePeriodiquesGridid');
		const updatedStore = Ext.create('Ext.data.Store', {  
			proxy: {
				type: 'ajax',  
				url: 'server-scripts/listes/Rapport.php',
				async: false, 
				reader: {  
					type: 'json',
					rootProperty: 'salaireRapport',
				},
				extraParams: {
					infoRequest: 4,
					anneeRecherche: Ext.getCmp('anneeRechercheSalaireid').getValue(),
					typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
					idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
				},  
			},  
			autoLoad: true,
		});
		rapportInspecGrid.reconfigure(updatedStore);
	},

	// Tooltip Graphe Salaire
	onBarTipRenderSalaire(tooltip, record, item) { // Axe des abscisses des Graphes en Barres Salaire
		tooltip.setHtml(`${record.get('categorieProfessionnelle')}: ${Gprh.ades.util.Globals.formatRenderNumber(parseFloat(record.get(item.field)))} Ar`);
	},

	// Clic pour changer dynamiquement les données en abcisses des Graphes en Barres Salaire
	onChangeVariables(yFields, colors, btn) {
		const chart = Ext.getCmp('grapheColumnSalaire');
		
		// define the new serie (do not use Ext.create())
		const serie = {
			type: 'bar',
			xField: 'categorieProfessionnelle',
			yField: yFields,
			title: btn.text,
			style: {
				opacity: 0.80,
			},
			highlight: {
				fillStyle: 'gold',
				lineWidth: 0,
			},
			tooltip: {
				renderer: 'onBarTipRenderSalaire',
			},
			colors: [colors],
		};

		const chartSeries = [];
		// add the serie to the chart (the variable 'chart' holds the complete chart component)
		chartSeries.push(serie);
		chart.setSeries(chartSeries);

		// redraw the chart
		chart.redraw();
	},

	// Axe des abscisses du Graphe Salaire pour réduire les zéros du million
	onAxisLabelRenderSalaire(axis, label) {
		return `${(label / 1000000)}.10^6`;
	},

	// Tooltip Graphe en Evolution
	onSeriesTooltipRenderEvolution(tooltip, record, item, ordonnees, abscisses) {
		const sexe = item.series.getTitle();
		tooltip.setHtml(`${sexe} en ${record.get(ordonnees)}: ${record.get(abscisses)}`);
	},

	// Exporter la table pivot Absence en Excel
	exportTableToExcel(filename = '') {
		const dataType = 'application/vnd.ms-excel';
		const tableSelect = document.getElementsByClassName('pvtTable');
		console.log(tableSelect);
		const tableHTML = tableSelect[0].outerHTML.replace(/ /g, '%20');
		// Specify file name
		const filenames = filename ? `${filename}.xls` : 'excel_data.xls';
		// Create download link element
		const downloadLink = document.createElement('a');
		document.body.appendChild(downloadLink);
		if (navigator.msSaveOrOpenBlob) {
			const blob = new Blob(['\ufeff', tableHTML], {
				type: dataType,
			});
			navigator.msSaveOrOpenBlob(blob, filenames);
		} else {
		// Create a link to the file
			downloadLink.href = `data:${dataType}, ${tableHTML}`;
			// Setting the file name
			downloadLink.download = filenames;
			// triggering the function
			downloadLink.click();
		}
	},
});
