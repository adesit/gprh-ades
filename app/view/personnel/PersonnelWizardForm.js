Ext.define('Gprh.ades.view.personnel.PersonnelWizardForm', {
	extend: 'Ext.panel.Panel',
	xtype: 'wizardform',

	requires: [
		'Ext.ux.layout.ResponsiveColumn',
		'Gprh.ades.store.Conjoint',
		'Gprh.ades.store.Correspondance',
		'Gprh.ades.view.personnel.forms.Renseignement',
		'Gprh.ades.view.personnel.forms.ConjointCorrespondant',
		'Gprh.ades.view.personnel.forms.EnfantScolarite',
		'Gprh.ades.view.personnel.grid.Bancaire',
		'Gprh.ades.view.personnel.forms.AdresseQualificationJuridique',
		'Gprh.ades.view.personnel.forms.Equipement',
	],

	bodyPadding: 5,

	minHeight: Ext.Element.getViewportHeight() - 150,

	layout: 'card',

	viewModel: {
		type: 'personnelViewModel',
	},

	controller: 'personnelController',
	
	initComponent() {
		let dataRenseignement;
		let dataConjoint;
		let dataCorrespondant;
		const dataEnfantScolarite = this.enfantScolariteParameters;
		let conjointSexe;
		let dataAdresse;
		let dataQualification;
		let dataJuridique;
		let dataEquipement;
		
		if (Ext.isEmpty(this.parameters)) {
			dataRenseignement = null;
			dataConjoint = null;
			dataCorrespondant = null;
			dataAdresse = null;
			dataQualification = null;
			dataJuridique = null;
			dataEquipement = null;
		} else {
			dataRenseignement = this.parameters;
			dataConjoint = this.conjointParameters;
			dataCorrespondant = this.correspondantParameters;
			// eslint-disable-next-line no-nested-ternary
			conjointSexe = Ext.isEmpty(dataConjoint) ? (dataRenseignement.get('sexe') === '1' ? '2' : '1') : dataConjoint.get('genreConjoint');
			dataAdresse = this.adresseParameters;
			dataQualification = this.qualificationParameters;
			dataJuridique = this.juridiqueParameters;
			dataEquipement = this.equipementParameters;
		}
		
		this.items = [
			{
				xtype: 'renseignementForm',
				itemId: 'form0',
				sendData: dataRenseignement,
				height: Ext.Element.getViewportHeight() - 168,
				autoScroll: true,
			},
			{
				xtype: 'conjointCorrespondantForm',
				itemId: 'form1',
				sendData: dataRenseignement,
				sendDataConjoint: dataConjoint,
				sendDataCorrespondant: dataCorrespondant,
				sendConjointSexe: conjointSexe,
				height: Ext.Element.getViewportHeight() - 168,
				autoScroll: true,
			},
			{
				xtype: 'enfantScolarite',
				itemId: 'form2',
				sendData: dataRenseignement,
				sendEnfantScolariteData: dataEnfantScolarite,
				height: Ext.Element.getViewportHeight() - 168,
				autoScroll: true,
			},
			{
				xtype: 'bancaireGrid',
				itemId: 'form3',
				sendData: dataRenseignement,
				height: Ext.Element.getViewportHeight() - 168,
			},
			{
				xtype: 'form',
				itemId: 'form4',
				id: 'personnelDiversFormid',
				height: Ext.Element.getViewportHeight() - 168,
				items: [
					{
						xtype: 'personnelDivers',
						itemId: 'personnelDiversId',
						sendData: dataRenseignement,
						sendAdresseData: dataAdresse,
						sendQualificationData: dataQualification,
						sendJuridiqueData: dataJuridique,
						height: Ext.Element.getViewportHeight() - 168,
						autoScroll: true,
					},
				],
			},
			{
				xtype: 'equipement',
				itemId: 'form5',
				id: 'form5',
				sendEquipementData: dataEquipement,
				sendData: dataRenseignement,
				height: Ext.Element.getViewportHeight() - 168,
				autoScroll: true,
			},
		];

		const nomEmploye = Ext.isEmpty(dataRenseignement) ? 0 : dataRenseignement.get('nom');
		const prenomEmploye = Ext.isEmpty(dataRenseignement) ? 0 : dataRenseignement.get('prenom');

		this.dockedItems = [
			{
				xtype: 'toolbar',
				dock: 'top',
				items: [
					{
						html: `<div><span><b>${nomEmploye} ${prenomEmploye} </b></span></div>`,
						style: {
							background: 'transparent',
							border: 'none',
						},
					},
				],
			},
			{
				xtype: 'toolbar',
				dock: 'top',
				reference: 'progress',
				defaultButtonUI: `wizard-${this.colorScheme}`,
				cls: 'wizardprogressbar',
				defaults: {
					iconAlign: 'top',
					listeners: {
						click: 'navigateTo',
					},
					width: 150,
				},
				layout: {
					pack: 'left',
				},
				items: [
					{
						step: 0,
						iconCls: 'fa fa-info',
						pressed: true,
						enableToggle: true,
						text: 'Renseignements',
						bind: {
							disabled: '{disabledSaveBtn}',
						},
					},
					{
						step: 1,
						iconCls: 'fa fa-female',
						enableToggle: true,
						text: 'Conjoint et contact',
						bind: {
							disabled: '{disabledSaveBtn}',
						},
					},
					{
						step: 2,
						iconCls: 'fa fa-child',
						enableToggle: true,
						text: 'Enfants en charge',
						bind: {
							disabled: '{disabledSaveBtn}',
						},
					},
					{
						step: 3,
						iconCls: 'fa fa-credit-card',
						enableToggle: true,
						text: 'Info. bancaires',
						bind: {
							disabled: '{disabledSaveBtn}',
						},
					},
					{
						step: 4,
						iconCls: 'fa fa-plus-circle',
						enableToggle: true,
						text: 'Autres informations',
						bind: {
							disabled: '{disabledSaveBtn}',
						},
					},
					{
						step: 5,
						iconCls: 'fa fa-bicycle',
						enableToggle: true,
						text: 'Equipements',
						bind: {
							disabled: '{disabledSaveBtn}',
						},
					},
					{
						text: 'Liste',
						itemId: 'btnListePersonnelInPersonnel',
						ui: 'soft-purple',
						formBind: false,
						iconCls: 'x-fa fa-angle-left',
						listeners: {
							click: 'onBackListPersonnel',
						},
					},
				],
			},
			{
				xtype: 'toolbar',
				dock: 'bottom',
				items: [
					{
						xtype: 'textfield',
						itemId: 'generalPersonnelId',
						id: 'generalPersonnelid',
						hidden: true,
						value: Ext.isEmpty(dataRenseignement) ? 0 : dataRenseignement.get('idEmploye'), // Cette valeur est utilisée par le Tab Panel Bancaire.
					},
				],
			},
		];
		
		this.callParent();
	},
});
