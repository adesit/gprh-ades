Ext.define('Gprh.ades.view.personnel.PersonnelMainContainer', {
	extend: 'Ext.container.Container',

	xtype: 'personnelMainContainer',

	requires: [
		'Gprh.ades.view.personnel.PersonnelController',
		'Gprh.ades.view.personnel.PersonnelGrid',
	],

	controller: 'personnelController',

	itemId: 'personnelMainContainerId',	

	viewModel: {
		type: 'personnelViewModel',
	},

	layout: {
		type: 'hbox',
		align: 'stretch',
	},

	items: [
		{
			xtype: 'container',
			itemId: 'contentPanelPersonnel',
			flex: 1,
			layout: {
				type: 'anchor',
				anchor: '100%',
			},
			items: [
				{
					xtype: 'personnelGrid',
				},
			],
		},
	],
});
