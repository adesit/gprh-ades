Ext.define('Gprh.ades.view.personnel.PersonnelViewModel', {
	extend: 'Ext.app.ViewModel',

	alias: 'viewmodel.personnelViewModel',

	requires: [
		'Gprh.ades.store.Matrimoniale',
		'Gprh.ades.store.Sexe',
		'Gprh.ades.store.Foko',
		'Gprh.ades.store.GroupeSanguin',
		'Gprh.ades.store.Classe',
		'Gprh.ades.store.Province',
		'Gprh.ades.store.Region',
		'Gprh.ades.store.District',
		'Gprh.ades.store.Commune',
		'Gprh.ades.store.Fokotany',
		'Gprh.ades.store.Centre',
		'Gprh.ades.store.Poste',
		'Gprh.ades.store.Section',
		'Gprh.ades.store.Departement',
		'Gprh.ades.store.Categorie',
		'Gprh.ades.store.Smie',
		'Gprh.ades.store.TypeContrat',
		'Gprh.ades.store.NatureContrat',
		'Gprh.ades.store.ModePaiement',
		'Gprh.ades.store.TypeEquipement',
		'Gprh.ades.store.EtatMateriel',
	],

	stores: {
		matrimonialeResults: {
			type: 'matrimonialeStore',
		},
		sexeResults: {
			type: 'sexeStore',
		},
		fokoResults: {
			type: 'fokoStore',
		},
		groupeSanguinResults: {
			type: 'groupeSanguinStore',
		},
		classeResults: {
			type: 'classeStore',
		},
		provinceResults: {
			type: 'provinceStore',
		},
		regionResults: {
			type: 'regionStore',
		},
		districtResults: {
			type: 'districtStore',
		},
		communeResults: {
			type: 'communeStore',
		},
		fokotanyResults: {
			type: 'fokotanyStore',
		},
		centreResults: {
			type: 'centreStore',
		},
		posteResults: {
			type: 'posteStore',
			proxy: {
				extraParams: {
					infoRequest: 2,
				},
			},
		},
		sectionResults: {
			type: 'sectionStore',
		},
		departementResults: {
			type: 'departementStore',
		},
		categorieResults: {
			type: 'categorieStore',
		},
		smieResults: {
			type: 'smieStore',
		},
		typeContratResults: {
			type: 'typeContratStore',
		},
		natureContratResults: {
			type: 'natureContratStore',
		},
		modePaiementResults: {
			type: 'modePaiementStore',
		},
		typeEquipementResults: {
			type: 'typeEquipementStore',
		},
		etatMaterielResults: {
			type: 'etatMaterielStore',
		},
	},
	
	formulas: {
		disabledSaveBtn: {
			bind: {
				x: '{!idEmployeIdRef.value}',
			},

			get: data => data.x,
		},
	},
});
