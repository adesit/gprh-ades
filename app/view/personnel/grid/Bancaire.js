Ext.define('Gprh.ades.view.personnel.grid.Bancaire', {
	extend: 'Ext.grid.Panel',
	xtype: 'bancaireGrid',
	itemId: 'bancaireGridId',
	id: 'bancaireGridid',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.store.Bancaire',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
		'Gprh.ades.model.BancaireModel',
	],

	controller: 'personnelController',

	// grid default height
	gridHeight: Ext.Element.getViewportHeight() - 170,

	initComponent() {
		const Store = Ext.create('Gprh.ades.store.Bancaire');
		Ext.apply(Store.getProxy(), {
			extraParams: {
				infoRequest: 2,
				idEmploye: Ext.isEmpty(this.sendData) ? 0 : this.sendData.get('idEmploye'),
			},
		});

		this.store = Store;
		const defaultPersonnelId = this.up().dockedItems[2].items[0].value;
		const rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
			clicksToMoveEditor: 1,
			autoCancel: false,
			saveBtnText: 'Enregistrer',
			cancelBtnText: 'Annuler',
			showToolTip: false,
			errorSummary: false,
			listeners: {
				validateedit: (editor, context) => {
					const model = {
						infoRequest: 2,
						data: context.newValues,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/Bancaire.php', model);
					if (saveOrUpdate.request.responseText > 0) {
						context.grid.getStore().reload();
					}
				},
				canceledit: (editor, context) => {
					context.grid.getStore().reload();
					context.grid.getView().refresh();
				},
			},
		});
		// Gprh.ades.util.Globals.setRowEditingConfig('server-scripts/personnel/Bancaire.php', this);

		this.selModel = 'rowmodel';
		this.minHeight = this.gridHeight;
		this.columnLines = true;
		this.stripeRows = true;
		this.columns = [
			{
				xtype: 'gridcolumn',
				width: 40,
				dataIndex: 'idBancaire',
				text: '#',
			},
			{
				xtype: 'gridcolumn',
				width: 40,
				dataIndex: 'idEmploye',
				text: 'idEmploye',
				hidden: true,
			},
			{
				xtype: 'gridcolumn',
				cls: 'content-column',
				dataIndex: 'nomBanque',
				text: 'Nom de la banque',
				align: 'center',
				flex: 1,
				editor: {
					allowBlank: false,
				},
			},
			{
				xtype: 'gridcolumn',
				cls: 'content-column',
				dataIndex: 'codeBanque',
				text: 'Code banque',
				align: 'center',
				flex: 1,
				editor: {
					allowBlank: false,
				},
			},
			{
				xtype: 'gridcolumn',
				cls: 'content-column',
				dataIndex: 'codeGuichet',
				text: 'Code guichet',
				align: 'center',
				flex: 1,
				editor: {
					allowBlank: false,
				},
			},
			{
				xtype: 'gridcolumn',
				cls: 'content-column',
				dataIndex: 'cleRib',
				text: 'Clé rib',
				align: 'center',
				flex: 1,
				editor: {
					allowBlank: false,
				},
			},
			{
				xtype: 'gridcolumn',
				cls: 'content-column',
				dataIndex: 'numeroCompte',
				text: 'Numéro de compte',
				align: 'center',
				flex: 1,
				editor: {
					allowBlank: false,
				},
			},
			{
				xtype: 'datecolumn',
				header: 'Date de modification',
				dataIndex: 'datePriseEffetBanque',
				width: 135,
				format: 'd/m/Y',
				editor: {
					xtype: 'datefield',
					allowBlank: false,
					format: 'd/m/Y',
					minText: 'Date de changement des informations bancaires!',
					submitFormat: 'Y-m-d',		
				},
			},
			{
				xtype: 'checkcolumn',
				header: 'Actif',
				dataIndex: 'actifBancaire',
				width: 60,
				editor: {
					xtype: 'checkboxfield',
					cls: 'x-grid-checkheader-editor',
				},
				listeners: {
					checkchange: 'confirmActifBancaire',
				},
			},
		];
		this.dockedItems = [
			{
				xtype: 'pagingtoolbar',
				dock: 'bottom',
				itemId: 'cnapsPaginationToolbar',
				displayInfo: true,
				store: Store,
			},
		];
		this.tbar = [
			'->', 
			{
				text: 'Nouvelle ligne',
				iconCls: 'fa fa-plus',
				margin: '0 0 10 0',
				handler: () => {
					rowEditing.cancelEdit();
										
					const r = Ext.create('Gprh.ades.model.BancaireModel', {
						idBancaire: '',
						idEmploye: Ext.isEmpty(this.sendData) ? defaultPersonnelId : this.sendData.get('idEmploye'),
						nomBanque: '',
						numeroCompte: '',
						codeGuichet: '',
						cleRib: '',
						datePriseEffetBanque: Ext.Date.clearTime(new Date()),
						actifBancaire: false,
					});
					
					this.getView().getStore().insert(0, r);
					rowEditing.startEdit(0, 0);
				},
			}];
		this.plugins = [rowEditing];

		this.callParent();
	},
});
