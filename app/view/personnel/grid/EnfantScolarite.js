Ext.define('Gprh.ades.view.personnel.grid.EnfantScolarite', {
	extend: 'Ext.grid.Panel',
	xtype: 'enfantScolariteGrid',

	requires: [
		'Gprh.ades.util.Globals',
	],

	statics: {
		groupTitle: (nom, prenom, dobEnfant, sexeEnfant) => {
			const formatDob = Ext.Date.format(new Date(dobEnfant), 'd/m/Y');
			const age = Ext.Date.diff(new Date(dobEnfant), new Date(), Ext.Date.YEAR);
			const alertAge = (age >= 25) ? `<span style="color:red;">(${age}ans)</span>` : `(${age}ans)`;
			const genre = (sexeEnfant === '1' ? '<span class="x-fa fa-male"></span>' : '<span class="x-fa fa-female"></span>');
			const ret = `<b>${genre} - ${nom} ${prenom} - ${formatDob} ${alertAge}</b>`;		
			return ret;
		},
	},

	initComponent() {
		this.store = this.storeParam;
		
		if (this.store) {
			this.store.setGroupField('idEnfant');
			this.store.setGroupDir('ASC');
			this.features = [{
				ftype: 'grouping',
				// groupHeaderTpl: '{name}',
				groupHeaderTpl: '{[Gprh.ades.view.personnel.grid.EnfantScolarite.groupTitle(values.children[0].data["nomEnfant"], values.children[0].data["prenomEnfant"], values.children[0].data["dateNaissanceEnfant"], values.children[0].data["genreEnfant"])]}',
				enableNoGroups: true,
			}];	
		}

		this.selModel = 'rowmodel';
		this.minHeight = 400;
		this.columnLines = true;
		this.stripeRows = true;
		this.columns = [
			{
				dataIndex: 'idEnfant',
				text: '#',
				align: 'left',
				autoSizeColumn: true,
				hidden: true,
			},
			{
				dataIndex: 'nomEnfant',
				text: 'Nom',
				align: 'left',
				autoSizeColumn: true,
				hidden: true,
			},
			{
				dataIndex: 'prenomEnfant',
				text: 'Prénom',
				align: 'left',
				autoSizeColumn: true,
				hidden: true,
			},
			{
				xtype: 'datecolumn',
				dataIndex: 'dateNaissanceEnfant',
				text: 'Né(e) le',
				align: 'left',
				format: 'd/m/Y',
				width: 100,
				hidden: true,
			},
			{
				dataIndex: 'genreEnfant',
				text: 'Sexe',
				align: 'left',
				minWidth: 120,
				hidden: true,
			},
			{
				dataIndex: 'classe',
				text: 'Niveau',
				align: 'left',
				minWidth: 120,
				flex: 1,
			},
			{
				dataIndex: 'nomEcole',
				text: 'Nom école',
				align: 'left',
				minWidth: 120,
				flex: 1,
			},
			{
				dataIndex: 'anneeScolaire',
				text: 'Année scolarité',
				align: 'left',
				minWidth: 120,
			},
			{
				dataIndex: 'fraisScolarite',
				text: 'Frais de scolarité (Ariary)',
				align: 'left',
				minWidth: 200,
				renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
			},
			{
				xtype: 'actioncolumn',
				width: 120,
				text: 'Supprimer',
				align: 'left',
				flex: 1,
				renderer: (value, meta, record, row, column, store) => this.initRendererDelEnfant(value, meta, record, row, column, store),
				listeners: {
					click: 'actionRowEnfantScolarite',
				},
			},
		];
		this.dockedItems = [
			{
				xtype: 'pagingtoolbar',
				dock: 'bottom',
				itemId: 'enfantScolaritePaginationToolbar',
				displayInfo: true,
				store: this.storeParam,
			},
		];
		this.tbar = [
			'->',
			{
				text: 'Liste des enfants et leur scolarité',
				style: {
					background: 'transparent',
					border: 'none',
				},
			},
		];
		
		this.listeners = {
			rowclick: 'onEnfantScolariteClick',
		};

		this.callParent();
	},

	initRendererDelEnfant(value, meta, record, row, column, store) {
		const items = [];

		const enfantId = store.getAt(row).data.idEnfant;
		const butonDelEnfant = `<button id="delEnfant_${enfantId}" 
		type="button" 
		style="margin-right: 2px;">
		<span class="fa fa-trash"></span>
		Enfant
		</button>`;

		const ScolariteId = store.getAt(row).data.scolariteId;
		const butonDelScolarite = `<button id="delScolarite_${ScolariteId}"  
		type="button" 
		style="margin-right: 2px;" >
		<span class="fa fa-trash"></span>
		Scolarité
		</button>`;

		const butonAddScolarite = `<button id="addScolarite_${enfantId}"  
		type="button" 
		style="margin-right: 2px;" >
		<span class="fa fa-plus"></span>
		Scolarité
		</button>`;

		items.push(butonDelEnfant);
		items.push(butonDelScolarite);
		items.push(butonAddScolarite);

		return items;
	},
});
