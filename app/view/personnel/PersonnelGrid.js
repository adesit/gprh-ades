Ext.define('Gprh.ades.view.personnel.PersonnelGrid', {
	extend: 'Ext.panel.Panel',
	xtype: 'personnelGrid',

	requires: [
		'Gprh.ades.store.Personnel',
	],

	initComponent() {
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		const Store = Ext.create('Gprh.ades.store.Personnel', {
			localFilter: true,
			listeners: {
				load: (store) => {
					const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
					Ext.apply(store, {
						pageSize: store.count(),
					});
					toolbar.onLoad();
				},
				filterchange: (store) => {
					const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
					Ext.apply(store, {
						pageSize: store.count(),
					});
					store.reload();
					toolbar.onLoad();
				},
			},
		});
		Ext.apply(Store.getProxy(), extraParameters);

		this.items = [{
			xtype: 'gridpanel',
			itemId: 'personnelGridId',
			reference: 'personnelGridRef',
			store: Store,
			viewConfig: {
				preserveScrollOnRefresh: true,
				stripeRows: true,
			},
			scrollable: true,
			selModel: 'rowmodel',
			listeners: {
				rowdblclick: 'onRowPersonnelDblClick',
				itemmouseenter: (view, record, item) => {
					Ext.fly(item).set({ 'data-qtip': 'Double clic dessus pour voir les détails.' });
				},
			},
			height: Ext.Element.getViewportHeight() - 75,
			columnLines: true,
			plugins: 'gridfilters',
			columns: {
				defaults: {
					align: 'left',
				},
				items: [
					{
						xtype: 'gridcolumn',
						dataIndex: 'idEmploye',
						text: '#',
						hidden: true,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'sexe',
						maxWidth: 40,
						renderer: value => (value === '1' ? '<span class="x-fa fa-male"></span>' : '<span class="x-fa fa-female"></span>'),
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'nom',
						text: 'Nom',
						flex: 1,
						filter: {
							type: 'string',
						},
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'prenom',
						text: 'Prénoms',
						flex: 1,
						filter: {
							type: 'string',
						},
					},					
					{
						xtype: 'gridcolumn',
						dataIndex: 'dateNaissance',
						text: 'Né(e) le',
						renderer: value => this.rendererAge(value),
						flex: 1,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'numeroCinPasseport',
						text: 'CIN/Passeport',
						flex: 1,
					},
					{
						xtype: 'datecolumn',
						dataIndex: 'dateDelivranceCin',
						text: 'Délivré(e) le',
						// Params can be value, metaData, record, rowIndex
						renderer: (value, metaData, record) => this.rendererCIN(record),
						flex: 1,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'numCnaps',
						text: 'N° CNaps',
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'nbEnfant',
						text: 'Nombre d\'enfant',
					},
				],				
			},
			dockedItems: [
				Gprh.ades.util.Globals.getPagingToolbar(this),
			],
			tbar: [
				{
					tooltip: 'Créer un nouvel employé',
					itemId: 'addPersonnelBtnId',
					iconCls: 'fa fa-user-plus',
					text: 'Créer',
					margin: '0 0 10 10',
					listeners: {
						click: 'onAddPersonnelClick',
					},
					hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
				},
				{
					text: 'Contrat',
					tooltip: 'Gérer les contrats de l\'employé',
					itemId: 'editPersonnelContratBtnId',
					iconCls: 'x-fas fa-file-contract',
					ui: 'soft-green',
					margin: '0 0 10 10',
					listeners: {
						click: 'onEditContratClick',
					},
					params: {
						gridId: 'personnelGridId',
						editOrCreate: 'edit',
					},
					bind: { 
						disabled: '{!personnelGridRef.selection}',
					},
					hidden: !this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1', '2']),
				},
				'->',
				{
					text: 'Supprimer',
					iconCls: 'fa fa-trash',
					ui: 'soft-red-small',
					listeners: {
						click: 'onDeletePersonnel',
					},
					params: {
						gridId: 'personnelGridId',
					},
					bind: { 
						disabled: '{!personnelGridRef.selection}',
					},
					hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
				},
			],
		}];

		this.callParent();
	},

	rendererAge(value) {
		const dob = Ext.Date.format(new Date(value), 'd/m/Y');
		const age = Ext.Date.diff(new Date(value), new Date(), Ext.Date.YEAR);
		const alertAge = (age >= 58) ? 'style="color:#f2a4ac9f;"' : ''; // Alerter 1an avant la retraite (à 60ans)
		const alertHBD = (Ext.Date.format(new Date(value), 'd/m') === Ext.Date.format(new Date(), 'd/m'))
			? '<span style= "width: 5px; margin-left: 2px; color: #20b2aba9" class="x-fa fa-birthday-cake"></span>'
			: '';

		const val = `<span ${alertAge}>${dob} (${age}ans)</span>${alertHBD}`;
		return val;
	},

	rendererCIN(record) {
		const dateDelivranceCin = Ext.Date.format(new Date(record.data.dateDelivranceCin), 'd/m/Y');
		return `${dateDelivranceCin} à ${record.data.lieuDelivranceCinPasseport}`;
	},

	privileges(typeUtilisateur, valeursPossibles) {
		const found = valeursPossibles.indexOf(typeUtilisateur);
		
		return found !== -1;
	},
});
