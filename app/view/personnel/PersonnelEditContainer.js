Ext.define('Gprh.ades.view.personnel.PersonnelEditContainer', {
	extend: 'Ext.container.Container',

	xtype: 'personnelEditContainer',

	requires: [
	],
	
	cls: 'wizards',
	defaultFocus: 'wizardform',
	
	
	initComponent() {
		this.itemId = 'personnelEditContainerId';
		this.items = [
			{
				xtype: 'wizardform',
				itemId: 'personnelWizardFormId',
				cls: 'wizardthree shadow',
				colorScheme: 'soft-green',
				flex: 1,
				parameters: this.params,
				conjointParameters: this.conjointParams,
				correspondantParameters: this.correspondantParams,
				enfantScolariteParameters: this.enfantScolariteParams,
				adresseParameters: this.adresseParams,
				qualificationParameters: this.qualificationParams,
				juridiqueParameters: this.juridiqueParams,
				equipementParameters: this.equipementParams,
			},
		];
		this.callParent();
	},
});
