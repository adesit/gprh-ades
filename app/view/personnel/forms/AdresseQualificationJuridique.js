Ext.define('Gprh.ades.view.personnel.forms.AdresseQualificationJuridique', {
	extend: 'Ext.tab.Panel',
	xtype: 'personnelDivers',

	requires: [
		'Ext.grid.Panel',
		'Ext.toolbar.Paging',
		'Ext.grid.column.Date',
		'Gprh.ades.util.Globals',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	controller: 'personnelController',

	activeTab: 0,
	bodyPadding: 10,

	initComponent() {
		const adresseStore = Ext.isEmpty(this.sendAdresseData) ? null : this.sendAdresseData.store;
		const adresseContainer = {
			title: 'Adresses',
			xtype: 'panel',
			itemId: 'adressePanelId',
			layout: {
				type: 'vbox',
				align: 'stretch',
			},
			items: [
				{
					xtype: 'form',
					itemId: 'adresseFormId',
					defaultType: 'textfield',
					defaults: {
						minWidth: 500,
						labelWidth: 150,
						labelAlign: 'left',
						labelSeparator: ':',
						submitEmptyText: false,
					},
					items: [
						{
							xtype: 'container',
							layout: 'column',
							items: [
								{
									defaultType: 'textfield',
									defaults: {
										minWidth: 500,
										labelWidth: 150,
										labelAlign: 'left',
										labelSeparator: ':',
										submitEmptyText: false,
									},
									columnWidth: '0.50',
									items: [
										{
											name: 'idEmploye',
											itemId: 'idEmployeId',
											fieldLabel: '# Employé',
											hidden: true,
											value: Ext.isEmpty(this.sendData) ? 0 : this.sendData.get('idEmploye'),
										},
										{
											name: 'idAdresse',
											itemId: 'idAdresseId',
											fieldLabel: '# Adresse',
											hidden: true,
										},
										{
											xtype: 'combobox',
											name: 'idProvince',
											itemId: 'idProvinceId',
											fieldLabel: 'Province',
											hidden: false,
											bind: {
												store: '{provinceResults}',
											},
											queryMode: 'local',
											displayField: 'nomProvince',
											valueField: 'idProvince',
											renderTo: Ext.getBody(),
											editable: false,
											minWidth: 100,
											margin: '0 0 10 0',
											listeners: {
												change: 'changeRegionStore',
											},
										},
										{
											xtype: 'combobox',
											name: 'idRegion',
											itemId: 'idRegionId',
											fieldLabel: 'Région',
											hidden: false,
											bind: {
												store: '{regionResults}',
											},
											queryMode: 'local',
											displayField: 'nomRegion',
											valueField: 'idRegion',
											renderTo: Ext.getBody(),
											editable: false,
											margin: '0 0 10 0',
											listeners: {
												change: 'changeDistrictStore',
											},
										},
										{
											xtype: 'combobox',
											name: 'idDistrict',
											itemId: 'idDistrictId',
											fieldLabel: 'District',
											hidden: false,
											bind: {
												store: '{districtResults}',
											},
											queryMode: 'local',
											displayField: 'nomDistrict',
											valueField: 'idDistrict',
											renderTo: Ext.getBody(),
											editable: false,
											margin: '0 0 10 0',
											listeners: {
												change: 'changeCommuneStore',
											},
										},
									],
								}, {
									defaultType: 'textfield',
									defaults: {
										minWidth: 500,
										labelWidth: 150,
										labelAlign: 'left',
										labelSeparator: ':',
										submitEmptyText: false,
										anchor: '100%',
									}, 
									columnWidth: '0.50',
									items: [
										{
											xtype: 'combobox',
											name: 'idCommune',
											itemId: 'idCommuneId',
											fieldLabel: 'Commune',
											hidden: false,
											bind: {
												store: '{communeResults}',
											},
											queryMode: 'local',
											displayField: 'nomCommune',
											valueField: 'idCommune',
											renderTo: Ext.getBody(),
											editable: false,
											margin: '0 0 10 0',
											listeners: {
												change: 'changeFokotanyStore',
											},
										},
										{
											xtype: 'combobox',
											name: 'idFokotany',
											itemId: 'idFokotanyId',
											fieldLabel: 'Fokotany',
											hidden: false,
											bind: {
												store: '{fokotanyResults}',
											},
											queryMode: 'local',
											displayField: 'nomFokotany',
											valueField: 'idFokotany',
											renderTo: Ext.getBody(),
											editable: false,
											margin: '0 0 10 0',
										},
										{
											name: 'logementEmploye',
											itemId: 'logementEmployeId',
											fieldLabel: 'Adresse exacte',
											hidden: false,
										},
										{
											xtype: 'checkboxfield',
											fieldLabel: 'Adresse actuelle',
											name: 'actifAdresse',
											itemId: 'actifAdresseId',
											hidden: false,
										},
									],
								},
							],
						},
						{
							xtype: 'toolbar',
							layout: 'hbox',
							items: [
								'->',
								{
									itemId: 'adresseSaveBtn',
									xtype: 'button',
									text: 'Enregistrer',
									ui: 'soft-green-small',
									margin: '10 10 0 0',
									formBind: true,
									listeners: {
										click: 'onAdresseSaveClick',
									},
								},
								{
									itemId: 'adresseCancelBtn',
									xtype: 'button',
									text: 'Annuler',
									ui: 'soft-blue-small',
									margin: '10 0 0 0',
									listeners: {
										click: 'cancelAction',
									},
									params: 'adresseFormId', // Send formId to the reset method 
								},
							],
						},
					],
				},
				{
					xtype: 'gridpanel',
					title: 'Liste des adresses',
					itemId: 'adresseGridId',
					id: 'adresseGridid',
					scrollable: true,
					columnLines: true,
					selModel: 'rowmodel',
					store: adresseStore,
					height: 225,
					cls: 'user-grid',
					maxWidth: Ext.Element.getViewportWidth() - 300,
					dockedItems: [
						Gprh.ades.util.Globals.getPagingToolbar(this),
					],
					columns: [
						{
							xtype: 'actioncolumn',
							cls: 'content-column',
							width: 80,
							text: 'Actions',
							items: [
								{
									iconCls: 'x-fa fa-trash soft-red-small',
									handler: 'confirmDeleteAdresse',
								},
							],					
						},
						{
							text: 'Adresse actuelle',
							dataIndex: 'actifAdresse',
							renderer: value => (value ? 'Oui' : ''),
							width: 125,
						},
						{
							text: 'Adresse exacte',
							dataIndex: 'logementEmploye',
							width: 200,
						},
						{
							text: 'Fokotany',
							dataIndex: 'nomFokotany',
							width: 200,
						},
						{
							text: 'Commune urbaine/rurale',
							dataIndex: 'nomCommune',
							width: 200,
						},
						{
							text: 'District',
							dataIndex: 'nomDistrict',
							width: 150,
						},
						{
							text: 'Région',
							dataIndex: 'nomRegion',
							width: 150,
						},
						{
							text: 'Province',
							dataIndex: 'nomProvince',
							width: 100,
						},
					],
					listeners: {
						rowclick: 'onRowAdresseClick',
					},
				},
			],
		};

		const qualificationContainer = {
			title: 'Qualifications',
			xtype: 'form',
			itemId: 'qualificationFormId',
			defaultType: 'textfield',
			defaults: {
				minWidth: 500,
				labelWidth: 200,
				labelAlign: 'left',
				labelSeparator: ':',
				submitEmptyText: false,
			},
			items: [
				{
					name: 'idEmploye',
					itemId: 'idEmployeId',
					fieldLabel: '# Employé',
					hidden: true,
					value: Ext.isEmpty(this.sendData) ? 0 : this.sendData.get('idEmploye'),
				},
				{
					name: 'idQualification',
					itemId: 'idQualificationId',
					fieldLabel: '# Qualification',
					hidden: true,
					value: Ext.isEmpty(this.sendQualificationData) ? '' : this.sendQualificationData.get('idQualification'),
				},				
				{
					xtype: 'combobox',
					name: 'niveauFormation',
					itemId: 'niveauFormationId',
					fieldLabel: 'Niveau de formation',
					hidden: false,
					bind: {
						store: '{classeResults}',
					},
					queryMode: 'local',
					displayField: 'classe',
					valueField: 'idEducation',
					renderTo: Ext.getBody(),
					editable: false,
					minWidth: 400,
					margin: '0 0 10 0',
					value: Ext.isEmpty(this.sendQualificationData) ? '' : this.sendQualificationData.get('niveauFormation'),
				},
				{
					name: 'intituleDiplome',
					itemId: 'intituleDiplomeId',
					fieldLabel: 'Intitulé du diplôme le plus élevé',
					hidden: false,
					value: Ext.isEmpty(this.sendQualificationData) ? '' : this.sendQualificationData.get('intituleDiplome'),
				},
				{
					name: 'ecoleUniversite',
					itemId: 'ecoleUniversiteId',
					fieldLabel: 'Ecole ou université fréquentée',
					hidden: false,
					value: Ext.isEmpty(this.sendQualificationData) ? '' : this.sendQualificationData.get('ecoleUniversite'),
				},
				{
					name: 'anneeExperience',
					itemId: 'anneeExperienceId',
					fieldLabel: 'Nombre d\'années d\'expérience',
					hidden: false,
					value: Ext.isEmpty(this.sendQualificationData) ? '' : this.sendQualificationData.get('anneeExperience'),
				},
				{
					xtype: 'toolbar',
					layout: 'hbox',
					items: [
						'->',
						{
							itemId: 'qualificationSaveBtn',
							xtype: 'button',
							text: 'Enregistrer',
							ui: 'soft-green-small',
							margin: '10 10 0 0',
							formBind: true,
							listeners: {
								click: 'onQualificationSaveClick',
							},
						},
						{
							itemId: 'qualificationCancelBtn',
							xtype: 'button',
							text: 'Annuler',
							ui: 'soft-blue-small',
							margin: '10 0 0 0',
							listeners: {
								click: 'cancelAction',
							},
							params: 'qualificationFormId', // Send formId to the reset method 
						},
					],
				},
			],
		};


		const juridiqueContainer = {
			title: 'Antécédents juridiciaires',
			xtype: 'form',
			itemId: 'juridiqueFormId',
			defaultType: 'textfield',
			defaults: {
				minWidth: 500,
				labelWidth: 200,
				labelAlign: 'left',
				labelSeparator: ':',
				submitEmptyText: false,
			},
			items: [
				{
					name: 'idEmploye',
					itemId: 'idEmployeId',
					fieldLabel: '# Employé',
					hidden: true,
					value: Ext.isEmpty(this.sendData) ? 0 : this.sendData.get('idEmploye'),
				},
				{
					name: 'idJuridique',
					itemId: 'idJuridiqueId',
					fieldLabel: '# Juridique',
					hidden: true,
					value: Ext.isEmpty(this.sendJuridiqueData) ? '' : this.sendJuridiqueData.get('idJuridique'),
				},
				{
					name: 'natureCondamnation',
					itemId: 'natureCondamnationId',
					fieldLabel: 'Nature de condamnation',
					hidden: false,
					value: Ext.isEmpty(this.sendJuridiqueData) ? '' : this.sendJuridiqueData.get('natureCondamnation'),
				},
				{
					xtype: 'datefield',
					name: 'dateCondamnation',
					itemId: 'dateCondamnationId',
					fieldLabel: 'Date de condamnation',
					hidden: false,
					format: 'd/m/Y',
					submitFormat: 'Y-m-d',
					minWidth: 100,
					value: Ext.isEmpty(this.sendJuridiqueData) ? Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -18), 'd/m/Y') : this.sendJuridiqueData.get('dateCondamnation'),
				},
				{
					name: 'dureeCondamnation',
					itemId: 'dureeCondamnationId',
					fieldLabel: 'Durée de la peine',
					hidden: false,
					value: Ext.isEmpty(this.sendJuridiqueData) ? '' : this.sendJuridiqueData.get('dureeCondamnation'),
				},
				{
					xtype: 'toolbar',
					layout: 'hbox',
					items: [
						'->',
						{
							itemId: 'juridiqueSaveBtn',
							xtype: 'button',
							text: 'Enregistrer',
							ui: 'soft-green-small',
							margin: '10 10 0 0',
							formBind: true,
							listeners: {
								click: 'onJuridiqueSaveClick',
							},
						},
						{
							itemId: 'juridiqueCancelBtn',
							xtype: 'button',
							text: 'Annuler',
							ui: 'soft-blue-small',
							margin: '10 0 0 0',
							listeners: {
								click: 'cancelAction',
							},
							params: 'juridiqueFormId', // Send formId to the reset method 
						},
					],
				},
			],
		};

		this.items = [
			adresseContainer,
			qualificationContainer,
			juridiqueContainer,
		];
		this.layout = 'fit';

		this.callParent();
	},
});
