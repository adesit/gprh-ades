Ext.define('Gprh.ades.view.personnel.forms.EnfantScolarite', {
	extend: 'Ext.form.Panel',
	xtype: 'enfantScolarite',
	
	requires: [		
		'Gprh.ades.view.personnel.grid.EnfantScolarite',
		'Gprh.ades.util.VTypes',
	],
	
	initComponent() {
		this.items = [
			{
				xtype: 'container',
				layout: 'column',
				items: [
					{
						defaultType: 'textfield',
						defaults: {
							minWidth: 500,
							labelWidth: 150,
							labelAlign: 'left',
							labelSeparator: ':',
							submitEmptyText: false,
							listeners: {
								focus: 'disableAddEnfantBtn',
							},
						},
						columnWidth: '0.50',
						xtype: 'fieldset',
						itemId: 'enfantFieldSetId',
						title: 'Renseignements d\'un(e) enfant',
						style: {
							background: 'transparent',
						},
						items: [
							{
								name: 'idEmploye',
								itemId: 'idEmployeId',
								fieldLabel: '# Employé',
								hidden: true,
								value: Ext.isEmpty(this.sendData) ? 0 : this.sendData.get('idEmploye'),
							},
							{
								name: 'nomEnfant',
								itemId: 'nomEnfantId',
								fieldLabel: 'Nom',
								hidden: false,
								allowBlank: false,
							},
							{
								name: 'prenomEnfant',
								itemId: 'prenomEnfantId',
								fieldLabel: 'Prénoms',
								hidden: false,
							},
							{
								xtype: 'datefield',
								name: 'dateNaissanceEnfant',
								itemId: 'dateNaissanceEnfantId',
								fieldLabel: 'Date de naissance',
								hidden: false,
								format: 'd/m/Y',
								submitFormat: 'Y-m-d',
								minWidth: 100,
								value: new Date(),
							},
							{
								name: 'lieuNaissanceEnfant',
								itemId: 'lieuNaissanceEnfantId',
								fieldLabel: 'Lieu de naissance',
								hidden: false,
							},
							{
								xtype: 'combobox',
								name: 'genreEnfant',
								itemId: 'genreEnfantId',
								fieldLabel: 'Sexe',
								hidden: false,
								bind: {
									store: '{sexeResults}',
								},
								queryMode: 'local',
								displayField: 'displayText',
								valueField: 'sexe',
								renderTo: Ext.getBody(),
								editable: false,
								minWidth: 100,
								margin: '0 0 10 0',
								value: 1,
							},
						],
					}, {
						defaultType: 'textfield',
						defaults: {
							minWidth: 500,
							labelWidth: 150,
							labelAlign: 'left',
							labelSeparator: ':',
							submitEmptyText: false,
						}, 
						columnWidth: '0.50',
						xtype: 'fieldset',
						itemId: 'scolariteFieldSetId',
						title: 'Scolarité d\'un(e) enfant',
						style: {
							background: 'transparent',
						},
						items: [
							{
								name: 'idEnfant',
								itemId: 'idEnfantId',
								fieldLabel: '# Enfant',
								hidden: true,
							},
							{
								name: 'idScolarite',
								itemId: 'idScolariteId',
								fieldLabel: '# Scolarité',
								hidden: true,
							},
							{
								xtype: 'combobox',
								name: 'idEducation',
								itemId: 'idEducationId',
								fieldLabel: 'Niveau',
								hidden: false,
								bind: {
									store: '{classeResults}',
								},
								queryMode: 'local',
								displayField: 'classe',
								valueField: 'idEducation',
								renderTo: Ext.getBody(),
								editable: false,
								minWidth: 400,
								margin: '0 0 10 0',
								value: 1,
							},
							{
								name: 'nomEcole',
								itemId: 'nomEcoleId',
								fieldLabel: 'Nom de l\'école',
								hidden: false,
							},
							{
								name: 'adresseEcole',
								itemId: 'adresseEcoleId',
								fieldLabel: 'Adresse de l\'école',
								hidden: false,
							},
							{
								name: 'anneeScolaire',
								itemId: 'anneeScolaireId',
								fieldLabel: 'Année scolaire',
								hidden: false,
								vtype: 'schoolYear',
								msgTarget: 'side',
							},
							{
								name: 'fraisScolarite',
								itemId: 'fraisScolariteId',
								fieldLabel: 'Frais de scolarité',
								hidden: false,
								listeners: {
									blur: self => Gprh.ades.util.Globals.formatNumber(self),
								},
							},							
						],
					},
				],
			},
			{
				xtype: 'toolbar',
				layout: 'hbox',
				items: [
					{
						xtype: 'component',
						html: '* <b>Cliquez sur une ligne dans le tableau pour voir les détails.</b></br>* Frais de scolarité en <b>Ariary</b>',
					},
					'->',
					{
						itemId: 'enfantSaveBtn',
						xtype: 'button',
						text: 'Enregistrer',
						ui: 'soft-green-small',
						margin: '10 10 0 0',
						formBind: true,
						listeners: {
							click: 'onEnfantSaveClick',
						},
					},
					{
						itemId: 'enfantCancelBtn',
						xtype: 'button',
						text: 'Annuler',
						ui: 'soft-blue-small',
						margin: '10 10 0 0',
						listeners: {
							click: 'cancelAction',
						},
						params: 'form2', // Envoyer le formId à la méthode de réinitialisation
					},
					{
						itemId: 'enfantAddBtn',
						xtype: 'button',
						text: 'Ajouter un(e) autre enfant',
						ui: 'soft-green-small',
						iconCls: 'fa fa-plus',
						margin: '10 0 0 0',
						bind: {
							disabled: '{!storeLen}',
						},
						listeners: {
							click: 'resetAllValuesInForm2',
						},
					},
				],
			},
			{
				xtype: 'enfantScolariteGrid',
				itemId: 'enfantScolariteGridId',
				storeParam: this.sendEnfantScolariteData,
				reference: 'enfantScolariteGridRef',
			},
		];
	
		this.callParent();
	},
});
