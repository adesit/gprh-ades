Ext.define('Gprh.ades.view.personnel.forms.Equipement', {
	extend: 'Ext.form.Panel',
	xtype: 'equipement',

	requires: [
		'Gprh.ades.util.ColorField',
	],

	layout: {
		type: 'vbox',
		align: 'stretch',
	},

	initComponent() {
		const equipementStore = Ext.isEmpty(this.sendEquipementData) ? null : this.sendEquipementData.store;

		this.items = [
			{
				xtype: 'panel',
				layout: 'column',
				items: [
					{
						defaultType: 'textfield',
						defaults: {
							minWidth: 500,
							labelWidth: 150,
							labelAlign: 'left',
							labelSeparator: ':',
							submitEmptyText: false,
						},
						columnWidth: '0.50',
						items: [
							{
								name: 'idEmploye',
								itemId: 'idEmployeId',
								id: 'idEmployeid',
								fieldLabel: '# Employé',
								hidden: true,
								value: Ext.isEmpty(this.sendData) ? 0 : this.sendData.get('idEmploye'),
								publishes: 'value',
							},
							{
								name: 'idEquipement',
								itemId: 'idEquipementId',
								id: 'idEquipementid',
								fieldLabel: '# idEquipement',
								hidden: true,
							},
							{
								xtype: 'combobox',
								name: 'typeEquipement',
								itemId: 'typeEquipementId',
								id: 'typeEquipementid',
								fieldLabel: 'Type d\'équipement',
								hidden: false,
								bind: {
									store: '{typeEquipementResults}',
								},
								queryMode: 'local',
								displayField: 'displayText',
								valueField: 'typeEquipement',
								renderTo: Ext.getBody(),
								editable: false,
								margin: '0 0 10 0',
								value: 1,
								allowBlank: false,
								listeners: {
									change: 'equipementType',
								},
							},
							{
								name: 'marque',
								itemId: 'marqueId',
								id: 'marqueid',
								fieldLabel: 'Marque',
								hidden: false,
								allowBlank: false,
							},
							{
								xtype: 'htmleditor',
								fieldLabel: 'Autres caractéristiques',
								name: 'autresCaracteristiques',
								reference: 'autresCaracteristiquesRef',
								publishes: 'value',
								itemId: 'autresCaracteristiquesId',
								id: 'autresCaracteristiquesid',
								labelAlign: 'top',
								enableFont: false,
								enableColors: false,
								enableFontSize: false,
								enableSourceEdit: false,
								enableLinks: false,
								width: '100%',
								height: 300,
							},
						],
					},
					{
						defaultType: 'textfield',
						defaults: {
							minWidth: 500,
							labelWidth: 150,
							labelAlign: 'left',
							labelSeparator: ':',
							submitEmptyText: false,
						},
						columnWidth: '0.50',
						items: [
							{
								name: 'numeroSerie',
								itemId: 'numeroSerieId',
								id: 'numeroSerieid',
								fieldLabel: 'N° de série/ Num. flotte',
								hidden: false,
								allowBlank: false,
								msgTarget: 'side',
							},
							{
								xtype: 'fieldcontainer',
								items: [
									{
										xtype: 'colorPicker2',
										name: 'couleur',
										itemId: 'couleurId',
										id: 'couleurid',
										fieldLabel: 'Couleur',
										hidden: false,
										allowBlank: false,
										renderTo: Ext.getBody(),
										value: 'FFFFFF',
										maxWidth: 350,
										labelWidth: 150,
										labelAlign: 'left',
										labelSeparator: ':',
										submitEmptyText: false,
									},

								],
							},
							{
								xtype: 'datefield',
								name: 'dateReception',
								itemId: 'dateReceptionId',
								id: 'dateReceptionid',
								fieldLabel: 'Date de réception',
								hidden: false,
								format: 'd/m/Y',
								submitFormat: 'Y-m-d',
								minWidth: 100,
								value: new Date(),
								allowBlank: false,
							},
							{
								xtype: 'combobox',
								name: 'etatReception',
								itemId: 'etatReceptionId',
								id: 'etatReceptionid',
								fieldLabel: 'Etat à la réception',
								hidden: false,
								bind: {
									store: '{etatMaterielResults}',
								},
								queryMode: 'local',
								displayField: 'displayText',
								valueField: 'etatMateriel',
								renderTo: Ext.getBody(),
								editable: false,
								margin: '0 0 10 0',
								value: 1,
								minWidth: 350,
								allowBlank: false,
							},
							{
								xtype: 'datefield',
								name: 'dateRestitution',
								itemId: 'dateRestitutionId',
								id: 'dateRestitutionid',
								fieldLabel: 'Date de restitution',
								hidden: false,
								format: 'd/m/Y',
								submitFormat: 'Y-m-d',
								minWidth: 100,
							},
							{
								xtype: 'combobox',
								name: 'etatRestitution',
								itemId: 'etatRestitutionId',
								id: 'etatRestitutionid',
								fieldLabel: 'Etat à la restitution',
								hidden: false,
								bind: {
									store: '{etatMaterielResults}',
								},
								queryMode: 'local',
								displayField: 'displayText',
								valueField: 'etatMateriel',
								renderTo: Ext.getBody(),
								editable: false,
								margin: '0 0 10 0',
								minWidth: 350,
							},
							{
								xtype: 'toolbar',
								layout: 'hbox',
								items: [
									'->',
									{
										itemId: 'equipementSaveBtn',
										xtype: 'button',
										text: 'Enregistrer',
										ui: 'soft-green-small',
										margin: '10 10 0 0',
										formBind: true,
										listeners: {
											click: 'onEquipementSaveClick',
										},
									},
									{
										itemId: 'equipementCancelBtn',
										xtype: 'button',
										text: 'Annuler',
										ui: 'soft-blue-small',
										margin: '10 10 0 0',
										listeners: {
											click: () => {
												Ext.getCmp('form5').reset();
												Ext.getCmp('couleurid').inputEl.setStyle({
													backgroundColor: '#FFFFFF',
												});
											},
										},
										params: 'form5', // Envoyer le formId à la méthode de réinitialisation
									},
								],
							},
						],
					},
				],
			},
			{
				xtype: 'gridpanel',
				title: 'Liste des équipements',
				itemId: 'equipementGridId',
				id: 'equipementGridid',
				columnLines: true,
				selModel: 'rowmodel',
				cls: 'user-grid',
				store: equipementStore,
				height: 350,
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbar(this),
				],
				columns: [
					{
						text: '# idEquipement',
						hidden: true,
						dataIndex: 'idEquipement',
					},
					{
						text: '# idEmploye',
						hidden: true,
						dataIndex: 'idEmploye',
					},
					{
						xtype: 'actioncolumn',
						cls: 'content-column',
						width: 80,
						text: 'Actions',
						items: [
							{
								iconCls: 'x-fa fa-trash soft-red-small',
								ui: 'soft-red',
								handler: 'confirmDeleteEquipement',
								align: 'center',
							},
						],
					},
					{
						text: 'Type équipement',
						dataIndex: 'typeEquipement',
						width: 200,
						renderer: (value) => {
							const typeEquipementStore = Ext.getStore('typeEquipementStore');
							const typeEquipement = typeEquipementStore.findRecord('typeEquipement', value);
							return `${typeEquipement.data.displayText}`;
						},
					},
					{
						text: 'Marque',
						dataIndex: 'marque',
						width: 150,
					},
					{
						text: 'Numéro de série',
						dataIndex: 'numeroSerie',
						width: 150,
					},
					{
						text: 'Couleur',
						dataIndex: 'couleur',
						renderer: (value, meta) => {
							meta.style = `background-color:#${value};`;
							return value;
						},
					},
					{
						text: 'Autres caractéristiques',
						dataIndex: 'autresCaracteristiques',
						width: 300,
						renderer: value => `<span data-qtitle="Caractéristiques" data-qwidth="200" data-qtip="${value}">${value}</span>`,
					},
					{
						text: 'Date de réception',
						dataIndex: 'dateReception',
						renderer: value => Gprh.ades.util.Globals.rendererDate(value),
					},
					{
						text: 'Etat de réception',
						dataIndex: 'etatReception',
						width: 200,
						renderer: (value) => {
							const etatEquipementStore = Ext.getStore('etatMaterielStore');
							const etatMateriel = etatEquipementStore.findRecord('etatMateriel', value);
							return etatMateriel.data.displayText;
						},
					},
					{
						text: 'Date de restitution',
						dataIndex: 'dateRestitution',
						renderer: value => Gprh.ades.util.Globals.rendererDate(value),
					},
					{
						text: 'Etat de restitution',
						dataIndex: 'etatRestitution',
						width: 200,
						renderer: (value) => {
							const etatEquipementStore = Ext.getStore('etatMaterielStore');
							const etatMateriel = Ext.isEmpty(value) ? value : (etatEquipementStore.findRecord('etatMateriel', value)).data.displayText;
							return etatMateriel;
						},
					},
				],
				listeners: {
					rowclick: 'onRowEquipementClick',
				},
			},
		];
	
		this.callParent();
	},
});
