Ext.define('Gprh.ades.view.personnel.forms.ConjointCorrespondant', {
	extend: 'Ext.form.Panel',
	xtype: 'conjointCorrespondantForm',
	
	initComponent() {
		this.items = [
			{
				xtype: 'container',
				layout: 'column',
				items: [
					{
						defaultType: 'textfield',
						defaults: {
							minWidth: 500,
							labelWidth: 150,
							labelAlign: 'left',
							labelSeparator: ':',
							submitEmptyText: false,
						},
						columnWidth: '0.50',
						xtype: 'fieldset',
						title: 'Conjoint(e)',
						style: {
							background: 'transparent',
						},
						items: [
							{
								name: 'idConjoint',
								itemId: 'idConjointId',
								fieldLabel: '# Conjoint',
								hidden: true,
								value: Ext.isEmpty(this.sendDataConjoint) ? '' : this.sendDataConjoint.get('idConjoint'),
							},
							{
								name: 'idEmploye',
								itemId: 'idEmployeId',
								fieldLabel: '# Employé',
								hidden: true,
								value: Ext.isEmpty(this.sendData) ? 0 : this.sendData.get('idEmploye'),
							},
							{
								name: 'nomConjoint',
								itemId: 'nomConjointId',
								fieldLabel: 'Nom',
								hidden: false,
								value: Ext.isEmpty(this.sendDataConjoint) ? '' : this.sendDataConjoint.get('nomConjoint'),
							},
							{
								name: 'prenomConjoint',
								itemId: 'prenomConjointId',
								fieldLabel: 'Prénoms',
								hidden: false,
								value: Ext.isEmpty(this.sendDataConjoint) ? '' : this.sendDataConjoint.get('prenomConjoint'),
							},
							{
								xtype: 'datefield',
								name: 'dateNaissanceConjoint',
								itemId: 'dateNaissanceConjointId',
								fieldLabel: 'Date de naissance',
								hidden: false,
								format: 'd/m/Y',
								submitFormat: 'Y-m-d',
								minWidth: 100,
								value: Ext.isEmpty(this.sendDataConjoint) ? Ext.Date.format(Ext.Date.add(new Date(), Ext.Date.YEAR, -18), 'd/m/Y') : this.sendDataConjoint.get('dateNaissanceConjoint'),
							},
							{
								xtype: 'combobox',
								name: 'genreConjoint',
								itemId: 'genreConjointId',
								fieldLabel: 'Sexe',
								hidden: false,
								bind: {
									store: '{sexeResults}',
								},
								queryMode: 'local',
								displayField: 'displayText',
								valueField: 'sexe',
								renderTo: Ext.getBody(),
								editable: false,
								minWidth: 100,
								margin: '0 0 10 0',
								value: this.sendConjointSexe,
							},
							{
								name: 'fonctionConjoint',
								itemId: 'fonctionConjointId',
								fieldLabel: 'Fonction du conjoint',
								hidden: false,
								value: Ext.isEmpty(this.sendDataConjoint) ? '' : this.sendDataConjoint.get('fonctionConjoint'),
							},
							{
								name: 'telConjoint',
								itemId: 'telConjointId',
								fieldLabel: 'Tél. du conjoint',
								hidden: false,
								value: Ext.isEmpty(this.sendDataConjoint) ? '' : this.sendDataConjoint.get('telConjoint'),
							},
						],
					}, {
						defaultType: 'textfield',
						defaults: {
							minWidth: 500,
							labelWidth: 150,
							labelAlign: 'left',
							labelSeparator: ':',
							submitEmptyText: false,
							anchor: '100%',
						}, 
						columnWidth: '0.50',
						xtype: 'fieldset',
						title: 'Personne à contacter en cas d\'urgence',
						style: {
							background: 'transparent',
						},
						items: [
							{
								name: 'idCorrespondant',
								itemId: 'idCorrespondantId',
								fieldLabel: '# Correspondance',
								hidden: true,
								value: Ext.isEmpty(this.sendDataCorrespondant) ? '' : this.sendDataCorrespondant.get('idCorrespondant'),
							},
							{
								xtype: 'fieldcontainer',
								itemId: 'choicecontainerId',
								fieldLabel: 'Conjoint(e) est la personne à contater en cas d\'urgence',
								labelWidth: 350,
								defaultType: 'radiofield',
								defaults: {
									flex: 1,
									labelWidth: 25,
								},
								layout: 'hbox',
								items: [
									{
										labelSeparator: '',
										fieldLabel: 'Oui',
										name: 'contact',
										itemId: 'contactYesId',
										checked: false,
										handler: 'yesCorrespondanceChecked',
									},
									{
										labelSeparator: '',
										fieldLabel: 'Non',
										name: 'contact',
										itemId: 'contactNoId',
										checked: true,
									},
								],
								
							},
							{
								name: 'nomPrenomCorrespondant',
								itemId: 'nomPrenomCorrespondantId',
								fieldLabel: 'Nom complet',
								hidden: false,
								value: Ext.isEmpty(this.sendDataCorrespondant) ? '' : this.sendDataCorrespondant.get('nomPrenomCorrespondant'),
								allowBlank: false,
							},
							{
								name: 'telCorrespondant',
								itemId: 'telCorrespondantId',
								fieldLabel: 'Téléphone',
								hidden: false,
								value: Ext.isEmpty(this.sendDataCorrespondant) ? '' : this.sendDataCorrespondant.get('telCorrespondant'),
								allowBlank: false,
							},
							{
								xtype: 'component',
								margin: '119 0 0 0',
								html: '* <b>Sont obligatoires le nom et le numéro de téléphone de la personne contact.</b>',
							},							
						],
					},
				],
			},
			{
				xtype: 'toolbar',
				layout: 'hbox',
				items: [
					'->',
					{
						itemId: 'conjointCorrespondantSaveBtn',
						xtype: 'button',
						text: 'Enregistrer',
						ui: 'soft-green-small',
						margin: '10 10 0 0',
						formBind: true,
						listeners: {
							click: 'onConjointCorrespondantSaveClick',
						},
					},
					{
						itemId: 'conjointCorrespondantCancelBtn',
						xtype: 'button',
						text: 'Annuler',
						ui: 'soft-blue-small',
						margin: '10 0 0 0',
						listeners: {
							click: 'cancelAction',
						},
						params: 'form1', // Send formId to the reset method 
					},
				],
			},
		];
	
		this.callParent();
	},
});
