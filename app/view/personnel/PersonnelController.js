Ext.define('Gprh.ades.view.personnel.PersonnelController', {
	extend: 'Ext.app.ViewController',
	
	alias: 'controller.personnelController',

	requires: [
		'Gprh.ades.util.Globals',
		/* 'Gprh.ades.view.personnel.PersonnelEditContainer',
		'Gprh.ades.view.personnel.PersonnelWizardForm', */
		'Gprh.ades.store.EnfantScolarite',
		'Gprh.ades.store.Adresse',
		'Gprh.ades.store.Qualification',
		'Gprh.ades.store.Juridique',
		'Gprh.ades.view.contrat.ContratForm',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
		'Gprh.ades.view.contrat.ContratController',
	],


	init() {
		this.setCurrentView('personnelGrid');
	},

	setCurrentView(view, params) {
		const contentPanel = this.getView().down('#contentPanelPersonnel');
		// We skip rendering for the following scenarios:
		// * There is no contentPanel
		// * view xtype is not specified
		// * current view is the same
		if (!contentPanel || view === '' || (contentPanel.down() && contentPanel.down().xtype === view)) {
			return false;
		}

		if (params && params.openWindow) {
			const cfg = Ext.apply({
				xtype: 'prototypeWindow',
				items: [
					Ext.apply({
						xtype: view,
					}, params.targetCfg),
				],
			}, params.windowCfg);

			Ext.create(cfg);
		} else {
			Ext.suspendLayouts();

			contentPanel.removeAll(true);
			contentPanel.add(
				Ext.apply({
					xtype: view,
				}, {
					parameters: params,
				})
			);

			Ext.resumeLayouts(true);
		}
		return true;
	},

	onAddPersonnelClick() {
		this.setCurrentView('personnelEditContainer');
	},

	onBackListPersonnel(btn) {
		let targetContainer;
		const resetPersonnelMainContainer = (target) => {
			Ext.suspendLayouts();

			target.removeAll(true);
			target.add(
				Ext.apply({
					xtype: 'personnelGrid',
				})
			);

			Ext.resumeLayouts(true);
		};

		if (btn.itemId === 'bntListePersonnelInContrat') {
			targetContainer = this.getView().up('#contentPanel').down('#personnelMainContainerId').down('#contentPanelPersonnel');
			targetContainer.removeAll(true);
			
			resetPersonnelMainContainer(targetContainer);

			const navigationTreeList = this.getView().up('#main-view-detail-wrap').down('#navigationTreeList');
			const contratNode = navigationTreeList.itemMap[4].getNode();

			navigationTreeList.fireEvent('selectionchange', null, contratNode);
		} else {
			if (Ext.isEmpty(this.getView().up('#contentPanelPersonnel'))) {
				const navigationTreeList = this.getView().up('#main-view-detail-wrap').down('#navigationTreeList');
				const contratNode = navigationTreeList.itemMap[3].getNode();

				navigationTreeList.fireEvent('selectionchange', null, contratNode);
			} else {
				targetContainer = this.getView().up('#contentPanelPersonnel');
				resetPersonnelMainContainer(targetContainer);
			}
		}
	},

	onPreviousPersonnelClick(){
		this.setCurrentView('personnelGrid');
	},

	navigateTo(button) {
		const panel = button.up('panel');		
		const progress = this.lookupReference('progress');
		
		/*
		 * For carrousel animation automatically to the left or right direction
		 * const layout = panel.getLayout();
		 * const activeItem = layout.getActiveItem();
		 * const activeIndex = panel.items.indexOf(activeItem);
		 * const direction = (activeIndex < button.step) ? 'next' : 'prev';
		 * layout[direction]();
		 * activeItem.focus();
		 */

		const progressItems = progress.items.items;

		Ext.each(progressItems, (item) => {
			if (button.step === item.step) {
				item.setPressed(true);
			} else {
				item.setPressed(false);
			}
			/* 
			 * IE8 has an odd bug with handling font icons in pseudo elements;
			 * it will render the icon once and not update it when something
			 * like text color is changed via style addition or removal.
			 * We have to force icon repaint by adding a style with forced empty
			 * pseudo element content, (x-sync-repaint) and removing it back to work
			 * around this issue.
			 * See this: https://github.com/FortAwesome/Font-Awesome/issues/954
			 * and this: https://github.com/twbs/bootstrap/issues/13863
			 */
			if (Ext.isIE8) {
				item.btnIconEl.syncRepaint();
			}
		});

		panel.layout.setActiveItem(`form${button.step}`);

		/*
		* If button Prev & Next exist, check if the current item is the last
		* and then disable the Next button
		* Check if the first item is active then disable the Prev button
		*/
		// beginning disables previous
		/*
		if (activeIndex === 0) {
			model.set('atBeginning', true);
		}

		// wizard is at last step. Disable next at end.
		const model = panel.getViewModel();
		if (activeIndex === 3) {
			model.set('atEnd', true);
		}
		*/
	},

	cancelAction(formId) {
		const form = this.getView().down(`#${formId.params}`);
		form.reset();
	},

	onRenseignementSaveClick() {
		const form = this.getView().down('#form0');
		const form1 = this.getView().down('#form1');
		const form2 = this.getView().down('#form2');
		const form5 = this.getView().down('#form5');
		const formAdresse = this.getView().down('#adresseFormId');
		const formQualification = this.getView().down('#qualificationFormId');
		const formJuridique = this.getView().down('#juridiqueFormId');
		const viewModel = this.getViewModel();
		
		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/Personnel.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			form.down('#idEmployeId').setValue(serverResponse);
			form1.down('#idEmployeId').setValue(serverResponse);
			form2.down('#idEmployeId').setValue(serverResponse);
			form5.down('#idEmployeId').setValue(serverResponse);
			formAdresse.down('#idEmployeId').setValue(serverResponse);
			formQualification.down('#idEmployeId').setValue(serverResponse);
			formJuridique.down('#idEmployeId').setValue(serverResponse);
			
			const msg = Gprh.ades.util.Globals.successMsg + 'Les autres onglets sont activés.';
			Gprh.ades.util.Helper.showSuccess(msg);
			
			this.getView().down('#generalPersonnelId').setValue(serverResponse);
			// viewModel.set('personnelId', serverResponse);
		}
	},

	onRowPersonnelDblClick(view, record) {
		const idEmploye = record.get('idEmploye');
		if (Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') !== '3') {
			this.chargePersonnelDetails(view, idEmploye);
		} else {
			return false;
		}
		
	},

	async chargePersonnelDetails (view, idEmploye) {
		let targetContainer;
		if (!this.hasOwnProperty('view')) {
			targetContainer = view.getView().up('#contentPanel');
		} else {
			targetContainer = this.getView().down('#contentPanelPersonnel');
		}

		const extraParameters = {
			extraParams: {
				infoRequest: 2,
				idEmploye: idEmploye,
			},
		};

		// Récupère les renseignements du personnel
		const inputRenseignementData = await Gprh.ades.util.Globals.requestOneRecordSettings('Gprh.ades.store.Personnel', extraParameters);
		
		// Récupère les informations des conjoints	
		const inputConjointData = await Gprh.ades.util.Globals.requestOneRecordSettings('Gprh.ades.store.Conjoint', extraParameters);
		
		// Récupère les informations de la personne contact
		const inputCorrepondantData = await Gprh.ades.util.Globals.requestOneRecordSettings('Gprh.ades.store.Correspondance', extraParameters);

		// Récupère les enfants et leurs scolarités
		const inputEnfantScolariteData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.EnfantScolarite', extraParameters);

		// Récupère les adresses
		const inputAdresseData = await Gprh.ades.util.Globals.requestOneRecordSettings('Gprh.ades.store.Adresse', extraParameters);

		// Récupère le résumé des expériences, niveau d'éducation le plus élevé
		const inputQualificationData = await Gprh.ades.util.Globals.requestOneRecordSettings('Gprh.ades.store.Qualification', extraParameters);

		// Récupère les antécédants judiciaires
		const inputJuridiqueData = await Gprh.ades.util.Globals.requestOneRecordSettings('Gprh.ades.store.Juridique', extraParameters);

		// Récupère les équipements
		const inputEquipementData = await Gprh.ades.util.Globals.requestOneRecordSettings('Gprh.ades.store.Equipement', extraParameters);

		Ext.suspendLayouts();

		targetContainer.removeAll(true);
		targetContainer.add(
			Ext.apply({
				xtype: 'personnelEditContainer',
				params: inputRenseignementData[0],
				conjointParams: inputConjointData[0],
				correspondantParams: inputCorrepondantData[0],
				enfantScolariteParams: inputEnfantScolariteData,
				adresseParams: inputAdresseData[0],
				qualificationParams: inputQualificationData[0],
				juridiqueParams: inputJuridiqueData[0],
				equipementParams: inputEquipementData[0],
			})
		);
		
		// const nbEnfant = Ext.isEmpty(inputEnfantScolariteData.data) ? 0 : inputEnfantScolariteData.data.length;
		// this.getViewModel().set('storeLen', nbEnfant > 0);

		Ext.resumeLayouts(true);
	},

	onConjointCorrespondantSaveClick() {
		const form = this.getView().down('#form1');
		const viewModel = this.getViewModel();
		
		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/ConjointCorrespondant.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		const checkReturns = serverResponse.data.reduce((a, b) => parseInt(a, 10) + parseInt(b, 10));
		if (checkReturns > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			
			viewModel.set('personnelId', serverResponse);
		}
	},

	yesCorrespondanceChecked(newValue) {
		const conjointNomComplet = `${this.getView().down('#nomConjointId').value} ${this.getView().down('#prenomConjointId').value}`;
		if (newValue.checked) {
			if (!Ext.isEmpty(this.getView().down('#nomConjointId').value)) {
				this.getView().down('#nomPrenomCorrespondantId').setValue(conjointNomComplet);
				this.getView().down('#telCorrespondantId').setValue(this.getView().down('#telConjointId').value);
			} else {
				Gprh.ades.util.Helper.showError('Vous devez remplir les champs du conjoint.');
				this.getView().down('#contactNoId').setValue(true);
				this.getView().down('#contactYesId').setValue(false);
			}
		} else {
			this.getView().down('#nomPrenomCorrespondantId').reset();
			this.getView().down('#telCorrespondantId').reset();
		}
	},

	civiliteOnChange(combobox, newValue) {
		if (newValue === '1') {
			this.getView().down('#sexeId').setValue('1');
		} else this.getView().down('#sexeId').setValue('2');
	},

	onEnfantSaveClick() {
		const form = this.getView().down('#form2');
		const viewModel = this.getViewModel();

		const model = {
			data: form.getValues(),
		};

		if (form.getValues().hasOwnProperty('nomEnfant')) {
			Ext.apply(model, {
				infoRequest: 2,
			});
		} else {
			Ext.apply(model, {
				infoRequest: 5,
			});
		}
		
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/EnfantScolarite.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		const checkReturns = serverResponse.data.reduce((a, b) => parseInt(a, 10) + parseInt(b, 10));
		if (checkReturns > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			this.resetAllValuesInForm2();
			this.getView().down('#enfantScolariteGridId').getStore().load();
			this.getView().down('#enfantFieldSetId').setDisabled(false);
		} else Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
	},

	resetAllValuesInForm2() {
		const allFields = [
			'idEnfant',
			'nomEnfant',
			'prenomEnfant',
			'dateNaissanceEnfant',
			'lieuNaissanceEnfant',
			'genreEnfant',
			'idScolarite',
			'idEducation',
			'nomEcole',
			'adresseEcole',
			'anneeScolaire',
			'fraisScolarite'
		];
		Ext.each(allFields, (field) => {
			let resetValue;
			if (field === 'dateNaissanceEnfant') {
				resetValue = new Date();
			} else if (field === 'genreEnfant' || field === 'idEducation') {
				resetValue = 1;
			} else {
				resetValue = '';
			}
			this.getView().down(`#${field}Id`).setValue(resetValue);
		});
	},

	onEnfantScolariteClick(record, element, rowIndex, e, eOpts) {
		const allFields = [
			'idEnfant',
			'nomEnfant',
			'prenomEnfant',
			'dateNaissanceEnfant',
			'lieuNaissanceEnfant',
			'genreEnfant',
			'idScolarite',
			'idEducation',
			'nomEcole',
			'adresseEcole',
			'anneeScolaire',
			'fraisScolarite'
		];
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.selection.get(field));
			});
		}
	},

	actionRowEnfantScolarite(view, cell, recordIndex, cellIndex, e, record) {
		const domEl = e.target;
		const [action, id] = domEl.id.split('_');

		const enfantParams = {
			idEnfant: record.get('idEnfant'),
		};

		const messageBoxEnfant = Ext.create('Ext.window.MessageBox', {
			itemId: 'confirmMsgBoxEnfant',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelEnfant = {
						infoRequest: 3,
						data: enfantParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/EnfantScolarite.php', modelDelEnfant);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#confirmMsgBoxEnfant').close();
						this.getView().down('#enfantScolariteGridId').getStore().load();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#confirmMsgBoxEnfant').close();
				},
			}],
		});

		const scolariteParams = {
			idScolarite: record.get('idScolarite'),
		};

		const messageBoxScolarite = Ext.create('Ext.window.MessageBox', {
			itemId: 'confirmMsgBoxScolarite',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelScolarite = {
						infoRequest: 4,
						data: scolariteParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/EnfantScolarite.php', modelDelScolarite);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#confirmMsgBoxScolarite').close();
						this.getView().down('#enfantScolariteGridId').getStore().load();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#confirmMsgBoxScolarite').close();
				},
			}],
		});
		
		if (action === 'delEnfant') {
			messageBoxEnfant.show({
				title: 'Confirmer la suppression',
				msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
				icon: Ext.MessageBox.WARNING,
			});
		} else if (action === 'delScolarite') {
			messageBoxScolarite.show({
				title: 'Confirmer la suppression',
				msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
				icon: Ext.MessageBox.WARNING,
			});
		} else if (action === 'addScolarite') {
			this.getView().down('#enfantFieldSetId').setDisabled(true);
			this.getView().down('#idEnfantId').setValue(id);
		}
	},

	disableAddEnfantBtn() {
		this.getView().down('#enfantAddBtn').setDisabled(true);
	},

	changeRegionStore(view, newValue, oldValue, eOpts) {
		const regionCombobox = this.getView().down('#adresseFormId').down('#idRegionId');
		
		const regionStore = regionCombobox.getStore();
		regionStore.filter('idProvince',newValue);
	},

	changeDistrictStore(view, newValue, oldValue, eOpts) {
		const districtCombobox = this.getView().down('#adresseFormId').down('#idDistrictId');
		
		const districtStore = districtCombobox.getStore();
		districtStore.filter('idRegion',newValue);
	},

	changeCommuneStore(view, newValue, oldValue, eOpts) {
		const communeCombobox = this.getView().down('#adresseFormId').down('#idCommuneId');
		
		const communeStore = communeCombobox.getStore();
		communeStore.filter('idDistrict',newValue);
	},

	changeFokotanyStore(view, newValue, oldValue, eOpts) {
		const fokotanyCombobox = this.getView().down('#adresseFormId').down('#idFokotanyId');
		
		const fokotanyStore = fokotanyCombobox.getStore();
		fokotanyStore.filter('idCommune',newValue);
	},

	onAdresseSaveClick() {
		const form = this.getView().down('#adresseFormId');
		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/Adresse.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			const msg = Gprh.ades.util.Globals.successMsg;
			Gprh.ades.util.Helper.showSuccess(msg);
			
			const updatedStore = Ext.create('Gprh.ades.store.Equipement', {  
				proxy: {
					extraParams: {
						infoRequest: 2,
						idEmploye: Ext.getCmp('idEmployeid').getValue(),
					},  
				},  
				autoLoad: true,
			});
			Ext.getCmp('adresseGridid').reconfigure(updatedStore);
		} else Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
	},

	onQualificationSaveClick() {
		const form = this.getView().down('#qualificationFormId');
		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/Qualification.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			const msg = Gprh.ades.util.Globals.successMsg;
			Gprh.ades.util.Helper.showSuccess(msg);
		}
	},

	onJuridiqueSaveClick() {
		const form = this.getView().down('#juridiqueFormId');
		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/Juridique.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			const msg = Gprh.ades.util.Globals.successMsg;
			Gprh.ades.util.Helper.showSuccess(msg);
		}
	},

	onDeletePersonnel(parameters) {
		const grid = this.getView().down(`#${parameters.params.gridId}`);
		const selection = grid.getSelection();

		const personnelParams = {
			idEmploye: selection[0].get('idEmploye'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxPersonnelDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelPersonnel = {
						infoRequest: 3,
						data: personnelParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/Personnel.php', modelDelPersonnel);
					const result = saveOrUpdate.request.request.result.responseText;
					if (result > 0) {
						btn.up('#messageBoxPersonnelDelete').close();
						grid.getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxPersonnelDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxSalaireDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	confirmActifBancaire(column, rowIndex, checked, record) {
		const grid = Ext.getCmp('bancaireGridid');
		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxBancaireChecked',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelUpdLigneActif = {
						infoRequest: 4,
						data: {
							idEmploye: record.data.idEmploye,
						},
						idBancaire: record.data.idBancaire,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/Bancaire.php', modelUpdLigneActif);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxBancaireChecked').close();
						grid.getStore().load();
					}			
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxBancaireChecked').close();
					grid.getStore().reload();
				},
			}],
		});
		
		if (checked) {
			messageBox.show({
				title: 'Confirmer l\'enregistrement',
				msg: 'Cette valeur existe déjà. Voulez-vous la remplacer?',
				icon: Ext.MessageBox.WARNING,
			});
		}
	},

	onEquipementSaveClick() {
		const form = this.getView().down('#form5');
		
		const model = {
			data: form.getValues(),
			infoRequest: 2,
		};
		
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/Equipement.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		const checkReturns = serverResponse;
		
		if (checkReturns > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			Ext.getCmp('form5').reset();
			Ext.getCmp('couleurid').inputEl.setStyle({
				backgroundColor: '#FFFFFF',
			});
			const updatedStore = Ext.create('Gprh.ades.store.Equipement', {  
				proxy: {
					extraParams: {
						infoRequest: 2,
						idEmploye: Ext.getCmp('idEmployeid').getValue(),
					},  
				},  
				autoLoad: true,
			});
			Ext.getCmp('equipementGridid').reconfigure(updatedStore);
		} else Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
	},

	onRowEquipementClick(record) {
		const allFields = [
			'idEquipement',
			'idEmploye',
			'typeEquipement',
			'marque',
			'numeroSerie',
			'couleur',
			'autresCaracteristiques',
			'dateReception',
			'etatReception',
			'dateRestitution',
			'etatRestitution',
		];
		let valueToShow;
		
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				if (field === 'dateReception' || field === 'dateRestitution') valueToShow = Ext.util.Format.date(record.selection.get(field), 'd/m/Y');
				else if (field === 'couleur') {
					Ext.getCmp('couleurid').inputEl.setStyle({
						backgroundColor: `#${record.selection.get(field)}`,
					});
					valueToShow = record.selection.get(field);
					this.getView().down(`#${field}Id`).setValue(valueToShow);
				} else valueToShow = record.selection.get(field);
				this.getView().down(`#${field}Id`).setValue(valueToShow);
			});
		}
	},

	onRowAdresseClick(record) { // TODO https://github.com/ifsnop/mysqldump-php
		const allFields = [
			'idFokotany',
			'idCommune',
			'idDistrict',
			'idRegion',
			'idProvince',
			'idEmploye',
			'idAdresse',
			'actifAdresse',
			'logementEmploye',
		];
		
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.selection.get(field));
			});
		}
	},

	confirmDeleteAdresse(view, cell, recordIndex, cellIndex, e, record) {
		const CngParams = {
			idAdresse: record.get('idAdresse'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxAdresseDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelCng = {
						infoRequest: 3,
						data: CngParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/Adresse.php', modelDelCng);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxAdresseDelete').close();
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						Ext.getCmp('adresseGridid').getStore().reload();
					} else Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxAdresseDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	equipementType(view, newValue, oldValue, eOpts) {
		const dateFinContratField = Ext.getCmp('numeroSerieid');
		if (newValue === '9' || newValue === '10') {
			Ext.apply(dateFinContratField, {
				vtype: 'numeroTel',
			});
			dateFinContratField.focus(true);
		} else {
			Ext.apply(dateFinContratField, {
				vtype: false,
			});
			dateFinContratField.unsetActiveError();
		}
	},

	confirmDeleteEquipement(view, cell, recordIndex, cellIndex, e, record) {
		const equipementParams = {
			idEquipement: record.get('idEquipement'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxEquipementDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelEquipement = {
						infoRequest: 3,
						data: equipementParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/personnel/Equipement.php', modelDelEquipement);
					const result = saveOrUpdate.request.request.result.responseText;
					if (result > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						btn.up('#messageBoxEquipementDelete').close();
						Ext.getCmp('equipementGridid').getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxEquipementDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxEquipementDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},
});
