// ViewModel des composants graphiques UtilisateurForm et UtilisateurGrid

Ext.define('Gprh.ades.view.utilisateur.UtilisateurViewModel', {
	extend: 'Ext.app.ViewModel',

	alias: 'viewmodel.utilisateurViewModel',

	requires: [
		'Gprh.ades.store.Utilisateur',
		'Gprh.ades.store.Centre',
	],

	// Store à binder
	stores: {
		utilisateurResults: {
			type: 'utilisateurStore',
			proxy: {
				extraParams: {
					infoRequest: 1,
				},
			},
		},
		centreResults: {
			type: 'centreStore',
		},
	},

	// Résultats de calcul à bider
	formulas: {},

	// Données à binder
	data: {},
});
