/**
 * Composant graphique contenant:
 * {Object} un tableau de tous les utilisateurs
 * {Event} la sélection d'une ligne replit les champs du formulaire à gauche
 */
Ext.define('Gprh.ades.view.utilisateur.UtilisateurGrid', {
	extend: 'Ext.panel.Panel',
	xtype: 'utilisateurGrid',

	requires: [
		'Gprh.ades.view.utilisateur.UtilisateurViewModel',
	],

	viewModel: {
		type: 'utilisateurViewModel',
	},

	initComponent() {
		this.items = [{
			xtype: 'gridpanel',
			cls: 'user-grid',
			itemId: 'utilisateurGridId',
			reference: 'utilisateurGridRef',
			bind: {
				store: '{utilisateurResults}',
			},
			viewConfig: {
				preserveScrollOnRefresh: true,
				stripeRows: true,
			},
			scrollable: true,
			selModel: 'rowmodel',
			listeners: {
				rowclick: 'onRowUtilisateurClick',
			},
			height: Ext.Element.getViewportHeight() - 75,
			columnLines: true,
			columns: {
				defaults: {
					align: 'left',
				},
				items: [
					{
						xtype: 'gridcolumn',
						dataIndex: 'avatar',
						renderer: (value) => {
							const avatar = Ext.isEmpty(value) ? 'logo2.png': value;
							return `<img src="resources/images/user-profile/${avatar}" alt="Image" height="32" width="32"/>`;
						},
						width: 50,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'nomUtilisateur',
						text: 'Nom d\'utilisateur',
						flex: 1,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'nomCentre',
						text: 'Centre d\'affectation',
						width: 250,
					},					
					{
						xtype: 'gridcolumn',
						dataIndex: 'typeUtilisateur1',
						text: 'Type d\'utilisateur',
						width: 250,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'typeUtilisateur',
						text: 'Type d\'utilisateur',
						hidden: true,
					},
					{
						xtype: 'actioncolumn',
						cls: 'content-column',
						width: 80,
						text: 'Actions',
						items: [
							{
								iconCls: 'x-fa fa-trash soft-red-small',
								handler: 'confirmDeleteUtilisateur',
							},
						],					
					},
					
				],				
			},
			dockedItems: [
				Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
			],
			tbar: [],
		}];

		this.callParent();
	},

});
