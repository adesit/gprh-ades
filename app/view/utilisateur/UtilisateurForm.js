/**
 * Composant graphique contenant:
 * {Object} un formulaire d'enregistrement d'un Utilisateur
 */
Ext.define('Gprh.ades.view.utilisateur.UtilisateurForm', {
	extend: 'Ext.form.Panel',

	xtype: 'utilisateurForm',

	// fileUpload: true,

	jsonSubmit: true,

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.util.VTypes',
	],

	colorScheme: 'soft-green',
	bodyPadding: 5,

	viewModel: 'utilisateurViewModel',

	initComponent() {
		const form = {
			xtype: 'form',
			itemId: 'UtilisateurFormId',
			layout: {
				type: 'vbox',
				align: 'stretch',
			},
			defaults: {
				labelWidth: 150,
				labelAlign: 'top',
				labelSeparator: ':',
				submitEmptyText: false,
			},
			items: [
				{
					xtype: 'combobox',
					width: 200,
					labelWidth: 125,
					labelAlign: 'left',
					fieldLabel: 'Centre d\'affectation',
					name: 'centreAffectation',
					itemId: 'centreAffectationId',
					reference: 'centreAffectationRef',
					publishes: 'value',
					bind: {
						store: '{centreResults}',
					},
					queryMode: 'local',
					displayField: 'nomCentre',
					valueField: 'idCentre',
					renderTo: Ext.getBody(),
					allowBlank: false, 
					forceSelection: true,
					editable: false,
					margin: '5 0 0 0',
				},
				{
					xtype: 'radiogroup',
					labelAlign: 'top',
					fieldLabel: 'Type d\'utilisateur',
					itemId: 'typeUtilisateurId',
					simpleValue: true,
					bind: {
						value: '{switchItem}',
					},
					columns: 1,
					vertical: true,
					items: [
						{
							boxLabel: 'Administrateur',
							name: 'typeUtilisateur',
							inputValue: 1,
						}, {
							boxLabel: 'Responsable de centre', 
							name: 'typeUtilisateur',
							inputValue: 2,
						}, {
							boxLabel: 'Responsable de formation', 
							name: 'typeUtilisateur',
							inputValue: 3,
						},
					],
				},
				{
					xtype: 'textfield',
					fieldLabel: 'id_utilisateur',
					name: 'idUtilisateur',
					reference: 'idUtilisateurRef',
					publishes: 'value',
					itemId: 'idUtilisateurId',
					hidden: true,
				},
				{
					xtype: 'textfield',
					fieldLabel: 'Nom d\'utilisateur',
					name: 'nomUtilisateur',
					reference: 'nomUtilisateurRef',
					publishes: 'value',
					itemId: 'nomUtilisateurId',
					maxWidth: 200,
					allowBlank: false,					
				},
				{
					xtype: 'textfield',
					inputType: 'password',
					fieldLabel: 'Mot de passe',
					name: 'motDePasse',
					reference: 'motDePasseRef',
					publishes: 'value',
					itemId: 'motDePasseId',
					maxWidth: 200,
					allowBlank: false,
				},
				{
					xtype: 'textfield',
					inputType: 'password',
					fieldLabel: 'Confirmer le mot de passe',
					labelWidth: 200,
					name: 'ConfirmMotDePasse',
					reference: 'ConfirmMotDePasseeRef',
					publishes: 'value',
					itemId: 'ConfirmMotDePasseId',
					maxWidth: 200,
					allowBlank: false,
					vtype: 'passwordMatch',
					msgTarget: 'side',
				},
				{
					xtype: 'fileuploadfield',
					name: 'avatar',
					itemId: 'avatarId',
					// padding: '0 0 0 -140',
					buttonOnly: true,
					buttonConfig: {
						text: 'Photo de profil',
					},
					listeners: {
						afterrender: () => {
							const els = Ext.select('div.x-form-trigger.x-form-trigger-default.x-form-trigger-cmp.x-form-trigger-cmp-default', true);
							els.removeCls('x-form-trigger-default');
						},
						change(field) {
							const dom = Ext.getDom(field.fileInputEl);
							const container = field.up().up('panel');
							const viewModel = container.getViewModel();
							const reader = new FileReader();
							
							reader.onload = e => viewModel.set('imgData', e.target.result);
		
							reader.readAsDataURL(dom.files[0]);
						},
					},
				},
				{
					xtype: 'image',
					itemId: 'avatarImgId',
					renderTo: Ext.getBody(),
					flex: 1,
					alt: 'Avatar',
					maxWidth: 50,
					maxHeight: 50,
					bind: {
						src: '{imgData}',
					},
				},
			],
			
		};

		this.items = [
			{
				html: '<span class="x-fa fa-info-circle"></span> Sélectionnez une ligne dans la grille principale pour l\'éditer ici.',
				itemId: 'helperId',
				baseCls: 'x-toast-info',
				anchor: '100%',
			},
			form,
			{
				xtype: 'toolbar',
				cls: 'wizard-form-break',
				defaults: {
					flex: 1,
				},
				width: 300,
				layout: 'hbox',
				items: [
					{
						itemId: 'UtilisateurFormSaveBtn',
						xtype: 'button',
						text: 'Enregistrer',
						ui: 'soft-green-small',
						margin: '10 10 0 0',
						formBind: true,
						handler: 'onUtilisateurFormSaveClick',
					},
					{
						itemId: 'UtilisateurFormCancelBtn',
						xtype: 'button',
						text: 'Annuler',
						ui: 'soft-blue-small',
						margin: '10 0 0 0',
						listeners: {
							click: 'cancelAction',
						},
						params: 'UtilisateurFormId',
					},
				],
			},
		];

		this.callParent();
	},
});
