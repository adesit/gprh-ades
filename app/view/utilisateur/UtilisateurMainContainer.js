/**
 * Composant graphique contenant:
 * {@link Gprh.ades.view.utilisateur.UtilisateurForm} un formulaire d'enregistrement d'un Utilisateur
 * {@link Gprh.ades.view.utilisateur.UtilisateurGrid} un tableau de tous les utilisateurs
 */
Ext.define('Gprh.ades.view.utilisateur.UtilisateurMainContainer', {
	extend: 'Ext.container.Container',

	xtype: 'utilisateurMainContainer',

	requires: [
		'Gprh.ades.view.utilisateur.UtilisateurController',
		'Gprh.ades.view.utilisateur.UtilisateurGrid',
		'Gprh.ades.view.utilisateur.UtilisateurForm',
	],

	viewModel: {
		type: 'utilisateurViewModel',
	},

	controller: 'utilisateurController',

	itemId: 'utilisateurMainContainerId',

	layout: 'column',

	items: [
		{
			columnWidth: '0.30',
			items: [
				{
					xtype: 'utilisateurForm',
				},
			],
		},
		{
			columnWidth: '0.70',
			userCls: 'shadow',
			items: [
				{
					xtype: 'utilisateurGrid',
				},
			],
		},
	],
});
