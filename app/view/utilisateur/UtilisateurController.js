// ViewController du composant graphique UtilisateurForm et UtilisateurGrid

Ext.define('Gprh.ades.view.utilisateur.UtilisateurController', {
	extend: 'Ext.app.ViewController',
	
	alias: 'controller.utilisateurController',

	requires: [
		'Gprh.ades.util.Globals',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	// méthode appelée automatiquement à l’initialisation
	init() {
		
	},

	/**
	 * Reset le formulaire.
	 * @param {Object} formId contient le itemId du formulaire.
	 * @return {undefined}
	 * @private
	 */
	cancelAction(formId) {
		const form = this.getView().down(`#${formId.params}`);
		
		form.down('#avatarImgId').setSrc('');
		form.reset();
	},

	/**
	 * Enregistre les données d'un utilisateur et son image avatar
	 * @return {undefined}
	 * @private
	 */
	onUtilisateurFormSaveClick() {
		const form = this.getView().down('#UtilisateurFormId');
		const Utilisateurgrid = this.getView().down('#utilisateurGridId');

		form.getForm().submit({
			url: 'server-scripts/utilisateur/Utilisateur.php',
			params: {
				infoRequest: 2,
			},
			waitMsg: 'Enregistrement ...',
			scope: this,
			success: (formulaire, action) => {
				const data = Ext.JSON.decode(action.response.responseText);
				/**
				 * Si l'enregistrement a bient abouti, reset le champ de fichier
				 * reset le form
				 * afficher un message de réussite
				 * rafraichir le grid
				 */
				if (data.msg > 0) {
					form.down('#avatarImgId').setSrc('');
					form.reset();
					Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
					Utilisateurgrid.getStore().reload();
				}
			},
		});
	},

	/**
	 * Supprimer un utilisateur
	 * @return {undefined}
	 * @private
	 */
	confirmDeleteUtilisateur(view, cell, recordIndex, cellIndex, e, record) {
		const UtilisateurParams = {
			idUtilisateur: record.get('idUtilisateur'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxUtilisateurDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => { // Si le bouton 'Oui' est cliqué envoyer la requête de suppression
					const modelDelUtilisateur = {
						infoRequest: 3,
						data: UtilisateurParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/utilisateur/Utilisateur.php', modelDelUtilisateur);
					// Si la requête a bien aboutie, fermer la fenêtre de confirmation et rafraichir le grid.
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxUtilisateurDelete').close();
						this.getView().down('#utilisateurGridId').getStore().load();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxUtilisateurDelete').close();
				},
			}],
		});
		
		// Afficher un message de confirmation de suppression
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	onRowUtilisateurClick(record) {
		const allFields = [
			'idUtilisateur',
			'nomUtilisateur',
			'centreAffectation',
		];
		
		Ext.ComponentQuery.query('#typeUtilisateurId')[0].items.items[record.selection.get('typeUtilisateur') - 1].setValue(true);
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.selection.get(field));
			});
		}
	},
});
