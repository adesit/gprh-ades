/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('Gprh.ades.view.main.MainModel', {
	extend: 'Ext.app.ViewModel',

	alias: 'viewmodel.mainViewModel',

	data: {
		name: 'Gprh.ades',
	},

	// TODO - add data, formulas and/or methods to support your view
});
