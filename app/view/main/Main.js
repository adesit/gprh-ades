/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 */
Ext.define('Gprh.ades.view.main.Main', {
	extend: 'Ext.container.Viewport',
	xtype: 'app-main',

	requires: [
		'Ext.window.MessageBox',
		'Ext.button.Segmented',
		'Ext.list.Tree',

		'Gprh.ades.view.main.MainController',
		'Gprh.ades.view.main.MainModel',
		'Gprh.ades.view.main.MainContainerWrap',
	],

	controller: 'mainController',
	viewModel: 'mainViewModel',

	cls: 'sencha-dash-viewport',
	itemId: 'mainViewportId',

	layout: {
		type: 'vbox',
		align: 'stretch',
	},

	listeners: {
		render: 'onMainViewRender',
	},

	initComponent() {
		const avatar = Ext.isEmpty(Ext.util.LocalStorage.get('foo').getItem('avatar')) ? 'logo2.jpg' : Ext.util.LocalStorage.get('foo').getItem('avatar');
		this.items = [
			{
				xtype: 'toolbar',
				cls: 'sencha-dash-dash-headerbar shadow',
				height: 64,
				itemId: 'headerBar',
				items: [
					{
						xtype: 'component',
						reference: 'senchaLogo',
						cls: 'sencha-logo',
						html: '<div class="main-logo"><img src="resources/images/icons/cloud-icon.png">ADES</div>',
						width: 250,
					},
					{
						margin: '0 0 0 8',
						ui: 'header',
						iconCls: 'x-fa fa-navicon',
						id: 'main-navigation-btn',
						handler: 'onToggleNavigationSize',
					},
					'->',
					{
						xtype: 'segmentedbutton',
						margin: '0 16 0 0',
	
						platformConfig: {
							ie9m: {
								hidden: true,
							},
						},
	
						items: [{
							iconCls: 'x-fa fa-power-off',
							pressed: false,
							tooltip: 'Se déconnecter',
							handler: 'onClickButton',
						}],
					},
					/* {
						iconCls: 'flag-icon flag-icon-fr',
						ui: 'header',
						href: '#searchresults',
						hrefTarget: '_self',
						tooltip: 'Français',
					},
					{
						iconCls: 'flag-icon flag-icon-gb',
						ui: 'header',
						href: '#searchresults',
						hrefTarget: '_self',
						tooltip: 'English',
					},
					{
						iconCls: 'flag-icon flag-icon-de',
						ui: 'header',
						href: '#searchresults',
						hrefTarget: '_self',
						tooltip: 'Dutch',
					}, */
					{
						xtype: 'tbtext',
						text: Ext.util.LocalStorage.get('foo').getItem('nomUtilisateur'),
						cls: 'top-user-name',
					},
					{
						xtype: 'image',
						cls: 'header-right-profile-image',
						height: 35,
						width: 35,
						alt: 'current user image',
						src: `resources/images/user-profile/${avatar}`,
					},
				],
			},
			{
				xtype: 'maincontainerwrap',
				itemId: 'main-view-detail-wrap',
				reference: 'mainContainerWrap',
				flex: 1,
				items: [
					{
						xtype: 'panel',
						maxHeight: Ext.Element.getViewportHeight() - 64,
						scrollable: true,
						items: [
							{
								xtype: 'treelist',
								reference: 'navigationTreeList',
								itemId: 'navigationTreeList',
								ui: 'navigation',
								store: {
									root: {
										expanded: true,
										children: [
											{
												text: 'ADES',
												iconCls: 'x-fa fa-home',
												viewType: 'adesMainContainer',
												routeId: 'ades', // routeId defaults to viewType
												leaf: true,
												// visible: this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1']),
											},
											{
												text: 'Personnel',
												iconCls: 'x-fas fa-id-badge',
												viewType: 'personnelMainContainer',
												routeId: 'personnel',
												leaf: true,
											},
											{
												text: 'Contrats de travail',
												iconCls: 'x-fas fa-file-contract',
												visible: this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1', '2']),
												leaf: false,
												expanded: false,
												selectable: false,
												children: [
													{
														text: 'Liste des contrats',
														iconCls: 'x-fas fa-clipboard-list',
														viewType: 'contratMainContainer',
														routeId: 'contrat',
														leaf: true,
													},
													{
														text: 'Suivi des contrats',
														iconCls: 'x-far fa-flag',
														viewType: 'contratSuiviContainer',
														routeId: 'suivi',
														leaf: true,
													},
												],
											},			
											{
												text: 'Salaire',
												iconCls: 'x-fas fa-money-check-alt',
												viewType: 'salaireMainContainer',
												routeId: 'salaire',
												leaf: true,
												visible: this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1']),
											},
											{
												text: 'Eléments variables',
												iconCls: 'x-fa fa-cogs',
												leaf: false,
												expanded: false,
												selectable: false,
												visible: this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1', '2']),
												children: [
													{
														text: 'Rappels',
														iconCls: 'x-fa fa-plus-circle',
														viewType: 'rappelPanel',
														routeId: 'rappel',
														leaf: true,
													},
													{
														text: 'Salaire intérim',
														iconCls: 'x-fas fa-user-clock',
														viewType: 'salaireInterimPanel',
														routeId: 'salaire-interim',
														leaf: true,
														// visible: this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1']),
													},
													{
														text: 'Heures supplémentaires',
														iconCls: 'x-fa fa-clock',
														viewType: 'heuresSupplementairesPanel',
														routeId: 'heures-supplementaires',
														leaf: true,
													},
													{
														text: 'Absences et permissions',
														iconCls: 'x-fa fa-calendar-minus',
														viewType: 'absencePanel',
														routeId: 'absence',
														leaf: true,
													},
													{
														text: 'Congés',
														iconCls: 'x-fa fa-calendar-check',
														viewType: 'congePanel',
														routeId: 'conge',
														leaf: true,
													},
													{
														text: 'Avances',
														iconCls: 'x-fas fa-money-bill-wave',
														viewType: 'avancePanel',
														routeId: 'avance',
														leaf: true,
													},
													{
														text: 'Frais médicaux',
														iconCls: 'x-fa fa-user-md',
														viewType: 'fraisMedicauxPanel',
														routeId: 'frais-medicaux',
														leaf: true,
													},
													{
														text: 'Divers retenus',
														iconCls: 'x-fa fa-minus-circle',
														viewType: 'diversRetenusPanel',
														routeId: 'divers-retenus',
														leaf: true,
													},
													{
														text: 'Dépassements téléphoniques',
														iconCls: 'x-fa fa-tty',
														viewType: 'depassementPanel',
														routeId: 'depassement-telephonique',
														leaf: true,
													},
													/* {
														text: 'Frais de scolarité des enfants',
														iconCls: 'x-fa fa-university',
														viewType: 'scolarite',
														leaf: true,
													}, */
												],
											},
											{
												text: 'Etats et Fiches de paie',
												iconCls: 'x-fas fa-file-download',
												viewType: 'paieMainContainer',
												routeId: 'paie',
												leaf: true,
												visible: this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1', '2']),
											},
											{
												text: '13ème mois',
												iconCls: 'x-fas fa-gift',
												viewType: 'treiziemeMainContainer',
												routeId: 'treiziemeMois',
												leaf: true,
												visible: this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1', '2']),
											},
											{
												text: 'Solde de tout compte',
												iconCls: 'x-fas fa-file-invoice',
												viewType: 'soldeMainContainer',
												routeId: 'solde',
												leaf: true,
												// visible: this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1']),
												visible: false, // TODO
											},
											{
												text: 'Formation',
												iconCls: 'x-fas fa-graduation-cap',
												routeId: 'formation',
												leaf: false,
												expanded: false,
												selectable: false,
												visible: this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1', '2', '3']),
												children: [
													{
														text: 'Formateurs',
														iconCls: 'x-fas fa-chalkboard-teacher',
														viewType: 'formateurMainContainer',
														routeId: 'formateur',
														leaf: true,
													},
													{
														text: 'Catalogues',
														iconCls: 'x-fas fa-layer-group',
														viewType: 'catalogueMainContainer',
														routeId: 'catalogue',
														leaf: true,
													},
													{
														text: 'Planning de formation',
														iconCls: 'x-fas fa-calendar-alt',
														viewType: 'formationMainContainer',
														routeId: 'planning-formation',
														leaf: true,
														listeners: {
															click: () => {
																console.log('clicked');
															},
														},
													},
													{
														text: 'Stagiaires',
														iconCls: 'x-fas fa-user-graduate',
														viewType: 'participantMainContainer',
														routeId: 'stagiaire',
														leaf: true,
													},
													{
														text: 'Présence et évaluation',
														iconCls: 'x-fas fa-tasks',
														viewType: 'evaluationMainContainer',
														routeId: 'evaluation',
														leaf: true,
													},
													{
														text: 'Certification',
														iconCls: 'x-fas fa-award',
														viewType: 'page404',
														routeId: 'certification',
														leaf: true,
													},
												],
											},
											{
												text: 'Rapports',
												iconCls: 'x-fas fa-chart-area',
												viewType: 'rapportMainContainer',
												routeId: 'rapports',
												leaf: true,
												visible: this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1', '2']),
											},
											{
												text: 'Simulations sur salaire',
												iconCls: 'x-fa fa-calculator',
												viewType: 'simulationMainContainer',
												routeId: 'simulation',
												leaf: true,
												visible: this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1']),
											},
											{
												text: 'Utilisateurs',
												iconCls: 'x-fas fa-users-cog',
												viewType: 'utilisateurMainContainer',
												routeId: 'utilisateur',
												leaf: true,
												visible: this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1']),
											},
										],
									},
								},
								width: 250,
								expanderFirst: false,
								expanderOnly: false,
								listeners: {
									selectionchange: (tree, node) => {
										const mainCardPanel = Ext.getCmp('contentPanelid');
										mainCardPanel.mask('Chargement de l\'interface...');
										this.getController('mainController').onNavigationTreeSelectionChange(tree, node);
									},
								},
							},
						],
					},					
					{
						xtype: 'container',
						flex: 1,
						reference: 'mainCardPanel',
						cls: 'sencha-dash-right-main-container',
						style: {
							borderColor: '#f4f4f4',
							borderStyle: 'solid',
							backgroundColor: '#fff',
						},
						itemId: 'contentPanel',
						id: 'contentPanelid',
						layout: {
							type: 'card',
							anchor: '100%',
						},
					},
				],
			},
		];

		this.callParent();
	},

	privileges(typeUtilisateur, valeursPossibles) {
		const found = valeursPossibles.indexOf(typeUtilisateur);
		
		return found !== -1;
	},
});
