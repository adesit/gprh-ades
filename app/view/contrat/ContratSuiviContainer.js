Ext.define('Gprh.ades.view.contrat.ContratSuiviContainer', {
	extend: 'Ext.container.Container',
	xtype: 'contratSuiviContainer',
	itemId: 'contratSuiviContainerId',

	requires: [
		'Gprh.ades.util.PagingWithSearch',
		'Gprh.ades.store.Contrat',
	],

	controller: 'contratController',

	viewModel: {
		formulas: {
			isEnabledEvaluation: get => get('comboboxvalue') !== 1,
			isEnabledAnciennete: get => get('comboboxvalue') !== 2,
			isEnabledAnniversaire: get => get('comboboxvalue') !== 3,
		},
	},
	
	initComponent() {
		const states = Ext.create('Ext.data.Store', {
			fields: ['inputField', 'displayText'],
			data: [
				{ inputField: 1, displayText: 'Contrats en CDD' },
				{ inputField: 2, displayText: 'Ancienneté plus de 5ans' },
				{ inputField: 3, displayText: 'Anniversaires de ce mois' },
			],
		});

		const evaluationRenderer = (value, meta) => {
			if (!Ext.isEmpty(value)) {
				const colors = ['#CD6155', '#E6B0AA', '#F5B7B1', '#D7BDE2', '#D2B4DE', '#A9CCE3', '#AED6F1', '#A3E4D7', '#A2D9CE', '#A9DFBF', '#ABEBC6', '#58D68D', '#F9E79F', '#FAD7A0', '#F5CBA7', '#EDBB99', '#E59866', '#F7F9F9', '#E5E7E9', '#D5DBDB', '#CCD1D1', '#AEB6BF', '#ABB2B9', '#808B96', '#2C3E50'];
				
				const values = new Date(value);
				const differenceMois = values.getMonth() - new Date().getMonth() + (12 * (values.getFullYear() - new Date().getFullYear()));
				const color = colors[12 + differenceMois];
				meta.style = `background-color:${color};`;
				return Gprh.ades.util.Globals.rendererDate(value);
			}
			return '';
		};
		const ancienneteRenderer = (value, meta) => {
			if (!Ext.isEmpty(value)) {
				const colors = ['#CD6155', '#E6B0AA', '#F5B7B1', '#D7BDE2', '#D2B4DE', '#A9CCE3', '#AED6F1', '#A3E4D7', '#A2D9CE', '#A9DFBF', '#ABEBC6', '#58D68D', '#F9E79F', '#FAD7A0', '#F5CBA7', '#EDBB99', '#E59866', '#F7F9F9', '#E5E7E9', '#D5DBDB', '#CCD1D1', '#AEB6BF', '#ABB2B9', '#808B96', '#2C3E50'];
				const color = colors[value];
				meta.style = `background-color:${color};`;
				return value;
			}
			return '';
		};
		const anniversaireRenderer = (value, meta) => {
			const date1 = value.split('/');
			const newDate = `${date1[1]}/${date1[0]}/${date1[2]}`;
			const values = new Date(newDate);
			if (!Ext.isEmpty(values)) {
				const generate = (r, g, b) => {
					const red = r % 256;
					const green = g % 256;
					const blue = b % 256;
					return `rgb(${red + 33}, ${green + 33}, ${blue + 33})`;
				};
				const differenceJours = (Math.round((values - new Date()) / (1000 * 60 * 60 * 24))) - 1;
				const rouge = (31 + differenceJours) * 2.5;
				const color = (generate(rouge, 0, 0));
				
				meta.style = `background-color:${color};color:#fff`;
				return value;
			}
			return '';
		};

		this.items = [{
			xtype: 'gridpanel',
			itemId: 'contratSuiviGridId',
			id: 'contratSuiviGridid',
			reference: 'contratSuiviGridRef',
			store: Ext.create('Gprh.ades.store.ContratSuivi', {
				proxy: {
					extraParams: {
						infoRequest: 1,
						typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
						idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
						page: 1,
						start: 1,
						limit: 25,
					},
				},
				filters: [
					{
						property: 'actifContrat',
						value: true,
					},
					{
						filterFn: item => (!Ext.isEmpty(item.get('evaluationUnAn')) || !Ext.isEmpty(item.get('evaluationDeuxAn')) || item.get('anciennetePlusCinqAn') >= 5 || (!Ext.isEmpty(item.get('anniversaire')))),
					},
				],
			}),
			viewConfig: {
				preserveScrollOnRefresh: true,
				stripeRows: true,
			},
			scrollable: true,
			selModel: 'rowmodel',
			listeners: {
				rowdblclick: 'onRowContratDblClick',
			},
			plugins: 'gridfilters',
			height: Ext.Element.getViewportHeight() - 75,
			minWidth: 750,
			columnLines: true,
			columns: {
				defaults: {
					align: 'left',
				},
				items: [
					{
						xtype: 'gridcolumn',
						dataIndex: 'sexe',
						text: 'Genre',
						maxWidth: 75,
						renderer: value => (value === 'Masculin' ? '<span class="x-fa fa-male"></span>' : '<span class="x-fa fa-female"></span>'),
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'dateEmbauche',
						text: 'Embauché(e) le',
						width: 125,
					},
					{
						xtype: 'datecolumn',
						dataIndex: 'evaluationUnAn',
						text: 'Evaluation 1an',
						width: 150,
						renderer: evaluationRenderer,
						filter: {
							type: 'date',
						},
						hidden: true,
						bind: {
							hidden: '{isEnabledEvaluation}',
						},
					},
					{
						xtype: 'datecolumn',
						dataIndex: 'evaluationDeuxAn',
						text: 'Evaluation 2ans',
						width: 125,
						renderer: evaluationRenderer,
						filter: {
							type: 'date',
						},
						hidden: true,
						bind: {
							hidden: '{isEnabledEvaluation}',
						},
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'anciennetePlusCinqAn',
						text: 'Ancienneté (ans)',
						width: 150,
						renderer: ancienneteRenderer,
						filter: {
							type: 'list',
						},
						hidden: true,
						bind: {
							hidden: '{isEnabledAnciennete}',
						},
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'anniversaire',
						text: 'Anniversaire',
						width: 160,
						renderer: anniversaireRenderer,
						filter: {
							type: 'date',
						},
						hidden: true,
						bind: {
							hidden: '{isEnabledAnniversaire}',
						},
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'abreviationCentre',
						text: 'Centre',
						width: 100,
						filter: {
							type: 'list',
						},
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'nomPrenom',
						text: 'Noms et prénoms',
						width: 300,
						filter: {
							type: 'string',
						},
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'titrePoste',
						text: 'Fonction/Poste occupé',
						width: 350,
						filter: {
							type: 'list',
							idField: 'titrePoste',
							labelField: 'titrePoste',
							store: this.getViewModel().getStore('titrePosteFilter'),
						},
					},
				],
			},
			dockedItems: [
				Gprh.ades.util.Globals.getPagingToolbar(this),
			],
			tbar: [
				'->',
				{
					tooltip: 'Exporter sous Excel',
					iconCls: 'x-fa fa-file-excel-o',
					text: 'Exporter',
					ui: 'soft-green-small',
					itemId: 'exportSuiviBtnId',
					margin: '0 5 5 5',
					// handler: () => this.getController('contratController').downloadExcelXml(true, 'Test'),
					handler: () => this.getController('contratController').exportExcelSuiviContrat(),
				},
				{
					xtype: 'combobox',
					itemId: 'filtresContratId',
					fieldLabel: 'Filtre',
					reference: 'filtresContratRef',
					store: states,
					queryMode: 'local',
					displayField: 'displayText',
					valueField: 'inputField',
					renderTo: Ext.getBody(),
					labelWidth: 50,
					width: 275,
					bind: {
						value: '{comboboxvalue}',
					},
					style: {
						backgroundColor: '#ccc',
					},
					listeners: {
						change: (chkbx, newValue) => {
							const store = Ext.getCmp('contratSuiviGridid').getStore();							
							if (newValue === 1) { // Contrats en CDD
								store.clearFilter();
								store.filter([
									{
										filterFn: item => (!Ext.isEmpty(item.get('evaluationUnAn')) || !Ext.isEmpty(item.get('evaluationDeuxAn'))),
									},
									{
										property: 'actifContrat',
										value: true,
									},
								]);
								store.sort([
									{
										property: 'evaluationUnAn',
										direction: 'ASC',
									},
								]);
							} else if (newValue === 2) { // Employés depuis plus de 5ans
								store.clearFilter();
								store.filter([
									{
										filterFn: item => (item.get('anciennetePlusCinqAn') >= 5),
									},
								]);
								store.sort([
									{
										property: 'anciennetePlusCinqAn',
										direction: 'DESC',
									},
								]);
							} else if (newValue === 3) { // Anniversaire dans ce mois
								store.clearFilter();
								store.filter([
									{
										filterFn: (item) => {
											return !Ext.isEmpty(item.get('anniversaire'));
										},
									},
								]);
								store.sort([
									{
										property: 'anniversaire',
										direction: 'DESC',
									},
								]);
							}
						},
					},
				},
			],
		}];

		this.callParent();
	},
});
