Ext.define('Gprh.ades.view.contrat.ContratViewModel', {
	extend: 'Ext.app.ViewModel',

	alias: 'viewmodel.contratViewModel',

	requires: [
		'Gprh.ades.store.Centre',
		'Gprh.ades.store.Poste',
		'Gprh.ades.store.Section',
		'Gprh.ades.store.Departement',
		'Gprh.ades.store.Categorie',
		'Gprh.ades.store.Smie',
		'Gprh.ades.store.TypeContrat',
		'Gprh.ades.store.NatureContrat',
		'Gprh.ades.store.ModePaiement',
	],

	stores: {
		centreResults: {
			type: 'centreStore',
		},
		posteResults: {
			type: 'posteStore',
			proxy: {
				extraParams: {
					infoRequest: 2,
				},
			},
		},
		sectionResults: {
			type: 'sectionStore',
		},
		departementResults: {
			type: 'departementStore',
		},
		categorieResults: {
			type: 'categorieStore',
		},
		smieResults: {
			type: 'smieStore',
		},
		typeContratResults: {
			type: 'typeContratStore',
		},
		natureContratResults: {
			type: 'natureContratStore',
		},
		modePaiementResults: {
			type: 'modePaiementStore',
		},
	},
});
