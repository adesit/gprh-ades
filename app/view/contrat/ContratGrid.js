Ext.define('Gprh.ades.view.contrat.ContratGrid', {
	extend: 'Ext.panel.Panel',
	xtype: 'contratGrid',
	itemId: 'contratPanelId',

	requires: [
		'Gprh.ades.util.PagingWithSearch',
		'Gprh.ades.store.Contrat',
	],

	controller: 'contratController',

	viewModel: {
		stores: {
			contratResults: {
				type: 'contratStore',
				storeId: 'contratResultsId',
				proxy: {
					extraParams: {
						infoRequest: 1,
						typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
						idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
					},
				},
			},
			titrePosteFilter: {
				source: 'contratResultsId',
				sorters: [{
					property: 'titrePoste',
					direction: 'ASC',
				}],
			},
			categorieReelleFilter: {
				source: 'contratResultsId',
				sorters: [{
					property: 'categorieReelle',
					direction: 'ASC',
				}],
			},
		},
	},
	
	initComponent() {
		this.items = [{
			xtype: 'gridpanel',
			itemId: 'contratGridId',
			reference: 'contratGridRef',
			bind: {
				store: '{contratResults}',
			},
			viewConfig: {
				preserveScrollOnRefresh: true,
				stripeRows: true,
			},
			scrollable: true,
			selModel: 'rowmodel',
			listeners: {
				rowdblclick: 'onRowContratDblClick',
			},
			plugins: 'gridfilters',
			height: Ext.Element.getViewportHeight() - 75,
			minWidth: 750,
			columnLines: true,
			columns: {
				defaults: {
					align: 'left',
				},
				items: [
					{
						xtype: 'gridcolumn',
						dataIndex: 'sexe',
						text: 'Genre',
						maxWidth: 40,
						renderer: value => (value === '1' ? '<span class="x-fa fa-male"></span>' : '<span class="x-fa fa-female"></span>'),
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'flotte',
						text: 'Flotte',
						maxWidth: 75,
						renderer: value => (!Ext.isEmpty(value) ? `<span class="x-fas fa-mobile-alt" data-qtitle="Numéro de flotte:" data-qwidth="200" data-qtip="${value}"></span>` : ''),
						align: 'center',
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'idContrat',
						text: '#',
						hidden: true,
						width: 75,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'abreviationCentre',
						text: 'Centre',
						width: 100,
						filter: {
							type: 'list',
						},
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'matricule',
						text: 'Matricule',
						width: 100,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'nomPrenom',
						text: 'Noms et prénoms',
						width: 300,
						filter: {
							type: 'string',
						},
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'titrePoste',
						text: 'Fonction/Poste occupé',
						width: 350,
						filter: {
							type: 'list',
							idField: 'titrePoste',
							labelField: 'titrePoste',
							store: this.getViewModel().getStore('titrePosteFilter'),
						},
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'categorieReelle',
						text: 'Catégorie',
						width: 125,
						filter: {
							type: 'list',
							idField: 'categorieReelle',
							labelField: 'categorieReelle',
							store: this.getViewModel().getStore('categorieReelleFilter'),
						},
					},
					{
						xtype: 'datecolumn',
						dataIndex: 'dateEmbauche',
						text: 'Embauché(e) le',
						width: 125,
						renderer: value => Gprh.ades.util.Globals.rendererDate(value),
					},
					{
						xtype: 'datecolumn',
						dataIndex: 'datePrisePoste',
						text: 'Prise de poste',
						width: 125,
						renderer: value => Gprh.ades.util.Globals.rendererDate(value),
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'nbEnfant',
						text: '# Enfants',
						filter: {
							type: 'numeric',
							itemDefaults: {
								emptyText: 'Entrez un nombre',
								listeners: {
									afterrender: () => {
										const els = Ext.select('div.x-form-trigger.x-form-trigger-default.x-form-trigger-spinner.x-form-trigger-spinner-default.x-unselectable', true);
										els.removeCls('x-form-trigger-default');
									},
								},
							},				
						},
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'primeExceptionnelle',
						text: 'Prime exceptionnelle',
						renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'actifContrat',
						text: 'En cours',
						renderer: value => (value ? 'Oui' : ''),
						filter: {
							type: 'list',
						},
					},
				],
			},
			dockedItems: [
				Gprh.ades.util.Globals.getPagingToolbar(this),
			],
			tbar: [
				{
					tooltip: 'Créer un nouveau contrat ou un avenant',
					iconCls: 'x-fas fa-file-signature',
					text: 'Créer',
					ui: 'soft-green-small',
					itemId: 'addContratGridBtnId',
					margin: '0 0 10 10',
					listeners: {
						click: 'onEditContratClick',
					},
					params: {
						gridId: 'contratGridId',
						editOrCreate: 'create',
					},
					bind: { 
						disabled: '{!contratGridRef.selection}',
					},
					hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
				},
				{
					text: 'Modifier',
					tooltip: 'Modifier un contrat',
					iconCls: 'fa fa-cog',
					ui: 'soft-blue',
					itemId: 'editContratGridBtnId',
					margin: '0 0 10 10',
					listeners: {
						click: 'onEditContratClick',
					},
					params: {
						gridId: 'contratGridId',
						editOrCreate: 'edit',
					},
					bind: { 
						disabled: '{!contratGridRef.selection}',
					},
				},
				{
					xtype: 'panel',
					baseCls: 'x-toast-info',
					margin: '5 0 0 10',
					html: 'Double clic dessus pour voir les détails.',
				},
				/* {
					tooltip: 'Rompre un contrat',
					iconCls: 'x-far fa-file-archive',
					text: 'Solde de tout compte',
					ui: 'soft-red-small',
					itemId: 'closeContratGridBtnId',
					margin: '0 0 10 10',
					listeners: {
						click: 'onCloseContratClick',
					},
					params: {
						gridId: 'contratGridId',
						editOrCreate: 'close',
					},
					bind: { 
						disabled: '{!contratGridRef.selection}',
					},
					hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
				}, */
				'->',
				{
					tooltip: 'Exporter en Excel le personnel',
					iconCls: 'x-fa fa-file-excel-o',
					text: 'Liste de personnel',
					ui: 'soft-green',
					itemId: 'printContratsXlsxBtnId',
					margin: '0 0 10 10',
					listeners: {
						click: 'onPrintXlsxContratClick',
					},
					params: {
						gridId: 'paieGridId',
					},
				},
				{
					text: 'Supprimer',
					iconCls: 'fa fa-trash',
					ui: 'soft-red-small',
					listeners: {
						click: 'onDeleteContrat',
					},
					hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
				},
			],
		}];

		this.callParent();
	},
});
