Ext.define('Gprh.ades.view.contrat.ContratFormPrint', {
	extend: 'Ext.panel.Panel',
	xtype: 'contratFormPrint',
	itemId: 'contratFormPrintId',

	requires: [
		'Gprh.ades.util.VTypes',
	],

	viewModel: 'salaireViewModel',
	
	initComponent() {
		const actifContrat = Ext.isEmpty(this.parameters) ? null : this.parameters[0].findRecord('actifContrat', true); // le contrat actuel de l'employé
		
		const editOrCreate = Ext.isEmpty(this.parameters) ? null : this.parameters[2];

		const nouveauAvenant = (!Ext.isEmpty(actifContrat) || editOrCreate === 'create') ? 'Avenant' : 'Original';
				
		const contratData = (Ext.isEmpty(actifContrat) || editOrCreate === 'create') ? '' : actifContrat.data;
				
		const contratFormContainer = {
			xtype: 'form',
			itemId: 'contratFormContainerId',
			layout: 'column',
			items: [
				{
					defaultType: 'displayfield',
					defaults: {
						labelWidth: 150,
						labelAlign: 'left',
						labelSeparator: ':',
					},
					columnWidth: '0.50',
					items: [
						{
							name: 'idContrat',
							itemId: 'idContratId',
							fieldLabel: '# Contrat',
							hidden: true,
							value: Ext.isEmpty(contratData) ? '' : contratData.idContrat,
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							name: 'idEmploye',
							itemId: 'idEmployeId',
							fieldLabel: '# Employé',
							hidden: true,
							value: Ext.isEmpty(contratData) ? '' : contratData.idEmploye,
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							name: 'initialAvenant',
							itemId: 'initialAvenantId',
							fieldLabel: 'Original ou avenant',
							value: Ext.isEmpty(contratData) ? nouveauAvenant : contratData.initialAvenant,
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							xtype: 'combobox',
							name: 'centreField',
							itemId: 'centreFieldId',
							reference: 'centreFieldReference',
							displayField: 'nomCentre',
							valueField: 'idCentre',
							bind: {
								store: '{centreResults}',
								value: Ext.isEmpty(contratData) ? '' : contratData.idCentre,
							},
							hidden: true,
						},
						{
							name: 'idCentre',
							itemId: 'idCentreId',
							fieldLabel: 'Centre d\'affectation',
							fieldStyle: {
								minHeight: '14px',
							},
							bind: {
								value: '{centreFieldReference.selection.nomCentre}',
							},
						},
						{
							name: 'matricule',
							itemId: 'matriculeId',
							fieldLabel: 'Matricule',
							value: Ext.isEmpty(contratData) ? '' : contratData.matricule,
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							xtype: 'combobox',
							name: 'departementField',
							itemId: 'departementFieldId',
							reference: 'departementFieldReference',
							displayField: 'nomDepartement',
							valueField: 'idDepartement',
							bind: {
								store: '{departementResults}',
								value: Ext.isEmpty(contratData) ? '' : contratData.idDepartement,
							},
							hidden: true,
						},
						{
							name: 'idDepartement',
							itemId: 'idDepartement',
							fieldLabel: 'Département',
							fieldStyle: {
								minHeight: '14px',
							},
							bind: {
								value: '{departementFieldReference.selection.nomDepartement}',
							},
						},
						{
							xtype: 'combobox',
							name: 'sectionField',
							itemId: 'sectionFieldId',
							reference: 'sectionFieldReference',
							displayField: 'nomSection',
							valueField: 'idSection',
							bind: {
								store: '{sectionResults}',
								value: Ext.isEmpty(contratData) ? '' : contratData.idSection,
							},
							hidden: true,
						},
						{
							name: 'idSection',
							itemId: 'idSectionId',
							fieldLabel: 'Section de service',
							fieldStyle: {
								minHeight: '14px',
							},
							bind: {
								value: '{sectionFieldReference.selection.nomSection}',
							},
						},
						{
							name: 'titrePoste',
							itemId: 'titrePosteId',
							fieldLabel: 'Poste occupé',
							value: Ext.isEmpty(contratData) ? '' : contratData.titrePoste,
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							name: 'categorieAnciennete',
							itemId: 'categorieAncienneteId',
							fieldLabel: 'Cat. professionnelle',
							value: Ext.isEmpty(contratData) ? '' : contratData.categorieAnciennete,
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							fieldLabel: 'S. base du grille',
							name: 'salaireBaseGrille',
							itemId: 'salaireBaseGrilleId',
							value: Ext.isEmpty(contratData) ? '' : Gprh.ades.util.Globals.formatRenderNumber(contratData.salaireBaseGrille),
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							fieldLabel: 'S. base conclu',
							name: 'salaireBase',
							itemId: 'salaireBaseId',
							value: Ext.isEmpty(contratData) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(contratData.salaireBase),
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							fieldLabel: 'Prime exceptionnel',
							name: 'primeExceptionnelle',
							itemId: 'primeExceptionnelleId',
							value: Ext.isEmpty(contratData) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(contratData.primeExceptionnelle),
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							name: 'heureReglementaire',
							itemId: 'heureReglementaireId',
							fieldLabel: 'Heure règlementaire',
							value: Ext.isEmpty(contratData) ? '' : contratData.heureReglementaire,
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							fieldLabel: 'Taux horaire',
							name: 'tauxHoraire',
							itemId: 'tauxHoraireId',
							value: Ext.isEmpty(contratData) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(contratData.tauxHoraire),
							fieldStyle: {
								minHeight: '14px',
							},
						},						
					],
				}, {
					defaultType: 'displayfield',
					defaults: {
						labelWidth: 150,
						labelAlign: 'left',
						labelSeparator: ':',
						anchor: '100%',
					},
					columnWidth: '0.50',
					items: [
						{
							xtype: 'combobox',
							name: 'typeContratField',
							reference: 'typeContratFieldReference',
							displayField: 'displayText',
							valueField: 'typeContrat',
							bind: {
								store: '{typeContratResults}',
								value: Ext.isEmpty(contratData) ? '' : contratData.typeContrat,
							},
							hidden: true,
						},
						{
							name: 'typeContrat',
							itemId: 'typeContratId',
							fieldLabel: 'Type de contrat',
							fieldStyle: {
								minHeight: '14px',
							},
							bind: {
								value: '{typeContratFieldReference.selection.displayText}',
							},
						},
						{
							xtype: 'combobox',
							name: 'natureContratField',
							reference: 'natureContratFieldReference',
							displayField: 'displayText',
							valueField: 'natureContrat',
							bind: {
								store: '{natureContratResults}',
								value: Ext.isEmpty(contratData) ? '' : contratData.natureContrat,
							},
							hidden: true,
						},
						{
							name: 'natureContrat',
							itemId: 'natureContratId',
							fieldLabel: 'Nature du contrat',
							fieldStyle: {
								minHeight: '14px',
							},
							bind: {
								value: '{natureContratFieldReference.selection.displayText}',
							},
						},
						{
							name: 'dateEmbauche',
							itemId: 'dateEmbaucheId',
							fieldLabel: 'Date d\'embauche',
							hidden: false,
							format: 'd/m/Y',
							minWidth: 100,
							value: Ext.isEmpty(contratData) ? Ext.Date.format(new Date(), 'd/m/Y') : Ext.Date.format(new Date(contratData.dateEmbauche), 'd/m/Y'),
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							name: 'datePrisePoste',
							itemId: 'datePrisePosteId',
							fieldLabel: 'Date de prise de poste',
							hidden: false,
							format: 'd/m/Y',
							minWidth: 100,
							value: Ext.isEmpty(contratData) ? Ext.Date.format(new Date(), 'd/m/Y') : Ext.Date.format(new Date(contratData.datePrisePoste), 'd/m/Y'),
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							xtype: 'combobox',
							name: 'smieField',
							reference: 'smieFieldReference',
							displayField: 'nomSmie',
							valueField: 'idSmie',
							bind: {
								store: '{smieResults}',
								value: Ext.isEmpty(contratData) ? '' : contratData.idSmie,
							},
							hidden: true,
						},
						{
							name: 'idSmie',
							itemId: 'idSmieId',
							fieldLabel: 'Affiliation médicale',
							fieldStyle: {
								minHeight: '14px',
							},
							bind: {
								value: '{smieFieldReference.selection.nomSmie}',
							},
						},
						{
							name: 'horaire',
							itemId: 'horaireId',
							fieldLabel: 'Horaire de travail',
							hidden: false,
							value: Ext.isEmpty(contratData) ? '07h30 : 12h00 - 14h30 : 18h00' : contratData.horaire,
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							xtype: 'combobox',
							name: 'modePaiementField',
							reference: 'modePaiementFieldReference',
							displayField: 'displayText',
							valueField: 'modePaiement',
							bind: {
								store: '{modePaiementResults}',
								value: Ext.isEmpty(contratData) ? '' : contratData.modePaiement,
							},
							hidden: true,
						},
						{
							name: 'modePaiement',
							itemId: 'modePaiementId',
							fieldLabel: 'Mode de paiement',
							fieldStyle: {
								minHeight: '14px',
							},
							bind: {
								value: '{modePaiementFieldReference.selection.displayText}',
							},
						},
						{
							name: 'periodicitePaiement',
							itemId: 'periodicitePaiementId',
							fieldLabel: 'Paiement tous les',
							value: Ext.isEmpty(contratData) ? '25 du mois' : contratData.periodicitePaiement,
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							name: 'dateExpirationContrat',
							itemId: 'dateExpirationContratId',
							fieldLabel: 'Prévue fin de contrat',
							format: 'd/m/Y',
							submitFormat: 'Y-m-d',
							value: Ext.isEmpty(contratData.dateExpirationContrat) ? '' : Ext.Date.format(new Date(contratData.dateExpirationContrat), 'd/m/Y'),
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							fieldLabel: 'Contrat en cours',
							name: 'actifContrat',
							itemId: 'actifContratId',
							value: Ext.isEmpty(contratData) || !(contratData.actifContrat) ? 'Non' : 'Oui',
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							// Affiché si fin de la collaboration, envoyer des paramètres lors du clic de btn Terminer le contrat
							name: 'motifResiliationContrat',
							itemId: 'motifResiliationContratId',
							fieldLabel: 'Motif de résiliation',
							value: Ext.isEmpty(contratData) ? '' : contratData.motifResiliationContrat,
							fieldStyle: {
								minHeight: '14px',
							},
						},
						{
							// Affiché si fin de la collaboration, envoyer des paramètres lors du clic de btn Terminer le contrat
							name: 'dateDebauche',
							itemId: 'dateDebaucheId',
							fieldLabel: 'Date de débauche',
							format: 'd/m/Y',
							submitFormat: 'Y-m-d',
							value: Ext.isEmpty(contratData.dateDebauche) ? '' : Ext.Date.format(new Date(contratData.dateDebauche), 'd/m/Y'),
							fieldStyle: {
								minHeight: '14px',
							},
						},
					],
				},
			],
		};
		
		this.items = [
			{
				html: `<center><h3>${contratData.matricule}: ${contratData.nom} ${contratData.prenom}</h1></center>`,
			},
			contratFormContainer,
		];

		this.callParent();
	},

});
