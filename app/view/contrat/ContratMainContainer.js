Ext.define('Gprh.ades.view.contrat.ContratMainContainer', {
	extend: 'Ext.container.Container',

	xtype: 'contratMainContainer',

	requires: [
		'Gprh.ades.view.contrat.ContratController',
		'Gprh.ades.view.contrat.ContratGrid',
		'Gprh.ades.view.contrat.ContratViewModel',
		'Gprh.ades.view.contrat.ContratSuiviContainer',
	],
	
	viewModel: {
		type: 'contratViewModel',
	},

	controller: 'contratController',

	itemId: 'contratMainContainerId',

	layout: {
		type: 'hbox',
		align: 'stretch',
	},

	items: [
		{
			xtype: 'panel',
			itemId: 'contentPanelContrat',
			flex: 1,
			layout: {
				type: 'anchor',
				anchor: '100%',
			},
			items: [
				{
					xtype: 'contratGrid',
				},
			],
		},
	],
});
