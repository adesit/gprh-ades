Ext.define('Gprh.ades.view.contrat.ContratController', {
	extend: 'Ext.app.ViewController',
	
	alias: 'controller.contratController',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.view.contrat.ContratForm',
		'Gprh.ades.store.EffectifTotal',
		'Gprh.ades.store.Effectif',
		'Gprh.ades.store.Centre',
		'Gprh.ades.store.SalaireBase',
		'Gprh.ades.util.Downloader',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	init() {
		// this.setCurrentView('contratGrid');
	},

	setCurrentView(view, params) {
		const panel = this.getView().up('#contentPanelContrat');
		const contentPanel = !(panel) ? this.getView().down('#contentPanelContrat') : panel;
		// We skip rendering for the following scenarios:
		// * There is no contentPanel
		// * view xtype is not specified
		// * current view is the same
		if (!contentPanel || view === '' || (contentPanel.down() && contentPanel.down().xtype === view)) {
			return false;
		}

		if (params && params.openWindow) {
			const cfg = Ext.apply({
				xtype: 'prototypeWindow',
				items: [
					Ext.apply({
						xtype: view,
					}, params.targetCfg),
				],
			}, params.windowCfg);

			Ext.create(cfg);
		} else {
			Ext.suspendLayouts();

			contentPanel.removeAll(true);
			contentPanel.add(
				Ext.apply({
					xtype: view,
				}, {
					parameters: params,
				})
			);

			Ext.resumeLayouts(true);
		}
		return true;
	},

	async editContrat (record, editOrCreate) {
		const extraParameters = {
			extraParams: {
				infoRequest: 2,
				idEmploye: record.data.idEmploye,
			},
		};

		// Chercher tous les contrats d'un employé par son idEmploye
		const inputContratData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Contrat', extraParameters);
		const posteStore = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Poste', {
			extraParams: {
				infoRequest: 2,
			},
		});

		const newValue = Ext.isEmpty(inputContratData.findRecord('actifContrat', true)) ? {} : inputContratData.findRecord('actifContrat', true).data.idPoste;		

		this.setCurrentView('contratForm', [inputContratData, record.data, this.newCategorieStore(posteStore, newValue), editOrCreate]);
	},
	
	onRowContratDblClick(view, record) {
		this.editContrat(record);
	},

	onEditContratClick(parameters) {
		const grid = this.getView().down(`#${parameters.params.gridId}`);
		const selection = grid.getSelection();
		
		if (selection.length > 0) {
			this.editContrat(selection[0], parameters.params.editOrCreate);			
		} else Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.selectRowMsg);
	},

	onBackListContrat() {
		if (this.type === 'personnelController') {
			this.setCurrentView('personnelGrid');
			const navigationTreeList = this.getView().up('#main-view-detail-wrap').down('#navigationTreeList');
			const personnelNode = navigationTreeList.itemMap[4].getNode();

			navigationTreeList.fireEvent('selectionchange', null, personnelNode);
		} else if (this.type === 'contratController') {			
			this.setCurrentView('contratGrid');
		}
	},

	onPreviousPersonnelClick() {
		const navigationTreeList = this.getView().up('#main-view-detail-wrap').down('#navigationTreeList');
		const personnelNode = navigationTreeList.itemMap[3].getNode();

		navigationTreeList.fireEvent('selectionchange', null, personnelNode);
	},

	cancelContratAction(formId) {
		let form;
		if (this.type === 'personnelController') {
			form = this.getView().down('#contratFormContainerId');
		} else if (this.type === 'contratController') {			
			form = this.getView().down('#contentPanelContrat').down(`#${formId.params}`);
		}
		
		form.reset();
	},

	async changeMatricule(view, newValue, oldValue, eOpts) {
		const nationalResult = await Gprh.ades.util.Globals.requestOneRecordSettings('Gprh.ades.store.EffectifTotal', null);
		const nationalEffectif = parseInt(nationalResult[0].get('totalEffectif')) + 1;

		const centreEffectifStore = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Effectif', null);
		const centreEffectifFind = centreEffectifStore.findRecord('idCentre', newValue);
		const dernierEffectifCentre = Ext.isEmpty(centreEffectifFind) ? 1 : parseInt(centreEffectifFind.get('effectif')) + 1;

		const centreStore = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Centre', null);
		const centreFind = centreStore.findRecord('idCentre', newValue);
		const abreviationCentre = Ext.isEmpty(centreFind) ? '' : centreFind.get('abreviationCentre');;

		const matriculeField = this.getView().down('#matriculeId');
		matriculeField.setValue(`${abreviationCentre}.${dernierEffectifCentre}.${nationalEffectif}`);
	},

	async chargeCategorie(view, newValue, oldValue, eOpts) {
		const extraParameters = {
			extraParams: {
				infoRequest: 2,
			},
		};
		const posteStore = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Poste', extraParameters);
		
		const findPosteLine = posteStore.findRecord('idPoste', newValue);
		const heureReglementaireField = this.getView().down('#heureReglementaireId');
		const HRegValue = Ext.isEmpty(findPosteLine) ? 0 : findPosteLine.data.tauxHoraire;
		heureReglementaireField.setValue(HRegValue);

		const categorieCombobox = this.getView().down('#categorieAncienneteId');
		categorieCombobox.setStore(this.newCategorieStore(posteStore, newValue, heureReglementaireField));
	},

	newCategorieStore(posteStore, newValue, heureReglementaireField) {
		const findPosteLine = posteStore.findRecord('idPoste', newValue);
		
		const obj = Ext.isEmpty(findPosteLine) ? {} : findPosteLine.data;

		// Créer un nouveau store pour le combobox "catégorie professionnelle"
		delete obj.descriptionPoste;
		delete obj.id;
		delete obj.idPoste;
		delete obj.titrePoste;
		delete obj.tauxHoraire;
		const arr = (Array.from(Object.keys(obj), k=>obj[k]));
		
		const arrMapped = arr.map((currElement, index) => {
			return '(' + currElement + ') [' + index + '-' + (parseInt(index) + 1) + '[ans';
		});

		const newArray = [];
		Ext.each(arrMapped, (record) => {
			const ligne = {};
			ligne.categorieProfessionnelle = record;
			ligne.idCategorie = record;
			newArray.push(ligne);
		});

		
		return newStore = new Ext.data.Store({
			data: newArray,
		});
	},

	/* Récupère la valeur de la catégorie et l'intervalle de l'ancienneté.
	 * Cherche le salaire de base proportionnel à la catégorie et l'ancienneté.
	 * Attribut cette valeur dans la zone de texte pour le salaire de base
	 */ 
	async onCategorieComboChange(view, newValue, oldValue, eOpts) {
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
			},
		};
		const grilleStore = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.SalaireBase', extraParameters);
		
		const categorie = Ext.isEmpty(newValue) ? '' : /\(([^)]*)\)/.exec(newValue)[1];
		 // Cherche la ligne d'une grille pour une catégorie donnée
		const findGrilleLine = grilleStore.findRecord('categorieProfessionnelle', categorie);

		 // Récupère l'intervalle d'ancienneté
		const intervalAnnees = Ext.isEmpty(newValue) ? '0-1' : /\[([^)]*)\[/.exec(newValue)[1];
		
		const bornes = /([^)]*)-([^)]*)/.exec(intervalAnnees);
		const borneInf = bornes[1];
		const borneSup = bornes[2];

		let salaireBase = 0 ;
		// Test l'ancienneté et récupère le salaire de base correspondant à cette durée
		if (borneInf >= 0 && borneSup <= 3) {
			salaireBase = Ext.isEmpty(findGrilleLine) ? 0 : findGrilleLine.data.salaireBaseMinimum;
		} else if (borneInf >= 3 && borneSup <= 4) {
			salaireBase = Ext.isEmpty(findGrilleLine) ? 0 : findGrilleLine.data.salaireBase3;
		} else if (borneInf >= 4 && borneSup <= 5) {
			salaireBase = Ext.isEmpty(findGrilleLine) ? 0 : findGrilleLine.data.salaireBase4;
		} else if (borneInf >= 5) {
			salaireBase = Ext.isEmpty(findGrilleLine) ? 0 : findGrilleLine.data.salaireBase5;
		}

		// Attribut la valeur au zone de texte du salaire de base du grille
		const salaireBaseGrilleField = this.getView().down('#salaireBaseGrilleId');
		salaireBaseGrilleField.setValue(Gprh.ades.util.Globals.formatRenderNumber(salaireBase));
		const salaireBaseGrille = Gprh.ades.util.Globals.simpleNumber(salaireBaseGrilleField.getValue());

		// Fait le calcul entre le salaire du grille et le salaire négocié par l'employé
		const salaireConcluField = this.getView().down('#salaireBaseId');
		const salaireConclu = Gprh.ades.util.Globals.simpleNumber(salaireConcluField.getValue());
		
		// Si le salaire négocié est donné, si non ne fait rien
		if (salaireConclu > 0) {
			const difference = parseInt(salaireConclu) - parseInt(salaireBase);
			const primeExceptionnelleField = this.getView().down('#primeExceptionnelleId');

			// Mettre à jour le 'salaire supplémentaire'
			if (difference > 0) { // si le salaire conclu est supérieur par rapport à la grille, mettre la différence dans le salaire supplémentaire
				primeExceptionnelleField.setValue(Gprh.ades.util.Globals.formatRenderNumber(difference));
			} else { // si le salaire conclu est inférieur à la grille, mettre celui de la grille comme salaire de base conclu
				salaireConcluField.setValue(Gprh.ades.util.Globals.formatRenderNumber(salaireBase));
				primeExceptionnelleField.setValue(Gprh.ades.util.Globals.formatRenderNumber(0));
			}
		}

		// Calcule le taux horaire
		const tauxHoraireField = this.getView().down('#tauxHoraireId');
		const heureReglementaireField = this.getView().down('#heureReglementaireId');

		const tauxHoraire = salaireBaseGrille / Gprh.ades.util.Globals.simpleNumber(heureReglementaireField.getValue());
		tauxHoraireField.setValue(Gprh.ades.util.Globals.formatRenderNumber(Math.round(tauxHoraire)));
	},

	updateSalaireFields(self) {
		Gprh.ades.util.Globals.formatNumber(self);
		const salaireBaseGrilleField = this.getView().down('#salaireBaseGrilleId');
		const salaireBaseGrille = Gprh.ades.util.Globals.simpleNumber(salaireBaseGrilleField.getValue());
		
		const salaireConcluField = this.getView().down('#salaireBaseId');
		const salaireConclu = Gprh.ades.util.Globals.simpleNumber(salaireConcluField.getValue());

		const primeExceptionnelleField = this.getView().down('#primeExceptionnelleId');
		const difference = parseInt(salaireConclu) - parseInt(salaireBaseGrille);
		/*
		// Mettre à jour le 'salaire supplémentaire'
		if (difference > 0) { // si le salaire conclu est supérieur par rapport à la grille, mettre la différence dans le salaire supplémentaire
			primeExceptionnelleField.setValue(Gprh.ades.util.Globals.formatRenderNumber(difference));
		} else { // si le salaire conclu est inférieur à la grille, mettre celui de la grille comme salaire de base conclu
			salaireConcluField.setValue(Gprh.ades.util.Globals.formatRenderNumber(salaireBaseGrille));
			primeExceptionnelleField.setValue(Gprh.ades.util.Globals.formatRenderNumber(0));
		} */

		if (difference > 0) { // si le salaire conclu est supérieur par rapport à la grille, mettre la différence dans le salaire supplémentaire
			primeExceptionnelleField.setValue(Gprh.ades.util.Globals.formatRenderNumber(difference));
		} else { // si le salaire conclu est inférieur à la grille, mettre celui de la grille comme salaire de base conclu
			// salaireConcluField.setValue(Gprh.ades.util.Globals.formatRenderNumber(salaireBaseGrille));
			primeExceptionnelleField.setValue(Gprh.ades.util.Globals.formatRenderNumber(0));
		}

		const tauxHoraireField = this.getView().down('#tauxHoraireId');
		const heureReglementaireField = this.getView().down('#heureReglementaireId');

		const tauxHoraire = salaireBaseGrille / Gprh.ades.util.Globals.simpleNumber(heureReglementaireField.getValue());
		tauxHoraireField.setValue(Gprh.ades.util.Globals.formatRenderNumber(Math.round(tauxHoraire)));
	},

	onPersonnelEditClick(parameters) {
		const personnelController = Ext.create('Gprh.ades.view.personnel.PersonnelController');
		personnelController.chargePersonnelDetails(this, parameters.params);
	},

	onContratSaveClick() {
		const form = this.getView().down('#contratFormContainerId');
		const grid = this.getView().down('#contratByPersonnelId');
		
		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/contrat/Contrat.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			grid.getStore().getProxy().setExtraParams({
				page: 1,
				start:1,
				limit: 25,
				infoRequest: 2,
				idEmploye: form.down('#idEmployeId').getValue(),
			});
			grid.getStore().load();
		}
	},

	confirmActifContrat(view, newValue, oldValue, eOpts) {
		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxContratChecked',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const contratParams = {
						idEmploye: view.params,
					};
					const modelUpdLigneActif = {
						infoRequest: 4,
						data: contratParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/contrat/Contrat.php', modelUpdLigneActif);
					
					if (saveOrUpdate.request.responseText >= 0) {
						btn.up('#messageBoxContratChecked').close();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					this.getView().down('#contratFormContainerId').down('#actifContratId').setValue(false);
					btn.up('#messageBoxContratChecked').close();
				},
			}],
		});
		
		if (newValue) {
			messageBox.show({
				title: 'Confirmer la modification',
				msg: 'Les données du contrat actuel ne seront plus utilisées et remplacées par ce formulaire. Voulez-vous continuer?',
				icon: Ext.MessageBox.WARNING,
			});
		}
	},

	onDeleteContrat() {
		const contratGrid = this.getView().down('#contratGridId');
		
		const contratParams = {
			idContrat: contratGrid.getSelection()[0].get('idContrat'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxContratDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelContrat = {
						infoRequest: 3,
						data: contratParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/contrat/Contrat.php', modelDelContrat);
					const result = saveOrUpdate.request.responseText;
					
					if (result > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						btn.up('#messageBoxContratDelete').close();
						contratGrid.getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxContratDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxContratDelete').close();
				},
			}],
		});
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	contratType(view, newValue, oldValue, eOpts) {
		const dateFinContratField = this.getView().down('#dateExpirationContratId');
		if (newValue === '1') {
			Ext.apply(dateFinContratField, {
				allowBlank: false,
			});
			dateFinContratField.focus(true);
		} else {
			Ext.apply(dateFinContratField, {
				allowBlank: true,
			});
			this.getView().down('#dateExpirationContratId').unsetActiveError();
		}
	},

	/*
	 * Cette fonction permet de récupérer le contrat en cours à solder
	 * Faire le calcul de salaire de l'employé pour ce mois
	 */

	async onCloseContratClick(parameters) {
		const grid = this.getView().down(`#${parameters.params.gridId}`);
		const selection = grid.getSelection();

		const extraParameters = {
			extraParams: {
				infoRequest: 4,
				idEmploye: selection[0].get('idEmploye'),
			},
		};

		 // Récupère le contrat actif de l'employé
		const inputContratData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Contrat', extraParameters);

		// Calul de salaire du mois en cours // TO DO
		const dateAvance = new Date();
		const month = dateAvance.getUTCMonth() + 1; //months from 1-12
		const year = dateAvance.getUTCFullYear();
		const idContratValue = Ext.isEmpty(inputContratData.data.items[0])? 0 : inputContratData.data.items[0].get('idContrat');

		if (idContratValue > 0) {
			const salaireParameters = {
				extraParams: {
					moisSalarial: year + '-' + month,
					infoRequest: 2,
					idContrat: idContratValue,
					action: 1,
				},
			};
			const salaireDataStore = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.SalaireDetails', salaireParameters);
			const salaireData = Ext.isEmpty(salaireDataStore.getData().items[0]) ? null : salaireDataStore.getData().items[0].data;
			
			this.setCurrentView('soldeMainContainer', [inputContratData, selection[0], 'edit', salaireData]);
		} else Gprh.ades.util.Helper.showError('Ce contrat n\'existe plus.');		
	},

	async onPrintXlsxContratClick() {
		Ext.getBody().mask('Téléchargement...');

		// Va exporter les données du personnel sur Excel (2007 .xlsx)
		Gprh.ades.util.Downloader.get({
			url: 'server-scripts/export/ListePersonnel.php',
			params: {
				infoRequest: 3,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			}
		});
	},

	downloadExcelXml(includeHidden, title) {
		let Base64 = (function() {
			// Private property
			let keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
		
			// Private method for UTF-8 encoding
		
			function utf8Encode(string) {
				string = string.replace(/\r\n/g, '\n');
				let utftext = '';
				for (let n = 0; n < string.length; n++) {
					let c = string.charCodeAt(n);
					if (c < 128) {
						utftext += String.fromCharCode(c);
					} else if ((c > 127) && (c < 2048)) {
						utftext += String.fromCharCode((c >> 6) | 192);
						utftext += String.fromCharCode((c & 63) | 128);
					} else {
						utftext += String.fromCharCode((c >> 12) | 224);
						utftext += String.fromCharCode(((c >> 6) & 63) | 128);
						utftext += String.fromCharCode((c & 63) | 128);
					}
				}
				return utftext;
			}
		
			// Public method for encoding
			return {
				encode: (typeof btoa === 'function') ? function(input) {
					return btoa(utf8Encode(input));
				} : function(input) {
					let output = '';
					let chr1; var chr2; var chr3; var enc1; var enc2; var enc3; var enc4;
					let i = 0;
					input = utf8Encode(input);
					while (i < input.length) {
						chr1 = input.charCodeAt(i++);
						chr2 = input.charCodeAt(i++);
						chr3 = input.charCodeAt(i++);
						enc1 = chr1 >> 2;
						enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
						enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
						enc4 = chr3 & 63;
						if (isNaN(chr2)) {
							enc3 = enc4 = 64;
						} else if (isNaN(chr3)) {
							enc4 = 64;
						}
						output = output
							+ keyStr.charAt(enc1) + keyStr.charAt(enc2)
							+ keyStr.charAt(enc3) + keyStr.charAt(enc4);
					}
					return output;
				},
			};
		}());

		if (!title) title = this.title;

		var vExportContent = this.getExcelXml(includeHidden, title);

		var location = 'data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);

		/* 
		  dynamically create and anchor tag to force download with suggested filename 
		  note: download attribute is Google Chrome specific
		*/
		
		if (Ext.isChrome) {
			var gridEl = this.getView().down('gridpanel').getEl();

			var el = Ext.DomHelper.append(gridEl, {
				tag: "a",
				download: title + "-" + Ext.Date.format(new Date(), 'Y-m-d Hi') + '.xls',
				href: location
			});

			el.click();

			Ext.fly(el).destroy();

		} else {

			var form = this.down('form#uploadForm');
			if (form) {
				form.destroy();
			}
			form = this.add({
				xtype: 'form',
				itemId: 'uploadForm',
				hidden: true,
				standardSubmit: true,
				url: 'http://webapps.figleaf.com/dataservices/Excel.cfc?method=echo&mimetype=application/vnd.ms-excel&filename=' + escape(title + ".xls"),
				items: [{
					xtype: 'hiddenfield',
					name: 'data',
					value: vExportContent
				}]
			});

			form.getForm().submit();

		}
	},

	/*
		Welcome to XML Hell
		See: http://msdn.microsoft.com/en-us/library/office/aa140066(v=office.10).aspx
		for more details
		* A partir les fonctions getExcelXml, getModelField, generateEmptyGroupRow, createWorksheet vont créer un export en Excel 97-2003
	*/
	getExcelXml(includeHidden, title){

		var theTitle = title || this.title;

		var worksheet = this.createWorksheet(includeHidden, theTitle);
		var totalWidth = this.getView().down('gridpanel').columnManager.columns.length;

		// Generate the data rows from the data in the Store
		let styleCustom = '';
		var cellType = [];
		var cellTypeClass = [];
		var cm = this.getView().down('gridpanel').columnManager.columns;

		var colCount = cm.length;
		
		for (var i = 0, it = this.getView().down('gridpanel').store.data.items, l = it.length; i < l; i++) {

			r = it[i].data;
			var k = 0;
			for (var j = 0; j < colCount; j++) {
				if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && (includeHidden || !cm[j].hidden)) {
					if (cm[j].dataIndex === 'evaluationUnAn') {
						const colorUn = Ext.isEmpty(r.evaluationRendererUn) ? '#FFFFFF' : r.evaluationRendererUn;
						styleCustom += ''.concat('<Style ss:ID="evaluationRendererUn' + i + j + '">',
						'<Borders>',
						'<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1" />',
						'<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" />',
						'<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" />',
						'<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" />',
					'</Borders>',
					'<Interior ss:Color="' + colorUn + '" ss:Pattern="Solid"/></Style>');
					} else if (cm[j].dataIndex === 'evaluationDeuxAn') {
						const colorDeux = Ext.isEmpty(r.evaluationRendererDeux) ? '#FFFFFF' : r.evaluationRendererDeux;
						styleCustom += ''.concat('<Style ss:ID="evaluationRendererDeux' + i + j + '">',
						'<Borders>',
						'<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1" />',
						'<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" />',
						'<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" />',
						'<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" />',
					'</Borders>',
					'<Interior ss:Color="' + colorDeux + '" ss:Pattern="Solid"/></Style>');
					} else if (cm[j].dataIndex === 'anciennetePlusCinqAn') {
						const colorAnc = Ext.isEmpty(r.ancienneteRenderer) ? '#FFFFFF' : r.ancienneteRenderer;
						styleCustom += ''.concat('<Style ss:ID="ancienneteRenderer' + i + j + '">',
						'<Font ss:Color="#FFFFFF"/>',
						'<Alignment ss:Horizontal="Center" ss:Vertical="Center"/>',
						'<Borders>',
						'<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1" />',
						'<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" />',
						'<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" />',
						'<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" />',
					'</Borders>',
					'<Interior ss:Color="' + colorAnc + '" ss:Pattern="Solid"/></Style>');
					} else if (cm[j].dataIndex === 'anniversaire') {
						const colorAnn = Ext.isEmpty(r.anniversaireRenderer) ? '#FFFFFF' : r.anniversaireRenderer;
						console.log(colorAnn);
						// styleCustom += '<Style ss:ID="anniversaireRenderer' + i + j + '"><Interior ss:Color="' + colorAnn + '"/></Style>';
					}
					
					k++;
				}
			}
		}
		
		const xmlStyle = ''.concat(
			'<?xml version="1.0"?>',
			'<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">',
			'<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Title>' + theTitle + '</Title></DocumentProperties>',
			'<OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office"><AllowPNG/></OfficeDocumentSettings>',
			'<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">',
			'<WindowHeight>' + worksheet.height + '</WindowHeight>',
			'<WindowWidth>' + worksheet.width + '</WindowWidth>',
			'<ProtectStructure>False</ProtectStructure>',
			'<ProtectWindows>False</ProtectWindows>',
			'</ExcelWorkbook>',

			'<Styles>',

			'<Style ss:ID="Default" ss:Name="Normal">',
			'<Alignment ss:Vertical="Bottom"/>',
			'<Borders/>',
			'<Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>',
			'<Interior/>',
			'<NumberFormat/>',
			'<Protection/>',
			'</Style>',

			styleCustom,

			'<Style ss:ID="title">',
			'<Borders />',
			'<Font ss:Bold="1" ss:Size="18" />',
			'<Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1" />',
			'<NumberFormat ss:Format="@" />',
			'</Style>',

			'<Style ss:ID="headercell">',
			'<Font ss:Bold="1" ss:Size="10" />',
			'<Alignment ss:Horizontal="Center" ss:WrapText="1" />',
			'<Interior ss:Color="#A3C9F1" ss:Pattern="Solid" />',
			'<Borders>',
				'<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1" />',
				'<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" />',
				'<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" />',
				'<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" />',
			'</Borders>',
			'</Style>',

			'<Style ss:ID="even">',
			'<Interior ss:Pattern="Solid" />',
			'<Borders>',
				'<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1" />',
				'<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" />',
				'<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" />',
				'<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" />',
			'</Borders>',
			'</Style>',

			'<Style ss:ID="evendate" ss:Parent="even">',
			'<NumberFormat ss:Format="dd/mm/yyyy" />',
			'</Style>',

			'<Style ss:ID="evenint" ss:Parent="even">',
			'<Numberformat ss:Format="0" />',
			'</Style>',

			'<Style ss:ID="evenfloat" ss:Parent="even">',
			'<Numberformat ss:Format="0.00" />',
			'</Style>',

			'<Style ss:ID="odd">',
			'<Interior ss:Pattern="Solid" />',
			'<Borders>',
				'<Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1" />',
				'<Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" />',
				'<Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" />',
				'<Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" />',
			'</Borders>',
			'</Style>',

			'<Style ss:ID="groupSeparator">',
			'<Interior ss:Color="#D3D3D3" ss:Pattern="Solid" />',
			'</Style>',

			'<Style ss:ID="odddate" ss:Parent="odd">',
			'<NumberFormat ss:Format="dd/mm/yyyy" />',
			'</Style>',

			'<Style ss:ID="oddint" ss:Parent="odd">',
			'<NumberFormat Format="0" />',
			'</Style>',

			'<Style ss:ID="oddfloat" ss:Parent="odd">',
			'<NumberFormat Format="0.00" />',
			'</Style>',

			'</Styles>',
			worksheet.xml,
			'</Workbook>'
		);
		
		return xmlStyle;
	},

	/*
		Support function to return field info from store based on fieldname
	*/

	getModelField(fieldName) {
		var fields = this.getView().down('gridpanel').store.model.getFields();
		for (var i = 0; i < fields.length; i++) {
			if (fields[i].name === fieldName) {
				return fields[i];
			}
		}
	},

	/*		
		Convert store into Excel Worksheet
	*/
	generateEmptyGroupRow(dataIndex, value, cellTypes, includeHidden) {
		var cm = this.columnManager.columns;
		var colCount = cm.length;
		var rowTpl = '<Row ss:AutoFitHeight="0"><Cell ss:StyleID="groupSeparator" ss:MergeAcross="{0}"><Data ss:Type="String"><html:b>{1}</html:b></Data></Cell></Row>';
		var visibleCols = 0;

		// rowXml += '<Cell ss:StyleID="groupSeparator">'

		for (var j = 0; j < colCount; j++) {
			if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && (includeHidden || !cm[j].hidden)) {
				// rowXml += '<Cell ss:StyleID="groupSeparator"/>';
				visibleCols++;
			}
		}

		// rowXml += "</Row>";

		return Ext.String.format(rowTpl, visibleCols - 1, value);
	},

	/*
		Write in workSheet xls
	*/
	createWorksheet(includeHidden, theTitle) {
		// Calculate cell data types and extra class names which affect formatting
		var cellType = [];
		var cellTypeClass = [];
		var cm = this.getView().down('gridpanel').columnManager.columns;

		var totalWidthInPixels = 0;
		var colXml = '';
		var headerXml = '';
		var visibleColumnCountReduction = 0;
		var colCount = cm.length;
		for (var i = 0; i < colCount; i++) {
			if (cm[i].xtype != 'actioncolumn' && (cm[i].dataIndex != '') && (includeHidden || !cm[i].hidden)) {
				var w = cm[i].getEl().getWidth();
				totalWidthInPixels += w;

				if (cm[i].text === "") {
					cellType.push("None");
					cellTypeClass.push("");
					++visibleColumnCountReduction;
				} else {
					colXml += '<Column ss:AutoFitWidth="1" ss:Width="' + w + '" />';
					headerXml += '<Cell ss:StyleID="headercell">' +
						'<Data ss:Type="String">' + cm[i].text + '</Data>' +
						'<NamedCell ss:Name="Print_Titles"></NamedCell></Cell>';


					var fld = this.getModelField(cm[i].dataIndex);
					
					if (!Ext.isEmpty(fld)) {
						switch (fld.type.type) {
							case "int":
								cellType.push("Number");
								cellTypeClass.push("int");
								break;
							case "float":
								cellType.push("Number");
								cellTypeClass.push("float");
								break;
	
							case "bool":
	
							case "boolean":
								cellType.push("String");
								cellTypeClass.push("");
								break;
							case "date":
								cellType.push("DateTime");
								cellTypeClass.push("date");
								break;
							default:
								cellType.push("String");
								cellTypeClass.push("");
								break;
						}
					}
				}
			}
		}
		var visibleColumnCount = cellType.length - visibleColumnCountReduction;

		var result = {
			height: 9000,
			width: Math.floor(totalWidthInPixels * 30) + 50
		};

		// Generate worksheet header details.

		// determine number of rows
		var numGridRows = this.getView().down('gridpanel').store.getCount() + 2;
		if (!Ext.isEmpty(this.getView().down('gridpanel').store.groupField)) {
			numGridRows = numGridRows + this.getView().down('gridpanel').store.getGroups().length;
		}

		// create header for worksheet
		var t = ''.concat(
			'<Worksheet ss:Name="' + theTitle + '">',

			'<Names>',
			'<NamedRange ss:Name="Print_Titles" ss:RefersTo="=\'' + theTitle + '\'!R1:R2">',
			'</NamedRange></Names>',

			'<Table ss:ExpandedColumnCount="' + (visibleColumnCount + 2),
			'" ss:ExpandedRowCount="' + numGridRows + '" x:FullColumns="1" x:FullRows="1" ss:DefaultColumnWidth="65" ss:DefaultRowHeight="15">',
			colXml,
			'<Row ss:Height="38">',
			'<Cell ss:MergeAcross="' + (visibleColumnCount - 1) + '" ss:StyleID="title">',
			'<Data ss:Type="String" xmlns:html="http://www.w3.org/TR/REC-html40">',
			'<html:b>' + theTitle + '</html:b></Data><NamedCell ss:Name="Print_Titles">',
			'</NamedCell></Cell>',
			'</Row>',
			'<Row ss:AutoFitHeight="1">',
			headerXml +
			'</Row>'
		);

		// Generate the data rows from the data in the Store
		var groupVal = "";
		var groupField = "";
		for (var i = 0, it = this.getView().down('gridpanel').store.data.items, l = it.length; i < l; i++) {

			if (!Ext.isEmpty(groupField)) {
				if (groupVal != this.getView().down('gridpanel').store.getAt(i).get(groupField)) {
					groupVal = this.getView().down('gridpanel').store.getAt(i).get(groupField);
					t += this.generateEmptyGroupRow(groupField, groupVal, cellType, includeHidden);
				}
			}
			t += '<Row>';
			var cellClass = (i & 1) ? 'odd' : 'even';
			r = it[i].data;
			var k = 0;
			for (var j = 0; j < colCount; j++) {
				if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && (includeHidden || !cm[j].hidden)) {
					var v = r[cm[j].dataIndex];
					if (cellType[k] !== "None") {						
						if (cm[j].dataIndex === 'evaluationUnAn') {
							t += '<Cell ss:StyleID="evaluationRendererUn' + i + j + '"><Data ss:Type="' + cellType[k] + '">' + Gprh.ades.util.Globals.rendererDate(v);
						} else if (cm[j].dataIndex === 'evaluationDeuxAn') {
							t += '<Cell ss:StyleID="evaluationRendererDeux' + i + j + '"><Data ss:Type="' + cellType[k] + '">' + Gprh.ades.util.Globals.rendererDate(v);
						} else if (cm[j].dataIndex === 'anciennetePlusCinqAn') {
							t += '<Cell ss:StyleID="ancienneteRenderer' + i + j + '"><Data ss:Type="' + cellType[k] + '">' +parseInt(v, 10);
						} else t += '<Cell ss:StyleID="' + cellClass + cellTypeClass[k] + '"><Data ss:Type="' + cellType[k] + '">' + v;
						
						t += '</Data></Cell>';
					}
					k++;
				}
			}
			t += '</Row>';
		}

		result.xml = t.concat(
			'</Table>',
			'<WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">',
			'<PageLayoutZoom>0</PageLayoutZoom>',
			'<Selected/>',
			'<Panes>',
			'<Pane>',
			'<Number>3</Number>',
			'<ActiveRow>2</ActiveRow>',
			'</Pane>',
			'</Panes>',
			'<ProtectObjects>False</ProtectObjects>',
			'<ProtectScenarios>False</ProtectScenarios>',
			'</WorksheetOptions>',
			'</Worksheet>'
		);
		return result;
	},

	/*
	 * Cette fonction demandera au serveur PHP d'exporter le store sous Excel
	 */
	exportExcelSuiviContrat() {
		const entetes = this.getView().down('gridpanel').columnManager.columns;
		const donnees = this.getView().down('gridpanel').store.data.items;
		const filtre = this.getView().down('#filtresContratId').rawValue;
		
		Ext.getBody().mask('Téléchargement...');
		
		// Va exporter les données du salaire clôturé sur Excel (2007 .xlsx)
		const newArrHeaders = entetes.map(elem => {
			return {
				text: elem.text,
				dataIndex: elem.dataIndex,
				hidden: elem.hidden
			};
		});
		
		const newArrData = donnees.map(elem => {
			return elem.data;
		});
		Gprh.ades.util.Downloader.get({
			url: 'server-scripts/export/SuiviContrat.php',
			params: {
				headersText: Ext.encode(newArrHeaders),
				datas: Ext.encode(newArrData),
				filtre: filtre,
			},
		});
	},
});
