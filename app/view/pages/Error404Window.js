Ext.define('Gprh.ades.view.pages.Error404Window', {
	// extend: 'Gprh.ades.view.pages.ErrorBase',
	extend: 'Ext.panel.Panel',
	xtype: 'page404',

	requires: [
		'Ext.container.Container',
		'Ext.form.Label',
		'Ext.layout.container.VBox',
		'Ext.toolbar.Spacer',
	],

	items: [
		{
			xtype: 'panel', // A changer en container si vous utilisez le extend: 'Gprh.ades.view.pages.ErrorBase',
			// A enlever si vous utilisez le extend: 'Gprh.ades.view.pages.ErrorBase',
			cls: 'error-page-container error-page-inner-container',
			title: 'Toutes nos excuses, le module que vous recherchez n\'existe pas ou a été désactivé.',
			titleAlign: 'center',
			// cls: 'error-page-inner-container', // A activer si vous utilisez le extend: 'Gprh.ades.view.pages.ErrorBase',
			layout: {
				type: 'vbox',
				align: 'center',
				pack: 'center',
			},
			items: [
				{
					xtype: 'label',
					cls: 'error-page-top-text',
					text: '404',
				},
				{
					xtype: 'label',
					cls: 'error-page-desc',
					html: '<div>Oups!</div><a>La page que vous recherchez semble introuvable.</a>',
				},
			],
		},
	],
});
