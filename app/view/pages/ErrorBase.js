Ext.define('Gprh.ades.view.pages.ErrorBase', {
	extend: 'Ext.window.Window',

	requires: [],

	controller: 'login',
	autoShow: true,
	cls: 'error-page-container',
	closable: true,
	title: 'Sorry! ERROR 404 = Invalid url.',
	titleAlign: 'center',
	maximized: true,
	modal: true,

	layout: {
		type: 'vbox',
		align: 'center',
		pack: 'center',
	},
});
