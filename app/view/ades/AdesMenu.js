Ext.define('Gprh.ades.view.ades.AdesMenu', {
	extend: 'Ext.menu.Menu',

	alias: 'widget.adesmenu',

	viewModel: {
		type: 'adesmenu',
	},

	floating: false,

	initComponent() {
		this.items = [
			{
				routeId: 'adesInformations',
				itemId: 'adesInformationsMId',
				iconCls: 'x-fa fa-inbox',
				text: 'Information sur ADES',
				listeners: {
					click: 'onEditAdesClick',
				},
			},
			{
				routeId: 'structures',
				itemId: 'structuresMId',
				iconCls: 'x-fa fa-sitemap',
				text: 'Organigramme & Postes',
				listeners: {
					click: 'onStructuresClick',
				},
				hidden: !this.privileges(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'), ['1', '2']),
			},
			{
				routeId: 'cnaps',
				itemId: 'cnapsMId',
				iconCls: 'x-fa fa-umbrella',
				text: 'CNaPS',
				listeners: {
					click: 'onCnapsClick',
				},
				hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
			},
			{
				routeId: 'irsa',
				itemId: 'irsaMId',
				iconCls: 'x-fa fa-exclamation-circle',
				text: 'IRSA',
				listeners: {
					click: 'onIrsaClick',
				},
				hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
			},
			{
				routeId: 'smie',
				itemId: 'smieMId',
				iconCls: 'x-fa fa-medkit',
				text: 'Affiliation médicale',
				listeners: {
					click: 'onSmieClick',
				},
				hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
			},
			{
				routeId: 'hopital',
				itemId: 'hopitalMId',
				iconCls: 'x-fas fa-hospital-alt',
				text: 'Remboursement médical',
				listeners: {
					click: 'onHopitalClick',
				},
				hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
			},
			{
				routeId: 'grille',
				itemId: 'grilleMId',
				iconCls: 'x-fa fa-columns',
				text: 'Grille salariale',
				listeners: {
					click: 'onGrilleClick',
				},
				hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
			},
			{
				routeId: 'taux',
				itemId: 'tauxMId',
				iconCls: 'x-fa fa-chart-line',
				text: 'Taux et paramètres',
				listeners: {
					click: 'onTauxClick',
				},
				hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
			},
			{
				routeId: 'formule',
				itemId: 'formuleMId',
				iconCls: 'x-fa fa-briefcase',
				text: 'Voir les formules',
				listeners: {
					click: 'onFormuleClick',
				},
				hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
			},
		];

		this.callParent();
	},

	privileges(typeUtilisateur, valeursPossibles) {
		const found = valeursPossibles.indexOf(typeUtilisateur);
		
		return found !== -1;
	},

});
