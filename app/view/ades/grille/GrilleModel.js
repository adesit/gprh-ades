Ext.define('Gprh.ades.view.ades.grille.GrilleModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.grilleModel',

	requires: [
		'Gprh.ades.store.Groupe',
		'Gprh.ades.store.Categorie',
		'Gprh.ades.store.SalaireBase',
	],

	parameters: '',

	stores: {
		groupeResults: {
			type: 'groupeStore',
		},

		categorieResults: {
			type: 'categorieStore',
		},

		grilleResults: {
			type: 'salaireBaseStore',
			groupField: 'nomGroupe',
			groupDir: 'ASC',
			proxy: {
				extraParams: {
					infoRequest: 1,
				},
			},
		},
	},
	formulas: {
		isEnabledDelCategorie: {
			bind: {
				t1: '{!categorieGridRef.selection}',
			},
			// 1 ligne de catégorie est sélectionnée
			get: d => d.t1,
		},
	},
});
