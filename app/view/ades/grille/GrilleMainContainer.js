Ext.define('Gprh.ades.view.ades.grille.GrilleMainContainer', {
	extend: 'Ext.tab.Panel',
	xtype: 'grille',

	requires: [
		'Ext.grid.Panel',
		'Ext.toolbar.Paging',
		'Ext.grid.column.Date',
		'Gprh.ades.util.Globals',
		'Gprh.ades.view.ades.grille.GrilleModel',
		'Gprh.ades.model.GroupeModel',
		'Gprh.ades.model.CategorieModel',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	controller: 'adesController',

	viewModel: {
		type: 'grilleModel',
	},

	cls: 'shadow',
	activeTab: 0,
	bodyPadding: 15,

	// grid default height
	gridHeight: Ext.Element.getViewportHeight() - 150,

	initComponent() {
		const rowEditing = Gprh.ades.util.Globals.setRowEditingConfig('server-scripts/grille/Groupe.php', this);

		const rowEditingCategorie = Gprh.ades.util.Globals.setRowEditingConfig('server-scripts/grille/Categorie.php', this);
		
		const grid = {
			xtype: 'gridpanel',
			cls: 'user-grid',
			title: 'Groupes',
			routeId: 'groupeTabId',
			itemId: 'groupeGridId',
			bind: {
				store: '{groupeResults}',
			},
			height: this.gridHeight,
			stripeRows: true,
			columnLines: true,
			columns: [
				{
					xtype: 'gridcolumn',
					width: 40,
					dataIndex: 'idGroupeCategorie',
					text: '#',
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'nomGroupe',
					text: 'Nom du groupe',
					align: 'left',
					flex: 1,
					editor: {
						allowBlank: false,
					},
				},
				{
					xtype: 'actioncolumn',
					cls: 'content-column',
					width: 80,
					text: 'Actions',
					items: [
						{
							iconCls: 'x-fa fa-trash soft-red-small',
							handler: 'confirmDeleteGroupe',
						},
					],					
				},
			],
			dockedItems: [
				Gprh.ades.util.Globals.getPagingToolbar(this),
			],
			tbar: [
				'->', 
				{
					text: 'Nouveau Groupe',
					iconCls: 'fa fa-plus',
					margin: '0 0 10 0',
					handler: () => {
						rowEditing.cancelEdit();
										
						const r = Ext.create('Gprh.ades.model.GroupeModel', {
							idGroupeCategorie: '',
							nomGroupe: '',
						});
	
						this.down('#groupeGridId').getStore().insert(0, r);
						rowEditing.startEdit(0, 0);
					},
				}],
			plugins: [rowEditing],
		};

		const gridCategorie = {
			xtype: 'gridpanel',
			cls: 'user-grid',
			title: 'Catégories professionnelles',
			itemId: 'categorieGridId',
			reference: 'categorieGridRef',
			routeId: 'categorieTabId',
			bind: {
				store: '{categorieResults}',
			},
			maxHeight: this.gridHeight,
			anchor: '100%',
			stripeRows: true,
			columnLines: true,
			columns: [
				{
					xtype: 'gridcolumn',
					width: 40,
					dataIndex: 'idCategorie',
					text: '#',
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'nomGroupe',
					text: 'Groupe',
					align: 'left',
					autoSizeColumn: true,
					editor: {
						xtype: 'combobox',
						store: 'groupeStore',
						queryMode: 'local',
						displayField: 'nomGroupe',
						valueField: 'idGroupeCategorie',
						renderTo: Ext.getBody(),
						allowBlank: false, 
						forceSelection: true,
						editable: false,
					},
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'categorieProfessionnelle',
					text: 'Catégorie professionnelle',
					align: 'left',
					autoSizeColumn: true,
					width: 125,
					editor: {
						allowBlank: false,
					},
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'descriptionCategorie',
					text: 'Description',
					align: 'left',
					autoSizeColumn: true,
					flex: 1,
					cellWrap: true,
					editor: {
						allowBlank: true,
					},
				},
				{
					xtype: 'actioncolumn',
					dataIndex: 'nbActifLigneGrille',
					renderer: (value, meta, record, row, column, store) => this.initRendererColumnAction(value, meta, record, row, column, store),
					listeners: {
						click: 'showGrilleWindow',
					},
					cls: 'content-column',
					cellWrap: true,
					flex: 0.3,
					text: 'Actions',
					align: 'left',
				},
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					dock: 'bottom',
					itemId: 'categoriePaginationToolbar',
					displayInfo: true,
					bind: {
						store: '{categorieResults}',
					},
				},
			],
			tbar: [
				{
					text: 'Nouvelle catégorie',
					iconCls: 'fa fa-plus',
					margin: '0 0 10 0',
					handler: () => {
						rowEditingCategorie.cancelEdit();
						
						const r = Ext.create('Gprh.ades.model.CategorieModel', {
							idCategorie: '',
							idGroupeCategorie: '',
							categorieProfessionnelle: '',
							descriptionCategorie: '',
						});
						
						this.down('#categorieGridId').getStore().insert(0, r);
						rowEditingCategorie.startEdit(0, 0);
					},
				},
				'->', 
				{
					tooltip: 'Supprimer',
					text: 'Supprimer',
					iconCls: 'fa fa-trash',
					margin: '0 0 10 2',
					ui: 'soft-red-small',
					listeners: {
						click: 'onDeleteCategorie',
					},
					bind: {
						disabled: '{isEnabledDelCategorie}',
					},
				}],
			plugins: [rowEditingCategorie],
		};
		
		const gridGrille = {
			xtype: 'gridpanel',
			cls: 'user-grid',
			title: 'Grille salariale',
			routeId: 'grilleTabId',
			itemId: 'grilleGridId',
			bind: {
				store: '{grilleResults}',
			},
			anchor: '100%',
			maxHeight: this.gridHeight,
			width: 450,
			stripeRows: true,
			columnLines: true,
			columns: [
				{
					dataIndex: 'nomGroupe',
					text: 'Groupe',
					align: 'left',
					autoSizeColumn: true,
					hidden: true,
				},
				{
					dataIndex: 'categorieProfessionnelle',
					text: 'Catégorie',
					align: 'left',
					width: 150,
					autoSizeColumn: true,
				},
				{
					dataIndex: 'salaireBaseMinimum',
					text: 'Base minimum',
					align: 'left',
					autoSizeColumn: true,
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
				},
				{
					dataIndex: 'salaireBase3',
					text: 'Base si ancienneté > 3',
					align: 'left',
					minWidth: 120,
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
				},
				{
					dataIndex: 'salaireBase4',
					text: 'Base si ancienneté > 4',
					align: 'left',
					minWidth: 120,
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
				},
				{
					dataIndex: 'salaireBase5',
					text: 'Base si ancienneté > 5',
					align: 'left',
					minWidth: 120,
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'datePriseEffetGrille',
					text: 'Dernière modification',
					align: 'left',
					format: 'd/m/Y',
					width: 100,
				},
				{
					xtype: 'actioncolumn',
					cls: 'content-column',
					width: 120,
					text: 'Actions',
					items: [
						{
							iconCls: 'x-fa fa-trash soft-red-small',
							handler: 'confirmDeleteGrille',
						},
					],					
				},
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					dock: 'bottom',
					itemId: 'grillePaginationToolbar',
					displayInfo: true,
					bind: {
						store: '{grilleResults}',
					},
				},
			],
			tbar: [
				'->',
				{
					text: 'Grille salariale actuelle',
					style: {
						background: 'transparent',
						border: 'none',
					},
				},
			],
			features: [{
				ftype: 'grouping',
				groupHeaderTpl: '{name} ({children.length})',
				enableNoGroups: true,
			}],
		};
		
		this.items = [
			grid,
			gridCategorie,
			gridGrille,
		];
		this.layout = 'fit';

		this.callParent();
	},

	initRendererColumnAction(value, meta, record, row, column, store) {
		let iconCls;
		let buttonText;
		let action;
		const items = [];
		let datas;
		let actifExists;
		let lignesExist;
		let butonId;

		if (!Ext.isEmpty(row)) {
			datas = store.getAt(row).data;
			actifExists = datas.nbActifLigneGrille;
			lignesExist = datas.nbLigneGrilleByCategorie;
			butonId = datas.idCategorie;
		}
		
		if (actifExists > 0) {
			iconCls = 'fa fa-ban';
			buttonText = 'Modifier';
			action = 'changeBases';
		} else {
			iconCls = 'fa fa-pencil';
			buttonText = 'Ajouter';
			action = 'addBases';
		}
		const butonActionActif = `<button id="${action}_${butonId}" 
		type="button" 
		style="margin-right: 2px;" >
		<span class="${iconCls}"></span> ${buttonText}
		</button>`;

		items.push(butonActionActif);

		if (lignesExist > 0 && actifExists === 0) {
			items.push(`<button id="listeBases_${butonId}" type="button">
			<span class="fa fa-list-alt"></span> Liste
			</button>`);
		}

		return items;
	}, 
});
