Ext.define('Gprh.ades.view.ades.grille.SalaireBaseGridPanel', {
	extend: 'Ext.form.Panel',
	alias: 'widget.salaireBaseGridPanel',

	requires: [
		'Gprh.ades.util.Globals',
	],
	
	viewModel: {
		type: 'salaireBaseViewModel',
	},

	controller: 'adesController',

	colorScheme: 'soft-green',
	bodyPadding: 15,

	initComponent() {
		const record = this.params;
		
		const gridSalaireBase = {
			xtype: 'gridpanel',
			routeId: 'listeGrilleGridPanelId',
			itemId: 'listeGrilleGridPanelId',
			margin: '10 0 0 0',
			store: this.storeGrilleSalaire,
			height: this.gridHeight,
			stripeRows: true,
			columnLines: true,
			columns: [
				{
					xtype: 'gridcolumn',
					width: 40,
					dataIndex: 'idLigneGrille',
					text: '#',
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'salaireBaseMinimum',
					text: 'Base minimum',
					align: 'left',
					autoSizeColumn: true,
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'salaireBase3',
					text: 'Base si ancienneté > 3',
					align: 'left',
					minWidth: 120,
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'salaireBase4',
					text: 'Base si ancienneté > 4',
					align: 'left',
					minWidth: 120,
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'salaireBase5',
					text: 'Base si ancienneté > 5',
					align: 'left',
					minWidth: 120,
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'datePriseEffetGrille',
					text: 'Dernière modification',
					align: 'left',
					format: 'd/m/Y',
					width: 100,
				},
				{
					xtype: 'checkcolumn',
					header: 'Actif',
					dataIndex: 'actifLigneGrille',
					width: 60,
					editor: {
						xtype: 'checkbox',
						cls: 'x-grid-checkheader-editor',
					},
					listeners: {
						checkchange: 'activeLigneGrille',
					},
				},
			],
			
		};
		
		this.items = [
			{
				xtype: 'component',
				html: `${'<div class="services-legend" style: "padding-bottom: 5px;">'
				+ '<span><div class="legend-finance"></div><u>Groupe:</u> <b>'}${record.data.nomGroupe}</b></span>`
				+ `<span><div class="legend-research"></div><u>Catégorie:</u><b>${record.data.categorieProfessionnelle}</b></span>`
				+ '<div>',
			},
			gridSalaireBase,
		];

		this.callParent();
	},
});
