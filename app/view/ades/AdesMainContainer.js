Ext.define('Gprh.ades.view.ades.AdesMainContainer', {
	extend: 'Ext.container.Container',

	xtype: 'adesMainContainer',

	requires: [
		'Gprh.ades.view.ades.AdesController',
		'Gprh.ades.view.ades.AdesMenu',
		'Gprh.ades.view.ades.AdesInformations',
	],

	controller: 'adesController',

	itemId: 'adesMainContainer',	

	layout: {
		type: 'hbox',
		align: 'stretch',
	},

	items: [
		{
			xtype: 'panel',
			itemId: 'navigationPanel',
			collapsible: true,
			collapseDirection: 'left',
			collapseToolText: 'ADES',
			title: 'ADES',
			layout: {
				type: 'vbox',
				align: 'stretch',
			},
			width: '30%',
			minWidth: 180,
			maxWidth: 240,
			defaults: {
				cls: 'navigation-email',
			},
			items: [
				{
					xtype: 'adesmenu',
					listeners: {
						click: 'onMenuClick',
					},
				},
			],
		},
		{
			xtype: 'container',
			itemId: 'contentPanel',
			flex: 1,
			layout: {
				type: 'anchor',
				anchor: '100%',
			},
			items: [
				{
					xtype: 'adesInformations',
				},
			],
		},
	],
});
