Ext.define('Gprh.ades.view.ades.smie.SmieViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.smieViewModel',

	requires: [
		'Gprh.ades.store.Smie',
		'Gprh.ades.store.Centre',
	],

	stores: {
		smieResults: {
			type: 'smieStore',
		},
		centreResults: {
			type: 'centreStore',
		},
	},
	formulas: {
		isEnabledDelSmie: {
			bind: {
				t1: '{!smieGridRef.selection}',
			},
			// 1 ligne de smie est sélectionnée
			get: d => d.t1,
		},
	},
});
