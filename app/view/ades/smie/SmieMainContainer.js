Ext.define('Gprh.ades.view.ades.smie.SmieMainContainer', {
	extend: 'Ext.tab.Panel',
	xtype: 'smie',

	requires: [
		'Ext.grid.Panel',
		'Ext.toolbar.Paging',
		'Ext.grid.column.Date',
		'Gprh.ades.util.Globals',
		'Gprh.ades.view.ades.smie.SmieViewModel',
		'Gprh.ades.model.SmieModel',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	controller: 'adesController',

	viewModel: {
		type: 'smieViewModel',
	},

	cls: 'shadow',
	activeTab: 0,
	bodyPadding: 15,

	// grid default height
	gridHeight: Ext.Element.getViewportHeight() - 150,

	initComponent() {
		const rowEditing = Gprh.ades.util.Globals.setRowEditingConfig('server-scripts/smie/Smie.php', this);

		const grid = {
			xtype: 'gridpanel',
			cls: 'user-grid',
			title: 'Organismes d\'affiliation médicale',
			routeId: 'smieTabId',
			itemId: 'smieGridId',
			reference: 'smieGridRef',
			bind: {
				store: '{smieResults}',
			},
			height: this.gridHeight,
			columnLines: true,
			stripeRows: true,
			columns: [
				{
					xtype: 'gridcolumn',
					dataIndex: 'idSmie',
					text: '#',
					hidden: true,
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'nomCentre',
					text: 'Centre',
					align: 'left',
					width: 175,
					editor: {
						xtype: 'combobox',
						store: 'centreStore',
						queryMode: 'local',
						displayField: 'nomCentre',
						valueField: 'idCentre',
						renderTo: Ext.getBody(),
						allowBlank: false, 
						forceSelection: true,
						editable: false,
					},
				},
				{
					xtype: 'gridcolumn',
					width: 40,
					dataIndex: 'nomSmie',
					text: 'Organisme d\'affiliation',
					align: 'left',
					flex: 1,
					editor: {
						allowBlank: false,
					},
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'plafondSmie',
					text: 'Plafond (Ariary)',
					align: 'left',
					flex: 1,
					editor: {
						allowBlank: false,
						listeners: {
							blur: self => Gprh.ades.util.Globals.formatNumber(self),
						},
					},
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'deductionEmploye',
					text: 'Part employé (%)',
					align: 'left',
					flex: 1,
					editor: {
						allowBlank: false,
					},
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'deductionEmployeur',
					text: 'Part employeur (%)',
					align: 'left',
					flex: 1,
					editor: {
						allowBlank: false,
					},
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'coutExcedentaire',
					text: 'Coût excédentaire (Ariary)',
					align: 'left',
					hidden: true,
					flex: 1,
					editor: {
						allowBlank: false,
						listeners: {
							blur: self => Gprh.ades.util.Globals.formatNumber(self),
						},
					},
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'franchiseCoutExcendentaire',
					text: 'Franchise (Ariary)',
					align: 'left',
					hidden: true,
					flex: 1,
					editor: {
						allowBlank: false,
						listeners: {
							blur: self => Gprh.ades.util.Globals.formatNumber(self),
						},
					},
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
				},
				{
					xtype: 'datecolumn',
					header: 'Date prise d\'effet',
					dataIndex: 'datePriseEffetSmie',
					editor: {
						xtype: 'datefield',
						allowBlank: false,
						format: 'd/m/Y',
						minText: 'Ne devrait pas être null et doit être antérieure à la date du jour!',
					},
				},
				{
					xtype: 'checkcolumn',
					header: 'Actif',
					dataIndex: 'actifSmie',
					width: 60,
					editor: {
						xtype: 'checkbox',
						cls: 'x-grid-checkheader-editor',
					},
				},
			],
			dockedItems: [
				Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
			],
			tbar: [
				'->', 
				{
					text: 'Nouveau organisme',
					iconCls: 'fa fa-plus',
					margin: '0 0 10 0',
					handler: () => {
						rowEditing.cancelEdit();
										
						const r = Ext.create('Gprh.ades.model.SmieModel', {
							idSmie: '',
							nomSmie: '',
							plafondSmie: '',
							deductionEmploye: 0,
							deductionEmployeur: 0,
							coutExcedentaire: 0,
							franchiseCoutExcendentaire: 0,
							datePriseEffetSmie: Ext.Date.clearTime(new Date()),
							actifSmie: false,
							idCentre: '',
						});
	
						this.down('#smieGridId').getStore().insert(0, r);
						rowEditing.startEdit(0, 0);
					},
				},
				{
					tooltip: 'Supprimer',
					text: 'Supprimer',
					iconCls: 'fa fa-trash',
					margin: '0 0 10 0',
					ui: 'soft-red-small',
					listeners: {
						click: 'onDeleteSmie',
					},
					bind: {
						disabled: '{isEnabledDelSmie}',
					},
				}],
			plugins: [rowEditing],
		};

		this.items = [
			grid,
		];
		this.layout = 'fit';

		this.callParent();
	},
});
