Ext.define('Gprh.ades.view.ades.AdesController', {
	extend: 'Ext.app.ViewController',

	requires: [
		'Gprh.ades.view.ades.AdesInformationForm',
		'Gprh.ades.view.ades.structures.StructureMainContainer',
		'Gprh.ades.view.ades.cnaps.CnapsMainContainer',
		'Gprh.ades.view.ades.irsa.IrsaMainContainer',
		'Gprh.ades.view.ades.smie.SmieMainContainer',
		'Gprh.ades.view.ades.hopital.HopitalMainContainer',
		'Gprh.ades.view.ades.grille.GrilleMainContainer',
		'Gprh.ades.view.pages.PrototypeWindow',
		'Gprh.ades.view.ades.grille.SalaireBasePanel',
		'Gprh.ades.view.ades.grille.SalaireBaseGridPanel',
		'Gprh.ades.view.ades.structures.PosteForm',
		'Gprh.ades.view.ades.structures.PosteMainContainer',
		'Gprh.ades.view.ades.formule.FormuleMainContainer',
		'Gprh.ades.view.ades.formule.FormuleForm',
		'Gprh.ades.view.ades.taux.TauxMainContainer',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	alias: 'controller.adesController',

	init() {
		this.setCurrentView('adesInformations');
	},

	onEditAdesClick() {
		this.setCurrentView('adesInformations');
	},

	onMenuClick(menu, item) {
		if (item) {
			menu.items.each((items) => {
				if (items !== item) {
					items.setStyle('background-color');					
				}
			});
			item.setStyle('background-color', '#ffefbb');
			this.setCurrentView(item.routeId, item.params);
		}
	},

	setCurrentView(view, params) {
		const contentPanel = this.getView().down('#contentPanel');
		// We skip rendering for the following scenarios:
		// * There is no contentPanel
		// * view xtype is not specified
		// * current view is the same
		if (!contentPanel || view === '' || (contentPanel.down() && contentPanel.down().xtype === view)) {
			return false;
		}

		if (params && params.openWindow) {
			const cfg = Ext.apply({
				xtype: 'prototypeWindow',
				items: [
					Ext.apply({
						xtype: view,
					}, params.targetCfg),
				],
			}, params.windowCfg);

			Ext.create(cfg);
		} else {
			Ext.suspendLayouts();

			contentPanel.removeAll(true);
			contentPanel.add(
				Ext.apply({
					xtype: view,
				}, params)
			);

			Ext.resumeLayouts(true);
		}
		return true;
	},

	onAdesInformationEditClick(item) {
		this.setCurrentView('adesInformationForm', item.params);
	},

	onAdesInformationSaveClick(view) {
		const me = this;
		const form = view.up('form');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/ades/Ades.php', model);
		
		if (saveOrUpdate.request.request.result.responseText > 0) {
			const mainView = me.getView().up('#adesMainContainer');
			
			mainView.down('#adesInformationsMId').fireEvent('click');
		}
	},

	onAdesInformationCancelClick() {
		const me = this;
		const mainView = me.getView().up('#adesMainContainer');
			
		mainView.down('#adesInformationsMId').fireEvent('click');
	},

	onStructuresClick() {
		this.setCurrentView('structures');
	},

	onCnapsClick() {
		this.setCurrentView('cnaps');
	},

	onIrsaClick() {
		this.setCurrentView('irsa');
	},

	onSmieClick() {
		this.setCurrentView('smie');
	},

	onHopitalClick() {
		this.setCurrentView('hopital');
	},
	
	onGrilleClick() {
		this.setCurrentView('grille');
	},
	
	onTauxClick() {
		this.setCurrentView('taux');
	},

	/*
	 * Create a new form to fill all wadges for the given categorie
	 * @param {object} view the Tab panel view
	 * @param {int} rowIndex selected rowIndex
	 * @param {int} colIndex selected colIndex
	 * @param {object} item all items config where the event was handled
	 * @param {object} e the event
	 * @param {object} record selected row data
	 * @param {object} row dom element for the row selected
	 * 
	 * @return {object} {@link Gprh.ades.grille.SalaireBase} instance which this class exposes.
	 */

	showGrilleWindow(view, cell, recordIndex, cellIndex, e, record) {
		const domEl = e.target;
		const [action] = domEl.id.split('_');

		const grilleParams = {
			idGroupe: record.get('idGroupeCategorie'),
			idCategorie: record.get('idCategorie'),
		};

		const salaireBaseWindow = {
			xtype: 'prototypeWindow',
			title: 'Salaires de base',
			maxWidth: 400,
			maxHeight: 450,
			items: [
				{
					xtype: 'salaireBasePanel',
					params: record,
					parentPanelRef: view,
				},
			],
		};

		const salaireBaseStore = Ext.create('Gprh.ades.store.SalaireBase');
		salaireBaseStore.getProxy().setExtraParams({
			infoRequest: 2,
			idGroupe: record.get('idGroupeCategorie'),
			idCategorie: record.get('idCategorie'),
		});

		const salaireBaseGridPanel = {
			xtype: 'prototypeWindow',
			title: 'Liste disponible',
			maxWidth: 750,
			maxHeight: 400,
			items: [
				{
					xtype: 'salaireBaseGridPanel',
					params: record,
					parentPanelRef: view,
					storeGrilleSalaire: salaireBaseStore,
				},
			],
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'confirmMsgBox',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelUpdLigneActif = {
						infoRequest: 3,
						data: grilleParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/grille/SalaireBase.php', modelUpdLigneActif);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#confirmMsgBox').close();
						this.getView().down('#categorieGridId').getStore().load();
						Ext.create(salaireBaseWindow);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#confirmMsgBox').close();
				},
			}],
		});

		if (action === 'addBases') {
			Ext.create(salaireBaseWindow);
		} else if (action === 'changeBases') {
			messageBox.show({
				title: 'Confirmer la modification',
				msg: 'Cette valeur existe déjà. Voulez-vous la remplacer?',
				icon: Ext.MessageBox.WARNING,
			});
		} else if (action === 'listeBases') {
			Ext.create(salaireBaseGridPanel);
		}
	},

	onSalaireBasePanelCancelClick() {
		this.getView().up().close();
	},

	onSalaireBasePanelSaveClick(view) {
		const form = view.up('#salaireBasePanelFormId');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/grille/SalaireBase.php', model);
		
		if (saveOrUpdate.request.request.result.responseText > 0) {
			this.getView().parentPanelRef.up('#categorieGridId').getStore().load();
			this.getView().up().close();
		}
	},

	/*
	 * @param {(column, rowIndex, checked, record, e)}
	 */
	confirmActifCnaps(column, rowIndex, checked, record) {
		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxCnapsChecked',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelUpdLigneActif = {
						infoRequest: 4,
						idCnaps: record.data.idCnaps,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/cnaps/Cnaps.php', modelUpdLigneActif);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxCnapsChecked').close();
						this.getView().down('#cnapsGridId').getStore().load();
					}			
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxCnapsChecked').close();
					this.getView().down('#cnapsGridId').getStore().reload();
				},
			}],
		});
		
		if (checked) {
			messageBox.show({
				title: 'Confirmer l\'enregistrement',
				msg: 'Cette valeur existe déjà. Voulez-vous la remplacer?',
				icon: Ext.MessageBox.WARNING,
			});
		}
	},

	activeLigneGrille(column, rowIndex, checked, record) {
		const grilleParams = {
			idLigneGrille: record.get('idLigneGrille'),
			idGroupe: record.get('idGroupeCategorie'),
			idCategorie: record.get('idCategorie'),
		};

		if (checked) {
			const modelUpdLigneActif = {
				infoRequest: 4,
				data: grilleParams,
			};
			const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/grille/SalaireBase.php', modelUpdLigneActif);
			if (saveOrUpdate.request.request.result.responseText > 0) {
				this.getView().parentPanelRef.up('#categorieGridId').getStore().load();
				this.getView().up().close();
			}
		}
	},

	confirmDeleteCentre(view, cell, recordIndex, cellIndex, e, record) {
		const centreParams = {
			idCentre: record.get('idCentre'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxCentreDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelCentre = {
						infoRequest: 3,
						data: centreParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/centreDepartement/Centre.php', modelDelCentre);
					const result = saveOrUpdate.request.request.result.responseText;
					
					if (result > 0) {
						btn.up('#messageBoxCentreDelete').close();
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						this.getView().down('#centreGridId').getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxCentreDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxCentreDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	confirmDeleteDepartement(view, cell, recordIndex, cellIndex, e, record) {
		const departementParams = {
			idDepartement: record.get('idDepartement'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxDepartementDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelDepartement = {
						infoRequest: 3,
						data: departementParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/centreDepartement/Departement.php', modelDelDepartement);
					const result = saveOrUpdate.request.request.result.responseText;

					if (result > 0) {
						btn.up('#messageBoxDepartementDelete').close();
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						this.getView().down('#departementGridId').getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxDepartementDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxDepartementDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	confirmDeleteSection(view, cell, recordIndex, cellIndex, e, record) {
		const sectionParams = {
			idSection: record.get('idSection'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxSectionDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelSection = {
						infoRequest: 3,
						data: sectionParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/centreDepartement/Section.php', modelDelSection);
					const result = saveOrUpdate.request.request.result.responseText;
					
					if (result > 0) {
						btn.up('#messageBoxSectionDelete').close();
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						this.getView().down('#sectionGridId').getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxSectionDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxSectionDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	onAddPoste(btn, record) {
		btn.setDisabled(true);
		Ext.suspendLayouts();
		
		const postMainView = this.getView().down('#posteMainContainerId');
		postMainView.removeAll(true);
		postMainView.add(
			Ext.apply({
				xtype: 'posteForm',
				params: record,
			})
		);
		Ext.resumeLayouts(true);
	},

	onPosteListe() {
		Ext.suspendLayouts();
		this.getView().down('#addPosteBtnId').setDisabled(false);
		const postMainView = this.getView().down('#posteMainContainerId');
		postMainView.removeAll(true);
		postMainView.add(
			Ext.apply({
				xtype: 'posteGrid',
			})
		);
		Ext.resumeLayouts(true);
	},

	cancelAction(formId) {
		const form = this.getView().down(`#${formId.params}`);
		form.reset();
	},

	/*
	 * Cette fonction s'applique à une grille qui a selModel: Ext.create('Ext.selection.CheckboxModel', {
				checkOnly: true,
			}),
	 prend toutes les données nécessaires, les met dans un tableau et l'envoie vers le serveur
	 */
	onPosteSaveClick() {
		const grille = this.getView().down('#grilleInPosteGridId');
		const form = this.getView().down('#posteFormId');
		const ids = [];

		const s = grille.getSelectionModel().getSelection();

		if (Ext.isEmpty(s)) {
			const msg = 'Sélectionnez une ligne contenant les salaires de base pour ce poste.';
			Gprh.ades.util.Helper.showError(msg); 
		} else {
			Ext.each(s, (record) => {
				ids.push([record.get('idGroupe'), record.get('idCategorie')]);
			});
			const model = {
				infoRequest: 2,
				data: form.getValues(),
				paramsIds: ids,
			};

			const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/centreDepartement/Poste.php', model);
			const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
			if (serverResponse.length > 0) {
				grille.getStore().load();
				this.onPosteListe();
			}
		}
	},

	onPosteAncienneteSaveClick() {
		const form = this.getView().down('#posteFormId');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};

		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/centreDepartement/Poste.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		if (serverResponse > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			this.onPosteListe();
		}
	},

	onFormuleClick() {
		this.setCurrentView('formule');
	},

	onAddFormuleClick(record) {
		const targetContainer = this.getView().down('#formuleContainerId');
		
		Ext.suspendLayouts();

		targetContainer.removeAll(true);
		targetContainer.add(
			Ext.apply({
				xtype: 'formuleForm',
				params: record,
			})
		);

		Ext.resumeLayouts(true);
	},

	formuleBackAction() {
		const targetContainer = this.getView().down('#formuleContainerId');

		Ext.suspendLayouts();

		targetContainer.removeAll(true);
		targetContainer.add(
			Ext.apply({
				xtype: 'formuleGrid',
			})
		);

		Ext.resumeLayouts(true);
	},

	onFormuleSaveClick(view) {
		const form = view.up('#formuleFormId');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/formule/Formule.php', model);
		
		if (saveOrUpdate.request.request.result.responseText > 0) {
			this.formuleBackAction();
		}
	},

	onDeleteFormule() {
		const formuleGrid = this.getView().down('#formuleGridId');
		
		const formuleParams = {
			idFormule: formuleGrid.getSelection()[0].get('idFormule'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxFormuleDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelFormule = {
						infoRequest: 3,
						data: formuleParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/formule/Formule.php', modelDelFormule);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxFormuleDelete').close();
						this.formuleBackAction();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxFormuleDelete').close();
				},
			}],
		});
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	onRowFormuleDblClick(view, record) {
		this.onAddFormuleClick(record);
	},

	onRowPosteDblClick(view, record) {
		const btn = this.getView().down('#addPosteBtnId');
		
		this.onAddPoste(btn, record);
	},

	setAncienneteItemId(field) {
		this.getViewModel().set('storeLen', field.getItemId());
	},

	/*
	 * @{param} (view, td, rowIndex, cellIndex, e, record, row)
	 */
	onGrilleSalaireClick(view, td, rowIndex, cellIndex, e, record) {
		const postForm = this.getView().down('#posteFormId');
		const fieldId = this.getViewModel().get('storeLen');
		if (Ext.isEmpty(fieldId)) {
			const msg = 'Cliquez sur une zone de texte d\'ancienneté pour l\'activer, puis choisissez la catégorie professionnelle adéquate.';
			Gprh.ades.util.Helper.showError(msg); 
		} else {
			postForm.down(`#${fieldId}`).setValue(record.get('categorieProfessionnelle'));
		}		
	},

	onDeletePoste() {
		const posteGrid = this.getView().down('#posteGridId');
		
		const posteParams = {
			idPoste: posteGrid.getSelection()[0].get('idPoste'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxPosteDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelPoste = {
						infoRequest: 3,
						data: posteParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/centreDepartement/Poste.php', modelDelPoste);
					const result = saveOrUpdate.request.responseText;
					
					if (result > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
						btn.up('#messageBoxPosteDelete').close();
						posteGrid.getStore().load();
						this.onPosteListe();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxPosteDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxPosteDelete').close();
				},
			}],
		});
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	confirmDeleteCnaps(view, cell, recordIndex, cellIndex, e, record) {
		const cnapsParams = {
			idCnaps: record.get('idCnaps'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxCnapsDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelCnaps = {
						infoRequest: 3,
						data: cnapsParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/cnaps/Cnaps.php', modelDelCnaps);
					const result = saveOrUpdate.request.request.result.responseText;
					if (result > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						btn.up('#messageBoxCnapsDelete').close();
						this.getView().down('#cnapsGridId').getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxCnapsDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxCnapsDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	confirmActifIrsa(column, rowIndex, checked, record) {
		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxIrsaChecked',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelUpdLigneActif = {
						infoRequest: 4,
						idIrsa: record.data.idIrsa,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/irsa/Irsa.php', modelUpdLigneActif);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxIrsaChecked').close();
						this.getView().down('#irsaGridId').getStore().load();
					}			
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxIrsaChecked').close();
					this.getView().down('#irsaGridId').getStore().reload();
				},
			}],
		});
		
		if (checked) {
			messageBox.show({
				title: 'Confirmer l\'enregistrement',
				msg: 'Cette valeur existe déjà. Voulez-vous la remplacer?',
				icon: Ext.MessageBox.WARNING,
			});
		}
	},

	confirmDeleteIrsa(view, cell, recordIndex, cellIndex, e, record) {
		const irsaParams = {
			idIrsa: record.get('idIrsa'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxIrsaDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelIrsa = {
						infoRequest: 3,
						data: irsaParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/irsa/Irsa.php', modelDelIrsa);
					const result = saveOrUpdate.request.request.result.responseText;
					if (result > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						btn.up('#messageBoxIrsaDelete').close();
						this.getView().down('#irsaGridId').getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxIrsaDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxIrsaDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	onDeleteSmie() {
		const smieGrid = this.getView().down('#smieGridId');
		
		const smieParams = {
			idSmie: smieGrid.getSelection()[0].get('idSmie'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxSmieDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelSmie = {
						infoRequest: 3,
						data: smieParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/smie/Smie.php', modelDelSmie);
					const result = saveOrUpdate.request.responseText;
					
					if (result > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						btn.up('#messageBoxSmieDelete').close();
						smieGrid.getStore().load();
						this.onSmieListe();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxSmieDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxSmieDelete').close();
				},
			}],
		});
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},
	
	confirmDeleteHopital(view, cell, recordIndex, cellIndex, e, record) {
		const hopitalParams = {
			idHopital: record.get('idHopital'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxHopitalDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelHopital = {
						infoRequest: 3,
						data: hopitalParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/hopital/Hopital.php', modelDelHopital);
					const result = saveOrUpdate.request.request.result.responseText;
					if (result > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						btn.up('#messageBoxHopitalDelete').close();
						this.getView().down('#hopitalGridId').getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxHopitalDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxHopitalDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},
	
	confirmActifHopital(column, rowIndex, checked, record) {
		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxHopitalChecked',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelUpdLigneActif = {
						infoRequest: 4,
						idHopital: record.data.idHopital,
						idCentre: record.data.idCentre,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/hopital/Hopital.php', modelUpdLigneActif);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxHopitalChecked').close();
						this.getView().down('#hopitalGridId').getStore().load();
					}			
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxHopitalChecked').close();
					this.getView().down('#hopitalGridId').getStore().reload();
				},
			}],
		});
		
		if (checked) {
			messageBox.show({
				title: 'Confirmer l\'enregistrement',
				msg: 'Cette valeur existe déjà. Voulez-vous la remplacer?',
				icon: Ext.MessageBox.WARNING,
			});
		}
	},

	confirmDeleteGroupe(view, cell, recordIndex, cellIndex, e, record) {
		const groupeParams = {
			idGroupeCategorie: record.get('idGroupeCategorie'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxGroupeDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelGroupe = {
						infoRequest: 3,
						data: groupeParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/grille/Groupe.php', modelDelGroupe);
					const result = saveOrUpdate.request.request.result.responseText;
					if (result > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						btn.up('#messageBoxGroupeDelete').close();
						this.getView().down('#groupeGridId').getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxGroupeDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxGroupeDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	onDeleteCategorie() {
		const categorieGrid = this.getView().down('#categorieGridId');
		
		const categorieParams = {
			idCategorie: categorieGrid.getSelection()[0].get('idCategorie'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxCategorieDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelCategorie = {
						infoRequest: 3,
						data: categorieParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/grille/Categorie.php', modelDelCategorie);
					const result = saveOrUpdate.request.responseText;
					
					if (result > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						btn.up('#messageBoxCategorieDelete').close();
						categorieGrid.getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxCategorieDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxCategorieDelete').close();
				},
			}],
		});
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	confirmDeleteGrille(view, cell, recordIndex, cellIndex, e, record) {
		const grilleParams = {
			idLigneGrille: record.get('idLigneGrille'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxGrilleDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelGrille = {
						infoRequest: 5,
						data: grilleParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/grille/SalaireBase.php', modelDelGrille);
					const result = saveOrUpdate.request.request.result.responseText;
					if (result > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						btn.up('#messageBoxGrilleDelete').close();
						this.getView().down('#grilleGridId').getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxGrilleDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxGrilleDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	confirmActifTaux(column, rowIndex, checked, record) {
		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxTauxChecked',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelUpdLigneActif = {
						infoRequest: 4,
						nomTaux: record.data.typeTaux,
						idTaux: record.data.idTaux,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/taux/Taux.php', modelUpdLigneActif);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxTauxChecked').close();
						this.getView().down('#tauxGridId').getStore().load();
					}			
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxTauxChecked').close();
					this.getView().down('#tauxGridId').getStore().reload();
				},
			}],
		});
		
		if (checked) {
			messageBox.show({
				title: 'Confirmer l\'enregistrement',
				msg: 'Cette valeur existe déjà. Voulez-vous la remplacer?',
				icon: Ext.MessageBox.WARNING,
			});
		}
	},

	confirmDeleteTaux(view, cell, recordIndex, cellIndex, e, record) {
		const tauxParams = {
			idTaux: record.get('idTaux'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxTauxDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelTaux = {
						infoRequest: 3,
						data: tauxParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/taux/Taux.php', modelDelTaux);
					const result = saveOrUpdate.request.request.result.responseText;
					if (result > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						btn.up('#messageBoxTauxDelete').close();
						this.getView().down('#tauxGridId').getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxTauxDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxTauxDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	confirmActifTauxHS(column, rowIndex, checked, record) {
		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxTauxHSChecked',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelUpdLigneActif = {
						infoRequest: 4,
						natureHsBase: record.data.typeTauxHS,
						idHsBase: record.data.idHsBase,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/taux/TauxHS.php', modelUpdLigneActif);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxTauxHSChecked').close();
						this.getView().down('#tauxHSGridId').getStore().load();
					}			
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxTauxHSChecked').close();
					this.getView().down('#tauxHSGridId').getStore().reload();
				},
			}],
		});
		
		if (checked) {
			messageBox.show({
				title: 'Confirmer l\'enregistrement',
				msg: 'Cette valeur existe déjà. Voulez-vous la remplacer?',
				icon: Ext.MessageBox.WARNING,
			});
		}
	},

	confirmDeleteTauxHS(view, cell, recordIndex, cellIndex, e, record) {
		const tauxParams = {
			idHsBase: record.get('idHsBase'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxTauxHSDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelTaux = {
						infoRequest: 3,
						data: tauxParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/taux/TauxHS.php', modelDelTaux);
					const result = saveOrUpdate.request.request.result.responseText;
					if (result > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successDelMsg);
						btn.up('#messageBoxTauxHSDelete').close();
						this.getView().down('#tauxHSGridId').getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxTauxHSDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxTauxHSDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},
});
