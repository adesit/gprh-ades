Ext.define('Gprh.ades.view.ades.structures.StructuresModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.structuresModel',

	requires: [
		'Gprh.ades.store.Centre',
		'Gprh.ades.store.Departement',
		'Gprh.ades.store.Section',
		'Gprh.ades.store.SalaireBase',
		'Gprh.ades.store.Poste',
	],

	stores: {
		centreResults: {
			type: 'centreStore',
		},

		departementResults: {
			type: 'departementStore',
		},

		sectionResults: {
			type: 'sectionStore',
		},

		salaireResults: {
			type: 'salaireBaseStore',
			proxy: {
				extraParams: {
					infoRequest: 1,
				},
			},
		},

		posteResults: {
			type: 'posteStore',
			proxy: {
				extraParams: {
					infoRequest: 1,
				},
			},
		},
	},

	formulas: {
		disabledSaveBtn: {
			bind: {
				y: '{!grilleInPosteGridRef.selection}',
				x: '{!titrePosteRef.value}',
			},

			get: data => data.x || data.y,
		},
		isEnabledDelPoste: {
			bind: {
				t1: '{!posteGridRef.selection}',
			},
			// 1 poste est sélectionné
			get: d => d.t1,
		},
	},
});
