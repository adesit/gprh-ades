Ext.define('Gprh.ades.view.ades.structures.PosteGrid', {
	extend: 'Ext.grid.Panel',
	xtype: 'posteGrid',
	itemId: 'posteGridId',

	requires: [
		'Gprh.ades.util.Globals',
	],

	useArrows: true,
	layout: 'fit',
	bind: {
		store: '{posteResults}',
	},
	height: Ext.Element.getViewportHeight() - 175,

	tbar: [],

	viewConfig: {
		preserveScrollOnRefresh: true,
		stripeRows: true,
	},
	
	selModel: 'rowmodel',
	listeners: {
		rowdblclick: 'onRowPosteDblClick',
	},
	
	columnLines: true,
	columns: {
		defaults: {				
			align: 'left',
		},
		items: [
			{
				dataIndex: 'titrePoste',
				header: 'Titre',
				width: 350,
				// locked: true, => x-rtl ext-element-126 + 128 lockedScrollbar, lockedScrollbarClipper
				locked: true,
			},
			{
				header: 'Catégorie professionnelle par ancienneté',
				defaults: {	
					width: 120,
				},
				columns: [
					{
						dataIndex: 'un',
						header: '[0-1[ans',
					},
					{
						dataIndex: 'deux',
						header: '[1-2[ans',
					},
					{
						dataIndex: 'trois',
						header: '[2-3[ans',
					},
					{
						dataIndex: 'quatre',
						header: '[3-4[ans',
					},
					{
						dataIndex: 'cinq',
						header: '[4-5[ans',
					},
					{
						dataIndex: 'six',
						header: '[5-6[ans',
					},
					{
						dataIndex: 'sept',
						header: '[6-7[ans',
					},
					{
						dataIndex: 'huit',
						header: '[7-8[ans',
					},
					{
						dataIndex: 'neuf',
						header: '[8-9[ans',
					},
					{
						dataIndex: 'dix',
						header: '[9-10[ans',
					},
					{
						dataIndex: 'vingt',
						header: '[10-20[ans',
					},
				],
			},
		],
	},
	dockedItems: [
		{
			xtype: 'pagingtoolbar',
			dock: 'bottom',
			itemId: 'postePaginationToolbar',
			displayInfo: true,
			bind: {
				store: '{posteResults}',
			},
		},
	],

	initComponent() {
		this.callParent();
	},
});
