Ext.define('Gprh.ades.view.ades.structures.PosteForm', {
	extend: 'Ext.panel.Panel',
	xtype: 'posteForm',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.store.Poste',
	],

	viewModel: {
		type: 'structuresModel',
	},

	formWidth: 850,
	height: Ext.Element.getViewportHeight() - 195,
	autoScroll: true,

	initComponent() {
		const data = Ext.isEmpty(this.params.data) ? null : this.params;

		const gridGrille = {
			xtype: 'gridpanel',
			cls: 'user-grid',
			routeId: 'posteTabId',
			itemId: 'grilleInPosteGridId',
			reference: 'grilleInPosteGridRef',
			selModel: Ext.create('Ext.selection.RowModel'),
			bind: {
				store: '{salaireResults}',
			},
			height: 450,
			flex: 1,
			columnLines: true,
			stripeRows: true,
			columns: [
				{
					dataIndex: 'idLigneGrille',
					text: 'idLigneGrille',
					align: 'left',
					autoSizeColumn: true,
					hidden: true,
					cellFocusable: false,
				},
				{
					dataIndex: 'nomGroupe',
					text: 'Groupe',
					align: 'left',
					autoSizeColumn: true,
					cellFocusable: false,
				},
				{
					dataIndex: 'idGroupe',
					text: '# Groupe',
					align: 'left',
					autoSizeColumn: true,
					hidden: true,
					cellFocusable: false,
				},
				{
					dataIndex: 'categorieProfessionnelle',
					text: 'Catégorie',
					align: 'left',
					width: 120,
					listeners: {
						click: 'onGrilleSalaireClick',
					},
				},
				{
					dataIndex: 'idCategorie',
					text: '# Catégorie',
					align: 'left',
					autoSizeColumn: true,
					hidden: true,
					cellFocusable: false,
				},
				{
					dataIndex: 'salaireBaseMinimum',
					text: 'Base minimum',
					align: 'left',
					autoSizeColumn: true,
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
					cellFocusable: false,
				},
				{
					dataIndex: 'salaireBase3',
					text: 'Base si ancienneté > 3',
					align: 'left',
					minWidth: 120,
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
					cellFocusable: false,
				},
				{
					dataIndex: 'salaireBase4',
					text: 'Base si ancienneté > 4',
					align: 'left',
					minWidth: 120,
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
					cellFocusable: false,
				},
				{
					dataIndex: 'salaireBase5',
					text: 'Base si ancienneté > 5',
					align: 'left',
					minWidth: 120,
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
					cellFocusable: false,
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'datePriseEffetGrille',
					text: 'Dernière modification',
					align: 'left',
					format: 'd/m/Y',
					width: 100,
					cellFocusable: false,
				},
			],
			dockedItems: [
				Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
			],
			tbar: [
				'->',
				{
					text: 'Grille salariale actuelle',
					style: {
						background: 'transparent',
						border: 'none',
					},
				},
			],
		};

		const postForm = {
			xtype: 'container',
			layout: {
				type: 'hbox',
				align: 'stretch',
			},
			items: [
				{
					xtype: 'form',
					itemId: 'posteFormId',
					defaults: {
						labelSeparator: ':',
						submitEmptyText: false,
					},
					items: [
						{
							xtype: 'container',
							itemId: 'postSubFormId',
							layout: 'column',
							items: [
								{
									columnWidth: '0.50',
									items: [
										{
											xtype: 'textfield',
											name: 'idPoste',
											itemId: 'idPosteId',
											fieldLabel: '# poste',
											labelAlign: 'top',
											hidden: true,
											value: Ext.isEmpty(data) ? '' : data.get('idPoste'),
										},
										{
											xtype: 'textfield',
											name: 'titrePoste',
											itemId: 'titrePosteId',
											fieldLabel: 'Titre du poste',
											labelAlign: 'top',
											reference: 'titrePosteRef',
											publishes: 'value',
											minWidth: 350,	
											allowBlank: false,
											value: Ext.isEmpty(data) ? '' : data.get('titrePoste'),
										},
										{
											xtype: 'textfield',
											name: 'tauxHoraire',
											itemId: 'tauxHoraireId',
											fieldLabel: 'Heure réglementaire',
											labelAlign: 'top',
											reference: 'tauxHoraireRef',
											publishes: 'value',
											minWidth: 350,	
											allowBlank: false,
											value: Ext.isEmpty(data) ? '' : data.get('tauxHoraire'),
											listeners: {
												blur: self => Gprh.ades.util.Globals.formatNumber(self),
											},
										},
										{
											html: '<span class="x-fa fa-info-circle"></span> eg:Agent d\'entretien et de sécurité 1B;2A;2B;3A',
											baseCls: 'x-toast-info',
											margin: '5 0 0 0',
										},
										{
											xtype: 'textfield',
											name: 'categorieReelle',
											itemId: 'categorieReelleId',
											fieldLabel: 'Catégories réelles à afficher (séparez les valeurs par des point-virgules)',
											labelAlign: 'top',
											reference: 'categorieReelleRef',
											publishes: 'value',
											width: 350,	
											allowBlank: false,
											value: Ext.isEmpty(data) ? '' : data.get('categorieReelle'),
										},
									],
								},
								{
									columnWidth: '0.50',
									items: [
										{
											xtype: 'htmleditor',
											enableColors: false,
											enableFontSize: false,
											enableSourceEdit: false,
											name: 'descriptionPoste',
											itemId: 'descriptionPosteId',
											fieldLabel: 'Description',
											labelAlign: 'top',
											height: 250,
											width: 475,
											margin: '0 0 0 10',
											value: Ext.isEmpty(data) ? '' : data.get('descriptionPoste'),
										},
									],
								},							
							],
						},
						{
							html: '<span class="x-fa fa-info-circle"></span> Cliquez sur une zone de texte d\'ancienneté pour l\'activer, puis choisissez en cliquant dans la céllule adéquate la catégorie professionnelle à ce poste et à l\'ancienneté définie.',
							baseCls: 'x-toast-info',
							maxWidth: 800,
							margin: '5 0 0 0',
						},
						{
							xtype: 'fieldset',
							title: 'Evolution de salaire par ancienneté',
							itemId: 'postSubCategoriesId',
							style: {
								background: 'transparent',
							},
							layout: 'hbox',
							defaultType: 'textfield',
							defaults: {
								width: 110,
								labelAlign: 'top',
								editable: false,
								listeners: {
									focusenter: 'setAncienneteItemId',
								},
							},
							maxWidth: 800,
							scrollable: true,
							items: [
								{
									name: 'un',
									itemId: 'unId',
									fieldLabel: '[0-1[ans',
									value: Ext.isEmpty(data) ? '' : data.get('un'),
									allowBlank: false,
								},
								{
									name: 'deux',
									itemId: 'deuxId',
									fieldLabel: '[1-2[ans',
									value: Ext.isEmpty(data) ? '' : data.get('deux'),
									allowBlank: false,
								},
								{
									name: 'trois',
									itemId: 'troisId',
									fieldLabel: '[2-3[ans',
									value: Ext.isEmpty(data) ? '' : data.get('trois'),
									allowBlank: false,
								},
								{
									name: 'quatre',
									itemId: 'quatreId',
									fieldLabel: '[3-4[ans',
									value: Ext.isEmpty(data) ? '' : data.get('quatre'),
									allowBlank: false,
								},
								{
									name: 'cinq',
									itemId: 'cinqId',
									fieldLabel: '[4-5[ans',
									value: Ext.isEmpty(data) ? '' : data.get('cinq'),
									allowBlank: false,
								},
								{
									name: 'six',
									itemId: 'sixId',
									fieldLabel: '[5-6[ans',
									value: Ext.isEmpty(data) ? '' : data.get('six'),
									allowBlank: false,
								},
								{
									name: 'sept',
									itemId: 'septId',
									fieldLabel: '[6-7[ans',
									value: Ext.isEmpty(data) ? '' : data.get('sept'),
									allowBlank: false,
								},
								{
									name: 'huit',
									itemId: 'huitId',
									fieldLabel: '[7-8[ans',
									value: Ext.isEmpty(data) ? '' : data.get('huit'),
									allowBlank: false,
								},
								{
									name: 'neuf',
									itemId: 'neufId',
									fieldLabel: '[8-9[ans',
									value: Ext.isEmpty(data) ? '' : data.get('neuf'),
									allowBlank: false,
								},
								{
									name: 'dix',
									itemId: 'dixId',
									fieldLabel: '[9-10[ans',
									value: Ext.isEmpty(data) ? '' : data.get('dix'),
									allowBlank: false,
								},
								{
									name: 'vingt',
									itemId: 'vingtId',
									fieldLabel: '[10-20[ans',
									value: Ext.isEmpty(data) ? '' : data.get('vingt'),
									allowBlank: false,
								},
							],
						},						
						{
							xtype: 'toolbar',
							cls: 'wizard-form-break',
							layout: 'hbox',
							items: [
								{
									itemId: 'posteSaveBtn',
									xtype: 'button',
									text: 'Enregistrer',
									ui: 'soft-green-small',
									margin: '10 10 0 0',
									formBind: true,
									/* bind: {
										disabled: '{disabledSaveBtn}',
									}, */
									listeners: {
										// click: 'onPosteSaveClick',
										click: 'onPosteAncienneteSaveClick',
									},
									hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
								},
								{
									itemId: 'posteCancelBtn',
									xtype: 'button',
									text: 'Annuler',
									ui: 'soft-blue-small',
									margin: '10 0 0 0',
									listeners: {
										click: 'cancelAction',
									},
									params: 'posteFormId', 
								},
							],
						},
					],
				},
			],
		};

		this.items = [
			{
				xtype: 'container',
				layout: {
					type: 'vbox',
					align: 'stretch',
				},
				items: [
					postForm,
					gridGrille,
				],
			},
		];

		this.callParent();
	},
});
