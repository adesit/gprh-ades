Ext.define('Gprh.ades.view.ades.structures.PosteMainContainer', {
	extend: 'Ext.Panel',
	xtype: 'posteMainContainer',
	alias: 'posteMainContainer',

	requires: [
		'Gprh.ades.view.ades.structures.PosteGrid',
	],

	controller: 'adesController',

	cls: 'card',

	// @private
	initComponent() {
		this.items = [
			{
				xtype: 'toolbar',
				docked: 'top',
				scrollable: {
					y: false,
				},
				margin: '0 0 5 0',
				items: [
					{
						xtype: 'button',
						itemId: 'listePostBtnId',
						tooltip: 'Voir la liste de tous les postes',
						text: 'Liste des postes',
						handler: 'onPosteListe',
						// hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
					},
					{
						xtype: 'button',
						itemId: 'addPosteBtnId',
						tooltip: 'Créer un nouveau poste',
						text: 'Ajouter',
						handler: 'onAddPoste',
						hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
					},
					'->',
					{
						tooltip: 'Supprimer un poste',
						text: 'Supprimer',
						iconCls: 'fa fa-trash',
						margin: '0 0 10 0',
						ui: 'soft-red-small',
						listeners: {
							click: 'onDeletePoste',
						},
						bind: {
							disabled: '{isEnabledDelPoste}',
						},
						hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
					},
				],
			},
			{
				xtype: 'container',
				itemId: 'posteMainContainerId',
				flex: 1,
				layout: {
					type: 'anchor',
					anchor: '100%',
				},
				items: [
					{
						xtype: 'posteGrid',
						reference: 'posteGridRef',
					},
				],
			},
		];
		this.callParent();
	},
});
