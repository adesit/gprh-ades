Ext.define('Gprh.ades.view.ades.structures.StructureMainContainer', {
	extend: 'Ext.tab.Panel',
	xtype: 'structures',

	requires: [
		'Ext.grid.Panel',
		'Ext.toolbar.Paging',
		'Ext.grid.column.Date',
		'Gprh.ades.util.Globals',
		'Gprh.ades.view.ades.structures.StructuresModel',
		'Gprh.ades.model.CentreModel',
		'Gprh.ades.model.DepartementModel',
		'Gprh.ades.model.SectionModel',
		'Gprh.ades.view.ades.structures.PosteMainContainer',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	controller: 'adesController',

	viewModel: {
		type: 'structuresModel',
	},

	cls: 'shadow',
	activeTab: 0,
	bodyPadding: 5,

	// grid default height
	gridHeight: Ext.Element.getViewportHeight() - 125,

	initComponent() {
		const rowEditing = Gprh.ades.util.Globals.setRowEditingConfig('server-scripts/centreDepartement/Centre.php', this);

		const gridCentre = {
			xtype: 'gridpanel',
			cls: 'user-grid',
			title: 'Centres',
			routeId: 'centreTabId',
			listeners: {
				itemmouseenter: (view, record, item) => {
					Ext.fly(item).set({ 'data-qtip': 'Double clic dessus pour l\'éditer.' });
				},
			},
			itemId: 'centreGridId',
			bind: {
				store: '{centreResults}',
			},
			height: this.gridHeight,
			columnLines: true,
			stripeRows: true,
			columns: [
				{
					xtype: 'gridcolumn',
					width: 40,
					dataIndex: 'idCentre',
					text: '#',
					hidden: true,
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'abreviationCentre',
					text: 'Abréviation',
					align: 'left',
					width: 100,
					editor: {
						allowBlank: false,
					},
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'nomCentre',
					text: 'Nom du Centre',
					align: 'left',
					flex: 1,
					editor: {
						allowBlank: false,
					},
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'adresseCentre',
					text: 'Adresse du Centre',
					align: 'left',
					flex: 1,
					editor: {
						allowBlank: false,
					},
				},
				{
					xtype: 'actioncolumn',
					cls: 'content-column',
					width: 80,
					text: 'Actions',
					items: [
						{
							iconCls: 'x-fa fa-trash soft-red-small',
							handler: 'confirmDeleteCentre',
						},
					],
					hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),					
				},
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					dock: 'bottom',
					itemId: 'centrePaginationToolbar',
					displayInfo: true,
					bind: {
						store: '{centreResults}',
					},
				},
			],
			tbar: [
				'->', 
				{
					tooltip: 'Créer un nouveau centre',
					text: 'Nouveau centre',
					iconCls: 'fa fa-plus',
					margin: '0 0 10 0',
					handler: () => {
						rowEditing.cancelEdit();
										
						const r = Ext.create('Gprh.ades.model.CentreModel', {
							idCentre: '',
							abreviationCentre: '',
							nomCentre: '',
							adresseCentre: '',
						});
	
						this.down('#centreGridId').getStore().insert(0, r);
						rowEditing.startEdit(0, 0);
					},
					hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
				}],
			plugins: [rowEditing],
		};

		const rowEditingDepartement = Gprh.ades.util.Globals.setRowEditingConfig('server-scripts/centreDepartement/Departement.php', this);

		const gridDepartement = {
			xtype: 'gridpanel',
			cls: 'user-grid',
			title: 'Départements',
			itemId: 'departementGridId',
			routeId: 'departementTabId',
			listeners: {
				itemmouseenter: (view, record, item) => {
					Ext.fly(item).set({ 'data-qtip': 'Double clic dessus pour l\'éditer.' });
				},
			},
			bind: {
				store: '{departementResults}',
			},
			height: this.gridHeight,
			stripeRows: true,
			columnLines: true,
			columns: [
				{
					xtype: 'gridcolumn',
					width: 40,
					dataIndex: 'idDepartement',
					text: '#',
					hidden: true,
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'nomDepartement',
					text: 'Nom du département',
					align: 'left',
					flex: 1,
					editor: {
						allowBlank: false,
					},
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'abreviationDepartement',
					text: 'Abréviation',
					align: 'left',
					flex: 1,
					editor: {
						allowBlank: false,
					},
				},
				{
					xtype: 'actioncolumn',
					cls: 'content-column',
					width: 80,
					text: 'Actions',
					items: [
						{
							iconCls: 'x-fa fa-trash soft-red-small',
							handler: 'confirmDeleteDepartement',
						},
					],
					hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),					
				},
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					dock: 'bottom',
					itemId: 'departementPaginationToolbar',
					displayInfo: true,
					bind: {
						store: '{departementResults}',
					},
				},
			],
			tbar: [
				'->', 
				{
					tooltip: 'Créer un nouveau département',
					text: 'Nouveau département',
					iconCls: 'fa fa-plus',
					margin: '0 0 10 0',
					handler: () => {
						rowEditingDepartement.cancelEdit();
										
						const r = Ext.create('Gprh.ades.model.DepartementModel', {
							idDepartement: '',
							nomDepartement: '',
							abreviationDepartement: '',
						});
	
						this.down('#departementGridId').getStore().insert(0, r);
						rowEditingDepartement.startEdit(0, 0);
					},
					hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
				}],
			plugins: [rowEditingDepartement],
		};

		const rowEditingSection = Gprh.ades.util.Globals.setRowEditingConfig('server-scripts/centreDepartement/Section.php', this);

		const gridSection = {
			xtype: 'gridpanel',
			cls: 'user-grid',
			title: 'Sections',
			itemId: 'sectionGridId',
			routeId: 'sectionTabId',
			listeners: {
				itemmouseenter: (view, record, item) => {
					Ext.fly(item).set({ 'data-qtip': 'Double clic dessus pour l\'éditer.' });
				},
			},
			bind: {
				store: '{sectionResults}',
			},
			height: this.gridHeight,
			stripeRows: true,
			columnLines: true,
			columns: [
				{
					xtype: 'gridcolumn',
					width: 40,
					dataIndex: 'idSection',
					text: '#',
					hidden: true,
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'nomSection',
					text: 'Nom du section',
					align: 'left',
					flex: 1,
					editor: {
						allowBlank: false,
					},
				},
				{
					xtype: 'actioncolumn',
					cls: 'content-column',
					width: 80,
					text: 'Actions',
					items: [
						{
							iconCls: 'x-fa fa-trash soft-red-small',
							handler: 'confirmDeleteSection',
						},
					],
					hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),				
				},
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					dock: 'bottom',
					itemId: 'sectionPaginationToolbar',
					displayInfo: true,
					bind: {
						store: '{sectionResults}',
					},
				},
			],
			tbar: [
				'->', 
				{
					tooltip: 'Créer une nouvelle section',
					text: 'Nouvelle section',
					iconCls: 'fa fa-plus',
					margin: '0 0 10 0',
					handler: () => {
						rowEditingSection.cancelEdit();
										
						const r = Ext.create('Gprh.ades.model.SectionModel', {
							idSection: '',
							nomSection: '',
						});
	
						this.down('#sectionGridId').getStore().insert(0, r);
						rowEditingSection.startEdit(0, 0);
					},
					hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
				}],
			plugins: [rowEditingSection],
		};

		const postMainPanel = {
			xtype: 'container',
			itemId: 'postContainerId',
			title: 'Postes de travail',
			layout: {
				type: 'vbox',
				align: 'stretch',
			},
			items: [
				{
					xtype: 'posteMainContainer',
				},
			],
		};

		this.items = [
			gridCentre,
			gridDepartement,
			gridSection,
			postMainPanel,
		];
		this.layout = 'fit';

		this.callParent();
	},
});
