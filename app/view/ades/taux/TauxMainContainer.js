Ext.define('Gprh.ades.view.ades.taux.TauxMainContainer', {
	extend: 'Ext.tab.Panel',
	xtype: 'taux',

	requires: [
		'Ext.grid.Panel',
		'Ext.toolbar.Paging',
		'Ext.grid.column.Date',
		'Gprh.ades.util.Globals',
		'Gprh.ades.view.ades.taux.TauxViewModel',
		'Gprh.ades.model.TauxModel',
		'Gprh.ades.store.TauxListe',
		'Gprh.ades.model.TauxHSModel',
		'Gprh.ades.store.TauxHSListe',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	controller: 'adesController',

	viewModel: {
		type: 'tauxViewModel',
	},

	cls: 'shadow',
	activeTab: 0,
	bodyPadding: 5,

	// grid default height
	gridHeight: 450,

	initComponent() {
		const rowEditing = Gprh.ades.util.Globals.setRowEditingConfig('server-scripts/taux/Taux.php', this);
		const rowHSEditing = Gprh.ades.util.Globals.setRowEditingConfig('server-scripts/taux/TauxHS.php', this);

		const typeTauxstore = Ext.create('Gprh.ades.store.TauxListe');
		const typeTauxHSstore = Ext.create('Gprh.ades.store.TauxHSListe');
		const tauxStore = Ext.create('Gprh.ades.store.Taux', {
			proxy: {
				extraParams: {
					infoRequest: 1,
				},
			},
		});
		const augmentationGrid = {
			xtype: 'gridpanel',
			cls: 'user-grid',
			title: 'Divers taux et paramètres',
			routeId: 'tauxTabId',
			itemId: 'tauxGridId',
			stripeRows: true,
			store: tauxStore,
			height: Ext.Element.getViewportHeight() - 130,
			columnLines: true,
			columns: [
				{
					xtype: 'gridcolumn',
					width: 40,
					dataIndex: 'idTaux',
					text: '#',
					hidden: true,
				},
				{
					xtype: 'gridcolumn',
					width: 40,
					dataIndex: 'typeTaux',
					text: '#',
					hidden: false,
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'nomTaux',
					text: 'Appelation du taux',
					flex: 2,
					editor: {
						xtype: 'combobox',
						store: typeTauxstore,
						queryMode: 'local',
						displayField: 'nomTaux',
						valueField: 'id',
						renderTo: Ext.getBody(),
						allowBlank: false, 
						forceSelection: true,
						editable: false,
					},
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'valeurPourcent',
					text: 'Valeur',
					align: 'right',
					flex: 1,
					editor: {
						allowBlank: false,
						listeners: {
							blur: self => Gprh.ades.util.Globals.formatNumber(self),
						},
					},
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
				},
				{
					xtype: 'datecolumn',
					header: 'Date prise d\'effet',
					dataIndex: 'datePriseEffetTaux',
					width: 135,
					format: 'd/m/Y',
					editor: {
						xtype: 'datefield',
						allowBlank: false,
						format: 'd/m/Y',
						minText: 'Date de changement!',
						submitFormat: 'Y-m-d',		
					},
				},
				{
					xtype: 'checkcolumn',
					header: 'Actif',
					dataIndex: 'actifTaux',
					width: 60,
					editor: {
						xtype: 'checkboxfield',
						cls: 'x-grid-checkheader-editor',
					},
					listeners: {
						checkchange: 'confirmActifTaux',
					},
				},
				{
					xtype: 'actioncolumn',
					cls: 'content-column',
					width: 80,
					text: 'Actions',
					items: [
						{
							iconCls: 'x-fa fa-trash soft-red-small',
							handler: 'confirmDeleteTaux',
						},
					],					
				},
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					dock: 'bottom',
					itemId: 'tauxPaginationToolbar',
					displayInfo: true,
					store: tauxStore,
				},
			],
			tbar: [
				'->', 
				{
					text: 'Nouveau taux',
					iconCls: 'fa fa-plus',
					margin: '0 0 10 0',
					handler: () => {
						rowEditing.cancelEdit();
										
						const r = Ext.create('Gprh.ades.model.TauxModel', {
							idTaux: '',
							nomTaux: '',
							valeurPourcent: 0,
							datePriseEffetTaux: Ext.Date.clearTime(new Date()),
							actifTaux: false,
						});
						
						this.down('#tauxGridId').getStore().insert(0, r);
						rowEditing.startEdit(0, 0);
					},
				},
			],
			plugins: [rowEditing],
			listeners: {
				/* beforeedit: (editor, context) => {
					if (Ext.isEmpty(context.record.get('nomTaux'))) {
						return true;
					}								
					return false;
				}, */
			},
		};

		const HSTauxGrid = {
			xtype: 'gridpanel',
			cls: 'user-grid',
			title: 'Taux des heures supplémentaires',
			routeId: 'tauxHSTabId',
			itemId: 'tauxHSGridId',
			stripeRows: true,
			bind: {
				store: '{tauxHSResults}',
			},
			height: Ext.Element.getViewportHeight() - 130,
			columnLines: true,
			columns: [
				{
					xtype: 'gridcolumn',
					width: 40,
					dataIndex: 'idHsBase',
					text: '#',
					hidden: true,
				},
				{
					xtype: 'gridcolumn',
					width: 40,
					dataIndex: 'typeTauxHS',
					text: '#',
					hidden: false,
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'natureHsBase',
					text: 'Type de HS',
					flex: 1,
					editor: {
						xtype: 'combobox',
						store: typeTauxHSstore,
						queryMode: 'local',
						displayField: 'nomTaux',
						valueField: 'id',
						renderTo: Ext.getBody(),
						allowBlank: false, 
						forceSelection: true,
						editable: false,
					},
				},
				{
					xtype: 'gridcolumn',
					cls: 'content-column',
					dataIndex: 'formuleCalcul',
					text: 'Majoration (%)',
					align: 'right',
					flex: 1,
					editor: {
						allowBlank: false,
						listeners: {
							blur: self => Gprh.ades.util.Globals.formatNumber(self),
						},
					},
					renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
				},
				{
					xtype: 'datecolumn',
					header: 'Date prise d\'effet',
					dataIndex: 'dateEffetHs',
					width: 135,
					format: 'd/m/Y',
					editor: {
						xtype: 'datefield',
						allowBlank: false,
						format: 'd/m/Y',
						minText: 'Date de changement!',
						submitFormat: 'Y-m-d',		
					},
				},
				{
					xtype: 'checkcolumn',
					header: 'Actif',
					dataIndex: 'actifHsBase',
					width: 60,
					editor: {
						xtype: 'checkboxfield',
						cls: 'x-grid-checkheader-editor',
					},
					listeners: {
						checkchange: 'confirmActifTauxHS',
					},
				},
				{
					xtype: 'actioncolumn',
					cls: 'content-column',
					width: 80,
					text: 'Actions',
					items: [
						{
							iconCls: 'x-fa fa-trash soft-red-small',
							handler: 'confirmDeleteTauxHS',
						},
					],					
				},
			],
			dockedItems: [
				{
					xtype: 'pagingtoolbar',
					dock: 'bottom',
					itemId: 'tauxPaginationToolbar',
					displayInfo: true,
					bind: {
						store: '{tauxHSResults}',
					},
				},
			],
			tbar: [
				'->', 
				{
					text: 'Nouveau taux',
					iconCls: 'fa fa-plus',
					margin: '0 0 10 0',
					handler: () => {
						rowHSEditing.cancelEdit();
										
						const r = Ext.create('Gprh.ades.model.TauxHSModel', {
							idHsBase: '',
							natureHsBase: '',
							formuleCalcul: 0,
							dateEffetHs: Ext.Date.clearTime(new Date()),
							actifHsBase: false,
						});
						
						this.down('#tauxHSGridId').getStore().insert(0, r);
						rowHSEditing.startEdit(0, 0);
					},
				},
			],
			plugins: [rowHSEditing],
			listeners: {
				beforeedit: (editor, context) => {
					if (Ext.isEmpty(context.record.get('natureHsBase'))) {
						return true;
					}								
					return false;
				},
			},
		};

		this.items = [
			augmentationGrid,
			HSTauxGrid,
		];
		this.layout = 'fit';

		this.callParent();
	},
});
