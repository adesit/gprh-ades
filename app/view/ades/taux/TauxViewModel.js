Ext.define('Gprh.ades.view.ades.taux.TauxViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.tauxViewModel',

	requires: [
		'Gprh.ades.store.Taux',
		'Gprh.ades.store.TauxHS',
	],

	stores: {
		tauxResults: {
			type: 'tauxStore',
			proxy: {
				extraParams: {
					infoRequest: 1,
				},
			},
		},
		tauxHSResults: {
			type: 'tauxHSStore',
			proxy: {
				extraParams: {
					infoRequest: 1,
				},
			},
		},
	},
});
