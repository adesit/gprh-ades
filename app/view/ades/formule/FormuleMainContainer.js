Ext.define('Gprh.ades.view.ades.formule.FormuleMainContainer', {
	extend: 'Ext.tab.Panel',
	xtype: 'formule',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.view.ades.formule.FormuleGrid',
		'Gprh.ades.view.ades.formule.FormuleViewModel',	
	],

	controller: 'adesController',

	viewModel: {
		type: 'formuleViewModel',
	},

	cls: 'shadow',
	activeTab: 0,
	bodyPadding: 5,
	
	initComponent() {
		this.items = [
			{
				xtype: 'container',
				title: 'Formules de calcul',
				itemId: 'formuleContainerId',
				items: [
					{ xtype: 'formuleGrid' },
				],
			},
			{
				xtype: 'panel',
				title: 'Algorithme de calcul',
				itemId: 'algorithmeContainerId',
				height: Ext.Element.getViewportHeight() - 152,
				autoScroll: true,
				items: [
					{
						xtype: 'image',
						height: '75%',
						src: 'resources/images/20190328rnFormules.png',
					},
				],
			},
		];
		this.callParent();
	},
});
