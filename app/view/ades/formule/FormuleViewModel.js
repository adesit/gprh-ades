Ext.define('Gprh.ades.view.ades.formule.FormuleViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.formuleViewModel',

	requires: [
		'Gprh.ades.store.Formule',
	],

	stores: {
		formuleResults: {
			type: 'formuleStore',
		},
	},
	formulas: {
		isEnabledDelFormule: {
			bind: {
				t1: '{!formuleGridRef.selection}',
			},
			// 1 poste est sélectionné
			get: d => d.t1,
		},
	},
});
