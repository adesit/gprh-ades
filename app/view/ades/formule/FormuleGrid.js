Ext.define('Gprh.ades.view.ades.formule.FormuleGrid', {
	extend: 'Ext.grid.Panel',
	xtype: 'formuleGrid',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.view.ades.formule.FormuleViewModel',
	],

	viewModel: {
		type: 'formuleViewModel',
	},

	
	cls: 'user-grid',
	itemId: 'formuleGridId',
	reference: 'formuleGridRef',
	bind: {
		store: '{formuleResults}',
	},
	minHeight: Ext.Element.getViewportHeight() - 125,
	maxHeight: Ext.Element.getViewportHeight() - 125,
	viewConfig: {
		preserveScrollOnRefresh: true,
		stripeRows: true,
	},
	scrollable: true,
	hideHeaders: true,
	selModel: 'rowmodel',

	listeners: {
		rowdblclick: 'onRowFormuleDblClick',
	},

	stripeRows: true,
	columnLines: true,
	columns: [
		{
			xtype: 'gridcolumn',
			text: '#',
			align: 'left',
			flex: 1,
			renderer(value, metaData, record) {
				const page = `<div class='resultsItemCls'>								
								<div style="font-weight: bold">${this.setIcon(record.data.actifFormule)} ${record.data.resultat}</div>
								<div style="color: #2eadf5; white-space: normal">${record.data.formuleCalcul}</div>
								<div class='resultsContentCls' style="white-space: normal">${record.data.descriptionFormule}</div>
								<div style="font-style: italic;">${record.data.dateApplication}</div>
								</div>`;
								
				return page;
			},
		},
	],
	dockedItems: [
		{
			xtype: 'pagingtoolbar',
			dock: 'bottom',
			itemId: 'formulePaginationToolbar',
			displayInfo: true,
			bind: {
				store: '{formuleResults}',
			},
		},
	],
	tbar: [
		{
			text: 'Nouvelle formule',
			iconCls: 'fa fa-plus',
			margin: '0 0 10 0',
			listeners: {
				click: 'onAddFormuleClick',
			},
		},
		'->',
		{
			text: 'Supprimer',
			iconCls: 'fa fa-trash',
			margin: '0 0 10 0',
			ui: 'soft-red-small',
			listeners: {
				click: 'onDeleteFormule',
			},
			bind: {
				disabled: '{isEnabledDelFormule}',
			},
		},
	],

	setIcon(value) {
		let icon = 'x-fa fa-ban';
		let toolTip = 'Formule expirée.';
		if (value > 0) {
			icon = 'x-fa fa-check';
			toolTip = 'Formule utilisée actuel.';
		}
		return `<span data-ref="btnIconEl" class="x-action-col-icon x-action-col-0 ${icon}" tooltip="${toolTip}" style=""></span>`;
	},
});
