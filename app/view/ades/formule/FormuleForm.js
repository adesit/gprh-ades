Ext.define('Gprh.ades.view.ades.formule.FormuleForm', {
	extend: 'Ext.panel.Panel',
	xtype: 'formuleForm',

	requires: [
		'Gprh.ades.util.Globals',
		'Ext.ux.layout.ResponsiveColumn',
	],

	viewModel: {
		type: 'structuresModel',
	},

	formWidth: 800,

	initComponent() {
		const data = Ext.isEmpty(this.params.data) ? null : this.params;
		
		this.items = [
			{
				xtype: 'form',
				itemId: 'formuleFormId',
				minWidth: this.formWidth,
				items: [
					{
						xtype: 'container',
						itemId: 'postSubFormId',
						layout: 'responsivecolumn',
						defaults: {
							labelSeparator: ':',
							submitEmptyText: false,
							margin: '0 0 0 10',
							labelAlign: 'top',
						},				
						items: [
							{
								xtype: 'textfield',
								name: 'idFormule',
								itemId: 'idFormuleId',
								fieldLabel: '# formule',
								hidden: true,
								value: Ext.isEmpty(data) ? '' : data.get('idFormule'),
							},
							{
								xtype: 'textfield',
								name: 'idAdes',
								itemId: 'idAdesId',
								fieldLabel: '# ADES',
								hidden: true,
								value: 1,
							},
							{
								xtype: 'textfield',
								name: 'resultat',
								itemId: 'resultatId',
								fieldLabel: 'Résultat',
								reference: 'resultatRef',
								publishes: 'value',
								minWidth: 350,	
								allowBlank: false,
								value: Ext.isEmpty(data) ? '' : data.get('resultat'),
							},
							{
								xtype: 'htmleditor',
								enableColors: false,
								enableFontSize: false,
								enableSourceEdit: false,
								minWidth: 400,
								height: 200,
								name: 'formuleCalcul',
								itemId: 'formuleCalculId',
								fieldLabel: 'Formule',
								allowBlank: false,
								value: Ext.isEmpty(data) ? '' : data.get('formuleCalcul'),
							},
							{
								xtype: 'checkboxfield',
								fieldLabel: 'Formules utilisées actuelles',
								name: 'actifFormule',
								itemId: 'actifFormuleId',
								value: Ext.isEmpty(data) ? '' : data.get('actifFormule'),
							},
							{
								xtype: 'datefield',
								name: 'dateApplication',
								itemId: 'dateApplicationId',
								fieldLabel: 'Date de prise d\'effet',
								allowBlank: false,
								format: 'd/m/Y',
								minText: 'Date de modification de la formule!',
								submitFormat: 'Y-m-d',
								value: Ext.isEmpty(data) ? Ext.Date.format(new Date(), 'd/m/Y') : data.get('dateApplication'),
							},					
							{
								xtype: 'htmleditor',
								enableColors: false,
								enableFontSize: false,
								enableSourceEdit: false,
								minWidth: 400,
								height: 200,
								name: 'descriptionFormule',
								itemId: 'descriptionFormuleId',
								fieldLabel: 'Description',
								value: Ext.isEmpty(data) ? '' : data.get('descriptionFormule'),
							},
						],
					},						
					{
						xtype: 'toolbar',
						cls: 'wizard-form-break',
						layout: 'hbox',
						items: [
							{
								itemId: 'formuleSaveBtn',
								xtype: 'button',
								text: 'Enregistrer',
								ui: 'soft-green-small',
								margin: '10 10 0 0',
								formBind: true,
								listeners: {
									click: 'onFormuleSaveClick',
								},
							},
							{
								itemId: 'formuleCancelBtn',
								xtype: 'button',
								text: 'Annuler',
								ui: 'soft-blue-small',
								margin: '10 10 0 0',
								listeners: {
									click: 'cancelAction',
								},
								params: 'formuleFormId', // Send formId to the reset method 
							},
							{
								itemId: 'formuleBackBtn',
								xtype: 'button',
								text: 'Retour',
								ui: 'soft-purple-small',
								iconCls: 'x-fa fa-angle-left',
								margin: '10 10 0 0',
								listeners: {
									click: 'formuleBackAction',
								},
							},
						],
					},
				],
			},
		];
		this.callParent();
	},
});
