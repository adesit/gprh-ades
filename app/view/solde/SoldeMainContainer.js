Ext.define('Gprh.ades.view.solde.SoldeMainContainer', {
	extend: 'Ext.panel.Panel',
	xtype: 'soldeMainContainer',

	requires: [
		'Gprh.ades.view.solde.SalaireDetailsPrint',
	],
	
	bodyPadding: 5,

	itemId: 'soldeMainContainerId',

	controller: 'contratController',

	initComponent() {
		let toolbar;
		let contratListeTab;

		// si l'action est générée à partir du module "Contrat", ne pas afficher dans le toolbar le bouton "Détails" 
		// et ne pas afficher le tabPanel "Liste de contrats"
		if (!Ext.isEmpty(this.parameters)) {
			toolbar = {
				xtype: 'toolbar',
				layout: 'hbox',
				itemId: 'contratTopToolbar',
				margin: '0 0 5 5',
				items: [
					{
						text: 'Contrats',
						tooltip: 'Liste de tous les contrats',
						itemId: 'btnListeContratInContrat',
						ui: 'soft-purple',
						formBind: false,
						iconCls: 'x-fa fa-list',
						listeners: {
							click: 'onBackListContrat',
						},
					},
					{ xtype: 'tbspacer', width: 2 },
					{
						text: 'Personnels',
						tooltip: 'Liste de tous les personnels',
						itemId: 'bntListePersonnelInContrat',
						ui: 'soft-purple',
						formBind: false,
						iconCls: 'x-fa fa-list',
						listeners: {
							click: (butt, evt) => {
								this.getController('ContratController').onPreviousPersonnelClick();
							},
						},
					},
					{ xtype: 'tbspacer', width: 2 },
					{
						text: 'Détails',
						tooltip: 'Détails du personnel',
						ui: 'soft-green',
						iconCls: 'x-fas fa-user-edit',
						listeners: {
							click: 'onPersonnelEditClick',
						},
						params: Ext.isEmpty(this.parameters) ? '' : this.parameters[1].get('idEmploye'),
					},
					'->',
					'Rupture de contrat',
				],
			};
		} else {
			contratListeTab = {
				title: 'Liste de contrats',
				itemId: 'contratGridMinPanelId',
				items: [
					{
						xtype: 'contratGridMin',
						itemId: 'contratGridMinId',
						listeners: {
							// select: 'onContratSoldeClick',
						},
					},
				],
			};
		}

		const tabPanels = {
			xtype: 'tabpanel',
			itemsId: 'tabPanelSoldeId',
			items: [
				contratListeTab,
				{
					title: 'Contrat en cours',
					itemId: 'contratTabPanelId',
					items: [
						{
							xtype: 'contratFormPrint',
							parameters: this.parameters,
						},
					],
				},
				{
					title: 'Salaire de ce mois',
					itemId: 'salaireTabPanelId',
					items: [
						{
							xtype: 'salaireDetailsPrint',
							parameters: this.parameters,
						},
					],
				},
				{
					title: 'Droit de préavis',
				},
				{
					title: 'Indemnités et primes',
				},
				{
					title: 'Solde de tout compte',
				},
			],
		};
		
		this.items = [
			toolbar,
			tabPanels,
		];
		this.callParent();
	},
});
