Ext.define('Gprh.ades.view.solde.SalaireDetailsPrint', {
	extend: 'Ext.panel.Panel',
	xtype: 'salaireDetailsPrint',
	itemId: 'salaireDetailsPrintId',

	requires: [
		'Gprh.ades.util.VTypes',
	],

	viewModel: 'salaireViewModel',
	
	initComponent() {
		const actifContrat = Ext.isEmpty(this.parameters) ? null : this.parameters[3];
		const contratFormContainer = {
			xtype: 'form',
			itemId: 'salaireFormContainerId',
			layout: 'column',
			items: [
				{
					defaultType: 'displayfield',
					defaults: {
						labelWidth: 150,
						labelAlign: 'left',
						labelSeparator: ':',
					},
					columnWidth: '0.50',
					items: [
						{
							fieldLabel: 'Matricule',
							value: Ext.isEmpty(actifContrat) ? '' : actifContrat.matricule,
							fieldStyle: {
								minHeight: '10px',
							},
						},
						{
							fieldLabel: 'Nom - prénom',
							value: Ext.isEmpty(actifContrat) ? '' : actifContrat.nomPrenom,
							fieldStyle: {
								minHeight: '10px',
							},
						},
						{
							fieldLabel: 'Fonction',
							value: Ext.isEmpty(actifContrat) ? '' : actifContrat.titrePoste,
							fieldStyle: {
								minHeight: '10px',
							},
						},
						{
							fieldLabel: 'Catégorie',
							value: Ext.isEmpty(actifContrat) ? '' : actifContrat.categorieReelle,
							fieldStyle: {
								minHeight: '10px',
							},
						},
						{
							fieldLabel: 'Date d\'embauche',
							value: Ext.isEmpty(actifContrat) ? '' : Ext.Date.format(new Date(actifContrat.dateEmbauche), 'd/m/Y'),
							fieldStyle: {
								minHeight: '10px',
							},
						},
						{
							fieldLabel: 'Bureau/Centre',
							value: Ext.isEmpty(actifContrat) ? '' : actifContrat.abreviationCentre,
							fieldStyle: {
								minHeight: '10px',
							},
						},
					],
				}, {
					columnWidth: '0.50',
					layout: 'column',
					items: [
						{
							defaultType: 'displayfield',
							defaults: {
								labelWidth: 150,
								labelAlign: 'left',
								labelSeparator: ':',
							},
							columnWidth: '0.50',
							items: [
								{
									fieldLabel: 'Prime exceptionnelle',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.primeExceptionnel),
									fieldStyle: {
										minHeight: '10px',
									},
								},
								{
									fieldLabel: 'Rappel sur salaire de base',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.rappelBase),
									fieldStyle: {
										minHeight: '10px',
									},
								},
								{
									fieldLabel: 'Salaire intérim',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.montantInterim),
									fieldStyle: {
										minHeight: '10px',
									},
								},
								{
									fieldLabel: 'Salaire supplémentaire',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.salaireSupplementaire),
									fieldStyle: {
										minHeight: '10px',
									},
								},
								{
									fieldLabel: 'Heures supplémentaires',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.totalHeureHS),
									fieldStyle: {
										minHeight: '10px',
									},
								},
								{
									fieldLabel: 'Montant HS',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.montantHS),
									fieldStyle: {
										minHeight: '10px',
									},
								},
								{
									fieldLabel: 'Salaire Brut',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.salaireBrutFinal),
									fieldStyle: {
										minHeight: '10px',
									},
								},
							],
						},
						{
							defaultType: 'displayfield',
							defaults: {
								labelWidth: 150,
								labelAlign: 'left',
								labelSeparator: ':',
							},
							columnWidth: '0.50',
							items: [
								{
									fieldLabel: 'CNaPS',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.cnapsPrime),
									fieldStyle: {
										minHeight: '10px',
									},
								},
								{
									fieldLabel: 'IRSA',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.IrsaNet),
									fieldStyle: {
										minHeight: '10px',
									},
								},
								{
									fieldLabel: 'Caisse hospitalisation',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.caisseHospitalisation),
									fieldStyle: {
										minHeight: '10px',
									},
								},
								{
									fieldLabel: 'Rappel sur salaire Net',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.rappelNet),
									fieldStyle: {
										minHeight: '10px',
									},
								},
								{
									fieldLabel: 'Frais médicaux',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.participationFraisMedicaux),
									fieldStyle: {
										minHeight: '10px',
									},
								},
								{
									fieldLabel: 'Remboursement avance',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.deductionAvance),
									fieldStyle: {
										minHeight: '10px',
									},
								},
								{
									fieldLabel: 'Dépassement téléphonique',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.depassement),
									fieldStyle: {
										minHeight: '10px',
									},
								},
								{
									fieldLabel: 'Divers retenus',
									value: Ext.isEmpty(actifContrat) ? 0 : Gprh.ades.util.Globals.formatRenderNumber(actifContrat.diversRetenus),
									fieldStyle: {
										minHeight: '10px',
									},
								},
							],
						},
					],
				},
			],
		};
		
		this.items = [
			contratFormContainer,
		];

		this.callParent();
	},

});
