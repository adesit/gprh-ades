Ext.define('Gprh.ades.solde.SoldeController', {
	extend: 'Ext.app.ViewController',
	
	alias: 'controller.soldeController',

	requires: [
		'Gprh.ades.util.Globals',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	init() {
		// this.setCurrentView('contratGrid');
	},
});
