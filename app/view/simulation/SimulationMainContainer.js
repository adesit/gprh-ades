Ext.define('Gprh.ades.view.simulation.SimulationMainContainer', {
	extend: 'Ext.tab.Panel',

	xtype: 'simulationMainContainer',

	requires: [
		'Gprh.ades.view.simulation.SimulationController',
		'Gprh.ades.store.Salaire',
		'Gprh.ades.store.Mois',
		'Gprh.ades.util.VTypes',
	],
	
	viewModel: {
		stores: {
			salaireResults: {
				type: 'salaireStore',
			},
			moisResults: {
				type: 'moisStore',
			},
		},
	},

	controller: 'simulationController',

	itemId: 'simulationMainContainerId',

	layout: {
		type: 'vbox',
		pack: 'start',
		align: 'stretch',
	},

	bodyPadding: 1,

	initComponent() {
		let fielddatas;

		// cet appel permet la récupération des taux (Augmentation générale = 15%, et l'inflation = 5%)
		Ext.Ajax.request({
			url: 'server-scripts/listes/TauxValue.php',
			async: false,
			success: (response) => {
				const data = Ext.decode(response.responseText);
				fielddatas = data.data;
			},
		});
		
		this.items = [
			{
				title: 'Masse salariale',
				itemId: 'simuationMasseSalarialeId',
				id: 'simuationMasseSalarialeid',
				layout: 'border',
				height: Ext.Element.getViewportHeight() - 120,
				items: [
					{
						title: 'Paramètres',
						region: 'west',
						// floatable: false,
						width: '70%',
						split: true,
						collapsible: true,
						scrollable: true,
						collapsed: false,
						id: 'westRegionid',
						items: [
							{
								xtype: 'form',
								itemId: 'simulMSId',
								id: 'simulMSid',
								layout: {
									type: 'vbox',
									align: 'stretch',
								},
								region: 'west',
								items: [
									{
										layout: {
											type: 'table',
											columns: 3,
											tableAttrs: {
												style: {
													width: '100%',
												},
											},
										},
										items: [
											{
												layout: {
													type: 'vbox',
													align: 'stretch',
												},
												defaults: {
													labelWidth: 150,
													labelAlign: 'top',
													labelSeparator: ':',
													submitEmptyText: false,
													maxWidth: 200,
													listeners: {
														blur: self => Gprh.ades.util.Globals.formatNumber(self),
													},
												},
												items: [
													{
														xtype: 'combobox',
														maxWidth: 200,
														fieldLabel: 'Mois salarial',
														labelWidth: 150,
														labelAlign: 'top',
														labelSeparator: ':',
														submitEmptyText: false,
														name: 'moisSalarial',
														itemId: 'idSalaireId',
														reference: 'moisFieldReference',
														publishes: 'value',
														store: Ext.create('Gprh.ades.store.Salaire'),
														queryMode: 'local',
														displayField: 'mois',
														valueField: 'moisAnnee',
														renderTo: Ext.getBody(),
														allowBlank: false, 
														forceSelection: true,
														editable: false,
														tpl: Ext.create('Ext.XTemplate',
															'<ul class="x-list-plain"><tpl for=".">',
															'<tpl if="clotureSalaire != 1">',
															'<li role="option" class="x-boundlist-item">{mois}</li>',
															'<tpl else>',
															// @todo Use a dedicated class instead of style
															'<li role="option" class="x-boundlist-item" style="cursor: not-allowed;color: orange;">{mois}</li>',
															'</tpl>',
															'</tpl></ul>'),
														listeners: {
														// Evite la sélection des mois de salaire déjà clôturés
															beforeselect: (combo, record) => record.get('clotureSalaire') !== '1',
															boxready: (combo) => {
																const store = combo.getStore();
																if (store.isLoaded()) {
																	const dataFound = store.findRecord('clotureSalaire', '0').data.moisAnnee;
																	combo.setValue(dataFound);
																} else {
																	store.on({
																		load: () => {
																			const dataFound = store.findRecord('clotureSalaire', '0').data.moisAnnee;
																			combo.setValue(dataFound);
																		},
																		single: true,
																	});
																}
															},
														},
													},
													{
														xtype: 'textfield',
														fieldLabel: 'Taux n°1 (%)',
														itemId: 'tauxId1',
														name: 'taux1',
														id: 'tauxid1',
														vtype: 'decimal',
														allowBlank: false,
													},
													{
														xtype: 'textfield',
														fieldLabel: 'Tranche max n°1 (Ariary)',
														itemId: 'trancheMaxId1',
														name: 'trancheMax1',
														id: 'trancheMaxid1',											
														reference: 'trancheMaxid1Ref',
														publishes: 'value',
														allowBlank: false,
													},
												],
											},
											{
												layout: {
													type: 'vbox',
													align: 'stretch',
												},
												defaults: {
													labelWidth: 150,
													labelAlign: 'top',
													labelSeparator: ':',
													submitEmptyText: false,
													maxWidth: 200,
													listeners: {
														blur: self => Gprh.ades.util.Globals.formatNumber(self),
													},
												},
												items: [
													{
														xtype: 'textfield',
														fieldLabel: 'Taux augmentation (%)',
														itemId: 'tauxAugmentationId',
														name: 'tauxAugmentation',
														id: 'tauxAugmentationid',
														vtype: 'decimal',
														value: fielddatas[0],
													},
													{
														xtype: 'textfield',
														fieldLabel: 'Taux n°2 (%)',
														itemId: 'tauxId2',
														name: 'taux2',
														id: 'tauxid2',
														vtype: 'decimal',
														allowBlank: false,
													},
													{
														xtype: 'textfield',
														fieldLabel: 'Tranche max n°2 (Ariary)',
														itemId: 'trancheMaxId2',
														name: 'trancheMax2',
														id: 'trancheMaxid2',
														allowBlank: false,
														vtype: 'trancheMatch',
														msgTarget: 'side',
													},
												],
											},
											{
												layout: {
													type: 'vbox',
													align: 'stretch',
												},
												defaults: {
													labelWidth: 150,
													labelAlign: 'top',
													labelSeparator: ':',
													submitEmptyText: false,
													maxWidth: 200,
													listeners: {
														blur: self => Gprh.ades.util.Globals.formatNumber(self),
													},
												},
												items: [
													{
														xtype: 'textfield',
														fieldLabel: 'Taux inflation (%)',
														itemId: 'tauxInflationId',
														name: 'tauxInflation',
														id: 'tauxInflationid',
														vtype: 'decimal',
														value: fielddatas[1],
													},
													{
														xtype: 'textfield',
														fieldLabel: 'Taux n°3 (%)',
														itemId: 'tauxId3',
														name: 'taux3',
														id: 'tauxid3',
														vtype: 'decimal',
														allowBlank: false,
													},
													{
														layout: {
															type: 'hbox',
															align: 'stretch',
														},
														margin: '20 0 0 0',
														items: [
															{
																xtype: 'button',
																tooltip: 'Calculer le salaire d\'un mois',
																iconCls: 'x-fa fa-calculator',
																text: 'Calculer',
																ui: 'soft-green',
																itemId: 'runSimulationBtnId',
																handler: 'onRunSimulationClick',
																formBind: true,
															},
															{
																xtype: 'button',
																tooltip: 'Annuler',
																iconCls: 'x-fas fa-undo-alt',
																ui: 'blue',
																itemId: 'cancelSimulationBtnId',
																handler: () => {
																	Ext.getCmp('simulMSid').reset();
																	Ext.getCmp('syntheseid').removeAll(true);
																}, 
															},
															{
																xtype: 'button',
																tooltip: 'Exporter',
																iconCls: 'x-fa fa-file-excel-o',
																ui: 'soft-green',
																itemId: 'exportSimulationBtnId',
																handler: 'onExportSimulationClick',
																formBind: true, 
															},
														],
													},
												
												],
											},
										],
									},
									{
										xtype: 'displayfield',
										labelAlign: 'top',
										fieldLabel: 'Salaire Brut annuel inclu le 13ème mois (Ariary)',
										name: 'salaireBrut',
										publishes: 'value',
										itemId: 'salaireBrutId',
										id: 'salaireBrutid',
										maxWidth: 350,
										baseCls: 'x-toast-success',
										fieldStyle: 'font-weight: bold;',
									},
									{
										html: '<img src="resources/images/Tuto simulation.png">',
										height: 70,
									},
									{
										xtype: 'displayfield',
										fieldLabel: 'Remarque',
										labelAlign: 'top',
										maxWidth: 650,
										baseCls: 'x-toast-info',
										fieldStyle: 'font-weight: bold;',
										value: 'Cette simulation calcule l\'augmentation par rapport aux salaires actuels. Pensez à rajouter une marge pour les embauches et les primes exceptionnelles.',
									},
								],
							},
						],
						height: 325,
					},					
					{
						xtype: 'panel',
						id: 'syntheseid',
						region: 'center',
					},
				],
			},
			{
				// title: 'Embauche ou entretien d\'évaluation',				
			},
		];
		this.callParent();
	},
	
		
});
