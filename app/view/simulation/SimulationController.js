/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 */
Ext.define('Gprh.ades.view.simulation.SimulationController', {
	extend: 'Ext.app.ViewController',

	alias: 'controller.simulationController',
	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.util.Downloader',
	],

	init() {},

	onRunSimulationClick() {
		const formValues = Ext.getCmp('simulMSid').getValues();
		let dataResult;
		// cet appel permet la récupération des taux (Augmentation générale = 15%, et l'inflation = 5%)
		Ext.Ajax.request({
			url: 'server-scripts/formule/Simulation.php',
			async: false,
			method: 'POST',
			params: {
				model: Ext.encode(formValues),
				infoRequest: 1,
				action: 1,
			},
			success: (response) => {
				const data = Ext.decode(response.responseText);
				dataResult = data.data;
			},
		});

		// Enlever les données inutiles
		const modifiedData = dataResult;
		const usefullData = ['categorieProfessionnelle', 'salaireBase', 'salaireSupplementaire', 'primeExceptionnel', 'montantHS', 'sBrutAvantMaternite', 'salaireBrutFinal', 'netAPayer'];
		modifiedData.map(object => Object.keys(object).forEach(key => usefullData.includes(key) || delete object[key]));

		const dataJSON = Ext.encode(modifiedData);

		const secondPartInMainContainer = Ext.getCmp('syntheseid');

		/* const panel = Ext.getCmp('westRegionid');
		panel.collapse(); */
		
		secondPartInMainContainer.add({
			xtype: 'panel',
			bodyCls: 'custom-panel',
			header: {
				title: 'Résumé des résultats',
				style: {
					'background-color': '#3b9ca8',
				},
			},
			scrollable: true,
			width: Ext.Element.getViewportWidth(),
			height: Ext.Element.getViewportHeight() - 100,
			items: [
				{
					xtype: 'container',
					autoScroll: true,
					id: 'ityVe',
					layout: {
						type: 'vbox',
						align: 'stretch',
					},
				},
			],
			listeners: {
				afterrender: () => {
					const sampleData = JSON.parse(dataJSON);
					
					const nrecoPivotExt = new NRecoPivotTableExtensions({
						wrapWith: `<div class="pvtTableRendererHolder" style="max-width:${Ext.Element.getViewportWidth() - 600}px;max-height: ${Ext.Element.getViewportHeight() - 320}px;"></div>`, // special div is needed by fixed headers when used with pivotUI
						fixedHeaders: true,
						// sortByLabelEnabled: false,
						height: 750,
					});
					
					const stdRendererNames = ['Table', 'Table Barchart', 'Heatmap', 'Row Heatmap', 'Col Heatmap'];
					const wrappedRenderers = $.extend({}, $.pivotUtilities.renderers);
					Ext.each(stdRendererNames, (rName) => {
						wrappedRenderers[rName] = nrecoPivotExt.wrapTableRenderer(wrappedRenderers[rName]);
					});
					
					$('#ityVe').pivotUI(sampleData, {
						renderers: wrappedRenderers,
						rows: ['categorieProfessionnelle'],
						cols: ['sBrutAvantMaternite'],
						vals: ['sBrutAvantMaternite'],
						aggregatorName: 'Sum',
						rendererName: 'Heatmap',
						onRefresh: () => {
							// this is correct way to apply fixed headers with pivotUI
							nrecoPivotExt.initFixedHeaders($('#ityVe table.pvtTable'));
						},
						unusedAttrsVertical: 250,
					});
				},
			},
		});

		const sBrutField = Ext.getCmp('salaireBrutid');
		
		// Les 13 mois de salaire par année
		const sBrutValue = modifiedData.reduce((total, obj) => obj.sBrutAvantMaternite + total, 0) * 13;

		sBrutField.setValue(Gprh.ades.util.Globals.formatRenderNumber(sBrutValue));
	},

	async onExportSimulationClick() {
		const formValues = Ext.getCmp('simulMSid').getValues();
		let dataResult;

		Ext.getBody().mask('Téléchargement...');

		// Va exporter les données du salaire clôturé sur Excel (2007 .xlsx)
		Gprh.ades.util.Downloader.get({
			url: 'server-scripts/formule/Simulation.php',
			params: {
				model: Ext.encode(formValues),
				infoRequest: 1,
				action: 2,
			},
		});
	},
});
