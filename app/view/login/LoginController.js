Ext.define('Gprh.ades.view.login.LoginController', {
	extend: 'Ext.app.ViewController',

	alias: 'controller.login',

	requires: [
		'Gprh.ades.util.Globals',
	],

	onLoginClick(me) {
		// This would be the ideal location to verify the user's credentials via
		// a server-side lookup. We'll just move forward for the sake of this example.
		const form = me.up('window').down('form').getForm();
		const storage = Ext.util.LocalStorage.get('foo');
		if (form.isValid()) {
			form.submit({
				url: 'server-scripts/login/Login.php',
				method: 'POST',
				params: null, // needed for additional params
				scope: this,
				success: (formulaire, action) => {
					// Set the localStorage value to true
					const data = Ext.JSON.decode(action.response.responseText);
					if (data.success) {
						storage.setItem('LoggedIn', true);
						storage.setItem('nomUtilisateur', data.nomUtilisateur);
						storage.setItem('avatar', data.avatar); 
						storage.setItem('idCentre', data.centreAffectation);
						storage.setItem('typeUtilisateur', data.typeUtilisateur);
						Ext.apply(Gprh.ades.util.Globals, {
							userType: data.typeUtilisateur,
						});
						// Remove Login Window
						this.getView().destroy();
						
						// Add the main view to the viewport
						Ext.get('page-loader').show();
						// Ext.create({ xtype: 'app-main' });
						Ext.widget('app-main', {});
						// Ext.require('Gprh.ades.view.main.Main', () => Ext.widget('app-main', {}), this);
						Ext.get('page-loader').remove();
					}
				},
				failure: () => {
					// Remove Login Window and show error window to redirect to the Login Window
					this.getView().destroy();
					Ext.Msg.show({
						title: 'Echec de la connexion',
						message: 'Le nom d\'utilisateur ou le mot de passe est incorrecte. Recommencez',
						icon: Ext.MessageBox.INFO,
						buttons: Ext.MessageBox.OK,
						fn: () => {
							window.location.href = '';
						},
					});
				},
			});
		}
	},
});
