Ext.define('Gprh.ades.view.paie.PaieMainContainer', {
	extend: 'Ext.panel.Panel',

	xtype: 'paieMainContainer',

	requires: [
		'Gprh.ades.view.paie.PaieGrid',
	],
	
	viewModel: {
		type: 'salaireViewModel',
	},

	controller: 'salaireController',

	id: 'paieMainContainerId',

	layout: 'column',

	items: [
		{
			columnWidth: '0.33',
			userCls: 'shadow',
			items: [
				{
					xtype: 'paieGrid',
					itemId: 'paiePanelId',
					minHeight: Ext.Element.getViewportHeight() - 75,
				},
			],
		},
		{
			columnWidth: '0.67',
			items: [
				{
					xtype: 'panel',
					id: 'panelForPdfViewerId',
					items: [
						{
							xtype: 'component',
							width: '100%',
							height: Ext.Element.getViewportHeight() - 75,
							id: 'pdfBoxLoaderId',
							autoEl: {
								tag: 'iframe',
								name: 'pdfViewer',
								itemId: 'pdfViewerId',
								id: 'pdfViewerid',
							},
							listeners: {
								load: {
									element: 'el',
									fn() {
										Ext.getCmp('panelForPdfViewerId').body.unmask();
									},
								},
							},
						},
					],
				},
			],
		},
	],
});
