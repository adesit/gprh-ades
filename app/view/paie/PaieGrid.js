Ext.define('Gprh.ades.view.paie.PaieGrid', {
	extend: 'Ext.panel.Panel',
	xtype: 'paieGrid',

	requires: [
		'Gprh.ades.util.VTypes',
		'Gprh.ades.model.SalaireModel',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	controller: 'salaireController',

	layout: 'fit',

	initComponent() {
		const cfg = {
			xtype: 'gridpanel',
			itemId: 'paieGridId',
			reference: 'paieGridRef',
			loadMask: true,
			bind: {
				store: '{salaireResults}',
			},
			viewConfig: {
				preserveScrollOnRefresh: true,
				stripeRows: true,
			},
			selModel: 'rowmodel',
			maxWidth: 450,
			columnLines: true,
			columns: {
				defaults: {
					align: 'left',
				},
				items: [
					{
						xtype: 'gridcolumn',
						dataIndex: 'idSalaire',
						text: '#',
						hidden: true,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'moisSeulement',
						text: 'Mois',
						width: 100,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'anneeSalaire',
						text: 'Année',
						width: 75,
					},
					{
						xtype: 'datecolumn',
						dataIndex: 'dateClotureSalaire',
						text: 'Clôturé',
						format: 'd/m/Y',
						width: 95,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'clotureSalaire',
						text: 'Statut',
						width: 90,
						renderer: value => this.statusSalaire(value),
					},
				],
			},
			dockedItems: [
				Gprh.ades.util.Globals.getSimplePagingToolbar(this),
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							tooltip: 'Exporter en Excel les états de paie',
							iconCls: 'x-fa fa-file-excel-o',
							text: 'Etats de paie',
							ui: 'soft-green',
							itemId: 'printEtatPaieXlsxBtnId',
							margin: '0 0 10 10',
							listeners: {
								click: 'onPrintXlsxEtatPaieClick',
							},
							params: {
								gridId: 'paieGridId',
							},
							disabled: true,
							bind: { 
								disabled: '{paieGridRef.selection.clotureSalaire != 1}',
							},
						},
						{
							tooltip: 'Editer et imprimer les fiches de paie',
							text: 'Fiches de paie',
							iconCls: 'x-far fa-file-pdf',
							ui: 'soft-purple',
							itemId: 'printSalaireGridBtnId',
							margin: '0 0 10 10',
							listeners: {
								click: 'onFDPpdfClick',
							},
							params: {
								gridId: 'paieGridId',
							},
							disabled: true,
							bind: { 
								disabled: '{paieGridRef.selection.clotureSalaire != 1}',
							},
						},
					],
				},
			],
		};
		this.items = [cfg];
		this.callParent();
	},

	statusSalaire(value) {
		const status = (value === '1') ? 'Clôturée' : 'En cours';
		return status;
	},
});
