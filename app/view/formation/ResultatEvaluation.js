Ext.define('Gprh.ades.view.formation.ResultatEvaluation', {
	extend: 'Ext.Panel',
	xtype: 'resultatEvaluationPanel',

	requires: [],

	controller: 'formationController',

	layout: {
		type: 'vbox',
		align: 'stretch',
	},

	tbar: [
		{
			xtype: 'panel',
			html: `<b>1. </b> Pointer la souris sur le graphe pour voir les détails
			</br><b>2.</b> Cliquer sur un item du légende le cacher/l'afficher.`,
			baseCls: 'x-toast-info',
			margin: '5 0 5 0',
		},
		'->',
		{
			xtype: 'button',
			itemId: 'participantEmployeBackBtn',
			iconCls: 'x-fa fa-arrow-circle-left',
			text: 'Retour',
			ui: 'soft-purple-small',
			margin: '10 10 0 10',
			formBind: true,
			listeners: {
				click: 'onParticipantEmployeBackClick',
			},
		},
	],

	initComponent() {
		// Store pour la graphe en barre
		const substore1 = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'generalite',  
				},  
			},  
		});

		// Store pour la graphe en aréa
		const substore2 = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'questionnaire',  
				},  
			},  
		});

		// Store pour la graphe en toile d'araignée
		const substore3 = Ext.create('Ext.data.Store', {  
			proxy: {  
				type: 'memory',  
				reader: {  
					type: 'json',  
					rootProperty: 'byThemeStore',  
				},  
			},  
		});

		// le tableau qui contient toutes les questionnaires
		let questionArray;

		// le tableau qui contient les séries pour la toile d'araignée
		let Series;

		Ext.create('Ext.data.Store', {  
			proxy: {
				type: 'ajax',  
				url: 'server-scripts/listes/ResultatsEvaluation.php',
				async: false, 
				reader: {  
					type: 'json',
				},
				extraParams: {
					infoRequest: 4,
					idSessionFormation: this.parameters.get('idSessionFormation'),
				},  
			},  
			autoLoad: true,  
			listeners: {  
				load: (store, records) => {
					substore1.loadRawData(records[0].store.data.items[0].get('generalite'));  
					substore2.loadRawData(records[0].store.data.items[0].get('questionnaire'));
					questionArray = records[0].store.data.items[0].get('questions');
					substore3.loadRawData(records[0].store.data.items[0].get('byThemeStore'));
					Series = records[0].store.data.items[0].get('series');
					Ext.getCmp('evaluationTabid').add({
						title: 'Evaluation par thème',
						itemId: 'themeResultTabPanelId',
						items: [
							{
								xtype: 'container',
								itemId: 'themeResultContainerId',
								id: 'themeResultContainerid',
								layout: {
									type: 'vbox',
									align: 'stretch',
								},
								maxHeight: 480,
								autoScroll: true,
								margin: 5,
								items: [
									{
										html: `
										<b><u>Lecture de graphique: </u></b></br>Si les ${parseInt(this.parameters.get('nbEvaluation'), 10)} stagiaires ont tous donné la note <b>"Excellente"</b> à la question <b>Qualité de la formation</b>, le graphe aurait atteint l'axe <b>Qualité de la formation = ${parseInt(this.parameters.get('nbEvaluation'), 10) * 5}</b></br>
										`,
										baseCls: 'x-toast-success',
										style: {
											'text-align': 'justify',
										},
										margin: 10,
									},
									{
										xtype: 'polar',
										reference: 'chart',
										width: '100%',
										height: 500,
										legend: {
											docked: 'right',
										},
										store: substore3,
										insetPadding: '40 40 60 40',
										interactions: ['rotate'],
										sprites: [
											{
												type: 'text',
												text: 'Détails de l\'évaluation par thème',
												fontSize: 22,
												width: 100,
												height: 30,
												x: 40, // the sprite x position
												y: 20, // the sprite y position
											}, {
												type: 'text',
												text: 'Note: Ce graphe représente les notes par thème',
												fontSize: 12,
												x: 12,
												y: 480,
											},
										],
										axes: [{
											type: 'numeric',
											position: 'radial',
											grid: true,
											minimum: 0,
											maximum: parseInt(this.parameters.get('nbEvaluation'), 10) * 5,
											majorTickSteps: 4,
											renderer: 'onAxisLabelRender',
										}, {
											type: 'category',
											position: 'angular',
											grid: true,
										}],
										series: Series,
									},
								],
							},
						],
					});
				},
			},
		});

		this.items = [
			{
				xtype: 'tabpanel',
				itemsId: 'evaluationTabId',
				id: 'evaluationTabid',
				items: [
					{
						title: 'Evaluation générale',
						itemId: 'generalResultTabPanelId',
						items: [
							{
								xtype: 'container',
								itemId: 'generalResultContainerId',
								id: 'generalResultContainerid',
								layout: {
									type: 'hbox',
									align: 'stretch',
								},
								maxHeight: 480,
								autoScroll: true,
								margin: 5,
								items: [
									{
										xtype: 'cartesian',
										id: 'grapheGenerale',
										width: 650,
										height: Ext.Element.getViewportHeight() - 125,
										reference: 'chart',
										store: substore1,
										insetPadding: '40 40 40 20',
										animation: Ext.isIE8 ? false : {
											easing: 'backOut',
											duration: 500,
										},
										axes: [{
											type: 'numeric',
											position: 'left',
											fields: 'nbVote',
											maximum: parseInt(this.parameters.get('nbEvaluation'), 10) + 4,
											majorTickSteps: 5,
											label: {
												textAlign: 'right',
											},
											title: 'Nombre de stagiaires',
											grid: {
												odd: {
													fillStyle: 'rgba(255, 255, 255, 0.06)',
												},
												even: {
													fillStyle: 'rgba(0, 0, 0, 0.03)',
												},
											},
											listeners: { // this event we need.
												rangechange: (axis) => {
													const store = axis.getChart().getStore();
													let sum = 0;
													
													store.each((rec) => {
														sum += rec.get('nbVote');
													});
						
													axis.setLimits({
														value: sum,
														line: {
															title: {
																text: `Nombre d'évaluations réçues: ${sum}`,
															},
															lineDash: [2, 2],
														},
													});
												},
											},
										}, {
											type: 'category',
											position: 'bottom',
											fields: 'noteGenerale',
											grid: true,
										}],
										series: [{
											type: 'bar',
											xField: 'noteGenerale',
											yField: 'nbVote',
											style: {
												minGapWidth: 20,
											},
											highlight: {
												strokeStyle: 'black',
												fillStyle: 'gold',
											},
											label: {
												field: 'nbVote',
												display: 'insideEnd',
											},
											tooltip: {
												trackMouse: true,
												renderer: 'onTooltipRender',
											},
										}],
										sprites: [
											{
												type: 'text',
												text: this.parameters.get('themeFormation'),
												fontSize: 22,
												width: 'auto',
												height: 30,
												x: 40, // the sprite x position
												y: 20, // the sprite y position
											},
										],
									}, 
									{
										html: `
										<b>Début:</b> ${this.parameters.get('dateDebutFormation')}</br>
										<b>Fin:</b> ${this.parameters.get('dateFinFormation')}</br>
										<b>Domaine:</b> ${this.parameters.get('DomaineFormation')}</br>
										<b>Type:</b> ${this.parameters.get('TypeFormation')}</br>
										<b>Objectifs:</b> ${this.parameters.get('objectifsFormation')}</br>
										<b>Lieu:</b> ${this.parameters.get('adresseSeance')}</br>
										<b>Nombre de stagiaires:</b> ${this.parameters.get('nbApprenant')}</br>`,
										width: 425,
										style: {
											'text-align': 'justify',
										},
									},
								],
							},
						],
					},
					{
						title: 'Evaluation par question',
						itemId: 'questionResultTabPanelId',
						items: [
							{
								xtype: 'container',
								itemId: 'questionResultContainerId',
								id: 'questionResultContainerid',
								layout: {
									type: 'vbox',
									align: 'stretch',
								},
								maxHeight: 480,
								autoScroll: true,
								margin: 5,
								items: [
									{
										html: `
										<b><u>Lecture de graphique: </u></b></br><b>n sur ${parseInt(this.parameters.get('nbEvaluation'), 10)} stagiaires</b> ont donné la note <b>"Excellente"</b> à la question <b>Qualité de la formation</b>`,
										baseCls: 'x-toast-success',
										style: {
											'text-align': 'justify',
										},
										margin: 10,
									},
									{
										html: `
										<b>1.</b> Que pensez-vous de la qualité de la formation en général ? Temps, lieu, ambiance</br>
										<b>2.</b> Que pensez-vous de l’organisation générale de la formation ?</br>
										<b>3.</b> Qu’avez-vous pensé du rythme de la formation ?</br>
										<b>4.</b> Que pensez-vous de la durée de la formation ?</br>
										<b>5.</b> Comment évaluez-vous la qualité de l’enseignement et/ou le formateur ?</br>
										<b>6.</b> Est-ce-que vous avez perfectionné vos connaissances ?</br>
										<b>7.</b> Auriez-vous besoin d'un suivi particulier à la suite de ces sujets?</br>`,
										width: 425,
										style: {
											'text-align': 'justify',
										},
										margin: 5,
									},
									{
										xtype: 'cartesian',
										reference: 'chartDetails',
										id: 'grapheQuestionnaire',
										width: 650,
										height: Ext.Element.getViewportHeight() - 125,
										insetPadding: '40 40 40 40',
										store: substore2,
										legend: {
											docked: 'right',
										},
										axes: [{
											type: 'numeric',
											position: 'left',
											fields: ['FLQ1', 'FLQ2', 'FLQ3', 'FLQ4', 'FLQ5', 'FLQ6', 'FLQ7'],
											title: 'Note en moyenne',
											grid: true,
											minimum: 0,
											maximum: parseInt(this.parameters.get('nbEvaluation'), 10),
											majorTickSteps: 5,
											renderer: 'onAxisLabelRender',
										}, {
											type: 'category',
											position: 'bottom',
											fields: 'note',
											label: {
												rotate: {
													degrees: -45,
												},
											},
										}],
										// No 'series' config here,
										// as series are dynamically added in the controller.
										sprites: [{
											type: 'text',
											text: 'Détails du résultat par questionnaire',
											fontSize: 22,
											width: 100,
											height: 30,
											x: 40, // the sprite x position
											y: 20, // the sprite y position
										}, {
											type: 'text',
											text: 'Note: Ce graphe représente les moyennes des évaluations par questionnaire.',
											fontSize: 10,
											x: 12,
											y: 525,
										}],
										listeners: {
											afterrender: () => this.getController('formationController').onAfterRender(questionArray),
										},
									},									
								],
							},
						],
					},					
				],
			},
		];
		
		this.callParent();
	},	
});
