Ext.define('Gprh.ades.view.formation.ParticipantGrid', {
	extend: 'Ext.panel.Panel',

	xtype: 'participantGrid',
	
	controller: 'formationController',

	itemId: 'participantGridPanelId',
	id: 'participantGridPanelid',
	flex: 1,

	initComponent() {
		const Store = this.parameters[0];

		const typeStore = Ext.create('Gprh.ades.store.TypeRelationAdes');
		
		this.items = [{
			xtype: 'gridpanel',
			itemId: 'participantGridId',
			id: 'participantGridid',
			reference: 'participantGridRef',
			store: Store,
			viewConfig: {
				preserveScrollOnRefresh: true,
				stripeRows: true,
			},
			scrollable: true,
			cls: 'user-grid',
			selModel: 'rowmodel',
			height: Ext.Element.getViewportHeight() - 75,
			columnLines: true,
			plugins: 'gridfilters',
			columns: {
				defaults: {
					align: 'left',
				},
				items: [
					{
						xtype: 'gridcolumn',
						dataIndex: 'idInscription',
						text: '#',
						hidden: true,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'sexeApprenant',
						maxWidth: 40,
						renderer: value => (value === '1' ? '<span class="x-fa fa-male"></span>' : '<span class="x-fa fa-female"></span>'),
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'nomPrenomApprenant',
						text: 'Nom et prénom',
						flex: 1,
						filter: {
							type: 'string',
						},
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'dateNaissanceApprenant',
						text: 'Né(e) le',
						renderer: value => Gprh.ades.util.Globals.rendererDate(value),
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'typeRelationAdes',
						text: 'Type de relation',
						flex: 1,
						filter: {
							type: 'list',
						},
						renderer: (value) => {
							const findType = typeStore.findRecord('typeRelationAdes', value);
							return `${findType.data.displayText}`;
						},
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'themeFormation',
						text: 'Thème de formation',
						flex: 1,
						filter: {
							type: 'list',
						},
					},
					{
						xtype: 'actioncolumn',
						cls: 'content-column',
						width: 80,
						text: 'Actions',
						items: [
							{
								cls: 'user-grid',
								iconCls: 'x-fa fa-trash soft-red-small',
								handler: 'confirmDeleteInscription',
							},
						],					
					},
					
				],				
			},
			dockedItems: [
				Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
			],
			tbar: [
				{
					xtype: 'panel',
					html: `Liste ${this.parameters[1]}`,
				},
				'->',
				{
					xtype: 'button',
					itemId: 'participantEmployeBackBtn2',
					iconCls: 'x-fa fa-arrow-circle-left',
					text: 'Retour',
					ui: 'soft-purple-small',
					margin: '10 10 0 10',
					formBind: true,
					listeners: {
						click: 'onParticipantEmployeBackClick',
					},
				},
			],
		}];

		this.callParent();
	},

});
