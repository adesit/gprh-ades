Ext.define('Gprh.ades.view.formation.FormationMainContainer', {
	extend: 'Ext.panel.Panel',

	xtype: 'formationMainContainer',

	requires: [
		'Gprh.ades.view.formation.FormationController',
		'Gprh.ades.view.formation.FormationViewModel',
		'Ext.ux.form.ItemSelector',
		'Gprh.ades.store.CatalogueListe',
		'Gprh.ades.store.Formation',
	],
	
	viewModel: {
		type: 'formationViewModel',
	},

	controller: 'formationController',

	itemId: 'formationMainContainerId',
	
	layout: {
		type: 'hbox',
		align: 'stretch',
	},
	
	initComponent() {
		// const ds = Ext.create('Gprh.ades.store.CatalogueListe');
		
		this.listeners = {
			activate: () => {
				const myitemselector = Ext.getCmp('idSousThemeid');
				myitemselector.fromField.store.load();
			},
		};

		this.items = [
			{
				title: 'Enregistrement',
				itemId: 'SessionFormPanelId',
				collapsible: false,
				flex: 1,
				items: [
					{
						xtype: 'form',
						itemId: 'FormationFormId',
						id: 'FormationFormid',
						layout: {
							type: 'vbox',
							align: 'stretch',
						},
						autoScroll: true,
						height: Ext.Element.getViewportHeight() - 100,
						items: [
							{
								layout: 'column',
								items: [
									{
										columnWidth: '0.30',
										layout: {
											type: 'vbox',
											align: 'stretch',
										},
										defaultType: 'textfield',
										defaults: {
											labelWidth: 150,
											labelAlign: 'top',
											labelSeparator: ':',
											submitEmptyText: false,
										},
										padding: '0 0 0 5',
										items: [
											{
												fieldLabel: 'idSessionFormation',
												name: 'idSessionFormation',
												reference: 'idSessionFormationRef',
												publishes: 'value',
												itemId: 'idSessionFormationId',
												hidden: true,
											},
											{
												fieldLabel: 'Titre du module de formation',
												name: 'themeFormation',
												reference: 'themeFormationRef',
												publishes: 'value',
												itemId: 'themeFormationId',
												allowBlank: false,					
											},
											{
												layout: 'column',
												xtype: 'container',
												items: [
													{
														columnWidth: '0.50',
														items: [
															{
																labelAlign: 'top',
																labelSeparator: ':',
																xtype: 'datefield',
																name: 'dateDebutFormation',
																itemId: 'dateDebutFormationId',
																reference: 'dateDebutFormationRef',
																publishes: 'value',
																fieldLabel: 'Date début',
																format: 'd/m/Y',
																submitFormat: 'Y-m-d',
																width: 150,
																allowBlank: false,
															},				
														],
													},
													{
														columnWidth: '0.50',
														items: [
															{
																labelAlign: 'top',
																labelSeparator: ':',
																xtype: 'datefield',
																name: 'dateFinFormation',
																itemId: 'dateFinFormationId',
																reference: 'dateFinFormationRef',
																publishes: 'value',
																fieldLabel: 'Date de fin',
																format: 'd/m/Y',
																submitFormat: 'Y-m-d',
																width: 150,
																allowBlank: false,
															},				
														],
													},
												],
											},
											{
												xtype: 'textarea',
												fieldLabel: 'Lieu de formation',
												name: 'adresseSeance',
												reference: 'adresseSeanceRef',
												publishes: 'value',
												itemId: 'adresseSeanceId',
												allowBlank: false,			
											},
										],			
									},
									{
										columnWidth: '0.30',
										layout: {
											type: 'vbox',
											align: 'stretch',
										},
										defaultType: 'textfield',
										defaults: {
											labelWidth: 150,
											labelAlign: 'top',
											labelSeparator: ':',
											submitEmptyText: false,
											maxWidth: 300,
										},
										padding: '0 0 0 5',
										items: [
											{
												xtype: 'combobox',
												fieldLabel: 'Domaine de formation',
												name: 'domaineFormation',
												reference: 'domaineFormationRef',
												publishes: 'value',
												itemId: 'domaineFormationId',
												bind: {
													store: '{domaineFormationResults}',
												},
												queryMode: 'local',
												displayField: 'nomDomaineFormation',
												valueField: 'domaineFormation',
												renderTo: Ext.getBody(),
												allowBlank: false,
												forceSelection: true,
												editable: false,
												minWidth: 100,
											},
											{
												xtype: 'combobox',
												fieldLabel: 'Type de formation',
												name: 'typeFormation',
												reference: 'typeFormationRef',
												publishes: 'value',
												itemId: 'typeFormationId',
												bind: {
													store: '{typeFormationResults}',
												},
												queryMode: 'local',
												displayField: 'nomTypeFormation',
												valueField: 'typeFormation',
												renderTo: Ext.getBody(),
												allowBlank: false,
												forceSelection: true,
												editable: false,
												minWidth: 100,
											},
											{
												fieldLabel: 'Profil cible',
												name: 'niveauCible',
												reference: 'niveauCibleRef',
												publishes: 'value',
												itemId: 'niveauCibleId',	
											},
											{
												xtype: 'datefield',
												name: 'dateCreationSession',
												itemId: 'dateCreationSessionId',
												reference: 'dateCreationSessionRef',
												publishes: 'value',
												fieldLabel: 'Date de création',
												format: 'd/m/Y',
												submitFormat: 'Y-m-d',
												maxWidth: 150,
												allowBlank: false,
											},
										],	
									},
									{
										columnWidth: '0.40',
										layout: {
											type: 'vbox',
											align: 'stretch',
										},
										items: [								
											{
												xtype: 'htmleditor',
												fieldLabel: 'Objectifs de la formation',
												name: 'objectifsFormation',
												reference: 'objectifsFormationRef',
												publishes: 'value',
												itemId: 'objectifsFormationId',
												enableFont: false,
												enableColors: false,
												enableFontSize: false,
												enableSourceEdit: false,
												enableLinks: false,
												height: 250,
												labelWidth: 150,
												labelAlign: 'top',
												labelSeparator: ':',
											},
										],	
									},
								],			
							},					
							{
								xtype: 'itemselector',
								name: 'idSousTheme',
								id: 'idSousThemeid',
								itemId: 'idSousThemeId',
								fieldLabel: 'Thèmes de formation',
								imagePath: '../ux/images/',
								store: Ext.create('Gprh.ades.store.CatalogueListe'),
								displayField: 'titreSousTheme',
								valueField: 'idSousTheme',
								allowBlank: false,
								msgTarget: 'side',
								fromTitle: 'Disponibles',
								toTitle: 'Sélectionnés',
								height: 240,
								maxWidth: Ext.Element.getDocumentWidth() - 275,
								labelAlign: 'top',
								labelSeparator: ':',
							},
							{
								xtype: 'toolbar',
								layout: 'hbox',
								padding: '0 0 5 0',
								items: [
									'->',
									{
										itemId: 'FormationFormSaveBtn',
										xtype: 'button',
										text: 'Enregistrer',
										ui: 'soft-green-small',
										formBind: true,
										margin: '0 5 0 0',
										handler: 'onFormationFormSaveClick',
									},
									{
										itemId: 'CallAllFormationBtn',
										xtype: 'button',
										text: 'Toutes les sessions',
										ui: 'soft-purple-small',
										margin: '0 5 0 0',
										listeners: {
											click: () => {
												const customTpl = `
												<b>({rows.length} thèmes)
												<font style="color: #2eadf5; white-space: normal"> {[values.children[0].data["themeFormation"]]}</font></b></br>
												<br>
													<b>Début: </b>
														<font style="font-style: italic;"> {[Gprh.ades.util.Globals.rendererDate(values.children[0].data["dateDebutFormation"])]}</font>
													<b style="margin-left:100px">Fin: </b>
														<font style="font-style: italic;">{[Gprh.ades.util.Globals.rendererDate(values.children[0].data["dateFinFormation"])]}</font>
												<br>
													<b>Domaine: </b><font style="font-style: italic;">{[values.children[0].data["DomaineFormation"]]}</font>
												<br>
													<b>Type: </b><font style="font-style: italic;">{[values.children[0].data["TypeFormation"]]}</font>
												<br>
													<b>Objectifs: </b>
													<font style="white-space: normal;">{[values.children[0].data["objectifsFormation"]]}</font>
												<br>
													<b>Lieu: </b>
													<font style="white-space: normal;">{[values.children[0].data["adresseSeance"]]}</font>
												<br>
													<button type="button" name="updateSession"><span class="fa fa-edit"></span> Modifier</button>
													<button type="button" name="deleteSession" style="margin-left:5px"><span class="fa fa-trash"></span> Supprimer</button>`;

												const win = Ext.create('Ext.window.Window', {
													title: 'Sessions de formation disponibles',
													width: 775,
													resizable: false,
													heigth: Ext.Element.getViewportHeight() - 125,
													autoScroll: true,
													modal: true,
													autoDestroy: false,
													closeAction: 'close',
													viewModel: {
														type: 'formationViewModel',
													},												
													controller: 'formationController',
													items: [
														{
															xtype: 'gridpanel',
															itemId: 'formationGridId',
															reference: 'formationGridRef',
															userCls: 'shadow',
															cls: 'user-grid',
															store: Ext.create('Gprh.ades.store.Formation', {
																groupField: 'idSessionFormation',
																proxy: {
																	extraParams: {
																		infoRequest: 1,
																	},
																},
															}),
															viewConfig: {
																preserveScrollOnRefresh: true,
																stripeRows: true,
															},
															scrollable: true,
															selModel: 'rowmodel',
															height: Ext.Element.getViewportHeight() - 100,
															columnLines: true,
															columns: {
																defaults: {
																	align: 'left',
																	width: 100,
																	hidden: true,
																},
																items: [
																	{
																		dataIndex: 'themeFormation',
																		text: 'Titre du module',
																		width: 300,
																		renderer: (value, metaData) => {
																			Ext.apply(metaData, {
																				tdAttr: `data-qtip= "${value}" data-qclass="tipCls" `,
																			});
																			return value;
																		},
																	},
																	{
																		dataIndex: 'objectifsFormation',
																		text: 'Objectifs',
																		width: 300,
																		renderer: (value, metaData) => {
																			Ext.apply(metaData, {
																				tdAttr: `data-qtip= "${value}" data-qclass="tipCls" `,
																			});
																			return value;
																		},
																	},
																	{
																		dataIndex: 'titreSousTheme',
																		text: 'Thèmes de la formation',
																		flex: 1,
																		renderer: (value, metaData) => {
																			Ext.apply(metaData, {
																				tdAttr: `data-qtip= "${value}" data-qclass="tipCls" `,
																			});
																			return value;
																		},
																		hidden: false,
																	},
																	{
																		xtype: 'datecolumn',
																		dataIndex: 'dateDebutFormation',
																		text: 'Début',
																	},
																	{
																		xtype: 'datecolumn',
																		dataIndex: 'dateFinFormation',
																		text: 'Fin',
																	},							
																	{
																		dataIndex: 'DomaineFormation',
																		text: 'Domaine de la formation',
																		width: 200,
																		hidden: true,
																	},
																	{
																		dataIndex: 'TypeFormation',
																		text: 'Type de formation',
																		width: 200,
																		hidden: true,
																	},
																	{
																		dataIndex: 'adresseSeance',
																		text: 'Lieu de la formation',
																		width: 300,
																		renderer: (value, metaData) => {
																			Ext.apply(metaData, {
																				tdAttr: `data-qtip= "${value}" data-qclass="tipCls" `,
																			});
																			return value;
																		},
																	},
																	{
																		dataIndex: 'niveauCible',
																		text: 'Critères des bénéficiaires',
																		width: 300,
																		renderer: (value, metaData) => {
																			Ext.apply(metaData, {
																				tdAttr: `data-qtip= "${value}" data-qclass="tipCls" `,
																			});
																			return value;
																		},
																	},
																],
															},
															dockedItems: [
																Gprh.ades.util.Globals.getPagingToolbar(this),
															],
															tbar: [],
															features: [{
																ftype: 'grouping',
																startCollapsed: true,
																groupHeaderTpl: customTpl,
																enableNoGroups: true,
															}],
															listeners: {
																groupclick: (view, node, group, e) => {
																	if (e.getTarget().type === 'button') {
																		if (e.getTarget().name === 'updateSession') {
																			this.getController('formationController').updateSession(group, win);
																		} else if (e.getTarget().name === 'deleteSession') {
																			this.getController('formationController').confirmDeleteSession(group, view);
																		}
																	}
																},
															},
														},
													],
												});
												win.show();
											},
										},
									},
									{
										itemId: 'FormationFormCancelBtn',
										xtype: 'button',
										text: 'Annuler',
										ui: 'soft-blue-small',
										listeners: {
											click: 'cancelAction',
										},
										params: 'FormationFormId',
									},
								],
							},
						],
					},
				],
				listeners: {
					expand: (self) => {
						self.up().down('#SessionGridPanelId').collapse(true);
					},
					collapse: (self) => {
						self.up().down('#SessionGridPanelId').expand(true);
					},
				},
			},			
		];

		this.callParent();
	},

});
