Ext.define('Gprh.ades.view.formation.FormateurMainContainer', {
	extend: 'Ext.panel.Panel',

	xtype: 'formateurMainContainer',

	requires: [
		'Gprh.ades.view.formation.FormationController',
		'Gprh.ades.view.formation.FormationViewModel',
	],
	
	viewModel: {
		type: 'formationViewModel',
	},

	controller: 'formationController',

	itemId: 'formateurMainContainerId',

	layout: 'column',

	bodyPadding: 1,

	initComponent() {
		this.items = [
			{
				columnWidth: '0.30',
				xtype: 'form',
				itemId: 'FormateurFormId',
				bodyPadding: 5,
				layout: {
					type: 'vbox',
					align: 'stretch',
				},
				defaultType: 'textfield',
				defaults: {
					labelWidth: 150,
					labelAlign: 'top',
					labelSeparator: ':',
					submitEmptyText: false,
				},
				items: [
					{
						fieldLabel: 'idFormateur',
						name: 'idFormateur',
						reference: 'idFormateurRef',
						publishes: 'value',
						itemId: 'idFormateurId',
						hidden: true,
					},
					{
						fieldLabel: 'Nom du formateur',
						name: 'nomFormateur',
						reference: 'nomFormateurRef',
						publishes: 'value',
						itemId: 'nomFormateurId',
						allowBlank: false,					
					},
					{
						fieldLabel: 'Prénoms du formateur',
						name: 'prenomFormateur',
						reference: 'prenomFormateurRef',
						publishes: 'value',
						itemId: 'prenomFormateurId',
					},
					{
						fieldLabel: 'Civilité et Titre/Fonction',
						name: 'titreFonctionFormateur',
						reference: 'titreFonctionFormateurRef',
						publishes: 'value',
						itemId: 'titreFonctionFormateurId',
					},
					{
						fieldLabel: 'Institut d\'enseignement',
						name: 'institution',
						reference: 'institutionRef',
						publishes: 'value',
						itemId: 'institutionId',
					},
					{
						fieldLabel: 'Email',
						name: 'emailFormateur',
						reference: 'emailFormateurRef',
						publishes: 'value',
						itemId: 'emailFormateurId',
						vtype: 'email',
						msgTarget: 'side',
					},
					{
						fieldLabel: 'Téléphone mobile',
						name: 'telephoneFormateur',
						reference: 'telephoneFormateurRef',
						publishes: 'value',
						itemId: 'telephoneFormateurId',	
					},
					{
						fieldLabel: 'Adresse complète',
						name: 'adresseCompleteFormateur',
						reference: 'adresseCompleteFormateurRef',
						publishes: 'value',
						itemId: 'adresseCompleteFormateurId',	
					},
					{
						xtype: 'toolbar',
						layout: 'hbox',
						items: [
							'->',
							{
								itemId: 'FormateurFormSaveBtn',
								xtype: 'button',
								text: 'Enregistrer',
								ui: 'soft-green-small',
								formBind: true,
								margin: '0 5 0 0',
								handler: 'onFormateurFormSaveClick',
							},
							{
								itemId: 'FormateurFormCancelBtn',
								xtype: 'button',
								text: 'Annuler',
								ui: 'soft-blue-small',
								listeners: {
									click: 'cancelAction',
								},
								params: 'FormateurFormId',
							},
						],
					},
				],
			},
			{
				columnWidth: '0.70',
				xtype: 'gridpanel',
				itemId: 'formateurGridId',
				reference: 'formateurGridRef',
				userCls: 'shadow',
				cls: 'user-grid',
				bind: {
					store: '{formateurResults}',
				},
				viewConfig: {
					preserveScrollOnRefresh: true,
					stripeRows: true,
				},
				scrollable: true,
				selModel: 'rowmodel',
				listeners: {
					rowclick: 'onRowFormateurClick',
				},
				height: Ext.Element.getViewportHeight() - 75,
				columnLines: true,
				columns: {
					defaults: {
						align: 'left',
						width: 125,
					},
					items: [
						{
							xtype: 'gridcolumn',
							dataIndex: 'nomPrenomFormateur',
							text: 'Nom complet du formateur',
							width: 300,
							locked: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'titreFonctionFormateur',
							text: 'Titre/Fonction',
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'institution',
							text: 'Institut d\'enseignement',
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'emailFormateur',
							text: 'Email',
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'telephoneFormateur',
							text: 'Téléphone',
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'adresseCompleteFormateur',
							text: 'Adresse',
						},
						{
							xtype: 'actioncolumn',
							cls: 'content-column',
							width: 80,
							text: 'Actions',
							items: [
								{
									iconCls: 'x-fa fa-trash soft-red-small',
									handler: 'confirmDeleteFormateur',
								},
							],					
						},
					
					],				
				},
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbar(this),
				],
				tbar: [],					
			},
		];

		this.callParent();
	},
});
