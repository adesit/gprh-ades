Ext.define('Gprh.ades.view.formation.CatalogueMainContainer', {
	extend: 'Ext.panel.Panel',

	xtype: 'catalogueMainContainer',

	requires: [
		'Gprh.ades.view.formation.FormationController',
		'Gprh.ades.view.formation.FormationViewModel',
	],
	
	viewModel: {
		type: 'formationViewModel',
	},

	controller: 'formationController',

	itemId: 'catalogueMainContainerId',

	layout: 'column',

	bodyPadding: 1,

	initComponent() {
		this.items = [
			{
				columnWidth: '0.30',
				xtype: 'form',
				itemId: 'CatalogueFormId',
				bodyPadding: 5,
				layout: {
					type: 'vbox',
					align: 'stretch',
				},
				defaultType: 'textfield',
				defaults: {
					labelWidth: 150,
					labelAlign: 'top',
					labelSeparator: ':',
					submitEmptyText: false,
				},
				items: [
					{
						fieldLabel: 'idSousTheme',
						name: 'idSousTheme',
						reference: 'idSousThemeRef',
						publishes: 'value',
						itemId: 'idSousThemeId',
						hidden: true,
					},
					{
						xtype: 'combobox',
						fieldLabel: 'Formateur',
						name: 'idFormateur',
						reference: 'idFormateurRef',
						publishes: 'value',
						itemId: 'idFormateurId',
						bind: {
							store: '{formateurResults}',
						},
						queryMode: 'local',
						displayField: 'nomPrenomFormateur',
						valueField: 'idFormateur',
						renderTo: Ext.getBody(),
						allowBlank: false,
						forceSelection: true,
						editable: false,
						minWidth: 100,
					},
					{
						fieldLabel: 'Titre du thème',
						name: 'titreSousTheme',
						reference: 'titreSousThemeRef',
						publishes: 'value',
						itemId: 'titreSousThemeId',
						allowBlank: false,					
					},
					{
						xtype: 'htmleditor',
						fieldLabel: 'Objectifs du thème',
						name: 'objectifsSousTheme',
						reference: 'objectifsSousThemeRef',
						publishes: 'value',
						itemId: 'objectifsSousThemeId',
						enableFont: false,
						enableColors: false,
						enableFontSize: false,
						enableSourceEdit: false,
						enableLinks: false,
						minWidth: 200,
						height: 300,
					},
					{
						xtype: 'toolbar',
						layout: 'hbox',
						items: [
							'->',
							{
								itemId: 'CatalogueFormSaveBtn',
								xtype: 'button',
								text: 'Enregistrer',
								ui: 'soft-green-small',
								formBind: true,
								margin: '0 5 0 0',
								handler: 'onCatalogueFormSaveClick',
							},
							{
								itemId: 'CatalogueFormCancelBtn',
								xtype: 'button',
								text: 'Annuler',
								ui: 'soft-blue-small',
								listeners: {
									click: 'cancelAction',
								},
								params: 'CatalogueFormId',
							},
						],
					},
				],
			},
			{
				columnWidth: '0.70',
				xtype: 'gridpanel',
				itemId: 'catalogueGridId',
				reference: 'catalogueGridRef',
				userCls: 'shadow',
				cls: 'user-grid',
				bind: {
					store: '{catalogueResults}',
				},
				viewConfig: {
					preserveScrollOnRefresh: true,
					stripeRows: true,
				},
				scrollable: true,
				selModel: 'rowmodel',
				listeners: {
					rowclick: 'onRowCatalogueClick',
				},
				height: Ext.Element.getViewportHeight() - 75,
				columnLines: true,
				columns: {
					defaults: {
						align: 'left',
						width: 125,
					},
					items: [
						{
							dataIndex: 'titreSousTheme',
							text: 'Titre du thème',
							width: 300,
							locked: true,
							renderer: (value, metaData) => {
								Ext.apply(metaData, {
									tdAttr: `data-qtip= "${value}" data-qclass="tipCls" `,
								});
								return value;
							},
						},
						{
							dataIndex: 'nomPrenomFormateur',
							text: 'Titre/Fonction',
							width: 300,
							renderer: (value, metaData, record) => {
								Ext.apply(metaData, {
									tdAttr: `data-qtip= "
									${record.get('titreFonctionFormateur')}<br>
									<b>${value}</b><br>
									${record.get('institution')}<br>
									${record.get('emailFormateur')} - ${record.get('telephoneFormateur')}<br>
									${record.get('adresseCompleteFormateur')}" data-qclass="tipCls" `,
								});
								return value;
							},
						},
						{
							dataIndex: 'objectifsSousTheme',
							text: 'Objectifs',
							width: 300,
							renderer: value => `<span data-qtitle="Objectifs" data-qwidth="200" data-qtip="${value}">${value}</span>`,
						},
						{
							xtype: 'actioncolumn',
							cls: 'content-column',
							width: 80,
							text: 'Actions',
							items: [
								{
									iconCls: 'x-fa fa-trash soft-red-small',
									handler: 'confirmDeleteCatalogue',
								},
							],					
						},
					
					],				
				},
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbar(this),
				],
				tbar: [],
			},
		];

		this.callParent();
	},	
});
