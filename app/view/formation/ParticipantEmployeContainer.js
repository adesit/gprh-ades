Ext.define('Gprh.ades.view.formation.ParticipantEmployeContainer', {
	extend: 'Ext.panel.Panel',

	xtype: 'participantEmployeContainer',

	requires: [],
	
	viewModel: {
		stores: {
			posteResults: {
				type: 'posteStore',
				storeId: 'posteResultsId',
				proxy: {
					extraParams: {
						infoRequest: 1,
						typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
						idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
					},
				},
				listeners: {
					load: () => {},
					filterchange: () => {},
				},
			},
			titrePosteFilter: {
				source: 'posteResultsId',
				sorters: [{
					property: 'titrePoste',
					direction: 'ASC',
				}],
			},
		},
		formulas: {			
			disabledSaveBtn: {
				bind: {
					y: '{!participantEmployeGridRef.selection}',
				},

				get: data => data.y,
			},
		},
	},

	controller: 'formationController',

	itemId: 'participantEmployeContainerId',

	layout: 'column',
	
	bodyPadding: 1,
	
	initComponent() {
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		const Store = Ext.create('Gprh.ades.store.Personnel', {
			listeners: {
				load: () => {},
				filterchange: () => {},
			},
		});
		Ext.apply(Store.getProxy(), extraParameters);

		const formationDetails = this.parameters;

		this.items = [
			{
				columnWidth: '0.70',
				xtype: 'gridpanel',
				itemId: 'participantEmployeGridId',
				id: 'participantEmployeGridid',
				reference: 'participantEmployeGridRef',
				store: Store,
				viewConfig: {
					preserveScrollOnRefresh: true,
					stripeRows: true,
				},
				selModel: Ext.create('Ext.selection.CheckboxModel', {
					checkOnly: true,
					listeners: {
						selectionchange: (row, selected) => {
							let count = 0;
							Ext.each(selected, (record) => {
								count += record.get('idEmploye') ? 1 : 0;
							});
							Ext.getCmp('selectedNbid').setValue(count);
						},
					},
				}),
				width: 750,
				listeners: {},
				height: Ext.Element.getViewportHeight() - 75,
				columnLines: true,
				plugins: 'gridfilters',
				columns: {
					defaults: {
						align: 'left',
					},
					items: [
						{
							xtype: 'gridcolumn',
							dataIndex: 'idEmploye',
							text: '#',
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'sexe',
							text: 'Sexe',
							maxWidth: 40,
							renderer: value => (value === '1' ? '<span class="x-fa fa-male"></span>' : '<span class="x-fa fa-female"></span>'),
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nomPrenom',
							text: 'Nom et prénom',
							width: 350,
							filter: {
								type: 'string',
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'titrePoste',
							text: 'Foction/Poste occupé',
							width: 300,
							filter: {
								type: 'list',
								idField: 'titrePoste',
								labelField: 'titrePoste',
								store: this.getViewModel().getStore('titrePosteFilter'),
							},
						},
					],				
				},
				tbar: [
					{
						xtype: 'panel',
						html: 'Liste des employés chez ADES',
					},
				],
			},
			{
				columnWidth: '0.30',
				xtype: 'form',
				itemId: 'participantEmployePanelId',
				id: 'participantEmployePanelid',
				defaultType: 'displayfield',
				margin: 3,
				width: 325,
				defaults: {
					labelWidth: 150,
					labelAlign: 'left',
					labelSeparator: ':',
				},
				items: [
					{
						xtype: 'textfield',
						name: 'idSessionFormation',
						value: formationDetails.get('idSessionFormation'),
						hidden: true,
					},
					{
						// fieldLabel: 'Module',
						value: formationDetails.get('themeFormation'),
						fieldStyle: {
							minHeight: '10px',
							color: '#20b2aba9',
						},
						renderer: Ext.util.Format.nl2br,
					},
					{
						fieldLabel: 'Date début',
						value: formationDetails.get('dateDebutFormation'),
						fieldStyle: {
							minHeight: '10px',
						},
						renderer: value => Gprh.ades.util.Globals.rendererDate(value),
					},
					{
						fieldLabel: 'Date fin',
						value: formationDetails.get('dateFinFormation'),
						fieldStyle: {
							minHeight: '10px',
						},
						renderer: value => Gprh.ades.util.Globals.rendererDate(value),
					},
					{
						fieldLabel: 'Domaine de formation',
						value: formationDetails.get('DomaineFormation'),
						fieldStyle: {
							minHeight: '10px',
						},
					},
					{
						fieldLabel: 'Type de formation',
						value: formationDetails.get('TypeFormation'),
						fieldStyle: {
							minHeight: '10px',
						},
					},
					{
						fieldLabel: 'Employés sélectionnés',
						itemId: 'selectedNbId',
						id: 'selectedNbid',
						fieldStyle: {
							minHeight: '10px',
						},
					},
					{
						xtype: 'button',
						itemId: 'participantEmployeSaveBtn',
						text: 'Inscrire ces employés à la formation',
						ui: 'soft-green-small',
						margin: '10 10 0 10',
						width: 300,
						formBind: true,
						bind: {
							disabled: '{disabledSaveBtn}',
						},
						listeners: {
							click: 'onParticipantEmployeSaveClick',
						},
					},
					{
						itemId: 'listeStatigaireBySessionBtn2',
						xtype: 'button',
						text: 'Inscrits à cette formation',
						ui: 'soft-purple-small',
						margin: '10 10 0 10',
						width: 300,
						listeners: {
							click: () => {
								this.getController('formationController').listeStagiaireBySessionAction(formationDetails.get('idSessionFormation'), 'containerInscriptionid');
							},
						},
					},
					{
						xtype: 'button',
						itemId: 'participantEmployeBackBtn',
						iconCls: 'x-fa fa-arrow-circle-left',
						text: 'Retour',
						ui: 'soft-purple-small',
						margin: '10 10 0 10',
						formBind: true,
						listeners: {
							click: 'onParticipantEmployeBackClick',
						},
					},
				],
			},
		];

		this.callParent();
	},
});
