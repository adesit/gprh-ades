Ext.define('Gprh.ades.view.formation.FormationListeEvaluation', {
	extend: 'Ext.container.Container',

	xtype: 'formationListeEvaluation',

	requires: [],
	
	viewModel: {
		formulas: {
			isEnabledChoiceBtn2: {
				bind: {
					t2: '{!formationEvaluationGridRef.selection}',
				},
				// 1 session de formation est sélectionnée
				get: d => d.t2,
			},
		},
	},

	controller: 'formationController',

	itemId: 'containerEvaluationId',
	id: 'containerEvaluationid',
	layout: 'column',
	flex: 1,

	initComponent() {
		this.items = [
			{
				columnWidth: '0.70',
				xtype: 'gridpanel',
				itemId: 'formationEvaluationGridId',
				id: 'formationEvaluationGridid',
				reference: 'formationEvaluationGridRef',
				userCls: 'shadow',
				cls: 'user-grid',
				store: Ext.create('Gprh.ades.store.Formation', {
					proxy: {
						url: 'server-scripts/listes/FormationSimple.php',
						extraParams: {
							infoRequest: 1,
						},
					},
				}),
				viewConfig: {
					preserveScrollOnRefresh: true,
					stripeRows: true,
				},
				scrollable: true,
				selModel: 'rowmodel',
				height: Ext.Element.getViewportHeight() - 75,
				columnLines: true,
				columns: {
					defaults: {
						align: 'left',
						width: 100,
					},
					items: [
						{
							dataIndex: 'themeFormation',
							text: 'Titre du module',
							width: 300,
							renderer: (value, metaData) => {
								Ext.apply(metaData, {
									tdAttr: `data-qtip= "${value}" data-qclass="tipCls" `,
								});
								return value;
							},
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateDebutFormation',
							text: 'Début',
							renderer: value => Gprh.ades.util.Globals.rendererDate(value),
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateFinFormation',
							text: 'Fin',
							renderer: value => Gprh.ades.util.Globals.rendererDate(value),
						},
						{
							dataIndex: 'adresseSeance',
							text: 'Lieu de la formation',
							width: 300,
							renderer: (value, metaData) => {
								Ext.apply(metaData, {
									tdAttr: `data-qtip= "${value}" data-qclass="tipCls" `,
								});
								return value;
							},
						},
					],
				},
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
				],
				tbar: [],
				listeners: {},
			},
			{
				columnWidth: '0.30',
				xtype: 'panel',
				itemId: 'participantChoicePanelId',
				layout: {
					type: 'vbox',
					align: 'strech',
				},
				margin: 5,
				items: [
					{
						itemId: 'evaluationBtn',
						xtype: 'button',
						text: 'Evaluer la formation',
						ui: 'soft-green-small',
						margin: '10 0 0 0',
						width: 250,
						bind: {
							disabled: '{isEnabledChoiceBtn2}',
						},
						listeners: {
							click: () => {
								Ext.getCmp('evaluationMainContainerid').getController('formationController').evaluationFormationAction(Ext.getCmp('formationEvaluationGridid').selection);
							},
						},
					},
					{
						itemId: 'resultatEvaluationBtn',
						xtype: 'button',
						text: 'Résultats de l\'évaluation',
						ui: 'soft-green-small',
						margin: '10 0 0 0',
						width: 250,
						bind: {
							disabled: '{isEnabledChoiceBtn2}',
						},
						listeners: {
							click: () => {
								Ext.MessageBox.show({
									title: 'Veuillez patienter',
									msg: 'Chargement en cours...',
									progressText: 'Initialisation...',
									width: 300,
									progress: true,
									closable: false,
								});
								Ext.getCmp('evaluationMainContainerid').getController('formationController').resultatEvaluationAction(Ext.getCmp('formationEvaluationGridid').selection);
							},
						},
					},
					{
						html: '<br><font style="font-style: italic;font-size: 16px; font-weight: bold;">Listes:</font>',
					},
					{
						itemId: 'listeStatigaireBySessionBtn',
						xtype: 'button',
						text: 'Inscrits à la formation',
						ui: 'soft-purple-small',
						margin: '10 0 0 0',
						width: 250,
						bind: {
							disabled: '{isEnabledChoiceBtn2}',
						},
						listeners: {
							click: () => {
								Ext.getCmp('evaluationMainContainerid').getController('formationController').listeStagiaireBySessionAction(Ext.getCmp('formationEvaluationGridid').selection.get('idSessionFormation'), 'containerEvaluationContentid');
							},
						},
					},				
				],
			},
		];
		
		this.callParent();
	},

});
