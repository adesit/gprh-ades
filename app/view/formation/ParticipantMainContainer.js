Ext.define('Gprh.ades.view.formation.ParticipantMainContainer', {
	extend: 'Ext.panel.Panel',

	xtype: 'participantMainContainer',

	requires: [
		'Gprh.ades.view.formation.FormationListeInscription',
	],

	itemId: 'participantMainContainerId',
	id: 'participantMainContainerid',

	layout: {
		type: 'vbox',
		align: 'stretch',
	},
	bodyPadding: 1,
	
	initComponent() {
		this.listeners = {
			activate: () => {
				const myformationgird = Ext.getCmp('formationGridid');
				myformationgird.getStore().reload();
			},
		};
		this.items = [
			{
				xtype: 'container',
				itemId: 'containerInscriptionId',
				id: 'containerInscriptionid',
				items: [
					{
						xtype: 'formationListeInscription',
					},
				],
			},
		];

		this.callParent();
	},
});
