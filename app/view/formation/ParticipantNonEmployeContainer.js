Ext.define('Gprh.ades.view.formation.ParticipantNonEmployeContainer', {
	extend: 'Ext.panel.Panel',

	xtype: 'participantNonEmployeContainer',

	requires: [
		'Gprh.ades.model.ApprenantModel',
	],
	
	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	controller: 'formationController',

	itemId: 'participantNonEmployeContainerId',

	layout: 'fit',
	
	bodyPadding: 1,

	viewModel: {
		formulas: {			
			disabledSaveBtn: {
				bind: {
					y: '{!participantNonEmployeGridRef.selection}',
				},

				get: data => data.y,
			},
		},
	},
	
	initComponent() { // TODO create Apprenant Grid where typeRelation !== 1 and make it editable(-> Save directly) and selection model is checkbox(save multiple lines), check if record already exists and send and error helper before insert it
		const apprenantParameters = {
			extraParams: {
				infoRequest: 3,
				idSessionFormation: this.parameters[0],
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
				limit: 25,
			},
		};
		const Store = Ext.create('Gprh.ades.store.Apprenant');
		Ext.apply(Store.getProxy(), apprenantParameters);

		const StorePersonnel = this.parameters[1];

		const rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
			clicksToMoveEditor: 1,
			autoCancel: false,
			saveBtnText: 'Enregistrer',
			cancelBtnText: 'Annuler',
			showToolTip: false,
			errorSummary: false,
			listeners: {
				// beforeedit: () => (Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
				validateedit: (editor, context) => {
					const model = {
						infoRequest: 2,
						data: context.newValues,
						idSessionFormation: this.parameters[0],
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/formation/Apprenant.php', model);
					
					if (saveOrUpdate.res > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
						context.grid.getStore().load();
						context.grid.getView().refresh();
					} else if (Ext.typeOf(saveOrUpdate.res) === 'string') {
						Gprh.ades.util.Helper.showError(saveOrUpdate.res);
					}
				},
				canceledit: (editor, context) => {
					context.grid.getStore().reload();
					context.grid.getView().refresh();
				},
			},
		});

		const typeStore = Ext.create('Gprh.ades.store.TypeRelationAdes');

		this.items = [
			{
				xtype: 'gridpanel',
				itemId: 'participantNonEmployeGridId',
				id: 'participantNonEmployeGridid',
				reference: 'participantNonEmployeGridRef',
				store: Store,
				viewConfig: {
					preserveScrollOnRefresh: true,
					stripeRows: true,
				},
				selModel: Ext.create('Ext.selection.CheckboxModel', {
					checkOnly: true,
					listeners: {
						selectionchange: (row, selected) => {
							let count = 0;
							Ext.each(selected, (record) => {
								count += record.get('idEmploye') ? 1 : 0;
							});
							// Ext.getCmp('selectedNbid').setValue(count);
						},
					},
				}),
				width: Ext.Element.getViewportWidth(),
				listeners: {},
				height: Ext.Element.getViewportHeight() - 75,
				columnLines: true,
				plugins: [
					rowEditing,
					'gridfilters',
				],
				columns: {
					defaults: {
						align: 'left',
					},
					items: [
						{
							xtype: 'gridcolumn',
							dataIndex: 'idApprenant',
							text: '#',
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nomApprenant',
							text: 'Nom',
							width: 200,
							filter: {
								type: 'string',
							},
							editor: {
								allowBlank: false,
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'prenomApprenant',
							text: 'Prénom',
							width: 200,
							filter: {
								type: 'string',
							},
							editor: {},
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateNaissanceApprenant',
							text: 'Né(e) le',
							width: 125,
							renderer: value => Gprh.ades.util.Globals.rendererDate(value),
							editor: {
								xtype: 'datefield',
								allowBlank: false,
								format: 'd/m/Y',
								minText: 'Ne devrait pas être null et doit être antérieure à la date du jour!',
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'sexeApprenant',
							text: 'Sexe',
							width: 100,
							renderer: value => (value === '1' ? '<span class="x-fa fa-male"></span>' : '<span class="x-fa fa-female"></span>'),
							editor: {
								xtype: 'combobox',
								store: Ext.create('Gprh.ades.store.Sexe'),
								queryMode: 'local',
								displayField: 'displayText',
								valueField: 'sexe',
								renderTo: Ext.getBody(),
								allowBlank: false, 
								forceSelection: true,
								editable: false,
								maxWidth: 95,
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'typeRelationAdes',
							text: 'Relation',
							width: 180,
							renderer: (value) => {
								const findType = typeStore.findRecord('typeRelationAdes', value);
								return `${findType.data.displayText}`;
							},
							editor: {
								xtype: 'combobox',
								store: Ext.create('Gprh.ades.store.TypeRelationAdes'),
								queryMode: 'local',
								displayField: 'displayText',
								valueField: 'typeRelationAdes',
								renderTo: Ext.getBody(),
								allowBlank: false, 
								forceSelection: true,
								editable: false,
								maxWidth: 175,
								tpl: Ext.create('Ext.XTemplate',
									'<ul class="x-list-plain"><tpl for=".">',
									'<tpl if="typeRelationAdes != 1">',
									'<li role="option" class="x-boundlist-item">{displayText}</li>',
									'<tpl else>',
									// @todo Use a dedicated class instead of style
									'<li role="option" class="x-boundlist-item" style="cursor: not-allowed;color: orange;">{displayText}</li>',
									'</tpl>',
									'</tpl></ul>'),
								listeners: {
									// Evite la sélection des mois de salaire déjà clôturés
									beforeselect: (combo, record) => record.get('typeRelationAdes') !== '1',
								},
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'idEmploye',
							text: 'Proche d\'un employé',
							width: 250,
							renderer: (value) => {
								let nom;
								if (!Ext.isEmpty(value)) {
									const findType = StorePersonnel.findRecord('idEmploye', value);
									nom = findType.data.nomPrenom;
								} else nom = '';
								return `${nom}`;
							},
							editor: {
								xtype: 'combobox',
								store: StorePersonnel,
								queryMode: 'local',
								displayField: 'nomPrenom',
								valueField: 'idEmploye',
								renderTo: Ext.getBody(),
								editable: true,
								maxWidth: 245,
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'telephoneApprenant',
							text: 'Téléphone',
							width: 150,
							editor: {},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'adresseCompleteApprenant',
							text: 'Adresse',
							width: 200,
							editor: {},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'niveau',
							text: 'Niveau',
							width: 200,
							editor: {},
						},
					],				
				},
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
				],
				tbar: [
					{
						xtype: 'panel',
						html: 'Personnes non employées d\'ADES et ne sont pas encore inscrites à la formation',
					},
					'->',
					{
						xtype: 'button',
						itemId: 'participantNonEmployeSaveBtn',
						text: 'Inscrire les sélectionnées',
						ui: 'soft-green-small',
						margin: '10 0 0 0',
						formBind: true,
						bind: {
							disabled: '{disabledSaveBtn}',
						},
						listeners: {
							click: 'onParticipantNonEmployeSaveClick',
						},
						params: this.parameters[0],
					},		
					{
						tooltip: 'Créer un nouveau stagiaire',
						itemId: 'addApprenantBtnId',
						iconCls: 'fa fa-user-plus',
						text: 'Nouveau stagiaire',
						margin: '0 0 10 10',
						// hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
						handler: () => {
							rowEditing.cancelEdit();
											
							const r = Ext.create('Gprh.ades.model.ApprenantModel', {
								idApprenant: '',
								nomApprenant: '',
								prenomApprenant: '',
								dateNaissanceApprenant: '',
								sexeApprenant: '',
								telephoneApprenant: '',
								adresseCompleteApprenant: '',
								niveau: '',
								typeRelationAdes: '',
								idEmploye: '',
							});
		
							this.down('#participantNonEmployeGridId').getStore().insert(0, r);
							rowEditing.startEdit(0, 0);
						},
					},
					{
						xtype: 'button',
						itemId: 'participantEmployeBackBtn2',
						iconCls: 'x-fa fa-arrow-circle-left',
						text: 'Retour',
						ui: 'soft-purple-small',
						margin: '10 10 0 10',
						formBind: true,
						listeners: {
							click: 'onParticipantEmployeBackClick',
						},
					},		
				],
			},
		];

		this.callParent();
	},
});
