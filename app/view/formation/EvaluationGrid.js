Ext.define('Gprh.ades.view.formation.EvaluationGrid', {
	extend: 'Ext.panel.Panel',

	xtype: 'evaluationGridPanel',

	requires: [
		'Gprh.ades.view.formation.FormationController',
		'Gprh.ades.store.TypeNoteEvaluation',
		'Gprh.ades.store.Evaluation',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],
	
	viewModel: {},

	controller: 'formationController',

	itemId: 'evaluationGridPanelId',

	layout: {
		type: 'vbox',
		align: 'stretch',
	},

	bodyPadding: 1,

	initComponent() {
		let fielddatas;
		let columndatas;

		// cet appel permet la création d'un tableau de fields et d'un tableau de columns
		Ext.Ajax.request({
			url: 'server-scripts/listes/Evaluation.php',
			params: {
				infoRequest: 2,
				idSessionFormation: this.parameters.get('idSessionFormation'),
				limit: 25,
				start: 0,
				page: 1,
			},
			async: false,
			success: (response) => {
				const data = Ext.decode(response.responseText);
				fielddatas = data.fielddata;
				columndatas = data.columndata;
			},
		});

		// Une fois créer, cette instance refait la requête au serveur avec les données
		const Store = Ext.create('Gprh.ades.store.Evaluation', {
			fields: fielddatas,
			proxy: {
				extraParams: {
					infoRequest: 2,
					idSessionFormation: this.parameters.get('idSessionFormation'),
				},
			},
		});

		const rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
			clicksToMoveEditor: 1,
			autoCancel: false,
			saveBtnText: 'Enregistrer',
			cancelBtnText: 'Annuler',
			showToolTip: false,
			errorSummary: false,
			listeners: {
				// beforeedit: () => (Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
				validateedit: (editor, context) => {
					const model = {
						infoRequest: 2,
						data: context.newValues,
						idSessionFormation: this.parameters[0],
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/formation/Evaluation.php', model);
					const result = (saveOrUpdate.request.responseText);
					
					/* const selection = context.grid.getSelectionModel().getSelection()[0];
					const grille = Ext.getCmp('evaluationGridid'); */
					
					if (result > 0) {
						Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
						// context.grid.getStore().remove(selection);
						context.grid.getStore().load();
						context.grid.getView().refresh();
					} else if (Ext.typeOf(result) === 'string') {
						Gprh.ades.util.Helper.showError(result);
					}
				},
				canceledit: (editor, context) => {
					context.grid.getStore().reload();
					context.grid.getView().refresh();
				},
			},
		});

		Ext.tip.QuickTipManager.init();
					
		this.items = [
			{
				xtype: 'gridpanel',
				itemId: 'evaluationGridId',
				id: 'evaluationGridid',
				stripeRows: true,
				columnLines: true,
				height: Ext.Element.getViewportHeight() - 75,
				minWidth: 800,
				title: `Evaluation de la formation - ${this.parameters.get('themeFormation')}`,
				store: Store,
				selModel: 'rowmodel',
				columns: {
					items: columndatas,
					defaults: {
						editor: {
							xtype: 'combobox',
							store: Ext.create('Gprh.ades.store.TypeNoteEvaluation'),
							queryMode: 'local',
							displayField: 'displayText',
							valueField: 'typeNoteEvaluation',
							renderTo: Ext.getBody(),
							allowBlank: false, 
							forceSelection: true,
							editable: false,
							maxWidth: 100,
						},
					},
				},
				plugins: [rowEditing],
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
				],
				tbar: [
					{
						xtype: 'panel',
						html: `<span class="x-fa fa-info-circle"></span> Légende du tableau
						</br><b>Q :</b> Question
						</br><b>TH:</b> un thème présenté dans la session de formation. Glissez le pointeur de la souris sur l'en-tête du colonne pour voir le titre du thème.`,
						baseCls: 'x-toast-info',
						margin: '5 0 5 0',
					},
					'->',
					{
						xtype: 'button',
						itemId: 'participantEmployeBackBtn',
						iconCls: 'x-fa fa-arrow-circle-left',
						text: 'Retour',
						ui: 'soft-purple-small',
						margin: '10 10 0 10',
						formBind: true,
						listeners: {
							click: 'onParticipantEmployeBackClick',
						},
					},
				],
			},
		];
		
		this.callParent();
	},
});
