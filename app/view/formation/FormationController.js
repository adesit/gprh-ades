Ext.define('Gprh.ades.view.formation.FormationController', {
	extend: 'Ext.app.ViewController',
	
	alias: 'controller.formationController',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.store.Apprenant',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	init() {
		
	},

	cancelAction(formId) {
		const form = this.getView().down(`#${formId.params}`);
		form.reset();
	},

	setCurrentView(view, params, target) {
		const contentPanel = Ext.getCmp(target);
		// We skip rendering for the following scenarios:
		// * There is no contentPanel
		// * view xtype is not specified
		// * current view is the same
		if (!contentPanel || view === '' || (contentPanel.down() && contentPanel.down().xtype === view)) {
			return false;
		}

		if (params && params.openWindow) {
			const cfg = Ext.apply({
				xtype: 'prototypeWindow',
				items: [
					Ext.apply({
						xtype: view,
					}, params.targetCfg),
				],
			}, params.windowCfg);

			Ext.create(cfg);
		} else {
			Ext.suspendLayouts();

			contentPanel.removeAll(true);
			contentPanel.add(
				Ext.apply({
					xtype: view,
				}, {
					parameters: params,
				})
			);

			Ext.resumeLayouts(true);
		}
		return true;
	},
	onFormateurFormSaveClick() {
		const form = this.getView().down('#FormateurFormId');
		const Formateurgrid = this.getView().down('#formateurGridId');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/formation/Formateur.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			Formateurgrid.getStore().reload();
		} else if (Ext.typeOf(serverResponse) === 'string') {
			console.error(Ext.decode(serverResponse).error);
			Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
		}
		
		form.reset();
	},

	confirmDeleteFormateur(view, cell, recordIndex, cellIndex, e, record) {
		const FormateurParams = {
			idFormateur: record.get('idFormateur'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxFormateurDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelFormateur = {
						infoRequest: 3,
						data: FormateurParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/formation/Formateur.php', modelDelFormateur);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxFormateurDelete').close();
						this.getView().down('#formateurGridId').getStore().load();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxFormateurDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	onRowFormateurClick(record) {
		const allFields = [
			'idFormateur',
			'nomFormateur',
			'prenomFormateur',
			'titreFonctionFormateur',
			'institution',
			'emailFormateur',
			'telephoneFormateur',
			'adresseCompleteFormateur',
		];
		
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.selection.get(field));
			});
		}
	},

	onCatalogueFormSaveClick() {
		const form = this.getView().down('#CatalogueFormId');
		const Formateurgrid = this.getView().down('#catalogueGridId');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/formation/Catalogue.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			Formateurgrid.getStore().reload();
		} else if (Ext.typeOf(serverResponse) === 'string') {
			console.error(Ext.decode(serverResponse).error);
			Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
		}
		
		form.reset();
	},

	confirmDeleteCatalogue(view, cell, recordIndex, cellIndex, e, record) {
		const CatalogueParams = {
			idSousTheme: record.get('idSousTheme'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxCatalogueDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelCatalogue = {
						infoRequest: 3,
						data: CatalogueParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/formation/Catalogue.php', modelDelCatalogue);
					const result = saveOrUpdate.request.request.result.responseText;
					
					if (result > 0) {
						btn.up('#messageBoxCatalogueDelete').close();
						this.getView().down('#formateurGridId').getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxCatalogueDelete').close();
						Ext.create('Ext.window.MessageBox').show({
							title: 'Echec de la suppression',
							msg: (Gprh.ades.util.Globals.errorDelMsg),
							icon: Ext.MessageBox.WARNING,
						});
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxCatalogueDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	onRowCatalogueClick(record) {
		const allFields = [
			'idSousTheme',
			'idFormateur',
			'titreSousTheme',
			'objectifsSousTheme',
		];
		
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.selection.get(field));
			});
		}
	},

	onFormationFormSaveClick() {
		const form = this.getView().down('#FormationFormId');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/formation/Formation.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
		} else if (Ext.typeOf(serverResponse) !== 'number') {
			console.error(serverResponse.error);
			Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
		}
		
		form.reset();
	},

	confirmDeleteSession(record, table) {
		const FormationParams = {
			idSessionFormation: Number(record),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxFormationDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelFormation = {
						infoRequest: 3,
						data: FormationParams,
					};

					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/formation/Formation.php', modelDelFormation);
					
					const result = saveOrUpdate.request.request.result.responseText;

					if (result > 0) {
						btn.up('#messageBoxFormationDelete').close();
						table.getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxFormationDelete').close();
						Ext.create('Ext.window.MessageBox').show({
							title: 'Echec de la suppression',
							msg: (Gprh.ades.util.Globals.errorDelMsg),
							icon: Ext.MessageBox.WARNING,
						});
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxFormationDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	updateSession(record, win) {
		win.close();
		const findLine = Ext.getStore('formationStore').findRecord('idSessionFormation', Number(record));
		const form = Ext.getCmp('FormationFormid');
		
		const allFields = [
			'idSessionFormation',
			'themeFormation',
			'objectifsFormation',
			'dateCreationSession',
			'dateDebutFormation',
			'dateFinFormation',
			'domaineFormation',
			'adresseSeance',
			'typeFormation',
			'niveauCible',
			'idSousTheme',
		];
		if (!Ext.isEmpty(findLine)) {
			Ext.each(allFields, (field) => {
				form.down(`#${field}Id`).setValue(findLine.get(field));
			});
		}
	},

	ouiEmployeAction(sessionId) {
		this.setCurrentView('participantEmployeContainer', sessionId.selection, 'containerInscriptionid');
	},

	onParticipantEmployeSaveClick() {
		const grille = Ext.getCmp('participantEmployeGridid');
		const form = Ext.getCmp('participantEmployePanelid');
		const ids = [];
		
		const s = grille.getSelectionModel().getSelection();
		Ext.each(s, (record) => {
			ids.push(record.data);
		});
		
		const model = {
			infoRequest: 1,
			data: form.getValues(),
			paramsIds: ids,
		};
		
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/formation/Apprenant.php', model);
		const outputSalairesData = JSON.parse(saveOrUpdate.request.responseText);
				
		if (outputSalairesData.data) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			grille.getStore().load();
		} else if (Ext.typeOf(outputSalairesData.data) === 'string') {
			console.error(Ext.decode(outputSalairesData).error);
			Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
		}
	},

	onParticipantEmployeBackClick() {
		let targetContainer;
		let viewShown;
		if (Ext.isEmpty(Ext.getCmp('containerInscriptionid'))) {
			targetContainer = Ext.getCmp('containerEvaluationContentid');
			viewShown = 'formationListeEvaluation';
		} else {
			targetContainer = Ext.getCmp('containerInscriptionid');
			viewShown = 'formationListeInscription';
		}
		const resetPersonnelMainContainer = (target) => {
			Ext.suspendLayouts();

			target.removeAll(true);
			target.add(
				Ext.apply({
					xtype: viewShown,
				})
			);

			Ext.resumeLayouts(true);
		};
		
		targetContainer.removeAll(true);
		resetPersonnelMainContainer(targetContainer);
	},

	async listeStagiaireAction() {
		const apprenantParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
				limit: 25,
			},
		};
		const salaireDataStore = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Apprenant', apprenantParameters);
		this.setCurrentView('participantGrid', [salaireDataStore, 'de tous les inscrits en formation chez ADES'], 'containerInscriptionid');
	},

	listeStagiaireBySessionAction(idSessionFormationParam, target) {
		const apprenantParameters = {
			extraParams: {
				infoRequest: 2,
				idSessionFormation: idSessionFormationParam,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
				limit: 25,
			},
		};
		const salaireDataStore =  Ext.create('Gprh.ades.store.Apprenant');
		Ext.apply(salaireDataStore.getProxy(), apprenantParameters);
		this.setCurrentView('participantGrid', [salaireDataStore, 'des inscrits à la formation'], target);
	},

	confirmDeleteInscription(view, cell, recordIndex, cellIndex, e, record) {
		const table = Ext.getCmp('participantGridid');
		const InscriptionParams = {
			idInscription: record.get('idInscription'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxInscriptionDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelInscription = {
						infoRequest: 5,
						data: InscriptionParams,
					};

					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/formation/Apprenant.php', modelDelInscription);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxInscriptionDelete').close();
						table.getStore().load();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxInscriptionDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	async nonEmployeAction(sessionId) {
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		const StorePersonnel = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Personnel', extraParameters);

		this.setCurrentView('participantNonEmployeContainer', [sessionId.selection.get('idSessionFormation'), StorePersonnel], 'containerInscriptionid');
	},

	onParticipantNonEmployeSaveClick(params) {
		const grille = Ext.getCmp('participantNonEmployeGridid');
		const ids = [];
		
		const s = grille.getSelectionModel().getSelection();
		Ext.each(s, (record) => {
			ids.push(record.data);
		});
		
		const model = {
			infoRequest: 3,
			data: {
				idSessionFormation: params.params,
			},
			paramsIds: ids,
		};
		
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/formation/Apprenant.php', model);
		const outputSalairesData = JSON.parse(saveOrUpdate.request.responseText);
				
		if (outputSalairesData.data) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			grille.getStore().load();
		} else if (Ext.typeOf(outputSalairesData.data) === 'string') {
			console.error(Ext.decode(outputSalairesData).error);
			Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
		}
	},

	evaluationFormationAction(idSessionFormationParam) {
		this.setCurrentView('evaluationGridPanel', idSessionFormationParam, 'containerEvaluationContentid');
	},

	resultatEvaluationAction(idSessionFormationParam) {
		// Vérifie si la formation a déjà été évaluée
		const extraParamsverif = {
			extraParams: {
				infoRequest: 4,
				idSessionFormation: idSessionFormationParam.get('idSessionFormation'),
			},
		};
		/* const verif = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.ResultatsEvaluation', extraParamsverif);
		if (verif.getCount() > 0) {
			this.setCurrentView('resultatEvaluationPanel', idSessionFormationParam, 'containerEvaluationContentid');
		} else {
			Gprh.ades.util.Helper.showError('Aucune évaluation pour cette formation.');
			return false;
		} */
		this.setCurrentView('resultatEvaluationPanel', idSessionFormationParam, 'containerEvaluationContentid');
		Ext.MessageBox.close();
	},

	// Graphes
	onTooltipRender(tooltip, record, item) {
		const store = Ext.getCmp('grapheGenerale').getStore();
		let sum = 0;
		
		store.each(function (rec) {
            sum += rec.get('nbVote');
		});
		
		tooltip.setHtml(Ext.util.Format.number(record.get('nbVote'), '0,000 sur ' + sum) + ' ont notés la formation : ' + record.get('noteGenerale'));
	},

	onAxisLabelRender: function (axis, label, layoutContext) {
        var value = layoutContext.renderer(label);
        return value;
	},
	
	getSeriesConfig: function (field, title) {
		const store = Ext.getCmp('grapheGenerale').getStore();
		let sum = 0;
		
		store.each(function (rec) {
            sum += rec.get('nbVote');
		});
        return {
            type: 'area',
            title: title,
            xField: 'note',
            yField: field,
            style: {
                opacity: 0.60
            },
            marker: {
                opacity: 0,
                scaling: 0.01,
                fx: {
                    duration: 200,
                    easing: 'easeOut'
                }
            },
            highlightCfg: {
                opacity: 1,
                scaling: 1.5
            },
            tooltip: {
                trackMouse: true,
                renderer: function (tooltip, record, item) {
                    tooltip.setHtml(title + ' (' + record.get('note') + '): ' + record.get(field) + ' sur ' + sum);
                }
            }
        };
	},
	
	onAfterRender: function (questionArray) {
        const me = this;
		const chart = me.lookupReference('chartDetails');
		const questionArrayFormated = questionArray.map(obj => {
			return me.getSeriesConfig(obj.abreviation, obj.critere);
		});

        chart.setSeries(questionArrayFormated);
	},
	
	onDataRender: function (v) {
        return v + '%';
    },

    onAxisLabelRender: function (axis, label, layoutContext) {
		return layoutContext.renderer(label);
    },

    onMultiAxisLabelRender: function (axis, label, layoutContext) {
        return label === 'Qualité de la formation' ? '' : label;
    },

    onSeriesLabelRender: function (tooltip, record, item) {
		tooltip.setHtml(record.get('question') + ' - ' + item.series.config.title + ': ' + record.get(item.field));
    }
});
