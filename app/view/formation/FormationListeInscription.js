Ext.define('Gprh.ades.view.formation.FormationListeInscription', {
	extend: 'Ext.container.Container',

	xtype: 'formationListeInscription',

	requires: [
		'Gprh.ades.view.formation.FormationController',
		'Gprh.ades.view.formation.FormationViewModel',
		'Gprh.ades.view.personnel.PersonnelGrid',
	],
	
	viewModel: {
		type: 'formationViewModel',
	},

	controller: 'formationController',

	itemId: 'contentPanelParticipant',
	id: 'contentPanelParticipantid',
	layout: 'column',
	flex: 1,

	initComponent() {
		this.items = [
			{
				columnWidth: '0.70',
				xtype: 'gridpanel',
				itemId: 'formationGridId',
				id: 'formationGridid',
				reference: 'formationGridRef',
				userCls: 'shadow',
				cls: 'user-grid',
				store: Ext.create('Gprh.ades.store.Formation', {
					proxy: {
						url: 'server-scripts/listes/FormationSimple.php',
						extraParams: {
							infoRequest: 1,
						},
					},
				}),
				viewConfig: {
					preserveScrollOnRefresh: true,
					stripeRows: true,
				},
				scrollable: true,
				selModel: 'rowmodel',
				height: Ext.Element.getViewportHeight() - 75,
				columnLines: true,
				columns: {
					defaults: {
						align: 'left',
						width: 100,
					},
					items: [
						{
							dataIndex: 'themeFormation',
							text: 'Titre du module',
							width: 300,
							renderer: (value, metaData) => {
								Ext.apply(metaData, {
									tdAttr: `data-qtip= "${value}" data-qclass="tipCls" `,
								});
								return value;
							},
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateDebutFormation',
							text: 'Début',
							renderer: value => Gprh.ades.util.Globals.rendererDate(value),
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateFinFormation',
							text: 'Fin',
							renderer: value => Gprh.ades.util.Globals.rendererDate(value),
						},
						{
							dataIndex: 'adresseSeance',
							text: 'Lieu de la formation',
							width: 300,
							renderer: (value, metaData) => {
								Ext.apply(metaData, {
									tdAttr: `data-qtip= "${value}" data-qclass="tipCls" `,
								});
								return value;
							},
						},
					],
				},
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
				],
				tbar: [],
				listeners: {},
			},
			{
				columnWidth: '0.30',
				xtype: 'panel',
				itemId: 'participantChoicePanelId',
				layout: {
					type: 'vbox',
					align: 'strech',
				},
				margin: 5,
				items: [
					{
						html: '<font style="font-style: italic;font-size: 16px; font-weight: bold;">Inscrire les stagiaires:</font>',
					},
					{
						itemId: 'ouiEmployeBtn',
						xtype: 'button',
						text: 'Employé(es) d\' ADES',
						ui: 'soft-green-small',
						margin: '10 0 0 0',
						width: 250,
						bind: {
							disabled: '{isEnabledChoiceBtn}',
						},
						listeners: {
							click: () => {
								Ext.getCmp('contentPanelParticipantid').getController('formationController').ouiEmployeAction(Ext.getCmp('formationGridid'));
							},
						},
					},
					{
						itemId: 'nonEmployeBtn',
						xtype: 'button',
						text: 'Personne de l\'extérieur',
						ui: 'soft-blue-small',
						margin: '10 0 0 0',
						width: 250,
						bind: {
							disabled: '{isEnabledChoiceBtn}',
						},
						listeners: {
							click: () => {
								Ext.getCmp('contentPanelParticipantid').getController('formationController').nonEmployeAction(Ext.getCmp('formationGridid'));
							},
						},
					},
					{
						html: '<br><font style="font-style: italic;font-size: 16px; font-weight: bold;">Listes:</font>',
					},
					{
						itemId: 'listeStatigaireBySessionBtn',
						xtype: 'button',
						text: 'Inscrits à la formation',
						ui: 'soft-purple-small',
						margin: '10 0 0 0',
						width: 250,
						bind: {
							disabled: '{isEnabledChoiceBtn}',
						},
						listeners: {
							click: () => {
								Ext.getCmp('contentPanelParticipantid').getController('formationController').listeStagiaireBySessionAction(Ext.getCmp('formationGridid').selection.get('idSessionFormation'), 'containerInscriptionid');
							},
						},
					},
					{
						itemId: 'listeStatigaireBtn',
						xtype: 'button',
						text: 'Tous les stagiaires',
						ui: 'soft-purple-small',
						margin: '10 0 0 0',
						width: 250,
						listeners: {
							click: () => {
								Ext.getCmp('contentPanelParticipantid').getController('formationController').listeStagiaireAction();
							},
						},
					},
					
				],
			},
		];
		
		this.callParent();
	},

});
