Ext.define('Gprh.ades.view.formation.EvaluationMainContainer', {
	extend: 'Ext.panel.Panel',

	xtype: 'evaluationMainContainer',

	requires: [
		'Gprh.ades.view.formation.FormationListeEvaluation',
	],

	itemId: 'evaluationMainContainerId',
	id: 'evaluationMainContainerid',

	layout: {
		type: 'vbox',
		align: 'stretch',
	},

	bodyPadding: 1,

	controller: 'formationController',
	
	initComponent() {
		this.items = [
			{
				xtype: 'container',
				itemId: 'containerEvaluationContentId',
				id: 'containerEvaluationContentid',
				items: [
					{
						xtype: 'formationListeEvaluation',
					},
				],
			},
		];

		this.callParent();
	},
});
