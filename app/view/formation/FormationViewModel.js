Ext.define('Gprh.ades.view.formation.FormationViewModel', {
	extend: 'Ext.app.ViewModel',

	alias: 'viewmodel.formationViewModel',

	requires: [
		'Gprh.ades.store.Formateur',
		'Gprh.ades.store.Catalogue',
		'Gprh.ades.store.DomaineFormation',
		'Gprh.ades.store.TypeFormation',
	],

	stores: {
		formateurResults: {
			type: 'formateurStore',
			proxy: {
				extraParams: {
					infoRequest: 1,
				},
			},
		},
		catalogueResults: {
			type: 'catalogueStore',
			proxy: {
				extraParams: {
					infoRequest: 1,
				},
			},
		},
		typeFormationResults: {
			type: 'typeFormationStore',
		},
		domaineFormationResults: {
			type: 'domaineFormationStore',
		},
	},

	data: {
		switchItem: 1,
	},
	
	formulas: {
		isEnabledChoiceBtn: {
			bind: {
				t1: '{!formationGridRef.selection}',
			},
			// 1 session de formation est sélectionnée
			get: d => d.t1,
		},
		isEnabledChoiceBtn2: {
			bind: {
				t2: '{!formationEvaluationGridRef.selection}',
			},
			// 1 session de formation est sélectionnée
			get: d => d.t2,
		},
	},
});
