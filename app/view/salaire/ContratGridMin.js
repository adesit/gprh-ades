Ext.define('Gprh.ades.view.salaire.ContratGridMin', {
	extend: 'Ext.grid.Panel',
	xtype: 'contratGridMin',

	requires: [
		'Gprh.ades.store.Contrat',
	],

	viewModel: {
		stores: {
			contratResults: {
				type: 'contratStore',
				storeId: 'contratResultsId',
				proxy: {
					extraParams: {
						infoRequest: 1,
						typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
						idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
					},
				},
			},
			titrePosteFilter: {
				source: 'contratResultsId',
				sorters: [{
					property: 'titrePoste',
					direction: 'ASC',
				}],
			},
			categorieReelleFilter: {
				source: 'contratResultsId',
				sorters: [{
					property: 'categorieReelle',
					direction: 'ASC',
				}],
			},
		},
	},

	bind: {
		store: '{contratResults}',
	},
	
	initComponent() {
		this.reference = 'contratGridMinRef';
		this.bind = {
			store: '{contratResults}',
		};
		this.viewConfig = {
			preserveScrollOnRefresh: true,
			stripeRows: true,
			onStoreLoad: Ext.emptyFn,
		};
		this.plugins = 'gridfilters';
		this.scrollable = true;
		this.selModel = 'rowmodel';
		this.listeners = {
			rowdblclick: 'onRowContratMinDblClick',
			itemmouseenter: (view, record, item) => {
				Ext.fly(item).set({ 'data-qtip': 'Double clic dessus pour voir les détails.' });
			},
		};
		this.height = Ext.Element.getViewportHeight() - 125;
		this.columnLines = true;
		this.columns = {
			defaults: {
				align: 'left',
			},
			items: [
				{
					xtype: 'gridcolumn',
					dataIndex: 'abreviationCentre',
					text: 'Centre',
					hidden: false,
					width: 75,
					filter: {
						type: 'list',
					},
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'matricule',
					text: 'Matricule',
					width: 100,
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'actifContrat',
					text: 'En cours',
					renderer: value => (value ? 'Oui' : ''),
					filter: {
						type: 'list',
					},
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'idContrat',
					text: '#',
					hidden: true,
					width: 100,
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'nomPrenom',
					text: 'Noms et prénoms',
					width: 300,
					filter: {
						type: 'string',
					},					
					// locked: true,
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'titrePoste',
					text: 'Fonction/Poste occupé',
					width: 350,
					filter: {
						type: 'list',
						idField: 'titrePoste',
						labelField: 'titrePoste',
						store: this.getViewModel().getStore('titrePosteFilter'),
					},
				},
				{
					xtype: 'gridcolumn',
					dataIndex: 'categorieReelle',
					text: 'Catégorie',
					width: 125,
					filter: {
						type: 'list',
						idField: 'categorieReelle',
						labelField: 'categorieReelle',
						store: this.getViewModel().getStore('categorieReelleFilter'),
					},
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'dateEmbauche',
					text: 'Embauché(e) le',
					width: 125,
					renderer: value => Gprh.ades.util.Globals.rendererDate(value),
				},
				{
					xtype: 'datecolumn',
					dataIndex: 'datePrisePoste',
					text: 'Prise de poste',
					width: 125,
					renderer: value => Gprh.ades.util.Globals.rendererDate(value),
				},
			],
		};
		this.dockedItems = [
			Gprh.ades.util.Globals.getPagingToolbar(this),
		];
		this.tbar = [];

		this.callParent();
	},
});
