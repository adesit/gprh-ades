Ext.define('Gprh.ades.view.salaire.SalaireViewModel', {
	extend: 'Ext.app.ViewModel',

	alias: 'viewmodel.salaireViewModel',

	requires: [
		'Gprh.ades.store.Salaire',
		'Gprh.ades.store.Mois',
		'Gprh.ades.store.Contrat',
		'Gprh.ades.store.Centre',
		'Gprh.ades.store.Poste',
		'Gprh.ades.store.Section',
		'Gprh.ades.store.Departement',
		'Gprh.ades.store.Categorie',
		'Gprh.ades.store.Smie',
		'Gprh.ades.store.TypeContrat',
		'Gprh.ades.store.NatureContrat',
		'Gprh.ades.store.ModePaiement',
		'Gprh.ades.store.HeuresSupplementaires',
		'Gprh.ades.store.Absence',
		'Gprh.ades.store.Conge',
		'Gprh.ades.store.FraisMedicaux',
		'Gprh.ades.store.DiversRetenus',
		'Gprh.ades.store.Depassement',
		'Gprh.ades.store.Avance',
		'Gprh.ades.store.AvanceDecallage',
		'Gprh.ades.store.Rappel',
	],

	stores: {
		salaireResults: {
			type: 'salaireStore',
		},
		moisResults: {
			type: 'moisStore',
		},
		centreResults: {
			type: 'centreStore',
		},
		posteResults: {
			type: 'posteStore',
			proxy: {
				extraParams: {
					infoRequest: 2,
				},
			},
		},
		sectionResults: {
			type: 'sectionStore',
		},
		departementResults: {
			type: 'departementStore',
		},
		categorieResults: {
			type: 'categorieStore',
		},
		smieResults: {
			type: 'smieStore',
		},
		typeContratResults: {
			type: 'typeContratStore',
		},
		natureContratResults: {
			type: 'natureContratStore',
		},
		modePaiementResults: {
			type: 'modePaiementStore',
		},
	},

	data: {
		switchItem: 1,
	},
	
	formulas: {
		isEnabled: {
			bind: {
				t1: '{!moisFieldReference.value}',
				t2: '{!idContratRef.value}',
				t3: '{!infHuitHeuresRef.value}',
				t4: '{!supHuitHeuresRef.value}',
				t5: '{!nuitHabituelleRef.value}',
				t6: '{!nuitOccasionnelleRef.value}',
				t7: '{!dimancheRef.value}',
				t8: '{!ferieRef.value}',
			},
			/* 
			* 1 contrat est sélectionné
			* + 1 mois salarial
			* + au moins une zone de texte des H.Sup est remplie
			*/
			get: d => (d.t2 || d.t1 || (d.t3 && d.t4 && d.t5 && d.t6 && d.t7 && d.t8)),
		},
		isEnabledEmploye: {
			bind: {
				t1: '{!contratGridMinRef.selection}',
			},
			// 1 contrat est sélectionné
			get: d => d.t1,
		},
		isEnabledMois: {
			bind: {
				t1: '{!moisFieldReference.value}',
			},
			// 1 mois salarial est sélectionné
			get: d => d.t1,
		},
		isHiddenDureeAbs: {
			bind: {
				t1: '{switchItem}',
			},
			/*
			 * Absences sans motif => cacher les zones de date
			 * Permission pour évènement familial => cacher le zone pour heures minus
			 */
			get: data => (data.t1.typeAbsence > 0),
		},
		isHiddenSoldePermission: {
			bind: {
				t1: '{switchItem}',
			},
			/*
			 * Absences sans motif => cacher les zones de date
			 * Permission pour évènement familial => cacher le zone pour heures minus
			 */
			get: data => (data.t1.typeAbsence === 2),
		},
		isEnabledAbs: {
			bind: {
				t1: '{moisFieldReference.value}',
				t2: '{idContratRef.value}',
				t3: '{switchItem}',
				t4: '{dureeAbsenceRef.value}',
				t5: '{dateHeureDebutHsRef.value}',
				t6: '{dateHeureFinHsRef.value}',
				t7: '{contratGridMinRef.selection}',
				t8: '{motifAbsencesRef.value}',
			},
			/* 
			* 1 contrat est sélectionné
			* + 1 mois salarial
			* + (si Heures minus => Durée obligatoire
				 si Permission pour évènements familiaux => Début et fin obligatoires)
			*/
			get: (d) => {
				const condition1 = !Ext.isEmpty(d.t7);
				const condition2 = (!Ext.isEmpty(d.t2) && !Ext.isEmpty(d.t1) && (d.t3.typeAbsence === 1) && !Ext.isEmpty(d.t4));
				const condition3 = (!Ext.isEmpty(d.t2) && !Ext.isEmpty(d.t1) && (d.t3.typeAbsence === 2) && !Ext.isEmpty(d.t5) && !Ext.isEmpty(d.t6));
				const condition4 = (!Ext.isEmpty(d.t2) && !Ext.isEmpty(d.t1) && (d.t3.typeAbsence === 3) && !Ext.isEmpty(d.t5) && !Ext.isEmpty(d.t6));
				const condition5 = !Ext.isEmpty(d.t8);
				
				return condition1 && (condition2 || condition3 || condition4) && condition5;
			},
		},
		isEnabledCng: {
			bind: {
				t1: '{moisFieldReference.value}',
				t2: '{idContratRef.value}',
				t3: '{switchItem}',
				t4: '{dureeCongePriseRef.value}',
				t5: '{dateDebutCongeRef.value}',
				t6: '{dateFinCongeRef.value}',
				t7: '{contratGridMinRef.selection}',
			},
			/* 
			* 1 contrat est sélectionné
			* + 1 mois salarial
			* + (si Congé de maternité => Début et fin obligatoire
				 si Congé payé => durée obligatoire)
			*/
			get: (d) => {
				// const condition1 = !Ext.isEmpty(d.t7);
				const condition2 = (!Ext.isEmpty(d.t2) && !Ext.isEmpty(d.t1) && (d.t3.natureConge === 2) && !Ext.isEmpty(d.t4));
				const condition3 = (!Ext.isEmpty(d.t2) && !Ext.isEmpty(d.t1) && (d.t3.natureConge === 1) && !Ext.isEmpty(d.t5) && !Ext.isEmpty(d.t6));
				
				// return condition1 && (condition2 || condition3);
				return (condition2 || condition3);
			},
		},
		materinteFinCng: {
			bind: {
				t3: '{switchItem}',
				t5: '{dateDebutCongeRef.value}',
			},
			get: (d) => {
				// Le droit au congé de maternité est de 3mois = 98 jours 
				const finCongeMaternite = ((d.t3.natureConge === 1) && !Ext.isEmpty(d.t5)) ? Ext.Date.add(d.t5, Ext.Date.DAY, 98) : 0;
				return finCongeMaternite;
			},
		},
		isEnabledCngCmp: get => (get('reliquatRef') > 0 || (get('reliquatRef2') > 0 && get('reliquatRef') === 0)),
		isEnabledSalaireBtn: {
			bind: {
				t1: '{salaireGridRef.selection}',
				t2: '{salaireGridRef.selection.clotureSalaire == 0}',
			},
			get: d => d.t1 && d.t2,
		},
	},
});
