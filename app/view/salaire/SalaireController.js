Ext.define('Gprh.ades.view.salaire.SalaireController', {
	extend: 'Ext.app.ViewController',
	
	alias: 'controller.salaireController',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.store.SalaireInterim',
		'Gprh.ades.view.contrat.ContratFormPrint',
		'Gprh.ades.store.CongeSolde',
		'Gprh.ades.util.Print',
		'Gprh.ades.util.Downloader',
		'Gprh.ades.store.ListeMotifPermission',
		'Gprh.ades.store.AbsenceSoldePermission',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	init() {
		this.setCurrentView('salaireGrid');
	},

	setCurrentView(view, params) {
		const contentPanel = this.getView().down('#contentPanelSalaire');
		// We skip rendering for the following scenarios:
		// * There is no contentPanel
		// * view xtype is not specified
		// * current view is the same
		if (!contentPanel || view === '' || (contentPanel.down() && contentPanel.down().xtype === view)) {
			return false;
		}

		if (params && params.openWindow) {
			const cfg = Ext.apply({
				xtype: 'prototypeWindow',
				items: [
					Ext.apply({
						xtype: view,
					}, params.targetCfg),
				],
			}, params.windowCfg);

			Ext.create(cfg);
		} else {
			Ext.suspendLayouts();

			contentPanel.removeAll(true);
			contentPanel.add(
				Ext.apply({
					xtype: view,
				}, {
					parameters: params,
				})
			);

			Ext.resumeLayouts(true);
		}
		return true;
	},

	onDeleteSalaireClick(parameters) {
		const grid = this.getView().down(`#${parameters.params.gridId}`);
		const selection = grid.getSelection();

		const salaireParams = {
			idSalaire: selection[0].get('idSalaire'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxSalaireDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelSalaire = {
						infoRequest: 3,
						data: salaireParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/Salaire.php', modelDelSalaire);
					const result = saveOrUpdate.request.request.result.responseText;
					if (result > 0) {
						btn.up('#messageBoxSalaireDelete').close();
						this.getView().down('#salaireGridId').getStore().load();
					} else if (Ext.typeOf(result) === 'string') {
						btn.up('#messageBoxSalaireDelete').close();
						console.error(Ext.decode(result).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxSalaireDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	
	async onRunSalaireClick() {
		const grid = this.getView().down('#salaireGridId');
		const selection = grid.getSelection();
		const gridDetails = this.getView().up('#salaireMainContainerId').down('#salaireDetailsGridId');
		
		const extraParameters = {
			extraParams: {
				moisSalarial: selection[0].get('anneeSalaire')+'-'+selection[0].get('moisSalaire'),
				infoRequest: 1, // tous les employés d'ADES
				action: 1, // action == 1 retourne les données à afficher
			},
		};
		// Va faire les calculs et récupérer les résultats, puis afficher dans le grid SalaireDetailsGrid le store ainsi obtenu
		this.getView().up('#salaireMainContainerId').mask("Veuillez patientez ...");
		const inputSalairesData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.SalaireDetails', extraParameters);
		this.getView().up('#salaireMainContainerId').unmask();
		gridDetails.setStore(inputSalairesData);

		// Télécharge un fichier Excel prototype d'Etat de paie
		Ext.getBody().mask('Téléchargement...');

		// Va exporter les données du salaire clôturé sur Excel (2007 .xlsx) en format Etat de paie
		Gprh.ades.util.Downloader.get({
			url: 'server-scripts/formule/DetailsFormule.php',
			params: {
				infoRequest: 1, // tous les employés d'ADES
				action: 5, // action == 5 exporte un Etat de paie pré-clôture
				start: 0,
				page: 1,
				limit: 25,
				// idSalaire: selection[0].get('idSalaire'),
				mois: selection[0].get('moisSeulement') + '-' +  selection[0].get('anneeSalaire'),
				moisSalarial: selection[0].get('anneeSalaire')+'-'+selection[0].get('moisSalaire'),
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		});

	},
	
	cancelAction(formId) {
		const form = this.getView().down(`#${formId.params}`);
		form.reset();

		this.getView().down('#contratGridMinId').getStore().reload();
		if (formId.params === 'HSFormId') {
			const HSGrid = this.getView().down('#heuresSupplementaireGridId');
			HSGrid.down('#gridpanel').getStore().reload();
		}
	},


	/*
	 * Les paramètres de cette fonction pourront être (record, element, rowIndex, e, eOpts)
	 */
	async onContratInterimClick(view, record, rowIndex) {
		const grid = this.getView().down('#salaireInterimGridId');
		
		const allFields = [
			'idContrat',
		];
		if (!Ext.isEmpty(record)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.get(field));
			});

			const extraParameters = {
				extraParams: {
					infoRequest: 2,
					idContrat: record.get('idContrat'),
				},
			};
			// Récupère toutes les périodes intérimaires de l'employé
			const inputSalaireInterimData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.SalaireInterim', extraParameters);
			
			grid.setStore(inputSalaireInterimData);
		}
	},

	async onSalaireInterimFormSaveClick() {
		const form = this.getView().down('#salaireInterimFormId');
		const grid = this.getView().down('#salaireInterimGridId');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/SalaireInterim.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			const extraParameters = {
				extraParams: {
					infoRequest: 2,
					idContrat: serverResponse,
				},
			};
			// Récupère toutes les périodes intérimaires de l'employé
			const inputSalaireInterimData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.SalaireInterim', extraParameters);
			grid.getStore().reload();
		} else if (Ext.typeOf(serverResponse) === 'string') {
			console.error(Ext.decode(serverResponse).error);
			Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
		}

		form.reset();
	},

	confirmDeleteSalaireInterim(view, cell, recordIndex, cellIndex, e, record) {
		const salaireInterimParams = {
			idPrimeIndemnite: record.get('idPrimeIndemnite'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxSalaireInterimDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelSalaireInterim = {
						infoRequest: 3,
						data: salaireInterimParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/SalaireInterim.php', modelDelSalaireInterim);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxSalaireInterimDelete').close();
						this.getView().down('#salaireInterimGridId').getStore().load();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxSalaireInterimDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	onSalaireInterimRowClick(record) {
		const allFields = [
			'idPrimeIndemnite',
			'idContrat',
			'dateDebutInterim',
			'dateFinInterim',
		];
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.selection.get(field));
			});
		}
	},

	async onRowContratMinDblClick(view, record) {
		if (Ext.util.LocalStorage.get('foo').getItem('nomUtilisateur') === 'Amanda') return false; // Jusqu'à nouvel ordre
		else {
			const extraParameters = {
				extraParams: {
					infoRequest: 2,
					idEmploye: record.data.idEmploye,
				},
			};
	
			// Chercher tous les contrats d'un employé par son idEmploye
			const inputContratData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Contrat', extraParameters);
			
			const contratBaseWindow = {
				xtype: 'prototypeWindow',
				title: 'Contrat de travail',
				maxWidth: 750,
				maxHeight: 525,
				padding: 10,
				items: [
					{
						xtype: 'contratFormPrint',
						parameters: [inputContratData, record.data, 'edit'],
					},
				],
			};
			Ext.create(contratBaseWindow);
		}
	},

	onContratHSClick(view, record, rowIndex) {
		const form = this.getView().down('#heuresSupplementairesFormPanelId');
		
		const allFields = [
			'idContrat',
		];
		if (!Ext.isEmpty(record)) {
			if (record.get('categorieReelle').indexOf('HC') > -1 || record.get('titrePoste').indexOf('Agent d\'entretien et de sécurité') > -1) {
				form.setDisabled(true);
			}
			else {
				form.setDisabled(false);
				Ext.each(allFields, (field) => {
					this.getView().down(`#${field}Id`).setValue(record.get(field));
				});
			}
		}
	},

	async onHSFormSaveClick() {
		const contratGridMin = Ext.ComponentQuery.query('#contratGridMinId')[0];
		contratGridMin.view.saveScrollState();

		const form = this.getView().down('#HSFormId');
		const HSgrid = this.getView().down('#heuresSupplementaireGridId').down('#gridpanel');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/HeuresSupplementaires.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			HSgrid.getStore().reload();
		} else if (Ext.typeOf(serverResponse) === 'string') {
			console.error(Ext.decode(serverResponse).error);
			Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
		}
		
		form.reset();
		contratGridMin.view.restoreScrollState();
	},

	async onHSEmployeListClick() {
		const contratGridMin = this.getView().down('#contratGridMinId');
		const s = contratGridMin.getSelectionModel().getSelection();
		const ids = [];
		Ext.each(s, (record) => {
			ids.push(record.get('idContrat'));
		});
		const HSgrid = this.getView().down('#heuresSupplementaireGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 2,
				idContrat: ids[0],
			},
		};
		
		// Récupère toutes les heures supplémentaires de l'employé
		const inputHSData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.HeuresSupplementaires', extraParameters);
		
		HSgrid.setStore(inputHSData);

		this.getView().up().down('#heuresSupplementairesGridPanelId').expand(true);
	},

	async onHSAllListClick() {
		const HSgrid = this.getView().down('#heuresSupplementaireGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère toutes les heures supplémentaires
		const inputHSData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.HeuresSupplementaires', extraParameters);
		
		HSgrid.setStore(inputHSData);

		this.getView().up().down('#heuresSupplementairesGridPanelId').expand(true);
	},

	async onHSMoisListClick() {
		const moisSalaire = this.getView().down('#moisSalaireId');
		const HSgrid = this.getView().down('#heuresSupplementaireGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 3,
				moisSalaire: moisSalaire.getValue(),
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère toutes les heures supplémentaires par mois
		const inputHSData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.HeuresSupplementaires', extraParameters);
		
		HSgrid.setStore(inputHSData);

		this.getView().up().down('#heuresSupplementairesGridPanelId').expand(true);
	},

	onRowHSClick(record) {
		const allFields = [
			'idHeureSupplementaire',
			'idContrat',
			'infHuitHeures',
			'supHuitHeures',
			'nuitHabituelle',
			'nuitOccasionnelle',
			'dimanche',
			'ferie',
			'remarqueMotifHs',
			'moisSalaire',
		];
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.selection.get(field));
			});
			this.getView().up().down('#heuresSupplementairesFormPanelId').expand(true);
		}
	},

	confirmDeleteHS(view, cell, recordIndex, cellIndex, e, record) {
		const HSParams = {
			idHeureSupplementaire: record.get('idHeureSupplementaire'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxHSDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelHS = {
						infoRequest: 3,
						data: HSParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/HeuresSupplementaires.php', modelDelHS);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxHSDelete').close();
						this.getView().down('#heuresSupplementaireGridId').down('#gridpanel').getStore().load();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxHSDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	async onContratAbsClick(view, record, rowIndex) {
		const allFields = [
			'idContrat',
		];
		const soldeField = Ext.ComponentQuery.query('#soldePermissionId')[0];
		const soldeField2 = Ext.ComponentQuery.query('#soldePermissionId2')[0];
		
		if (!Ext.isEmpty(record)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.get(field));
			});
			
			const extraParamsSolde = {
				extraParams: {
					infoRequest: 1,
					idContrat: record.get('idContrat'),
				},
			};
			const soldePermissionData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.AbsenceSoldePermission', extraParamsSolde);
			const soldeObject = soldePermissionData.findRecord('idContrat', record.get('idContrat'));
			const soldeValue = Ext.isEmpty(soldeObject) ? 10 : soldeObject.get('soldePermission');
			
			view.view.saveScrollState();
			soldeField.setValue(soldeValue);
			soldeField2.setValue(soldeValue);
			view.view.restoreScrollState();
			
		}
	},

	refreshDureeSoldePermission(cmp, newValue, oldValue, eOpts) {
		const dureeField = this.getView().down('#nbJoursId');
		const nbHeursField = this.getView().down('#dureeAbsenceId');

		const debutAbsField = this.getView().down('#dateHeureDebutHsId');
		const debutValue = debutAbsField.getValue();

		const finAbsField = this.getView().down('#dateHeureFinHsId');
		const finValue = finAbsField.getValue();

		const typeAbsenceRadio = this.getView().down('#typeAbsenceId');
		const typeAbsenceValue = typeAbsenceRadio.getChecked();
		
		const dureeValue = (typeAbsenceValue[0].inputValue !== 2) ? Ext.Date.diff(debutValue, finValue, Ext.Date.DAY) + 1 : this.dureePermission(debutValue, finValue) ; // TO DO if permission, not count the week-end days

		dureeField.setValue(dureeValue);
		nbHeursField.setValue(dureeValue * 8);

		const reliquatField = this.getView().down('#soldePermissionId');
		const reliquatFieldOld = this.getView().down('#soldePermissionId2');
		const reliquatOldValue = reliquatFieldOld.getValue();
		const nouveauReliquat = (typeAbsenceValue[0].inputValue == 1) ? reliquatOldValue : reliquatOldValue - dureeValue;
		
		reliquatField.setValue(nouveauReliquat);
	},

	dureePermission(startDate, endDate) {
		// Validate input
		if (endDate < startDate)
        return 0;
    
		// Calculate days between dates
		var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
		startDate.setHours(0,0,0,1);  // Start just after midnight
		endDate.setHours(23,59,59,999);  // End just before midnight
		var diff = endDate - startDate;  // Milliseconds between datetime objects    
		var days = Math.ceil(diff / millisecondsPerDay);
		
		// Subtract two weekend days for every week in between
		var weeks = Math.floor(days / 7);
		days = days - (weeks * 2);

		// Handle special cases
		var startDay = startDate.getDay();
		var endDay = endDate.getDay();
		
		// Remove weekend not previously removed.   
		if (startDay - endDay > 1)         
			days = days - 2;      
		
		// Remove start day if span starts on Sunday but ends before Saturday
		if (startDay == 0 && endDay != 6)
			days = days - 1  
				
		// Remove end day if span ends on Saturday but starts after Sunday
		if (endDay == 6 && startDay != 0)
			days = days - 1  
		
		return days;
	},

	addMotifAbsenceField(comp, newValue, oldValue, eOpts) {
		const contratGridMin = Ext.ComponentQuery.query('#contratGridMinId')[0];
		contratGridMin.view.saveScrollState();
		
		const form = this.getView().down('#AbsFormId');
		const motifField = Ext.ComponentQuery.query('#motifAbsenceId')[0];
		form.remove(motifField);
		//form.update();
		if (newValue.typeAbsence === 2) {
			const motifStore = Ext.create('Gprh.ades.store.ListeMotifPermission');
			form.add({
				xtype: 'combobox',
				name: 'motifAbsence',
				itemId: 'motifAbsenceId',
				reference: 'motifAbsencesRef',
				publishes: 'value',
				fieldLabel: 'Remarques ou motifs',
				store: motifStore,
				queryMode: 'local',
				displayField: 'motif',
				valueField: 'motif',
				renderTo: Ext.getBody(),
				allowBlank: false,
				forceSelection: true,
				editable: false,
				margin: '0 0 10 0',
			});
			
		} else {
			form.add({
				xtype: 'textfield',
				name: 'motifAbsence',
				itemId: 'motifAbsenceId',
				reference: 'motifAbsencesRef',
				publishes: 'value',
				labelWidth: 150,
				labelAlign: 'top',
				labelSeparator: ':',
				width: 325,
				fieldLabel: 'Remarques ou motifs',
				allowBlank: false,
			});
		}
	},

	onAbsFormSaveClick() {
		const contratGridMin = Ext.ComponentQuery.query('#contratGridMinId')[0];
		contratGridMin.view.saveScrollState();

		const form = this.getView().down('#AbsFormId');
		const Absgrid = this.getView().down('#absenceGridId').down('#gridpanel');

		
		const typeAbsenceRadio = this.getView().down('#typeAbsenceId');
		const typeAbsenceValue = typeAbsenceRadio.getChecked();
		const reliquatField = this.getView().down('#soldePermissionId');
		
		if (typeAbsenceValue[0].inputValue === 2 && reliquatField.getValue() < 0) {
			Ext.Msg.show({
				title: 'Permission sans solde',
				message: 'Désolé! Traiter la demande autrement. Le solde de permission est insuffisant.',
				icon: Ext.MessageBox.INFO,
				buttons: Ext.MessageBox.OK,
			});
		} else {
				const model = {
					infoRequest: 2,
					data: form.getValues(),
				};
				const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/Absence.php', model);
				const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
				
				if (serverResponse > 0) {
					Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
					Absgrid.getStore().reload();
				} else if (Ext.typeOf(serverResponse) === 'string') {
					console.error(Ext.decode(serverResponse).error);
					Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
				}
		
				form.reset();
				contratGridMin.view.restoreScrollState();
		}		
	},

	confirmDeleteAbs(view, cell, recordIndex, cellIndex, e, record) {
		const AbsParams = {
			idAbsence: record.get('idAbsence'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxAbsDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelAbs = {
						infoRequest: 3,
						data: AbsParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/Absence.php', modelDelAbs);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxAbsDelete').close();
						this.getView().down('#absenceGridId').down('#gridpanel').getStore().load();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxAbsDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	async onAbsEmployeListClick() {
		const contratGridMin = this.getView().down('#contratGridMinId');
		const s = contratGridMin.getSelectionModel().getSelection();
		const ids = [];
		Ext.each(s, (record) => {
			ids.push(record.get('idContrat'));
		});
		const Absgrid = this.getView().down('#absenceGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 2,
				idContrat: ids[0],
			},
		};
		
		// Récupère toutes les absences de l'employé
		const inputAbsData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Absence', extraParameters);
		
		Absgrid.setStore(inputAbsData);

		this.getView().up().down('#absenceGridPanelId').expand(true);
	},

	async onAbsAllListClick() {
		const Absgrid = this.getView().down('#absenceGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère toutes les absences
		const inputAbsData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Absence', extraParameters);
		
		Absgrid.setStore(inputAbsData);

		this.getView().up().down('#absenceGridPanelId').expand(true);
	},

	async onAbsMoisListClick() {
		const moisSalaire = this.getView().down('#idSalaireId');
		const Absgrid = this.getView().down('#absenceGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 3,
				moisSalaire: moisSalaire.getValue(),
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère toutes les absences par mois
		const inputAbsData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Absence', extraParameters);
		
		Absgrid.setStore(inputAbsData);

		this.getView().up().down('#absenceGridPanelId').expand(true);
	},

	onRowAbsClick(record) {
		const allFields = [
			'idAbsence',
			'idContrat',
			'idSalaire',
			'dureeAbsence',
			'dateHeureDebutHs',
			'dateHeureFinHs',
			'motifAbsence',
			'nbJours',
		];
		let valueToShow;
		Ext.ComponentQuery.query('#typeAbsenceId')[0].items.items[record.selection.get('typeAbsence') - 1].setValue(true);
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				if (field === 'dateHeureDebutHs' || field === 'dateHeureFinHs') valueToShow = Ext.util.Format.date(record.selection.get(field), 'd/m/Y');
				else valueToShow = record.selection.get(field);
				this.getView().down(`#${field}Id`).setValue(valueToShow);
			});
			this.getView().up().down('#absenceFormPanelId').expand(true);
			this.getView().up().down('#AbsFormSaveBtn').setDisabled(false);
		}
	},

	async onContratCngClick(view, record, rowIndex) {
		const grid = this.getView('#contratGridMinId').down('#contratGridMinId');
		const allFields = [
			'idContrat',
		];
		if (!Ext.isEmpty(record)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.get(field));
			});
			
			const extraParamsSolde = {
				extraParams: {
					infoRequest: 2,
					idContrat: record.get('idContrat'),
				},
			};
			const soldeCongeData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.CongeSolde', extraParamsSolde);
			const soldeValue = soldeCongeData.findRecord('idContrat', record.get('idContrat')).get('reliquat');

			view.view.saveScrollState();
			this.getView().down('#reliquatId').setValue(soldeValue);
			this.getView().down('#reliquatId2').setValue(soldeValue);
			view.view.restoreScrollState();
			
			if (soldeValue < 0) {
				Ext.Msg.show({
					title: 'Congé sans solde',
					message: 'Désolé! Vous serez obligé de traiter la demande comme "Heures minus".',
					icon: Ext.MessageBox.INFO,
					buttons: Ext.MessageBox.OK,
				});
			}
		}
	},

	onCngFormSaveClick() {
		const form = this.getView().down('#CngFormId');
		const Cnggrid = this.getView().down('#congeGridId').down('#gridpanel');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/Conge.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			Cnggrid.getStore().reload();
		} else if (Ext.typeOf(serverResponse) === 'string') {
			console.error(Ext.decode(serverResponse).error);
			Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
		}

		form.reset();
	},

	onRowCngClick(record) {
		const allFields = [
			'idConge',
			'idContrat',
			'idSalaire',
			'dateDebutConge',
			'dateFinConge',
			'remarqueConge',
			'dureeCongePrise',
		];
		let valueToShow;
		Ext.ComponentQuery.query('#natureCongeId')[0].items.items[record.selection.get('natureConge') - 1].setValue(true);
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				if (field === 'dateDebutConge' || field === 'dateFinConge') valueToShow = Ext.util.Format.date(record.selection.get(field), 'd/m/Y');
				else valueToShow = record.selection.get(field);
				this.getView().down(`#${field}Id`).setValue(valueToShow);
			});
			
			this.getView().up().down('#congeFormPanelId').expand(true);
			Ext.ComponentQuery.query('#CngFormSaveBtn')[0].setDisabled(false);
		}
	},

	confirmDeleteCng(view, cell, recordIndex, cellIndex, e, record) {
		const CngParams = {
			idConge: record.get('idConge'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxCngDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelCng = {
						infoRequest: 3,
						data: CngParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/Conge.php', modelDelCng);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxCngDelete').close();
						this.getView().down('#congeGridId').down('#gridpanel').getStore().load();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxCngDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	async onCngEmployeListClick() {
		const contratGridMin = this.getView().down('#contratGridMinId');
		const s = contratGridMin.getSelectionModel().getSelection();
		const ids = [];
		Ext.each(s, (record) => {
			ids.push(record.get('idContrat'));
		});
		const Cnggrid = this.getView().down('#congeGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 2,
				idContrat: ids[0],
			},
		};
		
		// Récupère tous les congés de l'employé
		const inputCngData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Conge', extraParameters);
		
		Cnggrid.setStore(inputCngData);

		this.getView().up().down('#congeGridPanelId').expand(true);
	},

	async onCngAllListClick() {
		const Cnggrid = this.getView().down('#congeGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère tous les congés
		const inputCngData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Conge', extraParameters);
		
		Cnggrid.setStore(inputCngData);

		this.getView().up().down('#congeGridPanelId').expand(true);
	},

	async onCngMoisListClick() {
		const moisSalaire = this.getView().down('#idSalaireId');
		const Cnggrid = this.getView().down('#congeGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 3,
				moisSalaire: moisSalaire.getValue(),
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère tous les congés par mois
		const inputCngData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Conge', extraParameters);
		
		Cnggrid.setStore(inputCngData);

		this.getView().up().down('#congeGridPanelId').expand(true);
	},

	refreshDureeSoldeConge(textField, newValue, oldValue, eOpts) {
		const dureeField = this.getView().down('#dureeCongePriseId');
		const debutCngField = this.getView().down('#dateDebutCongeId');
		const debutValue = debutCngField.getValue();
		const finCngField = this.getView().down('#dateFinCongeId');
		const finValue = finCngField.getValue();
		const typeCongeRadio = this.getView().down('#natureCongeId');
		const typeCongeValue = typeCongeRadio.getChecked();
		
		const dureeValue = (typeCongeValue[0].inputValue == 1) ? Ext.Date.diff(debutValue, finValue, Ext.Date.DAY) : Ext.Date.diff(debutValue, finValue, Ext.Date.DAY) + 1;

		dureeField.setValue(dureeValue);

		const reliquatField = this.getView().down('#reliquatId');
		const reliquatFieldOld = this.getView().down('#reliquatId2');
		const reliquatOldValue = reliquatFieldOld.getValue();
		const nouveauReliquat = (typeCongeValue[0].inputValue == 1) ? reliquatOldValue : reliquatOldValue - dureeValue;
		
		reliquatField.setValue(nouveauReliquat);
	},
	
	refreshDureeSoldeCongeAfterEdit(textField, newValue, oldValue, eOpts) {
		const reliquatFieldOld = this.getView().down('#reliquatId2');
		const reliquatField = this.getView().down('#reliquatId');
		const reliquatOldValue = reliquatFieldOld.getValue();
		const nouveauReliquat = reliquatOldValue - newValue;
		
		reliquatField.setValue(nouveauReliquat);
	},

	onContratFMClick(view, record, rowIndex) {
		const allFields = [
			'idContrat',
		];
		if (!Ext.isEmpty(record)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.get(field));
			});
		}
	},

	onFMFormSaveClick() {
		const contratGridMin = this.getView().down('#contratGridMinId');
		contratGridMin.view.saveScrollState();

		const form = this.getView().down('#FMFormId');
		const FMgrid = this.getView().down('#fraisMedicauxGridId').down('#gridpanel');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/FraisMedicaux.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			FMgrid.getStore().reload();
		} else if (Ext.typeOf(serverResponse) === 'string') {
			console.error(Ext.decode(serverResponse).error);
			Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
		}

		form.reset();
		contratGridMin.view.restoreScrollState();
	},

	onRowFMClick(record) {
		const allFields = [
			'idMedicaux',
			'idContrat',
			'idSalaire',
			'dateMedicaux',
			'fraisMedicaux',
			'remarqueMotifMedical'
		];
		let valueToShow;
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				if (field === 'dateMedicaux') valueToShow = Ext.util.Format.date(record.selection.get(field), 'd/m/Y');
				else valueToShow = record.selection.get(field);
				this.getView().down(`#${field}Id`).setValue(valueToShow);
			});
			this.getView().up().down('#fraisMedicauxFormPanelId').expand(true);
			Ext.ComponentQuery.query('#FMFormSaveBtn')[0].setDisabled(false);
		}
	},

	confirmDeleteFM(view, cell, recordIndex, cellIndex, e, record) {
		const FMParams = {
			idMedicaux: record.get('idMedicaux'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxFMDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelFM = {
						infoRequest: 3,
						data: FMParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/FraisMedicaux.php', modelDelFM);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxFMDelete').close();
						this.getView().down('#fraisMedicauxGridId').down('#gridpanel').getStore().load();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxFMDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	async onFMEmployeListClick() {
		const contratGridMin = this.getView().down('#contratGridMinId');
		const s = contratGridMin.getSelectionModel().getSelection();
		const ids = [];
		Ext.each(s, (record) => {
			ids.push(record.get('idContrat'));
		});
		const FMgrid = this.getView().down('#fraisMedicauxGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 2,
				idContrat: ids[0],
			},
		};
		
		// Récupère tous les frais médicaux de l'employé
		const inputFMData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.FraisMedicaux', extraParameters);
		
		FMgrid.setStore(inputFMData);

		this.getView().up().down('#fraisMedicauxGridPanelId').expand(true);
	},

	async onFMAllListClick() {
		const FMgrid = this.getView().down('#fraisMedicauxGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère tous les frais médicaux
		const inputFMData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.FraisMedicaux', extraParameters);
		
		FMgrid.setStore(inputFMData);

		this.getView().up().down('#fraisMedicauxGridPanelId').expand(true);
	},

	async onFMMoisListClick() {
		const moisSalaire = this.getView().down('#idSalaireId');
		const FMgrid = this.getView().down('#fraisMedicauxGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 3,
				moisSalaire: moisSalaire.getValue(),
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère tous les frais médicaux par mois
		const inputFMData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Conge', extraParameters);
		
		FMgrid.setStore(inputFMData);

		this.getView().up().down('#fraisMedicauxGridPanelId').expand(true);
	},

	onContratDRClick(view, record, rowIndex) {
		const allFields = [
			'idContrat',
		];
		if (!Ext.isEmpty(record)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.get(field));
			});
		}
	},

	onDRFormSaveClick() {
		const contratGridMin = this.getView().down('#contratGridMinId');
		contratGridMin.view.saveScrollState();

		const form = this.getView().down('#DRFormId');
		const DRgrid = this.getView().down('#diversRetenusGridId').down('#gridpanel');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/DiversRetenus.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			DRgrid.getStore().reload();
		} else if (Ext.typeOf(serverResponse) === 'string') {
			console.error(Ext.decode(serverResponse).error);
			Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
		}

		form.reset();
		contratGridMin.view.restoreScrollState();
	},

	onRowDRClick(record) {
		const allFields = [
			'idDiversRetenus',
			'idContrat',
			'idSalaire',
			'montantDiversRetenus',
			'dateDiversRetenus',
			'remarqueDiversRetenus',
		];
		let valueToShow;
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				if (field === 'dateDiversRetenus') valueToShow = Ext.util.Format.date(record.selection.get(field), 'd/m/Y');
				else valueToShow = record.selection.get(field);
				this.getView().down(`#${field}Id`).setValue(valueToShow);
			});
			this.getView().up().down('#diversRetenusFormPanelId').expand(true);
			Ext.ComponentQuery.query('#DRFormSaveBtn')[0].setDisabled(false);
		}
	},

	confirmDeleteDR(view, cell, recordIndex, cellIndex, e, record) {
		const DRParams = {
			idDiversRetenus: record.get('idDiversRetenus'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxDRDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelDR = {
						infoRequest: 3,
						data: DRParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/DiversRetenus.php', modelDelDR);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxDRDelete').close();
						this.getView().down('#diversRetenusGridId').down('#gridpanel').getStore().load();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxDRDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	async onDREmployeListClick() {
		const contratGridMin = this.getView().down('#contratGridMinId');
		const s = contratGridMin.getSelectionModel().getSelection();
		const ids = [];
		Ext.each(s, (record) => {
			ids.push(record.get('idContrat'));
		});
		const DRgrid = this.getView().down('#diversRetenusGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 2,
				idContrat: ids[0],
			},
		};
		
		// Récupère tous les divers retenus de l'employé
		const inputDRData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.DiversRetenus', extraParameters);
		
		DRgrid.setStore(inputDRData);

		this.getView().up().down('#diversRetenusGridPanelId').expand(true);
	},

	async onDRAllListClick() {
		const DRgrid = this.getView().down('#diversRetenusGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère tous les divers retenus
		const inputDRData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.DiversRetenus', extraParameters);
		
		DRgrid.setStore(inputDRData);

		this.getView().up().down('#diversRetenusGridPanelId').expand(true);
	},

	async onDRMoisListClick() {
		const moisSalaire = this.getView().down('#idSalaireId');
		const DRgrid = this.getView().down('#diversRetenusGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 3,
				moisSalaire: moisSalaire.getValue(),
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère tous les divers retenus par mois
		const inputDRData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.DiversRetenus', extraParameters);
		
		DRgrid.setStore(inputDRData);

		this.getView().up().down('#diversRetenusGridPanelId').expand(true);
	},

	onContratDepasseClick(view, record, rowIndex) {
		const allFields = [
			'idContrat',
		];
		if (!Ext.isEmpty(record)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.get(field));
			});
		}
	},
	
	onDPSFormSaveClick() {
		const contratGridMin = this.getView().down('#contratGridMinId');
		contratGridMin.view.saveScrollState();

		const form = this.getView().down('#DPSFormId');
		const DPSgrid = this.getView().down('#depassementGridId').down('#gridpanel');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/Depassement.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			DPSgrid.getStore().reload();
		} else if (Ext.typeOf(serverResponse) === 'string') {
			console.error(Ext.decode(serverResponse).error);
			Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
		}

		form.reset();
		contratGridMin.view.restoreScrollState();
	},
	
	async onDPSEmployeListClick() {
		const contratGridMin = this.getView().down('#contratGridMinId');
		const s = contratGridMin.getSelectionModel().getSelection();
		const ids = [];
		Ext.each(s, (record) => {
			ids.push(record.get('idContrat'));
		});
		const DPSgrid = this.getView().down('#depassementGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 2,
				idContrat: ids[0],
			},
		};
		
		// Récupère tous les dépassements téléphoniques retenus de l'employé
		const inputDPSData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Depassement', extraParameters);
		
		DPSgrid.setStore(inputDPSData);

		this.getView().up().down('#depassementGridPanelId').expand(true);
	},
	
	async onDPSAllListClick() {
		const DPSgrid = this.getView().down('#depassementGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère tous les dépassements téléphoniques
		const inputDPSData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Depassement', extraParameters);
		
		DPSgrid.setStore(inputDPSData);

		this.getView().up().down('#depassementGridPanelId').expand(true);
	},
	
	async onDPSMoisListClick() {
		const moisSalaire = this.getView().down('#idSalaireId');
		const DPSgrid = this.getView().down('#depassementGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 3,
				moisSalaire: moisSalaire.getValue(),
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère tous les divers retenus par mois
		const inputDPSData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Depassement', extraParameters);
		
		DPSgrid.setStore(inputDPSData);

		this.getView().up().down('#depassementGridPanelId').expand(true);
	},
	
	confirmDeleteDPS(view, cell, recordIndex, cellIndex, e, record) {
		const DPSParams = {
			idAppel: record.get('idAppel'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxDPSDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelDPS = {
						infoRequest: 3,
						data: DPSParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/Depassement.php', modelDelDPS);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxDPSDelete').close();
						this.getView().down('#depassementGridId').down('#gridpanel').getStore().load();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxDPSDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},
	
	onRowDPSClick(record) {
		const allFields = [
			'idAppel',
			'idContrat',
			'idSalaire',
			'depassement',
			'remarqueDepassement',
		];
		let valueToShow;
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				valueToShow = record.selection.get(field);
				this.getView().down(`#${field}Id`).setValue(valueToShow);
			});
			this.getView().up().down('#depassementFormPanelId').expand(true);
			Ext.ComponentQuery.query('#DPSFormSaveBtn')[0].setDisabled(false);
		}
	},

	async onContratAvanceClick(view, record, rowIndex) {
		const idContratValue = record.get('idContrat');

		// 1. Si l'employé n'a pas d'avance en cours
		const extraParamsAvanceEnCours = {
			extraParams: {
				infoRequest: 3,
				idContrat: idContratValue,
			},
		};
		
		const avanceEnCoursData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Avance', extraParamsAvanceEnCours);
		
		view.saveScrollState();

		if (Ext.isEmpty(avanceEnCoursData.data.items)) { // 1.Pas d'avance en cours, continuez les autres conditions
			const form = this.getView().down('#AvFormId');
			const dateReceptionField = form.down('#dateReceptionId');		
			// Calculer et récupérer le salaire Net de l'employé, par défaut pour le mois en cours
			const dateAvance = Ext.isEmpty(dateReceptionField.getValue()) ? new Date() : dateReceptionField.getValue();
			const month = dateAvance.getUTCMonth() + 1; //months from 1-12
			const year = dateAvance.getUTCFullYear();

			const salaireNetField = Ext.ComponentQuery.query('#salaireNetId')[0];
			const idContratField = Ext.ComponentQuery.query('#idContratId')[0];
			const maxAutoriseField = Ext.ComponentQuery.query('#maxAutoriseId')[0];
			const maxDeductionPossibleField = Ext.ComponentQuery.query('#maxDeductionPossibleId')[0];

			const moisSalariauxStore = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Salaire', {});
			const dateDemandeFilter = moisSalariauxStore.findBy(function(record, id) {
				if(record.get('moisSalaire') === month.toString() && record.get('anneeSalaire') === year.toString()) {
					return true;
				}
			});


			if (dateDemandeFilter > 0) { // La date de la demande appartient à l'un des mois de salaire (cf. onglet "Salaire")
				const extraParameters = {
					extraParams: {
						moisSalarial: year + '-' + month,
						infoRequest: 2,
						idContrat: idContratValue,
						action: 1,
					},
				};
	
				const salaireData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.SalaireDetails', extraParameters);
	
				const decallageAvances = Ext.isEmpty(salaireData.data.items) ? 0 : salaireData.data.items[0].get('decallageAvance');
	
				const extraParamsAvanceDecallage = {
					extraParams: {
						idContrat: idContratValue,
					},
				};
	
				const checkDerniereAvance = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.AvanceDecallage', extraParamsAvanceDecallage);
	
				const callBack = () => {
					const SalaireNet = Ext.isEmpty(salaireData.data.items) ? 0 : salaireData.data.items[0].get('SalaireNet'); // salaireBrutInclInflation
					
					salaireNetField.setValue(Gprh.ades.util.Globals.formatRenderNumber(SalaireNet));
					idContratField.setValue(idContratValue);
					maxAutoriseField.setValue(Gprh.ades.util.Globals.formatRenderNumber(SalaireNet * 2)); // Max = 200% de Salaire Brut
					maxDeductionPossibleField.setValue(Gprh.ades.util.Globals.formatRenderNumber(SalaireNet * 2 * 30 / 100)); // Max = 200% de Salaire Net
					view.restoreScrollState();
				};
	
				// 2. Vérifier si l'intervalle entre deux avances est respectée
				if (Ext.isEmpty(checkDerniereAvance.data.items)) { // Aucune avance auparavant, donc OK pour une nouvelle demande
					callBack();
				} else { // Il y avait des avances auparavant
					const decallageDerniereAvance = new Date(checkDerniereAvance.data.items[0].get('dateFinRemboursement'));
					const dernierInterval = Ext.Date.add(decallageDerniereAvance, Ext.Date.MONTH, decallageAvances);
					if (dernierInterval < dateAvance) {// Si date fin de remboursement + décallage attendue (ex. 3mois) < date de la demande, OK
						callBack();
					} else {// Si non l'intervalle n'est pas respecté, NOK
						form.reset();
						Gprh.ades.util.Helper.showError(`L\'intervalle entre 2 avances est de ${decallageAvances} mois. L\'employé ne peut pas encore faire cette demande`);
						view.restoreScrollState();
					}
				}
			} else { // La date n'appartient pas aux mois de salaire, NOK
				form.reset();
				Gprh.ades.util.Helper.showError('Sélectionnez une date qui appartient à l\'un de vos mois de salaire (cf. Onglet "Salaire").');
				view.restoreScrollState();
			}


		} else { // Une avance en cours, NOK
			// this.cancelAction({params: 'AvFormId'});
			Gprh.ades.util.Helper.showError('Une avance est encore en cours de régulation. L\'employé ne peut plus faire cette requête.');
			view.restoreScrollState();
		}
	},

	setDetailsRemboursement(e, newValue, oldValue, eOpts) {
		this.detailsChange(this);
	},

	detailsChange(obj) {
		const form = obj.getView().down('#AvFormId');
		const montantAvanceField = form.down('#montantAvanceId');
		const remboursementMoisField = form.down('#deductionMensuelleId');
		const trancheField = form.down('#nombreTrancheId');
		const debutRemboursementField = form.down('#dateDebutRemboursementId');
		const finRemboursementField = form.down('#dateFinRemboursementId');
		const dateDemandeField = form.down('#dateReceptionId');

		// Values
		const trancheValue = trancheField.getValue() - 1;
		const dateDemandeValue = Ext.isEmpty(dateDemandeField.getValue()) ? new Date() : dateDemandeField.getValue();
		const debutRemboursementValue = Ext.isEmpty(debutRemboursementField.getValue()) ? dateDemandeValue : debutRemboursementField.getValue();
		const finRemboursementValue = Ext.Date.add(debutRemboursementValue, Ext.Date.MONTH, trancheValue);

		// MAJ des fields
		dateDemandeField.setValue(Ext.util.Format.date(dateDemandeValue, 'd/m/Y'));
		remboursementMoisField.setValue(Gprh.ades.util.Globals.formatRenderNumber(parseFloat(montantAvanceField.getValue().replace(/\s/gi, '')) / trancheField.getValue()));
		debutRemboursementField.setValue(Ext.util.Format.date(debutRemboursementValue, 'd/m/Y'));
		finRemboursementField.setValue(Ext.util.Format.date(finRemboursementValue, 'd/m/Y'));
	},

	detailsRemboursement2(event) {
		const newValue = parseFloat(event.getValue().replace(/\s/gi, ''));
		const form = this.getView().down('#AvFormId');
		const montantAvanceField = form.down('#montantAvanceId');
		const remboursementMoisField = form.down('#deductionMensuelleId');
		const nombreTrancheField = form.down('#nombreTrancheId');		
		
		const debutRemboursementField = form.down('#dateDebutRemboursementId');
		const finRemboursementField = form.down('#dateFinRemboursementId');
		const dateDemandeField = form.down('#dateReceptionId');

		// Values
		const trancheValue = parseFloat(montantAvanceField.getValue().replace(/\s/gi, '')) / newValue;
		const dateDemandeValue = Ext.isEmpty(dateDemandeField.getValue()) ? new Date() : dateDemandeField.getValue();
		const debutRemboursementValue = Ext.isEmpty(debutRemboursementField.getValue()) ? dateDemandeValue : debutRemboursementField.getValue();
		const finRemboursementValue = Ext.Date.add(debutRemboursementValue, Ext.Date.MONTH, trancheValue - 1);

		// MAJ des fields
		remboursementMoisField.setValue(Gprh.ades.util.Globals.formatRenderNumber(newValue));
		dateDemandeField.setValue(Ext.util.Format.date(dateDemandeValue, 'd/m/Y'));
		nombreTrancheField.setValue(trancheValue);
		debutRemboursementField.setValue(Ext.util.Format.date(debutRemboursementValue, 'd/m/Y'));
		finRemboursementField.setValue(Ext.util.Format.date(finRemboursementValue, 'd/m/Y'));
	},

	onRowAvanceClick(record) {
		const allFields = [
			'idAvance',
			'idContrat',
			'maxAutorise',
			'maxDeductionPossible',
			'montantAvance',
			'dateReception',
			'dateDebutRemboursement',
			'dateFinRemboursement',
			'nombreTranche',
			'deductionMensuelle',
			'raison',
			'enCours',
		];
		let valueToShow;
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				if (field === 'dateReception' || field === 'dateDebutRemboursement' || field === 'dateFinRemboursement') valueToShow = Ext.util.Format.date(record.selection.get(field), 'd/m/Y');
				else valueToShow = record.selection.get(field);
				this.getView().down(`#${field}Id`).setValue(valueToShow);
			});
			this.getView().up().down('#avanceFormPanelId').expand(true);
			Ext.ComponentQuery.query('#AvFormSaveBtn')[0].setDisabled(false);
		}
	},

	confirmDeleteAv(view, cell, recordIndex, cellIndex, e, record) {
		const AVParams = {
			idAvance: record.get('idAvance'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxAVDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelAV = {
						infoRequest: 3,
						data: AVParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/Avance.php', modelDelAV);
					const outputAvanceData = saveOrUpdate.request.request.result.responseText;
					if (outputAvanceData > 0) {
						btn.up('#messageBoxAVDelete').close();
						this.getView().down('#avanceGridId').down('#gridpanel').getStore().load();
					} else if (Ext.typeOf(outputAvanceData) === 'string') {
						btn.up('#messageBoxAVDelete').close();
						console.error(Ext.decode(outputAvanceData).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxAVDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	onAvFormSaveClick() {
		const form = this.getView().down('#AvFormId');
		const Avgrid = this.getView().down('#avanceGridId').down('#gridpanel');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/Avance.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			Avgrid.getStore().reload();
		} else if (Ext.typeOf(serverResponse) === 'string') {
			console.error(Ext.decode(serverResponse).error);
			Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorDelMsg);
		}

		form.reset();
	},

	onRowAvanceDblClick() { // TODO
		// Details des remboursements
	},

	async onCloseSalaireClick() {
		// Sauvegarde les données, parametres de calcules, les formules dans une table
		// Met à jour le solde de congé
		// Met à jour les déductions avances
		// Met à jour la catégorie professionnelle
		// Change le statut d'un mois de salaire en "Clôturé"
		const grid = this.getView().down('#salaireGridId');
		const selection = grid.getSelection();

		const extraParameters_btn1 = {
			extraParams: {
				moisSalarial: selection[0].get('anneeSalaire')+'-'+selection[0].get('moisSalaire'),
				infoRequest: 1, // tous les employés d'ADES
				action: 3, // action ==  fait la sauvegarde des données
			},
		};

		const messageBox1 = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxBackUp',
			buttons: [{
				text: 'Oui',
				handler: async (btn) => {
					btn.up('#messageBoxBackUp').close();
					Ext.getBody().mask("Patientez, sauvegarde de la base de données en cours ...");
					const outputSalairesData1 = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.SalaireDetails', extraParameters_btn1);
					Ext.getBody().unmask();
					
					if (outputSalairesData1.getAt(0).get('response') > 0) {
						const messageBox2 = Ext.create('Ext.window.MessageBox', {
							itemId: 'messageBoxCloture',
							buttons: [{
								text: 'Oui',
								handler: async(btn) => {
									btn.up('#messageBoxCloture').close();
									const extraParameters = {
										extraParams: {
											moisSalarial: selection[0].get('anneeSalaire')+'-'+selection[0].get('moisSalaire'),
											infoRequest: 1, // tous les employés d'ADES
											action: 2, // action ==  fait la sauvegarde des données
										},
									};
							
									Ext.getBody().mask("Veuillez patientez ...");
									const outputSalairesData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.SalaireDetails', extraParameters);
									Ext.getBody().unmask();
									
									if (outputSalairesData.getAt(0).get('response') > 0) {
										Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
										grid.getStore().load();
							
										const store = grid.getStore();
										store.on('load', function() {
											var desired_record = store.findRecord('idSalaire', selection[0].get('idSalaire'));
											grid.getSelectionModel().select(desired_record);
											});
									} else if (Ext.typeOf(outputSalairesData.getAt(0).get('response')) === 'string') {
										console.error(Ext.decode(outputSalairesData).error);
										Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
									}
								},
							}, {
								text: 'Non',
								handler: (btn) => {
									btn.up('#messageBoxCloture').close();
								},
							}],
						});

						messageBox2.show({
							title: 'Confirmer la clôture du salaire',
							msg: 'La clôture du salaire est irreversible. Voulez-vous vraiment continuer et clôturer le salaire de ce mois?',
							icon: Ext.MessageBox.WARNING,
						});
					} else if (Ext.typeOf(outputSalairesData.getAt(0).get('response')) === 'string') {
						console.error(Ext.decode(outputSalairesData).error);
						Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxBackUp').close();
				},
			}],
		});
		
		messageBox1.show({
			title: 'Sauvegarder la base de données avant la clôture de salaire.',
			msg: 'Cette opération va sauvegarder votre base de données avant la clôture du salaire. Voulez-vous vraiment le confirmer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	async onSalaireRowClick() {
		const grid = this.getView().down('#salaireGridId');
		const selection = grid.getSelection();
		const gridDetails = this.getView().up('#salaireMainContainerId').down('#salaireDetailsGridId');
		
		const extraParameters = {
			extraParams: {
				infoRequest: 3,
				idSalaire: selection[0].get('idSalaire'),
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
			url: 'server-scripts/listes/HistoriqueSalaire.php',
		};
		// Va récupérer les historiques, puis afficher dans le grid SalaireDetailsGrid le store ainsi obtenu
		gridDetails.mask("Veuillez patientez ...");
		const inputSalairesData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.SalaireDetails', extraParameters);
		gridDetails.unmask();
		gridDetails.setStore(inputSalairesData);
	},

	async onFDPpdfClick() {
		const grid = Ext.isEmpty(this.getView().down('#salaireGridId')) ? this.getView().down('#paieGridId') : this.getView().down('#salaireGridId');
		const selection = grid.getSelection();
		let target;

		/*
		 * si l'action a été lancée depuis l'onglet Etats et Fiches de paie, le mask et le téléchargement
		 * se fait dans le iframe
		 * si l'action a été lancée depuis l'onglet Salaire, le téléchargement se fait dans un new Tab
		 */

		if (Ext.isEmpty(this.getView().down('#salaireGridId')) && (typeof InstallTrigger !== 'undefined')) { // 
			target = 'pdfViewer';
			Ext.getCmp('panelForPdfViewerId').body.mask('Téléchargement...');
		} else {
			target = '_new';
		}

		// Va exporter les données du salaire clôturé dans les Fiches de paies en pdf 
		Gprh.ades.util.Downloader.getPDF({
			url: 'server-scripts/print/FicheDePaie.php',
			params: {
				infoRequest: 3,
				idSalaire: selection[0].get('idSalaire'),
				mois: selection[0].get('moisSeulement') + '-' +  selection[0].get('anneeSalaire'),
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			}
		}, target);
	},

	async onPrintXlsxSalaireClick() {
		const grid = this.getView().down('#salaireGridId');
		const selection = grid.getSelection();

		Ext.getBody().mask('Téléchargement...');
		
		// Va exporter les données du salaire clôturé sur Excel (2007 .xlsx) 
		Gprh.ades.util.Downloader.get({
			url: 'server-scripts/export/Salaire.php',
			params: {
				infoRequest: 3,
				idSalaire: selection[0].get('idSalaire'),
				mois: selection[0].get('moisSeulement') + '-' +  selection[0].get('anneeSalaire'),
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			}
		});
	},

	async onPrintXlsxChangeCategorieClick() {
		const grid = this.getView().down('#salaireGridId');
		const selection = grid.getSelection();

		Ext.getBody().mask('Téléchargement...');
		
		// Va exporter les données de la table historique_categorie sur Excel (2007 .xlsx) 
		Gprh.ades.util.Downloader.get({
			url: 'server-scripts/export/ChangeCategorie.php',
			params: {
				infoRequest: 3,
				idSalaire: selection[0].get('idSalaire'),
				mois: selection[0].get('moisSeulement') + '-' +  selection[0].get('anneeSalaire'),
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			}
		});
	},

	async onPrintXlsxEtatPaieClick() {
		const grid = Ext.isEmpty(this.getView().down('#salaireGridId')) ? this.getView().down('#paieGridId') : this.getView().down('#salaireGridId');
		const selection = grid.getSelection();

		Ext.getBody().mask('Téléchargement...');

		// Va exporter les données du salaire clôturé sur Excel (2007 .xlsx) en format Etat de paie
		Gprh.ades.util.Downloader.get({
			url: 'server-scripts/export/EtatDePaie.php',
			params: {
				infoRequest: 3,
				idSalaire: selection[0].get('idSalaire'),
				mois: selection[0].get('moisSeulement') + '-' +  selection[0].get('anneeSalaire'),
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			}
		});
	},

	updateDateDebutRappel(combo, record, eOpts) {
		const form = this.getView().down('#RAPFormId');
		const rec = record;
		const mois = (rec.get('moisSalaire').length < 2) ? `0${rec.get('moisSalaire')}` : rec.get('moisSalaire');
		const dateDebutRappelValue = `25/${mois}/${rec.get('anneeSalaire')}`;
		
		// MAJ des fields
		const debutRappelField = form.down('#dateDebutRappelId');
		debutRappelField.setValue(dateDebutRappelValue);
		this.setDetailsRappel();
	},

	setDetailsRappel() {
		const form = this.getView().down('#RAPFormId');
		const montantRappelField = form.down('#montantRappelId');
		const nbMoisField = form.down('#nombreMoisId');
		const debutRappelField = form.down('#dateDebutRappelId');
		const finRappelField = form.down('#dateFinRappelId');

		// Values
		const nbMoisValue = nbMoisField.getValue() - 1;
		// console.log(debutRappelField);
		const debutRappelValue = debutRappelField.getValue();
		const finRappelValue = Ext.Date.add(debutRappelValue, Ext.Date.MONTH, nbMoisValue);

		// MAJ des fields
		finRappelField.setValue(Ext.util.Format.date(finRappelValue, 'd/m/Y'));
	},

	onContratRAPClick(view, record, rowIndex) {
		const allFields = [
			'idContrat',
		];
		if (!Ext.isEmpty(record)) {
			Ext.each(allFields, (field) => {
				this.getView().down(`#${field}Id`).setValue(record.get(field));
			});
		}
	},

	onRAPFormSaveClick() {
		const contratGridMin = Ext.ComponentQuery.query('#contratGridMinId')[0];
		contratGridMin.view.saveScrollState();

		const form = this.getView().down('#RAPFormId');
		const RAPgrid = this.getView().down('#rappelGridId').down('#gridpanel');

		const model = {
			infoRequest: 2,
			data: form.getValues(),
		};
		const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/Rappel.php', model);
		const serverResponse = JSON.parse(saveOrUpdate.request.responseText);
		
		if (serverResponse > 0) {
			Gprh.ades.util.Helper.showSuccess(Gprh.ades.util.Globals.successMsg);
			RAPgrid.getStore().reload();
		} else if (Ext.typeOf(serverResponse) === 'string') {
			console.error(Ext.decode(serverResponse).error);
			Gprh.ades.util.Helper.showError(Gprh.ades.util.Globals.errorMsg);
		}

		form.reset();
		contratGridMin.view.restoreScrollState();
	
	},

	onRowRAPClick(record) {
		Ext.getCmp('nombreMoisid').suspendEvent('change');
		console.log(Ext.getCmp('nombreMoisid'));
		const allFields = [
			'idRappel',
			'idContrat',
			'idSalaire',
			'montantRappel',
			'nombreMois',
			'dateDebutRappel',
			'dateFinRappel',
			'remarqueRappel',
		];
		let valueToShow;
		if (!Ext.isEmpty(record.selection)) {
			Ext.each(allFields, (field) => {
				if (field === 'dateDebutRappel' || field === 'dateFinRappel') {
					valueToShow = Ext.util.Format.date(record.selection.get(field), 'd/m/Y');
				} else {
					valueToShow = record.selection.get(field);
				}
				this.getView().down(`#${field}Id`).setValue(valueToShow);
			});
			this.getView().up().down('#rappelFormPanelId').expand(true);
			Ext.ComponentQuery.query('#RAPFormSaveBtn')[0].setDisabled(false);
		}

		Ext.getCmp('nombreMoisid').resumeEvent('change');
	},

	confirmDeleteRappel(view, cell, recordIndex, cellIndex, e, record) {
		const RAPParams = {
			idRappel: record.get('idRappel'),
		};

		const messageBox = Ext.create('Ext.window.MessageBox', {
			itemId: 'messageBoxRAPDelete',
			buttons: [{
				text: 'Oui',
				handler: (btn) => {
					const modelDelRAP = {
						infoRequest: 3,
						data: RAPParams,
					};
					const saveOrUpdate = this.requestSaveOrUpdate('server-scripts/salaire/Rappel.php', modelDelRAP);
					
					if (saveOrUpdate.request.request.result.responseText > 0) {
						btn.up('#messageBoxRAPDelete').close();
						this.getView().down('#rappelGridId').down('#gridpanel').getStore().load();
					}
				},
			}, {
				text: 'Non',
				handler: (btn) => {
					btn.up('#messageBoxRAPDelete').close();
				},
			}],
		});
		
		messageBox.show({
			title: 'Confirmer la suppression',
			msg: 'Cet enregistrement pourrait être lié à d\'autres données. Voulez-vous vraiment le supprimer?',
			icon: Ext.MessageBox.WARNING,
		});
	},

	async onRAPEmployeListClick() {
		const contratGridMin = this.getView().down('#contratGridMinId');
		const s = contratGridMin.getSelectionModel().getSelection();
		const ids = [];
		Ext.each(s, (record) => {
			ids.push(record.get('idContrat'));
		});
		const RAPgrid = this.getView().down('#rappelGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 2,
				idContrat: ids[0],
			},
		};
		
		// Récupère tous les divers retenus de l'employé
		const inputRAPData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Rappel', extraParameters);
		
		RAPgrid.setStore(inputRAPData);

		this.getView().up().down('#rappelGridPanelId').expand(true);
	},

	async onRAPAllListClick() {
		const RAPgrid = this.getView().down('#rappelGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère tous les divers retenus
		const inputRAPData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Rappel', extraParameters);
		
		RAPgrid.setStore(inputRAPData);

		this.getView().up().down('#rappelGridPanelId').expand(true);
	},

	async onRAPMoisListClick() {
		const moisSalaire = this.getView().down('#idSalaireId');
		const RAPgrid = this.getView().down('#rappelGridId').down('#gridpanel');
		const extraParameters = {
			extraParams: {
				infoRequest: 3,
				moisSalaire: moisSalaire.getValue(),
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		
		// Récupère tous les divers retenus par mois
		const inputRAPData = await Gprh.ades.util.Globals.requestStoreSettings('Gprh.ades.store.Rappel', extraParameters);
		
		RAPgrid.setStore(inputRAPData);

		this.getView().up().down('#rappelGridPanelId').expand(true);
	},

	async onDiffSalaireXlsxClick() {
		const grid = this.getView().down('#salaireGridId');
		const selection = grid.getSelection();

		Ext.getBody().mask('Téléchargement...');
		
		// Va exporter les données du salaire clôturé sur Excel (2007 .xlsx) 
		Gprh.ades.util.Downloader.get({
			url: 'server-scripts/formule/DetailsFormule.php',
			params: {
				moisSalarial: selection[0].get('anneeSalaire')+'-'+selection[0].get('moisSalaire'),
				infoRequest: 1, // tous les employés d'ADES
				action: 4, // action == 4 exporte les différences du mois dernier et celles d'en cours
				start: 0,
				page: 1,
				limit: 25,
				mois: selection[0].get('moisSeulement'),
			}
		});
	},
});
