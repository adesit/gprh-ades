Ext.define('Gprh.ades.view.salaire.SalaireMainContainer', {
	extend: 'Ext.panel.Panel',

	xtype: 'salaireMainContainer',

	requires: [
		'Gprh.ades.view.salaire.SalaireController',
		'Gprh.ades.view.salaire.SalaireViewModel',
		'Gprh.ades.view.salaire.SalaireGrid',
		'Gprh.ades.view.salaire.SalaireDetailsGrid',
	],
	
	viewModel: {
		type: 'salaireViewModel',
	},

	controller: 'salaireController',

	itemId: 'salaireMainContainerId',

	layout: {
		type: 'hbox',
		pack: 'start',
		align: 'stretch',
	},

	bodyPadding: 1,

	items: [
		{
			title: 'Les mois de salaire',
			flex: 1,
			margin: '0 1 0 0',
			itemId: 'salaireGridPanelId',
			collapsible: true,
			frame: true,
			collapseDirection: 'left',
			items: [
				{ xtype: 'salaireGrid' },
			],
		},
		{
			title: 'Détails',
			flex: 2,
			margin: '0 1 0 0',
			frame: true,
			itemId: 'contratGridMinPanelId',
			items: [
				{ 
					xtype: 'salaireDetailsGrid',
					hideAllColumns: false,
				},
			],
		},
	],
		
});
