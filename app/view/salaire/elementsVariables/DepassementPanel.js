Ext.define('Gprh.ades.view.salaire.elementsVariables.DepassementPanel', {
	extend: 'Ext.panel.Panel',

	xtype: 'depassementPanel',

	requires: [
		'Ext.layout.container.HBox',
		'Gprh.ades.view.salaire.SalaireController',
		'Gprh.ades.view.salaire.SalaireViewModel',
		'Gprh.ades.view.salaire.ContratGridMin',
		'Gprh.ades.view.salaire.elementsVariables.DepassementForm',
		'Gprh.ades.view.salaire.elementsVariables.DepassementGrid',
	],
	
	viewModel: {
		type: 'salaireViewModel',
	},

	controller: 'salaireController',
	
	itemId: 'depassementPanelId',

	layout: {
		type: 'hbox',
		pack: 'start',
		align: 'stretch',
	},

	bodyPadding: 1,

	defaults: {
		frame: false,
		bodyPadding: 1,
	},

	items: [
		{
			title: 'Liste de contrats',
			flex: 2,
			collapsible: true,
			collapseDirection: 'left',
			collapseMode: 'header',
			margin: '0 1 0 0',
			itemId: 'contratGridMinPanelId',
			items: [
				{
					xtype: 'contratGridMin',
					itemId: 'contratGridMinId',
					listeners: {
						select: 'onContratDepasseClick',
					},
				},
			],
		},
		{
			title: 'Formulaire d\'enregistrement',
			flex: 1,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'depassementFormPanelId',
			items: [
				{ xtype: 'depassementForm' },
			],
		},
		{
			title: 'Liste des dépassements téléphoniques',
			flex: 1,
			collapsed: true,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'depassementGridPanelId',
			items: [
				{
					xtype: 'depassementGrid',
					itemId: 'depassementGridId',
				},
			],
			listeners: {
				expand: (self) => {
					self.up().down('#contratGridMinPanelId').collapse(true);
					self.up().down('#depassementFormPanelId').collapse(true);
				},
				collapse: (self) => {
					self.up().down('#contratGridMinPanelId').expand(true);
					self.up().down('#depassementFormPanelId').expand(true);
				},
			},
		},
	],

});
