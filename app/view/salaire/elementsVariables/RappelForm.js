Ext.define('Gprh.ades.view.salaire.elementsVariables.RappelForm', {
	extend: 'Ext.form.Panel',

	xtype: 'rappelForm',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.util.VTypes',
	],

	colorScheme: 'soft-green',
	bodyPadding: 5,

	initComponent() {
		const form = {
			xtype: 'form',
			itemId: 'RAPFormId',
			layout: {
				type: 'vbox',
				align: 'stretch',
			},
			defaults: {
				labelAlign: 'top',
				labelSeparator: ':',
				submitEmptyText: false,
			},
			listeners: {				
				beforeadd: () => {
					const me = this;
					const scroller = me.getScrollable();
					if (scroller) {
						me.savedScrollPos = scroller.getPosition();
					}
				},
				afterLayout: () => {
					const grid = this.up('#rappelPanelId').down('#contratGridMinId');
					
					this.up('#rappelPanelId').down('#contratGridMinId').getView().restoreScrollState(grid.view.scrollState.top);
				},
			},
			items: [
				{
					layout: 'column',
					margin: '0 5 0 0',
					items: [
						{
							defaultType: 'textfield',
							defaults: {
								labelWidth: 150,
								labelAlign: 'top',
								labelSeparator: ':',
								submitEmptyText: false,
								width: 150,
							},
							columnWidth: '0.50',
							items: [
								{
									xtype: 'textfield',
									fieldLabel: 'ID_RAPPEL',
									name: 'idRappel',
									reference: 'idRappelRef',
									publishes: 'value',
									itemId: 'idRappelId',
									hidden: true,
								},
								{
									xtype: 'textfield',
									fieldLabel: 'ID_CONTRAT',
									name: 'idContrat',
									reference: 'idContratRef',
									publishes: 'value',
									itemId: 'idContratId',
									hidden: true,
									allowBlank: false,
								},
								{
									xtype: 'textfield',					
									fieldLabel: 'Montant (Ar)',
									name: 'montantRappel',
									reference: 'montantRappelRef',
									publishes: 'value',
									itemId: 'montantRappelId',
									listeners: {
										blur: self => Gprh.ades.util.Globals.formatNumber(self),
									},
									msgTarget: 'side',
									allowBlank: false,
								},
								{
									xtype: 'combobox',
									fieldLabel: 'Début du rappel',
									name: 'idSalaire',
									itemId: 'idSalaireId',
									reference: 'moisFieldReference',
									publishes: 'value',
									bind: {
										store: '{salaireResults}',
									},
									queryMode: 'local',
									displayField: 'mois',
									valueField: 'idSalaire',
									renderTo: Ext.getBody(),
									allowBlank: false,
									forceSelection: true,
									editable: false,
									tpl: Ext.create('Ext.XTemplate',
										'<ul class="x-list-plain"><tpl for=".">',
										'<tpl if="clotureSalaire != 1">',
										'<li role="option" class="x-boundlist-item">{mois}</li>',
										'<tpl else>',
										// @todo Use a dedicated class instead of style
										'<li role="option" class="x-boundlist-item" style="cursor: not-allowed;color: orange;">{mois}</li>',
										'</tpl>',
										'</tpl></ul>'),
									listeners: {
										// Evite la sélection des mois de salaire déjà clôturés
										beforeselect: (combo, record) => record.get('clotureSalaire') !== '1',
										select: 'updateDateDebutRappel',
									},
								},
								{
									xtype: 'numberfield',
									fieldLabel: 'Nombre de tranches',
									name: 'nombreMois',
									reference: 'nombreMoisRef',
									publishes: 'value',
									itemId: 'nombreMoisId',
									id: 'nombreMoisid',
									allowDecimal: false,
									allowBlank: false,
									minValue: 1,
									value: 1,
									msgTarget: 'side',
									listeners: {
										afterrender: () => {
											const els = Ext.select('div.x-form-trigger.x-form-trigger-default.x-form-trigger-spinner.x-form-trigger-spinner-default.x-unselectable', true);
											els.removeCls('x-form-trigger-default');
										},
										change: 'setDetailsRappel',
									},
								},								
								{
									xtype: 'datefield',
									name: 'dateFinRappel',
									itemId: 'dateFinRappelId',
									reference: 'dateFinRappelRef',
									publishes: 'value',
									fieldLabel: 'Fin de rappel',
									format: 'd/m/Y',
									submitFormat: 'Y-m-d',
									allowBlank: false,
								},
							],
						},
						{
							defaultType: 'textfield',
							defaults: {
								labelWidth: 170,
								labelAlign: 'top',
								labelSeparator: ':',
								submitEmptyText: false,
								width: 150,
							},
							columnWidth: '0.50',
							items: [
								{
									xtype: 'combobox',
									fieldLabel: 'Rappel sur',
									name: 'typeRappel',
									itemId: 'typeRappelId',
									reference: 'typeRappelRef',
									publishes: 'value',
									store: Ext.create('Ext.data.Store', {
										fields: ['typeRappel', 'nameTypeRappel'],
										data: [
											{ typeRappel: 1, nameTypeRappel: 'Salaire de base' },
											{ typeRappel: 2, nameTypeRappel: 'Salaire Net' },
										],
									}),
									queryMode: 'local',
									displayField: 'nameTypeRappel',
									valueField: 'typeRappel',
									renderTo: Ext.getBody(),
									allowBlank: false,
									forceSelection: true,
									editable: false,
									value: 1,
								},
								{
									xtype: 'datefield',
									name: 'dateDebutRappel',
									itemId: 'dateDebutRappelId',
									reference: 'dateDebutRappelRef',
									publishes: 'value',
									fieldLabel: 'Début de rappel',
									format: 'd/m/Y',
									submitFormat: 'Y-m-d',
									allowBlank: false,
									editable: false,
									hidden: true,
								},
							],
						},
					], 
				},
				{
					xtype: 'textfield',
					labelWidth: 150,
					labelAlign: 'top',
					labelSeparator: ':',
					width: 325,
					fieldLabel: 'Remarques ou motifs',
					name: 'remarqueRappel',
					itemId: 'remarqueRappelId',
					maxWidth: 300,
					allowBlank: false,
				},
			],
			
		};

		this.items = [
			{
				html: '<span class="x-fa fa-info-circle"></span> Sélectionnez une ligne dans la grille principale.</br>Le séparateur décimal à utiliser est le "."',
				itemId: 'helperId',
				baseCls: 'x-toast-info',
				anchor: '100%',
			},
			form,
			{
				xtype: 'toolbar',
				cls: 'wizard-form-break',
				defaults: {
					flex: 1,
				},
				width: 300,
				layout: 'hbox',
				items: [
					'->',
					{
						itemId: 'RAPFormSaveBtn',
						xtype: 'button',
						text: 'Enregistrer',
						ui: 'soft-green-small',
						margin: '10 10 0 0',
						formBind: true,
						listeners: {
							click: 'onRAPFormSaveClick',
						},
						hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
					},
					{
						itemId: 'RAPFormCancelBtn',
						xtype: 'button',
						text: 'Annuler',
						ui: 'soft-blue-small',
						margin: '10 0 0 0',
						listeners: {
							click: 'cancelAction',
						},
						params: 'RAPFormId',
					},
				],
			},
			{
				xtype: 'toolbar',
				cls: 'wizard-form-break',
				defaults: {
					flex: 1,
				},
				layout: 'hbox',
				items: [
					{
						itemId: 'RAPListBtn',
						xtype: 'button',
						text: 'Employé',
						tooltip: 'Liste des rappels par employé',
						ui: 'soft-purple',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onRAPEmployeListClick',
						},
						bind: {
							disabled: '{isEnabledEmploye}',
						},
					},
					{
						itemId: 'RAPAllListBtn',
						xtype: 'button',
						text: 'Tous',
						tooltip: 'Liste de tous les rappels',
						ui: 'soft-purple-small',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onRAPAllListClick',
						},
					},
					{
						itemId: 'RAPMoisListBtn',
						xtype: 'button',
						text: 'Mois',
						tooltip: 'Liste de tous les rappels d\'un mois',
						ui: 'soft-purple-small',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onRAPMoisListClick',
						},
						bind: {
							disabled: '{isEnabledMois}',
						},
					},
				],
			},
		];

		this.callParent();
	},
});
