Ext.define('Gprh.ades.view.salaire.elementsVariables.AvancePanel', {
	extend: 'Ext.panel.Panel',

	xtype: 'avancePanel',

	requires: [
		'Ext.layout.container.HBox',
		'Gprh.ades.view.salaire.SalaireController',
		'Gprh.ades.view.salaire.SalaireViewModel',
		'Gprh.ades.view.salaire.ContratGridMin',
		'Gprh.ades.view.salaire.elementsVariables.AvanceForm',
		'Gprh.ades.view.salaire.elementsVariables.AvanceGrid',
	],
	
	viewModel: {
		type: 'salaireViewModel',
	},

	controller: 'salaireController',
	
	itemId: 'avancePanelId',

	layout: {
		type: 'hbox',
		pack: 'start',
		align: 'stretch',
	},

	bodyPadding: 1,

	defaults: {
		frame: false,
		bodyPadding: 1,
	},

	items: [
		{
			title: 'Liste de contrats',
			flex: 1.80,
			collapsible: true,
			collapseDirection: 'left',
			collapseMode: 'header',
			margin: '0 1 0 0',
			itemId: 'contratGridMinPanelId',
			items: [
				{
					xtype: 'contratGridMin',
					itemId: 'contratGridMinId',
					mminWidth: 500,
					listeners: {
						rowclick: 'onContratAvanceClick',
					},
				},
			],
		},
		{
			title: 'Formulaire d\'enregistrement',
			flex: 1.20,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'avanceFormPanelId',
			items: [
				{ xtype: 'avanceForm' },
			],
		},
		{
			title: 'Liste des avances',
			flex: 1,
			collapsed: true,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'avanceGridPanelId',
			items: [
				{
					xtype: 'avanceGrid',
					itemId: 'avanceGridId',
				},
			],
			listeners: {
				expand: (self) => {
					self.up().down('#contratGridMinPanelId').collapse(true);
					self.up().down('#avanceFormPanelId').collapse(true);
				},
				collapse: (self) => {
					self.up().down('#contratGridMinPanelId').expand(true);
					self.up().down('#avanceFormPanelId').expand(true);
				},
			},
		},
	],

});
