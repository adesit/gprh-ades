Ext.define('Gprh.ades.view.salaire.elementsVariables.CongeGrid', {
	extend: 'Ext.panel.Panel',
	xtype: 'congeGrid',	
	
	initComponent() {
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		const Store = Ext.create('Gprh.ades.store.Conge');
		Ext.apply(Store.getProxy(), extraParameters);

		this.items = [
			{
				xtype: 'gridpanel',
				itemId: 'gridpanel',
				reference: 'congeGridRef',
				store: Store,
				viewConfig: {
					preserveScrollOnRefresh: true,
					stripeRows: true,
				},
				cls: 'user-grid',
				scrollable: true,
				selModel: 'rowmodel',
				listeners: {
					// rowclick: 'onRowCngClick',
				},
				height: Ext.Element.getViewportHeight() - 125,
				width: 'auto',
				columnLines: true,
				columns: {
					defaults: {
						align: 'left',
						width: 100,
					},
					items: [
						{
							xtype: 'gridcolumn',
							dataIndex: 'idConge',
							text: '#Congé',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'idContrat',
							text: '#Contrat',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'moisAnnee',
							text: 'Mois salaire',
							width: 120,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'matricule',
							text: 'Matricule',				
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nom',
							text: 'Noms et prénoms',
							width: 250,
							renderer(value, p, r) {
								return `${r.data.nom} ${r.data.prenom}`;
							},
							filter: {
								type: 'string',
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'natureConge',
							text: 'Type d\'conge',
							renderer: value => ((value === '1') ? 'Conge de maternité' : 'Congé payé'),
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'dureeCongePrise',
							text: 'Durée (jours)(décimal)',
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateDebutConge',
							text: 'Début de congé',
							format: 'd/m/Y',
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateFinConge',
							text: 'Fin de congé',
							format: 'd/m/Y',
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'remarqueConge',
							text: 'Motif ou remarque',
							width: 120,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'reliquat',
							text: 'Solde congé',
							width: 125,
							renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
							hidden: true,
						},
						{
							xtype: 'actioncolumn',
							cls: 'content-column',
							width: 80,
							text: 'Actions',
							items: [
								{
									iconCls: 'x-fa fa-trash soft-red-small',
									handler: 'confirmDeleteCng',
								},
							],					
						},
					],
				},
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
				],
				tbar: [],
			},
		];

		this.callParent();
	},
});
