Ext.define('Gprh.ades.view.salaire.elementsVariables.SalaireInterimPanel', {
	extend: 'Ext.panel.Panel',

	xtype: 'salaireInterimPanel',

	requires: [
		'Ext.layout.container.HBox',
		'Gprh.ades.view.salaire.SalaireController',
		'Gprh.ades.view.salaire.SalaireViewModel',
		'Gprh.ades.view.salaire.ContratGridMin',
		'Gprh.ades.view.salaire.elementsVariables.SalaireInterimForm',
	],
	
	viewModel: {
		type: 'salaireViewModel',
	},

	controller: 'salaireController',
	
	itemId: 'salaireInterimPanelId',

	layout: {
		type: 'hbox',
		pack: 'start',
		align: 'stretch',
	},

	bodyPadding: 1,

	defaults: {
		frame: false,
		bodyPadding: 1,
	},

	items: [
		{
			title: 'Contrats',
			flex: 2,
			margin: '0 1 0 0',
			itemId: 'contratGridMinPanelId',
			items: [
				{
					xtype: 'contratGridMin',
					itemId: 'contratGridMinId',
					listeners: {
						select: 'onContratInterimClick',
					},
				},
			],
		},
		{
			title: 'Durée de l\'intérim',
			flex: 1,
			collapsible: true,
			collapseDirection: 'right',
			itemId: 'salaireInterimFormPanelId',
			items: [
				{ xtype: 'salaireInterimForm' },
			],
		},
	],

});
