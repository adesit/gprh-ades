Ext.define('Gprh.ades.view.salaire.elementsVariables.FraisMedicauxGrid', {
	extend: 'Ext.panel.Panel',
	xtype: 'fraisMedicauxGrid',
	
	initComponent() {
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		const Store = Ext.create('Gprh.ades.store.FraisMedicaux');
		Ext.apply(Store.getProxy(), extraParameters);
		this.items = [
			{
				xtype: 'gridpanel',
				itemId: 'gridpanel',
				reference: 'fraisMedicauxGridRef',
				store: Store,
				viewConfig: {
					preserveScrollOnRefresh: true,
					stripeRows: true,
				},
				cls: 'user-grid',
				scrollable: true,
				selModel: 'rowmodel',
				listeners: {
					rowclick: 'onRowFMClick',
				},
				height: Ext.Element.getViewportHeight() - 125,
				width: 'auto',
				columnLines: true,
				columns: {
					defaults: {
						align: 'left',
						width: 100,
					},
					items: [
						{
							xtype: 'gridcolumn',
							dataIndex: 'idMedicaux',
							text: '#Médicaux',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'idContrat',
							text: '#Contrat',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'moisAnnee',
							text: 'Mois salaire',
							width: 150,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'matricule',
							text: 'Matricule',				
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nomPrenom',
							text: 'Noms et prénoms',
							minWidth: 200,
							filter: {
								type: 'string',
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'fraisMedicaux',
							text: 'Frais médicaux (Ar)',
							flex: 1,
							renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateMedicaux',
							text: 'Date de la facture',
							format: 'd/m/Y',
							flex: 1,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'remarqueMotifMedical',
							text: 'Motif ou remarque',
							width: 120,
						},
						{
							xtype: 'actioncolumn',
							cls: 'content-column',
							width: 80,
							text: 'Actions',
							items: [
								{
									iconCls: 'x-fa fa-trash soft-red-small',
									handler: 'confirmDeleteFM',
								},
							],					
						},
					],
				},
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
				],
				tbar: [],
			},
		];
		this.callParent();
	},
});
