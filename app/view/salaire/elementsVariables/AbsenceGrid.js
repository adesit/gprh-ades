Ext.define('Gprh.ades.view.salaire.elementsVariables.AbsenceGrid', {
	extend: 'Ext.panel.Panel',
	xtype: 'absenceGrid',
	
	initComponent() {
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		const Store = Ext.create('Gprh.ades.store.Absence');
		Ext.apply(Store.getProxy(), extraParameters);
		this.items = [
			{
				xtype: 'gridpanel',
				itemId: 'gridpanel',
				reference: 'absenceGridRef',
				store: Store,
				viewConfig: {
					preserveScrollOnRefresh: true,
					stripeRows: true,
				},
				cls: 'user-grid',
				scrollable: true,
				selModel: 'rowmodel',
				listeners: {
					rowclick: 'onRowAbsClick',
				},
				height: Ext.Element.getViewportHeight() - 125,
				columnLines: true,
				columns: {
					defaults: {
						align: 'left',
						width: 100,
					},
					items: [
						{
							xtype: 'gridcolumn',
							dataIndex: 'idAbsence',
							text: '#Abs',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'idContrat',
							text: '#Contrat',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'moisAnnee',
							text: 'Mois salaire',
							width: 150,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'matricule',
							text: 'Matricule',
							width: 100,				
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nom',
							text: 'Noms et prénoms',
							width: 300,
							renderer(value, p, r) {
								return `${r.data.nom} ${r.data.prenom}`;
							},
							filter: {
								type: 'string',
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'typeAbsence',
							text: 'Type d\'absence',
							renderer: value => this.typeAbsence(value),
							width: 200,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'dureeAbsence',
							text: 'Nombre d\'heures',
							width: 100,
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateHeureDebutHs',
							text: 'Début de permission',
							format: 'd/m/Y',
							width: 100,
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateHeureFinHs',
							text: 'Fin de permission',
							format: 'd/m/Y',
							width: 100,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'motifAbsence',
							text: 'Motif ou remarque',
							width: 300,
						},
						{
							xtype: 'actioncolumn',
							cls: 'content-column',
							width: 80,
							text: 'Actions',
							items: [
								{
									iconCls: 'x-fa fa-trash soft-red-small',
									handler: 'confirmDeleteAbs',
								},
							],					
						},
					],
				},
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
				],
				tbar: [],
			},
		];
		this.callParent();
	},

	typeAbsence(value) {
		if (value === '1') return 'Heure minus';
		if (value === '2') return 'Permission pour évènements familiaux';
		return 'Absence autorisée';
	},
});
