Ext.define('Gprh.ades.view.salaire.elementsVariables.SalaireInterimForm', {
	extend: 'Ext.form.Panel',

	xtype: 'salaireInterimForm',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.view.ades.grille.SalaireBaseViewModel',
		'Gprh.ades.view.salaire.elementsVariables.SalaireInterimGrid',
	],

	colorScheme: 'soft-green',
	bodyPadding: 5,

	initComponent() {
		const form = {
			xtype: 'form',
			itemId: 'salaireInterimFormId',
			defaultType: 'textfield',
			defaults: {
				labelWidth: 150,
				labelAlign: 'left',
				labelSeparator: ':',
				submitEmptyText: false,
			},
			items: [
				{
					fieldLabel: 'ID_PRIME_INDEMNITE',
					name: 'idPrimeIndemnite',
					itemId: 'idPrimeIndemniteId',
					anchor: '100%',
					hidden: true,
				},
				{
					fieldLabel: 'ID_CONTRAT',
					name: 'idContrat',
					itemId: 'idContratId',
					anchor: '100%',
					allowBlank: false,
					hidden: true,
				},
				{					
					xtype: 'datefield',
					fieldLabel: 'Date début de l\'intérim',
					name: 'dateDebutInterim',
					itemId: 'dateDebutInterimId',
					allowBlank: false,
					format: 'd/m/Y',
					submitFormat: 'Y-m-d',
					minText: 'Date début du congé de l\'employé à remplacer!',
				},
				{					
					xtype: 'datefield',
					fieldLabel: 'Date fin de l\'intérim',
					name: 'dateFinInterim',
					itemId: 'dateFinInterimId',
					allowBlank: false,
					format: 'd/m/Y',
					submitFormat: 'Y-m-d',
					minText: 'Date fin du congé de l\'employé à remplacer!',
				},
				{
					xtype: 'textfield',
					labelWidth: 150,
					labelAlign: 'top',
					labelSeparator: ':',
					width: 325,
					fieldLabel: 'Remarques ou motifs',
					name: 'remarquesInterim',
					itemId: 'remarquesInterimId',
				},
				{
					xtype: 'toolbar',
					cls: 'wizard-form-break',
					defaults: {
						flex: 1,
					},
					width: 300,
					layout: 'hbox',
					items: [
						{
							itemId: 'salaireInterimFormSaveBtn',
							xtype: 'button',
							text: 'Enregistrer',
							ui: 'soft-green-small',
							margin: '10 10 0 0',
							formBind: true,
							listeners: {
								click: 'onSalaireInterimFormSaveClick',
							},
							hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
						},
						{
							itemId: 'salaireInterimFormCancelBtn',
							xtype: 'button',
							text: 'Annuler',
							ui: 'soft-blue-small',
							margin: '10 0 0 0',
							listeners: {
								click: 'cancelAction',
							},
							params: 'salaireInterimFormId',
						},
					],
				},
			],
		};

		this.items = [
			{
				html: '<span class="x-fa fa-info-circle"></span> Sélectionnez une ligne dans la grille principale, replissez les cases et enregistrer.',
				baseCls: 'x-toast-info',
				anchor: '100%',
				margin: '0 0 2 0',
			},
			form,
			{ 
				xtype: 'salaireInterimGrid',
				itemId: 'salaireInterimGridId',
			},
		];

		this.callParent();
	},
});
