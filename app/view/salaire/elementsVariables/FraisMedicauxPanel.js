Ext.define('Gprh.ades.view.salaire.elementsVariables.FraisMedicauxPanel', {
	extend: 'Ext.panel.Panel',

	xtype: 'fraisMedicauxPanel',

	requires: [
		'Ext.layout.container.HBox',
		'Gprh.ades.view.salaire.SalaireController',
		'Gprh.ades.view.salaire.SalaireViewModel',
		'Gprh.ades.view.salaire.ContratGridMin',
		'Gprh.ades.view.salaire.elementsVariables.FraisMedicauxForm',
		'Gprh.ades.view.salaire.elementsVariables.FraisMedicauxGrid',
	],
	
	viewModel: {
		type: 'salaireViewModel',
	},

	controller: 'salaireController',
	
	itemId: 'fraisMedicauxPanelId',

	layout: {
		type: 'hbox',
		pack: 'start',
		align: 'stretch',
	},

	bodyPadding: 1,

	defaults: {
		frame: false,
		bodyPadding: 1,
	},

	items: [
		{
			title: 'Liste de contrats',
			flex: 2,
			collapsible: true,
			collapseDirection: 'left',
			collapseMode: 'header',
			margin: '0 1 0 0',
			itemId: 'contratGridMinPanelId',
			items: [
				{
					xtype: 'contratGridMin',
					itemId: 'contratGridMinId',
					listeners: {
						select: 'onContratFMClick',
					},
				},
			],
		},
		{
			title: 'Formulaire d\'enregistrement',
			flex: 1,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'fraisMedicauxFormPanelId',
			items: [
				{ xtype: 'fraisMedicauxForm' },
			],
		},
		{
			title: 'Liste des frais médicaux',
			flex: 1,
			collapsed: true,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'fraisMedicauxGridPanelId',
			items: [
				{
					xtype: 'fraisMedicauxGrid',
					itemId: 'fraisMedicauxGridId',
				},
			],
			listeners: {
				expand: (self) => {
					self.up().down('#contratGridMinPanelId').collapse(true);
					self.up().down('#fraisMedicauxFormPanelId').collapse(true);
				},
				collapse: (self) => {
					self.up().down('#contratGridMinPanelId').expand(true);
					self.up().down('#fraisMedicauxFormPanelId').expand(true);
				},
			},
		},
	],

});
