Ext.define('Gprh.ades.view.salaire.elementsVariables.DiversRetenusForm', {
	extend: 'Ext.form.Panel',

	xtype: 'diversRetenusForm',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.util.VTypes',
	],

	colorScheme: 'soft-green',
	bodyPadding: 5,

	initComponent() {
		const form = {
			xtype: 'form',
			itemId: 'DRFormId',
			layout: {
				type: 'vbox',
				align: 'stretch',
			},
			defaults: {
				labelWidth: 150,
				labelAlign: 'top',
				labelSeparator: ':',
				submitEmptyText: false,
			},
			listeners: {				
				beforelayout: () => {
					const me = this;
					const scroller = me.getScrollable();
					if (scroller) {
						me.savedScrollPos = scroller.getPosition();
					}
				},
				afterLayout: () => {
					const grid = this.up('#diversRetenusPanelId').down('#contratGridMinId');
					this.up('#diversRetenusPanelId').down('#contratGridMinId').getView().restoreScrollState(grid.view.scrollState.top);
				},
			},
			items: [
				{
					xtype: 'combobox',
					maxWidth: 200,
					fieldLabel: 'Mois salarial',
					name: 'idSalaire',
					itemId: 'idSalaireId',
					reference: 'moisFieldReference',
					publishes: 'value',
					bind: {
						store: '{salaireResults}',
					},
					queryMode: 'local',
					displayField: 'mois',
					valueField: 'idSalaire',
					renderTo: Ext.getBody(),
					allowBlank: false,
					forceSelection: true,
					editable: false,
					tpl: Ext.create('Ext.XTemplate',
						'<ul class="x-list-plain"><tpl for=".">',
						'<tpl if="clotureSalaire != 1">',
						'<li role="option" class="x-boundlist-item">{mois}</li>',
						'<tpl else>',
						// @todo Use a dedicated class instead of style
						'<li role="option" class="x-boundlist-item" style="cursor: not-allowed;color: orange;">{mois}</li>',
						'</tpl>',
						'</tpl></ul>'),
					listeners: {
						// Evite la sélection des mois de salaire déjà clôturés
						beforeselect: (combo, record) => record.get('clotureSalaire') !== '1',
					},
				},
				{
					xtype: 'textfield',
					fieldLabel: 'ID_DIVERS_RETENUS',
					name: 'idDiversRetenus',
					reference: 'idDiversRetenusRef',
					publishes: 'value',
					itemId: 'idDiversRetenusId',
					hidden: true,
				},
				{
					xtype: 'textfield',
					fieldLabel: 'ID_CONTRAT',
					name: 'idContrat',
					reference: 'idContratRef',
					publishes: 'value',
					itemId: 'idContratId',
					hidden: true,
					allowBlank: false,
				},
				{
					xtype: 'textfield',					
					fieldLabel: 'Montant (Ar)',
					name: 'montantDiversRetenus',
					reference: 'montantDiversRetenusRef',
					publishes: 'value',
					itemId: 'montantDiversRetenusId',
					maxWidth: 150,
					listeners: {
						blur: self => Gprh.ades.util.Globals.formatNumber(self),
					},
					msgTarget: 'side',
					allowBlank: false,
				},
				{
					xtype: 'datefield',
					name: 'dateDiversRetenus',
					itemId: 'dateDiversRetenusId',
					reference: 'dateDiversRetenusRef',
					publishes: 'value',
					fieldLabel: 'Date de la facture',
					format: 'd/m/Y',
					submitFormat: 'Y-m-d',
					maxWidth: 150,
					allowBlank: false,
				},
				{
					xtype: 'textfield',
					labelWidth: 150,
					labelAlign: 'top',
					labelSeparator: ':',
					width: 325,
					fieldLabel: 'Remarques ou motifs',
					name: 'remarqueDiversRetenus',
					itemId: 'remarqueDiversRetenusId',
				},
			],
			
		};

		this.items = [
			{
				html: '<span class="x-fa fa-info-circle"></span> Sélectionnez une ligne dans la grille principale, choisissez le mois salarial correspondant.</br>Le séparateur décimal à utiliser est le "."',
				itemId: 'helperId',
				baseCls: 'x-toast-info',
				anchor: '100%',
			},
			form,
			{
				xtype: 'toolbar',
				cls: 'wizard-form-break',
				defaults: {
					flex: 1,
				},
				width: 300,
				layout: 'hbox',
				items: [
					{
						itemId: 'DRFormSaveBtn',
						xtype: 'button',
						text: 'Enregistrer',
						ui: 'soft-green-small',
						margin: '10 10 0 0',
						formBind: true,
						listeners: {
							click: 'onDRFormSaveClick',
						},
					},
					{
						itemId: 'DRFormCancelBtn',
						xtype: 'button',
						text: 'Annuler',
						ui: 'soft-blue-small',
						margin: '10 0 0 0',
						listeners: {
							click: 'cancelAction',
						},
						params: 'DRFormId',
					},
				],
			},
			{
				xtype: 'toolbar',
				cls: 'wizard-form-break',
				defaults: {
					flex: 1,
				},
				layout: 'hbox',
				items: [
					{
						itemId: 'DRListBtn',
						xtype: 'button',
						text: 'Employé',
						tooltip: 'Liste des divers retenus par employé',
						ui: 'soft-purple',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onDREmployeListClick',
						},
						bind: {
							disabled: '{isEnabledEmploye}',
						},
					},
					{
						itemId: 'DRAllListBtn',
						xtype: 'button',
						text: 'Tous',
						tooltip: 'Liste de tous les divers retenus',
						ui: 'soft-purple-small',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onDRAllListClick',
						},
					},
					{
						itemId: 'DRMoisListBtn',
						xtype: 'button',
						text: 'Mois',
						tooltip: 'Liste de tous les divers retenus d\'un mois',
						ui: 'soft-purple-small',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onDRMoisListClick',
						},
						bind: {
							disabled: '{isEnabledMois}',
						},
					},
				],
			},
		];

		this.callParent();
	},
});
