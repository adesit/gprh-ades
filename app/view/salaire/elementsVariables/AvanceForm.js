Ext.define('Gprh.ades.view.salaire.elementsVariables.AvanceForm', {
	extend: 'Ext.form.Panel',

	xtype: 'avanceForm',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.util.VTypes',
	],

	colorScheme: 'soft-green',

	initComponent() {
		const form = {
			xtype: 'form',
			itemId: 'AvFormId',
			layout: {
				type: 'vbox',
				align: 'stretch',
			},
			defaults: {
				labelAlign: 'top',
				labelSeparator: ':',
				submitEmptyText: false,
			},
			listeners: {				
				beforeadd: () => {
					const me = this;
					const scroller = me.getScrollable();
					if (scroller) {
						me.savedScrollPos = scroller.getPosition();
					}
				},
				afterLayout: () => {
					const grid = this.up('#avancePanelId').down('#contratGridMinId');
					
					this.up('#avancePanelId').down('#contratGridMinId').getView().restoreScrollState(grid.view.scrollState.top);
				},
			},
			items: [
				{
					layout: 'column',
					margin: '0 5 0 0',
					items: [
						{
							defaultType: 'textfield',
							defaults: {
								labelWidth: 150,
								labelAlign: 'top',
								labelSeparator: ':',
								submitEmptyText: false,
								width: 150,
							},
							columnWidth: '0.50',
							items: [
								{
									xtype: 'textfield',					
									fieldLabel: 'Salaire Net',
									name: 'salaireBrut',
									reference: 'salaireBrutRef',
									publishes: 'value',
									itemId: 'salaireNetId',
									editable: false,
								},
								{
									xtype: 'textfield',
									fieldLabel: 'ID_AVANCE',
									name: 'idAvance',
									reference: 'idAvanceRef',
									publishes: 'value',
									itemId: 'idAvanceId',
									hidden: true,
								},
								{
									xtype: 'textfield',
									fieldLabel: 'ID_CONTRAT',
									name: 'idContrat',
									reference: 'idContratRef',
									publishes: 'value',
									itemId: 'idContratId',
									hidden: true,
									allowBlank: false,
								},
								{
									xtype: 'textfield',					
									fieldLabel: 'Montant plafond',
									name: 'maxAutorise',
									reference: 'maxAutoriseRef',
									publishes: 'value',
									itemId: 'maxAutoriseId',
									allowBlank: false,
									listeners: {
										blur: self => Gprh.ades.util.Globals.formatNumber(self),
									},
									editable: false,
								},
								{
									xtype: 'textfield',					
									fieldLabel: 'Déduction/mois max',
									name: 'maxDeductionPossible',
									reference: 'maxDeductionPossibleRef',
									publishes: 'value',
									itemId: 'maxDeductionPossibleId',
									allowBlank: false,
									listeners: {
										blur: self => Gprh.ades.util.Globals.formatNumber(self),
									},
									editable: false,
								},
								{
									xtype: 'textfield',					
									fieldLabel: 'Montant demandé',
									name: 'montantAvance',
									reference: 'montantAvanceRef',
									publishes: 'value',
									itemId: 'montantAvanceId',
									allowBlank: false,
									listeners: {
										blur: self => Gprh.ades.util.Globals.formatNumber(self),
									},
									vtype: 'avanceMatch',
									msgTarget: 'side',
								},
								{
									xtype: 'textfield',					
									fieldLabel: 'Remboursement/mois',
									name: 'deductionMensuelle',
									reference: 'deductionMensuelleRef',
									publishes: 'value',
									itemId: 'deductionMensuelleId',
									allowBlank: false,
									listeners: {
										blur: {
											fn: 'detailsRemboursement2',
										},
									},
									vtype: 'avanceRemboursementMatch',
									msgTarget: 'side',
								},
							],
						},
						{
							defaultType: 'textfield',
							defaults: {
								labelWidth: 175,
								labelAlign: 'top',
								labelSeparator: ':',
								submitEmptyText: false,
								width: 175,
							},
							columnWidth: '0.50',
							items: [
								{
									xtype: 'datefield',
									name: 'dateReception',
									itemId: 'dateReceptionId',
									reference: 'dateReceptionRef',
									publishes: 'value',
									fieldLabel: 'Date de la demande',
									format: 'd/m/Y',
									submitFormat: 'Y-m-d',
									allowBlank: false,
								},
								{
									xtype: 'datefield',
									name: 'dateDebutRemboursement',
									itemId: 'dateDebutRemboursementId',
									reference: 'dateDebutRemboursementRef',
									publishes: 'value',
									fieldLabel: 'Début de remboursement',
									format: 'd/m/Y',
									submitFormat: 'Y-m-d',
									allowBlank: false,
									listeners: {
										change: 'setDetailsRemboursement',
									},
								},
								{
									xtype: 'numberfield',
									fieldLabel: 'Nombre de tranches',
									name: 'nombreTranche',
									reference: 'nombreTrancheRef',
									publishes: 'value',
									itemId: 'nombreTrancheId',
									allowDecimal: false,
									allowBlank: false,
									maxValue: 10,
									minValue: 1,
									msgTarget: 'side',
									listeners: {
										afterrender: () => {
											const els = Ext.select('div.x-form-trigger.x-form-trigger-default.x-form-trigger-spinner.x-form-trigger-spinner-default.x-unselectable', true);
											els.removeCls('x-form-trigger-default');
										},
										change: 'setDetailsRemboursement',
									},
								},
								{
									xtype: 'datefield',
									name: 'dateFinRemboursement',
									itemId: 'dateFinRemboursementId',
									reference: 'dateFinRemboursementRef',
									publishes: 'value',
									fieldLabel: 'Fin de remboursement',
									format: 'd/m/Y',
									submitFormat: 'Y-m-d',
									allowBlank: false,
								},
								{
									xtype: 'checkboxfield',
									fieldLabel: 'En cours',
									labelWidth: 150,
									name: 'enCours',
									itemId: 'enCoursId',
									checked: true,
								},
							],
						},
					], 
				},
				{
					xtype: 'textfield',					
					fieldLabel: 'Raison de la demande',
					name: 'raison',
					reference: 'raisonRef',
					publishes: 'value',
					itemId: 'raisonId',
					allowBlank: false,
				},
			],
			
		};

		this.items = [
			form,
			{
				xtype: 'toolbar',
				cls: 'wizard-form-break',
				defaults: {
					flex: 1,
				},
				width: 300,
				layout: 'hbox',
				items: [
					{
						itemId: 'AvFormSaveBtn',
						xtype: 'button',
						text: 'Enregistrer',
						ui: 'soft-green-small',
						margin: '5 10 0 0',
						formBind: true,
						listeners: {
							click: 'onAvFormSaveClick',
						},
						hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
					},
					{
						itemId: 'AvFormCancelBtn',
						xtype: 'button',
						text: 'Annuler',
						ui: 'soft-blue-small',
						margin: '5 0 0 0',
						listeners: {
							click: 'cancelAction',
						},
						params: 'AvFormId',
					},
				],
			},
			{
				html: '<span class="x-fa fa-info-circle"></span> La date par défaut pour calculer le montant plafond est la date du jour. Vous pouvez choisir une autre en cliquant sur "Date de la demande".',
				itemId: 'helperId',
				baseCls: 'x-toast-info',
				anchor: '100%',
			},
		];

		this.callParent();
	},
});
