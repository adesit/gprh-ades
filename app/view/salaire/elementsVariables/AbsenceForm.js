Ext.define('Gprh.ades.view.salaire.elementsVariables.AbsenceForm', {
	extend: 'Ext.form.Panel',

	xtype: 'absenceForm',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.util.VTypes',
	],

	colorScheme: 'soft-green',
	bodyPadding: 2,

	initComponent() {
		const form = {
			xtype: 'form',
			itemId: 'AbsFormId',
			layout: {
				type: 'vbox',
				align: 'stretch',
			},
			defaults: {
				labelWidth: 150,
				labelAlign: 'top',
				labelSeparator: ':',
				submitEmptyText: false,
			},
			listeners: {				
				beforeadd: () => {
					const me = this;
					const scroller = me.getScrollable();
					if (scroller) {
						me.savedScrollPos = scroller.getPosition();
					}
				},
				afterLayout: () => {
					const grid = this.up('#absencePanelId').down('#contratGridMinId');
					
					/* const selectedRecord = grid.getSelectionModel().getSelection()[0];
					const row = grid.store.indexOf(selectedRecord);

					if (row > 0) {
						Ext.ComponentQuery.query('#contratGridMinId')[0].getView().getRow(row).scrollIntoView();
					} */
					this.up('#absencePanelId').down('#contratGridMinId').getView().restoreScrollState(grid.view.scrollState.top);
				},
			},
			items: [
				{
					layout: 'column',
					items: [
						{
							columnWidth: '0.55',
							defaults: {
								labelWidth: 150,
								labelAlign: 'top',
								labelSeparator: ':',
								submitEmptyText: false,
							},
							items: [
								{
									xtype: 'combobox',
									maxWidth: 200,
									fieldLabel: 'Mois salarial',
									name: 'idSalaire',
									itemId: 'idSalaireId',
									reference: 'moisFieldReference',
									publishes: 'value',
									bind: {
										store: '{salaireResults}',
									},
									queryMode: 'local',
									displayField: 'mois',
									valueField: 'idSalaire',
									renderTo: Ext.getBody(),
									allowBlank: false, 
									forceSelection: true,
									editable: false,
									tpl: Ext.create('Ext.XTemplate',
										'<ul class="x-list-plain"><tpl for=".">',
										'<tpl if="clotureSalaire != 1">',
										'<li role="option" class="x-boundlist-item">{mois}</li>',
										'<tpl else>',
										// @todo Use a dedicated class instead of style
										'<li role="option" class="x-boundlist-item" style="cursor: not-allowed;color: orange;">{mois}</li>',
										'</tpl>',
										'</tpl></ul>'),
									listeners: {
										// Evite la sélection des mois de salaire déjà clôturés
										beforeselect: (combo, record) => record.get('clotureSalaire') !== '1',
									},
								},
							],
						},
						{
							columnWidth: '0.45',
							items: [
								{
									xtype: 'displayfield',
									fieldLabel: 'Solde de permission',
									name: 'soldePermission',
									reference: 'soldePermissionRef',
									publishes: 'value',
									itemId: 'soldePermissionId',
									id: 'soldePermissionId',
									baseCls: 'x-toast-success',
									fieldStyle: 'font-weight: bold;',
									margin: '10 0 0 0',
									hidden: true,
									width: 150,
									bind: {
										hidden: '{!isHiddenSoldePermission}',
									},
								},
								{
									xtype: 'displayfield',
									fieldLabel: 'Solde de permission',
									name: 'soldePermission2',
									reference: 'soldePermissionRef2',
									publishes: 'value',
									itemId: 'soldePermissionId2',
									id: 'soldePermissionId2',
									baseCls: 'x-toast-error',
									fieldStyle: 'font-weight: bold;',
									margin: '10 0 0 0',
									hidden: true,
								},
							],
						},
					],
				},
				{
					xtype: 'radiogroup',
					labelAlign: 'top',
					fieldLabel: 'Type d\'absence',
					itemId: 'typeAbsenceId',
					bind: {
						value: '{switchItem}',
					},
					columns: 1,
					vertical: true,
					items: [
						{
							boxLabel: 'Heures minus ou absence sans motif',
							name: 'typeAbsence',
							inputValue: 1,
						}, {
							boxLabel: 'Permission pour évènements familiaux', 
							name: 'typeAbsence',
							inputValue: 2,
						}, {
							boxLabel: 'Absence autorisée', 
							name: 'typeAbsence',
							inputValue: 3,
						},
					],
					listeners: {
						change: 'addMotifAbsenceField',
					},
				},
				{
					xtype: 'textfield',
					fieldLabel: 'ID_ABSENCE',
					name: 'idAbsence',
					reference: 'idAbsenceRef',
					publishes: 'value',
					itemId: 'idAbsenceId',
					hidden: true,
				},
				{
					xtype: 'textfield',
					fieldLabel: 'ID_CONTRAT',
					name: 'idContrat',
					reference: 'idContratRef',
					publishes: 'value',
					itemId: 'idContratId',
					hidden: true,
					allowBlank: false,
				},
				{
					layout: 'column',
					items: [
						{
							columnWidth: '0.50',
							items: [
								{
									xtype: 'datefield',
									labelAlign: 'top',
									name: 'dateHeureDebutHs',
									itemId: 'dateHeureDebutHsId',
									reference: 'dateHeureDebutHsRef',
									publishes: 'value',
									fieldLabel: 'Début d\'absence',
									format: 'd/m/Y',
									submitFormat: 'Y-m-d',
									width: 140,
									disabled: true,
									bind: {
										disabled: '{!isHiddenDureeAbs}',
									},
									listeners: {
										select: 'refreshDureeSoldePermission',
									},
								},
								{
									xtype: 'textfield',
									labelAlign: 'top',				
									fieldLabel: 'Nombre d\'heures',
									name: 'dureeAbsence',
									reference: 'dureeAbsenceRef',
									publishes: 'value',
									itemId: 'dureeAbsenceId',
									width: 140,
									disabled: true,
									bind: {
										disabled: '{!isHiddenDureeAbs}',
									},
									editable: false,
									vtype: 'decimal',
									msgTarget: 'side',
								},
							],
						},
						{
							columnWidth: '0.50',
							items: [
								{
									xtype: 'datefield',
									labelAlign: 'top',
									name: 'dateHeureFinHs',
									itemId: 'dateHeureFinHsId',
									reference: 'dateHeureFinHsRef',
									publishes: 'value',
									fieldLabel: 'Fin d\'absence',
									format: 'd/m/Y',
									submitFormat: 'Y-m-d',
									width: 140,
									disabled: true,
									bind: {
										disabled: '{!isHiddenDureeAbs}',
									},
									listeners: {
										select: 'refreshDureeSoldePermission',
									},
								},
								{
									xtype: 'textfield',
									labelAlign: 'top',				
									fieldLabel: 'Nombre de jours',
									name: 'nbJours',
									reference: 'nbJoursRef',
									publishes: 'value',
									itemId: 'nbJoursId',
									width: 140,
									disabled: true,
									bind: {
										disabled: '{!isHiddenDureeAbs}',
									},
									listeners: {
										blur: (self) => {
											Gprh.ades.util.Globals.formatNumber(self);
											this.down('#dureeAbsenceId').setValue(self.value * 8);
										},
									},
									vtype: 'decimal',
									msgTarget: 'side',
								},
							],
						},
					],
				},				
				{
					xtype: 'textfield',
					name: 'motifAbsence',
					itemId: 'motifAbsenceId',
					reference: 'motifAbsencesRef',
					publishes: 'value',
					labelWidth: 150,
					labelAlign: 'top',
					labelSeparator: ':',
					width: 325,
					fieldLabel: 'Remarques ou motifs',
					allowBlank: false,
				},
			],
			
		};

		this.items = [
			{
				html: '<span class="x-fa fa-info-circle"></span> Sélectionnez une ligne dans la grille principale choisissez le mois salarial correspondant.',
				itemId: 'helperId',
				baseCls: 'x-toast-info',
				anchor: '100%',
			},
			form,
			{
				xtype: 'toolbar',
				cls: 'wizard-form-break',
				defaults: {
					flex: 1,
				},
				width: 300,
				layout: 'hbox',
				items: [
					{
						itemId: 'AbsFormSaveBtn',
						xtype: 'button',
						text: 'Enregistrer',
						ui: 'soft-green-small',
						margin: '0 10 0 0',
						disabled: true,
						formBind: true,
						bind: {
							disabled: '{!isEnabledAbs}',
						},
						listeners: {
							click: 'onAbsFormSaveClick',
						},
					},
					{
						itemId: 'AbsFormCancelBtn',
						xtype: 'button',
						text: 'Annuler',
						ui: 'soft-blue-small',
						margin: '0 0 0 0',
						listeners: {
							click: 'cancelAction',
						},
						params: 'AbsFormId',
					},
				],
			},
			{
				xtype: 'toolbar',
				cls: 'wizard-form-break',
				defaults: {
					flex: 1,
				},
				layout: 'hbox',
				items: [
					{
						itemId: 'AbsListBtn',
						xtype: 'button',
						text: 'Employé',
						tooltip: 'Liste des absences par employé',
						ui: 'soft-purple',
						iconCls: 'fa fa-list',
						margin: '5 10 0 0',
						listeners: {
							click: 'onAbsEmployeListClick',
						},
						bind: {
							disabled: '{isEnabledEmploye}',
						},
					},
					{
						itemId: 'AbsAllListBtn',
						xtype: 'button',
						text: 'Toutes',
						tooltip: 'Liste de toutes les absences',
						ui: 'soft-purple-small',
						iconCls: 'fa fa-list',
						margin: '5 10 0 0',
						listeners: {
							click: 'onAbsAllListClick',
						},
					},
					{
						itemId: 'AbsMoisListBtn',
						xtype: 'button',
						text: 'Mois',
						tooltip: 'Liste de toutes les absences d\'un mois',
						ui: 'soft-purple-small',
						iconCls: 'fa fa-list',
						margin: '5 10 0 0',
						listeners: {
							click: 'onAbsMoisListClick',
						},
						bind: {
							disabled: '{isEnabledMois}',
						},
					},
				],
			},
		];

		this.callParent();
	},
});
