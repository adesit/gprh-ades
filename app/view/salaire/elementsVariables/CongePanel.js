Ext.define('Gprh.ades.view.salaire.elementsVariables.CongePanel', {
	extend: 'Ext.panel.Panel',

	xtype: 'congePanel',

	requires: [
		'Ext.layout.container.HBox',
		'Gprh.ades.view.salaire.SalaireController',
		'Gprh.ades.view.salaire.SalaireViewModel',
		'Gprh.ades.view.salaire.ContratGridMin',
		'Gprh.ades.view.salaire.elementsVariables.CongeForm',
		'Gprh.ades.view.salaire.elementsVariables.CongeGrid',
	],
	
	viewModel: {
		type: 'salaireViewModel',
	},

	controller: 'salaireController',
	
	itemId: 'congePanelId',

	layout: {
		type: 'hbox',
		pack: 'start',
		align: 'stretch',
	},

	bodyPadding: 1,

	defaults: {
		frame: false,
		bodyPadding: 1,
	},

	items: [
		{
			title: 'Liste de contrats',
			flex: 2,
			collapsible: true,
			collapseDirection: 'left',
			collapseMode: 'header',
			margin: '0 1 0 0',
			itemId: 'contratGridMinPanelId',
			items: [
				{
					xtype: 'contratGridMin',
					itemId: 'contratGridMinId',
					listeners: {
						select: 'onContratCngClick',
					},
				},
			],
		},
		{
			title: 'Formulaire d\'enregistrement',
			flex: 1,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'congeFormPanelId',
			items: [
				{ xtype: 'congeForm' },
			],
		},
		{
			title: 'Liste des congés',
			flex: 1,
			collapsed: true,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'congeGridPanelId',
			items: [
				{
					xtype: 'congeGrid',
					itemId: 'congeGridId',
				},
			],
			listeners: {
				expand: (self) => {
					self.up().down('#contratGridMinPanelId').collapse(true);
					self.up().down('#congeFormPanelId').collapse(true);
				},
				collapse: (self) => {
					self.up().down('#contratGridMinPanelId').expand(true);
					self.up().down('#congeFormPanelId').expand(true);
				},
			},
		},
	],

});
