Ext.define('Gprh.ades.view.salaire.elementsVariables.DiversRetenusPanel', {
	extend: 'Ext.panel.Panel',

	xtype: 'diversRetenusPanel',

	requires: [
		'Ext.layout.container.HBox',
		'Gprh.ades.view.salaire.SalaireController',
		'Gprh.ades.view.salaire.SalaireViewModel',
		'Gprh.ades.view.salaire.ContratGridMin',
		'Gprh.ades.view.salaire.elementsVariables.DiversRetenusForm',
		'Gprh.ades.view.salaire.elementsVariables.DiversRetenusGrid',
	],
	
	viewModel: {
		type: 'salaireViewModel',
	},

	controller: 'salaireController',
	
	itemId: 'diversRetenusPanelId',

	layout: {
		type: 'hbox',
		pack: 'start',
		align: 'stretch',
	},

	bodyPadding: 1,

	defaults: {
		frame: false,
		bodyPadding: 1,
	},

	items: [
		{
			title: 'Liste de contrats',
			flex: 2,
			collapsible: true,
			collapseDirection: 'left',
			collapseMode: 'header',
			margin: '0 1 0 0',
			itemId: 'contratGridMinPanelId',
			items: [
				{
					xtype: 'contratGridMin',
					itemId: 'contratGridMinId',
					listeners: {
						select: 'onContratDRClick',
					},
				},
			],
		},
		{
			title: 'Formulaire d\'enregistrement',
			flex: 1,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'diversRetenusFormPanelId',
			items: [
				{ xtype: 'diversRetenusForm' },
			],
		},
		{
			title: 'Liste des divers retenus',
			flex: 1,
			collapsed: true,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'diversRetenusGridPanelId',
			items: [
				{
					xtype: 'diversRetenusGrid',
					itemId: 'diversRetenusGridId',
				},
			],
			listeners: {
				expand: (self) => {
					self.up().down('#contratGridMinPanelId').collapse(true);
					self.up().down('#diversRetenusFormPanelId').collapse(true);
				},
				collapse: (self) => {
					self.up().down('#contratGridMinPanelId').expand(true);
					self.up().down('#diversRetenusFormPanelId').expand(true);
				},
			},
		},
	],

});
