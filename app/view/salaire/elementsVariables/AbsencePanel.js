Ext.define('Gprh.ades.view.salaire.elementsVariables.AbsencePanel', {
	extend: 'Ext.panel.Panel',

	xtype: 'absencePanel',

	requires: [
		'Ext.layout.container.HBox',
		'Gprh.ades.view.salaire.SalaireController',
		'Gprh.ades.view.salaire.SalaireViewModel',
		'Gprh.ades.view.salaire.ContratGridMin',
		'Gprh.ades.view.salaire.elementsVariables.AbsenceForm',
		'Gprh.ades.view.salaire.elementsVariables.AbsenceGrid',
	],
	
	viewModel: {
		type: 'salaireViewModel',
	},

	controller: 'salaireController',
	
	itemId: 'absencePanelId',

	layout: {
		type: 'hbox',
		pack: 'start',
		align: 'stretch',
	},

	bodyPadding: 1,

	defaults: {
		frame: false,
		bodyPadding: 1,
	},

	items: [
		{
			title: 'Liste de contrats',
			flex: 2,
			collapsible: true,
			collapseDirection: 'left',
			collapseMode: 'header',
			margin: '0 1 0 0',
			itemId: 'contratGridMinPanelId',
			items: [
				{
					xtype: 'contratGridMin',
					itemId: 'contratGridMinId',
					listeners: {
						select: 'onContratAbsClick',
					},
				},
			],
		},
		{
			title: 'Formulaire d\'enregistrement',
			flex: 1,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'absenceFormPanelId',
			items: [
				{ xtype: 'absenceForm' },
			],
		},
		{
			title: 'Liste des absences',
			flex: 1,
			collapsed: true,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'absenceGridPanelId',
			items: [
				{
					xtype: 'absenceGrid',
					itemId: 'absenceGridId',
				},
			],
			listeners: {
				expand: (self) => {
					self.up().down('#contratGridMinPanelId').collapse(true);
					self.up().down('#absenceFormPanelId').collapse(true);
				},
				collapse: (self) => {
					self.up().down('#contratGridMinPanelId').expand(true);
					self.up().down('#absenceFormPanelId').expand(true);
				},
			},
		},
	],

});
