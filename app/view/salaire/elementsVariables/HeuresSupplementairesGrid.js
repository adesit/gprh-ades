Ext.define('Gprh.ades.view.salaire.elementsVariables.HeuresSupplementairesGrid', {
	extend: 'Ext.panel.Panel',
	xtype: 'heuresSupplementaireGrid',	
	
	initComponent() {
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		const Store = Ext.create('Gprh.ades.store.HeuresSupplementaires');
		Ext.apply(Store.getProxy(), extraParameters);

		this.items = [
			{
				xtype: 'gridpanel',
				reference: 'heuresSupplementaireGridRef',
				itemId: 'gridpanel',
				store: Store,
				viewConfig: {
					preserveScrollOnRefresh: true,
					stripeRows: true,
				},
				cls: 'user-grid',
				scrollable: true,
				selModel: 'rowmodel',
				listeners: {
					rowclick: 'onRowHSClick',
				},
				height: Ext.Element.getViewportHeight() - 125,
				width: 'auto',
				columnLines: true,
				columns: {
					defaults: {
						align: 'left',
						width: 100,
					},
					items: [
						{
							xtype: 'gridcolumn',
							dataIndex: 'idHeureSupplementaire',
							text: '#HS',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'idContrat',
							text: '#Contrat',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'moisAnnee',
							text: 'Mois salaire',
							width: 150,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'matricule',
							text: 'Matricule',				
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nom',
							text: 'Noms et prénoms',
							width: 300,
							renderer(value, p, r) {
								return `${r.data.nom} ${r.data.prenom}`;
							},
							filter: {
								type: 'string',
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'infHuitHeures',
							text: '<8h (130%)',
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'supHuitHeures',
							text: '>8h (150%)',
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nuitHabituelle',
							text: 'Nuit habituelle',
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nuitOccasionnelle',
							text: 'Nuit occasionnelle',
							width: 120,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'dimanche',
							text: 'Dimanche',
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'ferie',
							text: 'Travail le jour férié',
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'remarqueMotifHs',
							text: 'Remarques ou motifs',
							width: 150,
						},
						{
							xtype: 'actioncolumn',
							cls: 'content-column',
							width: 80,
							text: 'Actions',
							items: [
								{
									iconCls: 'x-fa fa-trash soft-red-small',
									handler: 'confirmDeleteHS',
								},
							],					
						},
					],
				},
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
				],
				tbar: [],
			},
		];
		this.callParent();
	},	
});
