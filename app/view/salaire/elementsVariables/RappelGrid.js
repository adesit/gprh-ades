Ext.define('Gprh.ades.view.salaire.elementsVariables.RappelGrid', {
	extend: 'Ext.panel.Panel',
	xtype: 'rappelGrid',
	
	initComponent() {
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		const Store = Ext.create('Gprh.ades.store.Rappel');
		Ext.apply(Store.getProxy(), extraParameters);
		this.items = [
			{
				xtype: 'gridpanel',
				itemId: 'gridpanel',
				reference: 'rappelGridRef',
				store: Store,
				viewConfig: {
					preserveScrollOnRefresh: true,
					stripeRows: true,
				},
				cls: 'user-grid',
				scrollable: true,
				selModel: 'rowmodel',
				listeners: {
					rowclick: 'onRowRAPClick',
				},
				height: Ext.Element.getViewportHeight() - 125,
				width: 'auto',
				columnLines: true,
				columns: {
					defaults: {
						align: 'left',
						width: 100,
					},
					items: [
						{
							xtype: 'gridcolumn',
							dataIndex: 'idRappel',
							text: '#Retenus',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'idContrat',
							text: '#Contrat',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateDebutRappel',
							text: 'Date début du rappel',
							format: 'd/m/Y',
							flex: 1,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'matricule',
							text: 'Matricule',				
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nom',
							text: 'Noms et prénoms',
							width: 300,
							renderer(value, p, r) {
								return `${r.data.nom} ${r.data.prenom}`;
							},
							filter: {
								type: 'string',
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'montantRappel',
							text: 'Montant (Ar)',
							flex: 1,
							renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateFinRappel',
							text: 'Date fin du rappel',
							format: 'd/m/Y',
							flex: 1,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'typeRappel',
							text: 'Rappel sur',
							width: 120,
							renderer: value => (value === '1' ? 'Salaire de base' : 'Salaire Net'),
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'remarqueRappel',
							text: 'Motif ou remarque',
							width: 120,
						},
						{
							xtype: 'actioncolumn',
							cls: 'content-column',
							width: 80,
							text: 'Actions',
							items: [
								{
									iconCls: 'x-fa fa-trash soft-red-small',
									handler: 'confirmDeleteRappel',
								},
							],					
						},
					],
				},
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
				],
				tbar: [],
			},
		];
		this.callParent();
	},
});
