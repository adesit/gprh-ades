Ext.define('Gprh.ades.view.salaire.elementsVariables.DiversRetenusGrid', {
	extend: 'Ext.panel.Panel',
	xtype: 'diversRetenusGrid',
	
	initComponent() {
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		const Store = Ext.create('Gprh.ades.store.DiversRetenus');
		Ext.apply(Store.getProxy(), extraParameters);
		this.items = [
			{
				xtype: 'gridpanel',
				itemId: 'gridpanel',
				reference: 'diversRetenusGridRef',
				store: Store,
				viewConfig: {
					preserveScrollOnRefresh: true,
					stripeRows: true,
				},
				cls: 'user-grid',
				scrollable: true,
				selModel: 'rowmodel',
				listeners: {
					rowclick: 'onRowDRClick',
				},
				height: Ext.Element.getViewportHeight() - 125,
				width: 'auto',
				columnLines: true,
				columns: {
					defaults: {
						align: 'left',
						width: 100,
					},
					items: [
						{
							xtype: 'gridcolumn',
							dataIndex: 'idDiversRetenus',
							text: '#Retenus',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'idContrat',
							text: '#Contrat',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'moisAnnee',
							text: 'Mois salaire',
							width: 150,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'matricule',
							text: 'Matricule',				
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nom',
							text: 'Noms et prénoms',
							width: 300,
							renderer(value, p, r) {
								return `${r.data.nom} ${r.data.prenom}`;
							},
							filter: {
								type: 'string',
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'montantDiversRetenus',
							text: 'Montant (Ar)',
							flex: 1,
							renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateDiversRetenus',
							text: 'Date de la facture',
							format: 'd/m/Y',
							flex: 1,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'remarqueDiversRetenus',
							text: 'Motif ou remarque',
							width: 120,
						},
						{
							xtype: 'actioncolumn',
							cls: 'content-column',
							width: 80,
							text: 'Actions',
							items: [
								{
									iconCls: 'x-fa fa-trash soft-red-small',
									handler: 'confirmDeleteDR',
								},
							],					
						},
					],
				},
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
				],
				tbar: [],
			},
		];
		this.callParent();
	},
});
