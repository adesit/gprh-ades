Ext.define('Gprh.ades.view.salaire.elementsVariables.DepassementForm', {
	extend: 'Ext.form.Panel',

	xtype: 'depassementForm',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.util.VTypes',
	],

	colorScheme: 'soft-green',
	bodyPadding: 5,

	initComponent() {
		const form = {
			xtype: 'form',
			itemId: 'DPSFormId',
			layout: {
				type: 'vbox',
				align: 'stretch',
			},
			defaults: {
				labelWidth: 150,
				labelAlign: 'top',
				labelSeparator: ':',
				submitEmptyText: false,
			},
			listeners: {				
				beforelayout: () => {
					const me = this;
					const scroller = me.getScrollable();
					if (scroller) {
						me.savedScrollPos = scroller.getPosition();
					}
				},
				afterLayout: () => {
					const grid = this.up('#depassementPanelId').down('#contratGridMinId');
					this.up('#depassementPanelId').down('#contratGridMinId').getView().restoreScrollState(grid.view.scrollState.top);
				},
			},
			items: [
				{
					xtype: 'combobox',
					maxWidth: 200,
					fieldLabel: 'Mois salarial',
					name: 'idSalaire',
					itemId: 'idSalaireId',
					reference: 'moisFieldReference',
					publishes: 'value',
					bind: {
						store: '{salaireResults}',
					},
					queryMode: 'local',
					displayField: 'mois',
					valueField: 'idSalaire',
					renderTo: Ext.getBody(),
					allowBlank: false,
					forceSelection: true,
					editable: false,
					tpl: Ext.create('Ext.XTemplate',
						'<ul class="x-list-plain"><tpl for=".">',
						'<tpl if="clotureSalaire != 1">',
						'<li role="option" class="x-boundlist-item">{mois}</li>',
						'<tpl else>',
						// @todo Use a dedicated class instead of style
						'<li role="option" class="x-boundlist-item" style="cursor: not-allowed;color: orange;">{mois}</li>',
						'</tpl>',
						'</tpl></ul>'),
					listeners: {
						// Evite la sélection des mois de salaire déjà clôturés
						beforeselect: (combo, record) => record.get('clotureSalaire') !== '1',
					},
				},
				{
					xtype: 'textfield',
					fieldLabel: 'ID_APPEL',
					name: 'idAppel',
					reference: 'idAppelRef',
					publishes: 'value',
					itemId: 'idAppelId',
					hidden: true,
				},
				{
					xtype: 'textfield',
					fieldLabel: 'ID_CONTRAT',
					name: 'idContrat',
					reference: 'idContratRef',
					publishes: 'value',
					itemId: 'idContratId',
					hidden: true,
					allowBlank: false,
				},
				{
					xtype: 'textfield',					
					fieldLabel: 'Montant (Ar)',
					name: 'depassement',
					reference: 'depassementRef',
					publishes: 'value',
					itemId: 'depassementId',
					maxWidth: 150,
					listeners: {
						blur: self => Gprh.ades.util.Globals.formatNumber(self),
					},
					msgTarget: 'side',
					allowBlank: false,
				},
				{
					xtype: 'textfield',
					labelWidth: 150,
					labelAlign: 'top',
					labelSeparator: ':',
					width: 325,
					fieldLabel: 'Remarques ou motifs',
					name: 'remarqueDepassement',
					itemId: 'remarqueDepassementId',
				},
			],
			
		};

		this.items = [
			{
				html: '<span class="x-fa fa-info-circle"></span> Sélectionnez une ligne dans la grille principale, choisissez le mois salarial correspondant.</br>Le séparateur décimal à utiliser est le "."',
				itemId: 'helperId',
				baseCls: 'x-toast-info',
				anchor: '100%',
			},
			form,
			{
				xtype: 'toolbar',
				cls: 'wizard-form-break',
				defaults: {
					flex: 1,
				},
				width: 300,
				layout: 'hbox',
				items: [
					{
						itemId: 'DPSFormSaveBtn',
						xtype: 'button',
						text: 'Enregistrer',
						ui: 'soft-green-small',
						margin: '10 10 0 0',
						formBind: true,
						listeners: {
							click: 'onDPSFormSaveClick',
						},
						hidden: !(Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur') === '1'),
					},
					{
						itemId: 'DPSFormCancelBtn',
						xtype: 'button',
						text: 'Annuler',
						ui: 'soft-blue-small',
						margin: '10 0 0 0',
						listeners: {
							click: 'cancelAction',
						},
						params: 'DPSFormId',
					},
				],
			},
			{
				xtype: 'toolbar',
				cls: 'wizard-form-break',
				defaults: {
					flex: 1,
				},
				layout: 'hbox',
				items: [
					{
						itemId: 'DPSListBtn',
						xtype: 'button',
						text: 'Employé',
						tooltip: 'Liste des dépassements par employé',
						ui: 'soft-purple',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onDPSEmployeListClick',
						},
						bind: {
							disabled: '{isEnabledEmploye}',
						},
					},
					{
						itemId: 'DPSAllListBtn',
						xtype: 'button',
						text: 'Tous',
						tooltip: 'Liste de tous les dépassements',
						ui: 'soft-purple-small',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onDPSAllListClick',
						},
					},
					{
						itemId: 'DPSMoisListBtn',
						xtype: 'button',
						text: 'Mois',
						tooltip: 'Liste de tous les dépassements d\'un mois',
						ui: 'soft-purple-small',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onDPSMoisListClick',
						},
						bind: {
							disabled: '{isEnabledMois}',
						},
					},
				],
			},
		];

		this.callParent();
	},
});
