Ext.define('Gprh.ades.view.salaire.elementsVariables.AvanceGrid', {
	extend: 'Ext.panel.Panel',
	xtype: 'avanceGrid',
	
	initComponent() {
		const extraParameters = {
			extraParams: {
				infoRequest: 1,
				typeUtilisateur: Ext.util.LocalStorage.get('foo').getItem('typeUtilisateur'),
				idCentre: Ext.util.LocalStorage.get('foo').getItem('idCentre'),
			},
		};
		const Store = Ext.create('Gprh.ades.store.Avance');
		Ext.apply(Store.getProxy(), extraParameters);
		this.items = [
			{
				xtype: 'gridpanel',
				itemId: 'gridpanel',
				reference: 'avanceGridRef',
				store: Store,
				viewConfig: {
					preserveScrollOnRefresh: true,
					stripeRows: true,
				},
				cls: 'user-grid',
				scrollable: true,
				selModel: 'rowmodel',
				listeners: {
					rowdblclick: 'onRowAvanceDblClick',
					rowclick: 'onRowAvanceClick',
				},
				height: Ext.Element.getViewportHeight() - 125,
				width: 'auto',
				columnLines: true,
				columns: {
					defaults: {
						align: 'left',
						width: 100,
					},
					items: [
						{
							xtype: 'gridcolumn',
							dataIndex: 'idAvance',
							text: '#Avance',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'idContrat',
							text: '#Contrat',
							width: 50,
							hidden: true,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'matricule',
							text: 'Matricule',				
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nom',
							text: 'Noms et prénoms',
							minWidth: 200,
							renderer(value, p, r) {
								return `${r.data.nom} ${r.data.prenom}`;
							},
							filter: {
								type: 'string',
							},
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'maxAutorise',
							text: 'Montant plafond',
							renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'montantAvance',
							text: 'Montant demandé',
							renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateReception',
							text: 'Début de la demande',
							format: 'd/m/Y',
							width: 100,
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateDebutRemboursement',
							text: 'Début de remboursement',
							format: 'd/m/Y',
							width: 100,
						},
						{
							xtype: 'datecolumn',
							dataIndex: 'dateFinRemboursement',
							text: 'Fin de remboursement',
							format: 'd/m/Y',
							width: 100,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'raison',
							text: 'Motif ou remarque',
							width: 120,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'nombreTranche',
							text: 'Nombre de tranches',
							width: 120,
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'deductionMensuelle',
							text: 'Déduction mensuelle',
							width: 120,
							renderer: value => Gprh.ades.util.Globals.formatRenderNumber(value),
						},
						{
							xtype: 'gridcolumn',
							dataIndex: 'enCours',
							text: 'En cours',
							width: 120,
							renderer: value => (value ? 'Oui' : ''),
						},
						{
							xtype: 'actioncolumn',
							cls: 'content-column',
							width: 80,
							text: 'Actions',
							items: [
								{
									iconCls: 'x-fa fa-trash soft-red-small',
									handler: 'confirmDeleteAv',
								},
							],					
						},
					],
				},
				dockedItems: [
					Gprh.ades.util.Globals.getPagingToolbarEnabled(this),
				],
				tbar: [],
			},
		];
		this.callParent();
	},
});
