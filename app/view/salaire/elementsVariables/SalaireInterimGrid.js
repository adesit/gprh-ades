Ext.define('Gprh.ades.view.salaire.elementsVariables.SalaireInterimGrid', {
	extend: 'Ext.grid.Panel',
	xtype: 'salaireInterimGrid',

	requires: [
		'Gprh.ades.util.Globals',
	],

	initComponent() {
		this.store = null;

		this.cls = 'user-grid';
		this.selModel = 'rowmodel';
		this.height = 280;
		this.columnLines = true;
		this.stripeRows = true;
		this.columns = [
			{
				xtype: 'datecolumn',
				dataIndex: 'dateDebutInterim',
				text: 'Début de l\'interim',
				align: 'left',
				format: 'd/m/Y',
				flex: 1,
			},
			{
				xtype: 'datecolumn',
				dataIndex: 'dateFinInterim',
				text: 'Fin de l\'interim',
				align: 'left',
				format: 'd/m/Y',
				flex: 1,
			},
			{
				xtype: 'actioncolumn',
				cls: 'content-column',
				width: 80,
				text: 'Actions',
				items: [
					{
						iconCls: 'x-fa fa-trash soft-red-small',
						handler: 'confirmDeleteSalaireInterim',
					},
				],					
			},
		];
		this.dockedItems = [
			{
				xtype: 'pagingtoolbar',
				dock: 'bottom',
				itemId: 'salaireInterimPaginationToolbar',
				displayInfo: false,
				store: this.storeParam,
			},
		];
		this.tbar = [
			'->',
			{
				text: 'Liste des périodes intérimaires',
				style: {
					background: 'transparent',
					border: 'none',
				},
			},
		];
		
		this.listeners = {
			rowclick: 'onSalaireInterimRowClick',
		};

		this.callParent();
	},
});
