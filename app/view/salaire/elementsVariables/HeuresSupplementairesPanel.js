Ext.define('Gprh.ades.view.salaire.elementsVariables.HeuresSupplementairesPanel', {
	extend: 'Ext.panel.Panel',

	xtype: 'heuresSupplementairesPanel',

	requires: [
		'Ext.layout.container.HBox',
		'Gprh.ades.view.salaire.SalaireController',
		'Gprh.ades.view.salaire.SalaireViewModel',
		'Gprh.ades.view.salaire.ContratGridMin',
		'Gprh.ades.view.salaire.elementsVariables.HeuresSupplementairesForm',
		'Gprh.ades.view.salaire.elementsVariables.HeuresSupplementairesGrid',
	],
	
	viewModel: {
		type: 'salaireViewModel',
	},

	controller: 'salaireController',
	
	itemId: 'heuresSupplementairesPanelId',

	layout: {
		type: 'hbox',
		pack: 'start',
		align: 'stretch',
	},

	bodyPadding: 1,

	defaults: {
		frame: false,
		bodyPadding: 1,
	},

	items: [
		{
			title: 'Liste de contrats',
			flex: 2,
			collapsible: true,
			collapseDirection: 'left',
			collapseMode: 'header',
			margin: '0 1 0 0',
			itemId: 'contratGridMinPanelId',
			items: [
				{
					xtype: 'contratGridMin',
					itemId: 'contratGridMinId',
					listeners: {
						select: 'onContratHSClick',
					},
				},
			],
		},
		{
			title: 'Formulaire d\'enregistrement',
			flex: 1,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'heuresSupplementairesFormPanelId',
			items: [
				{ xtype: 'heuresSupplementairesForm' },
			],
		},
		{
			title: 'Liste des HS',
			flex: 1,
			collapsed: true,
			collapsible: true,
			collapseDirection: 'right',
			collapseMode: 'header',
			itemId: 'heuresSupplementairesGridPanelId',
			items: [
				{
					xtype: 'heuresSupplementaireGrid',
					itemId: 'heuresSupplementaireGridId',
				},
			],
			listeners: {
				expand: (self) => {
					self.up().down('#contratGridMinPanelId').collapse(true);
					self.up().down('#heuresSupplementairesFormPanelId').collapse(true);
				},
				collapse: (self) => {
					self.up().down('#contratGridMinPanelId').expand(true);
					self.up().down('#heuresSupplementairesFormPanelId').expand(true);
				},
			},
		},
	],

});
