Ext.define('Gprh.ades.view.salaire.elementsVariables.HeuresSupplementairesForm', {
	extend: 'Ext.form.Panel',

	xtype: 'heuresSupplementairesForm',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.util.VTypes',
	],

	colorScheme: 'soft-green',
	bodyPadding: 5,

	initComponent() {
		const form = {
			xtype: 'form',
			itemId: 'HSFormId',
			layout: {
				type: 'vbox',
				align: 'stretch',
			},
			listeners: {				
				beforeadd: () => {
					const me = this;
					const scroller = me.getScrollable();
					if (scroller) {
						me.savedScrollPos = scroller.getPosition();
					}
				},
				afterLayout: () => {
					const grid = this.up('#heuresSupplementairesPanelId').down('#contratGridMinId');
					
					this.up('#heuresSupplementairesPanelId').down('#contratGridMinId').getView().restoreScrollState(grid.view.scrollState.top);
				},
			},
			items: [
				{
					xtype: 'combobox',
					labelAlign: 'top',
					labelSeparator: ':',
					maxWidth: 200,
					fieldLabel: 'Mois salarial',
					name: 'moisSalaire',
					itemId: 'moisSalaireId',
					reference: 'moisFieldReference',
					publishes: 'value',
					bind: {
						store: '{salaireResults}',
					},
					queryMode: 'local',
					displayField: 'mois',
					valueField: 'idSalaire',
					renderTo: Ext.getBody(),
					allowBlank: false, 
					forceSelection: true,
					editable: false,
					tpl: Ext.create('Ext.XTemplate',
						'<ul class="x-list-plain"><tpl for=".">',
						'<tpl if="clotureSalaire != 1">',
						'<li role="option" class="x-boundlist-item">{mois}</li>',
						'<tpl else>',
						// @todo Use a dedicated class instead of style
						'<li role="option" class="x-boundlist-item" style="cursor: not-allowed;color: orange;">{mois}</li>',
						'</tpl>',
						'</tpl></ul>'),
					listeners: {
						// Evite la sélection des mois de salaire déjà clôturés
						beforeselect: (combo, record) => record.get('clotureSalaire') !== '1',
					},
				},
				{
					layout: 'column',
					margin: '0 5 0 0',
					items: [
						{
							defaultType: 'textfield',
							defaults: {
								labelWidth: 150,
								labelAlign: 'top',
								labelSeparator: ':',
								submitEmptyText: false,
								width: 125,
								listeners: {
									blur: self => Gprh.ades.util.Globals.formatNumber(self),
								},
								vtype: 'decimal',
								msgTarget: 'side',
							},
							columnWidth: '0.50',
							items: [
								{
									fieldLabel: 'ID_HS',
									name: 'idHeureSupplementaire',
									reference: 'idHeureSupplementaireRef',
									publishes: 'value',
									itemId: 'idHeureSupplementaireId',
									hidden: true,
								},
								{
									fieldLabel: 'ID_CONTRAT',
									name: 'idContrat',
									reference: 'idContratRef',
									publishes: 'value',
									itemId: 'idContratId',
									hidden: true,
								},
								{
									fieldLabel: '<8h (130%)',
									name: 'infHuitHeures',
									reference: 'infHuitHeuresRef',
									publishes: 'value',
									itemId: 'infHuitHeuresId',
								},
								{
									fieldLabel: '>8h (150%)',
									name: 'supHuitHeures',
									reference: 'supHuitHeuresRef',
									publishes: 'value',
									itemId: 'supHuitHeuresId',
								},
								{
									fieldLabel: 'Nuit habituelle',
									name: 'nuitHabituelle',
									reference: 'nuitHabituelleRef',
									publishes: 'value',
									itemId: 'nuitHabituelleId',
									disabled: true,
								},
							],
						},
						{
							defaultType: 'textfield',
							defaults: {
								labelWidth: 150,
								labelAlign: 'top',
								labelSeparator: ':',
								submitEmptyText: false,
								width: 125,
								listeners: {
									blur: self => Gprh.ades.util.Globals.formatNumber(self),
								},
							},
							columnWidth: '0.50',
							items: [
								{
									fieldLabel: 'Nuit occasionnelle',
									name: 'nuitOccasionnelle',
									reference: 'nuitOccasionnelleRef',
									publishes: 'value',
									itemId: 'nuitOccasionnelleId',
								},
								{
									fieldLabel: 'Travail dimanche',
									name: 'dimanche',
									reference: 'dimancheRef',
									publishes: 'value',
									itemId: 'dimancheId',
								},
								{
									fieldLabel: 'Travail le jour férié',
									name: 'ferie',
									reference: 'ferieRef',
									publishes: 'value',
									itemId: 'ferieId',
								},
							],
						},
					], 
				},
				{
					xtype: 'textfield',
					labelWidth: 150,
					labelAlign: 'top',
					labelSeparator: ':',
					width: 325,
					fieldLabel: 'Remarques ou motifs',
					name: 'remarqueMotifHs',
					itemId: 'remarqueMotifHsId',
				},
			],
			
		};

		this.items = [
			{
				html: '<span class="x-fa fa-info-circle"></span> Sélectionnez une ligne dans la grille principale, choisissez le mois salarial correspondant.</br>Le séparateur décimal à utiliser est le "."',
				itemId: 'helperId',
				baseCls: 'x-toast-info',
				anchor: '100%',
			},
			form,
			{
				xtype: 'toolbar',
				cls: 'wizard-form-break',
				defaults: {
					flex: 1,
				},
				width: 300,
				layout: 'hbox',
				items: [
					{
						itemId: 'HSFormSaveBtn',
						xtype: 'button',
						text: 'Enregistrer',
						ui: 'soft-green-small',
						margin: '10 10 0 0',
						bind: {
							disabled: '{isEnabled}',
						},
						listeners: {
							click: 'onHSFormSaveClick',
						},
					},
					{
						itemId: 'HSFormCancelBtn',
						xtype: 'button',
						text: 'Annuler',
						ui: 'soft-blue-small',
						margin: '10 0 0 0',
						listeners: {
							click: 'cancelAction',
						},
						params: 'HSFormId',
					},
				],
			},
			{
				xtype: 'toolbar',
				cls: 'wizard-form-break',
				defaults: {
					flex: 1,
				},
				layout: 'hbox',
				items: [
					{
						itemId: 'HSListBtn',
						xtype: 'button',
						text: 'Employé',
						tooltip: 'Liste des HSup par employé',
						ui: 'soft-purple',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onHSEmployeListClick',
						},
						bind: {
							disabled: '{isEnabledEmploye}',
						},
					},
					{
						itemId: 'HSAllListBtn',
						xtype: 'button',
						text: 'Toutes',
						tooltip: 'Liste de toutes les HSup',
						ui: 'soft-purple-small',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onHSAllListClick',
						},
					},
					{
						itemId: 'HSMoisListBtn',
						xtype: 'button',
						text: 'Mois',
						tooltip: 'Liste de toutes les HSup d\'un mois',
						ui: 'soft-purple-small',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onHSMoisListClick',
						},
						bind: {
							disabled: '{isEnabledMois}',
						},
					},
				],
			},
		];

		this.callParent();
	},
});
