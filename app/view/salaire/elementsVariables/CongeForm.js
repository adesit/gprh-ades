Ext.define('Gprh.ades.view.salaire.elementsVariables.CongeForm', {
	extend: 'Ext.form.Panel',

	xtype: 'congeForm',

	requires: [
		'Gprh.ades.util.Globals',
		'Gprh.ades.util.VTypes',
	],

	colorScheme: 'soft-green',
	bodyPadding: 5,

	initComponent() {
		const form = {
			xtype: 'form',
			itemId: 'CngFormId',
			layout: {
				type: 'vbox',
				align: 'stretch',
			},
			defaults: {
				labelWidth: 150,
				labelAlign: 'top',
				labelSeparator: ':',
				submitEmptyText: false,
			},
			listeners: {				
				beforeadd: () => {
					const me = this;
					const scroller = me.getScrollable();
					if (scroller) {
						me.savedScrollPos = scroller.getPosition();
					}
				},
				afterLayout: () => {
					const grid = this.up('#congePanelId').down('#contratGridMinId');
					
					this.up('#congePanelId').down('#contratGridMinId').getView().restoreScrollState(grid.view.scrollState.top);
				},
			},
			items: [
				{
					xtype: 'combobox',
					maxWidth: 200,
					fieldLabel: 'Mois salarial',
					name: 'idSalaire',
					itemId: 'idSalaireId',
					reference: 'moisFieldReference',
					publishes: 'value',
					disabled: true,
					bind: {
						store: '{salaireResults}',
						disabled: '{!isEnabledCngCmp}',
					},
					queryMode: 'local',
					displayField: 'mois',
					valueField: 'idSalaire',
					renderTo: Ext.getBody(),
					allowBlank: false, 
					forceSelection: true,
					editable: false,
					tpl: Ext.create('Ext.XTemplate',
						'<ul class="x-list-plain"><tpl for=".">',
						'<tpl if="clotureSalaire != 1">',
						'<li role="option" class="x-boundlist-item">{mois}</li>',
						'<tpl else>',
						// @todo Use a dedicated class instead of style
						'<li role="option" class="x-boundlist-item" style="cursor: not-allowed;color: orange;">{mois}</li>',
						'</tpl>',
						'</tpl></ul>'),
					listeners: {
						// Evite la sélection des mois de salaire déjà clôturés
						beforeselect: (combo, record) => record.get('clotureSalaire') !== '1',
					},
				},
				{
					xtype: 'radiogroup',
					labelAlign: 'top',
					fieldLabel: 'Type de congé',
					itemId: 'natureCongeId',
					disabled: true,
					bind: {
						value: '{switchItem}',
						disabled: '{!isEnabledCngCmp}',
					},
					cls: 'x-check-group-alt',
					items: [
						{
							boxLabel: 'Congé de maternité',
							name: 'natureConge',
							inputValue: 1,
						}, {
							boxLabel: 'Congé payé', 
							name: 'natureConge',
							inputValue: 2,
							checked: true,
						},
					],
				},
				{
					xtype: 'textfield',
					fieldLabel: 'ID_CONGE',
					name: 'idConge',
					reference: 'idCongeRef',
					publishes: 'value',
					itemId: 'idCongeId',
					hidden: true,
				},
				{
					xtype: 'textfield',
					fieldLabel: 'ID_CONTRAT',
					name: 'idContrat',
					reference: 'idContratRef',
					publishes: 'value',
					itemId: 'idContratId',
					hidden: true,
					allowBlank: false,
				},
				{
					layout: 'column',
					items: [
						{
							columnWidth: '0.50',
							items: [
								{
									xtype: 'datefield',
									name: 'dateDebutConge',
									itemId: 'dateDebutCongeId',
									reference: 'dateDebutCongeRef',
									publishes: 'value',
									labelAlign: 'top',
									fieldLabel: 'Début de congé',
									hidden: false,
									format: 'd/m/Y',
									submitFormat: 'Y-m-d',
									width: 140,
									allowBlank: false,
									disabled: true,
									bind: {
										disabled: '{!isEnabledCngCmp}',
									},
									listeners: {
										change: 'refreshDureeSoldeConge',
									},
								},
								{
									xtype: 'datefield',
									name: 'dateFinConge',
									itemId: 'dateFinCongeId',
									reference: 'dateFinCongeRef',
									publishes: 'value',
									labelAlign: 'top',
									fieldLabel: 'Fin de congé',
									hidden: false,
									format: 'd/m/Y',
									submitFormat: 'Y-m-d',
									width: 140,
									disabled: true,
									bind: {
										value: '{materinteFinCng}',
										disabled: '{!isEnabledCngCmp}',
									},
									listeners: {
										change: 'refreshDureeSoldeConge',
									},
									allowBlank: false,
								},
							],
						},
						{
							columnWidth: '0.50',
							items: [
								{
									xtype: 'textfield',
									labelAlign: 'top',
									fieldLabel: 'Durée (jours)(décimal)',
									name: 'dureeCongePrise',
									reference: 'dureeCongePriseRef',
									publishes: 'value',
									itemId: 'dureeCongePriseId',
									width: 145,
									vtype: 'decimal',
									msgTarget: 'side',
									allowBlank: false,
									disabled: true,
									bind: {
										disabled: '{!isEnabledCngCmp}',
									},
									listeners: {
										change: 'refreshDureeSoldeCongeAfterEdit',
									},
								},
								{
									xtype: 'textfield',
									itemId: 'reliquatId2',
									width: 50,
									editable: false,
									hidden: true,
									bind: {
										value: '{reliquatRef2}',
									},
								},
								{
									xtype: 'displayfield',
									labelAlign: 'top',
									fieldLabel: 'Solde de congé',
									name: 'reliquat',
									bind: {
										value: '{reliquatRef}',
									},
									publishes: 'value',
									itemId: 'reliquatId',
									width: 145,
									baseCls: 'x-toast-success',
									fieldStyle: 'font-weight: bold;',
								},
							],
						},
					],
				},
				{
					xtype: 'textfield',
					labelWidth: 150,
					labelAlign: 'top',
					labelSeparator: ':',
					width: 325,
					fieldLabel: 'Remarques ou motifs',
					name: 'remarqueConge',
					itemId: 'remarqueCongeId',
				},
				{
					xtype: 'toolbar',
					cls: 'wizard-form-break',
					defaults: {
						flex: 1,
					},
					width: 300,
					layout: 'hbox',
					items: [
						{
							itemId: 'CngFormSaveBtn',
							xtype: 'button',
							text: 'Enregistrer',
							ui: 'soft-green-small',
							margin: '10 10 0 0',
							formBind: true,
							listeners: {
								click: 'onCngFormSaveClick',
							},
						},
						{
							itemId: 'CngFormCancelBtn',
							xtype: 'button',
							text: 'Annuler',
							ui: 'soft-blue-small',
							margin: '10 0 0 0',
							listeners: {
								click: 'cancelAction',
							},
							params: 'CngFormId',
						},
					],
				},
			],
			
		};

		this.items = [
			{
				html: '<span class="x-fa fa-info-circle"></span> Sélectionnez une ligne dans la grille principale, choisissez le mois salarial correspondant.',
				itemId: 'helperId',
				baseCls: 'x-toast-info',
				anchor: '100%',
			},
			form,
			{
				xtype: 'toolbar',
				cls: 'wizard-form-break',
				defaults: {
					flex: 1,
				},
				layout: 'hbox',
				items: [
					{
						itemId: 'CngListBtn',
						xtype: 'button',
						text: 'Employé',
						tooltip: 'Liste des congés par employé',
						ui: 'soft-purple',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onCngEmployeListClick',
						},
						bind: {
							disabled: '{isEnabledEmploye}',
						},
					},
					{
						itemId: 'CngAllListBtn',
						xtype: 'button',
						text: 'Tous',
						tooltip: 'Liste de tous les congés',
						ui: 'soft-purple-small',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onCngAllListClick',
						},
					},
					{
						itemId: 'CngMoisListBtn',
						xtype: 'button',
						text: 'Mois',
						tooltip: 'Liste de tous les congés d\'un mois',
						ui: 'soft-purple-small',
						iconCls: 'fa fa-list',
						margin: '10 10 0 0',
						listeners: {
							click: 'onCngMoisListClick',
						},
						bind: {
							disabled: '{isEnabledMois}',
						},
					},
				],
			},
		];

		this.callParent();
	},
});
