Ext.define('Gprh.ades.view.salaire.SalaireGrid', {
	extend: 'Ext.panel.Panel',
	xtype: 'salaireGrid',
	itemId: 'salairePanelId',

	requires: [
		'Gprh.ades.util.VTypes',
		'Gprh.ades.model.SalaireModel',
	],

	mixins: [
		'Gprh.ades.util.RequestCapable',
	],

	controller: 'salaireController',

	initComponent() {
		const rowEditing = Gprh.ades.util.Globals.setRowEditingConfig('server-scripts/salaire/Salaire.php', this);

		const salaireGrid = {
			xtype: 'gridpanel',
			itemId: 'salaireGridId',
			reference: 'salaireGridRef',
			loadMask: true,
			bind: {
				store: '{salaireResults}',
			},
			viewConfig: {
				preserveScrollOnRefresh: true,
				stripeRows: true,
			},
			selModel: 'rowmodel',
			height: Ext.Element.getViewportHeight() - 125,
			columnLines: true,
			columns: {
				defaults: {
					align: 'left',
				},
				items: [
					{
						xtype: 'gridcolumn',
						dataIndex: 'idSalaire',
						text: '#',
						hidden: true,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'moisSeulement',
						text: 'Mois',
						width: 100,
						editor: {
							xtype: 'combobox',
							store: 'moisStore',
							queryMode: 'local',
							displayField: 'displayText',
							valueField: 'moisSalaire',
							renderTo: Ext.getBody(),
							allowBlank: false, 
							forceSelection: true,
							editable: false,
						},
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'anneeSalaire',
						text: 'Année',
						width: 75,
						editor: {
							allowBlank: false,
							vtype: 'year',
							msgTarget: 'side',
						},
					},
					{
						xtype: 'datecolumn',
						dataIndex: 'dateClotureSalaire',
						text: 'Clôturé',
						format: 'd/m/Y',
						width: 95,
					},
					{
						xtype: 'gridcolumn',
						dataIndex: 'clotureSalaire',
						text: 'Statut',
						width: 90,
						renderer: value => this.statusSalaire(value),
					},
				],
			},
			dockedItems: [
				Gprh.ades.util.Globals.getSimplePagingToolbar(this),
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							tooltip: 'Préparer un nouveau mois de salaire',
							iconCls: 'fa fa-plus',
							text: 'Préparer',
							itemId: 'addSalaireGridBtnId',
							margin: '2 2 0 2',
							handler: () => {
								rowEditing.cancelEdit();
												
								const r = Ext.create('Gprh.ades.model.SalaireModel', {
									idSalaire: '',
									moisSalaire: '',
									anneeSalaire: 0,
								});
								
								this.down('#salaireGridId').getStore().insert(0, r);
								rowEditing.startEdit(0, 0);
							},
							params: {
								gridId: 'salaireGridId',
								editOrCreate: 'create',
							},
						},
						{
							tooltip: 'Supprimer un mois de salaire',
							iconCls: 'fa fa-trash',
							text: 'Supprimer',
							ui: 'soft-red-small',
							itemId: 'deleteSalaireGridBtnId',
							margin: '2 2 0 0',
							listeners: {
								click: 'onDeleteSalaireClick',
							},
							params: {
								gridId: 'salaireGridId',
							},
							bind: { 
								disabled: '{!salaireGridRef.selection}',
							},
						},
					],
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							tooltip: 'Calculer le salaire d\'un mois',
							iconCls: 'x-fa fa-calculator',
							text: 'Calculer',
							ui: 'blue',
							itemId: 'runSalaireGridBtnId',
							margin: '2 2 0 2',
							listeners: {
								click: 'onRunSalaireClick',
							},
							params: {
								gridId: 'salaireGridId',
							},
							disabled: true,
							bind: { 
								disabled: '{salaireGridRef.selection.clotureSalaire == 1}',
							},
						},
						{
							tooltip: 'Exporter les différences',
							iconCls: 'x-fa fa-file-excel-o',
							text: 'Différences',
							ui: 'green',
							itemId: 'diffSalaireGridBtnId',
							margin: '2 2 0 2',
							listeners: {
								click: 'onDiffSalaireXlsxClick',
							},
							params: {
								gridId: 'salaireGridId',
							},
							disabled: true,
							bind: { 
								disabled: '{salaireGridRef.selection.clotureSalaire == 1}',
							},
						},
						{
							tooltip: 'Valider et clôturer un mois de salaire',
							iconCls: 'x-fa fa-save',
							text: 'Clôturer',
							ui: 'soft-green',
							itemId: 'closeSalaireGridBtnId',
							margin: '2 2 0 0',
							listeners: {
								click: 'onCloseSalaireClick',
							},
							params: {
								gridId: 'salaireGridId',
							},
							disabled: true,
							bind: { 
								disabled: '{!isEnabledSalaireBtn}',
							},
						},
					],
				},
				{
					xtype: 'toolbar',
					dock: 'top',
					items: [
						{
							tooltip: 'Exporter en Excel les états de paie',
							iconCls: 'x-fa fa-file-excel-o',
							text: 'Etats de paie',
							ui: 'soft-green',
							itemId: 'printEtatPaieXlsxBtnId',
							margin: '2 2 0 2',
							listeners: {
								click: 'onPrintXlsxEtatPaieClick',
							},
							params: {
								gridId: 'salaireGridId',
							},
							disabled: true,
							bind: { 
								disabled: '{salaireGridRef.selection.clotureSalaire != 1}',
							},
						},
						{
							tooltip: 'Editer et imprimer les fiches de paie',
							text: 'Fiches de paie',
							iconCls: 'x-far fa-file-pdf',
							ui: 'soft-purple',
							itemId: 'printSalaireGridBtnId',
							margin: '2 2 0 0',
							listeners: {
								click: 'onFDPpdfClick',
							},
							params: {
								gridId: 'salaireGridId',
							},
							disabled: true,
							bind: { 
								disabled: '{salaireGridRef.selection.clotureSalaire != 1}',
							},
						},
					],
				},
			],
			bbar: {
				items: [
					{
						tooltip: 'Exporter en Excel le salaire',
						iconCls: 'x-fa fa-file-excel-o',
						text: 'Salaire',
						ui: 'soft-green',
						itemId: 'printSalaireGridXlsxBtnId',
						margin: '2 2 0 2',
						listeners: {
							click: 'onPrintXlsxSalaireClick',
						},
						params: {
							gridId: 'salaireGridId',
						},
						disabled: true,
						bind: { 
							disabled: '{salaireGridRef.selection.clotureSalaire != 1}',
						},
					},
					{
						tooltip: 'Exporter en Excel les changements de catégorie',
						iconCls: 'x-fa fa-file-excel-o',
						text: 'N.lle catégorie',
						ui: 'soft-green',
						itemId: 'printChangeCategorieXlsxBtnId',
						margin: '2 2 0 0',
						listeners: {
							click: 'onPrintXlsxChangeCategorieClick',
						},
						params: {
							gridId: 'salaireGridId',
						},
						disabled: true,
						bind: { 
							disabled: '{salaireGridRef.selection.clotureSalaire != 1}',
						},
					},
				],
			},
			plugins: [rowEditing],
			listeners: {
				rowclick: 'onSalaireRowClick',
			},
		};

		this.items = [
			salaireGrid,
		];

		this.callParent();
	},

	statusSalaire(value) {
		const status = (value === '1') ? 'Clôturée' : 'En cours';
		return status;
	},
});
