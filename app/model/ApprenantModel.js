Ext.define('Gprh.ades.model.ApprenantModel', {
	extend: 'Ext.data.Model',
	alias: 'ApprenantModel',
	fields: [
		'idApprenant',
		'nomApprenant',
		'prenomApprenant',
		{ name: 'dateNaissanceApprenant', type: 'date', dateFormat: 'n/j/Y' },
		'sexeApprenant',
		'telephoneApprenant',
		'adresseCompleteApprenant',
		'niveau',
		'typeRelationAdes',
		'idEmploye',
	],
});
