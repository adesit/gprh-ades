Ext.define('Gprh.ades.model.PosteModel', {
	extend: 'Ext.data.Model',
	alias: 'PosteModel',
	fields: [
		'idPoste',
		'idGroupeCategorie',
		'categorieProfessionnelle',
		'titrePoste',
		'descriptionPoste',
		'nomGroupe',
		'categorieProfessionnelle',
	],
});
