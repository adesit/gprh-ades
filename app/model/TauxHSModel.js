Ext.define('Gprh.ades.model.TauxHSModel', {
	extend: 'Ext.data.Model',
	alias: 'TauxHSModel',
	fields: [
		'idHsBase',
		'natureHsBase',
		{ name: 'formuleCalcul', type: 'float' },
		{ name: 'dateEffetHs', type: 'date', dateFormat: 'n/j/Y' },
		{ name: 'actifHsBase', type: 'bool' },
	],
});
