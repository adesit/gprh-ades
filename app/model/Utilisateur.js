Ext.define('Gprh.ades.model.UtilisateurModel', {
	extend: 'Ext.data.Model',
	alias: 'UtilisateurModel',
	fields: [
		'idUtilisateur',
		'nomUtilisateur',
		'motDePasse',
		'centreAffectation',
		'typeUtilisateur',
		'majMotDePasse',
		'avatar',
	],
});
