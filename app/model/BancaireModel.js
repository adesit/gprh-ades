Ext.define('Gprh.ades.model.BancaireModel', {
	extend: 'Ext.data.Model',
	alias: 'BancaireModel',
	fields: [
		'idBancaire',
		'idEmploye',
		'nomBanque',
		'codeBanque',
		'numeroCompte',
		'codeGuichet',
		'cleRib',
		{ name: 'datePriseEffetBanque', type: 'date', dateFormat: 'n/j/Y' },
		{ name: 'actifBancaire', type: 'bool' },
	],
});
