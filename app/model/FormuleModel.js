Ext.define('Gprh.ades.model.FormuleModel', {
	extend: 'Ext.data.Model',
	alias: 'FormuleModel',
	fields: [
		'idFormule',
		'idAdes',
		'resultat',
		'formuleCalcul',
		{ name: 'dateApplication', type: 'date', dateFormat: 'n/j/Y' },
		{ name: 'actifFormule', type: 'bool' },
		'descriptionFormule',
	],
});
