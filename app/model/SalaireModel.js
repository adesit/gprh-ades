Ext.define('Gprh.ades.model.SalaireModel', {
	extend: 'Ext.data.Model',
	alias: 'SalaireModel',
	fields: [
		'idSalaire',
		{ name: 'moisSalaire', type: 'int' },
		{ name: 'anneeSalaire', type: 'int' },
	],
});
