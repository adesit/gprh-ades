Ext.define('Gprh.ades.model.SectionModel', {
	extend: 'Ext.data.Model',
	alias: 'SectionModel',
	fields: [
		'idSection',
		'nomSection',
	],
});
