// Structure d'un Centre, composée de 4 champs
Ext.define('Gprh.ades.model.CentreModel', {
	extend: 'Ext.data.Model',
	alias: 'CentreModel',
	fields: [
		{ name: 'idCentre', type: 'integer' },
		{ name: 'abreviationCentre', type: 'string' },
		{ name: 'nomCentre', type: 'string' },
		{ name: 'adresseCentre', type: 'string' },
	],
});
