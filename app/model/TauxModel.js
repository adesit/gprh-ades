Ext.define('Gprh.ades.model.TauxModel', {
	extend: 'Ext.data.Model',
	alias: 'TauxModel',
	fields: [
		'idTaux',
		'nomTaux',
		{ name: 'valeurPourcent', type: 'float' },
		{ name: 'datePriseEffetTaux', type: 'date', dateFormat: 'n/j/Y' },
		{ name: 'actifTaux', type: 'bool' },
	],
});
