/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */

Ext.define('Gprh.ades.Application', {
	extend: 'Ext.app.Application',

	name: 'Gprh.ades',
	appFolder: 'app',

	quickTips: false,
	platformConfig: {
		desktop: {
			quickTips: true,
		},
	},

	stores: [
		'Centre',
	],

	requires: [
		// 'Gprh.ades.view.main.Main', 
		// si commentée, on gagne de vitesse pendant le chargement de Login + (mais) le XHR contient tous les fichiers js et php + (mais) le filtre de grids ne marche pas
		// si non la vitesse de chargement au démarrage de Login est lente + le XHR contient seulement les requêtes HTTPRequest 
	],

	launch() {
		Ext.enableAria = false;
		Ext.enableAriaButtons = false;
		Ext.enableAriaPanels = false;
		// Check to see the current value of the localStorage key

		if (Ext.get('page-loader')) {
			Ext.get('page-loader').hide();
		}
		
		const loggedIn = Ext.util.LocalStorage.get('foo').getItem('LoggedIn');

		if (loggedIn) {
			const idCentre = Ext.util.LocalStorage.get('idCentre');
			const typeUtilisateur = Ext.util.LocalStorage.get('typeUtilisateur');
			idCentre.release();
			typeUtilisateur.release();
			Ext.syncRequire('Gprh.ades.view.main.Main', () => Ext.widget('app-main', {}), this); // === requires
			// Ext.create({ xtype: 'app-main' });
			// Ext.widget('app-main', {});
		} else {
			Ext.require('Gprh.ades.view.login.Login', () => Ext.widget('login', {}), this);
			// Ext.create({ xtype: 'login' });
		}
	},

	onAppUpdate() {
		Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
			(choice) => {
				if (choice === 'yes') {
					window.location.reload();
				}
			});
	},
});
