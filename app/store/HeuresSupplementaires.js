Ext.define('Gprh.ades.store.HeuresSupplementaires', {
	extend: 'Ext.data.Store',
	
	storeId: 'heuresSupplementairesStore',
	alias: 'store.heuresSupplementairesStore',
	loading: true,
	
	fields: [
		'idHeureSupplementaire',
		'idContrat',
		'infHuitHeures',
		'supHuitHeures',
		'nuitHabituelle',
		'nuitOccasionnelle',
		'dimanche',
		'ferie',
		'remarqueMotifHs',
		'moisSalaire',
		'matricule',
		'mois',
		'annee',
		{
			name: 'moisAnnee',
			convert(v, rec) {
				const moisStore = Ext.getStore('moisStore');
				const findMois = moisStore.findRecord('moisSalaire', rec.get('mois'));
				return `${findMois.data.displayText} - ${rec.get('annee')}`;
			},
		},
		'nom',
		'prenom',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/HeuresSupplementaires.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
