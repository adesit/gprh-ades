Ext.define('Gprh.ades.store.Rappel', {
	extend: 'Ext.data.Store',
	
	storeId: 'rappelStore',
	alias: 'store.rappelStore',
	loading: true,
	
	fields: [
		'idRappel',
		'idContrat',
		'idSalaire',
		'montantRappel',
		'nombreMois',
		'dateDebutRappel',
		'dateFinRappel',
		'remarqueRappel',
		'typeRappel',
		'nom',
		'prenom',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Rappel.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
