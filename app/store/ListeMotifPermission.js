Ext.define('Gprh.ades.store.ListeMotifPermission', {
	extend: 'Ext.data.Store',
	
	storeId: 'listeMotifPermissionStore',
	alias: 'store.listeMotifPermissionStore',
	loading: true,
	
	fields: ['motif'],
	data: [
		{ motif: 'Mariage du travailleur - 4 jours' },
		{ motif: 'Mariage d\'un enfant du travailleur - 4 jours' },
		{ motif: 'Naissance d\'un enfant du travailleur - 3 jours' },
		{ motif: 'Circoncision d\'un enfant du travailleur - 2 jours' },
		{ motif: 'Décès dans la famille proche(soeur, frère, parent, beau parent) du travailleur - 3 jours' },
		{ motif: 'Hospitalisation de la conjointe ou des enfants du travailleur - 3 jours' },
		{ motif: 'Déménagement - 2 jours' },
		{ motif: 'Affaire administrative (enquête, témoignage) - 1 jours' },
		{ motif: 'Examen officiel du travailleur - 3 jours' },
		{ motif: 'Réunion des parents - 0.5 jour' },
		{ motif: 'Cérémonie fin d\'année scolaire - 0.5 jour' },
	],
	autoLoad: true,
});
