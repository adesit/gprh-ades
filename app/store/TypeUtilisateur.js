Ext.define('Gprh.ades.store.TypeUtilisateur', {
	extend: 'Ext.data.Store',
	
	storeId: 'typeUtilisateurStore',
	alias: 'store.typeUtilisateurStore',
	loading: true,
	
	fields: ['typeUtilisateur', 'displayText'],
	data: [
		{ typeUtilisateur: '1', displayText: 'Administrateur' },
		{ typeUtilisateur: '2', displayText: 'Responsable de centre' },
		{ typeUtilisateur: '3', displayText: 'Responsable de formation' },
	],
	autoLoad: true,
});
