Ext.define('Gprh.ades.store.Salaire', {
	extend: 'Ext.data.Store',
	
	storeId: 'salaireStore',
	alias: 'store.salaireStore',
	loading: true,
	
	fields: [
		'idSalaire',
		'moisSalaire',
		'anneeSalaire',
		'typeSalaire',
		'dateClotureSalaire',
		'clotureSalaire',
		{
			name: 'mois',
			convert(v, rec) {
				const moisStore = Ext.getStore('moisStore');
				const findMois = moisStore.findRecord('moisSalaire', rec.get('moisSalaire'));
				return `${findMois.data.displayText} - ${rec.get('anneeSalaire')}`;
			},
		},
		{
			name: 'moisSeulement',
			convert(v, rec) {
				const moisStore = Ext.getStore('moisStore');
				const findMois = moisStore.findRecord('moisSalaire', rec.get('moisSalaire'));
				return findMois.data.displayText;
			},
		},
		{
			name: 'moisAnnee',
			convert(v, rec) {
				return `${rec.get('anneeSalaire')}-${rec.get('moisSalaire')}`;
			},
		},
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Salaire.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
		extraParams: {
			infoRequest: 1,
		},
	},
	autoLoad: true,
});
