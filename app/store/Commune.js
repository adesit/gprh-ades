Ext.define('Gprh.ades.store.Commune', {
	extend: 'Ext.data.Store',
	
	storeId: 'communeStore',
	alias: 'store.communeStore',
	loading: true,
	
	fields: [
		'idCommune',
		'idDistrict',
		'codeCommune',
		'nomCommune',
		'codeDistrict',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Commune.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
