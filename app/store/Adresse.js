Ext.define('Gprh.ades.store.Adresse', {
	extend: 'Ext.data.Store',
	
	storeId: 'adresseStore',
	alias: 'store.adresseStore',
	loading: true,
	
	fields: [
		'idFokotany',
		'idCommune',
		'idDistrict',
		'idRegion',
		'idProvince',
		'idEmploye',
		'idAdresse',
		'actifAdresse',
		'logementEmploye',
		'nomFokotany',
		'nomCommune',
		'nomDistrict',
		'nomRegion',
		'nomProvince',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Adresse.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
