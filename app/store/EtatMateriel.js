Ext.define('Gprh.ades.store.EtatMateriel', {
	extend: 'Ext.data.Store',
	
	storeId: 'etatMaterielStore',
	alias: 'store.etatMaterielStore',
	
	fields: ['etatMateriel', 'displayText'],
	data: [
		{ etatMateriel: '1', displayText: 'Neuf' },
		{ etatMateriel: '2', displayText: 'Occasion - Comme neuf' },
		{ etatMateriel: '3', displayText: 'Occasion - Très bon état' },
		{ etatMateriel: '4', displayText: 'Occasiont - Bon état' },
		{ etatMateriel: '5', displayText: 'Etat correct' },
		{ etatMateriel: '6', displayText: 'Incomplet' },
		{ etatMateriel: '7', displayText: 'Hors d\'état de marche' },
		{ etatMateriel: '8', displayText: 'Abîmé' },
	],
	autoLoad: true,
});