Ext.define('Gprh.ades.store.Province', {
	extend: 'Ext.data.Store',
	
	storeId: 'provinceStore',
	alias: 'store.provinceStore',
	loading: true,
	
	fields: [
		'idProvince',
		'codeProvince',
		'nomProvince',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Province.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
