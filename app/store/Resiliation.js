Ext.define('Gprh.ades.store.Resiliation', {
	extend: 'Ext.data.Store',
	
	storeId: 'resiliationStore',
	alias: 'store.resiliationStore',
	
	fields: ['resiliation', 'displayText'],
	data: [
		{ resiliation: '1', displayText: 'Démission' },
		{ resiliation: '2', displayText: 'Licenciement' },
		{ resiliation: '3', displayText: 'Chômage technique' },
	],
	autoLoad: true,
});
