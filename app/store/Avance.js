Ext.define('Gprh.ades.store.Avance', {
	extend: 'Ext.data.Store',
	
	storeId: 'avanceStore',
	alias: 'store.avanceStore',
	loading: true,
	
	fields: [
		'idAvance',
		'idContrat',
		'maxAutorise',
		'maxDeductionPossible',
		'montantAvance',
		'dateReception',
		'dateDebutRemboursement',
		'dateFinRemboursement',
		'nombreTranche',
		'deductionMensuelle',
		'raison',
		'enCours',
		'matricule',
		'nom',
		'prenom',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Avance.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
