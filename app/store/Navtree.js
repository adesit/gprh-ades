const privileges = [];
const ADESMenu = {
	text: 'ADES',
	iconCls: 'x-fa fa-home',
	viewType: 'adesMainContainer',
	routeId: 'ades', // routeId defaults to viewType
	leaf: true,
};
const PersonnelMenu = {
	text: 'Personnel',
	iconCls: 'x-fa fa-users',
	viewType: 'personnelMainContainer',
	routeId: 'personnel',
	leaf: true,
};
const ContratMenu = {
	text: 'Contrats de travail',
	iconCls: 'x-fa fa-file',
	viewType: 'contratMainContainer',
	routeId: 'contrat',
	leaf: true,
};
const SalaireMenu = {
	text: 'Salaire',
	iconCls: 'x-fa fa-eur',
	viewType: 'salaireMainContainer',
	routeId: 'salaire',
	leaf: true,
};
const SalaireInterimChildMenu = {
	text: 'Salaire intérim',
	iconCls: 'x-fa  fa-asterisk',
	viewType: 'salaireInterimPanel',
	routeId: 'salaireInterim',
	leaf: true,
};
const HSupChildMenu = {
	text: 'Heures supplémentaires',
	iconCls: 'x-fa fa-clock-o',
	viewType: 'heuresSupplementairesPanel',
	routeId: 'heuresSupplementaires',
	leaf: true,
};
const AbsencesPermissionChildMenu = {
	text: 'Absences et permissions',
	iconCls: 'x-fa fa-calendar-check-o',
	viewType: 'absencePanel',
	routeId: 'absence',
	leaf: true,
};
const CongeChildMenu = {
	text: 'Congés',
	iconCls: 'x-fa fa-calendar',
	viewType: 'congePanel',
	routeId: 'conge',
	leaf: true,
};
const AvanceChildMenu = {
	text: 'Avances',
	iconCls: 'x-fa fa-money',
	viewType: 'avancePanel',
	routeId: 'avance',
	leaf: true,
};
const DiversRetenusChildMenu = {
	text: 'Divers retenus',
	iconCls: 'x-fa fa-minus-circle',
	viewType: 'retenuPanel',
	leaf: true,
};
const DepassementTelephoniqueChildMenu = {
	text: 'Dépassements téléphoniques',
	iconCls: 'x-fa fa-tty',
	viewType: 'depassements',
	leaf: true,
};
const FraisScolariteChildMenu = {
	text: 'Frais de scolarité des enfants',
	iconCls: 'x-fa fa-university',
	viewType: 'scolarite',
	leaf: true,
};
const STCMenu = {
	text: 'Solde de tout compte',
	iconCls: 'x-fa fa-chain-broken',
	viewType: 'sdtc',
	leaf: true,
};
const RapportStatistiqueMenu = {
	text: 'Rapports et statistiques',
	iconCls: 'x-fa fa-pie-chart',
	viewType: 'statistique',
	leaf: true,
};
const SimulationMenu = {
	text: 'Simulations sur salaire',
	iconCls: 'x-fa fa-calculator',
	expanded: false,
	selectable: false,
	children: [
		{
			text: 'Simulation individuelle',
			iconCls: 'x-fa fa-user-plus',
			viewType: 'simulationIndividuelle',
			leaf: true,
		},
		{
			text: 'Augmentation massive',
			iconCls: 'x-fa fa-line-chart',
			viewType: 'augmentationMassive',
			leaf: true,
		},
		{
			text: 'Prévision annuelle',
			iconCls: 'x-fa fa-umbrella',
			viewType: 'previsionAnnuelle',
			leaf: true,
		},
	],
};
const EtatPaieMenu = {
	text: 'Etats de paie',
	iconCls: 'x-fa fa-external-link',
	viewType: 'etatsPaie',
	leaf: true,
};
const CompteUtilisateurMenu = {
	text: 'Comptes d\'utilisateurs',
	iconCls: 'x-fa fa-user-secret',
	viewType: 'utilisateurMainContainer',
	routeId: 'utilisateur',
	leaf: true,
};

if (window.localStorage.getItem('nomUtilisateur') === 1) { // Administrateur
	const ElementsVariablesMenu = [];
	privileges.push(ADESMenu);
	privileges.push(PersonnelMenu);
	privileges.push(ContratMenu);
	privileges.push(SalaireMenu);
	ElementsVariablesMenu.push(SalaireInterimChildMenu);
	ElementsVariablesMenu.push(HSupChildMenu);
	ElementsVariablesMenu.push(AbsencesPermissionChildMenu);
	ElementsVariablesMenu.push(CongeChildMenu);
	ElementsVariablesMenu.push(AvanceChildMenu);
	ElementsVariablesMenu.push(DiversRetenusChildMenu);
	ElementsVariablesMenu.push(DepassementTelephoniqueChildMenu);
	ElementsVariablesMenu.push(DepassementTelephoniqueChildMenu);
	privileges.push({
		text: 'Eléments variables',
		iconCls: 'x-fa fa-cogs',
		expanded: false,
		selectable: false,
		children: ElementsVariablesMenu,
	});
	privileges.push(STCMenu);
	privileges.push(RapportStatistiqueMenu);
	privileges.push(SimulationMenu);
	privileges.push(EtatPaieMenu);
	privileges.push(CompteUtilisateurMenu);
} else if (window.localStorage.getItem('nomUtilisateur') === 1) { // Directeur de centre
	const ElementsVariablesMenu = [];
	privileges.push(PersonnelMenu);
	privileges.push(ContratMenu);
	privileges.push(SalaireMenu);
	ElementsVariablesMenu.push(SalaireInterimChildMenu);
	ElementsVariablesMenu.push(HSupChildMenu);
	ElementsVariablesMenu.push(AbsencesPermissionChildMenu);
	ElementsVariablesMenu.push(CongeChildMenu);
	ElementsVariablesMenu.push(AvanceChildMenu);
	ElementsVariablesMenu.push(DiversRetenusChildMenu);
	ElementsVariablesMenu.push(DepassementTelephoniqueChildMenu);
	privileges.push({
		text: 'Eléments variables',
		iconCls: 'x-fa fa-cogs',
		expanded: false,
		selectable: false,
		children: ElementsVariablesMenu,
	});
	privileges.push(STCMenu);
	privileges.push(SimulationMenu);
	privileges.push(EtatPaieMenu);
}
