/**
 * Définit une collection de Centre qui contient 4 éléments
 * Les données seront les résultats d'une requête vers le serveur PHP
 * requête de type AJAX
 * résultats attendus de type JSON
 * données prises en charge à partir de la clé 'data'
 */
Ext.define('Gprh.ades.store.Centre', {
	extend: 'Ext.data.Store',
	
	storeId: 'centreStore',
	alias: 'store.centreStore',
	loading: true,
	
	fields: [
		'idCentre',
		'abreviationCentre',
		'nomCentre',
		'adresseCentre',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Centres.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
