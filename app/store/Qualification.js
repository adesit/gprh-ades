Ext.define('Gprh.ades.store.Qualification', {
	extend: 'Ext.data.Store',
	
	storeId: 'qualificationStore',
	alias: 'store.qualificationStore',
	loading: true,
	
	fields: [
		'idQualification',
		'idEmploye',
		'anneeExperience',
		'niveauFormation',
		'intituleDiplome',
		'ecoleUniversite',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Qualification.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
