Ext.define('Gprh.ades.store.DomaineFormation', {
	extend: 'Ext.data.Store',
	
	storeId: 'domaineFormationStore',
	alias: 'store.domaineFormationStore',
	loading: true,
	
	fields: ['domaineFormation', 'nomDomaineFormation'],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/DomaineFormation.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
