Ext.define('Gprh.ades.store.Foko', {
	extend: 'Ext.data.Store',
	
	storeId: 'fokoStore',
	alias: 'store.fokoStore',
	
	fields: [
		'idFoko',
		'nomFoko',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Foko.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
