Ext.define('Gprh.ades.store.Catalogue', {
	extend: 'Ext.data.Store',
	
	storeId: 'catalogueStore',
	alias: 'store.catalogueStore',
	
	fields: [
		'idSousTheme',
		'idFormateur',
		'titreSousTheme',
		'objectifsSousTheme',
		'nomFormateur',
		'prenomFormateur',
		'titreFonctionFormateur',
		'institution',
		'emailFormateur',
		'telephoneFormateur',
		'adresseCompleteFormateur',
		{
			name: 'nomPrenomFormateur',
			convert(v, rec) {
				return `${rec.get('nomFormateur')} ${rec.get('prenomFormateur')}`;
			},
		},
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Catalogue.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
	localFilter: true,
	listeners: {
		load: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			if (!Ext.isEmpty(toolbar)) {
				toolbar.onLoad();
			}
		},
		filterchange: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			store.reload();
			toolbar.onLoad();
		},
	},
});
