Ext.define('Gprh.ades.store.TypeFormation', {
	extend: 'Ext.data.Store',
	
	storeId: 'typeFormationStore',
	alias: 'store.typeFormationStore',
	loading: true,
	
	fields: ['typeFormation', 'nomTypeFormation'],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/TypeFormation.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
