Ext.define('Gprh.ades.store.Classe', {
	extend: 'Ext.data.Store',
	
	storeId: 'classeStore',
	alias: 'store.classeStore',
	loading: true,
	
	fields: [
		'idEducation',
		'rangClasse',
		'classe',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Classe.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
