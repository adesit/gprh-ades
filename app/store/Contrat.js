Ext.define('Gprh.ades.store.Contrat', {
	extend: 'Ext.data.Store',
	
	storeId: 'contratStore',
	alias: 'store.contratStore',
	
	fields: [
		'idContrat',
		'idAdes',
		'idCentre',
		'idPoste',
		'idSection',
		'idDepartement',
		'idCategorie',
		'idEmploye',
		'idSmie',
		'typeContrat',
		'natureContrat',
		'initialAvenant',
		'dateEmbauche',
		'datePrisePoste',
		'dateExpirationContrat',
		'horaire',
		'heureReglementaire',
		'actifContrat',
		'periodicitePaiement',
		'modePaiement',
		'salaireBase',
		'tauxHoraire',
		'motifResiliationContrat',
		'dateDebauche',
		'salaireBaseGrille',
		'matricule',
		'primeExceptionnelle',
		'nbEnfant',
		'nom',
		'prenom',
		'sexe',
		'titrePoste',
		'categorieProfessionnelle',
		'abreviationCentre',
		'categorieAnciennete',
		'categorieReelle',
		{
			name: 'nomPrenom',
			convert(v, rec) {
				return `${rec.get('nom')} ${rec.get('prenom')}`;
			},
		},
		'flotte',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Contrat.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
			totalProperty: 'total',
		},
	},
	autoLoad: true,
	localFilter: true,
	listeners: {
		load: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			toolbar.onLoad();
		},
		filterchange: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			store.reload();
			toolbar.onLoad();
		},
	},
});
