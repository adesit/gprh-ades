Ext.define('Gprh.ades.store.Depassement', {
	extend: 'Ext.data.Store',
	
	storeId: 'depassementStore',
	alias: 'store.depassementStore',
	loading: true,
	
	fields: [
		'idAppel',
		'idContrat',
		'idSalaire',
		'depassement',
		'remarqueDepassement',
		'matricule',
		'mois',
		'annee',
		{
			name: 'moisAnnee',
			convert(v, rec) {
				const moisStore = Ext.getStore('moisStore');
				const findMois = moisStore.findRecord('moisSalaire', rec.get('mois'));
				return `${findMois.data.displayText} - ${rec.get('annee')}`;
			},
		},
		'nom',
		'prenom',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Depassement.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
