Ext.define('Gprh.ades.store.Conge', {
	extend: 'Ext.data.Store',
	
	storeId: 'congeStore',
	alias: 'store.congeStore',
	loading: true,
	
	fields: [
		'idConge',
		'idContrat',
		'idSalaire',
		'remarqueConge',
		'dateDebutConge',
		'dateFinConge',
		'dureeCongePrise',
		'natureConge',
		'matricule',
		'mois',
		'annee',
		{
			name: 'moisAnnee',
			convert(v, rec) {
				const moisStore = Ext.getStore('moisStore');
				const findMois = moisStore.findRecord('moisSalaire', rec.get('mois'));
				return `${findMois.data.displayText} - ${rec.get('annee')}`;
			},
		},
		'reliquat',
		'nom',
		'prenom',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Conge.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
