Ext.define('Gprh.ades.store.Formation', {
	extend: 'Ext.data.Store',
	
	storeId: 'formationStore',
	alias: 'store.formationStore',
	
	fields: [
		'idSessionFormation',
		'themeFormation',
		'objectifsFormation',
		'dateCreationSession',
		'dateDebutFormation',
		'dateFinFormation',
		'domaineFormation',
		'DomaineFormation',
		'adresseSeance',
		'typeFormation',
		'TypeFormation',
		'niveauCible',
		'idSousTheme',
		'idSousThemeST',
		'titreSousTheme',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Formation.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
	localFilter: true,
	listeners: {
		load: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			if (!Ext.isEmpty(toolbar)) {
				toolbar.onLoad();
			}
		},
		filterchange: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			store.reload();
			toolbar.onLoad();
		},
	},
});
