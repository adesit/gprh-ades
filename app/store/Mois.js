Ext.define('Gprh.ades.store.Mois', {
	extend: 'Ext.data.Store',
	
	storeId: 'moisStore',
	alias: 'store.moisStore',
	
	fields: ['moisSalaire', 'displayText'],
	data: [
		{ moisSalaire: '1', displayText: 'Janvier' },
		{ moisSalaire: '2', displayText: 'Février' },
		{ moisSalaire: '3', displayText: 'Mars' },
		{ moisSalaire: '4', displayText: 'Avril' },
		{ moisSalaire: '5', displayText: 'Mai' },
		{ moisSalaire: '6', displayText: 'Juin' },
		{ moisSalaire: '7', displayText: 'Juillet' },
		{ moisSalaire: '8', displayText: 'Août' },
		{ moisSalaire: '9', displayText: 'Septembre' },
		{ moisSalaire: '10', displayText: 'Octobre' },
		{ moisSalaire: '11', displayText: 'Novembre' },
		{ moisSalaire: '12', displayText: 'Décembre' },
	],
	autoLoad: true,
});
