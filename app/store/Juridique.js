Ext.define('Gprh.ades.store.Juridique', {
	extend: 'Ext.data.Store',
	
	storeId: 'juridiqueStore',
	alias: 'store.juridiqueStore',
	loading: true,
	
	fields: [
		'idJuridique',
		'idEmploye',
		'natureCondamnation',
		'dateCondamnation',
		'dureeCondamnation',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Juridique.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
