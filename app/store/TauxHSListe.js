Ext.define('Gprh.ades.store.TauxHSListe', {
	extend: 'Ext.data.Store',
	
	storeId: 'tauxHSListeStore',
	alias: 'store.tauxHSListeStore',
	loading: true,
	
	fields: ['nomTaux', 'id'],
	data: [
		{ nomTaux: 'HSup < 8heures', id: 1 },
		{ nomTaux: 'HSup > 8heures', id: 2 },
		{ nomTaux: 'HSup pour des nuits habituelles', id: 3 },
		{ nomTaux: 'HSup pour des nuits occasionnelles', id: 4 },
		{ nomTaux: 'HSup pour des dimanches', id: 5 },
		{ nomTaux: 'HSup pour des jours fériés', id: 6 },
	],
	autoLoad: true,
});
