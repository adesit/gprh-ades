Ext.define('Gprh.ades.store.TauxHS', {
	extend: 'Ext.data.Store',
	
	storeId: 'tauxHSStore',
	alias: 'store.tauxHSStore',
	loading: true,
	
	fields: [
		'idHsBase',
		{
			name: 'natureHsBase',
			convert(v, rec) {
				const moisStore = Ext.create('Gprh.ades.store.TauxHSListe');
				const findMois = moisStore.findRecord('id', rec.get('natureHsBase'));
				return findMois.data.nomTaux;
			},
		},
		'formuleCalcul', 
		'dateEffetHs', 
		'actifHsBase',
		{
			name: 'typeTauxHS',
			convert(v, rec) {
				const moisStore = Ext.create('Gprh.ades.store.TauxHSListe');
				const findMois = moisStore.findRecord('nomTaux', rec.get('natureHsBase'));
				return findMois.data.id;			
			},
		},
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/TauxHS.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
