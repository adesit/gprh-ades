Ext.define('Gprh.ades.store.Evaluation', {
	extend: 'Ext.data.ArrayStore',
	
	storeId: 'evaluationStore',
	alias: 'store.evaluationStore',

	loading: true,
	
	proxy: {
		type: 'ajax',
		method: 'POST',
		url: 'server-scripts/listes/Evaluation.php',
		reader: {
			type: 'json',
			rootProperty: 'values',
		},
	},
	autoLoad: true,
});
