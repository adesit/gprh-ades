Ext.define('Gprh.ades.store.SalaireInterim', {
	extend: 'Ext.data.Store',
	
	storeId: 'salaireInterimStore',
	alias: 'store.salaireInterimStore',
	loading: true,
	
	fields: [
		'idPrimeIndemnite',
		'idContrat',
		'dateDebutInterim',
		'dateFinInterim',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/SalaireInterim.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
