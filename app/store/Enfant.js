Ext.define('Gprh.ades.store.Enfant', {
	extend: 'Ext.data.Store',
	
	storeId: 'enfantStore',
	alias: 'store.enfantStore',
	loading: true,
	
	fields: [
		'idEnfant',
		'idEmploye',
		'nomEnfant',
		'prenomEnfant',
		'dateNaissanceEnfant',
		'lieuNaissanceEnfant',
		'genreEnfant',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Enfant.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
