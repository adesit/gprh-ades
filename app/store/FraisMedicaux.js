Ext.define('Gprh.ades.store.FraisMedicaux', {
	extend: 'Ext.data.Store',
	
	storeId: 'fraisMedicauxStore',
	alias: 'store.fraisMedicauxStore',
	loading: true,
	
	fields: [
		'idMedicaux',
		'idContrat',
		'idSalaire',
		'dateMedicaux',
		'fraisMedicaux',
		'remarqueMotifMedical',
		'matricule',
		'mois',
		'annee',
		{
			name: 'moisAnnee',
			convert(v, rec) {
				const moisStore = Ext.getStore('moisStore');
				const findMois = moisStore.findRecord('moisSalaire', rec.get('mois'));
				return `${findMois.data.displayText} - ${rec.get('annee')}`;
			},
		},
		'nom',
		'prenom',
		{
			name: 'nomPrenom',
			convert(v, rec) {
				return `${rec.get('nom')} ${rec.get('prenom')}`;
			},
		},
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/FraisMedicaux.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
