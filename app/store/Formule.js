Ext.define('Gprh.ades.store.Formule', {
	extend: 'Ext.data.Store',
	
	storeId: 'formuleStore',
	alias: 'store.formuleStore',
	loading: true,
	
	fields: [
		'idFormule',
		'idAdes',
		'resultat',
		'formuleCalcul',
		'dateApplication',
		'actifFormule',
		'descriptionFormule',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Formules.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
