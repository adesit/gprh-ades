Ext.define('Gprh.ades.store.SalaireDetails', {
	extend: 'Ext.data.Store',
	
	storeId: 'salaireDetailsStore',
	alias: 'store.salaireDetailsStore',
	
	fields: [
		'idContrat',
		'matricule',
		'nomPrenom',
		'titrePoste',
		'categorieProfessionnelle',
		'dateEmbauche',
		'datePrisePoste',
		{ name: 'SBConclu2017', type: 'float' },
		{ name: 'Bjanvier', type: 'float' },
		{ name: 'salaireBase', type: 'float' },
		{ name: 'salaireSupplementaire', type: 'float' },
		{ name: 'nonPercu', type: 'float' },
		{ name: 'primeExceptionnel', type: 'float' },
		'anomalie',
		'dateDebutInterim',
		'dateFinInterim',
		{ name: 'montantInterim', type: 'float' },
		{ name: 'baseParHeure', type: 'float' },
		{ name: 'montantHS', type: 'float' },
		{ name: 'sBrutAvantMaternite', type: 'float' },
		{ name: 'montantHMinus', type: 'float' },
		{ name: 'dureeCongeMaterniteCeMois', type: 'float' },
		{ name: 'deductionMaternite', type: 'float' },
		{ name: 'salaireBrutFinal', type: 'float' },
		{ name: 'cnapsBase', type: 'float' },
		{ name: 'cnapsPrime', type: 'float' },
		'nomCentreSmie',
		{ name: 'smiePrime', type: 'float' },
		{ name: 'sommeCnapsSmie', type: 'float' },
		{ name: 'salaireBrutApresCnapsSmie', type: 'float' },
		{ name: 'brutApresCnapsSmieArrondi', type: 'float' },
		{ name: 'montantIrsaApresPlafond', type: 'float' },
		{ name: 'IrsaBrutArrondi', type: 'float' },
		{ name: 'abattement', type: 'float' },
		{ name: 'IrsaNet', type: 'float' },
		{ name: 'caisseHospitalisation', type: 'float' },
		{ name: 'factureFraisMedicaux', type: 'float' },
		{ name: 'tauxFraisMedicaux', type: 'float' },
		{ name: 'participationFraisMedicaux', type: 'float' },
		{ name: 'deductionAvance', type: 'float' },
		{ name: 'diversRetenus', type: 'float' },
		{ name: 'depassement', type: 'float' },
		{ name: 'Charge', type: 'float' },
		{ name: 'SalaireNet', type: 'float' },
		{ name: 'Retenus', type: 'float' },
		{ name: 'netAPayer', type: 'float' },
		{ name: 'decallageAvance', type: 'float' },
		{ name: 'congePris', type: 'float' },
		{
			name: 'soldeConge',
			type: 'float',
		},
		{ name: 'nbEnfant', type: 'float' },
		{ name: 'totalHeureHS', type: 'float' },
		'abreviationCentre',
		{ name: 'sBrutAnneeDerniere', type: 'float' },
		{ name: 'salaireBrutInclInflation', type: 'float' },
		'categorieReelle',
		{ name: 'rappelBase', type: 'float' },
		{ name: 'rappelNet', type: 'float' },
		'remarque',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/formule/DetailsFormule.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
		extraParams: {
			moisSalarial: -1,
		},
	},
	autoLoad: false,
	localFilter: true,
	listeners: {
		load: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			toolbar.onLoad();
		},
		filterchange: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			store.reload();
			toolbar.onLoad();
		},
	},
});
