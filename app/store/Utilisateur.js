/*
 * Définit une collection de Centre qui contient 9 éléments
 * Les données seront les résultats d'une requête vers le serveur PHP
 * requête de type AJAX
 * résultats attendus de type JSON
 * données prises en charge à partir de la clé 'data'
 */

Ext.define('Gprh.ades.store.Utilisateur', {
	extend: 'Ext.data.Store',
	
	storeId: 'utilisateurStore',
	alias: 'store.utilisateurStore',
	loading: true,

	requires: [
		'Gprh.ades.store.TypeUtilisateur',
	],
	
	fields: [
		'idUtilisateur',
		'nomUtilisateur',
		'motDePasse',
		'centreAffectation',
		'typeUtilisateur',
		{
			name: 'typeUtilisateur1',
			convert(v, rec) {
				const moisStore = Ext.create('Gprh.ades.store.TypeUtilisateur');
				const findMois = moisStore.findRecord('typeUtilisateur', rec.get('typeUtilisateur'));
				return findMois.data.displayText;
			},
		},
		'majMotDePasse',
		'avatar',
		'nomCentre',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Utilisateur.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
