Ext.define('Gprh.ades.store.TypeContrat', {
	extend: 'Ext.data.Store',
	
	storeId: 'typeContratStore',
	alias: 'store.typeContratStore',
	loading: true,
	
	fields: ['typeContrat', 'displayText'],
	data: [
		{ typeContrat: '1', displayText: 'Contrat à durée déterminée' },
		{ typeContrat: '2', displayText: 'Contrat à durée indéterminée' },
		{ typeContrat: '3', displayText: 'Contrat de collaboration extérieure / Sous traitance' },
	],
	autoLoad: true,
});
