Ext.define('Gprh.ades.store.DiversRetenus', {
	extend: 'Ext.data.Store',
	
	storeId: 'diversRetenusStore',
	alias: 'store.diversRetenusStore',
	loading: true,
	
	fields: [
		'idDiversRetenus',
		'idContrat',
		'idSalaire',
		'montantDiversRetenus',
		'dateDiversRetenus',
		'remarqueDiversRetenus',
		'matricule',
		'mois',
		'annee',
		{
			name: 'moisAnnee',
			convert(v, rec) {
				const moisStore = Ext.getStore('moisStore');
				const findMois = moisStore.findRecord('moisSalaire', rec.get('mois'));
				return `${findMois.data.displayText} - ${rec.get('annee')}`;
			},
		},
		'nom',
		'prenom',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/DiversRetenus.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
