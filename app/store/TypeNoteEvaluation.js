Ext.define('Gprh.ades.store.TypeNoteEvaluation', {
	extend: 'Ext.data.Store',
	
	storeId: 'typeNoteEvaluationStore',
	alias: 'store.typeNoteEvaluationStore',
	
	fields: ['typeNoteEvaluation', 'displayText'],
	data: [
		{ typeNoteEvaluation: '1', displayText: 'Médiocre' },
		{ typeNoteEvaluation: '2', displayText: 'Passable' },
		{ typeNoteEvaluation: '3', displayText: 'Moyen' },
		{ typeNoteEvaluation: '4', displayText: 'Très bien' },
		{ typeNoteEvaluation: '5', displayText: 'Excellent' },
	],
	autoLoad: true,
});
