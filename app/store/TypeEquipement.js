Ext.define('Gprh.ades.store.TypeEquipement', {
	extend: 'Ext.data.Store',
	
	storeId: 'typeEquipementStore',
	alias: 'store.typeEquipementStore',
	
	fields: ['typeEquipement', 'displayText'],
	data: [
		{ typeEquipement: '1', displayText: 'Voiture' },
		{ typeEquipement: '2', displayText: 'Moto' },
		{ typeEquipement: '3', displayText: 'Vélo' },
		{ typeEquipement: '4', displayText: 'Bureau complet' },
		{ typeEquipement: '5', displayText: 'Ordinateur portable' },
		{ typeEquipement: '6', displayText: 'Ordinateur de bureau' },
		{ typeEquipement: '7', displayText: 'Téléphone fixe' },
		{ typeEquipement: '8', displayText: 'Téléphone portable' },
		{ typeEquipement: '9', displayText: 'Flotte complet' },
		{ typeEquipement: '10', displayText: 'Numéro flotte uniquement' },
		{ typeEquipement: '11', displayText: 'EPI' },
		{ typeEquipement: '12', displayText: 'Boîte à outillage complet' },
		{ typeEquipement: '13', displayText: 'Frigidaire' },
		{ typeEquipement: '14', displayText: 'Salon' },
		{ typeEquipement: '15', displayText: 'Etagère de rangement' },
		{ typeEquipement: '16', displayText: 'Armoire' },
		{ typeEquipement: '17', displayText: 'Autre' },
	],
	autoLoad: true,
});