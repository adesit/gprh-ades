Ext.define('Gprh.ades.store.TauxListe', {
	extend: 'Ext.data.Store',
	
	storeId: 'tauxListeStore',
	alias: 'store.tauxListeStore',
	loading: true,

	/* 
	 * Ne jamais changer la valeur des "id", vous pouvez faire les modification des noms mais non pas la valeur de "id" 
	 * car elle est récupérée dans les formules de calcule et ensuite enregistrée dans l'historique des salaires
	 */
	
	fields: ['nomTaux', 'id'],
	data: [
		{ nomTaux: 'Augmentation générale (%)', id: 1 }, // ex. 15%
		{ nomTaux: 'Augmentation inflation (%)', id: 2 }, // ex. 5%
		{ nomTaux: 'Réduction salaire brut des congés de maternité (%)', id: 3 }, // ex. 50%
		{ nomTaux: 'Abattement par enfant (Ariary)', id: 4 }, // ex. 2000Ar par enfant
		{ nomTaux: 'Hopital (%)', id: 5 }, // ex. 2%
		{ nomTaux: 'Décallage entre 2 avances (mois)', id: 6 }, // ex. 3 mois
		{ nomTaux: 'Tranche max n°1(Ariary)', id: 7 }, // ex. 0 à 250 000Ar
		{ nomTaux: 'Taux 1 (%)', id: 8 }, // ex. 15%
		{ nomTaux: 'Tranche max n°2(Ariary)', id: 9 }, // ex. Tranche max n°1 à 300 000Ar et la 3ème tranche est supérieure à Tranche max n°2
		{ nomTaux: 'Taux 2 (%)', id: 10 }, // ex. 8%
		{ nomTaux: 'Taux 3 (%)', id: 11 }, // ex. 5%
		{ nomTaux: 'Suspendre toute augmentation', id: 12 }, // ex. 15%
	],
	autoLoad: true,
});
