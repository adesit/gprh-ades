Ext.define('Gprh.ades.store.EnfantScolarite', {
	extend: 'Ext.data.Store',
	
	storeId: 'enfantScolariteStore',
	alias: 'store.enfantScolariteStore',
	loading: true,
	
	fields: [
		'idEnfant',
		'idEmploye',
		'nomEnfant',
		'prenomEnfant',
		'dateNaissanceEnfant',
		'lieuNaissanceEnfant',
		'genreEnfant',
		'idScolarite',
		'idEducation',
		'nomEcole',
		'adresseEcole',
		'anneeScolaire',
		'fraisScolarite',
		'classe',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/EnfantScolarite.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
