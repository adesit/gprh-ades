Ext.define('Gprh.ades.store.District', {
	extend: 'Ext.data.Store',
	
	storeId: 'districtStore',
	alias: 'store.districtStore',
	loading: true,
	
	fields: [
		'idDistrict',
		'idRegion',
		'codeDistrict',
		'nomDistrict',
		'codeRegion',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/District.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
