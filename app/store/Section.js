Ext.define('Gprh.ades.store.Section', {
	extend: 'Ext.data.Store',
	
	storeId: 'sectionStore',
	alias: 'store.sectionStore',
	loading: true,
	
	fields: [
		'idSection',
		'nomSection',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Section.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
