Ext.define('Gprh.ades.store.Region', {
	extend: 'Ext.data.Store',
	
	storeId: 'regionStore',
	alias: 'store.regionStore',
	loading: true,
	
	fields: [
		'idRegion',
		'idProvince',
		'codeRegion',
		'nomRegion',
		'codeProvince',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Region.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
