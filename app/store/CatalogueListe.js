Ext.define('Gprh.ades.store.CatalogueListe', {
	extend: 'Ext.data.Store',
	
	storeId: 'catalogueListeStore',
	alias: 'store.catalogueListeStore',
	
	fields: [
		'idSousTheme',
		'idFormateur',
		'titreSousTheme',
		'objectifsSousTheme',
		'nomFormateur',
		'prenomFormateur',
		'titreFonctionFormateur',
		'institution',
		'emailFormateur',
		'telephoneFormateur',
		'adresseCompleteFormateur',
		{
			name: 'nomPrenomFormateur',
			convert(v, rec) {
				return `${rec.get('nomFormateur')} ${rec.get('prenomFormateur')}`;
			},
		},
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Catalogue.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
		extraParams: {
			infoRequest: 1,
		},
	},
	autoLoad: true,
	localFilter: true,
});
