Ext.define('Gprh.ades.store.Effectif', {
	extend: 'Ext.data.Store',
	
	storeId: 'effectifStore',
	alias: 'store.effectifStore',
	loading: true,
	
	fields: [
		'idCentre',
		'effectif',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Effectifs.php',
		reader: {
			type: 'json',
			rootProperty: 'effectif',
		},
	},
	autoLoad: true,
});
