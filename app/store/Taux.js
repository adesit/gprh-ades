Ext.define('Gprh.ades.store.Taux', {
	extend: 'Ext.data.Store',
	
	storeId: 'tauxStore',
	alias: 'store.tauxStore',
	loading: true,
	
	fields: [
		'idTaux',
		{
			name: 'nomTaux',
			convert(v, rec) {
				const tauxListeStore = Ext.getStore('tauxListeStore');
				const findTaux = tauxListeStore.findRecord('id', rec.get('nomTaux'));
				
				let result;
				if (Ext.isEmpty(findTaux)) result = null;
				else result = findTaux.data.nomTaux;
				return result;
			},
		},
		'valeurPourcent', 
		'datePriseEffetTaux', 
		'actifTaux',
		{
			name: 'typeTaux',
			convert(v, rec) {
				const tauxListeStore = Ext.getStore('tauxListeStore');
				const findTaux = tauxListeStore.findRecord('nomTaux', rec.get('nomTaux'));
				return findTaux.data.id;			
			},
		},
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Taux.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
