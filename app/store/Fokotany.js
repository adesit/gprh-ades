Ext.define('Gprh.ades.store.Fokotany', {
	extend: 'Ext.data.Store',
	
	storeId: 'fokotanyStore',
	alias: 'store.fokotanyStore',
	loading: true,
	
	fields: [
		'idFokotany',
		'idCommune',
		'codeFokotany',
		'nomFokotany',
		'codeCommune',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Fokotany.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
