Ext.define('Gprh.ades.store.NatureContrat', {
	extend: 'Ext.data.Store',
	
	storeId: 'natureContratStore',
	alias: 'store.natureContratStore',
	
	fields: ['natureContrat', 'displayText'],
	data: [
		{ natureContrat: '1', displayText: 'Travail à temps plein' },
		{ natureContrat: '2', displayText: 'Travail à temps partiel' },
	],
	autoLoad: true,
});
