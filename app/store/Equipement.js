Ext.define('Gprh.ades.store.Equipement', {
	extend: 'Ext.data.Store',
	
	storeId: 'equipementStore',
	alias: 'store.equipementStore',
	loading: true,
	
	fields: [
		'idEquipement',
		'idEmploye',
		'typeEquipement',
		'marque',
		'numeroSerie',
		'couleur',
		'autresCaracteristiques',
		'dateReception',
		'etatReception',
		'dateRestitution',
		'etatRestitution',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Equipement.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
	localFilter: true,
	listeners: {
		load: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			toolbar.onLoad();
		},
		filterchange: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			store.reload();
			toolbar.onLoad();
		},
	},
});
