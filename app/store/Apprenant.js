Ext.define('Gprh.ades.store.Apprenant', {
	extend: 'Ext.data.Store',
	
	storeId: 'apprenantStore',
	alias: 'store.apprenantStore',

	requires: [
		'Gprh.ades.store.Sexe',
		'Gprh.ades.store.TypeRelationAdes',
	],

	loading: true,
	
	fields: [
		'idInscription',
		'idApprenant',
		'idSessionFormation',
		'nomApprenant',
		'prenomApprenant',
		'dateNaissanceApprenant',
		'sexeApprenant',
		'telephoneApprenant',
		'adresseCompleteApprenant',
		'niveau',
		'typeRelationAdes',
		'idEmploye',
		'themeFormation',
		{
			name: 'nomPrenomApprenant',
			convert(v, rec) {
				return `${rec.get('nomApprenant')} ${rec.get('prenomApprenant')}`;
			},
		},
		'nomPrenomEmploye',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Apprenant.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
	localFilter: true,
	listeners: {
		load: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			toolbar.onLoad();
		},
		filterchange: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			store.reload();
			toolbar.onLoad();
		},
	},
});
