Ext.define('Gprh.ades.store.CongeSolde', {
	extend: 'Ext.data.Store',
	
	storeId: 'congeSoldeStore',
	alias: 'store.congeSoldeStore',
	loading: true,
	
	fields: [
		'idReliquat',
		'idContrat',
		'reliquat',
		'dateReleve',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/CongeSolde.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
