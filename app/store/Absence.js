Ext.define('Gprh.ades.store.Absence', {
	extend: 'Ext.data.Store',
	
	storeId: 'absenceStore',
	alias: 'store.absenceStore',

	loading: true,
	
	fields: [
		'idAbsence',
		'idContrat',
		'idSalaire',
		'motifAbsence',
		'dateHeureDebutHs',
		'dateHeureFinHs',
		'dureeAbsence',
		'typeAbsence',
		'matricule',
		'mois',
		'annee',
		{
			name: 'moisAnnee',
			convert(v, rec) {
				const moisStore = Ext.getStore('moisStore');
				const findMois = moisStore.findRecord('moisSalaire', rec.get('mois'));
				return `${findMois.data.displayText} - ${rec.get('annee')}`;
			},
		},
		'nom',
		'prenom',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Absence.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
