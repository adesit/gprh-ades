Ext.define('Gprh.ades.store.TypeRelationAdes', {
	extend: 'Ext.data.Store',
	
	storeId: 'typeRelationAdesStore',
	alias: 'store.typeRelationAdesStore',
	
	fields: ['typeRelationAdes', 'displayText'],
	data: [
		{ typeRelationAdes: '1', displayText: 'Employé' },
		{ typeRelationAdes: '2', displayText: 'Proche d\'un employé' },
		{ typeRelationAdes: '3', displayText: 'Personne tierce' },
		{ typeRelationAdes: '4', displayText: 'Fournisseur' },
		{ typeRelationAdes: '5', displayText: 'Prestataire' },
	],
	autoLoad: true,
});
