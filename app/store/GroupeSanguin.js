Ext.define('Gprh.ades.store.GroupeSanguin', {
	extend: 'Ext.data.Store',
	
	storeId: 'groupeSanguinStore',
	alias: 'store.groupeSanguinStore',
	
	fields: ['groupe', 'displayText'],
	data: [
		{ groupe: 'O-', displayText: 'O-' },
		{ groupe: 'O+', displayText: 'O+' },
		{ groupe: 'A-', displayText: 'A-' },
		{ groupe: 'A+', displayText: 'A+' },
		{ groupe: 'B-', displayText: 'B-' },
		{ groupe: 'B+', displayText: 'B+' },
		{ groupe: 'AB-', displayText: 'AB-' },
		{ groupe: 'AB+', displayText: 'AB+' },
	],
	autoLoad: true,
});
