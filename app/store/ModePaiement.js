Ext.define('Gprh.ades.store.ModePaiement', {
	extend: 'Ext.data.Store',
	
	storeId: 'modePaiementStore',
	alias: 'store.modePaiementStore',
	
	fields: ['modePaiement', 'displayText'],
	data: [
		{ modePaiement: '1', displayText: 'Virement bancaire' },
		{ modePaiement: '2', displayText: 'En espèces' },
		{ modePaiement: '3', displayText: 'Chèque' },
		{ modePaiement: '4', displayText: 'e-Money' },
	],
	autoLoad: true,
});
