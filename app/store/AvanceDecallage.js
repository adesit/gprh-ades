Ext.define('Gprh.ades.store.AvanceDecallage', {
	extend: 'Ext.data.Store',
	
	storeId: 'avanceDecallageStore',
	alias: 'store.avanceDecallageStore',
	loading: true,
	
	fields: [
		'idAvance',
		'idContrat',
		'dateFinRemboursement',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/AvanceDecallage.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
