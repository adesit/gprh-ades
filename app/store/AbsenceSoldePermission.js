Ext.define('Gprh.ades.store.AbsenceSoldePermission', {
	extend: 'Ext.data.Store',
	
	storeId: 'absenceSoldePermissionStore',
	alias: 'store.absenceSoldePermissionStore',

	loading: true,
	
	fields: [
		'idContrat',
		'soldePermission',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/AbsenceSoldePermission.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
