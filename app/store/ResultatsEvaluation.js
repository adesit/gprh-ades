Ext.define('Gprh.ades.store.ResultatsEvaluation', {
	extend: 'Ext.data.ArrayStore',
	
	storeId: 'resultatsEvaluationStore',
	alias: 'store.resultatsEvaluationStore',

	loading: true,
	
	proxy: {
		type: 'ajax',
		method: 'POST',
		url: 'server-scripts/listes/ResultatsEvaluation.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
