Ext.define('Gprh.ades.store.Sexe', {
	extend: 'Ext.data.Store',
	
	storeId: 'sexeStore',
	alias: 'store.sexeStore',
	
	fields: ['sexe', 'displayText'],
	data: [
		{ sexe: '1', displayText: 'Masculin' },
		{ sexe: '2', displayText: 'Féminin' },
	],
	autoLoad: true,
});
