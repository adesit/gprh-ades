Ext.define('Gprh.ades.store.ContratSuivi', {
	extend: 'Ext.data.Store',
	
	storeId: 'contratStoreSuivi',
	alias: 'store.contratStoreSuivi',
	
	fields: [
		'typeContrat',
		{
			name: 'dateEmbauche',
			convert(v, rec) {
				return Gprh.ades.util.Globals.rendererDate(rec.get('dateEmbauche'));
			},
		},		
		{
			name: 'sexe',
			convert(v, rec) {
				return (rec.get('sexe') === '1') ? 'Masculin' : 'Féminin';
			},
		},
		'titrePoste',
		'abreviationCentre',
		{
			name: 'nomPrenom',
			convert(v, rec) {
				return `${rec.get('nom')} ${rec.get('prenom')}`;
			},
		},
		{
			name: 'TypeContrat',
			convert(v, rec) {
				const typeStore = Ext.create('Gprh.ades.store.TypeContrat');
				const findType = typeStore.findRecord('typeContrat', rec.get('typeContrat'));
				return findType.data.displayText;
			},
		},
		{
			name: 'evaluationUnAn',
			convert(v, rec) {
				let dateEvaluation;
				const dateEmbauche = new Date(Gprh.ades.util.Globals.rendererDate(rec.get('dateEmbauche')));
				if (rec.get('typeContrat') === '1') {
					const dateEvaluationUnAn = Ext.Date.add(dateEmbauche, Ext.Date.YEAR, 1);
					const anniversaire = dateEvaluationUnAn.getFullYear();
					const curYear = new Date().getFullYear();
					const nextYear = curYear + 1;

					if (anniversaire === curYear || anniversaire === nextYear) {
						dateEvaluation = dateEvaluationUnAn;
					}
				}
				return dateEvaluation;
			},
		},
		{
			name: 'evaluationDeuxAn',
			convert(v, rec) {
				let dateEvaluation;
				const dateEmbauche = new Date(Gprh.ades.util.Globals.rendererDate(rec.get('dateEmbauche')));
				if (rec.get('typeContrat') === '1') {
					const dateEvaluationDeuxAns = Ext.Date.add(dateEmbauche, Ext.Date.YEAR, 2);
					const anniversaire = dateEvaluationDeuxAns.getFullYear();
					const firstYear = new Date().getFullYear();
					const secondYear = firstYear + 1;
					const thirdYear = firstYear + 2;

					if (anniversaire === firstYear || anniversaire === secondYear || anniversaire === thirdYear) {
						dateEvaluation = dateEvaluationDeuxAns;
					}
				} 
				
				return dateEvaluation;
			},
		},
		{
			name: 'anciennetePlusCinqAn',
			convert(v, rec) {
				const calcDate = (date1, date2) => {
					const diff = Math.floor(date1.getTime() - date2.getTime());
					const day = 1000 * 60 * 60 * 24;
				
					const days = Math.floor(diff / day);
					const months = Math.floor(days / 31);
					const years = Math.floor(months / 12);
				
					return years;
				};
				return calcDate(new Date(), new Date(rec.get('dateEmbauche')));
			},
		},
		'dateNaissance',
		{
			name: 'anniversaire',
			convert(v, rec) {
				let anniversaire;
				const dob = new Date(rec.get('dateNaissance'));
				const moisAnniversaire = dob.getMonth();
				if (moisAnniversaire === new Date().getMonth()) {
					const anniv = dob.setFullYear(new Date().getFullYear());
					anniversaire = Gprh.ades.util.Globals.rendererDate(anniv);
				}
				
				return anniversaire;
			},
		},
		{
			name: 'evaluationRendererUn',
			convert(v, rec) {
				let dateEvaluation;
				const dateEmbauche = new Date(Gprh.ades.util.Globals.rendererDate(rec.get('dateEmbauche')));
				if (rec.get('typeContrat') === '1') {
					const dateEvaluationUnAn = Ext.Date.add(dateEmbauche, Ext.Date.YEAR, 1);
					const anniversaire = dateEvaluationUnAn.getFullYear();
					const curYear = new Date().getFullYear();
					const nextYear = curYear + 1;

					if (anniversaire === curYear || anniversaire === nextYear) {
						dateEvaluation = dateEvaluationUnAn;
					}
				}
				if (!Ext.isEmpty(dateEvaluation)) {
					// Palette de couleur Excel
					const colors = ['#CD6155', '#E6B0AA', '#F5B7B1', '#D7BDE2', '#D2B4DE', '#A9CCE3', '#AED6F1', '#A3E4D7', '#A2D9CE', '#A9DFBF', '#ABEBC6', '#58D68D', '#F9E79F', '#FAD7A0', '#F5CBA7', '#EDBB99', '#E59866', '#F7F9F9', '#E5E7E9', '#D5DBDB', '#CCD1D1', '#AEB6BF', '#ABB2B9', '#808B96', '#2C3E50'];
					const values = new Date(dateEvaluation);
					const differenceMois = values.getMonth() - new Date().getMonth() + (12 * (values.getFullYear() - new Date().getFullYear()));
					const color = colors[12 + differenceMois];
					return color;
				}
				return '#FFFFFF';
			},
		},
		{
			name: 'evaluationRendererDeux',
			convert(v, rec) {
				let dateEvaluation;
				const dateEmbauche = new Date(rec.get('dateEmbauche'));
				if (rec.get('typeContrat') === '1') {
					const dateEvaluationDeuxAns = Ext.Date.add(dateEmbauche, Ext.Date.YEAR, 2);
					const anniversaire = dateEvaluationDeuxAns.getFullYear();
					const firstYear = new Date().getFullYear();
					const secondYear = firstYear + 1;
					const thirdYear = firstYear + 2;

					if (anniversaire === firstYear || anniversaire === secondYear || anniversaire === thirdYear) {
						dateEvaluation = Gprh.ades.util.Globals.rendererDate(dateEvaluationDeuxAns);
					}
				}
				if (!Ext.isEmpty(dateEvaluation)) {
					// Palette de couleur Excel
					const colors = ['#CD6155', '#E6B0AA', '#F5B7B1', '#D7BDE2', '#D2B4DE', '#A9CCE3', '#AED6F1', '#A3E4D7', '#A2D9CE', '#A9DFBF', '#ABEBC6', '#58D68D', '#F9E79F', '#FAD7A0', '#F5CBA7', '#EDBB99', '#E59866', '#F7F9F9', '#E5E7E9', '#D5DBDB', '#CCD1D1', '#AEB6BF', '#ABB2B9', '#808B96', '#2C3E50'];
					const values = new Date(dateEvaluation);
					const differenceMois = values.getMonth() - new Date().getMonth() + (12 * (values.getFullYear() - new Date().getFullYear()));
					const color = colors[12 + differenceMois];
					return color;
				}
				return '#FFFFFF';
			},
		},
		{
			name: 'ancienneteRenderer',
			convert(v, rec) {
				const calcDate = (date1, date2) => {
					const diff = Math.floor(date1.getTime() - date2.getTime());
					const day = 1000 * 60 * 60 * 24;
				
					const days = Math.floor(diff / day);
					const months = Math.floor(days / 31);
					const years = Math.floor(months / 12);
				
					return years;
				};
				const value = calcDate(new Date(), new Date(rec.get('dateEmbauche')));
				if (!Ext.isEmpty(value)) {
					const colors = ['#CD6155', '#E6B0AA', '#F5B7B1', '#D7BDE2', '#D2B4DE', '#A9CCE3', '#AED6F1', '#A3E4D7', '#A2D9CE', '#A9DFBF', '#ABEBC6', '#58D68D', '#F9E79F', '#FAD7A0', '#F5CBA7', '#EDBB99', '#E59866', '#F7F9F9', '#E5E7E9', '#D5DBDB', '#CCD1D1', '#AEB6BF', '#ABB2B9', '#808B96', '#2C3E50'];
					const color = colors[value];
					return color;
				}
				return '#FFFFFF';
			},
		},
		{
			name: 'anniversaireRenderer',
			convert(v, rec) {
				let anniversaire;
				const dob = new Date(rec.get('dateNaissance'));
				const moisAnniversaire = dob.getMonth();
				if (moisAnniversaire === new Date().getMonth()) {
					const anniv = dob.setFullYear(new Date().getFullYear());
					anniversaire = Gprh.ades.util.Globals.rendererDate(anniv);
				}
				if (!Ext.isEmpty(anniversaire)) {
					const date1 = anniversaire.split('/');
					const newDate = `${date1[1]}/${date1[0]}/${date1[2]}`;
					const values = new Date(newDate);
					if (!Ext.isEmpty(values)) {
						const generate = (r, g, b) => {
							const red = r % 256;
							const green = g % 256;
							const blue = b % 256;
							return `rgb(${red + 33}, ${green + 33}, ${blue + 33})`;
						};
						const differenceJours = (Math.round((values - new Date()) / (1000 * 60 * 60 * 24))) - 1;
						const rouge = (31 + differenceJours) * 2.5;
						const color = (generate(rouge, 0, 0));
	
						return color;
					}
				}
				return '#FFFFFF';				
			},
		},
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Contrat.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
			totalProperty: 'total',
		},
	},
	autoLoad: true,
	localFilter: true,
	listeners: {
		load: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			toolbar.onLoad();
		},
		filterchange: (store) => {
			const toolbar = Ext.ComponentQuery.query('pagingtoolbar')[0];
			Ext.apply(store, {
				pageSize: store.count(),
			});
			store.reload();
			toolbar.onLoad();
		},
	},
});
