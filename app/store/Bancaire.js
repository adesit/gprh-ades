Ext.define('Gprh.ades.store.Bancaire', {
	extend: 'Ext.data.Store',
	
	storeId: 'bancaireStore',
	alias: 'store.bancaireStore',
	loading: true,
	
	fields: [
		'idBancaire',
		'idEmploye',
		'nomBanque',
		'codeBanque',
		'numeroCompte',
		'codeGuichet',
		'cleRib',
		'datePriseEffetBanque',
		'actifBancaire',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Bancaire.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
