Ext.define('Gprh.ades.store.Matrimoniale', {
	extend: 'Ext.data.Store',
	
	storeId: 'matrimonialeStore',
	alias: 'store.matrimonialeStore',
	
	fields: ['civilite', 'displayText'],
	data: [
		{ civilite: '1', displayText: 'Mr' },
		{ civilite: '2', displayText: 'Mme' },
		{ civilite: '3', displayText: 'Mlle' },
	],
	autoLoad: true,
});
