Ext.define('Gprh.ades.store.Conjoint', {
	extend: 'Ext.data.Store',
	
	storeId: 'conjointStore',
	alias: 'store.conjointStore',
	loading: true,
	
	fields: [
		'idConjoint',
		'idEmploye',
		'nomConjoint',
		'prenomConjoint',
		'dateNaissanceConjoint',
		'genreConjoint',
		'fonctionConjoint',
		'telConjoint',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Conjoint.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
