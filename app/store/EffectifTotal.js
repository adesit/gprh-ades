Ext.define('Gprh.ades.store.EffectifTotal', {
	extend: 'Ext.data.Store',
	
	storeId: 'effectifTotalStore',
	alias: 'store.effectifTotalStore',
	loading: true,
	
	fields: [
		'totalEffectif',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Effectifs.php',
		reader: {
			type: 'json',
			rootProperty: 'totalEffectif',
		},
	},
	autoLoad: true,
});
