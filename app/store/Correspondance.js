Ext.define('Gprh.ades.store.Correspondance', {
	extend: 'Ext.data.Store',
	
	storeId: 'correspondanceStore',
	alias: 'store.correspondanceStore',
	loading: true,
	
	fields: [
		'idCorrespondant',
		'idEmploye',
		'nomPrenomCorrespondant',
		'telCorrespondant',
	],
	proxy: {
		type: 'ajax',
		url: 'server-scripts/listes/Correspondance.php',
		reader: {
			type: 'json',
			rootProperty: 'data',
		},
	},
	autoLoad: true,
});
