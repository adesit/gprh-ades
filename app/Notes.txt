'Gprh.ades.view.login.Login',
'Gprh.ades.view.ades.AdesMainContainer',
'Gprh.ades.view.personnel.PersonnelMainContainer',
'Gprh.ades.view.contrat.ContratMainContainer',
'Gprh.ades.view.salaire.SalaireMainContainer',
'Gprh.ades.view.salaire.elementsVariables.SalaireInterimPanel',
'Gprh.ades.view.salaire.elementsVariables.HeuresSupplementairesPanel',
'Gprh.ades.view.salaire.elementsVariables.AbsencePanel',
'Gprh.ades.view.salaire.elementsVariables.CongePanel',
'Gprh.ades.view.salaire.elementsVariables.AvancePanel',
'Gprh.ades.view.salaire.elementsVariables.FraisMedicauxPanel',
'Gprh.ades.view.salaire.elementsVariables.DiversRetenusPanel',
'Gprh.ades.view.salaire.elementsVariables.DepassementPanel',
'Gprh.ades.view.salaire.elementsVariables.RappelPanel',
'Gprh.ades.view.utilisateur.UtilisateurMainContainer',
'Gprh.ades.view.paie.PaieMainContainer',

------------
Ext.Loader.setPath('Gprh.ades', 'app');
Ext.require('Gprh.ades.view.main.Main');
Ext.require('Gprh.ades.view.login.Login');